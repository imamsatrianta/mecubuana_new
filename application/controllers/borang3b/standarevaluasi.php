<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class standarevaluasi extends CI_Controller {

    function standarevaluasi()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('borang3b/standarevaluasi_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
	   $parameter= $this->input->get("parameter"); 
		$namatabel= $this->input->get("namatabel");
		$standar= $this->input->get("standar");
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(b.nm_jenjangprodi) like upper('%$search%')   or upper(c.nm_tahun_akademik) like upper('%$search%')
			or upper(d.nm_prodi) like upper('%$search%') or upper(a.uraian) like upper('%$search%')  ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->standarevaluasi_m->loaddataTabel($offset,$limit,$order,$where,$parameter,$namatabel,$standar); 
     
    } 
	
	
   
   // evaluasi borang
    function simpanDatareevaluasi(){
		
	$this->db->trans_begin();
	$status=true;
	$totalcek=0; 
	$setsimpan=0;
	$id_tahunakademikevl = $this->input->post("id_tahunakademikevl");
	$id_jenjangprodievl = $this->input->post("id_jenjangprodievl"); 
	$id_prodievl = $this->input->post("id_prodievl");
	$not_evaluasi = $this->input->post('not_evaluasi');
	$status_evaluasi = $this->input->post('status_evaluasi');
	$tgl_dodate= $this->input->post('tgl_dodate');
	$username=$this->session->userdata('username');
	$standar= $this->input->post('standar');
	$tgl=date('Y-m-d');
	
	$dudatedekan=date('Y-m-d', strtotime($tgl. ' + 7 days'));
	
	$levelprodi=$this->session->userdata('levelprodi');
	if($tgl_dodate==""){
		if($levelprodi=="1"){
			$tgl_dodateok='';
			$status_evaluasi="2";
		}else{
			$tgl_dodateok=$dudatedekan;
			$status_evaluasi="1";
		}
		
	}else{
		$tgl_dodateok=$tgl_dodate;
		$status_evaluasi="0";
	}
	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	$dataevaluasi=array(
		'id_tahunakademik'=>$id_tahunakademikevl,
		'id_jenjangprodi'=>$id_jenjangprodievl,
		'id_prodi'=>$id_prodievl,
		'standar'=>$standar,
		'tgl_dodate'=>$tgl_dodateok,
		'note_evaluasi'=>$not_evaluasi,
		'status_evaluasi'=>$status_evaluasi,
		'user_kirim'=>$username,
		'tgl_kirim'=>$tgl);
		
	$data=array(
		'status_data'=>0);
	
		//print_r($datasimpan); exit;
		
		$table="br_standarsatu";
		$ceksatu=$this->standarevaluasi_m->editData($id_tahunakademikevl,$id_jenjangprodievl,$id_prodievl,$data,$standar);
		
		//print_r($datasimpan); exit;
		$ceksatu=$this->standarevaluasi_m->simpanDatarevaluasi($dataevaluasi);
		
		/*
		$jenis=$status;
		$this->global_m->kirimEmailsarmut($id_prodi,"revisi",$jenis);
		$this->global_m->kirimEmailsarmutfakultas($id_prodi,"input");
		*/
		
		
		if($ceksatu==1){
			$setsim="ok";	
		}else{
			$totalcek++;
		}
	
	

	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
   }
   
   
      function cetakBorang(){
	   
	   $id_jenjangprodi=$_GET['id_jenjangprodi'];
	   $id_tahunakademik=$_GET['id_tahunakademik'];
	   $id_prodi=$_GET['id_prodi']; 
	   $tabel=$_GET['tabel']; 
	   $standar=$_GET['standar']; 
	   
	   if($standar=="1"){
		    $data['databorang']=$this->standarevaluasi_m->getBorang($id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel,$standar);
		//	print_r($data['databorang']);exit;
			   foreach($data['databorang'] as $std){
				$id=$std->id;
				$data['datauraian'][$id]=$this->standarevaluasi_m->getIsiborang($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel);
				
				}
			 $data['identitasborang']=$this->standarevaluasi_m->getIdentitas($id_jenjangprodi,$id_prodi);	
			 $data['dataprodi']=$this->standarevaluasi_m->getIdentitasprodi($id_jenjangprodi,$id_prodi);
			 $data['datapengisi']=$this->standarevaluasi_m->getPengisiborang($id_jenjangprodi,$id_prodi,$id_tahunakademik);
			 
		   if($id_jenjangprodi=="1"){
			   $html = $this->load->view('cetakborang/d33bsatu', $data,true);
					
		   }if($id_jenjangprodi=="2"){
			   $html = $this->load->view('cetakborang/d33bsatu', $data,true);
		   }if($id_jenjangprodi=="3"){
			   $html = $this->load->view('cetakborang/d33bsatu', $data,true);
		   }if($id_jenjangprodi=="4"){
			   $html = $this->load->view('cetakborang/d33bsatu', $data,true);
		   }if($id_jenjangprodi=="5"){
			   
		   }
		   
		   
		   
	   }else if($standar=="2"){
		   $data['databorang']=$this->standarevaluasi_m->getBorang($id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel,$standar);
			   foreach($data['databorang'] as $std){
				$id=$std->id;
				$data['datauraian'][$id]=$this->standarevaluasi_m->getIsiborang($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel);
				
				}
			   if($id_jenjangprodi=="1"){
				   $html = $this->load->view('cetakborang/d33bdua', $data,true);
						
			   }if($id_jenjangprodi=="2"){
				   $html = $this->load->view('cetakborang/d33bdua', $data,true);
			   }if($id_jenjangprodi=="3"){
				   $html = $this->load->view('cetakborang/d33bdua', $data,true);
			   }if($id_jenjangprodi=="4"){
				   $html = $this->load->view('cetakborang/d33bdua', $data,true);
			   }if($id_jenjangprodi=="5"){
				   
			   }
			   
	   }else if($standar=="3"){
		
				
			$data['databorang']=$this->standarevaluasi_m->getBorang($id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel,$standar);
			
			if($id_jenjangprodi=="1"){
				  foreach($data['databorang'] as $std){
					$id=$std->id;
					$data['datauraian'][$id]=$this->standarevaluasi_m->getIsiborang($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel);
						if($id=="55"){
							$data['datatigasatusatu'][$id]=$this->standarevaluasi_m->getTigasatusatu($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_3bd3_tiga_satu_dua');
						}else if($id==58){
							$data['datatigasatusatu'][$id]=$this->standarevaluasi_m->getTigasatusatu($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_3bd3_tiga_dua_satu');
						}
					
					}
				$html = $this->load->view('cetakborang/3bd3tiga', $data,true);
				
			   }if($id_jenjangprodi=="2"){
				  
				  foreach($data['databorang'] as $std){
					$id=$std->id;
					$data['datauraian'][$id]=$this->standarevaluasi_m->getIsiborang($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel);
						if($id=="76"){
							$data['standarsarjana'][$id]=$this->standarevaluasi_m->getTigasarjana($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_3bs1_tiga_satu_dua');
						}else if($id==79){
							$data['standarsarjana'][$id]=$this->standarevaluasi_m->getTigasarjana($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_3bs1_tiga_dua_satu');
						}
					
					}
				$html = $this->load->view('cetakborang/3bs1tiga', $data,true);
				
				
			   }if($id_jenjangprodi=="3"){
				   foreach($data['databorang'] as $std){
					$id=$std->id;
					$data['datauraian'][$id]=$this->standarevaluasi_m->getIsiborang($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel);
						if($id=="96"){
							$data['standarsarjana'][$id]=$this->standarevaluasi_m->getTigamagister($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_3bs2_tiga_dua_satu');
						}
					
					}
				$html = $this->load->view('cetakborang/3bs2tiga', $data,true);
			   }if($id_jenjangprodi=="4"){
				    foreach($data['databorang'] as $std){
					$id=$std->id;
					$data['datauraian'][$id]=$this->standarevaluasi_m->getIsiborang($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel);
						if($id=="112"){
							$data['standarsarjana'][$id]=$this->standarevaluasi_m->getTigadoktor($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_3bs3_tiga_dua_satu');
						}
					
					}
				$html = $this->load->view('cetakborang/3bs3tiga', $data,true);
			   }if($id_jenjangprodi=="5"){
				   
			   }
			   
				
				
			   
			//   print_r($data['datatigasatusatu']);
			   
			   
		   
	   }else if($standar=="4"){
		   
	   }else if($standar=="5"){
		   
	   }else if($standar=="6"){
		   
	   }else if($standar=="7"){
		   
	   }
	   
	 
	

libxml_use_internal_errors(true);
$doc = new DomDocument;
$doc->loadHTML($html);
$datauraian=$doc->saveHTML();



 header("Expires: 0");
 header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=Buku_Borang".$standar.".doc");

echo $datauraian;
	

		
		
   }
   
   
   
   
   
}?>