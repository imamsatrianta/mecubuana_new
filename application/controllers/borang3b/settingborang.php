<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class settingborang extends CI_Controller {

    function settingborang()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('borang3b/settingborang_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
	   //echo "xx";exit;
    	$this->load->view('borang3b/settingborang_v');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$set = $this->input->post("set");
	$standar= $this->input->post("standar");
	$poin = $this->input->post('poin');
	$judul = $this->input->post('judul');
	$uraian = $this->input->post('uraianck');
	$parent_id=$this->input->post('parent_id');
	$jenis = $this->input->post('jenis');
	$id_jenjangprodi = $this->input->post('id_jenjangprodi');
	$level = $this->input->post('level');
	$url= $this->input->post('url');
	$no = $this->input->post('no');
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');

	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	$data=array(
		'poin'=>$poin,
		'standar'=>$standar,
		'judul'=>$judul,
		'uraian'=>$uraian,
		'parent_id'=>$parent_id,
		'jenis'=>$jenis,
		'url'=>$url,
		'no'=>$no,
		'id_jenjangprodi'=>$id_jenjangprodi,
		'level'=>$level);
		
	if($set==0){
	$datasimpan=array_merge($datainput,$data);
	//print_r($datasimpan); exit;
	$ceksatu=$this->settingborang_m->simpanData($datasimpan);
		
	}else{
		
		$datasimpan=array_merge($dataubah,$data);
		$ceksatu=$this->settingborang_m->editData($id,$datasimpan);
	}
	
	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.judul) like upper('%$search%')   or upper(b.nm_jenjangprodi) like upper('%$search%')
			or upper(a.poin) like upper('%$search%') or upper(a.uraian) like upper('%$search%')  ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->settingborang_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->settingborang_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->settingborang_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function getCombo(){
	   $arrayMenu=array();
		$arauSatu =array();
		$arrayDua=array();
		$arrayTiga=array();
		$menusatu=$this->settingborang_m->getLevelsatu(); 
		//print_r($menusatu);
		foreach($menusatu as $datasatu){ 
			$idmenu=$datasatu->id;
			$menudes=$datasatu->poin;

			$arauSatu['id'] = $idmenu;
			$arauSatu['text'] = $menudes;
			 $arauSatu['children'] = array();
			 
			$dataarray=$this->settingborang_m->levelDetail($idmenu);
		//	echo count ($dataarray)."<br>";
			foreach($dataarray as $data){ 
			
			$iddua=$data->id;
			$textdua=$data->poin;
			$arrayDua['id']=$iddua;
			$arrayDua['text']=$textdua;
			$arrayDua['children']=array();
			
			$dataarraytiga=$this->settingborang_m->levelDetail($iddua);
				foreach($dataarraytiga as $datatiga){ 
					$idtiga=$datatiga->id;
					$texttiga=$datatiga->poin;
					$arrayTiga['id']=$idtiga;
					$arrayTiga['text']=$texttiga;
					//$arrayTiga['children']=1;
					array_push($arrayDua['children'],$arrayTiga);
				}
			
			
			
			
			array_push($arauSatu['children'],$arrayDua);
			
			}
			
			array_push($arrayMenu,$arauSatu);
		}
		
		
		
		 
		$jsonData = json_encode($arrayMenu);
		
		echo $jsonData; 
		
   }
   
   

}?>