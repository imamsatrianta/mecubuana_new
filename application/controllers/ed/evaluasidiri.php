<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class evaluasidiri extends CI_Controller {

    function evaluasidiri()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('ed/evaluasidiri_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		$level=$_GET['level'];
		$data['idm']=$_GET['idm'];
		$data['level']=$_GET['level'];
		$data['grupmenu']=$this->evaluasidiri_m->loadDatamenugrup();
		//print_r($data['grupmenu']);exit;
		foreach($data['grupmenu'] as $dataed){ 
			$id=$dataed->id; 
			$data['menu'][$id]=$this->evaluasidiri_m->loadDatamenu($level,$id);
			foreach($data['menu'][$id] as $datadua){ 
				$iddua=$datadua->id; 
				$menu_det=$datadua->menu_det; 
				if($menu_det=="1"){
				$data['menudua'][$id][$iddua]=$this->evaluasidiri_m->loadDatamenudua($level,$id,$iddua);
				//print_r($data['menudua']);exit;
				}
			}
			//print_r($data['menu']);exit;
		}
		
	   $this->load->view('atas_v');
	   //echo "xx";exit;
		$this->load->view('ed/evaluasidirimenu_v',$data);

			//exit;
			
			$parameter=$_GET['parameter'];
			if($parameter<>0){
				$this->load->view('ed/evaluasidiri_v',$data);
			$this->load->view('ed/evaluasidiri_js');
			}else{
				$this->load->view('ed/evaluasidirigrup_v',$data);
				$this->load->view('ed/evaluasidirigrup_js');
			}
			
			
			
		
		
    	
    	$this->load->view('bawah');
	}
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
	   $parameter= $this->input->get("parameter");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(b.nm_jenjangprodi) like upper('%$search%')   or upper(c.nm_tahun_akademik) like upper('%$search%')
			or upper(d.nm_prodi) like upper('%$search%') or upper(a.uraian) like upper('%$search%')  ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->evaluasidiri_m->loaddataTabel($offset,$limit,$order,$where,$parameter); 
     
    } 
	
	function simpanData(){
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$setsmpan=0;
	$id = $this->input->post("id");
	$set = $this->input->post("set");
	$id_tahunakademik= $this->input->post("id_tahunakademik");
	$id_parameter = $this->input->post('id_parameter');
	$id_jenjangprodi = $this->input->post('id_jenjangprodi');
	$id_prodi = $this->input->post('id_prodi');
	$tanggal=$this->input->post('tanggal');
	$uraian = $this->input->post('uraiandata');
	$nilai = $this->input->post('nilai');
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');

	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	$data=array(
		'id_tahunakademik'=>$id_tahunakademik,
		'id_parameter'=>$id_parameter,
		'id_jenjangprodi'=>$id_jenjangprodi,
		'id_prodi'=>$id_prodi,
		'tanggal'=>$tanggal,
		'uraian'=>$uraian,
		'nilai'=>$nilai);
	$ceksatu="";	
	if($set==0){
	$datasimpan=array_merge($datainput,$data);
	$cekborang=$this->evaluasidiri_m->cekBorang($id_tahunakademik,$id_parameter,$id_jenjangprodi,$id_prodi);
	if (!empty($cekborang)) {
		$setsmpan++;
	}else{
	//print_r($datasimpan); exit;
	$ceksatu=$this->evaluasidiri_m->simpanData($datasimpan);
	}
		
	}else{
		
		$datasimpan=array_merge($dataubah,$data);
		$ceksatu=$this->evaluasidiri_m->editData($id,$datasimpan);
	}
	
	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
	$status = $this->db->trans_status();
	if($setsmpan>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal, Data Sudah Ada","status" => "error"));
	}else if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
   
   function hapusData(){
	    $id = $this->input->get("id");
		 $ceksatu=$this->evaluasidiri_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->evaluasidiri_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function simpanDatarefisi(){
	$this->db->trans_begin();
	$status=true;
	$totalcek=0; 
	$setsimpan=0;
	$id = $this->input->post("id_ref");
	$id_prodi = $this->input->post("id_prodi_ref"); 
	$status = $this->input->post("status");
	//$tgl_dodate = $this->input->post('tgl_dodate');
	$note_status = $this->input->post('note_status');
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	$datarefisi=array(
		'id_ed'=>$id,
		'id_prodi'=>$id_prodi,
		'tgl'=>$tgl,
		'keterangan'=>$note_status);
	$data=array(
		'status'=>$status,
		//'tgl_dodate'=>$tgl_dodate,
		'note_status'=>$note_status);
	
		$datasimpan=array_merge($datainput,$data);
		//print_r($datasimpan); exit;
		$ceksatu=$this->evaluasidiri_m->editData($id,$datasimpan);
		
		$datasimpanref=array_merge($datainput,$datarefisi);
		//print_r($datasimpan); exit;
		$ceksatu=$this->evaluasidiri_m->simpanDatarefisi($datasimpanref);
		
		if($ceksatu==1){
			$setsim="ok";	
		}else{
			$totalcek++;
		}
	
	

	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
   }
  
}?>