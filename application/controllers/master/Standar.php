<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Standar extends CI_Controller {

    function Standar()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('master/standar_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
	   //echo "xx";exit;
    	$this->load->view('master/standar_v');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$id_renstra = $this->input->post('id_renstra');
	$parent_id = $this->input->post('parent_id');
	$no_dokumen = $this->input->post('no_dokumen');
	$nm_dokumen = $this->input->post('nm_dokumen');
	$no_refisi = $this->input->post('no_refisi');
	$tgl_publish = $this->input->post('tgl_publish');
	$set = $this->input->post("set");
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	$setgbr=$this->input->post('setgbr');
	
	if( $setgbr==1){	
	$allowedExts = array("pdf", "jpeg", "jpg", "png");
			$temp = explode(".", $_FILES["file_uploadda"]["name"]);
			$extension = end($temp);
			if ((($_FILES["file_uploadda"]["type"] == "application/pdf")
			|| ($_FILES["file_uploadda"]["type"] == "image/jpeg")
			|| ($_FILES["file_uploadda"]["type"] == "image/jpg")
			|| ($_FILES["file_uploadda"]["type"] == "image/pjpeg")
			|| ($_FILES["file_uploadda"]["type"] == "image/x-png")
			|| ($_FILES["file_uploadda"]["type"] == "image/png"))
			&& ($_FILES["file_uploadda"]["size"] < 200000000)
			&& in_array($extension, $allowedExts)) {
			//echo $extension."xxx";
			 $acak           = rand(000000,999999);
			$filename =$_FILES["file_uploadda"]["name"];
			$dataimage = $acak.$filename;
				
			move_uploaded_file($_FILES["file_uploadda"]["tmp_name"],
					"dokstandar/" . $dataimage);
			
			//echo "ok";
			}else{
			$dataimage="";
			}
	}else{
			$dataimage="";
			}
			
	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	if($set==0){
		$data=array(
		'id_renstra'=>$id_renstra,
		'parent_id'=>$parent_id,
		'no_dokumen'=>$no_dokumen,
		'nm_dokumen'=>$nm_dokumen,
		'no_refisi'=>$no_refisi,
		'tgl_publish'=>$tgl_publish,
		'file_upload'=>'dokstandar/'.$dataimage);
	$datasimpan=array_merge($datainput,$data);
	//print_r($datasimpan); exit;
	$ceksatu=$this->standar_m->simpanData($datasimpan);
	$iddet=mysql_insert_id();
		
	}else{
		if($dataimage==""){
		$data=array(
		'id_renstra'=>$id_renstra,
		'parent_id'=>$parent_id,
		'no_dokumen'=>$no_dokumen,
		'nm_dokumen'=>$nm_dokumen,
		'no_refisi'=>$no_refisi,
		'tgl_publish'=>$tgl_publish);
			
		}else{
		$data=array(
		'id_renstra'=>$id_renstra,
		'parent_id'=>$parent_id,
		'no_dokumen'=>$no_dokumen,
		'nm_dokumen'=>$nm_dokumen,
		'no_refisi'=>$no_refisi,
		'tgl_publish'=>$tgl_publish,
		'file_upload'=>'dokstandar/'.$dataimage);
		
		}
		$datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->standar_m->editData($id,$datasimpan);
		$iddet=$id;
		//$this->standar_m->hapusDetail($iddet);
	}
	
	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
		$idhapus=array();
		if (isset($_POST['detail'])) {
			$detail = $this->input->post('detail');
			
			foreach ($detail as $key => $detaildata ) {
				 $id_dok=trim($detaildata['id_dok']);
				 $fileupl =$_FILES["file_dok".$key]["name"];
				 $gbr="1";
				 $datadetdua = array(
						'id_standar' => $iddet,
						'nomor' => trim($detaildata['nomor']),
						'uraian' => trim($detaildata['uraian']),
						'indikaor' => trim($detaildata['indikaor']),
						'target_waktu' => trim($detaildata['target_waktu']),
						'user_input' => $username,
						'tgl_input'=>$tgl
					);
					
							$allowedExts = array("gif", "jpeg", "jpg", "png", "pdf", "doc","docx");
							$temp = explode(".", $_FILES["file_dok".$key]["name"]);
							$extension = end($temp);
							if ((($_FILES["file_dok".$key]["type"] == "image/jpeg")
							|| ($_FILES["file_dok".$key]["type"] == "image/jpg")
							|| ($_FILES["file_dok".$key]["type"] == "image/pjpeg")
							|| ($_FILES["file_dok".$key]["type"] == "application/pdf")
							|| ($_FILES["file_dok".$key]["type"] == "application/msword")
							|| ($_FILES["file_dok".$key]["type"] == "image/png"))
							&& ($_FILES["file_dok".$key]["size"] < 900000000)
							&& in_array($extension, $allowedExts)) {
							
							//echo $extension."xxx";
							 $acak           = rand(000000,999999);
							$filename =$_FILES["file_dok".$key]["name"];
							$dataimage = $acak.$filename;
								
							move_uploaded_file($_FILES["file_dok".$key]["tmp_name"],
									"dokstandar/" . $dataimage);
							
						//	echo "ok";
							}else{
							$setupload=1;
							$dataimage="";
							}
							
					
					if($id_dok==""){
						if($fileupl==""){
							$dataupl=array();
						}else{
							
							$dataupl=array('file_upload' => 'dokstandar/'.$dataimage);
						}
						$dataupload=array_merge($datadetdua,$dataupl);
						$ceksatu=$this->standar_m->simpanDatadetail($dataupload);
						$id_detstan=mysql_insert_id();
					}else{
						if($fileupl==""){
							$dataupl=array();
						}else{
							
							$dataupl=array('file_upload' => 'dokstandar/'.$dataimage);
						}
						$dataupload=array_merge($datadetdua,$dataupl);
						$ceksatu=$this->standar_m->editDatadetail($dataupload,$id_dok);
						$id_detstan=$id_dok;
					}
					$idhapus[]="'".$id_detstan."'";
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
			 }
			 $databapus = implode(',',$idhapus);
		//	print_r($databapus);
		$this->standar_m->hapusDetail($iddet,$databapus);
		}
		
		
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.no_dokumen) like upper('%$search%')   or upper(a.nm_dokumen) like upper('%$search%')
				or upper(b.nm_dokumen) like upper('%$search%') ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->standar_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 
		 $this->standar_m->hapusDetaildata($id);
		 $ceksatu=$this->standar_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$this->standar_m->hapusDetaildata($data[$row]);
		$ceksatu=$this->standar_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function getDetail(){
	   $id = $this->input->get("id");

	   echo $this->standar_m->getDetail($id); 
   }
   
   
   function getCombo(){
	   $arrayMenu=array();
		$arauSatu =array();
		$arrayDua=array();
		$arrayTiga=array();
		$menusatu=$this->standar_m->getStandarutama(); 
		//print_r($menusatu);
		foreach($menusatu as $datasatu){ 
			$idmenu=$datasatu->id;
			$menudes=$datasatu->nm_dokumen;

			$arauSatu['id'] = $idmenu;
			$arauSatu['text'] = $menudes;
			 $arauSatu['children'] = array();
			 
			$dataarray=$this->standar_m->standarDetail($idmenu);
		//	echo count ($dataarray)."<br>";
			foreach($dataarray as $data){ 
			
			$iddua=$data->id;
			$textdua=$data->nm_dokumen;
			$arrayDua['id']=$iddua;
			$arrayDua['text']=$textdua;
			$arrayDua['children']=array();
			
			$dataarraytiga=$this->standar_m->standarDetail($iddua);
				foreach($dataarraytiga as $datatiga){ 
					$idtiga=$datatiga->id;
					$texttiga=$datatiga->nm_dokumen;
					$arrayTiga['id']=$idtiga;
					$arrayTiga['text']=$texttiga;
					//$arrayTiga['children']=1;
					array_push($arrayDua['children'],$arrayTiga);
				}
			
			
			
			
			array_push($arauSatu['children'],$arrayDua);
			
			}
			
			array_push($arrayMenu,$arauSatu);
		}
		
		
		
		 
		$jsonData = json_encode($arrayMenu);
		
		echo $jsonData; 
		
   }
   
   
   

}?>