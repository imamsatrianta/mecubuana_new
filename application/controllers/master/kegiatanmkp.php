<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class kegiatanmkp extends CI_Controller {

    function kegiatanmkp(){
		parent::__construct();
        $this->load->database();
		$this->load->model(array('master/kegiatanmkp_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
	   //echo "xx";exit;
    	$this->load->view('master/kegiatanmkp_v');
		$this->load->view('master/kegiatanmkp_js');
    	$this->load->view('bawah');
	}
	
	function simpanDatarefisi(){
		$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$setsimpan=0;
	$id = $this->input->post("id_ref");
	$dudate = $this->input->post("dudate");
	$ket_revisi = $this->input->post('ket_revisi');
	$id_unit = $this->input->post("id_unit_ref");
	$jenis = $this->input->post("jenis_ref");
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	$datarefisi=array(
		'id_keg_mkp'=>$id,
		'dudate'=>$dudate,
		'ket_revisi'=>$ket_revisi);
	$data=array(
		'dudate'=>$dudate,
		'ket_revisi'=>$ket_revisi,
		'status'=>'1'); 
	
	
	$cekrefisi=$this->kegiatanmkp_m->cekRefisi($id);
	//echo $cekrefisi;exit;
	if($cekrefisi>=3){
		$setsimpan=1;
	}else{
		$this->global_m->kirimEmailmkp($id_unit,$jenis,"revisi");
		
		$datasimpan=array_merge($datainput,$data);
		//print_r($datasimpan); exit;
		$ceksatu=$this->kegiatanmkp_m->editData($id,$datasimpan);
		
		$datasimpanref=array_merge($datainput,$datarefisi);
		//print_r($datasimpan); exit;
		$ceksatu=$this->kegiatanmkp_m->simpanDatarefisi($datasimpanref);
		if($ceksatu==1){
			$setsim="ok";	
		}else{
			$totalcek++;
		}
	
	}

	$status = $this->db->trans_status();
	if($setsimpan>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal, Data sudh di Refisi 3 Kali","status" => "error"));
	}else if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$setpersen=0;
	$id = $this->input->post("id");
	$set = $this->input->post("set");
	$id_mkp_target = $this->input->post('id_mkp_target');
	$kegiatan = $this->input->post('kegiatanrel');
	$waktu = $this->input->post('waktu');
	$waktuarray=array();
	foreach($waktu as $wkt){
		$waktuarray[]=$wkt;
	}
	$varwaktu = implode(',',$waktuarray);
	
	
	$id_level=$this->session->userdata('id_level');
	$id_unitses=$this->session->userdata('id_unit');
	$id_direktorat=$this->session->userdata('id_direktorat');
	$id_prodi=$this->session->userdata('id_prodi');
	if($id_level=="3"){
		$jenis=2;
		$id_unit=$id_direktorat;
		$dataleel="0";
	}else if($id_level=="4"){
		$jenis=1;
		$id_unit=$id_unitses;
		$dataleel="0";
	}else if($id_level=="5"){
		$jenis=3;
		$id_unit=$id_prodi;
		$dataleel="0";
	}else{
		$dataleel="1";
	}
//	echo 
	
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	if($dataleel=="0"){
		$data=array(
		'id_mkp_target'=>$id_mkp_target,
		'kegiatan'=>$kegiatan,
		'waktu'=>$varwaktu,
		'id_unit'=>$id_unit,
		'status'=>'0',
		'jenis'=>$jenis); 
	}else{
		$totalcek++;
		$data=array();
		/*$data=array(
		'id_mkp_target'=>$id_mkp_target,
		'kegiatan'=>$kegiatan,
		'waktu'=>$varwaktu,
		'status'=>'0'); */
	}
	
	//print_r($data);exit;	
	if($set==0){
		$datasimpan=array_merge($datainput,$data);
	//print_r($datasimpan); exit;
		$ceksatu=$this->kegiatanmkp_m->simpanData($datasimpan);
		$this->global_m->kirimEmailmkp($id_unit,$jenis,"input");
	}else{
		
		$datasimpan=array_merge($dataubah,$data);
		$ceksatu=$this->kegiatanmkp_m->editData($id,$datasimpan);
		$this->global_m->kirimEmailmkp($id_unit,$jenis,"edit");
	}
	
	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	

	$status = $this->db->trans_status();
	if($setpersen>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal, Presentase Lebih dari 100","status" => "error"));
	}else if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(b.uraian_target) like upper('%$search%')   or upper(b.target) like upper('%$search%')
				or upper(a.kegiatan) like upper('%$search%') or upper(a.waktu) like upper('%$search%')) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->kegiatanmkp_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->kegiatanmkp_m->hapusData($id);
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->kegiatanmkp_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function loaddatakegiatan(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.uraian_target) like upper('%$search%')   or upper(a.id) like upper('%$search%') ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
       $this->kegiatanmkp_m->loaddatakegiatan($offset,$limit,$order,$where); 
     
    }
   
   
   

}?>