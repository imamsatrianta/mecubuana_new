<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mkp extends CI_Controller {

    function Mkp()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('master/mkp_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
	   //echo "xx";exit;
    	$this->load->view('master/mkp_v');
		$this->load->view('master/mkp_js');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$nm_mkp = $this->input->post('nm_mkp');
	$id_tahunakademik = $this->input->post('id_tahunakademik');
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	
	$set = $this->input->post("set");
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	$data=array(
		'nm_mkp'=>$nm_mkp,
		'id_tahunakademik'=>$id_tahunakademik);
		
	if($set==0){
	$datasimpan=array_merge($datainput,$data);
	//print_r($datasimpan); exit;
	$ceksatu=$this->mkp_m->simpanData($datasimpan);
	$iddet=mysql_insert_id();
		
	}else{
		$datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->mkp_m->editData($id,$datasimpan);
		$iddet=$id;
		$this->mkp_m->hapusDetail($iddet);
	}
	
	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
		if (isset($_POST['detail'])) {
			$detail = $this->input->post('detail');
			 foreach ($detail as  $detaildata ) {
				 $datadetdua = array(
						'id_mkp' => $iddet,
						'id_standar' => trim($detaildata['id_standar']),
						'user_input' => $username,
						'tgl_input'=>$tgl
					);
					$ceksatu=$this->mkp_m->simpanDatadetail($datadetdua);
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
			 }
		}
			
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.nm_mkp) like upper('%$search%')   or upper(a.id) like upper('%$search%') ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->mkp_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->mkp_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->mkp_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function getDetail(){
	   $id = $this->input->get("id");

	   echo $this->mkp_m->getDetail($id); 
   }
   
   
   // mkp target
   function simpanDatatarget(){
	   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("idtarget");
	$set = $this->input->post('settarget');
	$id_mkp = $this->input->post('id_mkp');
	$uraian_target = $this->input->post('uraian_target');
	$setcek = $this->input->post('setcek');
	$target = $this->input->post('target');
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	
	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	$data=array(
		'id_mkp'=>$id_mkp,
		'uraian_target'=>$uraian_target,
		'cek'=>$setcek,
		'target'=>$target);
		
	if($set==0){
	$datasimpan=array_merge($datainput,$data);
	//print_r($datasimpan); exit;
	$ceksatu=$this->mkp_m->simpanDatatarget($datasimpan);
		
	}else{
		$datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->mkp_m->editDatatarget($id,$datasimpan);
	}
	
	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
			
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
   }
   
   function loaddataTabeltarget(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(b.nm_mkp) like upper('%$search%')   or upper(a.uraian_target) like upper('%$search%') ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->mkp_m->loaddataTabeltarget($offset,$limit,$order,$where); 
     
    } 
	function hapusdatatarget(){
		$id = $this->input->get("id");
		 $ceksatu=$this->mkp_m->hapusdatatarget($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	}
	
	function hapusDataarraytarget(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->mkp_m->hapusdatatarget($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function simpanDatakeg(){
	   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id_keg");
	$set = $this->input->post('setkeg');
	$id_mkp_target = $this->input->post('id_mkp_target');
	
	$kegiatan = $this->input->post('kegiatan');
	$waktu = $this->input->post('waktu');
	$id_unit = $this->input->post('id_unit');
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
//	print_r($id_unit);
	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	$waktuarray=array();
	foreach($waktu as $wkt){
		$waktuarray[]=$wkt;
		
	}
	$varwaktu = implode(',',$waktuarray);
	$data=array(
		'id_mkp_target'=>$id_mkp_target,
		'kegiatan'=>$kegiatan,
		'waktu'=>$varwaktu);		
	
	
	if($set==0){
		$datasimpan=array_merge($datainput,$data);
		//print_r($datasimpan); exit;
		$ceksatu=$this->mkp_m->simpanDakegiatan($datasimpan);
		$iddet=mysql_insert_id();
	}else{
		$datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->mkp_m->editDatakegiatan($id,$datasimpan);
		$iddet=$id;
		$this->mkp_m->hapusdaunit($id);
	}
	
	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
	if (isset($_POST['id_unit'])) {
			 foreach ($id_unit as  $unit ) {
				 //echo $unit;
				 $data = explode("-",$unit);
						$id_un=$data[0];
						$jenis=$data[1];
						//echo $id_un;
				 $datadetdua = array(
						'id_mkp_kegiatan' => trim($iddet),
						'id_unit' => trim($id_un),
						'jenis' => trim($jenis),
						'user_input' => $username,
						'tgl_input'=>$tgl
					);
					$this->mkp_m->simpanDataunit($datadetdua);
					
			 }
		}
				
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
   
   }
   function loaddatadetailkegiatan(){
	   $id = $this->input->get("id");
	   $this->mkp_m->loaddatadetailkegiatan($id); 
	   
   }
   
   function hapusdatakegiatan(){
	   $id = $this->input->get("id");
	   $this->mkp_m->hapusdatakegiatanunit($id);
		 $ceksatu=$this->mkp_m->hapusdatakegiatan($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
   }

}?>