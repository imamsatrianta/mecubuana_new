<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Renstra extends CI_Controller {

    function Renstra()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('master/renstra_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
	   //echo "xx";exit;
    	$this->load->view('master/renstra_v');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$nm_dokumen = $this->input->post('nm_dokumen');
	$tgl_awal = $this->input->post('tgl_awal');
	$tgl_akhir = $this->input->post('tgl_akhir');
	$set = $this->input->post("set");
	$tgl_publish = $this->input->post('tgl_publish');
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	$setgbr=$this->input->post('setgbr');
	
	if( $setgbr==1){	
	$allowedExts = array("pdf", "jpeg", "jpg", "png");
			$temp = explode(".", $_FILES["file_uploadda"]["name"]);
			$extension = end($temp);
			if ((($_FILES["file_uploadda"]["type"] == "image/pdf")
			|| ($_FILES["file_uploadda"]["type"] == "image/jpeg")
			|| ($_FILES["file_uploadda"]["type"] == "image/jpg")
			|| ($_FILES["file_uploadda"]["type"] == "image/pjpeg")
			|| ($_FILES["file_uploadda"]["type"] == "image/x-png")
			|| ($_FILES["file_uploadda"]["type"] == "image/png"))
			&& ($_FILES["file_uploadda"]["size"] < 200000000)
			&& in_array($extension, $allowedExts)) {
			//echo $extension."xxx";
			 $acak           = rand(000000,999999);
			$filename =$_FILES["file_uploadda"]["name"];
			$dataimage = $acak.$filename;
				
			move_uploaded_file($_FILES["file_uploadda"]["tmp_name"],
					"dokrenstra/" . $dataimage);
			
			//echo "ok";
			}else{
			$dataimage="";
			}
	}else{
			$dataimage="";
			}
			
	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	if($set==0){
		
		$data=array(
		'nm_dokumen'=>$nm_dokumen,
		'tgl_awal'=>$tgl_awal,
		'tgl_akhir'=>$tgl_akhir,
		'tgl_publish'=>$tgl_publish,
		'file_upload'=>'dokrenstra/'.$dataimage);
	$datasimpan=array_merge($datainput,$data);
	//print_r($datasimpan); exit;
	$ceksatu=$this->renstra_m->simpanData($datasimpan);
	$iddet=mysql_insert_id();
		
	}else{
		if($dataimage==""){
			$data=array(
		'nm_dokumen'=>$nm_dokumen,
		'tgl_awal'=>$tgl_awal,
		'tgl_akhir'=>$tgl_akhir,
		'tgl_publish'=>$tgl_publish);
			
		}else{
		$data=array(
		'nm_dokumen'=>$nm_dokumen,
		'tgl_awal'=>$tgl_awal,
		'tgl_akhir'=>$tgl_akhir,
		'tgl_publish'=>$tgl_publish,
		'file_upload'=>'dokrenstra/'.$dataimage);
		
		}
		$datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->renstra_m->editData($id,$datasimpan);
		$iddet=$id;
		$this->renstra_m->hapusDetail($iddet);
	}
	
	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
		if (isset($_POST['detail'])) {
			$detail = $this->input->post('detail');
			 foreach ($detail as  $detaildata ) {
				 $datadetdua = array(
						'id_renstra' => $iddet,
						'nomor' => trim($detaildata['nomor']),
						'uraian' => trim($detaildata['uraian']),
						'target' => trim($detaildata['target']),
						'tahun_awal' => trim($detaildata['tahun_awal']),
						'tahun_tengah' => trim($detaildata['tahun_tengah']),
						'tahun_akhir' => trim($detaildata['tahun_akhir']),
						'user_input' => $username,
						'tgl_input'=>$tgl
					);
					$ceksatu=$this->renstra_m->simpanDatadetail($datadetdua);
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
			 }
		}
			
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.nm_dokumen) like upper('%$search%')   or upper(a.file_upload) like upper('%$search%') ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->renstra_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->renstra_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->renstra_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function getDetail(){
	   $id = $this->input->get("id");

	   echo $this->renstra_m->getDetail($id); 
   }
   
   
   
   

}?>