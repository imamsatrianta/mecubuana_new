<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sop extends CI_Controller {

    function Sop()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('master/sop_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
	   //echo "xx";exit;
    	$this->load->view('master/sop_v');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$nm_sop = $this->input->post('nm_sop');
	$id_standar = $this->input->post('id_standar');
	$set = $this->input->post("set");
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	$setgbr=$this->input->post('setgbr');
	
	if( $setgbr==1){	
	$allowedExts = array("pdf", "jpeg", "jpg", "png");
			$temp = explode(".", $_FILES["file_uploadda"]["name"]);
			$extension = end($temp);
			if ((($_FILES["file_uploadda"]["type"] == "image/pdf")
			|| ($_FILES["file_uploadda"]["type"] == "image/jpeg")
			|| ($_FILES["file_uploadda"]["type"] == "image/jpg")
			|| ($_FILES["file_uploadda"]["type"] == "image/pjpeg")
			|| ($_FILES["file_uploadda"]["type"] == "image/x-png")
			|| ($_FILES["file_uploadda"]["type"] == "image/png"))
			&& ($_FILES["file_uploadda"]["size"] < 200000000)
			&& in_array($extension, $allowedExts)) {
			//echo $extension."xxx";
			 $acak           = rand(000000,999999);
			$filename =$_FILES["file_uploadda"]["name"];
			$dataimage = $acak.$filename;
				
			move_uploaded_file($_FILES["file_uploadda"]["tmp_name"],
					"doksop/" . $dataimage);
			
			//echo "ok";
			}else{
			$dataimage="";
			}
	}else{
			$dataimage="";
			}
			
	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	if($set==0){
		
		$data=array(
		'nm_sop'=>$nm_sop,
		'id_standar'=>$id_standar,
		'file_upload'=>'doksop/'.$dataimage);
	$datasimpan=array_merge($datainput,$data);
	//print_r($datasimpan); exit;
	$ceksatu=$this->sop_m->simpanData($datasimpan);
		
	}else{
		if($dataimage==""){
			$data=array(
		'nm_sop'=>$nm_sop,
		'id_standar'=>$id_standar);
			
		}else{
		$data=array(
		'nm_sop'=>$nm_sop,
		'id_standar'=>$id_standar,
		'file_upload'=>'doksop/'.$dataimage);
		
		}
		$datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->sop_m->editData($id,$datasimpan);
	}
	
	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.nm_sop) like upper('%$search%') ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->sop_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->sop_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->sop_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
  
   
   

}?>