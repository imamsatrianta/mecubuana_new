<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class penilaianborang extends CI_Controller {

    function penilaianborang()
	{
		parent::__construct();
		$this->load->model(array('borang/penilaianborang_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		$this->load->view('atas_v');
	   //echo "xx";exit;
		$this->load->view('borang/penilaianborang_v');
    	$this->load->view('bawah');
		
		
	}
   function download_penilaian(){
	   $id_jenjangprodi=$_GET['id_jenjangprodi'];
	   $id_tahunakademik=$_GET['id_tahunakademik'];
	   $id_prodi=$_GET['id_prodi'];
	   $standarsatu=$this->penilaianborang_m->getParametersatu($id_jenjangprodi,$id_tahunakademik,$id_prodi);
	   $satu=0;
	   $dua=0;
	   $tiga=0;
	   foreach($standarsatu as $prm){
				$id=$prm->id;
				$nilai=$prm->nilai;
				$poin=$prm->poin;
				if($id=="25"){
					$satu=$nilai;
				}
				if($id=="29"){
					$dua=$nilai;
				}
				if($id=="30"){
					$tiga=$nilai;
				}
	   }
	   
	   $setduasatu=0;
	   $setduadua=0;
	   $setduatiga=0;
	   $setduaempat=0;
	   $setdualima=0;
	   $setduaenam=0;
	   $$tigasatudua=0;
	   $standardua=$this->penilaianborang_m->getParameterdua($id_jenjangprodi,$id_tahunakademik,$id_prodi);
	    foreach($standardua as $setdua){
				$id=$setdua->id;
				$nilai=$setdua->nilai;
				if($id=="31"){
					$setduasatu=$nilai;
				}
				if($id=="32"){
					$setduadua=$nilai;
				}
				if($id=="33"){
					$setduatiga=$nilai;
				}
				if($id=="34"){
					$setduaempat=$nilai;
				}
				if($id=="35"){
					$setdualima=$nilai;
				}
				if($id=="36"){
					$setduaenam=$nilai;
				}
				if($id=="39"){
					$tigasatudua=$nilai;
				}
				
				
		}
		
		$tigasatusatu=$this->penilaianborang_m->getStandartigasatu($id_jenjangprodi,$id_tahunakademik,$id_prodi);
		 $daya_tampung=0;
		 $mhs_ikut_seleksi=0;
		 $mhs_lulus_seleksi=0;
		 $jml_reg_bkn_transfer=0;
		 $ipk_lulusan_reg_rata=0;
		 foreach($tigasatusatu as $tigasatu){
			 $daya_tampung=$tigasatu->daya_tampung;
			 $mhs_ikut_seleksi=$tigasatu->mhs_ikut_seleksi;
			 $mhs_lulus_seleksi=$tigasatu->mhs_lulus_seleksi;
			 $jml_reg_bkn_transfer=$tigasatu->jml_reg_bkn_transfer;
			 $ipk_lulusan_reg_rata=$tigasatu->ipk_lulusan_reg_rata;
		 }
		 
		// $tigasatusatu=$this->penilaianborang_m->getStandartigasatudua($id_jenjangprodi,$id_tahunakademik,$id_prodi);
	   //print_r($parameter);
	  // exit;
	   include_once (APPPATH . "third_party/PHPExcel/Classes/PHPExcel.php");
	    $objPHPExcel = new PHPExcel();
		
		 $inputFileName = 'download/D3';
		
	   $excel2 = PHPExcel_IOFactory::createReader('Excel5');
		$excel2 = $excel2->load($inputFileName . '.xls');
		$excel2->setActiveSheetIndex(0);
	
		$baris=4;
	 $work_sheet_count=12;
	 $work_sheet=0;
	 while($work_sheet<=$work_sheet_count){ 
		 if($work_sheet==1){
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E4', $satu);
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E7', $dua);
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E10', $tiga);
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E13', $setduasatu);
			  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E16', $setduadua);
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E19', $setduatiga);
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E22', $setduaempat);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E25', $setdualima);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E28', $setduaenam);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E32', $mhs_ikut_seleksi);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E33', $daya_tampung);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E38', $jml_reg_bkn_transfer);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E50', $mhs_lulus_seleksi);
			// belum
			
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E39', $ipk_lulusan_reg_rata);
			
		 }

		 $work_sheet++;
	 }
	
	
   $writer = PHPExcel_IOFactory::createWriter($excel2, 'Excel2007');
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="penilaiand3.xls"');
	header('Cache-Control: max-age=0');
	ob_end_clean();
	$writer->save('php://output');
	exit;
   }
   
   
   
   
   
   
}?>