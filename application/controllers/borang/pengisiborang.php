<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pengisiborang extends CI_Controller {

    function pengisiborang()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('borang/pengisiborang_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
	   $this->load->view('atas_v');
		$this->load->view('borang/pengisiborang_v');
		$this->load->view('borang/pengisiborang_js');
    	$this->load->view('bawah');
	}
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(b.nm_jenjangprodi) like upper('%$search%')   or upper(c.nm_tahun_akademik) like upper('%$search%')
			or upper(d.nm_prodi) like upper('%$search%') or upper(a.uraian) like upper('%$search%')  ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->pengisiborang_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	
	function simpanData(){
	$iddet="";
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$setsmpan=0;
	$id = $this->input->post("id");
	$set = $this->input->post("set");
	$id_tahunakademik= $this->input->post("id_tahunakademik");
	$id_jenjangprodi = $this->input->post('id_jenjangprodi');
	$id_prodiarr = $this->input->post('id_prodi');
	$prodi=explode('-',$id_prodiarr);
	$id_prodi=$prodi[0]; 
	$kd_server=$prodi[1]; 
	$tanggal=$this->input->post('tanggal');
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');

	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	$data=array(
		'id_tahunakademik'=>$id_tahunakademik,
		'id_jenjangprodi'=>$id_jenjangprodi,
		'id_prodi'=>$id_prodi,
		'tanggal'=>$tanggal);
	$ceksatu="";	
	if($set==0){
	$datasimpan=array_merge($datainput,$data);
	$cekborang=$this->pengisiborang_m->cekBorang($id_tahunakademik,$id_jenjangprodi,$id_prodi);
	if (!empty($cekborang)) {
		$setsmpan++;
	}else{
	//print_r($datasimpan); exit;
	$idinsert=$this->pengisiborang_m->simpanData($datasimpan);
	$iddet=$idinsert;
	}
		
	}else{
		
		$datasimpan=array_merge($dataubah,$data);
		$ceksatu=$this->pengisiborang_m->editData($id,$datasimpan);
		$iddet=$id;
		$this->pengisiborang_m->hapusDetail($id);
	}
	
	
	

	
	if (isset($_POST['detail'])) {
			$detail = $this->input->post('detail');
			 foreach ($detail as  $detaildata ) {
				 $datadetdua = array(
						'id_pengisi' => $iddet,
						'id_karyawan' => trim($detaildata['id_karyawan']),
						'tgl_penisian'=>trim($detaildata['tgl_penisian'])
					);
					$datasimpan=array_merge($datadetdua,$datainput);
					$ceksatu=$this->pengisiborang_m->simpanDatadetail($datasimpan);
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
			 }
		}
		
		
	
	
	$status = $this->db->trans_status();
	if($setsmpan>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal, Data Sudah Ada","status" => "error"));
	}else if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
   
   function hapusData(){
	    $id = $this->input->get("id");
		$this->pengisiborang_m->hapusDetail($id);
		 $ceksatu=$this->pengisiborang_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$this->pengisiborang_m->hapusDetail($data[$row]);
		$ceksatu=$this->pengisiborang_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function getEditdetail(){
	   $id = $this->input->get("id");
	   echo $this->pengisiborang_m->getEditdetail($id); 
   }
  
  
   
   
}?>