<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class lapborang extends CI_Controller {

    function lapborang()
	{
		parent::__construct();
		$this->load->model(array('borang/lapborang_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		$this->load->view('atas_v');
	   //echo "xx";exit;
		$this->load->view('borang/lapborang_v');
    	$this->load->view('bawah');
		
		
	}
   function download_laporan(){
	   $id_jenjangprodi=$_GET['id_jenjangprodi'];
	   $id_tahunakademik=$_GET['id_tahunakademik'];
	   $id_prodi=$_GET['id_prodi'];
	   $standarsatu=$this->lapborang_m->getIdentitas($id_jenjangprodi,$id_prodi);
	   $prodi="";
	   $jurusan="";
	   $fakultas="";
	   $perguruantinggi="Mercubuana";
	   $nosk="";
	   $tglsk="";
	   $pejabatppd="";
	   $blntahunpen="";
	   $thnpen="";
	   $noskijin="";
	   $tglskijin="";
	   $peringkat="";
	   $nilai="";
	   $noskbanpt="";
	   $alamat="";
	   $notelp="";
	   $nofak="";
	   $homepagps="";
	   $emailps=""; 
	   foreach($standarsatu as $prm){
				$prodi=$prm->nm_prodi;
				$jurusan=$prm->nm_jenjangprodi;
				$fakultas=$prm->fskultas; 
				$perguruantinggi=$prm->nm_perguruantingi;
				$nosk=$prm->no_sk_pendirian;
				$tglsk=$prm->tgl_sk_pendirian;
				$pejabatppd=$prm->pejabat_penandatangan_sk;
				$blntahunpen=$prm->bulan_tahun_peneyelengara;
				$thnpen=$prm->tahun_peneyelengara;
				$noskijin=$prm->no_skijin_op;
				$tglskijin=$prm->tgl_sk_op;
				$peringkat=$prm->peringkat_ak_terakhir;
				$nilai=$prm->tgl_ak_terakhir;
				$noskbanpt=$prm->no_sk_banpt;
				$alamat=$prm->alamat_ps;
				$notelp=$prm->no_telp_ps;
				$nofak=$prm->no_fak_ps;
				$homepagps=$prm->homepag;
				$emailps=$prm->hp_dan_email_ps;
				
	   }
	   
	   
	   //print_r($parameter);
	  // exit;
	   include_once (APPPATH . "third_party/PHPExcel/Classes/PHPExcel.php");
	    $objPHPExcel = new PHPExcel();
		
		 $inputFileName = 'download/apdd3';
		
	   $excel2 = PHPExcel_IOFactory::createReader('Excel5');
		$excel2 = $excel2->load($inputFileName . '.xls');
		$excel2->setActiveSheetIndex(0);
	
		$baris=4;
	 $work_sheet_count=50;
	 $work_sheet=0;
	 while($work_sheet<=$work_sheet_count){ 
		 if($work_sheet==2){
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('D5', $prodi);
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('D6', $jurusan);
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('D7', $fakultas);
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('D8', $perguruantinggi);
			  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('D10', $nosk);
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('D11', $tglsk);
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('D12', $pejabatppd);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('D14', $blntahunpen);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E14', $thnpen);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('D17', $noskijin);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('D18', $tglskijin);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('D21', $peringkat);
			
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('D22', $nilai);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('D23', $noskbanpt);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('D25', $alamat);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('D30', $notelp);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('D32', $nofak);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('D34', $homepagps);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('D36', $emailps);
		 }
		
		 if($work_sheet==3){
			  $datadosen=$this->lapborang_m->getIdentitasdosen($id_jenjangprodi,$id_prodi);
			  $sitdosen=6;
			  $nodeosen=0;
			  foreach($datadosen as $dosen){
				  $sitdosen++;
				  $nodeosen++;
				  $nm_karyawan=$dosen->nm_karyawan;
				  $nidn=$dosen->nidn;
				  $tgl_lahir=$dosen->tgl_lahir;
				  $jabatan_akademik=$dosen->jabatan_akademik;
				  $jenjang_prodi=$dosen->jenjang_prodi;
				  $gelar_akademim=$dosen->gelar_akademim;
				  $asal_ps=$dosen->asal_ps;
				  $bidang_keahlian=$dosen->bidang_keahlian;
				  
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('A'.$sitdosen, $nodeosen);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('B'.$sitdosen, $nm_karyawan);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('C'.$sitdosen, $nidn);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('D'.$sitdosen, $tgl_lahir);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E'.$sitdosen, $jabatan_akademik);
				  
				  if($jenjang_prodi=="S1"){
					  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('F'.$sitdosen, $gelar_akademim);
					  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('G'.$sitdosen, $asal_ps);
					  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('H'.$sitdosen, $bidang_keahlian);
				  }
				   if($jenjang_prodi=="S2"){
					  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('I'.$sitdosen, $gelar_akademim);
					  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('J'.$sitdosen, $asal_ps);
					  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('K'.$sitdosen, $bidang_keahlian);
				  }
				   if($jenjang_prodi=="S3"){
					  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('L'.$sitdosen, $gelar_akademim);
					  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('M'.$sitdosen, $asal_ps);
					  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('N'.$sitdosen, $bidang_keahlian);
				  }
				  
			  }
		 }
		 
		 
		 if($work_sheet==1){
			  $datadosen=$this->lapborang_m->getPengisiborang($id_jenjangprodi,$id_prodi,$id_tahunakademik);
			  $sitdosen=6;
			  $nodeosen=0;
			  foreach($datadosen as $dosen){
				  $sitdosen++;
				  $nodeosen++;
				  $nm_karyawan=$dosen->nm_karyawan;
				  $nidn=$dosen->nidn;
				  $tgl_penisian=$dosen->tgl_penisian;
				  $nm_jabatan=$dosen->nm_jabatan;
				  
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('A'.$sitdosen, $nodeosen);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('B'.$sitdosen, $nm_karyawan);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('C'.$sitdosen, $nidn);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('D'.$sitdosen, $tgl_penisian);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E'.$sitdosen, $nm_jabatan);
				  
				 
				  
			  }
		 }
		 
		
		 
		 
		 // 3.1.1
		 if($work_sheet==4){
			  $datadosen=$this->lapborang_m->getBorang_tiga($id_jenjangprodi,$id_prodi,$id_tahunakademik,38,'br_d3_tiga_satu_satu');
			  $sitdosen=8;
			  foreach($datadosen as $dosen){
				  $sitdosen++;
				  $daya_tampung=$dosen->daya_tampung;
				  $mhs_ikut_seleksi=$dosen->mhs_ikut_seleksi;
				  $mhs_lulus_seleksi=$dosen->mhs_lulus_seleksi;
				  $jml_reg_bkn_transfer=$dosen->jml_reg_bkn_transfer;
				  
				  $jml_transfer=$dosen->jml_transfer;
				  $total_reg_bkn_transfer=$dosen->total_reg_bkn_transfer;
				  $total_transfer=$dosen->total_transfer;
				$lulus_reg_bkn_transfer=$dosen->lulus_reg_bkn_transfer;
					$lulus_transfer=$dosen->lulus_transfer;
					$ipk_lulusan_reg_min=$dosen->ipk_lulusan_reg_min;
					$ipk_lulusan_reg_rata=$dosen->ipk_lulusan_reg_rata;
					$ipk_lulusan_reg_mak=$dosen->ipk_lulusan_reg_mak;
					$ipk_kurang=$dosen->ipk_kurang;
					$ipk=$dosen->ipk;
					$ipk_lebih=$dosen->ipk_lebih;
				  
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('C'.$sitdosen, $daya_tampung);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('D'.$sitdosen, $mhs_ikut_seleksi);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E'.$sitdosen, $mhs_lulus_seleksi);
				  
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('F'.$sitdosen, $jml_reg_bkn_transfer);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('G'.$sitdosen, $jml_transfer);
				  
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('H'.$sitdosen, $total_reg_bkn_transfer);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('I'.$sitdosen, $total_transfer);
				  
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('J'.$sitdosen, $lulus_reg_bkn_transfer);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('K'.$sitdosen, $lulus_transfer);
				  
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('L'.$sitdosen, $ipk_lulusan_reg_min);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('M'.$sitdosen, $ipk_lulusan_reg_rata);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('N'.$sitdosen, $ipk_lulusan_reg_mak);
				  
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('O'.$sitdosen, $ipk_kurang);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('P'.$sitdosen, $ipk);
				  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('Q'.$sitdosen, $ipk_lebih);
				  
				 
				  
			  }
		 }
		 
		  
		  
		 // 3.3.3 
		 
		//echo $work_sheet;
		 if($work_sheet==5){
			  $tigatigaa=$this->lapborang_m->getBorang_tiga($id_jenjangprodi,$id_prodi,$id_tahunakademik,40,'br_d3_tiga_satu_tiga_a');
			 // print_r($tigatigaa); exit;
			  $sitdosen=11;
			  foreach($tigatigaa as $tigatiga){
				  $sitdosen++;
				  $ts_enam=$tigatiga->ts_enam;
				  $ts_lima=$tigatiga->ts_lima;
				  $ts_empat=$tigatiga->ts_empat;
				  $ts_tiga=$tigatiga->ts_tiga;
				  $ts_dua=$tigatiga->ts_dua;
				  $ts_satu=$tigatiga->ts_satu;
				  $ts=$tigatiga->ts;
				  $ts_jumlah=$tigatiga->ts_jumlah;
				  if($ts_enam!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('C'.$sitdosen,$ts_enam);  
				  }
				   if($ts_lima!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('D'.$sitdosen, $ts_lima);  
				  }
				   if($ts_empat!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E'.$sitdosen,$ts_empat);  
				  }
				   if($ts_tiga!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('F'.$sitdosen, $ts_tiga); 
				  }
				   if($ts_dua!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('G'.$sitdosen,$ts_dua);  
				  }
				   if($ts_satu!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('H'.$sitdosen, $ts_satu);  
				  }
				   if($ts!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('I'.$sitdosen,$ts);  
				  }
				   if($ts_jumlah!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('J'.$sitdosen,$ts_jumlah);  
				  }
				 
				  
			  }
			  
			   $tigatigab=$this->lapborang_m->getBorang_tiga($id_jenjangprodi,$id_prodi,$id_tahunakademik,41,'br_d3_tiga_satu_tiga_b');
			 // print_r($tigatigaa); exit;
			  $sitdosenb=26;
			  foreach($tigatigab as $tigatigab){
				  $sitdosenb++;
				  $ts_empat=$tigatigab->ts_empat;
				  $ts_tiga=$tigatigab->ts_tiga;
				  $ts_dua=$tigatigab->ts_dua;
				  $ts_satu=$tigatigab->ts_satu;
				  $ts=$tigatigab->ts;
				  $ts_jumlah=$tigatigab->ts_jumlah;
				 
				   if($ts_empat!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('C'.$sitdosenb,$ts_empat);  
				  }
				   if($ts_tiga!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('D'.$sitdosenb, $ts_tiga); 
				  }
				   if($ts_dua!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E'.$sitdosenb,$ts_dua);  
				  }
				   if($ts_satu!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('F'.$sitdosenb, $ts_satu);  
				  }
				   if($ts!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('G'.$sitdosenb,$ts);  
				  }
				   if($ts_jumlah!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('H'.$sitdosenb,$ts_jumlah);  
				  }
				 
				  
			  }
			  
			  $tigatigac=$this->lapborang_m->getBorang_tiga($id_jenjangprodi,$id_prodi,$id_tahunakademik,42,'br_d3_tiga_satu_tiga_c');
			  $sitdosenc=39;
			  foreach($tigatigac as $tigatigac){
				  $sitdosenc++;
				  $ts_dua=$tigatigac->ts_dua;
				  $ts_satu=$tigatigac->ts_satu;
				  $ts=$tigatigac->ts;
				  $ts_jumlah=$tigatigac->ts_jumlah;
				 
				   if($ts_dua!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('C'.$sitdosenc,$ts_dua);  
				  }
				   if($ts_satu!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('D'.$sitdosenc, $ts_satu);  
				  }
				   if($ts!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E'.$sitdosenc,$ts);  
				  }
				   if($ts_jumlah!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('F'.$sitdosenc,$ts_jumlah);  
				  }
			  }
			  
			  $tigatigad=$this->lapborang_m->getBorang_tiga($id_jenjangprodi,$id_prodi,$id_tahunakademik,43,'br_d3_tiga_satu_tiga_d');
			  $sitdosend=50;
			  foreach($tigatigad as $tigatigad){
				  $sitdosend++;
				  $ts_satu=$tigatigad->ts_satu;
				  $ts=$tigatigad->ts;
				  $ts_jumlah=$tigatigad->ts_jumlah;
				 
				   if($ts_satu!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('C'.$sitdosend, $ts_satu);  
				  }
				   if($ts!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('D'.$sitdosend,$ts);  
				  }
				   if($ts_jumlah!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E'.$sitdosend,$ts_jumlah);  
				  }
			  }
			  
			  
			  
		 }
		 // 3.4.1
		  if($work_sheet==6){
			   $tigaempat=$this->lapborang_m->getBorang_tiga($id_jenjangprodi,$id_prodi,$id_tahunakademik,48,'br_d3_tiga_empat_satu');
			  $barisempat=9;
			  foreach($tigaempat as $tigaempat){
				  $barisempat++;
				  $sangat_baik=$tigaempat->sangat_baik;
				  $baik=$tigaempat->baik;
				  $cukup=$tigaempat->cukup;
				  $kurang=$tigaempat->kurang;
				  $rencana_tindaklanjut=$tigaempat->rencana_tindaklanjut;
				 
				   if($sangat_baik!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E'.$barisempat, $sangat_baik);  
				  }
				   if($baik!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('F'.$barisempat,$baik);  
				  }
				   if($cukup!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('G'.$barisempat,$cukup);  
				  }
				  if($kurang!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('H'.$barisempat,$kurang);  
				  }
				  if($rencana_tindaklanjut!=0){
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('I'.$barisempat,$rencana_tindaklanjut);  
				  }
			  }
		  }
		  
		  // 3.4.5
		 if($work_sheet==7){
			   $tigaempatlima=$this->lapborang_m->getBorang_tiga($id_jenjangprodi,$id_prodi,$id_tahunakademik,52,'br_d3_tiga_empat_lima');
			  $barisempatlima=7;
			  foreach($tigaempatlima as $tigaempatlima){
				  $barisempatlima++;
				  $jml_lulusan_diwisuda=$tigaempatlima->jml_lulusan_diwisuda;
				  $nm_lembaga=$tigaempatlima->nm_lembaga;
				  $jml_lulusan_pesanterima=$tigaempatlima->jml_lulusan_pesanterima;
				  $jumlah_lulusanditerima=$tigaempatlima->jumlah_lulusanditerima;
				 
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('C'.$barisempatlima, $jml_lulusan_diwisuda);  
				 
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('D'.$barisempatlima,$nm_lembaga);  
				 
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E'.$barisempatlima,$jml_lulusan_pesanterima);  
				 
					$excel2->setActiveSheetIndex($work_sheet)->setCellValue('F'.$barisempatlima,$jumlah_lulusanditerima);  
				  
			  }
		  }
		  
		 
		 //exit;
		 $work_sheet++;
	 }
	
	
   $writer = PHPExcel_IOFactory::createWriter($excel2, 'Excel2007');
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="APDD3.xls"');
	header('Cache-Control: max-age=0');
	ob_end_clean();
	$writer->save('php://output');
	exit;
   }
   
   
   
   
   
   
}?>