<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Standarsatu extends CI_Controller {

    function Standarsatu()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('borang/standarsatu_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		$level=$_GET['level'];
		$data['idm']=$_GET['idm'];
		$data['level']=$_GET['level'];
		$data['menu']=$this->standarsatu_m->loadDatamenu($level);
	   $this->load->view('atas_v');
	   //echo "xx";exit;
		$this->load->view('borang/standarsatumenu_v',$data);
		if (isset($_GET["jenis"])) {
			$this->load->view('borang/standarsatu_v');
			$this->load->view('borang/standarsatu_js');
		}else{  
			$this->load->view('borang/standarsatuawal_v');
		}
		
		
    	
    	$this->load->view('bawah');
	}
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
	   $parameter= $this->input->get("parameter");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(b.nm_jenjangprodi) like upper('%$search%')   or upper(c.nm_tahun_akademik) like upper('%$search%')
			or upper(d.nm_prodi) like upper('%$search%') or upper(a.uraian) like upper('%$search%')  ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->standarsatu_m->loaddataTabel($offset,$limit,$order,$where,$parameter); 
     
    } 
	
	function simpanData(){
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$setsmpan=0;
	$id = $this->input->post("id");
	$set = $this->input->post("set");
	$id_tahunakademik= $this->input->post("id_tahunakademik");
	$id_parameter = $this->input->post('id_parameter');
	$id_jenjangprodi = $this->input->post('id_jenjangprodi');
	$id_prodi = $this->input->post('id_prodi');
	$tanggal=$this->input->post('tanggal');
	$uraian = $this->input->post('uraiandata');
	$nilai = $this->input->post('nilai');
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');

	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	$data=array(
		'id_tahunakademik'=>$id_tahunakademik,
		'id_parameter'=>$id_parameter,
		'id_jenjangprodi'=>$id_jenjangprodi,
		'id_prodi'=>$id_prodi,
		'tanggal'=>$tanggal,
		'uraian'=>$uraian,
		'nilai'=>$nilai);
	$ceksatu="";	
	if($set==0){
	$datasimpan=array_merge($datainput,$data);
	$cekborang=$this->standarsatu_m->cekBorang($id_tahunakademik,$id_parameter,$id_jenjangprodi,$id_prodi);
	if (!empty($cekborang)) {
		$setsmpan++;
	}else{
	//print_r($datasimpan); exit;
	$ceksatu=$this->standarsatu_m->simpanData($datasimpan);
	}
		
	}else{
		
		$datasimpan=array_merge($dataubah,$data);
		$ceksatu=$this->standarsatu_m->editData($id,$datasimpan);
	}
	
	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
	$status = $this->db->trans_status();
	if($setsmpan>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal, Data Sudah Ada","status" => "error"));
	}else if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
   
   function hapusData(){
	    $id = $this->input->get("id");
		$this->standarsatu_m->hapusKebijakanheder($id);
		 $ceksatu=$this->standarsatu_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$this->standarsatu_m->hapusKebijakanheder($data[$row]);
		$ceksatu=$this->standarsatu_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function simpanDatakebijakan(){
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id_borang = $this->input->post("id_borang");
	$setsit = $this->input->post("setsit");
	$id_keb = $this->input->post("id_keb");
	
	$kebijakan = $this->input->post('kebijakan');
	$prosedur = $this->input->post('prosedur'); 
	$imgdata = $this->input->post('imgdata'); 

	
	$setgbr=$this->input->post('setgbr');
	$ceksatu="";
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	
	
	if( $setgbr==1){	
	$allowedExts = array("jpeg", "jpg", "pdf");
			$temp = explode(".", $_FILES["imgdata"]["name"]);
			$extension = end($temp);
			if ((($_FILES["imgdata"]["type"] == "image/jpeg")
			|| ($_FILES["imgdata"]["type"] == "image/jpg")
			|| ($_FILES["imgdata"]["type"] == "image/pjpeg")
			|| ($_FILES["imgdata"]["type"] == "application/pdf"))
			&& ($_FILES["imgdata"]["size"] < 200000000)
			&& in_array($extension, $allowedExts)) {
			//echo $extension."xxx";
			 $acak           = rand(000000,999999);
			$filename =$_FILES["imgdata"]["name"];
			$dataimage = $acak.$filename;
				
			move_uploaded_file($_FILES["imgdata"]["tmp_name"],
					"kebijakanborang/" . $dataimage);
			
			//echo "ok";
			}else{
			$dataimage="";
			}
	}else{
			$dataimage="";
			}		
			
			
	if($setsit==0){
		$data=array(
		'kebijakan'=>$kebijakan,
		'id_borangsatu'=>$id_borang,
		'lampiran'=>$dataimage,
		'prosedur'=>$prosedur);
		$dataok=array_merge($datainput,$data);
		$ceksatu=$this->standarsatu_m->simpanDatakeb($dataok);
		
		
	}else{
		if($dataimage==""){
			$data=array(
			'kebijakan'=>$kebijakan,
			'id_borangsatu'=>$id_borang,
			'prosedur'=>$prosedur);
			
		}else{
			$data=array(
			'kebijakan'=>$kebijakan,
			'id_borangsatu'=>$id_borang,
			'lampiran'=>$dataimage,
			'prosedur'=>$prosedur);
		}
		$dataok=array_merge($dataubah,$data);
		$ceksatu=$this->standarsatu_m->editDatakeb($id_keb,$dataok);
	}	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	  
  }
   function loaddatakebijakan(){
	   $id = $this->input->get("id");
	   $this->standarsatu_m->loaddatakebijakan($id); 
   }

   function hapusKebijakan(){
	   $id = $this->input->get("id");
		 $ceksatu=$this->standarsatu_m->hapusKebijakan($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
   }
}?>