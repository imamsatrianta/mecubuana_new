<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class standarevaluasi extends CI_Controller {

    function standarevaluasi()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('borang/standarevaluasi_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
	   $parameter= $this->input->get("parameter"); 
		$namatabel= $this->input->get("namatabel");
		$standar= $this->input->get("standar");
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(b.nm_jenjangprodi) like upper('%$search%')   or upper(c.nm_tahun_akademik) like upper('%$search%')
			or upper(d.nm_prodi) like upper('%$search%') or upper(a.uraian) like upper('%$search%')  ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->standarevaluasi_m->loaddataTabel($offset,$limit,$order,$where,$parameter,$namatabel,$standar); 
     
    } 
	
	
   
   // evaluasi borang
    function simpanDatareevaluasi(){
		
	$this->db->trans_begin();
	$status=true;
	$totalcek=0; 
	$setsimpan=0;
	$id_tahunakademikevl = $this->input->post("id_tahunakademikevl");
	$id_jenjangprodievl = $this->input->post("id_jenjangprodievl"); 
	$id_prodievl = $this->input->post("id_prodievl");
	$not_evaluasi = $this->input->post('not_evaluasi');
	$status_evaluasi = $this->input->post('status_evaluasi');
	$tgl_dodate= $this->input->post('tgl_dodate');
	$username=$this->session->userdata('username');
	$standar= $this->input->post('standar');
	$tgl=date('Y-m-d');
	
	$dudatedekan=date('Y-m-d', strtotime($tgl. ' + 7 days'));
	
	$levelprodi=$this->session->userdata('levelprodi');
	if($tgl_dodate==""){
		if($levelprodi=="1"){
			$tgl_dodateok='';
			$status_evaluasi="2";
		}else{
			$tgl_dodateok=$dudatedekan;
			$status_evaluasi="1";
		}
		
	}else{
		$tgl_dodateok=$tgl_dodate;
		$status_evaluasi="0";
	}
	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	$dataevaluasi=array(
		'id_tahunakademik'=>$id_tahunakademikevl,
		'id_jenjangprodi'=>$id_jenjangprodievl,
		'id_prodi'=>$id_prodievl,
		'standar'=>$standar,
		'tgl_dodate'=>$tgl_dodateok,
		'note_evaluasi'=>$not_evaluasi,
		'status_evaluasi'=>$status_evaluasi,
		'user_kirim'=>$username,
		'tgl_kirim'=>$tgl);
		
	$data=array(
		'status_data'=>0);
	
		//print_r($datasimpan); exit;
		
		$table="br_standarsatu";
		$ceksatu=$this->standarevaluasi_m->editData($id_tahunakademikevl,$id_jenjangprodievl,$id_prodievl,$data,$standar);
		
		//print_r($datasimpan); exit;
		$ceksatu=$this->standarevaluasi_m->simpanDatarevaluasi($dataevaluasi);
		
		/*
		$jenis=$status;
		$this->global_m->kirimEmailsarmut($id_prodi,"revisi",$jenis);
		$this->global_m->kirimEmailsarmutfakultas($id_prodi,"input");
		*/
		
		
		if($ceksatu==1){
			$setsim="ok";	
		}else{
			$totalcek++;
		}
	
	

	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
   }
   function cetakBorang(){
	   
	   $id_jenjangprodi=$_GET['id_jenjangprodi'];
	   $id_tahunakademik=$_GET['id_tahunakademik'];
	   $id_prodi=$_GET['id_prodi']; 
	   $tabel=$_GET['tabel']; 
	   $standar=$_GET['standar']; 
	   
	   if($standar=="1"){
		    $data['databorang']=$this->standarevaluasi_m->getBorang($id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel,$standar);
			   foreach($data['databorang'] as $std){
				$id=$std->id;
				$data['datauraian'][$id]=$this->standarevaluasi_m->getIsiborang($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel);
				
				}
			 $data['identitasborang']=$this->standarevaluasi_m->getIdentitas($id_jenjangprodi,$id_prodi);
			// print_r($data['identitasborang']);
			$data['datadosen']=$this->standarevaluasi_m->getIdentitasdosen($id_jenjangprodi,$id_prodi);
			$data['datapengisi']=$this->standarevaluasi_m->getPengisiborang($id_jenjangprodi,$id_prodi,$id_tahunakademik);
			
			
		   if($id_jenjangprodi=="1"){
			   $html = $this->load->view('cetakborang/d3satu', $data,true);
					
		   }if($id_jenjangprodi=="2"){
			   $html = $this->load->view('cetakborang/d3satu', $data,true);
		   }if($id_jenjangprodi=="3"){
			   $html = $this->load->view('cetakborang/d3satu', $data,true);
		   }if($id_jenjangprodi=="4"){
			   $html = $this->load->view('cetakborang/d3satu', $data,true);
		   }if($id_jenjangprodi=="5"){
			   
		   }
		   
		   
		   
	   }else if($standar=="2"){
		   $data['databorang']=$this->standarevaluasi_m->getBorang($id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel,$standar);
			   foreach($data['databorang'] as $std){
				$id=$std->id;
				$data['datauraian'][$id]=$this->standarevaluasi_m->getIsiborang($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel);
				
				}
			   if($id_jenjangprodi=="1"){
				   $html = $this->load->view('cetakborang/d3dua', $data,true);
						
			   }if($id_jenjangprodi=="2"){
				   $html = $this->load->view('cetakborang/d3dua', $data,true);
			   }if($id_jenjangprodi=="3"){
				   $html = $this->load->view('cetakborang/d3dua', $data,true);
			   }if($id_jenjangprodi=="4"){
				   $html = $this->load->view('cetakborang/d3dua', $data,true);
			   }if($id_jenjangprodi=="5"){
				   
			   }
			   
	   }else if($standar=="3"){
		
				
			$data['databorang']=$this->standarevaluasi_m->getBorang($id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel,$standar);
			
			if($id_jenjangprodi=="1"){
				  foreach($data['databorang'] as $std){
					$id=$std->id;
					$data['datauraian'][$id]=$this->standarevaluasi_m->getIsiborang($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel);
						if($id=="38"){
							$data['datatigasatusatu'][$id]=$this->standarevaluasi_m->getTigasatusatu($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_d3_tiga_satu_satu');
						}else if($id==40){
							$data['datatigasatusatu'][$id]=$this->standarevaluasi_m->getTigasatusatu($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_d3_tiga_satu_tiga_a');
						}else if($id==41){
							$data['datatigasatusatu'][$id]=$this->standarevaluasi_m->getTigasatusatu($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_d3_tiga_satu_tiga_b');
						}else if($id==42){
							$data['datatigasatusatu'][$id]=$this->standarevaluasi_m->getTigasatusatu($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_d3_tiga_satu_tiga_c');
						}else if($id==43){
							$data['datatigasatusatu'][$id]=$this->standarevaluasi_m->getTigasatusatu($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_d3_tiga_satu_tiga_d');
						}else if($id==48){
							$data['datatigasatusatu'][$id]=$this->standarevaluasi_m->getTigasatusatu($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_d3_tiga_empat_satu');
						}else if($id==52){
							$data['datatigasatusatu'][$id]=$this->standarevaluasi_m->getTigasatusatu($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_d3_tiga_empat_lima');
						}
					
					}
				$html = $this->load->view('cetakborang/d3tiga', $data,true);
				
			   }if($id_jenjangprodi=="2"){
				  
				  foreach($data['databorang'] as $std){
					$id=$std->id;
					$data['datauraian'][$id]=$this->standarevaluasi_m->getIsiborang($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel);
						if($id=="150"){
							$data['standarsarjana'][$id]=$this->standarevaluasi_m->getTigasarjana($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_s1_tiga_satu_satu');
						}else if($id==151){
							$data['standarsarjana'][$id]=$this->standarevaluasi_m->getTigasarjana($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_s1_tiga_satu_dua');
						}else if($id==153){
							$data['standarsarjana'][$id]=$this->standarevaluasi_m->getTigasarjana($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_s1_tiga_satu_empat');
						}else if($id==157){
							$data['standarsarjana'][$id]=$this->standarevaluasi_m->getTigasarjana($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_s1_tiga_tiga_satu');
						}
					
					}
				$html = $this->load->view('cetakborang/s1tiga', $data,true);
				
				
			   }if($id_jenjangprodi=="3"){
				   foreach($data['databorang'] as $std){
					$id=$std->id;
					$data['datauraian'][$id]=$this->standarevaluasi_m->getIsiborang($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel);
						if($id=="175"){
							$data['standarsarjana'][$id]=$this->standarevaluasi_m->getTigamagister($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_s2_tiga_dua_satu');
						}else if($id==177){
							$data['standarsarjana'][$id]=$this->standarevaluasi_m->getTigamagister($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_s2_tiga_dua_tiga');
						}else if($id==180){
							$data['standarsarjana'][$id]=$this->standarevaluasi_m->getTigamagister($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_s2_tiga_tiga_dua');
						}
					
					}
				$html = $this->load->view('cetakborang/s2tiga', $data,true);
			   }if($id_jenjangprodi=="4"){
				    foreach($data['databorang'] as $std){
					$id=$std->id;
					$data['datauraian'][$id]=$this->standarevaluasi_m->getIsiborang($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel);
						if($id=="196"){
							$data['standarsarjana'][$id]=$this->standarevaluasi_m->getTigadoktor($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_s3_tiga_dua_satu');
						}else if($id==198){
							$data['standarsarjana'][$id]=$this->standarevaluasi_m->getTigadoktor($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_s3_tiga_dua_tiga');
						}else if($id==201){
							$data['standarsarjana'][$id]=$this->standarevaluasi_m->getTigadoktor($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,'br_s3_tiga_tiga_dua');
						}
					
					}
				$html = $this->load->view('cetakborang/s3tiga', $data,true);
			   }if($id_jenjangprodi=="5"){
				   
			   }
			   
				
				
			   
			//   print_r($data['datatigasatusatu']);
			   
			   
		   
	   }else if($standar=="4"){
		   
	   }else if($standar=="5"){
		   
	   }else if($standar=="6"){
		   
	   }else if($standar=="7"){
		   
	   }
	   
	 
	

libxml_use_internal_errors(true);
$doc = new DomDocument;
$doc->loadHTML($html);
$datauraian=$doc->saveHTML();



 header("Expires: 0");
 header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=Buku_Borang".$standar.".doc");

echo $datauraian;
	

		
		
   }
   
   
   function display_xml_error($error, $xml)
{
    $return  = $xml[$error->line - 1] . "\n";
    $return .= str_repeat('-', $error->column) . "^\n";

    switch ($error->level) {
        case LIBXML_ERR_WARNING:
            $return .= "Warning $error->code: ";
            break;
         case LIBXML_ERR_ERROR:
            $return .= "Error $error->code: ";
            break;
        case LIBXML_ERR_FATAL:
            $return .= "Fatal Error $error->code: ";
            break;
    }

    $return .= trim($error->message) .
               "\n  Line: $error->line" .
               "\n  Column: $error->column";

    if ($error->file) {
        $return .= "\n  File: $error->file";
    }

    return "$return\n\n--------------------------------------------\n\n";
}

   function download_penilaian(){
	   $id_jenjangprodi=$_GET['id_jenjangprodi'];
	   $id_tahunakademik=$_GET['id_tahunakademik'];
	   $id_prodi=$_GET['id_prodi'];
	   $standarsatu=$this->standarevaluasi_m->getParametersatu($id_jenjangprodi,$id_tahunakademik,$id_prodi);
	   $satu=0;
	   $dua=0;
	   $tiga=0;
	   foreach($standarsatu as $prm){
				$id=$prm->id;
				$nilai=$prm->nilai;
				$poin=$prm->poin;
				if($id=="25"){
					$satu=$nilai;
				}
				if($id=="29"){
					$dua=$nilai;
				}
				if($id=="30"){
					$tiga=$nilai;
				}
	   }
	   
	   $setduasatu=0;
	   $setduadua=0;
	   $setduatiga=0;
	   $setduaempat=0;
	   $setdualima=0;
	   $setduaenam=0;
	   $standardua=$this->standarevaluasi_m->getParameterdua($id_jenjangprodi,$id_tahunakademik,$id_prodi);
	    foreach($standardua as $setdua){
				$id=$setdua->id;
				$nilai=$setdua->nilai;
				if($id=="31"){
					$setduasatu=$nilai;
				}
				if($id=="32"){
					$setduadua=$nilai;
				}
				if($id=="33"){
					$setduatiga=$nilai;
				}
				if($id=="34"){
					$setduaempat=$nilai;
				}
				if($id=="35"){
					$setdualima=$nilai;
				}
				if($id=="36"){
					$setduaenam=$nilai;
				}
				
		}
		
		$tigasatusatu=$this->standarevaluasi_m->getStandartigasatu($id_jenjangprodi,$id_tahunakademik,$id_prodi);
		 $daya_tampung=0;
		 $mhs_ikut_seleksi=0;
		 foreach($tigasatusatu as $tigasatu){
			 $daya_tampung=$tigasatu->daya_tampung;
			 $mhs_ikut_seleksi=$tigasatu->mhs_ikut_seleksi;
			 
		 }
	   //print_r($parameter);
	  // exit;
	   include_once (APPPATH . "third_party/PHPExcel/Classes/PHPExcel.php");
	    $objPHPExcel = new PHPExcel();
		
		 $inputFileName = 'download/D3';
		
	   $excel2 = PHPExcel_IOFactory::createReader('Excel5');
		$excel2 = $excel2->load($inputFileName . '.xls');
		$excel2->setActiveSheetIndex(0);
	
		$baris=4;
	 $work_sheet_count=12;
	 $work_sheet=0;
	 while($work_sheet<=$work_sheet_count){ 
		 if($work_sheet==1){
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E4', $satu);
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E7', $dua);
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E10', $tiga);
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E13', $setduasatu);
			  $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E16', $setduadua);
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E19', $setduatiga);
			 $excel2->setActiveSheetIndex($work_sheet)->setCellValue('E22', $setduaempat);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E25', $setdualima);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E28', $setduaenam);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E32', $mhs_ikut_seleksi);
			$excel2->setActiveSheetIndex($work_sheet)->setCellValue('E33', $daya_tampung);
		 }

		 $work_sheet++;
	 }
	
	
   $writer = PHPExcel_IOFactory::createWriter($excel2, 'Excel2007');
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="penilaiand3.xls"');
	header('Cache-Control: max-age=0');
	ob_end_clean();
	$writer->save('php://output');
	exit;
   }
   
   
   
   
   function download_template_11(){
	   include_once (APPPATH . "third_party/PHPExcel/Classes/PHPExcel.php");
	   //include_once (APPPATH . "PHPExcel/Classes/PHPExcel/IOFactory.php");
	    $objPHPExcel = new PHPExcel();
		
		/*for ($i=0; $i <2 ; $i++) { 
    $objPHPExcel->getActiveSheet()->setTitle('satu'); //sheetname
    $newsheet = $objPHPExcel->createSheet(); //sheet create
}*/

	/*
$objPHPExcel->setActiveSheetIndex(0);
 $objPHPExcel->getActiveSheet()->setCellValue('A1', "nama");
 $objPHPExcel->getActiveSheet()->setCellValue('B1', "alamat");
 $objPHPExcel->getActiveSheet()->setCellValue('C1', "nomer");
 $objPHPExcel->getActiveSheet()->setCellValue('D1', "asal");
// $exeRpt = $this->import_akun_member_m->get_template();
*/

$rowCount = 2;
//foreach ($exeRpt->result_array() as $resReport) {
/*
$sql="select * from grup_aruskas_dua";
$result=mysql_query($sql);
while($row = mysql_fetch_array($result)){
	*/
	$filelokasi="download/penilaiand311.xlsx";
	//$excel2 = PHPExcel_IOFactory::createReader('Excel2007');
	//$excel2 = $excel2->load($filelokasi); // Empty Sheet
	
	$objReader= PHPExcel_IOFactory::createReader('Excel2007');
$objReader->setReadDataOnly(true); 
$objPHPExcel= PHPExcel_IOFactory::load($filelokasi);    
//        $this->response($objPHPExcel);
$objPHPExcel=$objReader->load($filelokasi); 
	
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, 'ggggggggggggghhhhh');
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,'55555555555');
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, '');
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, '');
    $rowCount++;
	
	/*
}*/

//}
exit;

 
$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="penilaiand3.xls"');
header('Cache-Control: max-age=0');
ob_end_clean();
$writer->save('php://output');
exit;
   
   }
   
   
}?>