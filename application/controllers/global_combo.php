<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class global_combo extends CI_Controller {
	
	function getProdijenjang($id_jenjang){
			$data = array();
		$hasil=$this->global_combomodel->getProdijenjang($id_jenjang);
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	
    function global_combo()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('global_combomodel'));
        $this->load->helper(array('form', 'url'));
		
	}
	function getDosen(){
		$data = array();
		$hasil=$this->global_combomodel->getDosen();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getDataktsob($where){
		$data = array();
		$hasil=$this->global_combomodel->getDataktsob($where);
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getKtsob(){
		$data = array();
		$hasil=$this->global_combomodel->getKtsob();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getAuditie(){
		$data = array();
		$hasil=$this->global_combomodel->getAuditie();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getUnitdep(){
		$data = array();
		$hasil=$this->global_combomodel->getUnitdep();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getSop(){
		$data = array();
		$hasil=$this->global_combomodel->getSop();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getMkp($id_tahunakademik){
		$data = array();
		$hasil=$this->global_combomodel->getMkp($id_tahunakademik);
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getStandar(){
		$data = array();
		$hasil=$this->global_combomodel->getStandar();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
//
	function getFormulirstandar($where){
	$data = array();
	$hasil=$this->global_combomodel->getFormulirstandar($where);
	foreach ($hasil->result_array() as $row){
		$data[] = $row;
		}
		$json = json_encode($data);			
		echo $json;
	}
	function getFormuliranggota($where){
		$data = array();
		$hasil=$this->global_combomodel->getFormuliranggota($where);
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}
			$json = json_encode($data);			
			echo $json;
		}
        function getFormulirstandaraudit($where){
		$data = array();
		$hasil=$this->global_combomodel->getFormulirstandaraudit($where);
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}
                        $json = json_encode($data);
                           
			echo $json;
	}
        function getPlanstandar($where){
		$data = array();
		$hasil=$this->global_combomodel->getPlanstandar($where);
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}
                        $json = json_encode($data);
                           
			echo $json;
	}
	
	function getRenstra(){
		$data = array();
		$hasil=$this->global_combomodel->getRenstra();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getRektorat(){
			$data = array();
		$hasil=$this->global_combomodel->getRektorat();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getDirektorat(){
		$data = array();
		$hasil=$this->global_combomodel->getDirektorat();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getDirektoratdua($id_rektorat){
		//$id_rektorat = $this->input->post("id_rektorat");
		$data = array();
		$hasil=$this->global_combomodel->getDirektoratdua($id_rektorat);
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getUnitdua($id_direktorat){
		$data = array();
		$hasil=$this->global_combomodel->getUnitdua($id_direktorat);
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getProdidua(){
		$data = array();
		$hasil=$this->global_combomodel->getProdidua();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getFakultas(){
			$data = array();
		$hasil=$this->global_combomodel->getFakultas();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	
	function getJenjangprodi(){
			$data = array();
		$hasil=$this->global_combomodel->getJenjangprodi();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getProdi($id_fakultas){
			$data = array();
		$hasil=$this->global_combomodel->getProdi($id_fakultas);
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getTahunakademik(){
		$data = array();
		$hasil=$this->global_combomodel->getTahunakademik();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getPlaningpersiapan(){
		$data = array();
		$hasil=$this->global_combomodel->getPlaningpersiapan();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getTahunakademikAuditLaporan(){
		$data = array();
		$hasil=$this->global_combomodel->getTahunakademikAuditLaporan();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}

	//===========Riyan=========//
	function getKaryawan(){
		$data = array();
		$hasil=$this->global_combomodel->getKaryawan();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
    function getDataplan($where){
		$data = array();
		$hasil=$this->global_combomodel->getDataplan($where);
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getDataplanstandar($where){
		$data = array();
		$hasil=$this->global_combomodel->getDataplanstandar($where);
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	function getFormulir(){
		$data = array();
		$hasil=$this->global_combomodel->getFormulir();
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}

			$json = json_encode($data);
		 
			echo $json;
	}
	
}


?>