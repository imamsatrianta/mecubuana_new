<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rool extends CI_Controller {

    function rool()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('masterdata/rool_m','global_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
        
	  $data['menu']= $this->global_m->getParent();
	  foreach($data['menu'] as $menudet){
				$idmenu=$menudet->id; 
				
				$data['detmenu'][$idmenu] = $this->global_m->getMenudet($idmenu);
				$data['jmldet'][$idmenu]=count ($data['detmenu'][$idmenu]);
				
				foreach($data['detmenu'][$idmenu] as $menudua){
					$idmenudua=$menudua->id; 
					
					$data['detmenudua'][$idmenu][$idmenudua] = $this->global_m->getMenudet($idmenudua);
					$data['jmldetdua'][$idmenu][$idmenudua]=count ($data['detmenudua'][$idmenu][$idmenudua]);
					
					//echo count ($data['detmenudua'][$idmenu][$idmenudua]);
				
				}
				
			  }
			  
	
	   $this->load->view('atas_v',$data);
    	$this->load->view('masterdata/rool_v',$data);
    	$this->load->view('bawah');
	}
    
 function simpanData(){
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	
	$id_rol = $this->input->post('id_rol');
    $nm_rol = $this->input->post('nm_rol');

	$set = $this->input->post('set');
	
	$tgl=date('Y-m-d');
	$id_user=$this->session->userdata('id_user');
	if($set==0){
		 $data=array(
		 'nm_rol'=>$nm_rol,
		 'user_c'=>$id_user,
		  'tgl_c'=>$tgl,
		 'status'=>'1'
		 );
		 
		$ceksatu=$this->rool_m->simpanData($id_rol,$data);
		$rolid=mysql_insert_id();
	}else{
		$data=array(
		 'nm_rol'=>$nm_rol,
		 'user_u'=>$id_user,
		  'tgl_u'=>$tgl,
		 'status'=>'1'
		 );
		$ceksatu=$this->rool_m->editData($id_rol,$data);
		$rolid=$id_rol;
		$cekhapus=$this->rool_m->hapusRoolmenu($rolid);
		if($cekhapus==1){
			$setsim="ok";	
		}else{
			$totalcek++;
		}
	}	
	
	
	
	
	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
	
	
	if(isset($_POST['detail'])){
		$detailsatu=$this->input->post('detail');
		foreach ( $detailsatu as $detaildata ) {
			if(isset($detaildata['id_menusatu'])){
				$id_menusatu=$detaildata['id_menusatu'];
				$tampil="1";
				
				
								
					$datasatu=array(
					 'id_menu'=>$id_menusatu,
					 'id_rol'=>$rolid,
					  'tampil'=>$tampil
					 );
					
					 
					$cekdua=$this->rool_m->simpanDatadet($datasatu);
					if($cekdua==""){
						$totalcek=$totalcek+1;
						break;
					}
					
				if(isset($detaildata['detaildua'])){
					$dataduaawal=$detaildata['detaildua'];
						foreach ( $dataduaawal as $detdua ) {
							$id_menudua=$detdua['menudet'];
							if(isset($detdua['tampil'])){
								$tampildet="1";
								if(isset($detdua['tambah'])){
									$tambah=1;
								}else{
									$tambah=0;
								}
								
								if(isset($detdua['ubah'])){
									$ubah=1;
								}else{
									$ubah=0;
								}
								
								if(isset($detdua['hapus'])){
									$hapus=1;
								}else{
									$hapus=0;
								}
								
								if(isset($detdua['approve'])){
									$approve=1;
								}else{
									$approve=0;
								}
								
								if(isset($detdua['cetak'])){
									$cetak=1;
								}else{
									$cetak=0;
								}
								
								if(isset($detdua['aktivasi'])){
									$aktivasi=1;
								}else{
									$aktivasi=0;
								}
								
								$dataduasimpan=array(
								 'id_menu'=>$id_menudua,
								 'id_rol'=>$rolid,
								  'tampil'=>$tampildet,
								  'ubah'=>$ubah,
								   'simpan'=>$tambah,
								  'hapus'=>$hapus,
								  'approve'=>$approve,
								  'cetak'=>$cetak
								 );
								 
								
								$cektiga=$this->rool_m->simpanDatadet($dataduasimpan);
								if($cektiga==""){
									$totalcek=$totalcek+1;
									break;
								}
								
								if(isset($detdua['detailtiga'])){
								$dataduatiga=$detdua['detailtiga'];
								//print_r($dataduatiga);
									foreach ( $dataduatiga as $dettiga ) {
										$id_menutiga=$dettiga['menudettiga'];
									//	echo $id_menutiga."/";
										if(isset($dettiga['tampiltiga'])){
											$tampiltiga=1;
											if(isset($dettiga['tambahtiga'])){
												$tambahtiga=1;
											}else{
												$tambahtiga=0;
											}
											
											if(isset($dettiga['ubahtiga'])){
												$ubahtiga=1;
											}else{
												$ubahtiga=0;
											}
											
											if(isset($dettiga['hapustiga'])){
												$hapustiga=1;
											}else{
												$hapustiga=0;
											}
											
											if(isset($dettiga['approvetiga'])){
												$approvetiga=1;
											}else{
												$approvetiga=0;
											}
											
											if(isset($dettiga['cetaktiga'])){
												$cetaktiga=1;
											}else{
												$cetaktiga=0;
											}
											
											if(isset($dettiga['aktivasitiga'])){
												$aktivasitiga=1;
											}else{
												$aktivasitiga=0;
											}
											
								
											$datatigasimpan=array(
											 'id_menu'=>$id_menutiga,
											 'id_rol'=>$rolid,
											  'tampil'=>$tampiltiga,
											  'ubah'=>$ubahtiga,
											   'simpan'=>$tambahtiga,
											  'hapus'=>$hapustiga,
											  'approve'=>$approvetiga,
											  'cetak'=>$cetaktiga
											 );
											 
											//  print_r($datatigasimpan);
											
											$cekempat=$this->rool_m->simpanDatadet($datatigasimpan);
											if($cekempat==""){
												$totalcek=$totalcek+1;
												break;
											}
											
											
											
										}
										
									}
									
								}
								
								
								
								
							
								
							}
							
							
						}
					
					}
			}
			}
		}
				
		
	
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array('pesan' => 'Informasi <br> Simpan Data Gagal','status' => 'danger'));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array('pesan' => 'Informasi <br> Simpan Data Gagal','status' => 'danger'));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array('pesan' => 'Informasi <br> Simpan data Berhasil','status' => 'success'));
		}
	return $status;	
	}

    
   function loaddataTabel(){
        $offset=$_REQUEST['offset'];
         $limit=$_REQUEST['limit'];
		 $order=$_REQUEST['order'];
		 if(isset($_REQUEST['search'])){
			 $search=$_REQUEST['search'];
			 $where="(upper(nm_rol) like upper('%$search%') ) ";
			 }else{
			  $where='nm_rol is not null';
			 }
		 
        $this->rool_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
    
    
   function hapusData(){
   		 $id=$_REQUEST['id'];
		 $ceksatu=$this->rool_m->aktivData($id);
		 
			if($ceksatu==1){
				echo json_encode(array('pesan' => 'Informasi <br> Aktivasi data Berhasil','status' => 'success'));
				//$setsim="ok";	
			}else{
				//$setsim="gagal";
				echo json_encode(array('pesan' => 'Informasi <br> Aktivasi Data Gagal','status' => 'danger'));
			}
		//	echo $setsim;
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	
   	$data=$_REQUEST['data'];
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
		if($data[$row]!=""){
		$ceksatu=$this->rool_m->aktivData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
		}	
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
		//	echo "Simpan data gagal";
			echo json_encode(array('pesan' => 'Informasi <br> Aktivasi Data Gagal','status' => 'danger'));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
		//	echo $setsim;
			echo json_encode(array('pesan' => 'Informasi <br> Aktivasi Data Gagal','status' => 'danger'));
		}else {
			$this->db->trans_commit();
			echo json_encode(array('pesan' => 'Informasi <br> Aktivasi data Berhasil','status' => 'success'));
			//echo $setsim;
			}
		return $status;	
	
   }
   
   
   
   function editDatarool(){
		$id_rol = $this->input->get('id_rol');
		//echo $id_rol;exit;
		$arrayMenu=array();
		$arauSatu =array();
		$arrayDua=array();
		$arrayTiga=array();
		$menusatu=$this->rool_m->getMenuutamaedit($id_rol); 
		//print_r($menusatu); exit;
		foreach($menusatu as $datasatu){ 
			$idmenu=$datasatu->id_menu;
			$u_access=$datasatu->tampil;
			

			$arauSatu['id'] = $idmenu;
			$arauSatu['u_access'] = $u_access;
			 $arauSatu['menusatu'] = array();
			 
			$dataarray=$this->rool_m->menuDetailedit($idmenu,$id_rol);
			//print_r($dataarray);
		//	echo count ($dataarray)."<br>";
			foreach($dataarray as $data){ 
			$iddua=$data->id_menu;
			$u_accessdua=$data->tampil;
			$u_updatedua=$data->ubah;
			$u_insertdua=$data->simpan;
			$u_deletedua=$data->hapus;
			$u_prosesdua=$data->approve;
			$u_printdua=$data->cetak;
			$arrayDua['id']=$iddua;
			$arrayDua['u_accessdua']=$u_accessdua;
			$arrayDua['u_insertdua']=$u_insertdua;
			$arrayDua['u_updatedua']=$u_updatedua;
			$arrayDua['u_deletedua']=$u_deletedua;
			$arrayDua['u_prosesdua']=$u_prosesdua;
			$arrayDua['u_printdua']=$u_printdua;
			//array_push($arauSatu['menusatu'],$arrayDua);
			$arrayDua['menudua']=array();
			
			$dataarraytiga=$this->rool_m->menuDetailedit($iddua,$id_rol);
				foreach($dataarraytiga as $datatiga){ 
					$idtiga=$datatiga->id_menu;
					$u_accesstiga=$datatiga->tampil;
					$u_updatetiga=$data->ubah;
					$u_inserttiga=$data->simpan;
					$u_deletetiga=$data->hapus;
					$u_prosestiga=$data->approve;
					$u_printtiga=$data->cetak;
			
					$arrayTiga['id']=$idtiga;
					$arrayTiga['u_accesstiga']=$u_accesstiga;
					$arrayTiga['u_inserttiga']=$u_inserttiga;
					$arrayTiga['u_updatetiga']=$u_updatetiga;
					$arrayTiga['u_deletetiga']=$u_deletetiga;
					$arrayTiga['u_prosestiga']=$u_prosestiga;
					$arrayTiga['u_printtiga']=$u_printtiga;
					//$arrayTiga['children']=1;
					array_push($arrayDua['menudua'],$arrayTiga);
				}
			
			
			
			
			array_push($arauSatu['menusatu'],$arrayDua);
			
			}
			
			array_push($arrayMenu,$arauSatu);
		}
		
		
		
		 
		$jsonData = json_encode($arrayMenu);
		
		echo $jsonData; 
	}
   
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */