<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class menu extends CI_Controller {

    function menu()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('masterdata/menu_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
	
	   $this->load->view('atas_v');
    	$this->load->view('masterdata/menu_v');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	
	$this->db->trans_begin();$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$menudes = $this->input->post('menudes');
	$menutool = $this->input->post('menutool');
	$url = $this->input->post('url');
	$parent = $this->input->post('parent');
	$no = $this->input->post('no');	
	$set = $this->input->post("set");
	$data=array(
	'menudes'=>$menudes,
	'menutool'=>$menutool,
	'url'=>$url,'parent'=>$parent,
	'no'=>$no,);	
	if($set==0){$ceksatu=$this->menu_m->simpanData($id,$data);
	}else{
		$ceksatu=$this->menu_m->editData($id,$data);
	}	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="upper(menudes) like upper('%$search%')  or upper(menutool) like upper('%$search%')  or upper(url) like upper('%$search%')  or upper(parent) like upper('%$search%')  or upper(no) like upper('%$search%')  ";
			 
			 
				
			 }else{
			 $where="id is not null";
			 }
		 
        $this->menu_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function aktivData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->menu_m->aktivData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusArray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->menu_m->aktivData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function getCombo(){
		$arrayMenu=array();
		$arauSatu =array();
		$arrayDua=array();
		$arrayTiga=array();
		$menusatu=$this->menu_m->getMenuutama(); 
		//print_r($menusatu);
		foreach($menusatu as $datasatu){ 
			$idmenu=$datasatu->id;
			$menudes=$datasatu->menudes;

			$arauSatu['id'] = $idmenu;
			$arauSatu['text'] = $menudes;
			 $arauSatu['children'] = array();
			 
			$dataarray=$this->menu_m->menuDetail($idmenu);
		//	echo count ($dataarray)."<br>";
			foreach($dataarray as $data){ 
			
			$iddua=$data->id;
			$textdua=$data->menudes;
			$arrayDua['id']=$iddua;
			$arrayDua['text']=$textdua;
			$arrayDua['children']=array();
			
			$dataarraytiga=$this->menu_m->menuDetail($iddua);
				foreach($dataarraytiga as $datatiga){ 
					$idtiga=$datatiga->id;
					$texttiga=$datatiga->menudes;
					$arrayTiga['id']=$idtiga;
					$arrayTiga['text']=$texttiga;
					//$arrayTiga['children']=1;
					array_push($arrayDua['children'],$arrayTiga);
				}
			
			
			
			
			array_push($arauSatu['children'],$arrayDua);
			
			}
			
			array_push($arrayMenu,$arauSatu);
		}
		
		
		
		 
		$jsonData = json_encode($arrayMenu);
		
		echo $jsonData; 
		

}


    function getCombo1(){
	$sql="select  *,id as id_menu   from menu where parent='0'  ";
	$hasil=mysql_query($sql);
	$looping=0;
	
	$sqljm="select count(*) as jml  from menu where parent='0'  ";
	$the_array12 = mysql_query($sqljm);
	$count11 = mysql_fetch_object($the_array12);
	$jml=$count11->jml;
	
	$jsonData="";
	$jsonData=$jsonData."[";
    $jsonData=$jsonData."{";
	$jsonData=$jsonData."\"id\":\"1\",";
	$jsonData=$jsonData."\"text\":\"Menu System\",";
	
	$jsonData=$jsonData."\"children\":";
	$jsonData=$jsonData."[";
    
	
	while($rs=mysql_fetch_object($hasil)){
	$looping++;
	$id_menu=$rs->id_menu;
	$menudes=$rs->menudes;
	
	$jsonData=$jsonData."{";
	
	
	 $jsonData=$jsonData."\"id\":\"$id_menu\",";
	  $jsonData=$jsonData."\"text\":\"$menudes\",";
	
	 
	 
	 	$sql1="select  *,id as id_menu  from menu where parent='$id_menu'  ";
		$hasil1=mysql_query($sql1);
		$looping1=0;
		
		$sqljm1="select count(*) as jml1  from menu where parent='$id_menu'  ";
				$the_array1 = mysql_query($sqljm1);
				$count1 = mysql_fetch_object($the_array1);
				$jml1=$count1->jml1;
				
		
		$jsonData=$jsonData."\"children\":";
		$jsonData=$jsonData."[";
		while($rs1=mysql_fetch_object($hasil1)){
		$looping1++;
		$id1=$rs1->id_menu;
		$menudes1=$rs1->menudes;
		
		
    	$jsonData=$jsonData."{";
		
		$jsonData=$jsonData."\"id\":\"$id1\",";
	    $jsonData=$jsonData."\"text\":\"$menudes1\"";
		//$jsonData=$jsonData."\"state\":\"closed\",";
		
				
		
				
				
				
				if($looping1==$jml1){
					$jsonData=$jsonData."}";
					}else{
					$jsonData=$jsonData."},";
					}	
				
	
     	
		
		
		
			}
	
	//children":[{
	$jsonData=$jsonData."]";
	
	
	
	
	if($looping==$jml){
	$jsonData=$jsonData."}";
	}else{
	$jsonData=$jsonData."},";
	}	
					
	
		
	
	}
	
	
	
	
	
     $jsonData=$jsonData."]";
	
	 $jsonData=$jsonData."}";
     $jsonData=$jsonData."]";
     echo $jsonData;
		

}
}?>