<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user extends CI_Controller {

    function user()
	{
		parent::__construct();
		$this->load->model(array('masterdata/user_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
		
		$data['menu']= $this->global_m->getParent();
	  foreach($data['menu'] as $menudet){
				$idmenu=$menudet->id; 
				
				$data['detmenu'][$idmenu] = $this->global_m->getMenudet($idmenu);
				$data['jmldet'][$idmenu]=count ($data['detmenu'][$idmenu]);
				
				foreach($data['detmenu'][$idmenu] as $menudua){
					$idmenudua=$menudua->id; 
					
					$data['detmenudua'][$idmenu][$idmenudua] = $this->global_m->getMenudet($idmenudua);
					$data['jmldetdua'][$idmenu][$idmenudua]=count ($data['detmenudua'][$idmenu][$idmenudua]);
					
					//echo count ($data['detmenudua'][$idmenu][$idmenudua]);
				
				}
				
			  }
	          
		$data['rool']= $this->global_m->getRool(); 
		$data['level']= $this->global_combomodel->getLevelmenu();
		$data['rektorat']= $this->global_combomodel->getRektoratdua();
	   $this->load->view('atas_v',$data);
    	$this->load->view('masterdata/user_v');
		$this->load->view('masterdata/userjs');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$username = $this->input->post('username');
	$password = paramEncrypt($this->input->post('password'));  //md5($this->input->post('password'));
	$idrool = $this->input->post('idrool');
	$id_level = $this->input->post('id_level');
	$id_rektorat = $this->input->post('id_rektorat');
	$id_direktorat = $this->input->post('id_direktorat');
	$id_unit = $this->input->post('id_unit');
	$id_prodi = $this->input->post('id_prodi');
	$tgl=date('Y-m-d');
	$id_user=$this->session->userdata('userid');	
	$set = $this->input->post("set");
	$ceksatu="";
	
	if($set==0){
		
		
		$this->db->select('user.*');
		$this->db->from('user');
		$this->db->where('username',$username);	
		$hasil = $this->db->get();
		
		if($hasil->num_rows() > 0){
			$totalcek=1;
		}else{
		$data=array(
		'username'=>$username,
		'password'=>$password,
		'idrool'=>$idrool,
		'id_level'=>$id_level,
		'id_rektorat'=>$id_rektorat,
		'id_direktorat'=>$id_direktorat,
		'id_unit'=>$id_unit,
		'id_prodi'=>$id_prodi,
		'user_c'=>$id_user,
		'tgl_c'=>$tgl);
	
	$ceksatu=$this->user_m->simpanData($id,$data);
	$iduser=mysql_insert_id();
		}
	}else{
		$data=array(
		'username'=>$username,
		'password'=>$password,
		'idrool'=>$idrool,
		'id_level'=>$id_level,
		'id_rektorat'=>$id_rektorat,
		'id_direktorat'=>$id_direktorat,
		'id_unit'=>$id_unit,
		'id_prodi'=>$id_prodi,
		'user_u'=>$id_user,
		'tgl_u'=>$tgl);
	
		$ceksatu=$this->user_m->editData($id,$data);
		$iduser=$id;
	}	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	if ($totalcek==0){
	if($set==0){
		$datamenu=$this->user_m->getMenurool($idrool);
			 foreach($datamenu as $data){
				$id_menu=$data->id_menu;
				$tampil=$data->tampil;
				$simpan=$data->simpan;
				$ubah=$data->ubah;
				$hapus=$data->hapus;
				$approve=$data->approve;
				$cetak=$data->cetak;
				$aktivasi=$data->aktivasi;
				
				$datarol=array(
				 'id_user'=>$iduser,
				 'id_menu'=>$id_menu,
				  'tampil'=>$tampil,
				  'simpan'=>$simpan,
				  'ubah'=>$ubah,
				  'hapus'=>$hapus,
				  'approve'=>$approve,
				  'cetak'=>$cetak,
				  'aktivasi'=>$aktivasi
				 );
				 
				$cektiga=$this->user_m->simpanDatadetdua($datarol);
				if($cektiga==""){
					$totalcek=$totalcek+1;
					break;
				}
				
			 }
	}/*else{
	
		$hasil=$this->user_m->getMenudata($iduser,$idrool);
	//	echo $hasil->num_rows();exit;
		if($hasil->num_rows() < 1){
		
			$this->user_m->hapusDatamenu($iduser);
			$datamenu=$this->user_m->getMenurool($idrool);
			 foreach($datamenu as $data){
				$id_menu=$data->id_menu;
				$tampil=$data->tampil;
				$simpan=$data->simpan;
				$ubah=$data->ubah;
				$hapus=$data->hapus;
				$approve=$data->approve;
				$cetak=$data->cetak;
				$aktivasi=$data->aktivasi;
				
				$datarol=array(
				 'id_user'=>$iduser,
				 'id_menu'=>$id_menu,
				  'tampil'=>$tampil,
				  'simpan'=>$simpan,
				  'ubah'=>$ubah,
				  'hapus'=>$hapus,
				  'approve'=>$approve,
				  'cetak'=>$cetak,
				  'aktivasi'=>$aktivasi
				 );
				 
				$cektiga=$this->user_m->simpanDatadetdua($datarol);
				if($cektiga==""){
					$totalcek=$totalcek+1;
					break;
				}
				
			 }
			 
			 
			
		}
	
	}
	
	*/
	}
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="upper(a.username) like upper('%$search%') 
			  or upper(b.nm_rol) like upper('%$search%') or upper(c.nm_perusahaan) like upper('%$search%')  ";
			 }else{
			 $where='a.id is not null';
			 }
		 
        $this->user_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 	 
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->user_m->hapusData($id);
		 $this->user_m->hapusDatamenu($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
			if($data[$row]!=""){
				$this->user_m->hapusDatamenu($data[$row]);
				$ceksatu=$this->user_m->hapusData($data[$row]);
				if($ceksatu==1){
					$setsim="ok";	
				}else{
					$totalcek++;
				}
			}
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   
   
    function editDatarool(){
		$id_user = $this->input->get('id_user');
		//echo $id_rol;exit;
		$arrayMenu=array();
		$arauSatu =array();
		$arrayDua=array();
		$arrayTiga=array();
		$menusatu=$this->user_m->getMenuutamaedit($id_user); 
		//print_r($menusatu); exit;
		foreach($menusatu as $datasatu){ 
			$idmenu=$datasatu->id_menu;
			$u_access=$datasatu->tampil;
			

			$arauSatu['id'] = $idmenu;
			$arauSatu['u_access'] = $u_access;
			 $arauSatu['menusatu'] = array();
			 
			$dataarray=$this->user_m->menuDetailedit($idmenu,$id_user);
			//print_r($dataarray);
		//	echo count ($dataarray)."<br>";
			foreach($dataarray as $data){ 
			$iddua=$data->id_menu;
			$u_accessdua=$data->tampil;
			$u_updatedua=$data->ubah;
			$u_insertdua=$data->simpan;
			$u_deletedua=$data->hapus;
			$u_prosesdua=$data->approve;
			$u_printdua=$data->cetak;
			$arrayDua['id']=$iddua;
			$arrayDua['u_accessdua']=$u_accessdua;
			$arrayDua['u_insertdua']=$u_insertdua;
			$arrayDua['u_updatedua']=$u_updatedua;
			$arrayDua['u_deletedua']=$u_deletedua;
			$arrayDua['u_prosesdua']=$u_prosesdua;
			$arrayDua['u_printdua']=$u_printdua;
			//array_push($arauSatu['menusatu'],$arrayDua);
			$arrayDua['menudua']=array();
			
			$dataarraytiga=$this->user_m->menuDetailedit($iddua,$id_user);
				foreach($dataarraytiga as $datatiga){ 
					$idtiga=$datatiga->id_menu;
					$u_accesstiga=$datatiga->tampil;
					$u_updatetiga=$data->ubah;
					$u_inserttiga=$data->simpan;
					$u_deletetiga=$data->hapus;
					$u_prosestiga=$data->approve;
					$u_printtiga=$data->cetak;
			
					$arrayTiga['id']=$idtiga;
					$arrayTiga['u_accesstiga']=$u_accesstiga;
					$arrayTiga['u_inserttiga']=$u_inserttiga;
					$arrayTiga['u_updatetiga']=$u_updatetiga;
					$arrayTiga['u_deletetiga']=$u_deletetiga;
					$arrayTiga['u_prosestiga']=$u_prosestiga;
					$arrayTiga['u_printtiga']=$u_printtiga;
					//$arrayTiga['children']=1;
					array_push($arrayDua['menudua'],$arrayTiga);
				}
			
			
			
			
			array_push($arauSatu['menusatu'],$arrayDua);
			
			}
			
			array_push($arrayMenu,$arauSatu);
		}
		
		
		
		 
		$jsonData = json_encode($arrayMenu);
		
		echo $jsonData; 
	}
   
   
   function simpanDatadetail(){
   		$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	
	$rolid = $this->input->post('id_userrol');

	$tgl=date('Y-m-d');
	$id_user=$this->session->userdata('id_user');
	

	
	$cekhapus=$this->user_m->hapusRoolmenu($rolid);
	if($cekhapus==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
	
	if(isset($_POST['detail'])){
		$detailsatu=$this->input->post('detail');
		foreach ( $detailsatu as $detaildata ) {
			if(isset($detaildata['id_menusatu'])){
				$id_menusatu=$detaildata['id_menusatu'];
				$tampil="1";
				
				
								
					$datasatu=array(
					 'id_menu'=>$id_menusatu,
					 'id_user'=>$rolid,
					  'tampil'=>$tampil
					 );
					
					 
					$cekdua=$this->user_m->simpanDatadet($datasatu);
					if($cekdua==""){
						$totalcek=$totalcek+1;
						break;
					}
					
				if(isset($detaildata['detaildua'])){
					$dataduaawal=$detaildata['detaildua'];
						foreach ( $dataduaawal as $detdua ) {
							$id_menudua=$detdua['menudet'];
							if(isset($detdua['tampil'])){
								$tampildet="1";
								if(isset($detdua['tambah'])){
									$tambah=1;
								}else{
									$tambah=0;
								}
								
								if(isset($detdua['ubah'])){
									$ubah=1;
								}else{
									$ubah=0;
								}
								
								if(isset($detdua['hapus'])){
									$hapus=1;
								}else{
									$hapus=0;
								}
								
								if(isset($detdua['approve'])){
									$approve=1;
								}else{
									$approve=0;
								}
								
								if(isset($detdua['cetak'])){
									$cetak=1;
								}else{
									$cetak=0;
								}
								
								if(isset($detdua['aktivasi'])){
									$aktivasi=1;
								}else{
									$aktivasi=0;
								}
								
								$dataduasimpan=array(
								 'id_menu'=>$id_menudua,
								 'id_user'=>$rolid,
								  'tampil'=>$tampildet,
								  'ubah'=>$ubah,
								   'simpan'=>$tambah,
								  'hapus'=>$hapus,
								  'approve'=>$approve,
								  'cetak'=>$cetak
								 );
								 
								
								$cektiga=$this->user_m->simpanDatadet($dataduasimpan);
								if($cektiga==""){
									$totalcek=$totalcek+1;
									break;
								}
								
								if(isset($detdua['detailtiga'])){
								$dataduatiga=$detdua['detailtiga'];
								//print_r($dataduatiga);
									foreach ( $dataduatiga as $dettiga ) {
										$id_menutiga=$dettiga['menudettiga'];
									//	echo $id_menutiga."/";
										if(isset($dettiga['tampiltiga'])){
											$tampiltiga=1;
											if(isset($dettiga['tambahtiga'])){
												$tambahtiga=1;
											}else{
												$tambahtiga=0;
											}
											
											if(isset($dettiga['ubahtiga'])){
												$ubahtiga=1;
											}else{
												$ubahtiga=0;
											}
											
											if(isset($dettiga['hapustiga'])){
												$hapustiga=1;
											}else{
												$hapustiga=0;
											}
											
											if(isset($dettiga['approvetiga'])){
												$approvetiga=1;
											}else{
												$approvetiga=0;
											}
											
											if(isset($dettiga['cetaktiga'])){
												$cetaktiga=1;
											}else{
												$cetaktiga=0;
											}
											
											if(isset($dettiga['aktivasitiga'])){
												$aktivasitiga=1;
											}else{
												$aktivasitiga=0;
											}
											
								
											$datatigasimpan=array(
											 'id_menu'=>$id_menutiga,
											 'id_user'=>$rolid,
											  'tampil'=>$tampiltiga,
											  'ubah'=>$ubahtiga,
											   'simpan'=>$tambahtiga,
											  'hapus'=>$hapustiga,
											  'approve'=>$approvetiga,
											  'cetak'=>$cetaktiga
											 );
											 
											//  print_r($datatigasimpan);
											
											$cekempat=$this->user_m->simpanDatadet($datatigasimpan);
											if($cekempat==""){
												$totalcek=$totalcek+1;
												break;
											}
											
											
											
										}
										
									}
									
								}
								
								
								
								
							
								
							}
							
							
						}
					
					}
			}
			}
		
		
			}
	
	
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array('pesan' => 'Informasi <br> Simpan Data Gagal','status' => 'danger'));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array('pesan' => 'Informasi <br> Simpan Data Gagal','status' => 'danger'));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array('pesan' => 'Informasi <br> Simpan data Berhasil','status' => 'success'));
		}
	return $status;	
   }
   
    
}?>