<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class aksesappborang extends CI_Controller {

    function aksesappborang()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('masterdata/aksesappborang_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
	 //  echo "xx";exit;
    	$this->load->view('masterdata/aksesappborang_v');
    	$this->load->view('bawah');
	}
	
	
	
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$statussimpan=0;
	$id = $this->input->post("id");
	$id_prodi = $this->input->post('id_prodi');
	
	$kode_approvel = $this->input->post('kode_approvel');
	
	/*$dataapp="";
	$dataappsatuok="";
	$dataappsatu=array();
	if (isset($_POST['kode_approvel'])) {
			 foreach ($kode_approvel as  $app ) {
				$dataapp=$dataapp."".$app; 
				$dataappsatu[]=$app; 
				 
			 }
		$dataappsatuok=implode(',',$dataappsatu);
	}
	*/

	
	
	$set = $this->input->post("set");
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	$data=array(
	'id_prodi'=>$id_prodi,
	'kode_approvel'=>$kode_approvel);	
	$cekdata=$this->aksesappborang_m->cekData($id_prodi);
	
		if($set==0){
			if($cekdata->num_rows() > 0){
				$statussimpan=1;
			}else{
				$datasimpan=array_merge($datainput,$data);
				$ceksatu=$this->aksesappborang_m->simpanData($datasimpan);
				if($ceksatu==1){
					$setsim="ok";	
				}else{
					$totalcek++;
				}
		
			}
		}else{
			if($cekdata->num_rows() > 1){
				$statussimpan=1;
			}else{
			$datasimpan=array_merge($dataubah,$data);
			$ceksatu=$this->aksesappborang_m->editData($id,$datasimpan);
				if($ceksatu==1){
					$setsim="ok";	
				}else{
					$totalcek++;
				}
			}
		}	
		
	
	
	$status = $this->db->trans_status();
	if($statussimpan>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal, Data Unit Sudah Ada","status" => "error"));
	}elseif($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="upper(a.kode_approvel) like upper('%$search%')   or upper(b.nm_prodi) like upper('%$search%')   ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->aksesappborang_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->aksesappborang_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->aksesappborang_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   

}?>