<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class prodi extends CI_Controller {

    function prodi()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('masterdata/prodi_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
	   //echo "xx";exit;
    	$this->load->view('masterdata/prodi_v');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$nm_prodi = $this->input->post('nm_prodi');
	$level = $this->input->post('level');
	$id_jenjangprodi = $this->input->post('id_jenjangprodi');
	$set = $this->input->post("set");
	$parent = $this->input->post('parent');
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	$data=array(
	'nm_prodi'=>$nm_prodi,
	'level'=>$level,
	'parent'=>$parent,
	'id_jenjangprodi'=>$id_jenjangprodi);	
	
	if($set==0){
		$datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->prodi_m->simpanData($datasimpan);
	}else{
		$datasimpan=array_merge($dataubah,$data);
		$ceksatu=$this->prodi_m->editData($id,$datasimpan);
	}	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.nm_prodi) like upper('%$search%')  or upper(c.nm_jenjangprodi) like upper('%$search%') ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->prodi_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->prodi_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->prodi_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function getCombo(){
	   $arrayMenu=array();
		$arauSatu =array();
		$arrayDua=array();
		$arrayTiga=array();
		$menusatu=$this->prodi_m->getProdiutama(); 
		//print_r($menusatu);
		foreach($menusatu as $datasatu){ 
			$idmenu=$datasatu->id;
			$menudes=$datasatu->nm_prodi;

			$arauSatu['id'] = $idmenu;
			$arauSatu['text'] = $menudes;
			 $arauSatu['children'] = array();
			 
			$dataarray=$this->prodi_m->prodiDetail($idmenu);
		//	echo count ($dataarray)."<br>";
			foreach($dataarray as $data){ 
			
			$iddua=$data->id;
			$textdua=$data->nm_prodi;
			$arrayDua['id']=$iddua;
			$arrayDua['text']=$textdua;
			$arrayDua['children']=array();
			
			$dataarraytiga=$this->prodi_m->prodiDetail($iddua);
				foreach($dataarraytiga as $datatiga){ 
					$idtiga=$datatiga->id;
					$texttiga=$datatiga->nm_prodi;
					$arrayTiga['id']=$idtiga;
					$arrayTiga['text']=$texttiga;
					//$arrayTiga['children']=1;
					array_push($arrayDua['children'],$arrayTiga);
				}
			
			
			
			
			array_push($arauSatu['children'],$arrayDua);
			
			}
			
			array_push($arrayMenu,$arauSatu);
		}
		
		
		
		 
		$jsonData = json_encode($arrayMenu);
		
		echo $jsonData; 
		
   }
   
   

}?>