<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class grupmenu extends CI_Controller {

    function grupmenu()
	{
		parent::__construct();
		$this->load->model(array('masterdata/grupmenu_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
		
		$data['menuparen']= $this->global_m->getParent();
	  foreach($data['menuparen'] as $menudet){
	  	$idmenu=$menudet->id; 
		$data['detmenu'][$idmenu] = $this->global_m->getMenudet($idmenu);
		$data['jmldet'][$idmenu]=$this->global_m->getJumlahMenudet($idmenu);
		
		
	  }
	          
		$data['rool']= $this->global_m->getRool();
	   $this->load->view('atas_v',$data);
    	$this->load->view('masterdata/grupmenu_v');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$username = $this->input->post('username');
	$password = paramEncrypt($this->input->post('password'));  //md5($this->input->post('password'));
	$idrool = $this->input->post('idrool');
	$id_perusahaan = $this->input->post('id_perusahaan');
	$tgl=date('Y-m-d');
	$id_user=$this->session->userdata('userid');	
	$set = $this->input->post("set");
	$ceksatu="";
	
	if($set==0){
		
		
		$this->db->select('user.*');
		$this->db->from('user');
		$this->db->where('username',$username);	
		$hasil = $this->db->get();
		
		if($hasil->num_rows() > 0){
			$totalcek=1;
		}else{
		$data=array(
		'username'=>$username,
		'password'=>$password,
		'idrool'=>$idrool,
		'id_perusahaan'=>$id_perusahaan,
		'user_c'=>$id_user,
		'tgl_c'=>$tgl);
	
	$ceksatu=$this->user_m->simpanData($id,$data);
	$iduser=mysql_insert_id();
		}
	}else{
		$data=array(
		'username'=>$username,
		'password'=>$password,
		'idrool'=>$idrool,
		'id_perusahaan'=>$id_perusahaan,
		'user_u'=>$id_user,
		'tgl_u'=>$tgl);
	
		$ceksatu=$this->user_m->editData($id,$data);
		$iduser=$id;
	}	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	if ($totalcek==0){
	if($set==0){
		$datamenu=$this->user_m->getMenurool($idrool);
			 foreach($datamenu as $data){
				$id_menu=$data->id_menu;
				$tampil=$data->tampil;
				$simpan=$data->simpan;
				$ubah=$data->ubah;
				$hapus=$data->hapus;
				$approve=$data->approve;
				$cetak=$data->cetak;
				$aktivasi=$data->aktivasi;
				
				$datarol=array(
				 'id_user'=>$iduser,
				 'id_menu'=>$id_menu,
				  'tampil'=>$tampil,
				  'simpan'=>$simpan,
				  'ubah'=>$ubah,
				  'hapus'=>$hapus,
				  'approve'=>$approve,
				  'cetak'=>$cetak,
				  'aktivasi'=>$aktivasi
				 );
				 
				$cektiga=$this->user_m->simpanDatadetdua($datarol);
				if($cektiga==""){
					$totalcek=$totalcek+1;
					break;
				}
				
			 }
	}else{
	
		$hasil=$this->user_m->getMenudata($iduser,$idrool);
	//	echo $hasil->num_rows();exit;
		if($hasil->num_rows() < 1){
		
			$this->user_m->hapusDatamenu($iduser);
			$datamenu=$this->user_m->getMenurool($idrool);
			 foreach($datamenu as $data){
				$id_menu=$data->id_menu;
				$tampil=$data->tampil;
				$simpan=$data->simpan;
				$ubah=$data->ubah;
				$hapus=$data->hapus;
				$approve=$data->approve;
				$cetak=$data->cetak;
				$aktivasi=$data->aktivasi;
				
				$datarol=array(
				 'id_user'=>$iduser,
				 'id_menu'=>$id_menu,
				  'tampil'=>$tampil,
				  'simpan'=>$simpan,
				  'ubah'=>$ubah,
				  'hapus'=>$hapus,
				  'approve'=>$approve,
				  'cetak'=>$cetak,
				  'aktivasi'=>$aktivasi
				 );
				 
				$cektiga=$this->user_m->simpanDatadetdua($datarol);
				if($cektiga==""){
					$totalcek=$totalcek+1;
					break;
				}
				
			 }
			 
			 
			
		}
	
	}
	}
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="upper(a.username) like upper('%$search%') 
			  or upper(b.nm_rol) like upper('%$search%') or upper(c.nm_perusahaan) like upper('%$search%')  ";
			 }else{
			 $where='id is not null';
			 }
		 
        $this->user_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 	 
	function aktivData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->user_m->aktivData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Aktivasi data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Aktivasi Data Gagal","status" => "error"));
			}
	
   }
   function aktivDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
			if($data[$row]!=""){
				$ceksatu=$this->user_m->aktivData($data[$row]);
				if($ceksatu==1){
					$setsim="ok";	
				}else{
					$totalcek++;
				}
			}
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Aktivasi Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Aktivasi Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Aktivasi data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   
   function cekDatamenu($id){
   	$sql = "SELECT * from user_menu where id_user='$id'";
//	echo $sql;
	$hasil = $this->db->query($sql);
		if($hasil->num_rows() > 0){
			echo "ada";
		}
		else{
			echo "kosong";
		}
   }
    function editDatamenu(){
   	$id=$_REQUEST['id'];
	$kondisi=$_REQUEST['kondisi'];
	
	if($kondisi==1){
		$sql = "SELECT rm.id_menu as id_menuok,mx.id as idmenu,rm.tampil 
from rol_menu as rm RIGHT JOIN 
(SELECT id,menudes,no FROM menu WHERE parent='0') 
mx on mx.id=rm.id_menu and rm.id_rol='$id' and rm.tampil!='0' ORDER BY mx.no asc";
	}else{
		$sql = "SELECT rm.id_menu as id_menuok,mx.id as idmenu,rm.tampil 
from user_menu as rm RIGHT JOIN 
(SELECT id,menudes,no FROM menu WHERE parent='0') 
mx on mx.id=rm.id_menu and rm.id_user='$id' and rm.tampil!='0' ORDER BY mx.no asc";
	}

			
			
			//echo $sql;
        $hasil = mysql_query($sql);
		$data = array();

		while ($row = mysql_fetch_assoc($hasil))
			$data[] = $row;

		$json = json_encode($data);
		 
		echo $json;
	   
   
   }
  
  function CarieditDatamenu(){
  		$id=$_REQUEST['id'];
		$id_menu=$_REQUEST['id_menu'];
		$kondisi=$_REQUEST['kondisi'];
		if($kondisi==1){
			$sql = "SELECT rm.id_menu as id_menuok,mx.id as idmenu,rm.tampil,rm.simpan,rm.ubah,rm.hapus,rm.cetak,rm.approve,rm.aktivasi 
from rol_menu as rm RIGHT JOIN 
(SELECT id,menudes,no FROM menu WHERE parent='$id_menu') 
mx on mx.id=rm.id_menu and rm.id_rol='$id'  ORDER BY mx.no asc";
			
		}else{
			$sql = "SELECT rm.id_menu as id_menuok,mx.id as idmenu,rm.tampil,rm.simpan,rm.ubah,rm.hapus,rm.cetak,rm.approve,rm.aktivasi 
	from user_menu as rm RIGHT JOIN 
	(SELECT id,menudes,no FROM menu WHERE parent='$id_menu') 
	mx on mx.id=rm.id_menu and rm.id_user='$id'  ORDER BY mx.no asc";
			}
			
		//s	echo $sql;
        $hasil = mysql_query($sql);
		$data = array();

		while ($row = mysql_fetch_assoc($hasil))
			$data[] = $row;

		$json = json_encode($data);
		 
		echo $json;
  }
   
   
   function simpanDatadetail(){
   		$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	
	$id_userrol = $this->input->post('id_userrol');

	$tgl=date('Y-m-d');
	$id_user=$this->session->userdata('id_user');
	

	
	$cekhapus=$this->user_m->hapusRoolmenu($id_userrol);
	if($cekhapus==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	if(isset($_POST['detail'])){
		$detail=$this->input->post('detail');
		foreach ( $detail as $detaildata ) {
			if(isset($detaildata['menusatu'])){
				$id_menu=$detaildata['id_menu'];
				$tampil="1";
				$cekdua=$this->user_m->simpanDatadet($id_userrol,$id_menu,$tampil);
				if($cekdua==""){
					$totalcek=$totalcek+1;
					break;
				}
				
				if(isset($detaildata['detaildua'])){
					$datadua=$detaildata['detaildua'];
					foreach ( $datadua as $detdua ) {
						$id_menudua=$detdua['menudet'];
						if(isset($detdua['tampil'])){
							$tampildet="1";
							if(isset($detdua['simpan'])){
								$simpan=1;
							}else{
								$simpan=0;
							}
							
							if(isset($detdua['simpan'])){
								$simpan=1;
							}else{
								$simpan=0;
							}
							
							if(isset($detdua['ubah'])){
								$ubah=1;
							}else{
								$ubah=0;
							}
							
							if(isset($detdua['hapus'])){
								$hapus=1;
							}else{
								$hapus=0;
							}
							
							if(isset($detdua['approve'])){
								$approve=1;
							}else{
								$approve=0;
							}
							
							if(isset($detdua['cetak'])){
								$cetak=1;
							}else{
								$cetak=0;
							}
							
							if(isset($detdua['aktivasi'])){
								$aktivasi=1;
							}else{
								$aktivasi=0;
							}
							
							$datarol=array(
							 'id_user'=>$id_userrol,
							 'id_menu'=>$id_menudua,
							  'tampil'=>$tampildet,
							  'simpan'=>$simpan,
							  'ubah'=>$ubah,
							  'hapus'=>$hapus,
							  'approve'=>$approve,
							  'cetak'=>$cetak,
							  'aktivasi'=>$aktivasi
							 );
							$cektiga=$this->user_m->simpanDatadetdua($datarol);
							if($cektiga==""){
								$totalcek=$totalcek+1;
								break;
							}
							
							
								
						}
					}
				
				}
				
			}
			
			
		}
		
		
			}
	
	
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array('pesan' => 'Informasi <br> Simpan Data Gagal','status' => 'danger'));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array('pesan' => 'Informasi <br> Simpan Data Gagal','status' => 'danger'));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array('pesan' => 'Informasi <br> Simpan data Berhasil','status' => 'success'));
		}
	return $status;	
   }
   
    
}?>