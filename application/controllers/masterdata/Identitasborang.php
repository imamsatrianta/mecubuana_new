<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Identitasborang extends CI_Controller {

    function Identitasborang()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('masterdata/identitasborang_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
	   //echo "xx";exit;
    	$this->load->view('masterdata/identitasborang_v');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$set = $this->input->post('set');
	$id_jenjang = $this->input->post('id_jenjang');
	$id_prodi = $this->input->post('id_prodi');
	$tgl = $this->input->post('tgl');
	$nm_perguruantingi = $this->input->post('nm_perguruantingi');
	$no_sk_pendirian = $this->input->post("no_sk_pendirian");
	$tgl_sk_pendirian = $this->input->post('tgl_sk_pendirian');
	$pejabat_penandatangan_sk = $this->input->post('pejabat_penandatangan_sk');
	$bulan_tahun_peneyelengara = $this->input->post('bulan_tahun_peneyelengara');
	$no_skijin_op = $this->input->post('no_skijin_op');
	$tgl_sk_op = $this->input->post('tgl_sk_op');
	$peringkat_ak_terakhir = $this->input->post('peringkat_ak_terakhir');
	$no_sk_banpt = $this->input->post('no_sk_banpt');
	$alamat_ps = $this->input->post('alamat_ps');
	$no_telp_ps = $this->input->post('no_telp_ps');
	$no_fak_ps = $this->input->post('no_fak_ps');
	$hp_dan_email_ps = $this->input->post('hp_dan_email_ps');
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	$data=array(
	'id_jenjang'=>$id_jenjang,
	'id_prodi'=>$id_prodi,
	'tgl'=>$tgl,
	'nm_perguruantingi'=>$nm_perguruantingi,
	'no_sk_pendirian'=>$no_sk_pendirian,
	'tgl_sk_pendirian'=>$tgl_sk_pendirian,
	'pejabat_penandatangan_sk'=>$pejabat_penandatangan_sk,
	'bulan_tahun_peneyelengara'=>$bulan_tahun_peneyelengara,
	'no_skijin_op'=>$no_skijin_op,
	'tgl_sk_op'=>$tgl_sk_op,
	'peringkat_ak_terakhir'=>$peringkat_ak_terakhir,
	'no_sk_banpt'=>$no_sk_banpt,
	'alamat_ps'=>$alamat_ps,
	'no_telp_ps'=>$no_telp_ps,
	'no_fak_ps'=>$no_fak_ps,
	'hp_dan_email_ps'=>$hp_dan_email_ps);	
	
	if($set==0){
		$datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->identitasborang_m->simpanData($datasimpan);
	}else{
		$datasimpan=array_merge($dataubah,$data);
		$ceksatu=$this->identitasborang_m->editData($id,$datasimpan);
	}	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.nidn) like upper('%$search%')   or upper(a.nm_karyawan) like upper('%$search%') )  ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->identitasborang_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->identitasborang_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->identitasborang_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function simpanDatadosen(){
	   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id_iddos");
	$set = $this->input->post('setkeg');
	$id_identitas = $this->input->post('id_identitas');
	$id_dosen = $this->input->post('id_dosen');
	$tgl_pengisian = $this->input->post('tgl_pengisian');
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	$data=array(
	'id_identitas'=>$id_identitas,
	'id_dosen'=>$id_dosen,
	'tgl_pengisian'=>$tgl_pengisian);	
	
	if($set==0){
		$datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->identitasborang_m->simpanDatadosen($datasimpan);
	}else{
		$datasimpan=array_merge($dataubah,$data);
		$ceksatu=$this->identitasborang_m->editDatadosen($id,$datasimpan);
	}	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
   }
   function loaddatadetail(){
	   $id = $this->input->get("id");
	   $this->identitasborang_m->loaddatadetail($id); 
	   
   
   }
   
   function hapusdatadosen(){
	   $id = $this->input->get("id");
		 $ceksatu=$this->identitasborang_m->hapusdatadosen($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
   }
   

}?>