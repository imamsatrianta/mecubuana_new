<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class identitasborang3b extends CI_Controller {

    function identitasborang3b()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('masterdata/identitasborang3b_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
	   //echo "xx";exit;
    	$this->load->view('masterdata/identitasborang3b_v');
		$this->load->view('masterdata/identitasborang3b_js');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$set = $this->input->post('set');
	$id_jenjang = $this->input->post('id_jenjang');
	$id_prodi = $this->input->post('id_prodi');
	$tgl = $this->input->post('tgl');
	$alamat_ps = $this->input->post('alamat_ps');
	$no_telp_ps = $this->input->post('no_telp_ps');
	$no_fak_ps = $this->input->post('no_fak_ps');
	$nm_perguruantingi = $this->input->post('nm_perguruantingi');
	$no_sk_pendirian = $this->input->post("no_sk_pendirian");
	$tgl_sk_pendirian = $this->input->post('tgl_sk_pendirian');
	$pejabat_penerbit_sk = $this->input->post('pejabat_penerbit_sk');
	$alamat_pengelola = $this->input->post('alamat_pengelola');
	$unit_pengelola = $this->input->post('unit_pengelola');
	$no_telp_pengelola = $this->input->post('no_telp_pengelola');
	$no_fax_pengelola = $this->input->post('no_fax_pengelola');
	$pejabat_sk_pengelola = $this->input->post('pejabat_sk_pengelola');
	
	$homepage_pengelola= $this->input->post('homepage_pengelola');
	$email_pengelola = $this->input->post('email_pengelola');
	$homepage= $this->input->post('homepage');
	$email = $this->input->post('email');
	$no_skpengelola = $this->input->post('no_skpengelola');
	$tgl_sk_pengelola = $this->input->post('tgl_sk_pengelola');
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	$data=array(
	'id_jenjang'=>$id_jenjang,
	'id_prodi'=>$id_prodi,
	'tgl'=>$tgl,
	'nm_perguruantingi'=>$nm_perguruantingi,
	'no_sk_pendirian'=>$no_sk_pendirian,
	'tgl_sk_pendirian'=>$tgl_sk_pendirian,
	'pejabat_penerbit_sk'=>$pejabat_penerbit_sk,
	'alamat_ps'=>$alamat_ps,
	'no_telp_ps'=>$no_telp_ps,
	'no_fak_ps'=>$no_fak_ps,
	'alamat_pengelola'=>$alamat_pengelola,
	'unit_pengelola'=>$unit_pengelola,
	'no_telp_pengelola'=>$no_telp_pengelola,
	'no_fax_pengelola'=>$no_fax_pengelola,
	'homepage'=>$homepage,
	'email'=>$email,
	'homepage_pengelola'=>$homepage_pengelola,
	'email_pengelola'=>$email_pengelola,
	'no_skpengelola'=>$no_skpengelola,
	'tgl_sk_pengelola'=>$tgl_sk_pengelola,
	'pejabat_sk_pengelola'=>$pejabat_sk_pengelola);	
	
	if($set==0){
		$datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->identitasborang3b_m->simpanData($datasimpan);
		$iddet=mysql_insert_id();
	}else{
		$datasimpan=array_merge($dataubah,$data);
		$ceksatu=$this->identitasborang3b_m->editData($id,$datasimpan);
		$iddet=$id;
		$this->identitasborang3b_m->hapusDatadetail($iddet);
	}	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
		if (isset($_POST['detail'])) {
			$detail = $this->input->post('detail');
			 foreach ($detail as  $detaildata ) {
				 $datadetdua = array(
						'id_identitas' => $iddet,
						'id_jenjang' => trim($detaildata['id_jenjang_det']),
						'id_prodi' => trim($detaildata['id_prodi_det']),
						'user_input' => $username,
						'tgl_input'=>$tgl
					);
					$ceksatu=$this->identitasborang3b_m->simpanDatadetail($datadetdua);
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
			 }
		}
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.nidn) like upper('%$search%')   or upper(a.nm_karyawan) like upper('%$search%') )  ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->identitasborang3b_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 $this->identitasborang3b_m->hapusDatadosenborang($id);
		 $this->identitasborang3b_m->hapusDatadetail($id);
		 $ceksatu=$this->identitasborang3b_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$this->identitasborang3b_m->hapusDatadosenborang($data[$row]);
		$this->identitasborang3b_m->hapusDatadetail($data[$row]);
		$ceksatu=$this->identitasborang3b_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function simpanDatadosen(){
	   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id_iddos");
	$set = $this->input->post('setkeg');
	$id_identitas = $this->input->post('id_identitas');
	$id_dosen = $this->input->post('id_dosen');
	$tgl_pengisian = $this->input->post('tgl_pengisian');
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	$data=array(
	'id_identitas'=>$id_identitas,
	'id_dosen'=>$id_dosen,
	'tgl_pengisian'=>$tgl_pengisian);	
	
	if($set==0){
		$datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->identitasborang3b_m->simpanDatadosen($datasimpan);
	}else{
		$datasimpan=array_merge($dataubah,$data);
		$ceksatu=$this->identitasborang3b_m->editDatadosen($id,$datasimpan);
	}	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
   }
   function loaddatadetail(){
	   $id = $this->input->get("id");
	   $this->identitasborang3b_m->loaddatadetail($id); 
	   
   
   }
   
   function hapusdatadosen(){
	   $id = $this->input->get("id");
		 $ceksatu=$this->identitasborang3b_m->hapusdatadosen($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
   }
   
   function getDetail(){
	    $id = $this->input->get("id");
	   echo $this->identitasborang3b_m->getDetail($id); 
   }
   

}?>