<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tiga_dua_satu extends CI_Controller {

    function tiga_dua_satu()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('d33b/tiga_dua_satu_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		$level=$_GET['level'];
		$data['idm']=$_GET['idm'];
		$data['level']=$_GET['level'];
		$data['menu']=$this->tiga_dua_satu_m->loadDatamenu($level);
	   $this->load->view('atas_v');
	   //echo "xx";exit;
		$this->load->view('borang3b/standarsatumenu_v',$data);
	
			$parameter=$_GET['parameter'];
			$data['uraian']=$this->global_m->loadDatamenusatu($parameter);
			//echo $data['uraian'];
			//exit;
			$this->load->view('d33b/tiga_dua_satu_v',$data);
			$this->load->view('d33b/tiga_dua_satu_js');
    	$this->load->view('bawah');
	}
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
	   $parameter= $this->input->get("parameter");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(b.nm_jenjangprodi) like upper('%$search%')   or upper(c.nm_tahun_akademik) like upper('%$search%')
			or upper(d.nm_prodi) like upper('%$search%') or upper(a.uraian) like upper('%$search%')  ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->tiga_dua_satu_m->loaddataTabel($offset,$limit,$order,$where,$parameter); 
     
    } 
	
	function simpanData(){
	$iddet="";
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$setsmpan=0;
	$id = $this->input->post("id");
	$set = $this->input->post("set");
	$id_tahunakademik= $this->input->post("id_tahunakademik");
	$id_parameter = $this->input->post('id_parameter');
	$id_jenjangprodi = $this->input->post('id_jenjangprodi');
	$id_prodiarr = $this->input->post('id_prodi');
	$prodi=explode('-',$id_prodiarr);
	$id_prodi=$prodi[0]; 
	$kd_server=$prodi[1]; 
	$tanggal=$this->input->post('tanggal');
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');

	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	$data=array(
		'id_tahunakademik'=>$id_tahunakademik,
		'id_parameter'=>$id_parameter,
		'id_jenjangprodi'=>$id_jenjangprodi,
		'id_prodi'=>$id_prodi,
		'tanggal'=>$tanggal);
	$ceksatu="";	
	if($set==0){
	$datasimpan=array_merge($datainput,$data);
	$cekborang=$this->tiga_dua_satu_m->cekBorang($id_tahunakademik,$id_parameter,$id_jenjangprodi,$id_prodi);
	if (!empty($cekborang)) {
		$setsmpan++;
	}else{
	//print_r($datasimpan); exit;
	$idinsert=$this->tiga_dua_satu_m->simpanData($datasimpan);
	$iddet=$idinsert;
	
	}
		
	}else{
		
		$datasimpan=array_merge($dataubah,$data);
		$ceksatu=$this->tiga_dua_satu_m->editData($id,$datasimpan);
		$iddet=$id;
		$this->tiga_dua_satu_m->hapusDetail($id);
		
	}
	
	
	
/*
	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}*/
	
	if (isset($_POST['detail'])) {
			$detail = $this->input->post('detail');
			 foreach ($detail as  $detaildata ) {
				 $datadetdua = array(
						'id_borangd3' => $iddet,
						'id_prodi' => trim($detaildata['id_prodidet']),
						'nm_prodi'=>trim($detaildata['nm_prodi']),
						'rata_masastudi'=>trim($detaildata['rata_masastudi']),
						'rata_ipk'=>trim($detaildata['rata_ipk']),
						'keterangan'=>trim($detaildata['keterangan'])
					);
					$datasimpan=array_merge($datadetdua,$datainput);
					$ceksatu=$this->tiga_dua_satu_m->simpanDatadetail($datasimpan);
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
			 }
		}
		
		
	
	
	$status = $this->db->trans_status();
	if($setsmpan>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal, Data Sudah Ada","status" => "error"));
	}else if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
   
   function hapusData(){
	    $id = $this->input->get("id");
		$this->tiga_dua_satu_m->hapusKebijakanheder($id);
		$this->tiga_dua_satu_m->hapusDetail($id);
		 $ceksatu=$this->tiga_dua_satu_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row]; br_3bd3_kebijakan
		$this->tiga_dua_satu_m->hapusKebijakanheder($data[$row]);
		$this->tiga_dua_satu_m->hapusDetail($data[$row]);
		$ceksatu=$this->tiga_dua_satu_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   function getEditdetail(){
	   $id = $this->input->get("id");
	   echo $this->tiga_dua_satu_m->getEditdetail($id); 
   }
  
   function getDataserver(){
	   $id_prodi = $this->input->get("id_prodi");
	   $id_tahunakademik = $this->input->get("id_tahunakademik");
	   echo $this->tiga_dua_satu_m->getDataserver($id_prodi); 
   }
   
   function simpanDatarefisi(){
	$this->db->trans_begin();
	$status=true;
	$totalcek=0; 
	$setsimpan=0;
	$id = $this->input->post("id_ref");
	$id_prodi = $this->input->post("id_prodi_ref"); 
	$status = $this->input->post("status");
	//$tgl_status = $this->input->post('tgl_status');
	$note_status = $this->input->post('note_status');
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	$datarefisi=array(
		'id_borang'=>$id,
		'standar'=>'3',
		'jenis'=>'1',
		'tgl'=>$tgl,
		'keterangan'=>$note_status);
	$data=array(
		'status'=>$status,
		'note_status'=>$note_status);
	
		$datasimpan=array_merge($datainput,$data);
		//print_r($datasimpan); exit;
		$ceksatu=$this->tiga_dua_satu_m->editData($id,$datasimpan);
		
		$datasimpanref=array_merge($datainput,$datarefisi);
		//print_r($datasimpan); exit;
		$ceksatu=$this->tiga_dua_satu_m->simpanDatarefisi($datasimpanref);
		
		
		
		
		if($ceksatu==1){
			$setsim="ok";	
		}else{
			$totalcek++;
		}
	
	

	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
   }
   
   // kebijakan
   function simpanDatakebijakan(){
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id_borang = $this->input->post("id_borang");
	$setsit = $this->input->post("setsit");
	$id_keb = $this->input->post("id_keb");
	
	$no = $this->input->post('no');
	$judul_lampiran = $this->input->post('judul_lampiran'); 
	$imgdata = $this->input->post('imgdata'); 

	
	$setgbr=$this->input->post('setgbr');
	$ceksatu="";
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	
	
	if( $setgbr==1){	
	$allowedExts = array("jpeg", "jpg", "pdf");
			$temp = explode(".", $_FILES["imgdata"]["name"]);
			$extension = end($temp);
			if ((($_FILES["imgdata"]["type"] == "image/jpeg")
			|| ($_FILES["imgdata"]["type"] == "image/jpg")
			|| ($_FILES["imgdata"]["type"] == "image/pjpeg")
			|| ($_FILES["imgdata"]["type"] == "application/pdf"))
			&& ($_FILES["imgdata"]["size"] < 200000000)
			&& in_array($extension, $allowedExts)) {
			//echo $extension."xxx";
			 $acak           = rand(000000,999999);
			$filename =$_FILES["imgdata"]["name"];
			$dataimage = $acak.$filename;
				
			move_uploaded_file($_FILES["imgdata"]["tmp_name"],
					"kebijakanborang/" . $dataimage);
			
			//echo "ok";
			}else{
			$dataimage="";
			}
	}else{
			$dataimage="";
			}		
			
			
	if($setsit==0){
		$data=array(
		'no'=>$no,
		'id_borang'=>$id_borang,
		'lampiran'=>$dataimage,
		'judul_lampiran'=>$judul_lampiran);
		$dataok=array_merge($datainput,$data);
		$ceksatu=$this->tiga_dua_satu_m->simpanDatakeb($dataok);
		
		
	}else{
		if($dataimage==""){
			$data=array(
			'no'=>$no,
			'id_borang'=>$id_borang,
			'judul_lampiran'=>$judul_lampiran);
			
		}else{
			$data=array(
			'no'=>$no,
			'id_borang'=>$id_borang,
			'lampiran'=>$dataimage,
			'judul_lampiran'=>$judul_lampiran);
		}
		$dataok=array_merge($dataubah,$data);
		$ceksatu=$this->tiga_dua_satu_m->editDatakeb($id_keb,$dataok);
	}	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	  
  }
   function loaddatakebijakan(){
	   $id = $this->input->get("id");
	   $this->tiga_dua_satu_m->loaddatakebijakan($id); 
   }

   function hapusKebijakan(){
	   $id = $this->input->get("id");
		 $ceksatu=$this->tiga_dua_satu_m->hapusKebijakan($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
   }
   
   
}?>