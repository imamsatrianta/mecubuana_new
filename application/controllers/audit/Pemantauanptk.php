<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pemantauanptk extends CI_Controller {

    function Pemantauanptk()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('audit/Pemantauanptk_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
    	$this->load->view('audit/Pemantauanptk_v');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$set = $this->input->post("set");
	
	$id_unit= $this->input->post("id_unit");
	$id_ketuatim=$this->input->post("id_ketuatim");
	$id_auditie=$this->input->post("id_auditie");
	$id_standar=$this->input->post("id_standar");
	$periode = $this->input->post("periode");
	$kts_ob=$this->input->post("kts_ob");
	$pernyataan=$this->input->post("pernyataan");
	$ptk_no = $this->input->post('ptk_no');
	$kategori = $this->input->post('kategori');
	$uraian_temuan=$this->input->post("uraian_temuan");
	$rencana_penyelesaian=$this->input->post("rencana_penyelesaian");
	$realisasi=$this->input->post('realisasi');
    $pj_ptk=$this->input->post('pj');
	$jadwal = $this->input->post("jadwal");
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');

	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	$data=array(
		'id_unit'=>$id_unit,
		'id_ketuatim'=>$id_ketuatim,
		'id_auditie'=>$id_auditie,
		'id_standar'=>$id_standar,
		'periode'=>$periode,
		'ktsob'=>$kts_ob,
		'jadwal'=>$jadwal,
		'pernyataan'=>$pernyataan,
		'ptk_no'=>$ptk_no,
		'kategori_ptk'=>$kategori,
		'uraian_temuan'=>$uraian_temuan,
		'rencana_penyelesaian'=>$rencana_penyelesaian,
		'realisasi_ptk'=>$realisasi,
        'pj_ptk'=>$pj_ptk);
		
	if($set==0){
	$datasimpan=array_merge($datainput,$data);
	//print_r($datasimpan); exit;
	$ceksatu=$this->Pemantauanptk_m->simpanData($datasimpan);
		
	}else{
		
                $datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->Pemantauanptk_m->editData($id,$datasimpan);
	}
	
	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.no_ptk) like upper('%$search%')   or upper(a.kategori) like upper('%$search%') 
                          or upper(a.penyelesaian) like upper('%$search%') or upper(a.realisasi) like upper('%$search%')
			 or upper(a.pj_ptk) like upper('%$search%') or upper(b.nm_unit) like upper('%$search%') ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->Pemantauanptk_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->Pemantauanptk_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->Pemantauanptk_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }

}?>