<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Persiapanformulir extends CI_Controller {

    function Persiapanformulir()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('audit/Persiapanformulir_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
	   //echo "xx";exit;
    	$this->load->view('audit/Persiapanformulir_v');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$set = $this->input->post("set");
	
	$id_standar = $this->input->post('id_standar');
	$materi = $this->input->post('materi');
	
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	// $setgbr=$this->input->post('setgbr');
	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
        $data=array(
                'id_standar'=>$id_standar,
                'materi'=>$materi
                );
	if($set==0){
		$datasimpan=array_merge($datainput,$data);
	//print_r($datasimpan); exit;
		$ceksatu=$this->Persiapanformulir_m->simpanData($datasimpan);
		$iddet=mysql_insert_id();
		
	}else{
		
		$datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->Persiapanformulir_m->editData($id,$datasimpan);
		$iddet=$id;
		$this->Persiapanformulir_m->hapusDetail($iddet);
	}
	
	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
		if (isset($_POST['detail'])) {
			$detail = $this->input->post('detail');
			 foreach ($detail as  $detaildata ) {
				 $datadetdua = array(
						'id_persiapan_formulir' => $iddet,
						'document' => trim($detaildata['document']),
						'user_input' => $username,
						'tgl_input'=>$tgl
					);
					$ceksatu=$this->Persiapanformulir_m->simpanDatadetail($datadetdua);
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
			 }
		}
			
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(b.nm_dokumen) like upper('%$search%')   or upper(a.materi) like upper('%$search%') ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->Persiapanformulir_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->Persiapanformulir_m->hapusData($id);
                 $cekdua=$this->Persiapanformulir_m->hapusDetail($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->Persiapanformulir_m->hapusData($data[$row]);
                $cekdua=$this->Persiapanformulir_m->hapusDetail($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function getDetail(){
	   $id = $this->input->get("id");

	   //echo $this->renstra_m->getDetail($id); 
           echo $this->Persiapanformulir_m->getDetail($id); 
   }
   
   function export() {
		$date = date('dmY');
		header("Content-type=appalication/vnd.ms-excel");
		header("content-disposition:attachment;filename=laporan_persiapanformulir_".$date.".xls");
		$data['record'] = $this->Persiapanformulir_m->export_excel();
		$this->load->view('audit/laporan_persiapanformulir_v',$data);
	}
   
   

}?>