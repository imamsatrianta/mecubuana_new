<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Persiapanpelaksanaan extends CI_Controller {

    function Persiapanpelaksanaan()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('audit/Persiapanpelaksanaan_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	  	$this->load->view('atas_v');
		$this->load->view('audit/Persiapanpelaksanaan_v');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$set = $this->input->post("set");
	$periode = $this->input->post("periode");
	$jadwal= $this->input->post("jadwal");
	$id_planingaudit= $this->input->post("id_plan");
	$id_planingaudit= $this->input->post("id_plan");
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');

	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	$data=array(
		'jadwal'=>$jadwal,
		'periode'=>$periode,
		'id_planingaudit'=>$id_planingaudit);
		
	if($set==0){
	$datasimpan=array_merge($datainput,$data);
	//print_r($datasimpan); exit;
	$ceksatu=$this->Persiapanpelaksanaan_m->simpanData($datasimpan);
		$iddet=mysql_insert_id();
	}else{
		
        $datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->Persiapanpelaksanaan_m->editData($id,$datasimpan);
		$iddet=$id;
		$this->Persiapanpelaksanaan_m->hapusstandar($iddet);
	}
	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	if (isset($_POST['standar'])) {
			$standar = $this->input->post('standar');
			 foreach ($standar as  $standardata ) {
				 $datadetdua = array(
						'id_persiapan_pelaksanaan' => $iddet,
						'id_standar' => $standardata['idstn'],
						'user_input' => $username,
						'tgl_input'=>$tgl
					);
					$ceksatu=$this->Persiapanpelaksanaan_m->simpanDatastandar($datadetdua);
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
			 }
                } else {}
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.jadwal) like upper('%$search%')   or upper(b.periode) like upper('%$search%') 
                          or upper(b.tglmulai) like upper('%$search%') or upper(b.tglselesai) like upper('%$search%')
			or upper(c.nm_unit) like upper('%$search%') or upper(d.nm_karyawan) like upper('%$search%')  ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->Persiapanpelaksanaan_m->loaddataTabel($offset,$limit,$order,$where); 
	} 
	function loaddatajadwal(){
		$offset = $this->input->get("offset");
		$limit = $this->input->get("limit");
		$order = $this->input->get("order");
		  if($this->input->get("search")){
			  $search = $this->input->get("search");
			  $where="(upper(b.tglmulai) like upper('%$search%')) ";
			  }else{
			  $where="a.id is not null";
			  }
		  
		$this->Persiapanpelaksanaan_m->loaddatajadwal($offset,$limit,$order,$where); 
	}
	function loaddatastandar(){
		$offset = $this->input->get("offset");
		$limit = $this->input->get("limit");
		$order = $this->input->get("order");
		$idstandar = $this->input->get("idstandar");
		  if($this->input->get("search")){
			  $search = $this->input->get("search");
			  $where="(upper(b.document) like upper('%$search%')) ";
			  }else{
			  $where="a.id is not null";
			  }
		  
		$this->Persiapanpelaksanaan_m->loaddatastandar($offset,$limit,$order,$where,$idstandar); 
	}
	function loaddatajadwal2(){
		$offset = $this->input->get("offset");
		$limit = $this->input->get("limit");
		$order = $this->input->get("order");
		  if($this->input->get("search")){
			$search = $this->input->get("search");
			$where="(upper(a.periode) like upper('%$search%')) ";
			}else{
			$where="a.id is not null";
			}
		  
		$this->Persiapanpelaksanaan_m->loaddatajadwal2($offset,$limit,$order,$where); 
	}

	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->Persiapanpelaksanaan_m->hapusData($id);
		 $cekdua=$this->Persiapanpelaksanaan_m->hapusstandar($id);
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   function getanggotaperencanaan(){
	$id = $this->input->get("id");
	echo $this->Persiapanpelaksanaan_m->getanggotaperencanaan($id); 
	}
	function getstandarperencanaan(){
	$id = $this->input->get("id");
	echo $this->Persiapanpelaksanaan_m->getstandarperencanaan($id); 
	}

   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->Persiapanpelaksanaan_m->hapusData($data[$row]);
		 $cekdua=$this->Persiapanpelaksanaan_m->hapusstandar($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}	
		}
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
}?>