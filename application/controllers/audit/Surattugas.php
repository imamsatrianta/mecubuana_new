<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Surattugas extends CI_Controller {

    function Surattugas()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('audit/Surattugas_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
        $this->load->view('atas_v');
    	$this->load->view('audit/Surattugas_v');
    	$this->load->view('bawah');
	}
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(b.nm_unit) like upper('%$search%')   or upper(c.nm_karyawan) like upper('%$search%')
			or upper(a.periode) like upper('%$search%') or upper(a.tglmulai) like upper('%$search%') 
                         or upper(a.tglselesai) like upper('%$search%') ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->Surattugas_m->loaddataTabel($offset,$limit,$order,$where); 
    } 
    function printDetail($id){
	   $data['query']=$this->Surattugas_m->export_pdf($id)->result_array();
	   $data['ketuatim']=$this->Surattugas_m->export_pdf2($id)->result_array();
	   $data['unit']=$this->Surattugas_m->export_pdf3($id)->result_array();
       $this->load->view('prinpdf/html2pdf.class.php');
       $this->load->view('audit/laporan_surattugas_v',$data);
   }

}

?>