<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Planingaudit extends CI_Controller {

    function Planingaudit()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('audit/Planingaudit_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
        $this->load->view('atas_v');
    	$this->load->view('audit/Planingaudit_v');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$set = $this->input->post("set");
	$id_unit = $this->input->post("id_unit");
	$id_ketuatim = $this->input->post('id_ketuatim');
	$periode = $this->input->post('periode');
	$tglmulai = $this->input->post('tglmulai');
	$tglselesai = $this->input->post('tglselesai');
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	$data=array(
            'id_unit' => $id_unit,
            'id_ketuatim' => $id_ketuatim,
            'periode' => $periode,
            'tglmulai' => $tglmulai,
            'tglselesai' => $tglselesai);
		
	if($set==0){
	$datasimpan=array_merge($datainput,$data);
	//print_r($datasimpan); exit;
	$ceksatu=$this->Planingaudit_m->simpanData($datasimpan);
        $iddet=mysql_insert_id();
		
	}else{
		
        $datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->Planingaudit_m->editData($id,$datasimpan);
		$iddet=$id;
		$this->Planingaudit_m->hapusanggota($iddet);
        $this->Planingaudit_m->hapusstandar($iddet);
		$this->Planingaudit_m->hapusauditie($iddet);
		$this->Planingaudit_m->hapustujuan($iddet);
        $this->Planingaudit_m->hapusjadwal($iddet);
	}

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	if (isset($_POST['anggota'])) {
			$anggota = $this->input->post('anggota');
			 foreach ($anggota as  $anggotadata ) {
				 $datadetdua = array(
						'id_planingaudit' => $iddet,
						'id_anggota' => trim($anggotadata['id_anggota']),
						'user_input' => $username,
						'tgl_input'=>$tgl
					);
					$ceksatu=$this->Planingaudit_m->simpanDataanggota($datadetdua);
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
			 }
                } else {}
        if (isset($_POST['standar'])) {
			$standar = $this->input->post('standar');
			 foreach ($standar as  $standardata ) {
				 $datadettiga = array(
						'id_planingaudit' => $iddet,
						'id_standar' => trim($standardata['id_standar']),
						'user_input' => $username,
						'tgl_input'=>$tgl
					);
					$ceksatu=$this->Planingaudit_m->simpanDatastandar($datadettiga);
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
			 }
                } else {}
        if (isset($_POST['auditie'])) {
			$auditie = $this->input->post('auditie');
			 foreach ($auditie as  $auditiedata ) {
				 $datadetempat = array(
						'id_planingaudit' => $iddet,
						'id_auditie' => trim($auditiedata['id_auditie']),
						'user_input' => $username,
						'tgl_input'=>$tgl
					);
					$ceksatu=$this->Planingaudit_m->simpanDataauditie($datadetempat);
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
			 }
				} else {}

		if (isset($_POST['jadwal'])) {
			$jadwal = $this->input->post('jadwal');
				foreach ($jadwal as  $jadwaldata ) {
					$datadetenam = array(
						'id_planingaudit' => $iddet,
						'tanggal' => trim($jadwaldata['tanggal']),
						'jam_mulai' => trim($jadwaldata['jam_mulai']),
						'jam_selesai' => trim($jadwaldata['jam_selesai']),
						'ket_jadwal' => trim($jadwaldata['ket_jadwal']),
						'user_input' => $username,
						'tgl_input'=>$tgl
					);
					$ceksatu=$this->Planingaudit_m->simpanDatajadwal($datadetenam);
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
				}
				} else {}
                 
		if (isset($_POST['tujuandet'])) {
			$tujuan = $this->input->post('tujuandet');
				foreach ($tujuan as  $tujuandata ) {
					$datadettujuh = array(
						'id_planingaudit' => $iddet,
						'tujuan' => trim($tujuandata['tujuan']),
						'user_input' => $username,
						'tgl_input'=>$tgl
					);
					$ceksatu=$this->Planingaudit_m->simpanDatatujuan($datadettujuh);
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
				}
				} else {}
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	// en of insert
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(b.nm_unit) like upper('%$search%')   or upper(c.nm_karyawan) like upper('%$search%')
			or upper(a.periode) like upper('%$search%') or upper(a.tglmulai) like upper('%$search%') 
                         or upper(a.tglselesai) like upper('%$search%') ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->Planingaudit_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		$id = $this->input->get("id");
		$ceksatu=$this->Planingaudit_m->hapusData($id);
		$cekdua=$this->Planingaudit_m->hapusanggota($id);
		$cektiga=$this->Planingaudit_m->hapusstandar($id);
		$cekempat=$this->Planingaudit_m->hapusauditie($id);
		$ceklima=$this->Planingaudit_m->hapustujuan($id);
		$cekenam=$this->Planingaudit_m->hapusjadwal($id);
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->Planingaudit_m->hapusData($data[$row]);
		$cekdua=$this->Planingaudit_m->hapusanggota($data[$row]);
		$cektiga=$this->Planingaudit_m->hapusstandar($data[$row]);
		$cekempat=$this->Planingaudit_m->hapusauditie($data[$row]);
		$ceklima=$this->Planingaudit_m->hapustujuan($data[$row]);
		$cekenam=$this->Planingaudit_m->hapusjadwal($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function getanggota(){
	   $id = $this->input->get("id");

	   echo $this->Planingaudit_m->getanggota($id); 
   }
   function getstandar(){
	   $id = $this->input->get("id");

	   echo $this->Planingaudit_m->getstandar($id); 
   }
   function getauditie(){
	   $id = $this->input->get("id");
	   echo $this->Planingaudit_m->getauditie($id); 
   }
   function getjadwal(){
	$id = $this->input->get("id");
	echo $this->Planingaudit_m->getjadwal($id); 
	}
	function gettujuan(){
	$id = $this->input->get("id");
	echo $this->Planingaudit_m->gettujuan($id); 
	}

    function export() {
        $date = date('dmY');
        header("Content-type=application/vnd.ms-excel");
        header("content-disposition:attachment;filename=laporan_planingaudit_".$date.".xls");
        $data['record'] = $this->Planingaudit_m->export_excel();
        $this->load->view('audit/laporan_planaudit_v',$data);
    }

}

?>