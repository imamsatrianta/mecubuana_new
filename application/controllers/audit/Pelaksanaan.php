<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pelaksanaan extends CI_Controller {

    function Pelaksanaan()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('audit/Pelaksanaan_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
    	$this->load->view('audit/pelaksanaan_v');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$set = $this->input->post("set");
	$id_unit= $this->input->post("id_unit");
	$id_ketuatim = $this->input->post("id_ketuatim");
	$id_standar = $this->input->post('id_standar');
	$id_auditie = $this->input->post('id_auditie');
	$catatan = $this->input->post('catatan');
	$rencana = $this->input->post('rencana');
	$jadwal = $this->input->post('jadwal');
	$tglclosing=$this->input->post('tglclosing');
	$periode = $this->input->post('periode');
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');

	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	

	$data=array(
		'id_unit'=>$id_unit,
		'id_auditie'=>$id_auditie,
		'id_standar'=>$id_standar,
		'id_ketuatim'=>$id_ketuatim,
		'catatan'=>$catatan,
		'rencana'=>$rencana,
		'jadwal'=>$jadwal,
		'periode'=>$periode,
		'tglclosing'=>$tglclosing);
		
	if($set==0){
	$datasimpan=array_merge($datainput,$data);
	//print_r($datasimpan); exit;
	$ceksatu=$this->Pelaksanaan_m->simpanData($datasimpan);
		$iddet=mysql_insert_id();
		
	}else{
		$datasimpan=array_merge($dataubah,$data);
		$ceksatu=$this->Pelaksanaan_m->editData($id,$datasimpan);
		$iddet=$id;
		$this->Pelaksanaan_m->hapusDetail($iddet);
	}
	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
        $iddoc= $this->input->post("iddoc");
        if ($iddoc!='null' || !empty($iddoc)) {
            $id_doc = explode(",",$iddoc);
            $arrlength = count($id_doc);

            for($x = 0; $x < $arrlength; $x++) {
                $id_formdoc = '';
                $checklist = '';
                $keterangan = '';
                $masalah = '';
                $tindakan = '';
                
                $id_formdoc = $this->input->post("id_formdoc_".$id_doc[$x]);
                $checklist = $this->input->post("checklist_".$id_doc[$x]);
                $keterangan = $this->input->post("keterangan_".$id_doc[$x]);
                $masalah = $this->input->post("masalah_".$id_doc[$x]);
                $tindakan = $this->input->post("tindakan_".$id_doc[$x]);
                $datadetdua = array(
                        'id_formulir_audit' => $iddet,
                        'id_persiapan_formulir_doc' => $id_formdoc,
                        'checklist'=>$checklist,
                        'keterangan'=>$keterangan,
                        'masalah'=>$masalah,
                        'tindakan'=>$tindakan,
                        'user_input' => $username,
                        'tgl_input'=>$tgl
                );
                $ceksatu=$this->Pelaksanaan_m->simpanDatadetail($datadetdua);

                if($ceksatu==1){
                        $setsim="ok";	
                }else{
                        $totalcek++;
                }
            }
		}
		
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabelPelaksanaan(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(b.nm_unit) like upper('%$search%') or upper(c.nm_karyawan) like upper('%$search%')
			or upper(a.periode) like upper('%$search%') or upper(a.tglmulai) like upper('%$search%')
                         or upper(a.tglselesai) like upper('%$search%') ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->Pelaksanaan_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
    function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(b.nm_unit) like upper('%$search%')   or upper(c.nm_dokumen) like upper('%$search%')
			or upper(a.catatan) like upper('%$search%') or upper(a.rencana) like upper('%$search%')
                                 or upper(a.tglclosing) like upper('%$search%')  ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->Pelaksanaan_m->loaddataTabelFormulir($offset,$limit,$order,$where); 
     
	} 

	function cetakLapformaudit(){
		$periode=$this->input->get("periode");//id_planingaudit
		$unit=$this->input->get("id_unit");
		$id_ketuatim=$this->input->get("id_ketuatim");
		$data = explode("-",$unit);
		$id_unit=$data[0];
		
		$data['nm_unit']=$this->Pelaksanaan_m->getUnit($periode);
		$data['nm_ketua_unit']=$this->Pelaksanaan_m->getKetuaUnit($periode);
		$data['nm_ketua_auditor']=$this->Pelaksanaan_m->getKetuatimauditor($periode);
		$data['nm_anggota']=$this->Pelaksanaan_m->getPlaningpersiapananggota($periode);
		$data['periode']=$this->Pelaksanaan_m->getPlaningpersiapan($periode);
		$data['tglmulai'] = $this->Pelaksanaan_m->getTglmulai($periode);
		$data['tglselesai'] = $this->Pelaksanaan_m->getTglselesai($periode);
		$data['tujuan'] = $this->Pelaksanaan_m->getTujuanaudit($periode);
		$data['standardok'] = $this->Pelaksanaan_m->getStandardok($periode);
		$data['auditiedata'] = $this->Pelaksanaan_m->getAuditiedata($periode);
		$data['materiaudit'] = $this->Pelaksanaan_m->getMateriaudit($periode);
		
		foreach($data['materiaudit'] as $std){
			$id_materi=$std->id_materi;

			$data['document'][$id_materi] = $this->Pelaksanaan_m->getDocumentaudit($periode,$id_materi);	
		}
		$this->load->library('pdfgenerator');
			$html = $this->load->view('cetakpdf/auditdokumen', $data,TRUE);
			$filename = 'report_'.time();
			$this->pdfgenerator->generate($html, $filename, true, 'A4', 'portrait');
	}

	function loaddatajadwal(){
		$offset = $this->input->get("offset");
		$limit = $this->input->get("limit");
		$order = $this->input->get("order");
		  if($this->input->get("search")){
			  $search = $this->input->get("search");
			  $where="(upper(b.tglmulai) like upper('%$search%')) ";
			  }else{
			  $where="a.id is not null";
			  }
		  
		$this->Pelaksanaan_m->loaddatajadwal($offset,$limit,$order,$where); 
	  
	 }
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->Pelaksanaan_m->hapusData($id);
                 $cekdua=$this->Pelaksanaan_m->hapusDetail($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->Pelaksanaan_m->hapusData($data[$row]);
                $cekdua=$this->Pelaksanaan_m->hapusDetail($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   function printDetail($id){
       $data['query']=$this->Pelaksanaan_m->export_pdf($id)->result_array();
       $this->load->view('prinpdf/html2pdf.class.php');
        $this->load->view('audit/laporan_pelaksanaan_v',$data);
   }

}?>