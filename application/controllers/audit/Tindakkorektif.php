<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tindakkorektif extends CI_Controller {

    function Tindakkorektif()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('audit/Tindakkorektif_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
    	$this->load->view('audit/Tindakkorektif_v');
    	$this->load->view('bawah');
	}

	function loaddatajadwal(){
		$offset = $this->input->get("offset");
		$limit = $this->input->get("limit");
		$order = $this->input->get("order");
		  if($this->input->get("search")){
			  $search = $this->input->get("search");
			  $where="(upper(b.tglmulai) like upper('%$search%')) ";
			  }else{
			  $where="a.id is not null";
			  }
		  
		$this->Tindakkorektif_m->loaddatajadwal($offset,$limit,$order,$where); 
	  
	}

	function loaddataktsob(){
		$offset = $this->input->get("offset");
		$limit = $this->input->get("limit");
		$order = $this->input->get("order");
		  if($this->input->get("search")){
			  $search = $this->input->get("search");
			  $where="(upper(b.tglmulai) like upper('%$search%')) ";
			  }else{
			  $where="a.id is not null";
			  }
		  
		$this->tindakkorektif_m->loaddataktsob($offset,$limit,$order,$where); 
	  
	}

	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$set = $this->input->post("set");

	$jadwal= $this->input->post("jadwal");
	$id_unit= $this->input->post("id_unit");
	$id_auditie = $this->input->post('id_auditie');
	$id_ketuatim = $this->input->post('id_ketuatim');
	$id_standar = $this->input->post('id_standar');
	$cari_ktsob = $this->input->post('cari_ktsob');
	$ptk_no = $this->input->post('ptk_no');
	$pj_ptk = $this->input->post('pj');
	$pernyataan = $this->input->post('pernyataan');
	$kategori = $this->input->post('kategori');
	$uraian_temuan = $this->input->post('uraian_temuan');
	$rencana_temuan = $this->input->post('rencana_temuan');
	$realisasi_ptk = $this->input->post('realisasi');
	$periode = $this->input->post('periode');
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');

	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	$data=array(
		'jadwal'=>$jadwal,
		'periode'=>$periode,
		'id_unit'=>$id_unit,
		'id_auditie'=>$id_auditie,
		'id_ketuatim'=>$id_ketuatim,
		'id_standar'=>$id_standar,
		'ptk_no'=>$ptk_no,
		'pj_ptk'=>$pj_ptk,
		'ktsob'=>$cari_ktsob,
		'pernyataan'=>$pernyataan,
		'kategori_ptk'=>$kategori,
		'uraian_temuan'=>$uraian_temuan,
		'rencana_temuan'=>$rencana_temuan,
		'realisasi_ptk'=>$realisasi_ptk);
		
	if($set==0){
	$datasimpan=array_merge($datainput,$data);
	//print_r($datasimpan); exit;
	$ceksatu=$this->Tindakkorektif_m->simpanData($datasimpan);
		
	}else{
		
        $datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->Tindakkorektif_m->editData($id,$datasimpan);
	}

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.no_ptk) like upper('%$search%')   or upper(a.kategori) like upper('%$search%') 
                          or upper(a.penyelesaian) like upper('%$search%') or upper(a.realisasi) like upper('%$search%')
			 or upper(a.pj_ptk) like upper('%$search%') or upper(b.nm_unit) like upper('%$search%') ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->Tindakkorektif_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->Tindakkorektif_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->Tindakkorektif_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }

}?>