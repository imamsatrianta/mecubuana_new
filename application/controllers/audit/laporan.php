<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

    function Laporan()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('audit/Laporan_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
    	$this->load->view('audit/Laporan_v');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$set = $this->input->post("set");
	
    $id_unit= $this->input->post("id_unit");
	$tgl_audit = $this->input->post('tglaudit');
	$jam = $this->input->post('jam');
	$kegiatan = $this->input->post('kegiatan');
    $tgl_mulai = $this->input->post('jadwal');
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	$id_ketuatim=$this->input->post('id_ketuatim');
	$periode=$this->input->post('periode');
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	$data=array(
        'id_unit'=>$id_unit,
		'tgl_audit'=>$tgl_audit,
		'tgl_mulai'=>$tgl_mulai,
		'id_ketuatim'=>$id_ketuatim,
		'jam'=>$jam,
		'periode'=>$periode,
		'kegiatan'=>$kegiatan);
		
	if($set==0){
	$datasimpan=array_merge($datainput,$data);
	//print_r($datasimpan); exit;
	$ceksatu=$this->Laporan_m->simpanData($datasimpan);
    $iddet=mysql_insert_id();
		
	}else{
		
        $datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->Laporan_m->editData($id,$datasimpan);
		$iddet=$id;
		$this->Laporan_m->hapusktsob($iddet);
		$this->Laporan_m->hapusbidang($iddet);
		$this->Laporan_m->hapuskehadiran($iddet);
		$this->Laporan_m->hapuskesimpulan($iddet);
	}

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	if (isset($_POST['ktsob'])) {
			$ktsob = $this->input->post('ktsob');
			 foreach ($ktsob as  $ktsobdata ) {
				 $datadetdua = array(
						'id_laporan_audit' => $iddet,
						'id_standar' => trim($ktsobdata['id_standar']),
						'kts_ob' => trim($ktsobdata['kts_ob']),
						'pernyataan' => trim($ktsobdata['pernyataan']),
						'user_input' => $username,
						'tgl_input'=>$tgl
					);
					$ceksatu=$this->Laporan_m->simpanDataktsob($datadetdua);
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
			 }
                } else {}
        if (isset($_POST['bidangdet'])) {
			$bidangdet = $this->input->post('bidangdet');
			 foreach ($bidangdet as  $bidangdetdata ) {
				 $datadettiga = array(
						'id_laporan_audit' => $iddet,
						'bidang' => trim($bidangdetdata['bidang']),
						'kelebihan' => trim($bidangdetdata['kelebihan']),
						'peluang' => trim($bidangdetdata['peluang']),
						'user_input' => $username,
						'tgl_input'=>$tgl
					);
					$ceksatu=$this->Laporan_m->simpanDatabidang($datadettiga);
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
			 }
				} else {}

		if (isset($_POST['kehadiran'])) {
			$kehadiran = $this->input->post('kehadiran');
				foreach ($kehadiran as  $kehadiran ) {
					$datadetempat = array(
						'id_laporan_audit' => $iddet,
						'id_unit' => trim($kehadiran['id_unitdet']),
						'nm_karyawan' => $kehadiran['nm_hadir'],
						'user_input' => $username,
						'tgl_input'=>$tgl
					);
					$ceksatu=$this->Laporan_m->simpanDatakehadiran($datadetempat);
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
				}
				} else {}

			if (isset($_POST['kesimpulans'])) {
				$kesimpulan = $this->input->post('kesimpulans');
					foreach ($kesimpulan as  $kesimpulan ) {
						$datadetlima = array(
							'id_laporan_audit' => $iddet,
							'kesimpulan' => trim($kesimpulan['kesimpulan']),
							'opsi' => $kesimpulan['opsi'],
							'lainnya' => $kesimpulan['lainnya'],
							'user_input' => $username,
							'tgl_input'=>$tgl
						);
						$ceksatu=$this->Laporan_m->simpanDatakesimpulan($datadetlima);
						
						if($ceksatu==1){
							$setsim="ok";	
						}else{
							$totalcek++;
						}
					}
					} else {}
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	function loaddatajadwal(){
		$offset = $this->input->get("offset");
		$limit = $this->input->get("limit");
		$order = $this->input->get("order");
		  if($this->input->get("search")){
			  $search = $this->input->get("search");
			  $where="(upper(b.tglmulai) like upper('%$search%')) ";
			  }else{
			  $where="a.id is not null";
			  }
		  
		$this->Laporan_m->loaddatajadwal($offset,$limit,$order,$where); 
	  
	 }
	 function loaddataktsob(){
		$offset = $this->input->get("offset");
		$limit = $this->input->get("limit");
		$order = $this->input->get("order");
		  if($this->input->get("search")){
			  $search = $this->input->get("search");
			  $where="(upper(b.tglmulai) like upper('%$search%')) ";
			  }else{
			  $where="a.id is not null";
			  }
		  
		$this->Laporan_m->loaddatajadwal($offset,$limit,$order,$where); 
	  
	 }
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.tgl_audit) like upper('%$search%')   or upper(a.jam) like upper('%$search%') 
                          or upper(a.kegiatan) like upper('%$search%') or upper(b.nm_unit) like upper('%$search%')  ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->Laporan_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->Laporan_m->hapusData($id);
		$cekdua=$this->Laporan_m->hapusktsob($id);
		$cektiga=$this->Laporan_m->hapusbidang($id);
		$cekempat=$this->Laporan_m->hapuskehadiran($id);
		$ceklima=$this->Laporan_m->hapuskesimpulan($id);
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->Laporan_m->hapusData($data[$row]);
		$cekdua=$this->Laporan_m->hapusktsob($data[$row]);
		$cektiga=$this->Laporan_m->hapusbidang($data[$row]);
		$cekempat=$this->Laporan_m->hapuskehadiran($data[$row]);
		$ceklima=$this->Laporan_m->hapuskesimpulan($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function getktsob(){
	   $id = $this->input->get("id");

	   echo $this->Laporan_m->getktsob($id); 
   }
	function getdaftarhadir(){
		$id = $this->input->get("id");

		echo $this->Laporan_m->getdaftarhadir($id); 
	}
   function getbidang(){
	   $id = $this->input->get("id");

	   echo $this->Laporan_m->getbidang($id); 
   }
   function getkesimpulan(){
	$id = $this->input->get("id");

	echo $this->Laporan_m->getkesimpulan($id); 
}

}?>