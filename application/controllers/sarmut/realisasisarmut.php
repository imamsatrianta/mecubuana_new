<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class realisasisarmut extends CI_Controller {

    function realisasisarmut(){
		parent::__construct();
        $this->load->database();
		$this->load->model(array('sarmut/realisasisarmut_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
	   //echo "xx";exit;
    	$this->load->view('sarmut/realisasisarmut_v');
		$this->load->view('sarmut/realisasisarmut_js');
    	$this->load->view('bawah');
	}
	
	
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$setpersen=0;
	$id = $this->input->post("id");
	$id_sarmut_kegiatan = $this->input->post('id_sarmut_kegiatan');
	$tgl_realisasi = $this->input->post('tgl_realisasi');
	//$uraian_realisasi = trim($this->input->post('uraian_realisasi'));
	$presentase_realisasi = str_replace(',', '', trim($this->input->post('presentase_realisasi')));
	
	$cekpresentase=$this->realisasisarmut_m->cekPresenkegiatan($id_sarmut_kegiatan);
	$jmlpersen=$presentase_realisasi+$cekpresentase;
	if($jmlpersen>100){
		$setpersen=1;
	}
	
	$id_level=$this->session->userdata('id_level');
	$id_unitses=$this->session->userdata('id_unit');
	$id_direktorat=$this->session->userdata('id_direktorat');
	$id_prodi=$this->session->userdata('id_prodi');
	if($id_level=="3"){
		$jenis=2;
		$id_unit=$id_direktorat;
	}else if($id_level=="4"){
		$jenis=1;
		$id_unit=$id_unitses;
	}else if($id_level=="5"){
		$jenis=3;
		$id_unit=$id_prodi;
	}
	
	
	
	$kendala = $this->input->post('kendala');
	$tindakan = $this->input->post('tindakan');
	$setgbr=$this->input->post('setgbr');
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	
	$set = $this->input->post("set");
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	if( $setgbr==1){
		$allowedExts = array("pdf", "jpeg", "jpg", "png");
			$temp = explode(".", $_FILES["file_uploadda"]["name"]);
			$extension = end($temp);
			if ((($_FILES["file_uploadda"]["type"] == "image/pdf")
			|| ($_FILES["file_uploadda"]["type"] == "image/jpeg")
			|| ($_FILES["file_uploadda"]["type"] == "image/jpg")
			|| ($_FILES["file_uploadda"]["type"] == "image/pjpeg")
			|| ($_FILES["file_uploadda"]["type"] == "image/x-png")
			|| ($_FILES["file_uploadda"]["type"] == "application/pdf"))
			&& ($_FILES["file_uploadda"]["size"] < 200000000)
			&& in_array($extension, $allowedExts)) {
			//echo $extension."xxx";
			 $acak           = rand(000000,999999);
			$filename =$_FILES["file_uploadda"]["name"];
			$dataimage = $acak.$filename;
				
			move_uploaded_file($_FILES["file_uploadda"]["tmp_name"],
					"doksarmut/" . $dataimage);
			
			//echo "ok";
			}else{
			$dataimage="";
			}
	}else{
		$dataimage="";
	}
	

	$data=array(
		'id_sarmut_kegiatan'=>$id_sarmut_kegiatan,
		'tgl_realisasi'=>$tgl_realisasi,
		'presentase_realisasi'=>$presentase_realisasi,
		//'anggaran'=>$anggaran,
		'kendala'=>$kendala,
		'tindakan'=>$tindakan); 
	$dataimg=array('file_upload'=>'doksarmut/'.$dataimage);	
	if($set==0){
		$aksesmenu=$this->realisasisarmut_m->getAksesmenu($jenis,$id_unit);
	$dataakses=array('kode_approvel'=>$aksesmenu);
	$alamat=substr($aksesmenu,0,2);
	$this->global_m->kirimEmail($alamat,1,"realisasisarmut");
	
	
	$datasimpan=array_merge($datainput,$data,$dataimg,$dataakses);
	//print_r($datasimpan); exit;
	$ceksatu=$this->realisasisarmut_m->simpanData($datasimpan);
		$iddet=mysql_insert_id();
	}else{
		if($dataimage==""){
			$datasimpan=array_merge($datainput,$data);
		}else{
			$datasimpan=array_merge($datainput,$data,$dataimg);
		}
		
		$ceksatu=$this->realisasisarmut_m->editData($id,$datasimpan);
		
		$iddet=$id;
	}
	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	$cekdua=$this->realisasisarmut_m->editDatakegiatan($id_sarmut_kegiatan,$jmlpersen);
	if($cekdua==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}

	$status = $this->db->trans_status();
	if($setpersen>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal, Presentase Lebih dari 100","status" => "error"));
	}else if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(b.tgl_selesai) like upper('%$search%')   or upper(b.tgl_mulai) like upper('%$search%')
				or upper(b.uraian_kegiatan) like upper('%$search%') or upper(a.uraian_realisasi) like upper('%$search%')
				or upper(a.kendala) like upper('%$search%') or upper(a.tindakan) like upper('%$search%')) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->realisasisarmut_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->realisasisarmut_m->hapusData($id);
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->realisasisarmut_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function loaddatakegiatan(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.uraian_kegiatan) like upper('%$search%')   or upper(a.id) like upper('%$search%') ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
       $this->realisasisarmut_m->loaddatakegiatan($offset,$limit,$order,$where); 
     
    }
   
   
   

}?>