<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class groupsarmut extends CI_Controller {

    function groupsarmut()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('sarmut/groupsarmut_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
	   //echo "xx";exit;
    	$this->load->view('sarmut/groupsarmut_v');
    	$this->load->view('bawah');
	}
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	$id_tahunakademik = $this->input->post('id_tahunakademik');
	$id_standar = $this->input->post('id_standar');
	$set = $this->input->post("set");
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	$data=array(
	'id_tahunakademik'=>$id_tahunakademik);
	
	if($set==0){
		$datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->groupsarmut_m->simpanData($datasimpan);
		$iddet=mysql_insert_id();
	}else{
		$datasimpan=array_merge($dataubah,$data);
		$ceksatu=$this->groupsarmut_m->editData($id,$datasimpan);
		$iddet=$id;
		$this->groupsarmut_m->hapusDatastandar($iddet);
	}	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	if (isset($_POST['id_standar'])) {
			 foreach ($id_standar as  $stdr ) {
				 $datadetdua = array(
						'id_group' => trim($iddet),
						'id_standar' => trim($stdr),
						'user_input' => $username,
						'tgl_input'=>$tgl
					);
				//	print_r($datadetdua);
					$this->groupsarmut_m->simpanDatastandar($datadetdua);
					
			 }
		}
		
	
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="upper(b.nm_tahun_akademik) like upper('%$search%')   or upper(a.id) like upper('%$search%')   ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->groupsarmut_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->groupsarmut_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->groupsarmut_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   

}?>