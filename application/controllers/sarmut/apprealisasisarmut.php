<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class apprealisasisarmut extends CI_Controller {

    function apprealisasisarmut(){
		parent::__construct();
        $this->load->database();
		$this->load->model(array('sarmut/apprealisasisarmut_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
	   //echo "xx";exit;
    	$this->load->view('sarmut/apprealisasisarmut_v');
		$this->load->view('sarmut/apprealisasisarmut_js');
    	$this->load->view('bawah');
	}
	
	
	
	
	
	
	function loaddataTabel(){
	    $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(b.tgl_selesai) like upper('%$search%')   or upper(b.tgl_mulai) like upper('%$search%')
				or upper(b.uraian_kegiatan) like upper('%$search%') or upper(a.uraian_realisasi) like upper('%$search%')
				or upper(a.kendala) like upper('%$search%') or upper(a.tindakan) like upper('%$search%')) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->apprealisasisarmut_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function approveData(){
		 $id = $this->input->get("id");
		  $ket = $this->input->get("ket");
		 $kode_approvel_rel = $this->input->get("kode_approvel_rel");
		 if($kode_approvel_rel=="null"){
			$kode_approvel_rel=""; 
		 }
		 $level_app = $this->input->get("level_app");
		// $kode_approve=$this->session->userdata('kode_approve');
		 $kode_approve=$this->input->get("kodeapp");
		 $userid=$this->session->userdata('userid').",";
		 $app=$kode_approvel_rel."".$kode_approve;
		 $level=$level_app+1;
		 $username=$this->session->userdata('username');
		$tgl=date('Y-m-d');
		 $user_app=$this->input->get("user_app");
		 $user=$user_app."".$userid;
		 $kodeappemail=$this->input->get("kodeappemail");
		 $this->global_m->kirimEmail($kodeappemail,2,"apprealisasisarmut");
		 
		// echo $userid;
		 $data=array(
		'kode_approvel_rel'=>$app,
		'level_app'=>$level,
		'user_app'=>$user,
		'user_update'=>$username,
		'tgl_update'=>$tgl);
		
		$dataapp=array(
		 'id_sarmut_realisasi'=>$id,
		'ket_app'=>$ket,
		'kode_app'=>$kode_approve,
		'user_app'=>$username,
		'tgl_app'=>$tgl);
		$ceksatu=$this->apprealisasisarmut_m->simpanapproveData($dataapp);
		
		// $ceksatu=$this->apprealisasisarmut_m->approveData($id,$data);
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Approve data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Approve Data Gagal","status" => "error"));
			}
	
   }
   
   function approveArray(){
   $this->db->trans_begin();
	$status=true; 
	$totalcek=0;
	// $kode_approve=$this->session->userdata('kode_approve');
	 $username=$this->session->userdata('username');
	 $userid=$this->session->userdata('userid').",";
		$tgl=date('Y-m-d');
	 $datapos = $this->input->get("data");
	 $ket= $this->input->get("ket");
	$dataarr = explode(",",$datapos,-1);
	$data      = $this->apprealisasisarmut_m->getBy_ArrayID($dataarr);
	foreach ($data as $key => $list) {
		$idhapus=$list['id'];
		$kode_approvel_rel=$list['kode_approvel_rel'];
		$kodeapp=$list['kodeapp'];
		if($kode_approvel_rel==null){
			$kode_approvel_rel=""; 
		 }
		 $app=$kode_approvel_rel."".$kodeapp;
		 
		$level_app=$list['level_app'];
		$level=$level_app+1;
		$user_app=$list['user_app'];
		$user=$user_app."".$userid;
		 $data=array(
			'kode_approvel_rel'=>$app,
			'level_app'=>$level,
			'user_app'=>$user,
			'user_update'=>$username,
			'tgl_update'=>$tgl);
			 $dataapp=array(
			 'id_sarmut'=>$idhapus,
			'ket_app'=>$ket,
			'kode_app'=>$kodeapp,
			'user_app'=>$username,
			'tgl_app'=>$tgl);
			$ceksatu=$this->apprealisasisarmut_m->simpanapproveData($dataapp);
		
			 $ceksatu=$this->apprealisasisarmut_m->approveData($idhapus,$data);
			 if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
	}
	

		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Approve Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Approve Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Approve data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   
   
   
   

}?>