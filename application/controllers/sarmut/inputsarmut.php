<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class inputsarmut extends CI_Controller {

    function inputsarmut()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('sarmut/inputsarmut_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
	   //echo "xx";exit;
    	$this->load->view('sarmut/inputsarmut_v');
		$this->load->view('sarmut/inputsarmut_js');
    	$this->load->view('bawah');
	}
	
	   function loaddatamkp(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
	    $id_mkp = $this->input->get("id_mkp"); 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.no) like upper('%$search%')   or upper(a.kegiatan) like upper('%$search%') ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->inputsarmut_m->loaddatamkp($offset,$limit,$order,$where,$id_mkp); 
     
    }
	
	
	function simpanData(){
	
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id");
	
	$id_mkp = $this->input->post('id_mkp');
	$uraian_sarmut = $this->input->post('uraian_sarmutformat');
	$target_kuantitatif = trim($this->input->post('target_kuantitatif'));
	$target_kualitatif = $this->input->post('target_kualitatif');
	$kinerja_lalu = $this->input->post('kinerja_lalu');
	$id_tahunakademik = $this->input->post('id_tahunakademik');
	//$tgl = $this->input->post('tgl');
	
	$id_kegiatan_mkp = $this->input->post('id_kegiatan_mkp');
	$jenis_sarmut = $this->input->post('jenis_sarmut');
	
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	
	$id_level=$this->session->userdata('id_level');
	$id_unitses=$this->session->userdata('id_unit');
	$id_direktorat=$this->session->userdata('id_direktorat');
	$id_prodi=$this->session->userdata('id_prodi');
	if($id_level=="3"){
		$jenis=2;
		$id_unit=$id_direktorat;
	}else if($id_level=="4"){
		$jenis=1;
		$id_unit=$id_unitses;
	}else if($id_level=="5"){
		$jenis=3;
		$id_unit=$id_prodi;
	}
	
	
	
	$set = $this->input->post("set");
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	if($target_kuantitatif==""){
		$target=100;
	}else{
		$target=$target_kuantitatif;
	}
	$data=array(
		'id_mkp'=>$id_mkp,
		'uraian_sarmut'=>$uraian_sarmut,
		'id_kegiatan_mkp'=>$id_kegiatan_mkp,
		'jenis_sarmut'=>$jenis_sarmut,
		'target_kuantitatif'=>$target,
		'target_kualitatif'=>$target_kualitatif,
		'kinerja_lalu'=>$kinerja_lalu,
		'id_tahunakademik'=>$id_tahunakademik,
		'tgl'=>$tgl);
		
	
	if($set==0){
	$aksesmenu=$this->inputsarmut_m->getAksesmenu($jenis,$id_unit);
	$alamat=substr($aksesmenu,0,2);
	$this->global_m->kirimEmail($alamat,1,"sarmut");

	$dataakses=array('id_unit'=>$id_unit,'jenis'=>$jenis,'kode_approvel'=>$aksesmenu);
	$datasimpan=array_merge($datainput,$data,$dataakses);
	//print_r($datasimpan); exit;
	$ceksatu=$this->inputsarmut_m->simpanData($datasimpan);
	$iddet=mysql_insert_id();
		
	}else{
		$datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->inputsarmut_m->editDatasarmut($id,$datasimpan);
		$iddet=$id;
		//$this->inputsarmut_m->hapusDetail($iddet);
	}
	
	
	//print_r($datasimpan);

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	
	
	
	
	
			
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
	
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="( upper(b.nm_tahun_akademik) like upper('%$search%')
				or upper(a.uraian_sarmut) like upper('%$search%') or upper(a.target_kuantitatif) like upper('%$search%')) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->inputsarmut_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	function hapusData(){
		 $id = $this->input->get("id");
		 $ceksatu=$this->inputsarmut_m->hapusData($id);
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
	
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$ceksatu=$this->inputsarmut_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   
   function getDetail(){
	   $id = $this->input->get("id");

	   echo $this->inputsarmut_m->getDetail($id); 
   }
   
   
   
   function simpanDatakeg(){
	   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$id = $this->input->post("id_keg");
	$set = $this->input->post('setkeg');
	$id_sarmut = $this->input->post('id_sarmut');
	
	$uraian_kegiatan = $this->input->post('uraian_kegiatan');
	$tgl_mulai = $this->input->post('tgl_mulai');
	$tgl_selesai = $this->input->post('tgl_selesai');
	$anggaran=$this->input->post('anggaran');
	
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
//	print_r($id_unit);
	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);


	$data=array(
		'id_sarmut'=>$id_sarmut,
		'anggaran'=>$anggaran,
		'uraian_kegiatan'=>$uraian_kegiatan,
		'tgl_mulai'=>$tgl_mulai,
		'tgl_selesai'=>$tgl_selesai);		
	
	
	if($set==0){
		$datasimpan=array_merge($datainput,$data);
		//print_r($datasimpan); exit;
		$ceksatu=$this->inputsarmut_m->simpanDakegiatan($datasimpan);
		
	}else{
		$datasimpan=array_merge($datainput,$data);
		$ceksatu=$this->inputsarmut_m->editDatakegiatan($id,$datasimpan);
		
	}
	
	
	

	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	

				
	
	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
   
   }
   function loaddatadetailkegiatan(){
	   $id = $this->input->get("id");
	   $this->inputsarmut_m->loaddatadetailkegiatan($id); 
	   
   }
    function loaddatadetailkegiatanrel(){
	   $id = $this->input->get("id");
	   $this->inputsarmut_m->loaddatadetailkegiatanrel($id); 
	   
   }
   
   function hapusdatakegiatan(){
	   $id = $this->input->get("id");
		 $ceksatu=$this->inputsarmut_m->hapusdatakegiatan($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
   }
   
   function loaddataTabelkegiatan(){
	      $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.uraian_kegiatan) like upper('%$search%')   or upper(b.uraian_sarmut) like upper('%$search%')
				or upper(b.target_kuantitatif) like upper('%$search%') or upper(a.target_kualitatif) like upper('%$search%')) ";
			 }else{
			 $where="a.id is not null";
			 }
        $this->inputsarmut_m->loaddataTabelkegiatan($offset,$limit,$order,$where); 
   }
   
   function cekScore(){
	   $this->inputsarmut_m->cekScore();
   }
   
   
   function cetakWord(){
	   $this->load->view('cetakword/test');
		//echo 'dfdddd dfsfdf';
		header("Content-type: application/vnd.ms-word");
		header("Content-Disposition: attachment;Filename=cetakMkp.doc");
	}
	
	function cetakRencanasarmut(){
		$id_tahunakademi=$this->input->get("id_tahunakademi");
		$unit=$this->input->get("id_unit");
		 $data = explode("-",$unit);
		$id_unit=$data[0];
		$jenis=$data[1];
			
			$data['unit']=$this->inputsarmut_m->getUnit($id_unit,$jenis);
			$data['tahunakademik']=$this->inputsarmut_m->getTahunakademik($id_tahunakademi);
			
			$data['datastandar']=$this->inputsarmut_m->getSarmutstandar($id_tahunakademi,$id_unit,$jenis);
			
			foreach($data['datastandar'] as $std){
				$id_mkp=$std->id_mkp;
				$data['datasarmut'][$id_mkp]=$this->inputsarmut_m->getSarmut($id_tahunakademi,$id_unit,$jenis,$id_mkp);
					foreach($data['datasarmut'][$id_mkp] as $sarmut){
						$id=$sarmut->id;
						$data['datasarmutdet'][$id_mkp][$id]=$this->inputsarmut_m->getSarmutdua($id);
					}
				
			}
			
			
		// print_r($data['datastandar']);exit;
			 
			 
			 
			//print_r($data['datasarmut']) ;	
			//exit;
			$this->load->library('pdfgenerator');
			$html = $this->load->view('cetakpdf/rencanasarmut', $data,TRUE);
			$filename = 'report_'.time();
			$this->pdfgenerator->generate($html, $filename, true, 'A4', 'landscape');
	}
	
	function loaddatastandar(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(a.id) like upper('%$search%')   or upper(a.nm_tahun_akademik) like upper('%$search%') ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->inputsarmut_m->loaddatastandar($offset,$limit,$order,$where); 
     
    }
	

}?>