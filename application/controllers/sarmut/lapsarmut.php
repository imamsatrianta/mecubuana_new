<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class lapsarmut extends CI_Controller {

    function lapsarmut(){
		parent::__construct();
        $this->load->database();
		$this->load->model(array('sarmut/lapsarmut_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		
	   $this->load->view('atas_v');
	   //echo "xx";exit;
    	$this->load->view('sarmut/lapsarmut_v');
    	$this->load->view('bawah');
	}
	
	
	
	
	
	
	function loaddataTabel(){
	    $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="( upper(b.nm_tahun_akademik) like upper('%$search%')
				or upper(a.uraian_sarmut) like upper('%$search%') or upper(a.target_kuantitatif) like upper('%$search%')) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->lapsarmut_m->loaddataTabel($offset,$limit,$order,$where); 
     
    } 
	
	function cetakSarmut(){
		$id_tahunakademi=$this->input->get("id_tahunakademi");
		$unit=$this->input->get("id_unit");
		 $data = explode("-",$unit);
		$id_unit=$data[0];
		$jenis=$data[1];
			
			$data['unit']=$this->lapsarmut_m->getUnit($id_unit,$jenis);
			$data['tahunakademik']=$this->lapsarmut_m->getTahunakademik($id_tahunakademi);
			
			$data['datastandar']=$this->lapsarmut_m->getSarmutstandar($id_tahunakademi,$id_unit,$jenis);
			//print_r($data['datastandar']);exit;
			foreach($data['datastandar'] as $std){
				$id_mkp=$std->id_mkp;
				$data['datasarmut'][$id_mkp]=$this->lapsarmut_m->getSarmut($id_tahunakademi,$id_unit,$jenis,$id_mkp);
					foreach($data['datasarmut'][$id_mkp] as $sarmut){
						$id=$sarmut->id;
						$data['datasarmutdet'][$id_mkp][$id]=$this->lapsarmut_m->getSarmutdua($id);
					}
				
			}
			$pdfFilePath = "output_pdf_name.pdf";
			
			$this->load->library('m_pdf');
			//$this->m_pdf->pdf->AddPage('L');
			$this->m_pdf->pdf->mPDF('utf-8','A4-L','','','5','5','50','10'); 
			
			// When 15=margin-left, 15=margin-right, 28=margin-top, 18=margin-bottom
			
			$this->m_pdf->pdf->SetHTMLHeader('
			<table style="width:100.2%;"   cellpadding="0" cellspacing="0" border="1">
  <tr>
    <td colspan="3" style="width:13%" align="center"  >
	<img src="assets/images/logo_mercu1.png" alt="Logo" style="width:80px;height:60px;">
	</td>
    <td colspan="16" style="width:75.5%" align="center">
		<strong>
			SASARAN MUTU  <br />

			TAHUN AKADEMIK <?php echo $tahunakademik;?><br />

			UNIT : <?php echo $unit;?><br />

			<br />

			</strong>
	</td>
	<td style="width:11.5%" align="center">
		<img src="assets/images/logo_q.png" alt="Logo" style="width:50px;height:40px;">
	</td>
  </tr>
  <tr>
		<td colspan="20"  style="width:auto;" >&nbsp;</td>
   </tr>
 </table> 
 
  <table style="width:auto;"   cellpadding="0" cellspacing="0" border="1">
   <tr>
	<td rowspan="2" style="width:20px" align="center" >No</td>
    <td rowspan="2" style="width:100px" align="center" >Standard UBM</td>
	<td rowspan="2" style="width:200px" align="center" >Pernyataan Sasaran Mutu</td>
	<td colspan="2" style="width:120px" align="center"  >Target</td>
	<td rowspan="2" style="width:89px" align="center"  >Kinerja Lalu</td>
	<td rowspan="2" style="width:300px;" align="center" >Rencana Kegiatan</td>
	<td rowspan="2" style="width:90px;" align="center" >Waktu</td>
	<td rowspan="2" style="width:90px" align="center"  >Anggaran</td>
	
	
	
  </tr>
  
	<tr>
			
			<td style="width:60px" align="center" >Kuantitatif</td>
			<td style="width:60px" align="center" >Cara Mengukur</td>
		
	</tr>
   
 </table>  
 
');



			$html = $this->load->view('cetakpdf/sarmut_mpdf', $data,true);
			
			$this->m_pdf->pdf->WriteHTML($html);
			
		
        //download it.
		$this->m_pdf->pdf->Output($pdfFilePath, "I");
	}
	
	function cetakSarmut_xxx(){
		$id_tahunakademi=$this->input->get("id_tahunakademi");
		$unit=$this->input->get("id_unit");
		 $data = explode("-",$unit);
		$id_unit=$data[0];
		$jenis=$data[1];
			
			$data['unit']=$this->lapsarmut_m->getUnit($id_unit,$jenis);
			$data['tahunakademik']=$this->lapsarmut_m->getTahunakademik($id_tahunakademi);
			
			$data['datastandar']=$this->lapsarmut_m->getSarmutstandar($id_tahunakademi,$id_unit,$jenis);
			//print_r($data['datastandar']);exit;
			foreach($data['datastandar'] as $std){
				$id_mkp=$std->id_mkp;
				$data['datasarmut'][$id_mkp]=$this->lapsarmut_m->getSarmut($id_tahunakademi,$id_unit,$jenis,$id_mkp);
					foreach($data['datasarmut'][$id_mkp] as $sarmut){
						$id=$sarmut->id;
						$data['datasarmutdet'][$id_mkp][$id]=$this->lapsarmut_m->getSarmutdua($id);
					}
				
			}
			
			/*$this->load->library('pdf');
			$html = $this->load->view('cetakpdf/sarmut', $data,TRUE);
			$this->pdf->pdf_create($html,"BarangKeluar",true);
		//	$this->pdf->generate($html, $filename, true, 'A4', 'landscape');
			*/
			
			$this->load->library('pdfgenerator');
			$html = $this->load->view('cetakpdf/sarmut', $data,TRUE);
			$filename = 'report_'.time();
			$this->pdfgenerator->generate($html, $filename, true, 'A4', 'landscape');
			
			
	}
	
	function cetakLapsarmut(){
		$id_tahunakademi=$this->input->get("id_tahunakademi");
		$unit=$this->input->get("id_unit");
		 $data = explode("-",$unit);
		$id_unit=$data[0];
		$jenis=$data[1];
			
			$data['unit']=$this->lapsarmut_m->getUnit($id_unit,$jenis);
			$data['tahunakademik']=$this->lapsarmut_m->getTahunakademik($id_tahunakademi);
			
			$data['datastandar']=$this->lapsarmut_m->getSarmutstandar($id_tahunakademi,$id_unit,$jenis);
			foreach($data['datastandar'] as $std){
				$id_mkp=$std->id_mkp;
				$data['datasarmut'][$id_mkp]=$this->lapsarmut_m->getSarmut($id_tahunakademi,$id_unit,$jenis,$id_mkp);
					foreach($data['datasarmut'][$id_mkp] as $sarmut){
						$id=$sarmut->id;
						$data['datasarmutdet'][$id_mkp][$id]=$this->lapsarmut_m->getSarmutdua($id);
						foreach($data['datasarmutdet'][$id_mkp][$id] as $sarmutdet){
							$iddet=$sarmutdet->id;
							$data['datasarmutrel'][$id_mkp][$id][$iddet]=$this->lapsarmut_m->getSarmutrel($iddet);
						}
					}
				
			}
			
			$this->load->library('pdfgenerator');
			$html = $this->load->view('cetakpdf/laporansarmut', $data,TRUE);
			$filename = 'report_'.time();
			$this->pdfgenerator->generate($html, $filename, true, 'A4', 'landscape');
	}
	
   
   
   
   
   

}?>