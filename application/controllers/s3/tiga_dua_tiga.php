<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tiga_dua_tiga extends CI_Controller {

    function tiga_dua_tiga()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('s3/tiga_dua_tiga_m'));
        $this->load->helper(array('form', 'url'));
		
	}	
    
	public function index(){
		$level=$_GET['level'];
		$data['idm']=$_GET['idm'];
		$data['level']=$_GET['level'];
		$data['menu']=$this->tiga_dua_tiga_m->loadDatamenu($level);
	   $this->load->view('atas_v');
	   //echo "xx";exit;
		$this->load->view('borang/standarsatumenu_v',$data);
	
			$parameter=$_GET['parameter'];
			$data['uraian']=$this->global_m->loadDatamenusatu($parameter);
			//echo $data['uraian'];
			//exit;
			$this->load->view('s3/tiga_dua_tiga_v',$data);
			$this->load->view('s3/tiga_dua_tiga_js');
    	$this->load->view('bawah');
	}
	
	function loaddataTabel(){
	   $offset = $this->input->get("offset");
	   $limit = $this->input->get("limit");
	   $order = $this->input->get("order");
	   $parameter= $this->input->get("parameter");
		 
		 if($this->input->get("search")){
		 	$search = $this->input->get("search");
			 $where="(upper(b.nm_jenjangprodi) like upper('%$search%')   or upper(c.nm_tahun_akademik) like upper('%$search%')
			or upper(d.nm_prodi) like upper('%$search%') or upper(a.uraian) like upper('%$search%')  ) ";
			 }else{
			 $where="a.id is not null";
			 }
		 
        $this->tiga_dua_tiga_m->loaddataTabel($offset,$limit,$order,$where,$parameter); 
     
    } 
	
	function simpanData(){
	$iddet="";
	$this->db->trans_begin();
	$status=true;
	$totalcek=0;
	$setsmpan=0;
	$id = $this->input->post("id");
	$set = $this->input->post("set");
	$id_tahunakademik= $this->input->post("id_tahunakademik");
	$id_parameter = $this->input->post('id_parameter');
	$id_jenjangprodi = $this->input->post('id_jenjangprodi');
	$id_prodiarr = $this->input->post('id_prodi');
	$prodi=explode('-',$id_prodiarr);
	$id_prodi=$prodi[0]; 
	$kd_server=$prodi[1]; 
	$tanggal=$this->input->post('tanggal');
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');

	
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	$data=array(
		'id_tahunakademik'=>$id_tahunakademik,
		'id_parameter'=>$id_parameter,
		'id_jenjangprodi'=>$id_jenjangprodi,
		'id_prodi'=>$id_prodi,
		'tanggal'=>$tanggal);
	$ceksatu="";	
	if($set==0){
	$datasimpan=array_merge($datainput,$data);
	$cekborang=$this->tiga_dua_tiga_m->cekBorang($id_tahunakademik,$id_parameter,$id_jenjangprodi,$id_prodi);
	if (!empty($cekborang)) {
		$setsmpan++;
	}else{
	//print_r($datasimpan); exit;
	$idinsert=$this->tiga_dua_tiga_m->simpanData($datasimpan);
	$iddet=$idinsert;
	
	}
		
	}else{
		
		$datasimpan=array_merge($dataubah,$data);
		$ceksatu=$this->tiga_dua_tiga_m->editData($id,$datasimpan);
		$iddet=$id;
		$this->tiga_dua_tiga_m->hapusDetail($id);
		
	}
	
	
	
/*
	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}*/
	
	if (isset($_POST['detail'])) {
			$detail = $this->input->post('detail');
			 foreach ($detail as  $detaildata ) {
				 $datadetdua = array(
						'id_borang_s3' => $iddet,
						'tahun_akademik' => trim($detaildata['tahun_akademik']),
						'ts_lima'=>trim($detaildata['ts_lima']),
						'ts_empat'=>trim($detaildata['ts_empat']),
						'ts_tiga'=>trim($detaildata['ts_tiga']),
						'ts_dua'=>trim($detaildata['ts_dua']),
						'ts_satu'=>trim($detaildata['ts_satu']),
						'ts'=>trim($detaildata['ts']),
						'ts_jumlah'=>trim($detaildata['ts_jumlah'])
					);
					$datasimpan=array_merge($datadetdua,$datainput);
					$ceksatu=$this->tiga_dua_tiga_m->simpanDatadetail($datasimpan);
					
					if($ceksatu==1){
						$setsim="ok";	
					}else{
						$totalcek++;
					}
			 }
		}
		
		
	
	
	$status = $this->db->trans_status();
	if($setsmpan>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal, Data Sudah Ada","status" => "error"));
	}else if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
	}
   
   function hapusData(){
	    $id = $this->input->get("id");
		$this->tiga_dua_tiga_m->hapusKebijakanheder($id);
		$this->tiga_dua_tiga_m->hapusDetail($id);
		 $ceksatu=$this->tiga_dua_tiga_m->hapusData($id);
		 
			if($ceksatu==1){
				echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
	
			}else{
				echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
			}
   }
   
   function hapusDataarray(){
   $this->db->trans_begin();
	$status=true;
	$totalcek=0;
	 $data = $this->input->get("data");
	$data = explode(",",$data);
		foreach ($data as $row => $item) {
	//	echo   $data[$row];
		$this->tiga_dua_tiga_m->hapusKebijakanheder($data[$row]);
		$this->tiga_dua_tiga_m->hapusDetail($data[$row]);
		$ceksatu=$this->tiga_dua_tiga_m->hapusData($data[$row]);
			if($ceksatu==1){
				$setsim="ok";	
			}else{
				$totalcek++;
			}
			
		}
		
		$status = $this->db->trans_status();
		if($totalcek>0){
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else if ($status === FALSE) {
			$this->db->trans_rollback();
			echo json_encode(array("pesan" => "Informasi <br> Hapus Data Gagal","status" => "error"));
		}else {
			$this->db->trans_commit();
			echo json_encode(array("pesan" => "Informasi <br> Hapus data Berhasil","status" => "success"));
		
			}
		return $status;	
	
   }
   function getEditdetail(){
	   $id = $this->input->get("id");
	   echo $this->tiga_dua_tiga_m->getEditdetail($id); 
   }
  
   function getDataserver(){
	   $id_prodi = $this->input->get("id_prodi");
	   $id_tahunakademik = $this->input->get("id_tahunakademik");
	   echo $this->tiga_dua_tiga_m->getDataserver($id_prodi); 
   }
   function simpanDatarefisi(){
	$this->db->trans_begin();
	$status=true;
	$totalcek=0; 
	$setsimpan=0;
	$id = $this->input->post("id_ref");
	$id_prodi = $this->input->post("id_prodi_ref"); 
	$status = $this->input->post("status");
	$note_status = $this->input->post('note_status');
	
	$username=$this->session->userdata('username');
	$tgl=date('Y-m-d');
	$datainput=array('user_input'=>$username,'tgl_input'=>$tgl);
	$dataubah=array('user_update'=>$username,'tgl_update'=>$tgl);
	
	$datarefisi=array(
		'id_borang'=>$id,
		'standar'=>'3',
		'jenis'=>'1',
		'id_prodi'=>$id_prodi,
		'tgl'=>$tgl,
		'keterangan'=>$note_status);
	$data=array(
		'status'=>$status,
		'note_status'=>$note_status);
	
		$datasimpan=array_merge($datainput,$data);
		//print_r($datasimpan); exit;
		$ceksatu=$this->tiga_dua_tiga_m->editData($id,$datasimpan);
		
		$datasimpanref=array_merge($datainput,$datarefisi);
		//print_r($datasimpan); exit;
		$ceksatu=$this->tiga_dua_tiga_m->simpanDatarefisi($datasimpanref);
	
		
		
		if($ceksatu==1){
			$setsim="ok";	
		}else{
			$totalcek++;
		}
	
	

	$status = $this->db->trans_status();
	if($totalcek>0){
		$this->db->trans_rollback();
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else if ($status === FALSE) {
		$this->db->trans_rollback();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan Data Gagal","status" => "error"));
	}else {
		$this->db->trans_commit();
	//	echo $setsim;
		echo json_encode(array("pesan" => "Informasi <br> Simpan data Berhasil","status" => "success"));
		}
	return $status;	
   }
}?>