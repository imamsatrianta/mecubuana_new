<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ad_log extends CI_Controller {

    function ad_log()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model(array('admin_m'));
        $this->load->helper(array('form', 'url'));
		// backButtonHandle();
	}	
    
	public function index(){
		
		
		$userid=$this->session->userdata('userid');
		//echo menuSpmi(); exit;
		//echo $id_rool;exit;
		if($this->session->userdata('id')){		
		
		   $this->load->view('atas_v');
			$this->load->view('tengah');
			$this->load->view('bawah');
			
		}else{
			
			if($_POST) {
				$this->load->library('form_validation');
				$this->form_validation->set_rules('username', 'Username', 'required');
				$this->form_validation->set_rules('password', 'Password', 'required');
				
				if(!$this->form_validation->run()) {
					$data['pesan'] = 'Username dan Password harus di isi';
					$data['page'] = 'login_v.php';
					$this->load->view('login', $data);
					return;
				}
				
				if(!$this->cekLogin($this->input->post('username'), $this->input->post('password'))) {
					$data['pesan'] = ' Username atau Password Salah';
					$data['page'] = 'login_v.php';
					$data['pesansalah'] = ' Username atau Password Salah';
					$this->load->view('login', $data);
				}else{
					$this->session->set_userdata('id', TRUE);
					$this->session->set_userdata('username', $this->input->post('username'));
					$idm=paramEncrypt("2");
					redirect("masterdata/menu?idm=$idm");
					//redirect("ad_log");
    					//redirect("admin");
                    
					
				}
				
			}else{
			//	echo "xx";exit;
				//$data['page'] = 'login.php';
				$data['pesansalah'] = '';
				$data['pesan'] ='';
				$this->load->view('login',$data);
				
			}	
			
		}
        
	}
    
	function cekLogin($username,$password){
      if($this->admin_m->cekLogin($username,$password))
        return true;
      return false;
	
	}

	function logout(){
	 $this->session->sess_destroy();
	 $data['pesansalah'] = '';
	$data['pesan'] ='';
	$this->load->view('login',$data);
      //redirect('ad_log');
	}
   
    function logData(){
		//echo "logoutData();";
	
//	echo json_encode(array("pesan" => "Informasi <br> Session Habis","status" => "error"));
	 $this->session->sess_destroy();
	 redirect('ad_log');
}

function ubahPasword(){
	$totalcek=0;
	$id = $this->input->post("idpas");
	$username = $this->input->post('usernameubah');
	$password = paramEncrypt($this->input->post('passwordubah')); 
	$data=array(
		'username'=>$username,
		'password'=>$password);
		
	$ceksatu=$this->admin_m->simpanDatapas($id,$data);
	
	if($ceksatu==1){
		$setsim="ok";	
	}else{
		$totalcek++;
	}
	
	if($totalcek>0){
		echo json_encode(array("pesan" => "Informasi <br> Ubah Password Gagal","status" => "error"));
	}else {
		echo json_encode(array("pesan" => "Informasi <br> Ubah Password  Berhasil","status" => "success"));
	}
	
	
}

    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */