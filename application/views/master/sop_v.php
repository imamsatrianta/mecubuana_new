
	
<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					echo aksesTambahupl();
					?>
					 <?php
					echo aksesHapus();
					?>
                    </div><table id="table" 
					data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
						   data-filter-control="true"
                           data-pagination="true"
                           data-url="sop/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="desc">
                        <thead>	
						<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
							<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
							<th data-field="nm_sop"  data-halign="center" data-align="left"  data-sortable="true" >Nama SOP </th>
							<th data-field="nm_dokumen"  data-halign="center" data-align="left"  data-sortable="true" >Nama Standar </th>
                        </tr>
						</thead>
                    </table>
                
              
            </div><!-- /.col -->
          </div>  
       
		
</div> <div class="modal fade" id="modal_formupl" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formupl" name="formupl" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
          
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Nama SOP  </label> 
			<div class="col-md-9">
				<input name="nm_sop" class="form-control input-sm" id="nm_sop" required="required" type="text">
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Nama Standar  </label> 
			<div class="col-md-9">
				<select type="select" name="id_standar" class="form-control select2 input-sm" id="id_standar" required="required"  style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
				</select>
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">File Upload  </label> 
			<div class="col-md-9">
				<img src="" alt="Ambil Foto" id="pic" onclick="takenFile()" height="150" border="1" width="116">
					<div class="hiddenfilez"><input name="file_uploadda"  id="file_uploadda"  onchange="loadFile(event)" type="file"></div> 	
		    </div>
		</div>
		  
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> <script src="<?php echo base_url();?>js/atribut.js"></script>
  <script>
	$(document).ready(function ($) {
	$("#id_standar").append('<option value="">Pilih</option>');
	  $.ajax({
		type: "POST",
		dataType:"JSON",
		url: "<?php echo base_url();?>global_combo/getStandar",
		success: function(result) {  
		$.each(result, function(key, val) {	
		$("#id_standar").append('<option value="'+val.id+'">'+val.nm_dokumen+'</option>');
			
		});
						  
		}
		});
	});			
	 function takenFile(){     
  //	alert('x');
	 $('#file_uploadda').trigger('click');
	}
  
   var loadFile = function(event) {	
    var output = document.getElementById('pic');
	//alert(output);
    output.src = URL.createObjectURL(event.target.files[0]);

  };
	  function editFormtambah(row){
		  $("#id_standar").val(row.id_standar).trigger('change');
		   var image = document.getElementById("pic");
		 
		 var isigbr="<?php echo base_url();?>"  + row['file_upload']; 
	//	 alert(isigbr);
			if (document.getElementById("pic") != ''){
				image.src = isigbr;   
			}else{
				image.src = '';    
			}
	  }
  function operateFormatter(value, row, index) {
       return [
			'<?php echo aksesUbahdua() ?>',
			'<?php echo aksesHapussatu() ?>'
        ].join('');
    }
  </script>