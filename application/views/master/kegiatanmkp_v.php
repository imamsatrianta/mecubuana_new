<script type="text/javascript" src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
	<style>
.hiddenfilez{
	 width: 0px;

 height: 0px;

 overflow: hidden;
}

</style>
<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
			
			<div class="nav-tabs-custom">
				
						<div id="toolbar">
						<?php
						$id_level=$this->session->userdata('id_level');
						 if( $id_level=="3" or  $id_level=="4" or  $id_level=="5"){
							 echo aksesTambahck();
						 }
						?>
						 <?php
						echo aksesHapus();
						?>
						</div><table id="table" 
						data-toolbar="#toolbar"
							   data-toggle="table"
							   data-search="true"
							   data-show-refresh="true"
							   data-show-columns="true"
							   data-show-export="true"
							   data-minimum-count-columns="2"
							   data-filter-control="true"
							   data-pagination="true"
							   data-url="kegiatanmkp/loaddataTabel"
							   data-side-pagination="server"
							   data-pagination="true"
							   data-sort-name="id"
							   data-sort-order="desc">
							<thead>	
							<tr>
								<th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
								<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
								<th data-field="uraian_target"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="htmlFormat" >Target MKP  </th>
								<th data-field="kegiatan"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="htmlFormatkegiatan">Kegiatan MKP  </th>
								<th data-field="waktu"  data-halign="center" data-align="left"  data-sortable="true" >Waktu  </th>
								<th data-field="nm_unit"  data-halign="center" data-align="left"  data-sortable="true" >Unit  </th> 
								<th data-field="dudate"  data-halign="center" data-align="left"  data-sortable="true" >Do Data  </th> 
								<th data-field="ket_revisi"  data-halign="center" data-align="left"  data-sortable="true" >Ket Revisi  </th> 
								<th data-field="status"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="formatStatus" data-cell-style="buatWaenapresentase" >Status  </th> 
							</tr>
							</thead>
						</table>
					 
			</div>
              
            </div><!-- /.col -->
          </div>  
		
		
</div> 



<div class="modal fade" id="modal_formck" role="dialog">
  <div class="modal-dialog" style="width:80%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formck" name="formck" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
		  <div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Target MKP  </label> 
			<div class="col-md-7"> 
				 <input type="hidden" value="" name="id_mkp_target" id="id_mkp_target"/>
				<input name="uraian_target" class="form-control input-sm " id="uraian_target" required="required" type="text">
		    </div>
			<div class="col-md-2">
					<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onclick="cariKegiatan()"  >
					<i class="fa fa-search"></i>
					Cari
					</button>
				
				</div>
		</div>
		
     
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Kegiatan  </label> 
			<div class="col-md-9">
				<textarea class="form-control" rows="5" id="kegiatanrel" name="kegiatan"></textarea>
		    </div>
		</div>
		
		
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Waktu  </label> 
			<div class="col-md-9">
				<select type="select" name="waktu[]" class="form-control text-input input-sm" id="waktu"  style="width: 100%;"  data-placeholder="Select an option" multiple>
			
					  <option value="">--Pilih--</option>
					  <option value="Sm Ganjil">Sm Ganjil</option>
					  <option value="Sm Genap">Sm Genap</option>
					  </select>
				
		    </div>
		</div>
		
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
	<div class="modal fade" id="modalTable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:70%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4>Data Target MKP </h4>
                    </div>
                    <div class="modal-body">
                        <table id="tabledata"
                           data-toggle="table"
                           data-search="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                           data-pagination="true"
						   data-height="500"
                           data-url="<?php echo base_url();?>master/kegiatanmkp/loaddatakegiatan"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="asc">
                            <thead>
                            <tr>
                            <th data-field="nm_mkp"  data-halign="center" data-align="left"  data-sortable="true">MKP  </th>
							<th data-field="uraian_target"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="htmlFormat">Target MKP </th>
							
								<th data-field="cari"  id="pilih" data-halign="center" data-align="center"
                    data-formatter="operateFormatterPilih" data-events="operateEventspilih">Pilih Data</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->		
	
	
	
<div class="modal fade" id="modal_formref" role="dialog">
  <div class="modal-dialog" style="width:60%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formref" name="formref" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id_ref" id="id_ref"/> 
		  <input type="hidden" value="" name="id_unit_ref" id="id_unit_ref"/> 
		  <input type="hidden" value="" name="jenis_ref" id="jenis_ref"/> 
		  <div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Tanggal  Due Data</label> 
			<div class="col-md-7"> 
				<input type="text" class="form-control date-picker input-sm" id="dudate" name="dudate"  placeholder="yyyy-mm-dd" style="width: 100%;" >
		    </div>
			
		</div>
		
     
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Keterangan  </label> 
			<div class="col-md-9">
				<textarea class="form-control" rows="5" id="ket_revisi" name="ket_revisi"></textarea>
		    </div>
		</div>
		
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSaveref" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
	
	
	<script src="<?php echo base_url();?>js/atribut.js"></script>
  