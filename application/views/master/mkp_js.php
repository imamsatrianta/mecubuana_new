<script>
$(document).ready(
    function () {
        $("#waktu").select2();
		$("#id_unit").select2();
    }
);
	  $(document).ready(function ($) {
		   $.ajax({
			type: "POST",
			dataType:"JSON",
			url: "<?php echo base_url();?>global_combo/getUnitdep",
			success: function(result) {  
			$.each(result, function(key, val) {	
			$("#id_unit").append('<option value="'+val.id+"-"+val.jenis+'">'+val.nm_unit+'</option>');
				
			});
							  
			}
			});
			
		//  $("#waktu").select2();
		  $.ajax({
			type: "POST",
			dataType:"JSON",
			url: "<?php echo base_url();?>global_combo/getStandar",
			success: function(result) {  
			$.each(result, function(key, val) {	
			$("#id_standar").append('<option value="'+val.id+'">'+val.nm_dokumen+'</option>');
				
			});
							  
			}
			});
			
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getTahunakademik",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_tahunakademik").append('<option value="'+val.id+'">'+val.nm_tahun_akademik+'</option>');
				$("#id_tahunakademiktarget").append('<option value="'+val.id+'">'+val.nm_tahun_akademik+'</option>');
					
				});
								  
				}
				});
	  });
	function cariMkp(x){
		//$("#id_mkp").html('');
		$("#id_mkp").html('');
		var id_tahunakademik=$("#id_tahunakademiktarget").val();
		$("#id_mkp").append('<option value="">Pilih</option>');
		$.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getMkp/"+id_tahunakademik,
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_mkp").append('<option value="'+val.id+'">'+val.nm_mkp+'</option>');
				
				});
				if(x!='0'){
					//	alert(DIKLAT_LEVEL)  
					$("#id_mkp").val(x).trigger('change');	
					}
								  
				}
				});
	}
	  function editFormtambah(row){
		  $("#id_tahunakademik").val(row.id_tahunakademik);
			bersih();
			ambilDatadetail(row.id);
	  }
	 function ambilDatadetail(id){
			 var sd="id="+id;
				var i=0;
		 		$.ajax({
                    type: "GET",
					  url: '<?php echo base_url();?>master/mkp/getDetail',
                    data: sd,
                    dataType:"json",
                    success: function(result){
					var x=0;
					$.each(result, function(key, val) {
					x++; 
						addRow();
							$('#id_standar'+x).val(val.id_standar);
							
					});	
			}
			});
	}
  function operateFormatter(value, row, index) {
       return [
			'<?php echo aksesUbahdetail() ?>',
			'<?php echo aksesHapussatu() ?>'
        ].join('');
    }
	
	
	// add dokumen
	function bersih() {
		//alert("x")
				var y = totalrow + 1;
				
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat" + x)) {
						hapusBaris("rowmat" + x);
					}
				}
				totalrow = 0;
			}
		
		function hapusBaris(x) {
				if (document.getElementById(x) != null) {
					
					var $row    = $(this).parents('.form-group'),
					 $option = $row.find('[name="option[]"]');
					 $('#' + x).remove();
				}
			}

		  var totalrow= 0;
		function addRow() {
			totalrow++;
			var $template = $('#bookTemplate'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow)
					.attr('id', 'rowmat'+totalrow)
					.insertBefore($template);
			$clone 
				.find('[name="btnDelRowX"]').attr('onClick','hapusBaris("rowmat' + totalrow + '")').end() 
				.find('[name="id_dok"]').attr('name', 'detail[' + totalrow + '][id_dok]').attr('id', 'id_dok'+totalrow).end()
				.find('[name="id_standar"]').attr('name', 'detail[' + totalrow + '][id_standar]').attr('id', 'id_standar'+totalrow).end();
				
			
		}
	// target
function tambahDatatarget(){
     $('#formtarget')[0].reset(); 
	 $('#formtarget').bootstrapValidator('resetForm', true);
      $('#modal_formtarget').modal('show'); 
      $('.modal-title').text('Form Tambah Target MKP'); 
	  document.getElementById('settarget').value=0;
	  document.getElementById('btnSavetarget').disabled=false;
	  document.getElementById('cek').checked = false;
	  $('#tar').hide();
	 // $("#id_grup_satu").select2("val","");
		
    }
	function cekStat(){
		var cek = document.getElementById('cek').checked;
		if(cek==true){
			$('#tar').show();
		}else{
			$('#tar').hide();
			$('#target').val(0);
		}
	}
	$('#formtarget')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDatatarget();
		 e.preventDefault();
		 
		 
    });
	
	function simpanDatatarget(){
	 var cek = document.getElementById('cek').checked;
		if(cek==true){
			var setcek=1;
		}else{
			var setcek=0;
		}
	 document.getElementById("btnSavetarget").disabled=true;
	 var fd = new FormData(document.getElementById("formtarget"));
			fd.append("setcek",setcek);
			$.ajax({
				 url: "mkp/simpanDatatarget",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						//  keluarLogin();
					}
					
					tutupFormtarget();
					loadDatatarget(1, 15,'desc');
			});
			return false;	
			
	 
 }
 function tutupFormtarget(){
		 $('#formtarget')[0].reset(); // reset form on modals 
      	$('#modal_formtarget').modal('hide'); // show bootstrap modal
	}
	
	function loadDatatarget(number, size,order){
			 var $table = $('#tabletarget');
            var offset=(number - 1) * size;
            var limit=size;
            $.ajax({
                    type: "POST",
                    url: "mkp/loaddataTabeltarget?order="+order+"&limit="+limit+"&offset="+offset,
                    dataType:"JSON",
                    success: function(result){
    				 $table.bootstrapTable('load', result);
    			
                    }
                });
        }
		
	function operateFormattertarget(value, row, index) {
       return [
			'<a class="btn btn-sm btn-primary btn-xs" id="kegiatan" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Kegiatan" ><i id="keg" class="glyphicon glyphicon-plus-sign" ></i></a> ',
			'<a class="btn btn-sm btn-primary btn-xs" id="edittarget" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Ubah" ><i id="edt" class="glyphicon glyphicon-edit" ></i></a> ',
			'<a class="btn btn-sm btn-danger btn-xs" id="hapustarget" href="javascript:void(0)" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a> '
        ].join('');
    }
    window.operateEventstarget = {
        'click #edittarget': function (e, value, row, index) {
				editFormtarget(row);
            
        },'click #kegiatan': function (e, value, row, index) {
            kegiatanData(row);
        },
		'click #hapustarget': function (e, value, row, index) {
            hapusdatatarget(row.id);
        }
    };
	
	function kegiatanData(row){
//	alert(row.id);
        $('#formkeg')[0].reset(); 
      $('#modal_formkeg').modal('show'); 
      $('.modal-title').text('Form Tambah Kegiatan MKP'); 
	  document.getElementById('btnSavekeg').disabled=false;
	    
		$('#formkeg').bootstrapValidator('resetForm',false);
		  
		   $('#id_mkp_target').val(row.id);
	      document.getElementById('setkeg').value=0;
		  
    }
	function hapusdatatarget(id){
		bootbox.confirm("Yakin Data akan di Hapus?", function(result) {
			if(result==true){
				 $.ajax({
                    url: 'mkp/hapusdatatarget?id='+id,
                    type: 'POST',
                    success: function (data) {
					//alert(data);
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
					
						
						loadDatatarget(1, 15,'desc');
                        
                    }
                })
			}
               
        });
	}
	function editFormtarget(row){
		 $('#formtarget')[0].reset(); 
      $('#modal_formtarget').modal('show'); 
      $('.modal-title').text('Form Ubah Target MKP'); 
	  document.getElementById('settarget').value=1;
	  document.getElementById('btnSavetarget').disabled=false;
	  
	    for (var name in row) {
		//alert(name);
			 $('#modal_formtarget').find('input[name="' + name + '"]').val(row[name]);
        }
		$("#idtarget").val(row.id);
		$("#uraian_target").val(row.uraian_target);
		$("#id_tahunakademiktarget").val(row.id_tahunakademik);
		var id_mkp= row.id_mkp;
		
		var cek = row.cek;
		if(cek=="1"){
			$('#tar').show();
		}else{
			$('#tar').hide();
			$('#target').val(0);
		}
		cariMkp(id_mkp);
		$('#formtarget').bootstrapValidator('resetForm',false);
		
	}
	
	var $tabletrg = $('#tabletarget'),
        $removetarget = $('#removetarget')
    $(function () {
        $tabletrg.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
            $removetarget.prop('disabled', !$tabletrg.bootstrapTable('getSelections').length);
        });
		
        $removetarget.click(function () {
		var hapus="";
            var ids = $.map($tabletrg.bootstrapTable('getSelections'), function (row) {
				hapus=row.id+","+hapus;
            });
			
			hapusArraytarget(hapus);
            $removetarget.prop('disabled', true);
        });
		
		
    });
	
	function hapusArraytarget(hapus){
		bootbox.confirm("Yakin Data akan di Hapus?", function(result) {
			if(result==true){
				 $.ajax({
                    url: 'mkp/hapusDataarraytarget?data='+hapus,
                    type: 'POST',
                    success: function (data) {
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
						
						loadDatatarget(1, 15,'desc');
                    }
                })
			}
               
        });
	}
	// kegiatan
	$('#formkeg')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDatakeg();
		 e.preventDefault();
    });
	
	function simpanDatakeg(){
	document.getElementById("btnSavekeg").disabled=true;

		var fd = new FormData(document.getElementById("formkeg"));
			fd.append("kegiatanisi",'');
			$.ajax({
			  url: "<?php echo base_url();?>master/mkp/simpanDatakeg",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						 keluarLogin();
					}
					
					 tutupFormkeg();
					// loadData(1, 15,'desc');
			});
			return false;		
 
}

 function tutupFormkeg(){
		 $('#formkeg')[0].reset(); // reset form on modals 
      	$('#modal_formkeg').modal('hide'); // show bootstrap modal
	}
	function operateDetail(index, row,element){
		$.ajax({
				  type: "POST",
				  url: "mkp/loaddatadetailkegiatan?id="+row['id']+"&index="+index,
				  dataType:"JSON",
				  success: function(result) { 

				  	var x1 = "no_baris_"+index;
				  	var el = "tr[data-index='"+index+"']"
					$(el).next('tr.detail-view').addClass(x1);

					var id,id_mkp_target,kegiatan,waktu,id_unit,jenis,nm_unit;
					
					
					var tbl_det;
					tbl_det = '<td colspan="9" align="center"><table border="1" width="100%" style="border-style: solid;border-color:#9F9492;">';
					tbl_det += '<tr style="border-color:#9F9492;">';
					tbl_det += '<td align="center" width="5%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Action</td>';
					tbl_det += '<td align="center" width="30%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;"> Kegiatan</td>';
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Unit Terkait </td>';
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Waktu</td>'
					tbl_det += '</tr>';
        			tbl_det += '<tr style="border-color:#9F9492;">';

        			$.each(eval(result), function (key,value) {
						
        				id = value['id'];
        				id_mkp_target = value['id_mkp_target'];
        				kegiatan = value['kegiatan'];
        				waktu= value['waktu'];
						id_unit=value['id_unit'];
						jenis=value['jenis'];
						nm_unit=value['nm_unit'];
					//	alert(idkeg);
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp;<a class="btn btn-sm btn-primary btn-xs" onclick="editFormkegiatan(\''+id+'\',\''+id_mkp_target+'\',\''+kegiatan+'\',\''+id_unit+'\',\''+waktu+'\',\''+jenis+'\')" id="editdata" class="btn btn-sm btn-primary"  href="#" title="Ubah Kegiatan" ><i id="edtkeg" class="glyphicon glyphicon-edit" ></i></a>';
						tbl_det += '&nbsp;&nbsp;<a class="btn btn-sm btn-danger btn-xs" onclick="hapusKegiatan(\''+id+'\')" id="hapuskegiatan" class="btn btn-sm btn-primary"  href="#" title="Hapus Kegiatan" ><i id="edtkeg" class="glyphicon glyphicon-trash" ></i></a> </td>';
        				tbl_det += '&nbsp;&nbsp; <td style="border-color:#9F9492;" id="kegiatan'+id+'">'+kegiatan+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;'+nm_unit+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;'+waktu+'</td>';
        				tbl_det += '</tr>';

        			});
        			tbl_det += '</table></td>';

					$(".detail-view."+x1).html(tbl_det);

				  }


		});

    }
	
	function editFormkegiatan(id_keg,id_mkp_target,kegiatan,id_unit,waktu,jenis){
		 // $('#formkeg')[0].reset(); 
      $('#modal_formkeg').modal('show'); 
      $('.modal-title').text('Form Ubah Kegiatan MKP'); 
	  document.getElementById('btnSavekeg').disabled=false;
	    
		
		  
		   $('#id_mkp_target').val(id_mkp_target);
	      document.getElementById('setkeg').value=1;
		   $('#id_keg').val(id_keg);
		    $("#kegiatanrel").val(kegiatan);
			
			var waktuarray = new Array();
		  var waktuid=waktu;
		   var splits = waktuid.split(",");
		   for(i = 0; i < splits.length; i++){
				//alert(splits[i]); 
				waktuarray.push (splits[i]);
			}
			
			
		 $('#waktu').val(waktuarray).trigger('change');
		 
		 
		 var idunitarray = new Array();
		  var unitid=id_unit;
		   var splits = unitid.split(",");
		   for(i = 0; i < splits.length; i++){
				//alert(splits[i]); 
				idunitarray.push (splits[i]);
			}
		
		
			
			
		 var idunitok=idunitarray;
		// alert(idunitok)
		 
		$('#id_unit').val(idunitok).trigger('change');
		//$('#formkeg').bootstrapValidator('resetForm',false);
	}
	
	function hapusKegiatan(id){
		bootbox.confirm("Yakin Data akan di Hapus?", function(result) {
			if(result==true){
				 $.ajax({
                    url: 'mkp/hapusdatakegiatan?id='+id,
                    type: 'POST',
                    success: function (data) {
					//alert(data);
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
					
						
						loadDatatarget(1, 15,'desc');
                        
                    }
                })
			}
               
        });
	}
  </script>