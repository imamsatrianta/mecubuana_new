<script>


	
$(document).ready(
    function () {
		readyToStart();
		$("#waktu").select2();
		CKEDITOR.replace('kegiatanrel');
		$(".date-picker").datepicker({ autoclose: true});
    }
	
	
);

function tambahDatack(){
		var halaman=document.getElementById("judulmenu").innerHTML; 
		//alert(halaman);
		 
     $('#formck')[0].reset(); 
	 $('#formck').bootstrapValidator('resetForm', true);
      $('#modal_formck').modal('show'); 
	  /* $('#modal_form').modal('show')
              .draggable({ handle: ".modal-header" });
			  */
      $('.modal-title').text('Form Tambah Kegiatan MKP'); 
	  document.getElementById('set').value=0;
	  document.getElementById('btnSave').disabled=false;
	}
	function editFormck(row){
		<?php 
	$id_level=$this->session->userdata('id_level');
	if($id_level=="7"){
		?>
		var halaman=document.getElementById("judulmenu").innerHTML; 
        $('#formck')[0].reset(); 
      $('#modal_formck').modal('show'); 
      $('.modal-title').text('Form Ubah Kegiatan MKP'); 
	  document.getElementById('set').value=1;
	  document.getElementById('btnSave').disabled=false;
	  
		 var buttonedit=document.getElementById('buttonedit').value;
		 
	    for (var name in row) {
		//alert(name);
			 $('#modal_formck').find('input[name="' + name + '"]').val(row[name]);
        }
		//alert(row);
		if (buttonedit==1){
			//alert("s");
			editFormtambah(row);
		}
	<?php
	}else{
		?>
		var status=row.status;
		
		if(status=="0"){
			var halaman=document.getElementById("judulmenu").innerHTML; 
			$('#formck')[0].reset(); 
		  $('#modal_formck').modal('show'); 
		  $('.modal-title').text('Form Ubah Kegiatan MKP'); 
		  document.getElementById('set').value=1;
		  document.getElementById('btnSave').disabled=false;
			 var buttonedit=document.getElementById('buttonedit').value;
			for (var name in row) {
				 $('#modal_formck').find('input[name="' + name + '"]').val(row[name]);
			}
			if (buttonedit==1){
				editFormtambah(row);
			}
		}else{
			var dodatebatas=row.dudate;
			
			var dodata = dodatebatas.split('-');
			var thn=dodata[0];
			var bln=parseInt(dodata[1]);
			var blnok=bln-1;
			var tgl=dodata[2];
			
			var tglbatas, tanggalsekarang; 
			 tanggalsekarang = new Date(); 
			  tglbatas = new Date(); 
			  tglbatas.setFullYear(thn, blnok, tgl); 
			  
			if(tanggalsekarang>tglbatas){
				bootbox.alert("Perubahan Data Tidak Boleh Lebih dari Tanggal Dodate!"); 
				//alert(blnok+'/'+tglbatas);
				// data tidak bisa direfisi
			}else{
				var halaman=document.getElementById("judulmenu").innerHTML; 
			$('#formck')[0].reset(); 
		  $('#modal_formck').modal('show'); 
		  $('.modal-title').text('Form Ubah Kegiatan MKP'); 
		  document.getElementById('set').value=1;
		  document.getElementById('btnSave').disabled=false;
			 var buttonedit=document.getElementById('buttonedit').value;
			for (var name in row) {
				 $('#modal_formck').find('input[name="' + name + '"]').val(row[name]);
			}
			if (buttonedit==1){
				editFormtambah(row);
			}
			}
			
		}
		
	<?php
	}
	?>
	
		
		//$("#idrool").select2("val", 1);
      
		$('#formck').bootstrapValidator('resetForm',false);
    }
	
	$('#formck')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDatack();
		 e.preventDefault();
		 
		 
    });
	
	
	function simpanDatack(){
	 var edituraian = CKEDITOR.instances.kegiatanrel;
	var uraianok=edituraian.getData();
	var uraian = uraianok.replace("&nbsp;", "");
	
	 document.getElementById("btnSave").disabled=true;
	 var fd = new FormData(document.getElementById("formck"));
			fd.append("kegiatanrel",uraian);
			$.ajax({
				 url: "kegiatanmkp/simpanData",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						  keluarLogin();
					}
					
					tutupFormckdata();
					loadData(1, 15,'desc');
			});
			return false;	
			
	 
 }
	
	function tutupFormckdata(){
		//alert("c")
		// $('#formck')[0].reset(); // reset form on modals 
      	$('#modal_formck').modal('hide'); // show bootstrap modal
		
	}
 $("#tbh").click(function () {
		CKEDITOR.instances['kegiatanrel'].setData("" );
    });
	
	 
	  
	function cariKegiatan(){
		$('#modalTable').modal('show'); 
		$("#modalTable").css({"z-index":"1060"});
		$('html,body').scrollTop(0);
		loadDatamkpkegiatan(1, 15,'desc');
	}	
	  
	 function tutupFormpopup(){
      	$('#modalTable').modal('hide'); // show bootstrap modal
		bukaModal();
	}
	
	function bukaModal(){
		
		$("#modal_formck").css({"overflow-y":"scroll"});
	
	}

	 function operateFormatterPilih(value, row, index) {
       return [
            '<a class="btn btn-sm btn-primary btn-xs" id="pilih" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Pilih" >',
            'Pilih',
            '</a> '
        ].join('');
    }
	
	
  window.operateEventspilih = {
        'click #pilih': function (e, value, row, index) {
            cariDatapopup(row);
        }
    };
		
	function cariDatapopup(row){
		
		 $("#id_mkp_target").val(row.id);
		 var ur=row.uraian_target;
		 var res = ur.substr(0, 200);
		var uraian=strip_html_tags(res);
		 $("#uraian_target").val(uraian);
		 
		 tutupFormpopup();
	 }
	  
	  function loadDatamkpkegiatan(number, size,order){
			 var $table = $('#tabledata');
            var offset=(number - 1) * size;
            var limit=size;
            $.ajax({
                    type: "POST",
                    url: "kegiatanmkp/loaddatakegiatan?order="+order+"&limit="+limit+"&offset="+offset,
                    dataType:"JSON",
                    success: function(result){
    				 $table.bootstrapTable('load', result);
    			
                    }
                });
        }
	  
	   function editFormtambah(row){
		  $("#kegiatansarmut").val(row.uraian_kegiatan);
		  var ur=row.uraian_target;
		
		  var res = ur.substr(0, 200);
		var uraian=strip_html_tags(res);
		  $("#uraian_target").val(uraian);
		  CKEDITOR.instances['kegiatanrel'].setData(row.kegiatan);
		 
			var waktuarray = new Array();
		  var waktuid=row.waktu;
		   var splits = waktuid.split(",");
		   for(i = 0; i < splits.length; i++){
				//alert(splits[i]); 
				waktuarray.push (splits[i]);
			}
			$('#waktu').val(waktuarray).trigger('change');
		
	  }
	<?php 
	$id_level=$this->session->userdata('id_level');
	?>
	function operateFormatter(value, row, index) {
       return [
			'<?php echo aksesUbahck() ?>',
			<?php
			if($id_level=="7"){
			?>
			'<a class="btn btn-sm btn-primary btn-xs" id="refisi" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Revisi" ><i id="edt" class="glyphicon glyphicon-pencil" ></i></a> &nbsp;',
			<?php } ?>
			'<?php echo aksesHapussatu() ?>'
        ].join('');
    }  
	  
function strip_html_tags(str)
{
   if ((str===null) || (str===''))
       return false;
  else
   str = str.toString();
  return str.replace(/<[^>]*>/g, '');
}


function htmlFormat(value, row, index){
	var mutu=row.uraian_target;
	
	var res = mutu.substr(0, 200);
	return strip_html_tags(res);
}
function htmlFormatkegiatan(value, row, index){
	var mutu=row.kegiatan;
	
	var res = mutu.substr(0, 200);
	return strip_html_tags(res);
}
	
function formatStatus(value, row, index){
	if(row.status=="1"){
		var ketref="Ada Revisi";	
	}else{
		var ketref="Tidak Ada Revisi";
	}
	return ketref;
}

	function refisiFormck(row){
		 $('#formref')[0].reset(); 
	 $('#formref').bootstrapValidator('resetForm', true);
      $('#modal_formref').modal('show'); 
      $('.modal-title').text('Form Revisi Kegiatan MKP'); 
	  document.getElementById('btnSaveref').disabled=false;
	   $('#id_ref').val(row.id);
		$('#id_unit_ref').val(row.id_unit); 
		$('#jenis_ref').val(row.jenis); 	   
	  
	}
	
	$('#formref')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDatarefisi();
		 e.preventDefault();
		 
		 
    });
	
	
	function simpanDatarefisi(){
	
	 document.getElementById("btnSaveref").disabled=true;
	 var fd = new FormData(document.getElementById("formref"));
			fd.append("kegiatanrel",'');
			$.ajax({
				 url: "kegiatanmkp/simpanDatarefisi",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						  keluarLogin();
					}
					
					tutupFormck();
					loadData(1, 15,'desc');
			});
			return false;	
			
	 
 }
 function tutupFormck(){
	 $('#formref')[0].reset(); // reset form on modals 
      	$('#modal_formref').modal('hide'); // show bootstrap modal
 }
 
 function buatWaenapresentase(value, row, index) {
	 var dodatebatas=''+row.dudate;
	 if(dodatebatas!="null"){
		var dodata = dodatebatas.split('-');
			var thn=dodata[0];
			var bln=parseInt(dodata[1]);
			var blnok=bln-1;
			var tgl=dodata[2];
			
			var tglbatas, tanggalsekarang; 
			 tanggalsekarang = new Date(); 
			  tglbatas = new Date(); 
			  tglbatas.setFullYear(thn, blnok, tgl); 
		if(row.jmlref==1){
			return {css: {"background-color": "#FF4500",}}; //  hijo muda
		}else if(row.jmlref==2){
			return {css: {"background-color": "#FFFF00",}};  // kuning
		}else if(row.jmlref==3){
			return {css: {"background-color": "#00BFFF",}}; // merah
		}else if(row.status==0){
			return {css: {"background-color": "#1E90FF",}}; // hijo
		}else if(tanggalsekarang>tglbatas){
			return {css: {"background-color": "#008000",}};  // merah
		}
		return {};
			  
	 }else{
		if(row.jmlref==1){
			return {css: {"background-color": "#FF4500",}}; //  hijo muda
		}else if(row.jmlref==2){
			return {css: {"background-color": "#FFFF00",}};  // kuning
		}else if(row.jmlref==3){
			return {css: {"background-color": "#00BFFF",}}; // merah
		}else if(row.status==0){
			return {css: {"background-color": "#1E90FF",}}; // hijo
		}
		return {};
	 }
		//alert(dodatebatas)
		
				
				
    var classes = ['active', 'success', 'info', 'warning', 'danger'];
		
}

  </script>