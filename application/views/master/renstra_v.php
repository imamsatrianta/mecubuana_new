

<div class="content-wrapper" style="min-height:293px;" >
    <section class="content-header">
        <h1>
            <div class="caption">
                <i class="fa fa-plus-square-o font-blue-chambray"></i>
                <span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
                    <?php echo callmenudess() ?>

                </span>
            </div>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                <div id="toolbar">
                    <?php
                    echo aksesTambahdetail();
                    ?>
                    <?php
                    echo aksesHapus();
                    ?>
                </div><table id="table" 
                             data-toolbar="#toolbar"
                             data-toggle="table"
                             data-search="true"
                             data-show-refresh="true"
                             data-show-columns="true"
                             data-show-export="true"
                             data-minimum-count-columns="2"
                             data-filter-control="true"
                             data-pagination="true"
                             data-url="renstra/loaddataTabel"
                             data-side-pagination="server"
                             data-pagination="true"
                             data-sort-name="id"
                             data-sort-order="desc">
                    <thead>	
                        <tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
                            <th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
                            <th data-field="nm_dokumen"  data-halign="center" data-align="left"  data-sortable="true" data-filter-control="input">Nama Dokumen  </th>
                            <th data-field="tgl_awal"  data-halign="center" data-align="left"  data-sortable="true" data-filter-control="input">Tanggal Awal  </th>
                            <th data-field="tgl_akhir"  data-halign="center" data-align="left"  data-sortable="true" data-filter-control="input">Tanggal Akhir  </th>
                            <th data-field="tgl_publish"  data-halign="center" data-align="left"  data-sortable="true" data-filter-control="input">Tanggal Publish  </th>
                        </tr>
                    </thead>
                </table>


            </div><!-- /.col -->
        </div>  


</div> <div class="modal fade" id="modal_formupl" role="dialog">
    <div class="modal-dialog" style="width:80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body form">
                <form  id="formupl" name="formupl" class="form-horizontal" onsubmit="return false;"     >
                    <input type="hidden" value="" name="id" id="id"/> 
                    <input type="hidden" value="" name="set" id="set"/> 

                    <div class="form-group ">
                        <label class="control-label col-md-3" for1="menudes">Nama Dokumen  </label> 
                        <div class="col-md-9">
                            <input name="nm_dokumen" class="form-control input-sm" id="nm_dokumen" required="required" type="text">
                        </div>
                    </div>
                    <div class="form-group ">	
                        <label class="control-label col-md-3" for1="menudes">Tanggal Awal    </label> 
                        <div class="col-md-2">

                            <input type="text" class="form-control date-picker input-sm" id="tgl_awal" name="tgl_awal"  placeholder="yyyy-mm-dd" style="width: 100%;" required="required">
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="control-label col-md-3" for1="menudes">Tanggal Akhir  </label> 
                        <div class="col-md-2">
                            <input type="text" class="form-control date-picker input-sm" id="tgl_akhir" name="tgl_akhir"  placeholder="yyyy-mm-dd" style="width: 100%;" required="required">
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="control-label col-md-3" for1="menudes">Tanggal Publish  </label> 
                        <div class="col-md-2">
                            <input type="text" class="form-control date-picker input-sm" id="tgl_publish" name="tgl_publish"  placeholder="yyyy-mm-dd" style="width: 100%;" required="required"> 	
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="control-label col-md-3" for1="menudes">File Upload  </label> 
                        <div class="col-md-9">
                            <img src="" alt="Ambil Foto" id="pic" onclick="takenFile()" height="150" border="1" width="116">
                            <div class="hiddenfilez"><input name="file_uploadda"  id="file_uploadda"  onchange="loadFile(event)" type="file"></div> 	
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="tab-pane active" id="tab_1">

                            <div class="panel panel-default">
                                <div class="panel-heading" >
                                    <button type="button" id="btnNewRow" name="btnNewRow" class="btn btn-xs btn-success" onclick="addRow();"><i class="fa fa-plus"></i>
                                        Baris Baru
                                    </button>
                                </div>

                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
                                            <thead> 
                                                <tr>
                                                    <td align="center" class="ganjil">Action </td>
                                                    <td align="center" class="ganjil">No</td>
                                                    <td align="center" class="ganjil">Uraian  </td>
                                                    <td align="center" class="ganjil">Target</td>
                                                    <td align="center" class="ganjil">Tahun Awal</td>
                                                    <td align="center" class="ganjil">Tahun Akhir</td>
                                                    <td align="center" class="ganjil">Tahun Tengah</td>
                                                </tr>

                                                <tr id="bookTemplate" name="rowda" class="hide">
                                                    <td  style="text-align: center;" valign="top">
                                                        <button type='button' style='text-align: center;' id='btnDelRowX' name='btnDelRowX' class='btn btn-sm btn-danger btn-xs btn-round' title='Hapus' ><i class='fa fa-minus'></i></button> </td>

                                                    <td align="center" style="text-align: center;" class="form-group " valign="top">
                                                        <input  class='form-control input-sm '  type='hidden'  name='id_dok' id='id_dok' >	
                                                        <input  class='form-control input-sm '  type='text'  name='nomor' id='nomor' >	
                                                    </td>
                                                    <td align="center" style="text-align: center;" class="form-group " valign="top">
                                                        <input  class='form-control input-sm '  type='text'  name='uraian' id='uraian' >	
                                                    </td>   
                                                    <td align="center" style="text-align: center;" valign="top">
                                                        <input  class='form-control input-sm '  type='text'  name='target' id='target' >
                                                    </td>
                                                    <td align="center" style="text-align: center;" valign="top">
                                                        <input  class='form-control input-sm '  type='text'  name='tahun_awal' id='tahun_awal' >
                                                    </td>
                                                    <td align="center" style="text-align: center;" valign="top">
                                                        <input  class='form-control input-sm '  type='text'  name='tahun_tengah' id='tahun_tengah' >
                                                    </td>
                                                    <td align="center" style="text-align: center;" valign="top">
                                                        <input  class='form-control input-sm '  type='text'  name='tahun_akhir' id='tahun_akhir' >
                                                    </td>

                                                </tr>


                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->

                                </div>
                                <!-- /.panel-body -->
                            </div>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" id="btnSave" class="btn btn-primary"   >
                    <i class="fa fa-save"></i>
                    Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
                    Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form> <script src="<?php echo base_url(); ?>js/atribut.js"></script>
<script>
                                                             $(document).ready(function ($) {
                                                                 $(".date-picker").datepicker({autoclose: true});
                                                                 $('.date-picker').on('changeDate show', function (e) {
                                                                     $('#formupl').bootstrapValidator('revalidateField', 'tgl_akhir');
                                                                     $('#formupl').bootstrapValidator('revalidateField', 'tgl_awal');
                                                                     $('#formupl').bootstrapValidator('revalidateField', 'tgl_publish');
                                                                 });
                                                             });

                                                             function takenFile() {
                                                                 //	alert('x');
                                                                 $('#file_uploadda').trigger('click');
                                                             }

                                                             var loadFile = function (event) {
                                                                 var output = document.getElementById('pic');
                                                                 //alert(output);
                                                                 output.src = URL.createObjectURL(event.target.files[0]);

                                                             };
                                                             function editFormtambah(row) {
                                                                 var image = document.getElementById("pic");

                                                                 var isigbr = "<?php echo base_url(); ?>" + row['file_upload'];
                                                                 //	 alert(isigbr);
                                                                 if (document.getElementById("pic") != '') {
                                                                     image.src = isigbr;
                                                                 } else {
                                                                     image.src = '';
                                                                 }
                                                                 bersih();
                                                                 ambilDatadetail(row.id);
                                                             }
                                                             function ambilDatadetail(id) {
                                                                 //var url="<?php echo base_url(); ?>dokumen/37689baruspaj.pdf";
                                                                 //alert (url);
                                                                 var sd = "id=" + id;
                                                                 var i = 0;
                                                                 $.ajax({
                                                                     type: "GET",
                                                                     url: '<?php echo base_url(); ?>master/renstra/getDetail',
                                                                     data: sd,
                                                                     dataType: "json",
                                                                     success: function (result) {
                                                                         var x = 0;
                                                                         $.each(result, function (key, val) {
                                                                             x++;
                                                                             addRow();
                                                                             $('#uraian' + x).val(val.uraian);
                                                                             $("#nomor" + x).val(val.nomor);
                                                                             $("#target" + x).val(val.target);
                                                                             $("#tahun_awal" + x).val(val.tahun_awal);
                                                                             $("#tahun_tengah" + x).val(val.tahun_tengah);
                                                                             $("#tahun_akhir" + x).val(val.tahun_akhir);
                                                                         });
                                                                     }
                                                                 });
                                                             }
                                                             function operateFormatter(value, row, index) {
                                                                 return [
                                                                     '<?php echo aksesUbahdua() ?>',
                                                                     '<?php echo aksesHapussatu() ?>'
                                                                 ].join('');
                                                             }


                                                             // add dokumen
                                                             function bersih() {
                                                                 //alert("x")
                                                                 var y = totalrow + 1;

                                                                 for (x = 0; x < y; x++) {
                                                                     if (document.getElementById("rowmat" + x)) {
                                                                         hapusBaris("rowmat" + x);
                                                                     }
                                                                 }
                                                                 totalrow = 0;
                                                             }

                                                             function hapusBaris(x) {
                                                                 if (document.getElementById(x) != null) {

                                                                     var $row = $(this).parents('.form-group'),
                                                                             $option = $row.find('[name="option[]"]');
                                                                     $('#' + x).remove();
                                                                 }
                                                             }

                                                             var totalrow = 0;
                                                             function addRow() {
                                                                 totalrow++;
                                                                 var $template = $('#bookTemplate'),
                                                                         $clone = $template
                                                                         .clone()
                                                                         .removeClass('hide')
                                                                         .removeAttr('id')
                                                                         .attr('data-book-index', totalrow)
                                                                         .attr('id', 'rowmat' + totalrow)
                                                                         .insertBefore($template);
                                                                 $clone
                                                                         .find('[name="btnDelRowX"]').attr('onClick', 'hapusBaris("rowmat' + totalrow + '")').end()
                                                                         .find('[name="id_dok"]').attr('name', 'detail[' + totalrow + '][id_dok]').attr('id', 'id_dok' + totalrow).end()
                                                                         .find('[name="nomor"]').attr('name', 'detail[' + totalrow + '][nomor]').attr('id', 'nomor' + totalrow).end()
                                                                         .find('[name="uraian"]').attr('name', 'detail[' + totalrow + '][uraian]').attr('id', 'uraian' + totalrow).end()
                                                                         .find('[name="target"]').attr('name', 'detail[' + totalrow + '][target]').attr('id', 'target' + totalrow).end()
                                                                         .find('[name="tahun_awal"]').attr('name', 'detail[' + totalrow + '][tahun_awal]').attr('id', 'tahun_awal' + totalrow).end()
                                                                         .find('[name="tahun_tengah"]').attr('name', 'detail[' + totalrow + '][tahun_tengah]').attr('id', 'tahun_tengah' + totalrow).end()
                                                                         .find('[name="tahun_akhir"]').attr('name', 'detail[' + totalrow + '][tahun_akhir]').attr('id', 'tahun_akhir' + totalrow).end();


                                                             }
</script>