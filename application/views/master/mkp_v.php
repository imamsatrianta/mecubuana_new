
	
<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
			
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
				  <li class="active"><a href="#tab_1" data-toggle="tab">MKP	</a></li>
				  <li><a href="#tab_2" data-toggle="tab">Target dan Realisasi</a></li>
				
				  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab_1">
						<div id="toolbar">
						<?php
						echo aksesDetail();
						?>
						 <?php
						echo aksesHapus();
						?>
						</div><table id="table" 
						data-toolbar="#toolbar"
							   data-toggle="table"
							   data-search="true"
							   data-show-refresh="true"
							   data-show-columns="true"
							   data-show-export="true"
							   data-minimum-count-columns="2"
							   data-filter-control="true"
							   data-pagination="true"
							   data-url="mkp/loaddataTabel"
							   data-side-pagination="server"
							   data-pagination="true"
							   data-sort-name="id"
							   data-sort-order="desc">
							<thead>	
							<tr>
								<th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
								<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
								<th data-field="nm_mkp"  data-halign="center" data-align="left"  data-sortable="true" >Nama MKP  </th>
								<th data-field="nm_tahun_akademik"  data-halign="center" data-align="left"  data-sortable="true" >Tahun Akademik  </th>
							</tr>
							</thead>
						</table>
					</div>
					
					 <div class="tab-pane" id="tab_2">
					 <div id="toolbartarget">
						<button id="tbh" type="button" class="btn btn-primary " data-toggle="modal" onclick="tambahDatatarget()" >
						<i class="glyphicon glyphicon-plus"> </i>   Tambah
						</button>
						 <button onclick="" id="removetarget" class="btn btn-danger" disabled>
                            <i class="glyphicon glyphicon-remove"></i> Hapus
                        </button> 
						</div>
						<table id="tabletarget" 
						data-toolbar="#toolbartarget"
							   data-toggle="table"
							   data-search="true"
							   data-show-refresh="true"
							   data-show-columns="true"
							   data-show-export="true"
							   data-minimum-count-columns="2"
							   data-filter-control="true"
							   data-pagination="true"
							   data-url="mkp/loaddataTabeltarget"
							   data-side-pagination="server"
							   data-pagination="true"
							   data-sort-name="id"
							   data-sort-order="desc"
							   data-detail-view="true"
						   data-detail-formatter="operateDetail">
							<thead>	
							<tr>
								<th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
								<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormattertarget" data-events="operateEventstarget">Action</th>
								<th data-field="uraian_target"  data-halign="center" data-align="left"  data-sortable="true" >Uraian Target  </th>
								<th data-field="cek"  data-halign="center" data-align="left"  data-sortable="true" >Status Monitoring  </th>
								<th data-field="target"  data-halign="center" data-align="left"  data-sortable="true" >Jumlah Target  </th>
								<th data-field="nm_mkp"  data-halign="center" data-align="left"  data-sortable="true" >Nama MKP  </th>
								<th data-field="nm_tahun_akademik"  data-halign="center" data-align="left"  data-sortable="true" >Tahun Akademik  </th>
								
							</tr>
							</thead>
						</table>
					 
					 </div>
					 
				</div>
			</div>
              
            </div><!-- /.col -->
			
			
			
			
			
			
          </div>  
       
		
</div> 

<div class="modal fade" id="modal_formdetail" role="dialog">
  <div class="modal-dialog" style="width:50%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formdetail" name="formdetail" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
          
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Nama MKP  </label> 
			<div class="col-md-9">
				<input name="nm_mkp" class="form-control input-sm" id="nm_mkp" required="required" type="text">
		    </div>
		</div>
		<div class="form-group ">	
			<label class="control-label col-md-3" for1="menudes">Tahun Akademik    </label> 
			<div class="col-md-5">
				<select type="select" name="id_tahunakademik" class="form-control select2 input-sm" id="id_tahunakademik" required="required"  style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
				</select>
				
		    </div>
		</div>
		
		
		<div class="col-md-12">
			<div class="tab-pane active" id="tab_1">
						
						<div class="panel panel-default">
							<div class="panel-heading" >
							   <button type="button" id="btnNewRow" name="btnNewRow" class="btn btn-xs btn-success" onclick="addRow();"><i class="fa fa-plus"></i>
								Baris Baru
								</button>
							</div>
							
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
										<thead> 
											<tr>
												<td align="center" class="ganjil">Action </td>
												<td align="center" class="ganjil">Standar</td>
											</tr>
											
											<tr id="bookTemplate" name="rowda" class="hide">
												<td  style="text-align: center;" valign="top">
												<button type='button' style='text-align: center;' id='btnDelRowX' name='btnDelRowX' class='btn btn-sm btn-danger btn-xs btn-round' title='Hapus' ><i class='fa fa-minus'></i></button> </td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='hidden'  name='id_dok' id='id_dok' >	
												<select type="select" name="id_standar" class="form-control select2 input-sm" id="id_standar" style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
				</select>
												</td>
												
												
											</tr>
											
									
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
								<!-- /.table-responsive -->
								
							</div>
							<!-- /.panel-body -->
						</div>
					  </div>
			</div>
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
	
	<div class="modal fade" id="modal_formtarget" role="dialog">
  <div class="modal-dialog" style="width:50%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formtarget" name="formtarget" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="idtarget" id="idtarget"/> 
		  <input type="hidden" value="" name="settarget" id="settarget"/> 
          
		
		<div class="form-group ">	
			<label class="control-label col-md-4" for1="menudes">Tahun Akademik    </label> 
			<div class="col-md-5">
				<select type="select" name="id_tahunakademiktarget" class="form-control select2 input-sm" id="id_tahunakademiktarget" required="required"  style="width: 100%;" onchange="cariMkp('0')" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
				</select>
				
		    </div>
		</div>
		<div class="form-group ">	
			<label class="control-label col-md-4" for1="menudes"> MKP    </label> 
			<div class="col-md-5">
				<select type="select" name="id_mkp" class="form-control select2 input-sm" id="id_mkp" required="required"  style="width: 100%;" >
					 
				</select>
				
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-4" for1="menudes">Uraian Target MKP  </label> 
			<div class="col-md-8">
				 <textarea class="form-control" rows="5" id="uraian_target" name="uraian_target"></textarea>
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-4" for1="menudes">Status Monitoring  </label> 
			<div class="col-md-8">
				<label class="checkbox" id="id_showPasswordinout">
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" name="cek"  id="cek" onclick="cekStat()" /> Monitoring/Tidak 
		  </label>
		    </div>
		</div>
		<div class="form-group " id="tar">
			<label class="control-label col-md-4" for1="menudes">Jumlah Target  </label> 
			<div class="col-md-3">
				<input name="target" class="form-control input-sm nomor2" id="target" type="text">
		    </div>
		</div>
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSavetarget" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	
	</form> 
	
	
	<div class="modal fade" id="modal_formkeg" role="dialog">
   <div class="modal-dialog" style="width:60%">
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formkeg" name="formkeg" class="form-horizontal" onsubmit="return false;"     >
		<input type="hidden" value="" name="id_keg" id="id_keg"/> 
          <input type="hidden" value="" name="id_mkp_target" id="id_mkp_target"/> 
          <input type="hidden" value="" name="setkeg" id="setkeg"/> 
		 
		  <div class="form-body">
			  <div class="form-group field-username">
				  <label class="control-label col-md-3" for1="username">Nama Kegiatan  </label> 
				  <div class="col-md-9">
				   <textarea class="form-control" rows="5" id="kegiatanrel" name="kegiatan"></textarea>
				  </div>
			  </div>
          </div>
		  <div class="form-body">
			  <div class="form-group field-username">
				  <label class="control-label col-md-3" for1="username">Waktu </label> 
				  <div class="col-md-9">
				  <select type="select" name="waktu[]" class="form-control text-input input-sm" id="waktu"  style="width: 100%;"  data-placeholder="Select an option" multiple>
			
					  <option value="">--Pilih--</option>
					  <option value="Sm Ganjil">Sm Ganjil</option>
					  <option value="Sm Genap">Sm Genap</option>
					  </select>
				
				  </div>
			  </div>
          </div>
		 
		  <div class="form-body">
			  <div class="form-group field-username">
				  <label class="control-label col-md-3" for1="username">Unit / Departemen   </label> 
				  <div class="col-md-9">
					<select type="select" name="id_unit[]" class="form-control text-input input-sm" id="id_unit"  style="width: 100%;"  data-placeholder="Select an option" multiple>
					  <option value="">--Pilih--</option>
					  </select>
				  </div>
			  </div>
          </div>
		
		
		  </div>
          <div class="modal-footer">
            <button type="submit" id="btnSavekeg" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" id="btlkeg" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
	<script src="<?php echo base_url();?>js/atribut.js"></script>
  