<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			
			
      <div class="modal-header">
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formbor" name="formbor" class="form-horizontal" onsubmit="return false;" >
		<div class="row">
			<div class="col-md-6">
				<div class="form-group " id="jab">
					<label class="control-label col-md-4" for1="menudes">Tahun Akademik  </label> 
					<div class="col-md-6">
						<select type="select" name="id_tahunakademik" class="form-control select2 input-sm" id="id_tahunakademik" required="required"  style="width: 100%;" >
							 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
						</select>
					</div>				
				</div>
				<div class="form-group " id="jab">
					<label class="control-label col-md-4" for1="menudes">Jenjang Prodi  </label> 
					<div class="col-md-6">
						<select type="select" name="id_jenjang" class="form-control select2 input-sm" id="id_jenjang" required="required"  style="width: 100%;" onchange="getProdi('0')" >
							 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
						</select>
					</div>				
				</div>
				<div class="form-group " id="jab">
					<label class="control-label col-md-4" for1="menudes">Prodi  </label> 
					<div class="col-md-8">
						<select type="select" name="id_prodi" class="form-control select2 input-sm" id="id_prodi" required="required"  style="width: 100%;" >
							 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
						</select>
					</div>
				</div>
				
				<div class="form-group " style="text-align:center;">
					<button type="button" id="btnSave" class="btn btn-primary" onclick="cetakPenilaian()"  >
					<i class="fa fa-file-word-o"></i>
					Print </button>
				</div>
			</div>
			
		</div>
		
        </div>
         
	</form> 
	
	
            </div><!-- /.col -->
			
          </div>  
       
		
</div> 

<script>

	function cetakPenilaian(){
		var ddd=$("#id_tahunakademik").val();
		var id_jenjangprodi=$("#id_jenjang").val();  //row.id_jenjangprodi;
		var id_tahunakademik=$("#id_tahunakademik").val(); //row.id_tahunakademik;
		var id_prodi=$("#id_prodi").val();  //row.id_prodi;
		window.location="<?php echo base_url(); ?>borang/lapborang/download_laporan?id_jenjangprodi="+id_jenjangprodi+"&id_tahunakademik="+id_tahunakademik+"&id_prodi="+id_prodi,'_blank';
	
	}
	
  $(document).ready(function ($) {
	  $("select").select2();
	getJenjang();
	getTahunakademik();
	  });
	
	function getTahunakademik(){
		$("#id_tahunakademik").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getTahunakademik",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_tahunakademik").append('<option value="'+val.id+'">'+val.nm_tahun_akademik+'</option>');
					
				});
			
								  
				}
				});
	  
	}
	
	function getJenjang(){
		$("#id_jenjang").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getJenjangprodi",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_jenjang").append('<option value="'+val.id+'">'+val.nm_jenjangprodi+'</option>');
					
				});
			
								  
				}
				});
	  
	}
	
	function getProdi(x){
		$("#id_prodi").html('');
		var id_jenjang=$("#id_jenjang").val();
		$("#id_prodi").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getProdijenjang/"+id_jenjang,
				success: function(result) {  
				if(x!="0"){
					$("#id_prodi").html('');
					}	
				$.each(result, function(key, val) {	
				$("#id_prodi").append('<option value="'+val.id+'">'+val.nm_prodi+'</option>');
					
				});
					if(x!="0"){
					 $("#id_prodi").val(x).trigger('change');
					}	  
				}
				});	
	}
</script>