
<script>
var parameter="<?php echo $_GET['parameter'];?>";
var level="<?php echo $_GET['level'];?>";
var levelprodi="<?php echo $this->session->userdata('levelprodi');?>";


<?php   
$url=$_SERVER['PHP_SELF'];
$nameurl =pathinfo($url, PATHINFO_FILENAME);
if($nameurl=="standarsatu"){
	$namatabel="br_standarsatu";
	$standar="1";
}else if($nameurl=="standardua"){
	$namatabel="br_standardua";
	$standar="2";
}else if($nameurl=="standartiga"){
	$namatabel="br_standartiga";
	$standar="3";
}else if($nameurl=="standarempat"){
	$namatabel="br_standarempat";
	$standar="4";
}else if($nameurl=="standarlima"){
	$namatabel="br_standarlima";
	$standar="5";
}else if($nameurl=="standarenam"){
	$namatabel="br_standarenam";
	$standar="6";
}else{
	$namatabel="br_standartujuh";
	$standar="7";
}
?>
var standar="<?php echo $standar;?>";
var namatabel="<?php echo $namatabel;?>";

function loadDataborang(number, size,order){
			var $table = $('#table');
            var offset=(number - 1) * size;
            var limit=size;
			
            $.ajax({
                    type: "POST",
                    url: "standarevaluasi/loaddataTabel?order="+order+"&limit="+limit+"&offset="+offset+"&parameter="+parameter+"&level="+level+"&namatabel="+namatabel+"&standar="+standar,
                    dataType:"JSON",
                    success: function(result){
    				 $table.bootstrapTable('load', result);
    			
                    }
                });
        }
		
function getParamstandar(params) {
		    return {
				standar: standar,
				namatabel: namatabel,
				level: level,
		        parameter: parameter,
				limit: params.limit,
				offset: params.offset,
				search: params.search,
				sort: params.sort,
				order: params.order
		    };
		}
function strip_html_tags(str)
{
   if ((str===null) || (str===''))
       return false;
  else
   str = str.toString();
  return str.replace(/<[^>]*>/g, '');
}


function htmlMutu(value, row, index){
	var mutu=row.uraian;
	var res = mutu.substr(0, 200);
	return strip_html_tags(res);
}



	

function statusBorang(value, row, index){
	if(row.status=="3"){
		return "Dikembalikan";
	}else if(row.status=="2"){
		return "Disetujui";
	}else if(row.status=="1"){
		return "Diajukan";
	}else{
		return "Baru";
	}
}
function statusEvaluasi(value, row, index){
	if(row.status_evaluasi=="2"){
		return "PAEE";
	}else if(row.status_evaluasi=="1"){
		return "Wakil Dekan/Dekan";
	}else{
		return "Prodi";
	}
}


	/// form proses evaluasi
	 function operateFormatterborangevl(value, row, index) { 
			 return [
			'<a class="btn btn-sm btn-primary btn-xs" id="refisiev" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Evaluasi" ><i id="edt" class="glyphicon glyphicon-pencil" ></i></a> ',
			'<a class="btn btn-sm btn-primary btn-xs" id="penilaian" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Cetak Borang" ><i id="nilai" class="fa fa-file-word-o" ></i></a> '
			
			].join('');
		
	}
	
	window.operateEventsborangevl = {
        'click #refisiev': function (e, value, row, index) {
				refisiFormevaluasi(row);
        },
		 'click #penilaian': function (e, value, row, index) {
				//penilaianData(row);
				cetakBorang(row);
        }
    };
	
	function cekEvaluasi(){
		var status_evaluasi=$('#status_evaluasi').val(); 
		if(status_evaluasi=="0"){
			$("#cekkirim").show();
			$(".date-picker").datepicker({ autoclose: true});
		}else{
			$("#cekkirim").hide();
			$('#tgl_dodate').val(''); 
		}
	}
	function refisiFormevaluasi(row){
		$("#cekkirim").hide();
		$('#tgl_dodate').val(''); 
		
		 $('#formevl')[0].reset(); 
		 $('#formevl').bootstrapValidator('resetForm', true);
		  $('#modal_formevl').modal('show'); 
		  $('.modal-title').text('Form Evaluasi Borang'); 
		  document.getElementById('btnSaveevl').disabled=false;
		   $('#id_tahunakademikevl').val(row.id_tahunakademik); 
		   $('#id_jenjangprodievl').val(row.id_jenjangprodi); 
		    $('#id_prodievl').val(row.id_prodi); 
			 $('#standar').val(standar);
			
	}
	
	$('#formevl')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDataevaluasi();
		 e.preventDefault();
		 
		 
    });
	
	
	function simpanDataevaluasi(){
	
	 document.getElementById("btnSaveevl").disabled=true;
	 var fd = new FormData(document.getElementById("formevl"));
			fd.append("kegiatanrel",'');
			$.ajax({
				 url: "standarevaluasi/simpanDatareevaluasi",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						  keluarLogin();
					}
					
					tutupFormevl();
					loadDataborang(1, 15,'desc');
			});
			return false;	
			
	 
 }
 function tutupFormevl(){
	 $('#formevl')[0].reset(); // reset form on modals 
      	$('#modal_formevl').modal('hide'); // show bootstrap modal
 }
 
	function penilaianData(row){
		var id_jenjangprodi=row.id_jenjangprodi;
		var id_tahunakademik=row.id_tahunakademik;
		var id_prodi=row.id_prodi;
		window.location="<?php echo base_url(); ?>borang/standarevaluasi/download_penilaian?id_jenjangprodi="+id_jenjangprodi+"&id_tahunakademik="+id_tahunakademik+"&id_prodi="+id_prodi;
	}
	
	function cetakBorang(row){
		var id_jenjangprodi=row.id_jenjangprodi;
		var id_tahunakademik=row.id_tahunakademik;
		var id_prodi=row.id_prodi;
		var tabel=namatabel;
		var standarda=standar;
		//alert(standarda)
		window.open("<?php echo base_url(); ?>borang/standarevaluasi/cetakBorang?id_jenjangprodi="+id_jenjangprodi+"&id_tahunakademik="+id_tahunakademik+"&id_prodi="+id_prodi+"&tabel="+tabel+"&standar="+standar);
		//window.location="<?php echo base_url(); ?>borang/standarevaluasi/cetakBorang?id_jenjangprodi="+id_jenjangprodi+"&id_tahunakademik="+id_tahunakademik+"&id_prodi="+id_prodi+"&tabel="+tabel+"&standar="+standar;
	}
  function DownloadFile(){ 
    window.location="<?php echo base_url(); ?>borang/standarevaluasi/download_penilaian";

  }

	</script>
 
  