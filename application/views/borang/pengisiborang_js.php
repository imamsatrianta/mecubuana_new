
	<script>
var level="<?php echo $_GET['level'];?>";
var levelprodi="<?php echo $this->session->userdata('levelprodi');?>";
function loadDataborang(number, size,order){
			var $table = $('#table');
            var offset=(number - 1) * size;
            var limit=size;
			
            $.ajax({
                    type: "POST",
                    url: "pengisiborang/loaddataTabel?order="+order+"&limit="+limit+"&offset="+offset+"&level="+level,
                    dataType:"JSON",
                    success: function(result){
    				 $table.bootstrapTable('load', result);
    			
                    }
                });
        }
		
function getParamstandar(params) {
		    return {
				level: level,
				limit: params.limit,
				offset: params.offset,
				search: params.search,
				sort: params.sort,
				order: params.order
		    };
		}

	 
	
	  $(document).ready(function ($) {
		   $(".close").click(function(){
			tutupFormborang();		
			});
		  $('#modal_formbor').modal({backdrop: 'static',keyboard: false,show: false });
		getTahunakademik();
		$(".date-picker").datepicker({ autoclose: true});
	  });
	  
	  $("#tbh").click(function () {
		  bersih();
    });
	
	function operateFormatterborang(value, row, index) {
			 return [
			'<a class="btn btn-sm btn-primary btn-xs" id="editborang" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Ubah" ><i id="edt" class="glyphicon glyphicon-edit" ></i></a> ',
			'<a class="btn btn-sm btn-danger btn-xs" id="hapusborang" href="javascript:void(0)" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a> '
			
			].join('');
		
	 
	}
	
	window.operateEventsborang = {
       'click #editborang': function (e, value, row, index) {
				editFormborang(row);
        },'click #hapusborang': function (e, value, row, index) {
				hapusBorang(row.id);
        }
    };
	
	
	function hapusBorang(id){
		bootbox.confirm("Yakin Data akan di Hapus?", function(result) {
			if(result==true){
				 $.ajax({
                    url: 'pengisiborang/hapusData?id='+id,
                    type: 'POST',
                    success: function (data) {
					//alert(data);
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
					
						
						loadDataborang(1, 15,'desc');
                        
                    }
                })
			}
               
        });
	}
	function tambahDataborang(){
		bersih();
     $('#formbor')[0].reset(); 
	 $('#formbor').bootstrapValidator('resetForm', true); 
      $('#modal_formbor').modal('show'); 
      $('.modal-title').text('Form Tambah Pengisi Borang'); 
	  document.getElementById('set').value=0;
	  document.getElementById('btnSave').disabled=false;
	 $("#id_jenjangprodi").val("<?php echo $_GET['level'];?>");	
	 var jenjang="<?php echo $_GET['level'];?>";
	 getProdi(jenjang,0,0);
	 $("#cke_1_contents").css({"height":"400px"});
	 
    }
	
	function editFormborang(row){
		bersih();
        $('#formbor')[0].reset(); 
      $('#modal_formbor').modal('show'); 
      $('.modal-title').text('Form Ubah Pengisi Borang'); 
	  document.getElementById('set').value=1;
	  document.getElementById('btnSave').disabled=false;
	  
		 var buttonedit=document.getElementById('buttonedit').value;
		 
	    for (var name in row) {
		//alert(name);
			 $('#modal_formbor').find('input[name="' + name + '"]').val(row[name]);
        }
		$("#id_tahunakademik").val(row.id_tahunakademik);
		
		
		//alert(row.kd_server)
		getProdi(row.id_jenjangprodi,row.id_prodi,row.kd_server);
		$("#cke_1_contents").css({"height":"400px"});
		$('#formbor').bootstrapValidator('resetForm',false);
		getEditdetail(row.id);
		
		
    }
	
	function getTahunakademik(){
		$("#id_tahunakademik").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getTahunakademik",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_tahunakademik").append('<option value="'+val.id+'">'+val.nm_tahun_akademik+'</option>');
					
				});
			
								  
				}
				});
	  
	}
	
	function tutupFormborang(){
		 $('#formbor')[0].reset(); // reset form on modals 
      	$('#modal_formbor').modal('hide'); // show bootstrap modal
	}
	
	function getProdi(jenjang,id_prodi,kd_server){
		$("#id_prodi").html('');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getProdijenjang/"+jenjang,
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_prodi").append('<option value="'+val.id+'-'+val.kd_server+'">'+val.nm_prodi+'</option>');
					
				});
					if(id_prodi!='0'){
						$("#id_prodi").val(id_prodi+'-'+kd_server);
					}
					
				}
				});	
	}
	
	$('#formbor')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDataborang();
		 e.preventDefault();
		 
		 
    });
	
	function simpanDataborang(){
	
	 document.getElementById("btnSave").disabled=true;
	 var fd = new FormData(document.getElementById("formbor"));
			fd.append("uraiandata",'');
			$.ajax({
				 url: "pengisiborang/simpanData",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						//  keluarLogin();
					}
					
					tutupFormborang();
					loadDataborang(1, 15,'desc');
			});
			return false;	
			
	 
	}
	
	
	
	
	
	function getEditdetail(id){
		bersih();
		var sd="id="+id;
				var i=0;
		 		$.ajax({
                    type: "GET",
					  url: '<?php echo base_url();?>borang/pengisiborang/getEditdetail',
                    data: sd,
                    dataType:"json",
                    success: function(result){
					var x=0;
					$.each(result, function(key, val) {
						addRow();
					x++; 
							$('#nm_karyawan'+x).val(val.nm_karyawan);
							$('#tgl_penisian'+x).val(val.tgl_penisian);
							$('#id_karyawan'+x).val(val.id_karyawan);
					});	
			}
			});
	}
	
	



		function bersih() {
				var y = totalrow + 1;
				
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat" + x)) {
						hapusBaris("rowmat" + x);
					}
				}
				totalrow = 0;
			}
		
		function hapusBaris(x) {
                    //     alert(y);                               //alert(x);
				if (document.getElementById(x) != null) {
					 $('#' + x).remove();
					//$('#formbor').bootstrapValidator('resetForm',false);
					
				}
			}

		  var totalrow= 0;
		function addRow() {
			totalrow++;
			var $template = $('#bookTemplate'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow)
					.attr('id', 'rowmat'+totalrow)
					.insertBefore($template);
			$clone
				.find('[name="btnDelRowX"]').attr('onClick','hapusBaris("rowmat' + totalrow + '")').end() 
				.find('[name="id_karyawan"]').attr('name', 'detail[' + totalrow + '][id_karyawan]').attr('id', 'id_karyawan'+totalrow).end()
				.find('[name="nm_karyawan"]').attr('name', 'detail[' + totalrow + '][nm_karyawan]').attr('onClick','cariKaryawan(' + totalrow + ')').attr('id', 'nm_karyawan'+totalrow).end()
				.find('[name="tgl_penisian"]').attr('name', 'detail[' + totalrow + '][tgl_penisian]').attr('id', 'tgl_penisian'+totalrow).end();
			$(".date-picker").datepicker({ autoclose: true});
		//   $('#formbor').bootstrapValidator('resetForm',false);

		}
		
		// tambahan
		
		

	function operateDetail(index, row,element){
		$.ajax({
				  type: "POST",
				  url: "pengisiborang/getEditdetail?id="+row['id'],
				  dataType:"JSON",
				  success: function(result) { 

				  	var x1 = "no_baris_"+index;
				  	var el = "tr[data-index='"+index+"']"
					$(el).next('tr.detail-view').addClass(x1);

					var id,id_karyawan,nm_karyawan,tgl_penisian;
					
					
					var tbl_det;
					tbl_det = '<td colspan="11" align="center"><table border="1" width="100%" style="border-style: solid;border-color:#9F9492;">';
					tbl_det += '<tr style="border-color:#9F9492;">';
					tbl_det += '<td align="center" width="30%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;"> Nama Karyawan</td>';
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Tanggal Pengisian </td>';
					tbl_det += '</tr>';
        			//tbl_det += '<tr style="border-color:#9F9492;">';

        			$.each(eval(result), function (key,value) {
						
        				nm_karyawan = value['nm_karyawan'];
        				tgl_penisian = value['tgl_penisian'];
					//	alert(lampiran); 
						tbl_det += '<tr>';
						tbl_det += ' <td style="border-color:#9F9492;" id="kegiatan">&nbsp;&nbsp;'+nm_karyawan+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp;'+tgl_penisian	+'</td>';
        				tbl_det += '</tr>';

        			});
        			tbl_det += '</table></td>';

					$(".detail-view."+x1).html(tbl_det);

				  }


		});

    }
	
	function cariKaryawan(x){
			document.getElementById('baris').value=x;
			$('#modalTable').modal('show'); 
			$("#modalTable").css({"z-index":"1060"});
			$('html,body').scrollTop(0);
		}
	function operateFormatterPilih(value, row, index) {
       return [
            '<a class="btn btn-sm btn-primary btn-xs" id="pilih" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Pilih" >',
            'Pilih',
            '</a> '
        ].join('');
    }
	
	
  window.operateEventspilih = {
        'click #pilih': function (e, value, row, index) {
            cariDatapopup(row);
        }
    };
	
	function cariDatapopup(row){


		var baris=document.getElementById('baris').value;
		//alert(baris);
	//	 alert(row.nm_grup_tiga);
		 $("#nm_karyawan"+baris).val(row.nm_karyawan);
		 $("#id_karyawan"+baris).val(row.id);
		 
		
		
		$('#formbor').bootstrapValidator('resetForm',false);
		 tutupFormpopup();
	 }
	 
	 function tutupFormpopup(){
      	$('#modalTable').modal('hide'); // show bootstrap modal
		bukaModal();
	}	
	
	function bukaModal(){
		
		$("#modal_formbor").css({"overflow-y":"scroll"});
	
	}
	</script>
 
  