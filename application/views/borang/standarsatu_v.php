<script type="text/javascript" src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
	<style>
.hiddenfilez{
	 width: 0px;

 height: 0px;

 overflow: hidden;
}

</style>

			<div class="col-md-10" style="background-color: #ecf0f5">
					<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					echo aksesTambahborang();
					?>
					 <?php
					echo aksesHapus();
					?>
                    </div><table id="table" 
					data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
						   data-filter-control="true"
                           data-pagination="true"
						    data-query-params="getParamstandar"
                           data-url="standarsatu/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-detail-view="true"
						   data-detail-formatter="operateDetail"
						   data-sort-name="id"
						   data-sort-order="desc">
                        <thead>	
						<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th> 
							<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatterborang" data-events="operateEventsborang">Action</th>
							<th data-field="nm_tahun_akademik"  data-halign="center" data-align="left"  data-sortable="true" >Tahun Akademik  </th>
							<th data-field="nm_jenjangprodi"  data-halign="center" data-align="left"  data-sortable="true" >Jenjang Prodi  </th>
							<th data-field="nm_prodi"  data-halign="center" data-align="left"  data-sortable="true" >Nama Prodi  </th>
                             <th data-field="uraian"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="htmlMutu" >Uraian</th>
							 <th data-field="nilai"  data-halign="center" data-align="left"  data-sortable="true" >Nilai </th>
                        </tr>
						</thead>
                    </table>
			</div>
		
		
		</div>
 </section>

</div>




<div class="modal fade" id="modal_formbor" role="dialog">
   <div class="modal-dialog" style="width:90%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formbor" name="formbor" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
        <input type="hidden" value="" name="id_parameter" id="id_parameter"/> 
		<input type="hidden" value="" name="id_jenjangprodi" id="id_jenjangprodi"/> 
		<div class="row">
			<div class="col-md-12">
				<div class="form-group " id="jab">
					<label class="control-label col-md-2" for1="menudes">Tahun Akademik  </label> 
					<div class="col-md-3">
						<select type="select" name="id_tahunakademik" class="form-control select2 input-sm" id="id_tahunakademik" required="required"  style="width: 100%;" >
							 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
						</select>
					</div>
					 
									
				</div>
				
				<div class="form-group " id="jab">
					<label class="control-label col-md-2" for1="menudes">Prodi  </label> 
					<div class="col-md-3">
						<select type="select" name="id_prodi" class="form-control select2 input-sm" id="id_prodi" required="required"  style="width: 100%;" >
							 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
						</select>
					</div>
				</div>
				
				<div class="form-group ">
					<label class="control-label col-md-2" for1="menudes">Tanggal  Pengisian</label> 
					<div class="col-md-3">
						<input type="text" class="form-control date-picker input-sm" id="tanggal" name="tanggal"  placeholder="yyyy-mm-dd" style="width: 100%;" >
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-2" for1="menudes">Uraian</label> 
					<div class="col-md-10">
						<textarea name="uraian" id="uraian" class="form-control" style="height:600px;"></textarea>
						
						
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-2" for1="menudes">Nilai</label> 
					<div class="col-md-3">
						<input name="nilai" class="form-control input-sm nomor2" id="nilai"  type="text">
					</div>
				</div>
				
				
			</div>
			
			
		</div>
		
		
		  
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> 
	
	<div class="modal fade" id="modal_formsit" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formsit" name="formsit" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id_borang" id="id_borang"/> 
		  <input type="hidden" value="" name="setsit" id="setsit"/> 
          <input type="hidden" value="" name="id_keb" id="id_keb"/> 
		  
		  <div class="form-body">
		  
		  <div class="form-group field-username">
			  <label class="control-label col-md-3" for1="username">Kebijakan  </label>
			  <div class="col-md-9">
			  <input  name="kebijakan" class="form-control text-input input-sm" id="kebijakandata" required="required" type="text">
			  </div>
		  </div>
		  <div class="form-group field-username">
			  <label class="control-label col-md-3" for1="username">Prosedur  </label>
			  <div class="col-md-9">
			  <input subtype="text" name="prosedur" class="form-control text-input input-sm" id="prosedur" required="required" type="text">
			  </div>
		  </div>
		  
		 <div class="form-body">
			  <div class="form-group field-username">
				  <label class="control-label col-md-3" for1="username">Lampiran   </label> 
				  <div class="col-md-9">
					
					<a id="test" onclick="takenFile()"> 
					<embed src="" width="200px"  value="Ambil Lampiran" height="200px" id="pic" >Upload</embed>
					
					</a>
					<div class="hiddenfilez"><input name="imgdata"  id="imgdata"  onchange="loadFile(event)" type="file"></div>
					
				  </div>
			  </div>
          </div>
		
			
          </div>
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSavesit" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" id="btlset" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
  <script src="<?php echo base_url();?>js/atribut.js"></script>