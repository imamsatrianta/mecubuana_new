
	<script>
var parameter="<?php echo $_GET['parameter'];?>";
function loadDataborang(number, size,order){
			var $table = $('#table');
            var offset=(number - 1) * size;
            var limit=size;
			
            $.ajax({
                    type: "POST",
                    url: "standarsatu/loaddataTabel?order="+order+"&limit="+limit+"&offset="+offset+"&parameter="+parameter,
                    dataType:"JSON",
                    success: function(result){
    				 $table.bootstrapTable('load', result);
    			
                    }
                });
        }
		
function getParamstandar(params) {
		    return {
		        parameter: parameter,
				limit: params.limit,
				offset: params.offset,
				search: params.search,
				sort: params.sort,
				order: params.order
		    };
		}
function strip_html_tags(str)
{
   if ((str===null) || (str===''))
       return false;
  else
   str = str.toString();
  return str.replace(/<[^>]*>/g, '');
}


function htmlMutu(value, row, index){
	var mutu=row.uraian;
	return strip_html_tags(mutu);
}

	 function operateFormatterborang(value, row, index) {
       return [
			'<a class="btn btn-sm btn-primary btn-xs" id="kebijakan" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Kebijakan" ><i id="kebij" class="glyphicon glyphicon-plus-sign" ></i></a> ',
			'<a class="btn btn-sm btn-primary btn-xs" id="editborang" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Ubah" ><i id="edt" class="glyphicon glyphicon-edit" ></i></a> ',
			'<a class="btn btn-sm btn-danger btn-xs" id="hapusborang" href="javascript:void(0)" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a> '
			
        ].join('');
	}
	
	window.operateEventsborang = {
        'click #kebijakan': function (e, value, row, index) {
				formKebijakan(row);
        },'click #editborang': function (e, value, row, index) {
				editFormborang(row);
        },'click #hapusborang': function (e, value, row, index) {
				hapusBorang(row.id);
        }
    };
	
	
	  $(document).ready(function ($) {
		  readyToStart();
		getTahunakademik();
		CKEDITOR.replace('uraian');
		$(".date-picker").datepicker({ autoclose: true});
	  });
	  
	 
	function hapusBorang(id){
		bootbox.confirm("Yakin Data akan di Hapus?", function(result) {
			if(result==true){
				 $.ajax({
                    url: 'standarsatu/hapusData?id='+id,
                    type: 'POST',
                    success: function (data) {
					//alert(data);
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
					
						
						loadDataborang(1, 15,'desc');
                        
                    }
                })
			}
               
        });
	}
	function tambahDataborang(){
     $('#formbor')[0].reset(); 
	 $('#formbor').bootstrapValidator('resetForm', true); 
      $('#modal_formbor').modal('show'); 
      $('.modal-title').text('Form Tambah Borang Standar 1 <?php echo $_GET['judul'];?>'); 
	  document.getElementById('set').value=0;
	  document.getElementById('btnSave').disabled=false;
	 $("#id_parameter").val("<?php echo $_GET['parameter'];?>");
	 $("#id_jenjangprodi").val("<?php echo $_GET['level'];?>");	
	 var jenjang="<?php echo $_GET['level'];?>";
	 getProdi(jenjang,0);
	 CKEDITOR.instances['uraian'].setData("" );
	 	
    }
	
	function editFormborang(row){
        $('#formbor')[0].reset(); 
      $('#modal_formbor').modal('show'); 
      $('.modal-title').text('Form Ubah Borang Standar 1 <?php echo $_GET['judul'];?>'); 
	  document.getElementById('set').value=1;
	  document.getElementById('btnSave').disabled=false;
	  
		 var buttonedit=document.getElementById('buttonedit').value;
		 
	    for (var name in row) {
		//alert(name);
			 $('#modal_formbor').find('input[name="' + name + '"]').val(row[name]);
        }
		$("#id_tahunakademik").val(row.id_tahunakademik);
		
		
		
		getProdi(row.id_jenjangprodi,row.id_prodi);
		CKEDITOR.instances['uraian'].setData(row['uraian'] );
		$('#formbor').bootstrapValidator('resetForm',false);
    }
	
	function getTahunakademik(){
		$("#id_tahunakademik").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getTahunakademik",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_tahunakademik").append('<option value="'+val.id+'">'+val.nm_tahun_akademik+'</option>');
					
				});
			
								  
				}
				});
	  
	}
	
	function tutupFormborang(){
		 $('#formbor')[0].reset(); // reset form on modals 
      	$('#modal_formbor').modal('hide'); // show bootstrap modal
	}
	
	function getProdi(jenjang,id_prodi){
		$("#id_prodi").html('');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getProdijenjang/"+jenjang,
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_prodi").append('<option value="'+val.id+'">'+val.nm_prodi+'</option>');
					
				});
					if(id_prodi!='0'){
						$("#id_prodi").val(id_prodi);
					}
					
				}
				});	
	}
	
	$('#formbor')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDataborang();
		 e.preventDefault();
		 
		 
    });
	
	function simpanDataborang(){
	var edituraian = CKEDITOR.instances.uraian;
	var uraianok=edituraian.getData();
	var uraian = uraianok.replace("&nbsp;", "");
 
	 document.getElementById("btnSave").disabled=true;
	 var fd = new FormData(document.getElementById("formbor"));
			fd.append("uraiandata",uraian);
			$.ajax({
				 url: "standarsatu/simpanData",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						//  keluarLogin();
					}
					
					tutupFormborang();
					loadDataborang(1, 15,'desc');
			});
			return false;	
			
	 
	}
	
	function formKebijakan(row){
	 $('#formsit')[0].reset(); 
	 $('#formsit').bootstrapValidator('resetForm', true);
      $('#modal_formsit').modal('show'); 
      $('.modal-title').text('Form Tambah Menu Kebijakan'); 
	  
	  $('#id_borang').val(row.id)
	  document.getElementById('setsit').value=0;
	  document.getElementById('btnSavesit').disabled=false;
	  var image = document.getElementById("pic");
	  image.src = ''; 
	}
	
	function editFormkebijakan(id,kebijakan,prosedur,lampiran,id_borangsatu){
		//alert(id_borangsatu)
		
		$('#modal_formsit').modal('show'); 
      $('.modal-title').text('Form Ubah Menu Kebijakan'); 
	  document.getElementById('btnSavesit').disabled=false;
	    $('#id_borang').val(id_borangsatu);
		document.getElementById('setsit').value=1;
	   $('#kebijakandata').val(kebijakan);
	   $('#prosedur').val(prosedur);
		//$("#lampiran").val(lampiran);
		$("#id_keb").val(id);
		 var image = document.getElementById("pic");
		 
		 var isigbr="<?php echo base_url();?>kebijakanborang/"+lampiran; 
		// alert(isigbr);
			if (document.getElementById("pic") != ''){
				image.src = isigbr;   
			}else{
				image.src = '';    
			}
	}
	
	function tutupKebijakan(){
		 $('#formsit')[0].reset(); // reset form on modals 
      	$('#modal_formsit').modal('hide'); // show bootstrap modal
	}
	
	
	function takenFile(){     
  //	alert('x');
	 $('#imgdata').trigger('click');
	}
  
   var loadFile = function(event) {	
    var output = document.getElementById('pic');
	//alert(output);
    output.src = URL.createObjectURL(event.target.files[0]);

  };
  
	
	$('#formsit')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDatakebijakan();
		 e.preventDefault();
    });
	
  
  function simpanDatakebijakan(){
	 document.getElementById("btnSavesit").disabled=true;
	 var gambar = document.getElementById('imgdata').value; 
	if(gambar==""||gambar==null){
		var setgbr=0;
	}else{
		var setgbr=1;
	}
	 
	 var fd = new FormData(document.getElementById("formsit"));
			fd.append("setgbr",setgbr);
			$.ajax({
			  url: "<?php echo base_url();?>borang/standarsatu/simpanDatakebijakan",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  
			  contentType: false   
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						  keluarLogin();
					}
					
					 tutupKebijakan();
					 loadDataborang(1, 15,'desc');
			});
			return false;		
			
			
 	 
 }	
	
	function operateDetail(index, row,element){
		$.ajax({
				  type: "POST",
				  url: "standarsatu/loaddatakebijakan?id="+row['id']+"&index="+index,
				  dataType:"JSON",
				  success: function(result) { 

				  	var x1 = "no_baris_"+index;
				  	var el = "tr[data-index='"+index+"']"
					$(el).next('tr.detail-view').addClass(x1);

					var id,kebijakan,prosedur,lampiran,id_borangsatu;
					
					
					var tbl_det;
					tbl_det = '<td colspan="8" align="center"><table border="1" width="100%" style="border-style: solid;border-color:#9F9492;">';
					tbl_det += '<tr style="border-color:#9F9492;">';
					tbl_det += '<td align="center" width="5%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Action</td>';
					tbl_det += '<td align="center" width="30%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;"> Kebijakan</td>';
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Prosedur </td>';
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Lampiran</td>'
					tbl_det += '</tr>';
        			//tbl_det += '<tr style="border-color:#9F9492;">';

        			$.each(eval(result), function (key,value) {
						
        				id = value['id'];
        				kebijakan = value['kebijakan'];
        				prosedur = value['prosedur'];
        				lampiran= value['lampiran'];
						id_borangsatu= value['id_borangsatu'];
					//	alert(lampiran); 
						tbl_det += '<tr><td style="border-color:#9F9492;">&nbsp;&nbsp;<a class="btn btn-sm btn-primary btn-xs" onclick="editFormkebijakan(\''+id+'\',\''+kebijakan+'\',\''+prosedur+'\',\''+lampiran+'\',\''+id_borangsatu+'\')" id="editdata" class="btn btn-sm btn-primary"  href="#" title="Ubah Kebijakan" ><i id="edtkeg" class="glyphicon glyphicon-edit" ></i></a>';
						tbl_det += '&nbsp;&nbsp;<a class="btn btn-sm btn-danger btn-xs" onclick="hapusKebijakan(\''+id+'\')" id="hapusKebijakan" class="btn btn-sm btn-primary"  href="#" title="Hapus Kegiatan" ><i id="edtkeg" class="glyphicon glyphicon-trash" ></i></a> </td>';
        				tbl_det += '&nbsp;&nbsp; <td style="border-color:#9F9492;" id="kegiatan">'+kebijakan+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;'+prosedur	+'</td>';
						tbl_det += '<td style="border-color:#9F9492;" ><a href="<?php echo base_url(); ?>kebijakanborang/'+lampiran+'" > &nbsp;'+lampiran+'</a></td>';
        				tbl_det += '</tr>';

        			});
        			tbl_det += '</table></td>';

					$(".detail-view."+x1).html(tbl_det);

				  }


		});

    }
	
		
	
		function hapusKebijakan(id){
		bootbox.confirm("Yakin Data akan di Hapus?", function(result) {
			if(result==true){
				 $.ajax({
                    url: 'standarsatu/hapusKebijakan?id='+id,
                    type: 'POST',
                    success: function (data) {
					//alert(data);
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
					
						
						loadDataborang(1, 15,'desc');
                        
                    }
                })
			}
               
        });
	}
  
	</script>
 
  