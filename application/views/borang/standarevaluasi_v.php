<script type="text/javascript" src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/ckfinder/ckfinder.js"></script>
	<style>
.hiddenfilez{
	 width: 0px;

 height: 0px;

 overflow: hidden;
}

</style>
<?php   
$url=$_SERVER['PHP_SELF'];
$nameurl =pathinfo($url, PATHINFO_FILENAME);

if($nameurl=="standarsatu"){
	$namastandar="Standar Satu";
}else if($nameurl=="standardua"){
	$namastandar="Standar Dua";
}else if($nameurl=="standartiga"){
	$namastandar="Standar Tiga";
}else if($nameurl=="standarempat"){
	$namastandar="Standar Empat";
}else if($nameurl=="standarlima"){
	$namastandar="Standar Lima";
}else if($nameurl=="standarenam"){
	$namastandar="Standar Enam";
}else{
	$namastandar="Standar Tujuh";
}
?>
			<div class="col-md-10" style="background-color: #fff;height:auto;min-height:600px;margin-bottom:-15px;margin-top:-15px;">
				<section class="content-header">
				<h1>
				<div class="caption">
					<i class="fa fa-plus-square-o font-blue-chambray"></i>
					<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
					Borang 3A  <?php echo $namastandar;?>
					
					</span>
				</div>
				</h1>
				 
				</section>
		
					<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
						
                    </div>
					<table id="table" 
					data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
						   data-filter-control="true"
                           data-pagination="true"
						    data-query-params="getParamstandar"
                           data-url="standarevaluasi/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="desc">
                        <thead>	
						<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th> 
						
							<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatterborangevl" data-events="operateEventsborangevl">Action</th>
							<th data-field="nm_tahun_akademik"  data-halign="center" data-align="left"  data-sortable="true" >Tahun Akademik  </th>
							<th data-field="nm_jenjangprodi"  data-halign="center" data-align="left"  data-sortable="true" >Jenjang Prodi  </th>
							<th data-field="nm_prodi"  data-halign="center" data-align="left"  data-sortable="true" >Nama Prodi  </th>
							
							 <th data-field="status_evaluasi"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="statusEvaluasi">Status Data </th>
							 <th data-field="note_evaluasi"  data-halign="center" data-align="left"  data-sortable="true" >Keterangan Evaluasi </th>
							 <th data-field="dodate"  data-halign="center" data-align="left"  data-sortable="true" >Tanggal Due Date </th>
							 
                        </tr>
						</thead>
                    </table>
			</div>
		
		
		</div>
 </section>

</div>



	
<div class="modal fade" id="modal_formevl" role="dialog">
  <div class="modal-dialog" style="width:60%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formevl" name="formevl" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id_tahunakademikevl" id="id_tahunakademikevl"/> 
		  <input type="hidden" value="" name="id_jenjangprodievl" id="id_jenjangprodievl"/> 
		 <input type="hidden" value="" name="id_prodievl" id="id_prodievl"/> 
		<input type="hidden" value="" name="standar" id="standar"/> 
		
		 <div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Kirim Kepada  </label> 
			<div class="col-md-5">
				<select type="select" name="status_evaluasi" class="form-control text-input input-sm" id="status_evaluasi"  style="width: 100%;" onchange="cekEvaluasi()" >
					  <option value="">--Pilih--</option>
					  <option value="0">Prodi</option>
					  <option value="1">Dekan/Wakil Dekan</option>
					  <option value="2">PAEE</option>
				</select>
				
		    </div>
		</div>
		<div class="col-md-12" id="cekkirim">
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes"> Tanggal  Due Data</label> 
			<div class="col-md-7"> 
				<input type="text" class="form-control date-picker input-sm" id="tgl_dodate" name="tgl_dodate"  placeholder="yyyy-mm-dd" style="width: 100%;" >
		    </div>
			
		</div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Keterangan  </label> 
			<div class="col-md-9">
				<textarea class="form-control" rows="5" id="not_evaluasi" name="not_evaluasi"></textarea>
		    </div>
		</div>
		
		
		
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSaveevl" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
	
	
  <script src="<?php echo base_url();?>js/atribut.js"></script>