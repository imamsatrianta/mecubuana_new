
	<script>
var parameter="<?php echo $_GET['parameter'];?>";
var level="<?php echo $_GET['level'];?>";
var levelprodi="<?php echo $this->session->userdata('levelprodi');?>";
function loadDataborang(number, size,order){
			var $table = $('#table');
            var offset=(number - 1) * size;
            var limit=size;
			
            $.ajax({
                    type: "POST",
                    url: "empat_tiga_satu/loaddataTabel?order="+order+"&limit="+limit+"&offset="+offset+"&parameter="+parameter+"&level="+level,
                    dataType:"JSON",
                    success: function(result){
    				 $table.bootstrapTable('load', result);
    			
                    }
                });
        }
		
function getParamstandar(params) {
		    return {
				level: level,
		        parameter: parameter,
				limit: params.limit,
				offset: params.offset,
				search: params.search,
				sort: params.sort,
				order: params.order
		    };
		}
function strip_html_tags(str)
{
   if ((str===null) || (str===''))
       return false;
  else
   str = str.toString();
  return str.replace(/<[^>]*>/g, '');
}


function htmlMutu(value, row, index){
	var mutu=row.uraian;
	return strip_html_tags(mutu);
}

	 
	
	
	  $(document).ready(function ($) {
		   $(".close").click(function(){
			tutupFormborang();		
			});
		  $('#modal_formbor').modal({backdrop: 'static',keyboard: false,show: false });
		getTahunakademik();
		$(".date-picker").datepicker({ autoclose: true});
	  });
	  
	 
	function hapusBorang(id){
		bootbox.confirm("Yakin Data akan di Hapus?", function(result) {
			if(result==true){
				 $.ajax({
                    url: 'empat_tiga_satu/hapusData?id='+id,
                    type: 'POST',
                    success: function (data) {
					//alert(data);
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
					
						
						loadDataborang(1, 15,'desc');
                        
                    }
                })
			}
               
        });
	}
	function tambahDataborang(){
     $('#formbor')[0].reset(); 
	 $('#formbor').bootstrapValidator('resetForm', true); 
      $('#modal_formbor').modal('show'); 
      $('.modal-title').text('Form Tambah Borang Standar 4 (4.3.1)'); 
	  document.getElementById('set').value=0;
	  document.getElementById('btnSave').disabled=false;
	 $("#id_parameter").val("<?php echo $_GET['parameter'];?>");
	 $("#id_jenjangprodi").val("<?php echo $_GET['level'];?>");	
	 var jenjang="<?php echo $_GET['level'];?>";
	 getProdi(jenjang,0,0);
	 $("#cke_1_contents").css({"height":"400px"});
	 
    }
	
	function editFormborang(row){
        $('#formbor')[0].reset(); 
      $('#modal_formbor').modal('show'); 
      $('.modal-title').text('Form Ubah Borang Standar 4 (4.3.1)'); 
	  document.getElementById('set').value=1;
	  document.getElementById('btnSave').disabled=false;
	  
		 var buttonedit=document.getElementById('buttonedit').value;
		 
	    for (var name in row) {
		//alert(name);
			 $('#modal_formbor').find('input[name="' + name + '"]').val(row[name]);
        }
		$("#id_tahunakademik").val(row.id_tahunakademik);
		
		
		//alert(row.kd_server)
		getProdi(row.id_jenjangprodi,row.id_prodi,row.kd_server);
		$("#cke_1_contents").css({"height":"400px"});
		$('#formbor').bootstrapValidator('resetForm',false);
		getEditdetail(row.id);
		
		
    }
	
	function getTahunakademik(){
		$("#id_tahunakademik").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getTahunakademik",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_tahunakademik").append('<option value="'+val.id+'">'+val.nm_tahun_akademik+'</option>');
					
				});
			
								  
				}
				});
	  
	}
	
	function tutupFormborang(){
		 $('#formbor')[0].reset(); // reset form on modals 
      	$('#modal_formbor').modal('hide'); // show bootstrap modal
	}
	
	function getProdi(jenjang,id_prodi,kd_server){
		$("#id_prodi").html('');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getProdijenjang/"+jenjang,
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_prodi").append('<option value="'+val.id+'-'+val.kd_server+'">'+val.nm_prodi+'</option>');
					
				});
					if(id_prodi!='0'){
						$("#id_prodi").val(id_prodi+'-'+kd_server);
					}
					
				}
				});	
	}
	
	$('#formbor')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDataborang();
		 e.preventDefault();
		 
		 
    });
	
	function simpanDataborang(){
	
	 document.getElementById("btnSave").disabled=true;
	 var fd = new FormData(document.getElementById("formbor"));
			fd.append("uraiandata",'');
			$.ajax({
				 url: "empat_tiga_satu/simpanData",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						//  keluarLogin();
					}
					
					tutupFormborang();
					loadDataborang(1, 15,'desc');
			});
			return false;	
			
	 
	}
	
	
	
	
	
	function getEditdetail(id){
		var sd="id="+id;
				var i=0;
		 		$.ajax({
                    type: "GET",
					  url: '<?php echo base_url();?>d3/empat_tiga_satu/getEditdetail',
                    data: sd,
                    dataType:"json",
                    success: function(result){
					var x=0;
					$.each(result, function(key, val) {
					x++; 
							$('#jml_lulusan_diwisuda'+x).val(val.jml_lulusan_diwisuda);
							$('#nm_lembaga'+x).val(val.nm_lembaga);
							$('#jml_lulusan_pesanterima'+x).val(val.jml_lulusan_pesanterima);
							$('#jumlah_lulusanditerima'+x).val(val.jumlah_lulusanditerima);
					});	
			}
			});
	}
	
	function getData(){
		var id_jenjangprodi=$("#id_jenjangprodi").val();
		var id_tahunakademik=$( "#id_tahunakademik option:selected" ).text();  
		var id_prodi=$("#id_prodi").val();
	//	alert(id_tahunakademik);
		var sd="id_prodi="+id_prodi+"&id_tahunakademik="+id_tahunakademik;
				var i=0;
		 		$.ajax({
                    type: "GET",
					  url: '<?php echo base_url();?>d3/empat_tiga_satu/getDataserver',
                    data: sd,
                    dataType:"json",
                    success: function(result){
					var x=0;
					$.each(result, function(key, val) {
					x++; 
							$('#jml_lulusan_diwisuda'+x).val(val.jml_lulusan_diwisuda);
							$('#nm_lembaga'+x).val(val.nm_lembaga); 
							$('#jml_lulusan_pesanterima'+x).val(val.jml_lulusan_pesanterima);
							$('#jumlah_lulusanditerima'+x).val(val.jumlah_lulusanditerima);
					});	
			}
			});
	}
	
	// tambahan refisi
	function refisiForm(row){
		 $('#formref')[0].reset(); 
		 $('#formref').bootstrapValidator('resetForm', true);
		  $('#modal_formref').modal('show'); 
		  $('.modal-title').text('Form Evaluasi Borang'); 
		  document.getElementById('btnSaveref').disabled=false;
		   $('#id_ref').val(row.id); 
		   $('#id_prodi_ref').val(row.id_prodi); 
	}
	
	$('#formref')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDatarefisi();
		 e.preventDefault();
		 
		 
    });
	
	
	function simpanDatarefisi(){
	
	 document.getElementById("btnSaveref").disabled=true;
	 var fd = new FormData(document.getElementById("formref"));
			fd.append("kegiatanrel",'');
			$.ajax({
				 url: "empat_tiga_satu/simpanDatarefisi",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						  keluarLogin();
					}
					
					tutupFormref();
					loadDataborang(1, 15,'desc');
			});
			return false;	
			
	 
 }
 function tutupFormref(){
	 $('#formref')[0].reset(); // reset form on modals 
      	$('#modal_formref').modal('hide'); // show bootstrap modal
 }

 function statusBorang(value, row, index){
	if(row.status=="3"){
		return "Dikembalikan";
	}else if(row.status=="2"){
		return "Disetujui";
	}else if(row.status=="1"){
		return "Diajukan";
	}else{
		return "Baru";
	}
}

function statusEvaluasi(value, row, index){
	if(row.status_evaluasi=="2"){
		return "PAEE";
	}else if(row.status_evaluasi=="1"){
		return "Wakil Dekan/Dekan";
	}else{
		return "Prodi";
	}
}


// tambahan

function operateFormatterborang(value, row, index) {
		 if(levelprodi!="2"){
			 return [
			'<a class="btn btn-sm btn-primary btn-xs" id="refisi" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Evaluasi" ><i id="edt" class="glyphicon glyphicon-pencil" ></i></a> ',
			'<a class="btn btn-sm btn-primary btn-xs" id="kebijakan" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Kebijakan" ><i id="kebij" class="glyphicon glyphicon-plus-sign" ></i></a> ',
			'<a class="btn btn-sm btn-primary btn-xs" id="editborang" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Ubah" ><i id="edt" class="glyphicon glyphicon-edit" ></i></a> ',
			'<a class="btn btn-sm btn-danger btn-xs" id="hapusborang" href="javascript:void(0)" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a> '
			
			].join('');
		 }else{
		   return [
				'<a class="btn btn-sm btn-primary btn-xs" id="editborang" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Ubah" ><i id="edt" class="glyphicon glyphicon-edit" ></i></a> ',
				'<a class="btn btn-sm btn-danger btn-xs" id="hapusborang" href="javascript:void(0)" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a> '
				
			].join('');
		 }
	}
	
	window.operateEventsborang = {
        'click #kebijakan': function (e, value, row, index) {
				formKebijakan(row);
        },'click #editborang': function (e, value, row, index) {
				editFormborang(row);
        },'click #hapusborang': function (e, value, row, index) {
				hapusBorang(row.id);
        },'click #refisi': function (e, value, row, index) {
				refisiForm(row);
        }
    };
	
	function operateDetail(index, row,element){
		$.ajax({
				  type: "POST",
				  url: "empat_tiga_satu/loaddatakebijakan?id="+row['id']+"&index="+index,
				  dataType:"JSON",
				  success: function(result) { 

				  	var x1 = "no_baris_"+index;
				  	var el = "tr[data-index='"+index+"']"
					$(el).next('tr.detail-view').addClass(x1);

					var id,no,judul_lampiran,lampiran,id_borang;
					
					
					var tbl_det;
					tbl_det = '<td colspan="11" align="center"><table border="1" width="100%" style="border-style: solid;border-color:#9F9492;">';
					tbl_det += '<tr style="border-color:#9F9492;">';
					<?php
					$parameter=$_GET['parameter'];
					if($parameter<>0){
					?>
					tbl_det += '<td align="center" width="5%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Action</td>';
					<?php } ?>
					tbl_det += '<td align="center" width="30%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;"> No</td>';
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Judul Lampiran </td>';
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Lampiran</td>'
					tbl_det += '</tr>';
        			//tbl_det += '<tr style="border-color:#9F9492;">';

        			$.each(eval(result), function (key,value) {
						
        				id = value['id'];
        				no = value['no'];
        				judul_lampiran = value['judul_lampiran'];
        				lampiran= value['lampiran'];
						id_borang= value['id_borang'];
					//	alert(judul_lampiran); 
						tbl_det += '<tr>';
						<?php
						$parameter=$_GET['parameter'];
						if($parameter<>0){
						?>
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp;<a class="btn btn-sm btn-primary btn-xs" onclick="editFormkebijakan(\''+id+'\',\''+no+'\',\''+judul_lampiran+'\',\''+lampiran+'\',\''+id_borang+'\')" id="editdata" class="btn btn-sm btn-primary"  href="#" title="Ubah Kebijakan" ><i id="edtkeg" class="glyphicon glyphicon-edit" ></i></a>';
						tbl_det += '&nbsp;&nbsp;<a class="btn btn-sm btn-danger btn-xs" onclick="hapusKebijakan(\''+id+'\')" id="hapusKebijakan" class="btn btn-sm btn-primary"  href="#" title="Hapus Kegiatan" ><i id="edtkeg" class="glyphicon glyphicon-trash" ></i></a> </td>';
        				<?php } ?>
						tbl_det += '<td style="border-color:#9F9492;" id="kegiatan">&nbsp;&nbsp; '+no+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp; &nbsp;'+judul_lampiran	+'</td>';
						tbl_det += '<td style="border-color:#9F9492;" ><a href="<?php echo base_url(); ?>kebijakanborang/'+lampiran+'" > &nbsp;&nbsp;'+lampiran+'</a></td>';
        				tbl_det += '</tr>';

        			});
        			tbl_det += '</table></td>';

					$(".detail-view."+x1).html(tbl_det);

				  }


		});

    }
	
	function hapusKebijakan(id){
		bootbox.confirm("Yakin Data akan di Hapus?", function(result) {
			if(result==true){
				 $.ajax({
                    url: 'empat_tiga_satu/hapusKebijakan?id='+id,
                    type: 'POST',
                    success: function (data) {
					//alert(data);
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
					
						
						loadDataborang(1, 15,'desc');
                        
                    }
                })
			}
               
        });
	}
	
	function formKebijakan(row){
	 $('#formsit')[0].reset(); 
	 $('#formsit').bootstrapValidator('resetForm', true);
      $('#modal_formsit').modal('show'); 
      $('.modal-title').text('Form Tambah Menu Kebijakan'); 
	  
	  $('#id_borang').val(row.id)
	  document.getElementById('setsit').value=0;
	  document.getElementById('btnSavesit').disabled=false;
	  var image = document.getElementById("pic");
	  image.src = ''; 
	}
	
	function editFormkebijakan(id,no,judul_lampiran,lampiran,id_borang){
		//alert(judul_lampiran)
		
		$('#modal_formsit').modal('show'); 
      $('.modal-title').text('Form Ubah Menu Kebijakan'); 
	  document.getElementById('btnSavesit').disabled=false;
	    $('#id_borang').val(id_borang);
		document.getElementById('setsit').value=1;
	   $('#no').val(no);
	   $('#judul_lampiran').val(judul_lampiran);
		//$("#lampiran").val(lampiran);
		$("#id_keb").val(id);
		 var image = document.getElementById("pic");
		 
		 var isigbr="<?php echo base_url();?>kebijakanborang/"+lampiran; 
		// alert(isigbr);
			if (document.getElementById("pic") != ''){
				image.src = isigbr;   
			}else{
				image.src = '';    
			}
	}
	
	function tutupKebijakan(){
		 $('#formsit')[0].reset(); // reset form on modals 
      	$('#modal_formsit').modal('hide'); // show bootstrap modal
	}
	
	
	function takenFile(){     
  //	alert('x');
	 $('#imgdata').trigger('click');
	}
  
   var loadFile = function(event) {	
    var output = document.getElementById('pic');
	//alert(output);
    output.src = URL.createObjectURL(event.target.files[0]);

  };
  
	
	$('#formsit')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDatakebijakan();
		 e.preventDefault();
    });
	
  
  function simpanDatakebijakan(){
	 document.getElementById("btnSavesit").disabled=true;
	 var gambar = document.getElementById('imgdata').value; 
	if(gambar==""||gambar==null){
		var setgbr=0;
	}else{
		var setgbr=1;
	}
	 
	 var fd = new FormData(document.getElementById("formsit"));
			fd.append("setgbr",setgbr);
			$.ajax({
			  url: "<?php echo base_url();?>d3/empat_tiga_satu/simpanDatakebijakan",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  
			  contentType: false   
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						  keluarLogin();
					}
					
					 tutupKebijakan();
					 loadDataborang(1, 15,'desc');
			});
			return false;		
    }	
//tambahan imam
    var totalrow= 0;
	function addRow() {
			totalrow++;
			var $template = $('#bookTemplate'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow)
					.attr('id', 'rowmat'+totalrow)
					.insertBefore($template);
			$clone 
				.find('[name="btnDelRowX"]').attr('onClick','hapusBaris("rowmat' + totalrow + '")').end() 
				.find('[name="id_kts_ob"]').attr('name', 'ktsob[' + totalrow + '][id_kts_ob]').attr('id', 'id_kts_ob'+totalrow).end()
				.find('[name="kts_ob"]').attr('name', 'ktsob[' + totalrow + '][kts_ob]').attr('id', 'kts_ob'+totalrow).end()
				.find('[name="id_dok"]').attr('name', 'ktsob[' + totalrow + '][id_dok]').attr('id', 'id_dok'+totalrow).end()
				.find('[name="id_standar"]').attr('name', 'ktsob[' + totalrow + '][id_standar]').attr('id', 'id_standar'+totalrow).end()
				.find('[name="id_pernyataan"]').attr('name', 'ktsob[' + totalrow + '][id_pernyataan]').attr('id', 'id_pernyataan'+totalrow).end()
				.find('[name="pernyataan"]').attr('name', 'ktsob[' + totalrow + '][pernyataan]').attr('id', 'pernyataan'+totalrow).end();
					
		}

	</script>
 
  