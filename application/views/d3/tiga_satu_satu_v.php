
	<style>
.hiddenfilez{
	 width: 0px;

 height: 0px;

 overflow: hidden;
}

</style>

			<div class="col-md-10" style="background-color: #fff;height:auto;min-height:600px;margin-bottom:-15px;margin-top:-15px;">
				<section class="content-header">
				<h1>
				<div class="caption">
					<i class="fa fa-plus-square-o font-blue-chambray"></i>
					<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
					Borang 3A Standar Tiga (3.1.1)
					
					</span>
				</div>
				</h1>
				 
				</section>
		
					<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					$parameter=$_GET['parameter'];
					if($parameter<>0){
						echo aksesTambahborang();
					}
					
					?>
					 <?php
					 $parameter=$_GET['parameter'];
					if($parameter<>0){
						echo aksesHapusborang();
					}
					?>
                    </div><table id="table" 
					data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
						   data-filter-control="true"
                           data-pagination="true"
						    data-query-params="getParamstandar"
                           data-url="tiga_satu_satu/loaddataTabel"
                           data-side-pagination="server"
						   data-detail-view="true"
						   data-detail-formatter="operateDetail"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="desc">
                        <thead>	
						<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th> 
							<?php
							$parameter=$_GET['parameter'];
							if($parameter<>0){
							?>
							<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatterborang" data-events="operateEventsborang">Action</th>
							<?php } ?>
							
							<th data-field="nm_tahun_akademik"  data-halign="center" data-align="left"  data-sortable="true" >Tahun Akademik  </th>
							<th data-field="nm_jenjangprodi"  data-halign="center" data-align="left"  data-sortable="true" >Jenjang Prodi  </th>
							<th data-field="nm_prodi"  data-halign="center" data-align="left"  data-sortable="true" >Nama Prodi  </th>
							<th data-field="status"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="statusBorang">Status Evaluasi </th>
							 <th data-field="status_evaluasi"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="statusEvaluasi">Status Data </th>
							 <th data-field="note_status"  data-halign="center" data-align="left"  data-sortable="true" >Catatan Evaluasi </th>
							 
							 
                        </tr>
						</thead>
                    </table>
			</div>
		
		
		</div>
 </section>

</div>




<div class="modal fade" id="modal_formbor" role="dialog">
   <div class="modal-dialog" style="width:90%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formbor" name="formbor" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
        <input type="hidden" value="" name="id_parameter" id="id_parameter"/> 
		<input type="hidden" value="" name="id_jenjangprodi" id="id_jenjangprodi"/> 
		<div class="row">
			<div class="col-md-6">
				<div class="form-group " id="jab">
					<label class="control-label col-md-4" for1="menudes">Tahun Akademik  </label> 
					<div class="col-md-6">
						<select type="select" name="id_tahunakademik" class="form-control select2 input-sm" id="id_tahunakademik" required="required"  style="width: 100%;" >
							 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
						</select>
					</div>				
				</div>
				<div class="form-group " id="jab">
					<label class="control-label col-md-4" for1="menudes">Prodi  </label> 
					<div class="col-md-8">
						<select type="select" name="id_prodi" class="form-control select2 input-sm" id="id_prodi" required="required"  style="width: 100%;" >
							 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Tanggal  Pengisian</label> 
					<div class="col-md-3">
						<input type="text" class="form-control date-picker input-sm" id="tanggal" name="tanggal"  placeholder="yyyy-mm-dd" style="width: 100%;" >
					</div>
				</div>
				
			</div>
			<div class="col-md-12">
				<div class="tab-pane active" id="tab_1">
						
						<div class="panel panel-default">
							<div class="panel-heading" >
							   <button type="button" id="btnNewRow" name="btnNewRow" class="btn btn-xs btn-success" onclick="getData();"><i class="fa fa-plus"></i>
								Ambil Data SIA
								</button>
							</div>
							
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
										<thead> 
											<tr>
												<td align="center" class="ganjil" rowspan="2">Tahun Akade-mik </td>
												<td align="center" class="ganjil" rowspan="2" >Daya Tampung</td>
												<td align="center" class="ganjil" colspan="2">Jumlah Calon Mahasiswa Reguler  </td>
												<td align="center" class="ganjil" colspan="2">Jumlah Mahasiswa Baru</td>
												<td align="center" class="ganjil" colspan="2">Jumlah Total Mahasiswa</td>
												<td align="center" class="ganjil" colspan="2">Jumlah Lulusan </td>
												<td align="center" class="ganjil" colspan="3">IPK Lulusan Reguler</td>
												<td align="center" class="ganjil" colspan="3">Persentase Lulusan Reguler dengan IPK :</td>
											</tr>
											<tr>
												<td align="center" class="ganjil">Ikut Seleksi</td>
												<td align="center" class="ganjil">Lulus Seleksi</td>
												<td align="center" class="ganjil">Regular bukan Transfer</td>
												<td align="center" class="ganjil">Transfer</td>
												<td align="center" class="ganjil">Reguler bukan Transfer</td>
												<td align="center" class="ganjil">Transfer</td>
												<td align="center" class="ganjil">Reguler bukan Transfer</td>
												<td align="center" class="ganjil">Transfer</td>
												<td align="center" class="ganjil">Min</td>
												<td align="center" class="ganjil">Rata2</td>
												<td align="center" class="ganjil">Mak</td>
												<td align="center" class="ganjil"> < 2,75</td>
												<td align="center" class="ganjil"> 2,75-3,50</td>
												<td align="center" class="ganjil"> > 3,50 </td>
											</tr>
											
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="TS-4"  name='detail[1][tahun_akademik]'  id='tahun_akademik1' readonly >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][daya_tampung]' id='daya_tampung1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][mhs_ikut_seleksi]' id='mhs_ikut_seleksi1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][mhs_lulus_seleksi]' id='mhs_lulus_seleksi1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][jml_reg_bkn_transfer]' id='jml_reg_bkn_transfer1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][jml_transfer]' id='jml_transfer1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][total_reg_bkn_transfer]' id='total_reg_bkn_transfer1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][total_transfer]' id='total_transfer1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][lulus_reg_bkn_transfer]' id='lulus_reg_bkn_transfer1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][lulus_transfer]' id='lulus_transfer1' >	
												</td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][ipk_lulusan_reg_min]' id='ipk_lulusan_reg_min1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][ipk_lulusan_reg_rata]' id='ipk_lulusan_reg_rata1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][ipk_lulusan_reg_mak]' id='ipk_lulusan_reg_mak1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][ipk_kurang]' id='ipk_kurang1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][ipk]' id='ipk1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][ipk_lebih]' id='ipk_lebih1' >	
												</td>
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="TS-3"  name='detail[2][tahun_akademik]'  id='tahun_akademik2' readonly >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][daya_tampung]' id='daya_tampung2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][mhs_ikut_seleksi]' id='mhs_ikut_seleksi2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][mhs_lulus_seleksi]' id='mhs_lulus_seleksi2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][jml_reg_bkn_transfer]' id='jml_reg_bkn_transfer2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][jml_transfer]' id='jml_transfer2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][total_reg_bkn_transfer]' id='total_reg_bkn_transfer2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][total_transfer]' id='total_transfer2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][lulus_reg_bkn_transfer]' id='lulus_reg_bkn_transfer2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][lulus_transfer]' id='lulus_transfer2' >	
												</td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][ipk_lulusan_reg_min]' id='ipk_lulusan_reg_min2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][ipk_lulusan_reg_rata]' id='ipk_lulusan_reg_rata2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][ipk_lulusan_reg_mak]' id='ipk_lulusan_reg_mak2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][ipk_kurang]' id='ipk_kurang2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][ipk]' id='ipk2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][ipk_lebih]' id='ipk_lebih2' >	
												</td>
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="TS-2"  name='detail[3][tahun_akademik]'  id='tahun_akademik3' readonly >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][daya_tampung]' id='daya_tampung3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][mhs_ikut_seleksi]' id='mhs_ikut_seleksi3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][mhs_lulus_seleksi]' id='mhs_lulus_seleksi3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][jml_reg_bkn_transfer]' id='jml_reg_bkn_transfer3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][jml_transfer]' id='jml_transfer3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][total_reg_bkn_transfer]' id='total_reg_bkn_transfer3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][total_transfer]' id='total_transfer3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][lulus_reg_bkn_transfer]' id='lulus_reg_bkn_transfer3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][lulus_transfer]' id='lulus_transfer3' >	
												</td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][ipk_lulusan_reg_min]' id='ipk_lulusan_reg_min3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][ipk_lulusan_reg_rata]' id='ipk_lulusan_reg_rata3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][ipk_lulusan_reg_mak]' id='ipk_lulusan_reg_mak3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][ipk_kurang]' id='ipk_kurang3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][ipk]' id='ipk3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][ipk_lebih]' id='ipk_lebih3' >	
												</td>
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="TS-1"  name='detail[4][tahun_akademik]'  id='tahun_akademik4' readonly >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][daya_tampung]' id='daya_tampung4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][mhs_ikut_seleksi]' id='mhs_ikut_seleksi4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][mhs_lulus_seleksi]' id='mhs_lulus_seleksi4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][jml_reg_bkn_transfer]' id='jml_reg_bkn_transfer4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][jml_transfer]' id='jml_transfer4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][total_reg_bkn_transfer]' id='total_reg_bkn_transfer4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][total_transfer]' id='total_transfer4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][lulus_reg_bkn_transfer]' id='lulus_reg_bkn_transfer4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][lulus_transfer]' id='lulus_transfer4' >	
												</td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][ipk_lulusan_reg_min]' id='ipk_lulusan_reg_min4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][ipk_lulusan_reg_rata]' id='ipk_lulusan_reg_rata4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][ipk_lulusan_reg_mak]' id='ipk_lulusan_reg_mak4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][ipk_kurang]' id='ipk_kurang4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][ipk]' id='ipk4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][ipk_lebih]' id='ipk_lebih4' >	
												</td>
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="TS"  name='detail[5][tahun_akademik]'  id='tahun_akademik5' readonly >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][daya_tampung]' id='daya_tampung5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][mhs_ikut_seleksi]' id='mhs_ikut_seleksi5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][mhs_lulus_seleksi]' id='mhs_lulus_seleksi5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][jml_reg_bkn_transfer]' id='jml_reg_bkn_transfer5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][jml_transfer]' id='jml_transfer5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][total_reg_bkn_transfer]' id='total_reg_bkn_transfer5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][total_transfer]' id='total_transfer5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][lulus_reg_bkn_transfer]' id='lulus_reg_bkn_transfer5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][lulus_transfer]' id='lulus_transfer5' >	
												</td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][ipk_lulusan_reg_min]' id='ipk_lulusan_reg_min5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][ipk_lulusan_reg_rata]' id='ipk_lulusan_reg_rata5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][ipk_lulusan_reg_mak]' id='ipk_lulusan_reg_mak5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][ipk_kurang]' id='ipk_kurang5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][ipk]' id='ipk5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][ipk_lebih]' id='ipk_lebih5' >	
												</td>
											</tr>
									
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
								<!-- /.table-responsive -->
								
							</div>
							<!-- /.panel-body -->
						</div>
					</div>
			
			</div>
			
		</div>
		
		
		  
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> 
	
		<div class="modal fade" id="modal_formref" role="dialog">
  <div class="modal-dialog" style="width:60%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formref" name="formref" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id_ref" id="id_ref"/> 
		  <input type="hidden" value="" name="id_prodi_ref" id="id_prodi_ref"/> 
		  <div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Status Evaluasi  </label> 
			<div class="col-md-5">
				<select type="select" name="status" class="form-control text-input input-sm" id="status"  style="width: 100%;" >
			
					  <option value="">--Pilih--</option>
					  <option value="3">Dikembalikan</option>
					  <option value="2">Disetujui</option>
					  </select>
				
		    </div>
		</div>
		
     
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Keterangan  </label> 
			<div class="col-md-9">
				<textarea class="form-control" rows="5" id="note_status" name="note_status"></textarea>
		    </div>
		</div>
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSaveref" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>

	<!--- kebijakan-->
	
		<div class="modal fade" id="modal_formsit" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formsit" name="formsit" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id_borang" id="id_borang"/> 
		  <input type="hidden" value="" name="setsit" id="setsit"/> 
          <input type="hidden" value="" name="id_keb" id="id_keb"/> 
		  
		  <div class="form-body">
		  
		  <div class="form-group field-username">
			  <label class="control-label col-md-3" for1="username">No  </label>
			  <div class="col-md-2">
			  <input  name="no" class="form-control text-input input-sm" id="no" required="required" type="text">
			  </div>
		  </div>
		  <div class="form-group field-username">
			  <label class="control-label col-md-3" for1="username">Judul Lampiran  </label>
			  <div class="col-md-9">
			  <textarea class="form-control" rows="5" id="judul_lampiran" name="judul_lampiran"></textarea>
			  </div>
		  </div>
		  
		 <div class="form-body">
			  <div class="form-group field-username">
				  <label class="control-label col-md-3" for1="username">Lampiran   </label> 
				  <div class="col-md-9">
					
					<a id="test" onclick="takenFile()"> 
					<embed src="" width="200px"  value="Ambil Lampiran" height="200px" id="pic" >Upload</embed>
					
					</a>
					<div class="hiddenfilez"><input name="imgdata"  id="imgdata"  onchange="loadFile(event)" type="file"></div>
					
				  </div>
			  </div>
          </div>
		
			
          </div>
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSavesit" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" id="btlset" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
  <script src="<?php echo base_url();?>js/atribut.js"></script>