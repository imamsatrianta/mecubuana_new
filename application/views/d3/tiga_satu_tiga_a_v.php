
	<style>
.hiddenfilez{
	 width: 0px;

 height: 0px;

 overflow: hidden;
}

</style>

			<div class="col-md-10" style="background-color: #fff;height:auto;min-height:600px;margin-bottom:-15px;margin-top:-15px;">
				<section class="content-header">
				<h1>
				<div class="caption">
					<i class="fa fa-plus-square-o font-blue-chambray"></i>
					<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
					Borang 3A Standar Tiga (3.1.3)
					
					</span>
				</div>
				</h1>
				 
				</section>
		
					<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					$parameter=$_GET['parameter'];
					if($parameter<>0){
						echo aksesTambahborang();
					}
					
					?>
					 <?php
					 $parameter=$_GET['parameter'];
					if($parameter<>0){
						echo aksesHapusborang();
					}
					?>
                    </div><table id="table" 
					data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
						   data-filter-control="true"
                           data-pagination="true"
						    data-query-params="getParamstandar"
                           data-url="tiga_satu_tiga_a/loaddataTabel"
						   data-detail-view="true"
						   data-detail-formatter="operateDetail"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="desc">
                        <thead>	
						<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th> 
							<?php
							$parameter=$_GET['parameter'];
							if($parameter<>0){
							?>
							<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatterborang" data-events="operateEventsborang">Action</th>
							<?php } ?>
							
							<th data-field="nm_tahun_akademik"  data-halign="center" data-align="left"  data-sortable="true" >Tahun Akademik  </th>
							<th data-field="nm_jenjangprodi"  data-halign="center" data-align="left"  data-sortable="true" >Jenjang Prodi  </th>
							<th data-field="nm_prodi"  data-halign="center" data-align="left"  data-sortable="true" >Nama Prodi  </th>
							 <th data-field="status"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="statusBorang">Status Evaluasi </th>
							 <th data-field="status_evaluasi"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="statusEvaluasi">Status Data </th>
							 <th data-field="note_status"  data-halign="center" data-align="left"  data-sortable="true" >Catatan Evaluasi </th>
							 
                        </tr>
						</thead>
                    </table>
			</div>
		
		
		</div>
 </section>

</div>




<div class="modal fade" id="modal_formbor" role="dialog">
   <div class="modal-dialog" style="width:90%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formbor" name="formbor" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
        <input type="hidden" value="" name="id_parameter" id="id_parameter"/> 
		<input type="hidden" value="" name="id_jenjangprodi" id="id_jenjangprodi"/> 
		<div class="row">
			<div class="col-md-6">
				<div class="form-group " id="jab">
					<label class="control-label col-md-4" for1="menudes">Tahun Akademik  </label> 
					<div class="col-md-6">
						<select type="select" name="id_tahunakademik" class="form-control select2 input-sm" id="id_tahunakademik" required="required"  style="width: 100%;" >
							 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
						</select>
					</div>				
				</div>
				<div class="form-group " id="jab">
					<label class="control-label col-md-4" for1="menudes">Prodi  </label> 
					<div class="col-md-8">
						<select type="select" name="id_prodi" class="form-control select2 input-sm" id="id_prodi" required="required"  style="width: 100%;" >
							 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Tanggal  Pengisian</label> 
					<div class="col-md-3">
						<input type="text" class="form-control date-picker input-sm" id="tanggal" name="tanggal"  placeholder="yyyy-mm-dd" style="width: 100%;" >
					</div>
				</div>
				
			</div>
			<div class="col-md-12">
				<div class="tab-pane active" id="tab_1">
						
						<div class="panel panel-default">
							<div class="panel-heading" >
							   <button type="button" id="btnNewRow" name="btnNewRow" class="btn btn-xs btn-success" onclick="getData();"><i class="fa fa-plus"></i>
								Ambil Data SIA
								</button>
							</div>
							
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
										<thead> 
											<tr>
												<td align="center" class="ganjil" rowspan="2" width="10%">Tahun Masuk </td>
												<td align="center" class="ganjil" colspan="7" width="75%">Jumlah Mahasiswa Reguler per Angkatan pada Tahun  </td>
												<td align="center" class="ganjil" rowspan="2" width="15%">Jumlah Lulusan s.d. TS (dari Mahasiswa Reguler)  </td>
												
											</tr>
											<tr>
												<td align="center" class="ganjil">TS-6</td>
												<td align="center" class="ganjil">TS-5</td>
												<td align="center" class="ganjil">TS-4</td>
												<td align="center" class="ganjil">TS-3</td>
												<td align="center" class="ganjil">TS-2</td>
												<td align="center" class="ganjil">TS-1</td>
												<td align="center" class="ganjil">TS</td>
												
											</tr>
											
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="TS-6"  name='detail[1][tahun_akademik]'  id='tahun_akademik1' readonly >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][ts_enam]' id='ts_enam1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][ts_lima]' id='ts_lima1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][ts_empat]' id='ts_empat1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][ts_tiga]' id='ts_tiga1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][ts_dua]' id='ts_dua1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][ts_satu]' id='ts_satu1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][ts]' id='ts1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][ts_jumlah]' id='ts_jumlah1' >	
												</td>
												
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="TS-5"  name='detail[2][tahun_akademik]'  id='tahun_akademik2' readonly >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][ts_enam]' id='ts_enam2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][ts_lima]' id='ts_lima2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][ts_empat]' id='ts_empat2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][ts_tiga]' id='ts_tiga2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][ts_dua]' id='ts_dua2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][ts_satu]' id='ts_satu2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][ts]' id='ts2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][ts_jumlah]' id='ts_jumlah2' >	
												</td>
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="TS-4"  name='detail[3][tahun_akademik]'  id='tahun_akademik3' readonly >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][ts_enam]' id='ts_enam3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][ts_lima]' id='ts_lima3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][ts_empat]' id='ts_empat3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][ts_tiga]' id='ts_tiga3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][ts_dua]' id='ts_dua3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][ts_satu]' id='ts_satu3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][ts]' id='ts3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][ts_jumlah]' id='ts_jumlah3' >	
												</td>
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="TS-3"  name='detail[4][tahun_akademik]'  id='tahun_akademik4' readonly >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][ts_enam]' id='ts_enam4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][ts_lima]' id='ts_lima4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][ts_empat]' id='ts_empat4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][ts_tiga]' id='ts_tiga4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][ts_dua]' id='ts_dua4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][ts_satu]' id='ts_satu4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][ts]' id='ts4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][ts_jumlah]' id='ts_jumlah4' >	
												</td>
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="TS-2"  name='detail[5][tahun_akademik]'  id='tahun_akademik5' readonly >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][ts_enam]' id='ts_enam5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][ts_lima]' id='ts_lima5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][ts_empat]' id='ts_empat5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][ts_tiga]' id='ts_tiga5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][ts_dua]' id='ts_dua5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][ts_satu]' id='ts_satu5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][ts]' id='ts5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][ts_jumlah]' id='ts_jumlah5' >	
												</td>
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="TS-1"  name='detail[6][tahun_akademik]'  id='tahun_akademik6' readonly >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[6][ts_enam]' id='ts_enam6' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[6][ts_lima]' id='ts_lima6' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[6][ts_empat]' id='ts_empat6' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[6][ts_tiga]' id='ts_tiga6' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[6][ts_dua]' id='ts_dua6' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[6][ts_satu]' id='ts_satu6' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[6][ts]' id='ts6' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[6][ts_jumlah]' id='ts_jumlah6' >	
												</td>
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="TS"  name='detail[7][tahun_akademik]'  id='tahun_akademik7' readonly >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[7][ts_enam]' id='ts_enam7' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[7][ts_lima]' id='ts_lima7' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[7][ts_empat]' id='ts_empat7' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[7][ts_tiga]' id='ts_tiga7' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[7][ts_dua]' id='ts_dua7' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[7][ts_satu]' id='ts_satu7' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[7][ts]' id='ts7' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[7][ts_jumlah]' id='ts_jumlah7' >	
												</td>
											</tr>
									
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
								<!-- /.table-responsive -->
								
							</div>
							<!-- /.panel-body -->
						</div>
					</div>
			
			</div>
			
		</div>
		
		
		  
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> 
	
	

	
		<div class="modal fade" id="modal_formref" role="dialog">
  <div class="modal-dialog" style="width:60%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formref" name="formref" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id_ref" id="id_ref"/> 
		  <input type="hidden" value="" name="id_prodi_ref" id="id_prodi_ref"/> 
		  <div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Status Evaluasi  </label> 
			<div class="col-md-5">
				<select type="select" name="status" class="form-control text-input input-sm" id="status"  style="width: 100%;" >
			
					  <option value="">--Pilih--</option>
					  <option value="3">Dikembalikan</option>
					  <option value="2">Disetujui</option>
					  </select>
				
		    </div>
		</div>
		
		 
		
     
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Keterangan  </label> 
			<div class="col-md-9">
				<textarea class="form-control" rows="5" id="note_status" name="note_status"></textarea>
		    </div>
		</div>
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSaveref" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
	
	<!--- kebijakan-->
	
		<div class="modal fade" id="modal_formsit" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formsit" name="formsit" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id_borang" id="id_borang"/> 
		  <input type="hidden" value="" name="setsit" id="setsit"/> 
          <input type="hidden" value="" name="id_keb" id="id_keb"/> 
		  
		  <div class="form-body">
		  
		  <div class="form-group field-username">
			  <label class="control-label col-md-3" for1="username">No  </label>
			  <div class="col-md-2">
			  <input  name="no" class="form-control text-input input-sm" id="no" required="required" type="text">
			  </div>
		  </div>
		  <div class="form-group field-username">
			  <label class="control-label col-md-3" for1="username">Judul Lampiran  </label>
			  <div class="col-md-9">
			  <textarea class="form-control" rows="5" id="judul_lampiran" name="judul_lampiran"></textarea>
			  </div>
		  </div>
		  
		 <div class="form-body">
			  <div class="form-group field-username">
				  <label class="control-label col-md-3" for1="username">Lampiran   </label> 
				  <div class="col-md-9">
					
					<a id="test" onclick="takenFile()"> 
					<embed src="" width="200px"  value="Ambil Lampiran" height="200px" id="pic" >Upload</embed>
					
					</a>
					<div class="hiddenfilez"><input name="imgdata"  id="imgdata"  onchange="loadFile(event)" type="file"></div>
					
				  </div>
			  </div>
          </div>
		
			
          </div>
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSavesit" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" id="btlset" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
  <script src="<?php echo base_url();?>js/atribut.js"></script>