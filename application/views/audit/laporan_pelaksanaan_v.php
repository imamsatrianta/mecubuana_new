<?php
    ob_start();
?>
<page>
    <table border="1" width="90%" cellspacing="0" cellpadding="0" align="center">
        <tr>
            <td align='center'>Unawdwdait</td>
            <td align='center'>Ketua Tim Audit</td>
            <td align='center'>Periode</td>
            <td align='center'>Tanggawadawdwl Mulai</td>
            <td align='center'>Tanggal Akhir</td>
        </tr>
<?php 
    foreach($query as $row){
    echo "<tr><td align='center'>".
        $row['nm_unit']."</td><td align='center'>".
        $row['nm_karyawan']."</td><td align='center'>".
        $row['periode']."</td><td align='center'>".
        $row['tglmulai']."</td><td align='center'>".
        $row['tglselesai']."</td></tr>";
    }
?>
    </table>
</page>
<?php
    $content = ob_get_clean();

    // convert to PDF
    //$dirpdf = str_replace('audit', 'prinpdf', dirname(__FILE__));
    //require_once(dirname(__FILE__).'/html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr');
        $html2pdf->pdf->IncludeJS("print(true);");
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('print.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
?>