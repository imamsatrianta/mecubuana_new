
	
<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					echo aksesDetail();
					?>
					 <?php
					echo aksesHapus();
					?>
                    </div><table id="table" 
					data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                            data-filter-control="true"
                           data-pagination="true"
                           data-url="Persiapanformulir/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
                            data-sort-name="id"
                            data-sort-order="desc">
                        <thead>	
						<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
							<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
							<th data-field="nm_dokumen"  data-halign="center" data-align="center"  data-sortable="true" data-filter-control="input">Nama Standar  </th>
							<th data-field="materi"  data-halign="center" data-align="left"  data-sortable="true" data-filter-control="input">Materi Audit  </th>
                        </tr>
						</thead>
                    </table>
					<!-- <a href="<?php echo base_url("audit/persiapanformulir/export"); ?>">
                        Export ke Excel</a> -->
              
            </div><!-- /.col -->
          </div>  
       
		
</div> <div class="modal fade" id="modal_formdetail" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formdetail" name="formdetail" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
          
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Nama Standar  </label> 
			<div class="col-md-9">
				<select type="select" name="id_standar" class="form-control select2 input-sm" id="id_standar" required="required"  style="width: 100%;" >
					 <option value=''>----- Pilih ----- </option>
				</select>
		    </div>
		</div>
		<div class="form-group ">	
			<label class="control-label col-md-3" for1="menudes">Materi Audit   </label> 
			<div class="col-md-9">
				<textarea class="form-control" rows="5" id="materi" name="materi"></textarea>
		    </div>
		</div>
		
		<div class="col-md-12">
			<div class="tab-pane active" id="tab_1">
						
						<div class="panel panel-default">
							<div class="panel-heading" >
							   <button type="button" id="btnNewRow" name="btnNewRow" class="btn btn-xs btn-success" onclick="addRow();"><i class="fa fa-plus"></i>
								Baris Baru
								</button>
							</div>
							
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
										<thead> 
											<tr>
												<td align="center" class="ganjil">Action </td>
												<td align="center" class="ganjil">Document</td>
											</tr>
											
											<tr id="bookTemplate" name="rowda" class="hide">
												<td  style="text-align: center;" valign="top">
												<button type='button' style='text-align: center;' id='btnDelRowX' name='btnDelRowX' class='btn btn-sm btn-danger btn-xs btn-round' title='Hapus' ><i class='fa fa-minus'></i></button> </td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='hidden'  name='id_document' id='id_document' >	
												<input  class='form-control input-sm '  type='text'  name='document' id='document' >	
												</td>
												
											</tr>
											
									
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
								<!-- /.table-responsive -->
								
							</div>
							<!-- /.panel-body -->
						</div>
					  </div>
			</div>
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> <script src="<?php echo base_url();?>js/atribut.js"></script>
  <script>
  $(document).ready(function ($) {
  	standar();
  });

	function standar(){
			
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getStandar",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_standar").append('<option value="'+val.id+'">'+val.nm_dokumen+'</option>');
					
				});
								  
				}
				});
		 }

	function editFormtambah(row){
		  $("#id_standar").val(row.id_standar);
                  $("#materi").val(row.materi);
			bersih();
			ambilDatadetail(row.id);
	  }
	 function ambilDatadetail(id){
			 var sd="id="+id;
				var i=0;
		 		$.ajax({
                    type: "GET",
					  url: '<?php echo base_url();?>audit/Persiapanformulir/getDetail',
                    data: sd,
                    dataType:"json",
                    success: function(result){
					var x=0;
					$.each(result, function(key, val) {
					x++; 
						addRow();
							$('#document'+x).val(val.document);
					});	
			}
			});
	}
  function operateFormatter(value, row, index) {
       return [
			'<?php echo aksesUbahdetail() ?>',
			'<?php echo aksesHapussatu() ?>'
        ].join('');
    }
	
	
	// add dokumen
	function bersih() {
		//alert("x")
				var y = totalrow + 1;
				
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat" + x)) {
						hapusBaris("rowmat" + x);
					}
				}
				totalrow = 0;
			}
		
		function hapusBaris(x) {
				if (document.getElementById(x) != null) {
					
					var $row    = $(this).parents('.form-group'),
					 $option = $row.find('[name="option[]"]');
					 $('#' + x).remove();
				}
			}

		  var totalrow= 0;
		function addRow() {
			totalrow++;
			var $template = $('#bookTemplate'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow)
					.attr('id', 'rowmat'+totalrow)
					.insertBefore($template);
			$clone 
				.find('[name="btnDelRowX"]').attr('onClick','hapusBaris("rowmat' + totalrow + '")').end() 
				.find('[name="id_document"]').attr('name', 'detail[' + totalrow + '][id_document]').attr('id', 'id_document'+totalrow).end()
				.find('[name="document"]').attr('name', 'detail[' + totalrow + '][document]').attr('id', 'document'+totalrow).end();
				
			
		}
  </script>