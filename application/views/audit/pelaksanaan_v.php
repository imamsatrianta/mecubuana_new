<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree/themes/default/easyui1.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree/themes/icon.css">
	<script type="text/javascript" src="<?php echo base_url();?>assets/tree/jquery.easyui.min.js"></script>
	
<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			</span>
		</div>
		</h1>
        </section>

            <!-- Modal PopUp Data -->
            <div class="modal fade" id="modalTable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" style="width:70%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4>Jadwal Audit </h4>
                            </div>
                            <div class="modal-body">
                                <table id="tabledata"
                                data-toggle="table"
                                data-search="true"
                                data-show-export="true"
                                data-minimum-count-columns="2"
                                data-pagination="true"
                                data-height="500"
                                data-url="<?php echo base_url();?>audit/Pelaksanaan/loaddatajadwal"
                                data-side-pagination="server"
                                data-pagination="true"
                                data-sort-name="id"
                                data-sort-order="asc">
                                    <thead>
                                    <tr>
                                    <th data-field="tglmulai"  data-halign="center" data-align="center"  data-sortable="true">Jadwal </th>
                                    <th data-field="periode"  data-halign="center" data-align="center"  data-sortable="true">Periode </th>
                                    <th data-field="nm_unit"  data-halign="center" data-align="center"  data-sortable="true">Unit  </th>
                                    <th data-field="nm_karyawan"  data-halign="center" data-align="center"  data-sortable="true">Ketua Tim Auditor  </th>
                                        <th data-field="cari"  id="pilih" data-halign="center" data-align="center"
                            data-formatter="operateFormatterPilih" data-events="operateEventspilih">Pilih Data</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->	

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                              <li class="active"><a href="#tab_1" data-toggle="tab">Pelaksanaan Audit</a></li>
                              <li><a href="#tab_2" data-toggle="tab">Formulir Audit</a></li>
                            </ul>
                            
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                <div id="toolbarpelaksanaan">   
                                <?php
                            echo aksesPrinheder();
                            ?> 
								</div>
                                <table id="tablepelaksanaan" 
								data-toolbar="#toolbarpelaksanaan"
									data-toggle="table"
									data-search="true"
									data-show-refresh="true"
									data-show-columns="true"
									data-show-export="true"
									data-minimum-count-columns="2"
									data-filter-control="true"
									data-pagination="true"
									data-url="Pelaksanaan/loaddataTabelPelaksanaan"
									data-side-pagination="server"
									data-pagination="true"
									data-sort-name="id"
									data-sort-order="desc">
									<thead>	
									<tr>
										<th data-field="nm_unit" data-halign="center" data-align="center" data-sortable="true">Unit</th>
										<th data-field="nm_karyawan" data-halign="center" data-align="center" data-sortable="true">Ketua Tim</th>
										<th data-field="periode" data-halign="center" data-align="center" data-sortable="true">Periode</th>
										<th data-field="tglmulai" data-halign="center" data-align="center" data-sortable="true">Tgl Mulai</th>
										<th data-field="tglselesai" data-halign="center" data-align="center" data-sortable="true">Tgl Selesai</th>
										<!-- untuk print report -->
										<!-- <th data-field="selling" data-halign="center" data-align="center" data-formatter="PrintFormatter" data-events="operateEvents">Action</th> -->
									</tr>
									</thead>
								</table>
                            </div>
                        <div class="tab-pane" id="tab_2">
                                <div id="toolbar">
                                <?php
                                echo aksesTambah();
                                ?>
                                <?php
                                echo aksesHapus();
                                ?>
                                </div><table id="table" 
                                    data-toolbar="#toolbar"
                                    data-toggle="table"
                                    data-search="true"
                                    data-show-refresh="true"
                                    data-show-columns="true"
                                    data-show-export="true"
                                    data-minimum-count-columns="2"
                                    data-filter-control="true"
                                    data-pagination="true"
                                    data-url="Pelaksanaan/loaddataTabel"
                                    data-side-pagination="server"
                                    data-pagination="true"
                                    data-sort-name="id"
                                    data-sort-order="desc">
                                    <thead>	
                                    <tr>
                                        <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
                                        <th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
                                        <th data-field="nm_unit" data-halign="center" data-align="center" data-sortable="true">Unit</th>
                                        <th data-field="nm_dokumen" data-halign="center" data-align="center" data-sortable="true">Standar Audit</th>
                                        <th data-field="catatan" data-halign="center" data-align="left" data-sortable="true">Catatan Audit</th>
                                        <th data-field="rencana" data-halign="center" data-align="left" data-sortable="true">Rencana Closing</th>
                                        <th data-field="tglclosing" data-halign="center" data-align="center" data-sortable="true">Tanggal Closing</th>
                                    </tr>
                                    </thead>
                                </table>
                                        </div>
                                        </div>
                                </div>
                        </div><!-- /.col -->
                    </div>  		
</div> 
<!-- buat popup print-->
<div class="modal fade" id="modal_formctk" role="dialog">
  <div class="modal-dialog" style="width:40%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formctk" name="formctk" class="form-horizontal" onsubmit="return false;"     >
        
		<div class="form-group ">	
			<label class="control-label col-md-5" for1="menudes"> Pilih Periode Pelaksanaan</label> 
			<div class="col-md-7">
				<select type="select" name="id_tahunakademikctk" class="form-control select2 input-sm" id="id_tahunakademikctk"   style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
				</select>
				
		    </div>
		</div>
		
          </div>
          <div class="modal-footer">
            <button type="button" id="btnctk" class="btn btn-primary" onclick="cetakDatasarmut()"  >
			<i class="fa fa-print"></i>
			Cetak</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>

     <!-- BEDA TAB -->     
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width: 90%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="form" name="form" class="form-horizontal" onsubmit="return false;"     >
         
          <input type="hidden" value="" name="id" id="id"/>
		  <input type="hidden" value="" name="set" id="set"/> 
          <input name="id_standar" id="id_standar"  type="hidden">
        <div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Periode</label> 
			<div class="col-md-7">
            <input name="id_plan" class="form-control  input-sm" id="id_plan"  type="hidden">
                 <input name="jadwal" class="form-control date-picker input-sm" id="jadwal"  type="hidden">
                 <input name="periode" class="form-control input-sm" id="periode" required="required" type="text">
		    </div>
            <div class="col-md-2">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onclick="cariJadwal()"><i class="fa fa-search"></i>Cari</button>	
            </div>
		</div> 
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Unit</label> 
			<div class="col-md-9">
            <!-- <input name="id_unit" class="form-control  input-sm" id="id_unit"  type="text"> -->
				<select type="select" name="id_unit" class="form-control select2 input-sm" id="id_unit" required="required" readonly style="width: 100%;" >
					 <option value=''></option>
				</select>
			</div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Ketua Tim Auditor  </label> 
			<div class="col-md-9">
            <!-- <input name="id_ketuatim" class="form-control  input-sm" id="id_ketuatim"  type="text"> -->
                <select type="select" name="id_ketuatim" class="form-control select2 input-sm" id="id_ketuatim" required="required"  style="width: 100%;" >
                    <option value=''> </option>
                </select>
            </div>
		</div>      
        <div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Auditie  </label> 
            <div class="col-md-9">
            <!-- <input name="id_auditie" class="form-control  input-sm" id="id_auditie"  type="text"> -->
            <select type="select" name="id_auditie" class="form-control select2 input-sm" id="id_auditie" required="required"   style="width: 100%;" >
                    <option value=''></option>
                </select>
                </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Catatan Audit </label> 
			<div class="col-md-9">
                <textarea class="form-control" rows="5" id="catatan" name="catatan"></textarea>
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Rencana Closing </label> 
			<div class="col-md-9">
				<textarea class="form-control" rows="5" id="rencana" name="rencana"></textarea>
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Tanggal Closing</label> 
			<div class="col-md-9">
				<input name="tglclosing" class="form-control date-picker input-sm" id="tglclosing" required="required" type="text">
		    </div>
		</div>
		<div class="col-md-12">
			<div class="tab-pane active" id="show_tab_1"> </div>
			</div>
		
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
              <button type="button" class="btn btn-danger" id="btnCancel" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>

          
  <script>
	 $(document).ready(function ($) {
		 // alert("s")
		
                $("#tbh").click(function () {
                    $("#show_tab_1").html('');
                });
         Unit();
         Standar();
         KetuaTim();
         Auditie();
		 });
		 
		 $(document).ready(function ($) {
		  	$(".date-picker").datepicker({ autoclose: true});
			$('.date-picker').on('changeDate show', function(e) {
                        $('#form').bootstrapValidator('revalidateField', 'tglclosing');
			});
		});
        function KetuaTim(){
			$.ajax({
				type: "POST",
				dataType: "JSON",
				url: "<?php echo base_url(); ?>global_combo/getKaryawan",
				success: function (result) {
					$.each(result, function (key, val) {
						$("#id_ketuatim").append('<option value="' + val.id + '">' + val.nm_karyawan + '</option>');
                        $("#ketuanya").append('<option value="' + val.id + '">' + val.nm_karyawan + '</option>');

					});

				}
			});
		 }
		 function Unit(){
			
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getUnitdep",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_unit").append('<option value="'+val.id+'">'+val.nm_unit+'</option>');
                $("#id_unitcetak").append('<option value="'+val.id+'">'+val.nm_unit+'</option>');
				});
								  
				}
				});
		 }
         function Auditie(){
			
            $.ajax({
              type: "POST",
              dataType:"JSON",
              url: "<?php echo base_url();?>global_combo/getAuditie",
              success: function(result) {  
              $.each(result, function(key, val) {	
              $("#id_auditie").append('<option value="'+val.id+'">'+val.nm_karyawan+'</option>');
              });
                                
              }
              });
       }

         function Standar(){
			
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getStandar",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_standar").append('<option value="'+val.id+'">'+val.nm_dokumen+'</option>');
					
				});
								  
				}
				});
         }
         function Formulirstandar(formulir){
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getFormulirstandar/"+formulir,
				success: function(result) {
                                    if(result==""){ 
                                        $("#show_tab_1").html('');
                                    } else {
                                    var standar = '';
                                    var datastandar = '';
                                    var rowstandar = [];
                                    var iddoc = '';
                                    $.each(result, function(key, val) {
                                        if(val.id_persiapan_formulir==standar){
                                            rowstandar[val.id]=rowstandar[val.id]+1;
                                        } else {
                                            rowstandar[val.id]=1;
                                        }
                                        standar = val.id_persiapan_formulir;
                                        iddoc = val.id_doc+','+iddoc;
                                    });
                                    standar = '';
                                    iddoc = iddoc+'_';
                                    iddoc = iddoc.replace(',_', '');
                                    $.each(result, function(key, val) {
                                    if(val.nm_dokumen==null){val.nm_dokumen='';}else{}
                                    if(val.materi==null){val.materi='';}else{}
                                    if(val.document==null){val.document='';}else{}
                                    if(val.nm_dokumen!=standar){
                                        datastandar = datastandar+"<tr><td rowspan='"+rowstandar[val.id]+"'>"+val.nm_dokumen+
                                        "</td><td rowspan='"+rowstandar[val.id]+"'>"+val.materi+
                                        "</td><td>"+val.document+
                                        "<input class='form-control input-sm'  type='hidden'  name='id_formdoc_"+val.id_doc+"' id='id_formdoc["+val.id_doc+"]' value='"+val.id_doc+"'></td><td align='center'><input  class='easyui-radiobutton'"+
                                        "type='radio' name='checklist_"+val.id_doc+"' id='checklist-a["+val.id_doc+"]' value='1'></td><td align='center'><input  class='easyui-radiobutton' "+
                                        "type='radio'  name='checklist_"+val.id_doc+"' id='checklist-b["+val.id_doc+"]' value='2'></td><td align='center'><input  class='easyui-radiobutton' "+
                                        "type='radio'  name='checklist_"+val.id_doc+"' id='checklist-c["+val.id_doc+"]' value='3'></td><td align='center'><textarea class='form-control' "+
                                        "rows='3'  name='keterangan_"+val.id_doc+"' id='keterangan["+val.id_doc+"]'></textarea></td><td align='center'><textarea class='form-control' "+
                                        "rows='3'  name='masalah_"+val.id_doc+"' id='masalah["+val.id_doc+"]'></textarea></td><td align='center'><textarea class='form-control' "+
                                        "rows='3'  name='tindakan_"+val.id_doc+"' id='tindakan["+val.id_doc+"]'></textarea></td><td align='center'><button type='button' style='text-align: center;' id='btnDelRowX' "+
                                        "name='btnDelRowX' class='btn btn-sm btn-danger btn-xs btn-round' title='Hapus' onclick='btnDelRowDoc(\""+val.id_doc+"\");'><i class='fa fa-minus'></i></button></td></tr>";
                                    } else {
                                        datastandar = datastandar+"<tr><td>"+val.document+
                                        "<input class='form-control input-sm'  type='hidden'  name='id_formdoc_"+val.id_doc+"' id='id_formdoc["+val.id_doc+"]' value='"+val.id_doc+"'></td><td align='center'><input  class='easyui-radiobutton' "+
                                        "type='radio'  name='checklist_"+val.id_doc+"' id='checklist-a["+val.id_doc+"]' value='1'></td><td align='center'><input  class='easyui-radiobutton' "+
                                        "type='radio'  name='checklist_"+val.id_doc+"' id='checklist-b["+val.id_doc+"]' value='2'></td><td align='center'><input  class='easyui-radiobutton' "+
                                        "type='radio'  name='checklist_"+val.id_doc+"' id='checklist-c["+val.id_doc+"]' value='3'></td><td align='center'><textarea class='form-control' "+
                                        "rows='3'  name='keterangan_"+val.id_doc+"' id='keterangan["+val.id_doc+"]'></textarea></td><td align='center'><textarea class='form-control' "+
                                        "rows='3'  name='masalah_"+val.id_doc+"' id='masalah["+val.id_doc+"]'></textarea></td><td align='center'><textarea class='form-control' "+
                                        "rows='3'  name='tindakan_"+val.id_doc+"' id='tindakan["+val.id_doc+"]'></textarea></td><td align='center'><button type='button' style='text-align: center;' id='btnDelRowX' "+
                                        "name='btnDelRowX' class='btn btn-sm btn-danger btn-xs btn-round' title='Hapus' onclick='btnDelRowDoc(\""+val.id_doc+"\");'><i class='fa fa-minus'></i></button></td></tr>"
                                    }
                                    standar = val.nm_dokumen;
                                });
                                var table = '<div class="panel panel-default"><div class="panel-body"><div class="table-responsive">'+
                                '<table class="table table-striped table-bordered table-hover"><thead><tr><td align="center" rowspan="2">Standar</td>'+
                                '<td align="center" rowspan="2">Materi Audit</td><td align="center" rowspan="2">Document</td><td align="center" colspan="3">Check List</td>'+
                                '<td align="center" rowspan="2">Keterangan</td><td align="center" rowspan="2">Akar Masalah</td>'+
                                '<td align="center" rowspan="2">Tindakan Korektif</td><td align="center" rowspan="2">Action</td></tr>'+
                                '<tr><td align="center">Ada</td><td align="center">Tidak Ada</td><td align="center">Ada Dengan Catatan</td></tr></thead><tbody>'+datastandar+
                                '</tbody></table><input class="form-control input-sm"  type="hidden"  name="iddoc" id="iddoc" value="'+iddoc+'"></div></div></div>';
                                $("#show_tab_1").html(table);
				}}
				});
		 }
            function Formulirstandaraudit(id){
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getFormulirstandaraudit/"+id,
				success: function(result) {  
                                var iddoc = '';
				$.each(result, function(key, val) {
                                    iddoc = val.id_persiapan_formulir_doc+','+iddoc;
                                    document.getElementById("id_formdoc["+val.id_persiapan_formulir_doc+"]").value = val.id_persiapan_formulir_doc;
                                    document.getElementById("keterangan["+val.id_persiapan_formulir_doc+"]").value = val.keterangan;
                                    document.getElementById("masalah["+val.id_persiapan_formulir_doc+"]").value = val.masalah;
                                    document.getElementById("tindakan["+val.id_persiapan_formulir_doc+"]").value = val.tindakan;
                                    if(val.checklist=='1'){
                                        document.getElementById("checklist-a["+val.id_persiapan_formulir_doc+"]").checked = true;
                                    } 
                                    else if(val.checklist=='2'){
                                        document.getElementById("checklist-b["+val.id_persiapan_formulir_doc+"]").checked = true;
                                    } 
                                    else if(val.checklist=='3'){
                                        document.getElementById("checklist-c["+val.id_persiapan_formulir_doc+"]").checked = true;
                                    } 
                                    else {
                                        ["checklist-a["+val.id_persiapan_formulir_doc+"]", "checklist-b["+val.id_persiapan_formulir_doc+"]", "checklist-c["+val.id_persiapan_formulir_doc+"]"].forEach(function(id) {
                                            document.getElementById(id).checked = false;
                                          });
                                    }
				});	
                                iddoc = iddoc+'_';
                                iddoc = iddoc.replace(',_', '');
                                document.getElementById("iddoc").value = iddoc;
				}
				});
		 }
                 
        function btnDelRowDoc(id) {
        document.getElementById("keterangan["+id+"]").value = "";
        document.getElementById("masalah["+id+"]").value = "";
        document.getElementById("tindakan["+id+"]").value = "";
        ["checklist-a["+id+"]", "checklist-b["+id+"]", "checklist-c["+id+"]"].forEach(function(id) {
            document.getElementById(id).checked = false;
            });
        }
	  function editFormtambah(row){
		   $("#id_unit").val(row.id_unit);
           $("#id_plan").val(row.id);
           $("#id_ketuatim").val(row.id_ketuatim);
		   $("#id_standar").val(row.id_standar);
           $("#id_auditie").val(row.id_auditie);
		   $("#catatan").val(row.catatan);
           $("#periode").val(row.periode);
		   $("#rencana").val(row.rencana);
           $("#jadwal").val(row.jadwal);
		   $("#tglclosing").val(row.tglclosing);
        Formulirstandar(row.id_standar);
        Formulirstandaraudit(row.id);
	  }
  function operateFormatter(value, row, index) {
      return [
			'<?php echo aksesUbah() ?>',
			'<?php echo aksesHapussatu() ?>'
        ].join('');
	}
	
	function PrintFormatter(value, row, index) {
      return [
			'<?php echo aksesPrindetail('pelaksanaan') ?>'
        ].join('');
    }
    function cariJadwal(){
            $('#modalTable').modal('show'); 
            $("#modalTable").css({"z-index":"1060"});
            $('html,body').scrollTop(0);
            loadDatajadwalaudit(1, 15,'desc');
    }	
    function tutupFormpopup(){
      	$('#modalTable').modal('hide'); // show bootstrap modal
		bukaModal();
    }
    function loadDatajadwalaudit(number, size,order){
			var $table = $('#tabledata');
            var offset=(number - 1) * size;
            var limit=size;
            $.ajax({
                    type: "POST",
                    url: "Pelaksanaan/loaddatajadwal?order="+order+"&limit="+limit+"&offset="+offset,
                    dataType:"JSON",
                    success: function(result){
    				 $table.bootstrapTable('load', result);
    			
                    }
                });
    }
    function operateFormatterPilih(value, row, index) {
       return [
            '<a class="btn btn-sm btn-primary btn-xs" id="pilih" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Pilih" >',
            'Pilih',
            '</a> '
        ].join('');
    }
    function tutupFormpopup(){
      	$('#modalTable').modal('hide'); // show bootstrap modal
		bukaModal();
	}
	
	function bukaModal(){
		
		$("#modal_form").css({"overflow-y":"scroll"});
	
    }
    window.operateEventspilih = {
        'click #pilih': function (e, value, row, index) {
            cariDatapopup(row);
        }
    };

    function cariDatapopup(row){
		
		//alert(baris);
	//	 alert(row.nm_grup_tiga);
         $("#id_plan").val(row.id);
         $("#jadwal").val(row.tglmulai);
         Dataplan(row.id);
		 tutupFormpopup();
     }
     function Dataplan(periode){
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getDataplanstandar/"+periode,
				success: function(result) {
                                if(result==""){
                                    $("#id_plan").val('');
                                    $("#id_formulir_audit").val('');
                                    $("#id_unit").val('');
                                    $("#nm_unit").val('');
                                    $("#id_ketuatim").val('');
                                    $("#nm_karyawan").val('');
                                    $("#id_standar").val('');
                                    $("#tglmulai").val('');
                                    $("#tglselesai").val('');
                                    $("#show_tab_1").html('');
                                } else {
                                    $.each(result, function(key, val) {
                                    $("#id_formulir_audit").val(val.id_formulir_audit);
                                    // $("#id_plan").val(val.id);
                                    $("#periode").val(val.periode);
                                    $("#id_unit").val(val.id_unit);
                                    $("#id_ketuatim").val(val.id_ketuatim);
                                    $("#id_auditie").val(val.id_auditie);
                                    $("#id_standar").val(val.id_standar);
                                    $("#nm_karyawan").val(val.nm_karyawan);
                                    Formulirstandar(val.id_standar);
                                   
                                    });
                                }}
				});
		 }

      	$(document).ready(function ($) {
			$.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getPlaningpersiapan",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_tahunakademikctk").append('<option value="'+val.id_planingaudit+'">'+val.nm_unit+' Periode '+val.periode+' Ketua Tim '+val.ketua_tim+'</option>');	
                // $("#id_tahunakademikctk").change(function () {
                //     $("#id_unitcetak").val(val.nm_unit);
                //     $("#ketuanya").val(val.ketua_tim);
                // });	
                });
            	  
				}
				});
             
	    });
	  
		function operateDetail(index, row,element){
		$.ajax({
				  type: "POST",
				  url: "inputsarmut/loaddatadetailkegiatan?id="+row['id']+"&index="+index,
				  dataType:"JSON",
				  success: function(result) { 

				  	var x1 = "no_baris_"+index;
				  	var el = "tr[data-index='"+index+"']"
					$(el).next('tr.detail-view').addClass(x1);

					var id,id_sarmut,uraian_kegiatan,tgl_mulai,tgl_selesai,angaran,score,presentase,anggaran;
					var tbl_det;
					tbl_det = '<td colspan="10" align="center"><table border="1" width="100%" style="border-style: solid;border-color:#9F9492;">';
					tbl_det += '<tr style="border-color:#9F9492;">';
					tbl_det += '<td align="center" width="10%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;"> Tanggal Mulai</td>';
					tbl_det += '<td align="center" width="10%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Tanggal Selesai </td>';
					tbl_det += '<td align="center" width="50%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Nama Kegiatan</td>'
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Anggaran</td>'
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Ket Proses</td>'
					tbl_det += '<td align="center" width="10%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Presentase</td>'
					tbl_det += '</tr>';
        			tbl_det += '<tr style="border-color:#9F9492;">';

        			$.each(eval(result), function (key,value) {
						
        				id = value['id'];
						id_sarmut = value['id_sarmut'];
        				uraian_kegiatan = value['uraian_kegiatan'];
        				tgl_mulai = value['tgl_mulai'];
        				tgl_selesai= value['tgl_selesai'];
						score=value['score'];
						presentase=value['presentase'];
						anggaran=value['anggaran'];
					//	alert(idkeg);
						tbl_det += '<td style="border-color:#9F9492;" >&nbsp;&nbsp; '+tgl_mulai+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+tgl_selesai+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+uraian_kegiatan+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+anggaran+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+score+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+presentase+'</td>';
        				tbl_det += '</tr>';

        			});
        			tbl_det += '</table></td>';

					$(".detail-view."+x1).html(tbl_det);

				  }


		});

    }
	
	function printHeder(){ 
	$('#formctk')[0].reset(); 
	 $('#formctk').bootstrapValidator('resetForm', true); 
      $('#modal_formctk').modal('show'); 
      $('.modal-title').text('Form Cetak Rencana Sarmut'); 
	  
		//window.open('<?php echo base_url();?>master/mkp/cetakMkp?id=1','_blank');
		//window.open('<?php echo base_url();?>master/mkp/cetakMkpword?id=1');
	}
	
	function cetakDatasarmut(){
		var periode=$('#id_tahunakademikctk').val();
		var id_unitcetak=$('#id_unitcetak').val();
		var id_ketuatim=$('#ketuanya').val();
        var id_planing_audit=$('#idnya').val();
           
		if(periode==""){
            bootbox.alert("Periode Tidak Boleh Kosong"); 
		}else if(id_unitcetak==""){
            bootbox.alert("Unit Tidak Boleh Kosong"); 
		}else{
            window.open('<?php echo base_url();?>audit/pelaksanaan/cetakLapformaudit?periode='+periode+'&id_unit='+id_unitcetak+'&id_ketuatim='+id_ketuatim);
		}
		
	}
function htmlFormatsarmut(value, row, index){
	var mutu=row.uraian_sarmut;
	var res = mutu.substr(0, 200);
	return strip_html_tags(res);
}

function strip_html_tags(str)
{
   if ((str===null) || (str===''))
       return false;
  else
   str = str.toString();
  return str.replace(/<[^>]*>/g, '');
}
  </script>
  
  
  <script src="<?php echo base_url();?>js/atribut.js"></script>