<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
        </section>

		<!-- Modal PopUp Data -->
		<div class="modal fade" id="modalTable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog" style="width:70%">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span></button>
									<h4>Jadwal Audit </h4>
								</div>
								<div class="modal-body">
									<table id="tabledata"
									data-toggle="table"
									data-search="true"
									data-show-export="true"
									data-minimum-count-columns="2"
									data-pagination="true"
									data-height="500"
									data-url="<?php echo base_url();?>audit/laporan/loaddatajadwal"
									data-side-pagination="server"
									data-pagination="true"
									data-sort-name="id"
									data-sort-order="asc">
										<thead>
										<tr>
									<th data-field="tglmulai"  data-halign="center" data-align="center"  data-sortable="true">Jadwal </th>
                                    <th data-field="periode"  data-halign="center" data-align="center"  data-sortable="true">Periode </th>
                                    <th data-field="nm_unit"  data-halign="center" data-align="center"  data-sortable="true">Unit  </th>
                                    <th data-field="nm_karyawan"  data-halign="center" data-align="center"  data-sortable="true">Ketua Tim Auditor  </th>
                                        <th data-field="cari"  id="pilih" data-halign="center" data-align="center"
								data-formatter="operateFormatterPilih" data-events="operateEventspilih">Pilih Data</th>
										</tr>
										</thead>
									</table>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->	
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					echo aksesDetail();
					?>
					 <?php
					echo aksesHapus();
					?>
                    </div><table id="table" 
                            data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                            data-filter-control="true"
                           data-pagination="true"
                           data-url="Laporan/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
                            data-sort-name="id"
                            data-sort-order="desc">
                        <thead>	
                        <tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
                            <th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
                            <th data-field="nm_unit" data-halign="center" data-align="center" data-sortable="true">Unit</th>
                            <th data-field="tgl_audit" data-halign="center" data-align="center" data-sortable="true">Tanggal Audit</th>
                            <th data-field="jam" data-halign="center" data-align="center" data-sortable="true">Jam</th>
                            <th data-field="kegiatan" data-halign="center" data-align="left" data-sortable="true">Kegiatan</th>
                        </tr>
			</thead>
                    </table>
                
              
            </div><!-- /.col -->
          </div>  
       
		
</div> 
<div class="modal fade" id="modal_formdetail" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formdetail" name="formdetail" class="form-horizontal" onsubmit="return false;"     >
          <input type="text"  name="id_plan" id="id_plan"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Periode</label> 
			<div class="col-md-7">
				<input name="jadwal" class="form-control date-picker input-sm" id="jadwal"  type="hidden">
                 <input name="periode" class="form-control  input-sm" id="periode"  type="text">
		    </div>
            <div class="col-md-2">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onclick="cariJadwal()"><i class="fa fa-search"></i>Cari</button>	
        </div>
		</div> 
        <div class="form-group ">
		    <label class="control-label col-md-3" for1="menudes">Unit </label> 
			<div class="col-md-9">
				<select type="select" name="id_unit" class="form-control select2 input-sm" id="id_unit" required="required" readonly style="width: 100%;" >
					 <option value=''>----- Pilih -----</option>
				</select>
			</div>
		</div>
		<div class="form-group ">
		    <label class="control-label col-md-3" for1="menudes">Ketua Tim Auditor </label> 
			<div class="col-md-9">
				<select type="select" name="id_ketuatim" class="form-control select2 input-sm" id="id_ketuatim" required="required" readonly style="width: 100%;" >
					 <option value=''>----- Pilih -----</option>
				</select>
			</div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Auditie  </label> 
			<div class="col-md-9">
	
			<select type="select" name="id_auditie" class="form-control select2 input-sm" id="id_auditie" required="required" readonly style="width: 100%;" >
					 <option value=''>----- Pilih -----</option>
				</select>
			</div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Tanggal Audit</label> 
			<div class="col-md-9">
				<input name="tglaudit" class="form-control date-picker input-sm" id="tglaudit" required="required" type="text">
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Jam Mulai </label> 
			<div class="col-md-9">
				<input name="jam" class="form-control timepicker input-sm" id="jam" required="required" type="text">
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Jam Selesai </label> 
			<div class="col-md-9">
				<input name="jam" class="form-control timepicker input-sm" id="jam" required="required" type="text">
		    </div>
		</div>
        <div class="form-group ">
            <label class="control-label col-md-3" for1="menudes">Kegiatan </label> 
			<div class="col-md-9">
               <textarea class="form-control" rows="5" id="kegiatan" name="kegiatan" required="required"></textarea>
		    </div>
		</div>

		
		 <div class="col-md-12">
		 	<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
                              <li class="active"><a href="#tab_1" data-toggle="tab">KTS/OB</a></li>
                              <li><a href="#tab_2" data-toggle="tab">Bidang</a></li>
							  <li><a href="#tab_3" data-toggle="tab">Daftar hadir</a></li>
							  <li><a href="#tab_4" data-toggle="tab">Kesimpulan</a></li>
                            </ul>
						<!-- tab content open -->
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1">
							<div class="panel panel-default">
							<div class="panel-heading" >
							   <button type="button" id="btnNewRow" name="btnNewRow" class="btn btn-xs btn-success" onclick="addRow();"><i class="fa fa-plus"></i>
								Baris Baru
								</button>
							</div>
							
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
										<thead> 
											<tr>
												<td align="center" class="ganjil">Action </td>
													<td align="center" class="ganjil">KTS/OB</td>
													<td align="center" class="ganjil">Standar</td>
													<td align="center" class="ganjil">Pernyataan</td>
											</tr>
											
											<tr id="bookTemplate" name="rowda" class="hide">
												<td  style="text-align: center;" valign="top">
												<button type='button' style='text-align: center;' id='btnDelRowX' name='btnDelRowX' class='btn btn-sm btn-danger btn-xs btn-round' title='Hapus' ><i class='fa fa-minus'></i></button> </td>
												
                                                <td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='hidden'  name='id_kts_ob' id='id_kts_ob' >	
												<input  class='form-control input-sm '  type='text'  name='kts_ob' id='kts_ob' >	
												</td>
                                                <td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='hidden'  name='id_dok' id='id_dok' >	
												<select type="select" name="id_standar" class="form-control select2 input-sm" id="id_standar" style="width: 100%;" >
                                            		<option <?php echo 'selected';?> value=''>--Pilih-- </option>
                                            	</select>
												</td>
                                                                                                <td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='hidden'  name='id_pernyataan' id='id_pernyataan' >	
												<input  class='form-control input-sm '  type='text'  name='pernyataan' id='pernyataan' >	
												</td>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div><!-- /.table-responsive -->
								
								
							</div><!-- /.panel-body -->
							
						</div><!-- /.panel-default -->
					</div><!-- tab_1 pane close -->
							<div class="tab-pane" id="tab_2">
								  <div class="panel panel-default">
							<div class="panel-heading" >
							   <button type="button" id="btnNewRow2" name="btnNewRow2" class="btn btn-xs btn-success" onclick="addRow2();"><i class="fa fa-plus"></i>
								Baris Baru
								</button>
							</div>
							
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
										<thead> 
											<tr>
												<td align="center" class="ganjil">Action </td>
													<td align="center" class="ganjil">Bidang</td>
													<td align="center" class="ganjil">Kelebihan</td>
													<td align="center" class="ganjil">Peluang Peningkatan</td>
											</tr>				
											<tr id="bookTemplate2" name="rowda2" class="hide">
												<td  style="text-align: center;" valign="top">
												<button type='button' style='text-align: center;' id='btnDelRowX2' name='btnDelRowX2' class='btn btn-sm btn-danger btn-xs btn-round' title='Hapus' ><i class='fa fa-minus'></i></button> </td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='hidden'  name='id_bidang' id='id_bidang' >	
												<input  class='form-control input-sm '  type='text'  name='bidang' id='bidang' ></td>	
                                                <td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='hidden'  name='id_kelebihan' id='id_kelebihan' >	
												<input  class='form-control input-sm '  type='text'  name='kelebihan' id='kelebihan' ></td>		
                                                <td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='hidden'  name='id_peluang' id='id_peluang' >	
												<input  class='form-control input-sm '  type='text'  name='peluang' id='peluang' >	
												</td>
									
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
								<!-- /.table-responsive -->
							</div><!-- /.panel-body -->		
						</div><!-- /.panel-default -->
					</div><!-- tab_2 pane close -->
					<div class="tab-pane" id = "tab_3">
							<div class="panel panel-default">
								<div class="panel-heading" >
									<button type="button" id="btnNewRow3" name="btnNewRow3" class="btn btn-xs btn-success" onclick="addRow3();"><i class="fa fa-plus"></i>
										Baris Baru
									</button>
								</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
										<thead> 
											<tr>
												<td align="center" class="ganjil">Action </td>
												<td align="center" class="ganjil">Unit</td>
												<td align="center" class="ganjil">Nama </td>
											</tr>
											
											<tr id="bookTemplate3" name="rowda3" class="hide">
												<td  style="text-align: center;" valign="top">
												<button type='button' style='text-align: center;' id='btnDelRowX3' name='btnDelRowX3' class='btn btn-sm btn-danger btn-xs btn-round' title='Hapus' ><i class='fa fa-minus'></i></button> </td>									
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<select type="select" name="id_unitdet" class="form-control select2 input-sm" id="id_unitdet" style="width: 100%;" >
													<option <?php echo 'selected';?> value=''>--Pilih-- </option>
												</select>
												</td>
												<td><input  class='form-control input-sm '  type='text'  name='nm_hadir' id='nm_hadir' ></td>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
								<!-- /.table-responsive -->
							</div>
							<!-- /.panel-body -->
						</div><!-- /.panel-default -->
					</div><!-- tab_3 pane close -->
					<div class="tab-pane" id="tab_4">
						<div class="panel panel-default">
							<div class="panel-heading" >
							<button type="button" id="btnNewRow4" name="btnNewRow4" class="btn btn-xs btn-success" onclick="addRow4();"><i class="fa fa-plus"></i>
								Baris Baru
								</button>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
										<thead> 
											<tr>
												<td align="center" class="ganjil">Action </td>
												<td align="center" class="ganjil">Kesimpulan</td>
												<td align="center" class="ganjil">Ya/Tidak</td>
												<td align="center" class="ganjil">Lainnya</td>
											</tr>
											<tr id="bookTemplate4" name="rowda4" class="hide">
												<td  style="text-align: center;" valign="top">
												<button type='button' style='text-align: center;' id='btnDelRowX4' name='btnDelRowX4' class='btn btn-sm btn-danger btn-xs btn-round' title='Hapus' ><i class='fa fa-minus'></i></button> </td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='kesimpulan' id='kesimpulan'>	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='opsi' id='opsi'>	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='lainnya' id='lainnya'>	
												</td>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
								<!-- /.table-responsive -->
							</div>
							<!-- /.panel-body -->
						</div>
					</div><!-- tab_4 pane close -->
				</div>
			</div><!-- tab content close -->	
			</div>
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> 
     <script src="<?php echo base_url();?>js/atribut.js"></script>
  <script>
	 $(document).ready(function ($) {
		 // alert("s")
		 
		
               
			$("#periode").change(function () {
				var periode = this.value;
				Dataplan(periode);
			});
      
		 Unit();
		 Standar();
		 KetuaTim();
		 Auditie();
		 });
		 $(document).ready(function ($) {
		  	$(".date-picker").datepicker({ autoclose: true});
			$('.date-picker').on('changeDate show', function(e) {
				$('#formdetail').bootstrapValidator('revalidateField', 'tglaudit');
			});
                        $(".timepicker").timepicker({ autoclose: true, showInputs: false, showMeridian: false, showSeconds: true});
			$('.timepicker').on('changeTime.timepicker', function(e) {
				$('#formdetail').bootstrapValidator('revalidateField', 'jam');
			});
                });
		
		function KetuaTim(){
			$.ajax({
				type: "POST",
				dataType: "JSON",
				url: "<?php echo base_url(); ?>global_combo/getKaryawan",
				success: function (result) {
					$.each(result, function (key, val) {
						$("#id_ketuatim").append('<option value="' + val.id + '">' + val.nm_karyawan + '</option>');

					});

				}
			});
		 }
		 function Auditie(){
			
            $.ajax({
              type: "POST",
              dataType:"JSON",
              url: "<?php echo base_url();?>global_combo/getAuditie",
              success: function(result) {  
              $.each(result, function(key, val) {	
              $("#id_auditie").append('<option value="'+val.id+'">'+val.nm_karyawan+'</option>');
              });
                                
              }
              });
       }
		 function Unit(){
			
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getUnitdep",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_unit").append('<option value="'+val.id+'">'+val.nm_unit+'</option>');
				$("#id_unitdet").append('<option value="'+val.id+'">'+val.nm_unit+'</option>');
				});
								  
				}
				});
		 }
                 function Standar(){
			
			  $.ajax({
			type: "POST",
			dataType:"JSON",
			url: "<?php echo base_url();?>global_combo/getStandar",
			success: function(result) {  
			$.each(result, function(key, val) {	
			$("#id_standar").append('<option value="'+val.id+'">'+val.nm_dokumen+'</option>');
				
			});
							  
			}
			});
		 }
	  function editFormtambah(row){
           $("#id_unit").val(row.id_unit);
		   $("#tglaudit").val(row.tgl_audit);
           $("#jam").val(row.jam);
		   $("#id_plan").val(row.id);
		   $("#kegiatan").val(row.kegiatan);
		   $("#periode").val(row.periode);
		   $("#jadwal").val(row.tgl_mulai); // jmb
		   $("#id_ketuatim").val(row.id_ketuatim);
		   $("#id_auditie").val('1');

		   bersih();
           ambilDataktsob(row.id);
		   ambilDatabidang(row.id);
		   ambilDatadaftarhadir(row.id);
		   ambilDatakesimpulan(row.id);
		   
	  }
          function ambilDataktsob(id){
			 var sd="id="+id;
				var i=0;
		 		$.ajax({
                    type: "GET",
                    url: '<?php echo base_url();?>audit/Laporan/getktsob',
                    data: sd,
                    dataType:"json",
                    success: function(result){
                        var x=0;
                        $.each(result, function(key, val) {
                        x++; 
                                addRow();
                                $('#kts_ob'+x).val(val.kts_ob);
                                $('#id_standar'+x).val(val.id_standar);
                                $('#pernyataan'+x).val(val.pernyataan);
                        });	
			}
			});
	}
		function ambilDatakesimpulan(id){
				var sd="id="+id;
					var i=0;
					$.ajax({
						type: "GET",
						url: '<?php echo base_url();?>audit/Laporan/getkesimpulan',
						data: sd,
						dataType:"json",
						success: function(result){
							var x=0;
							$.each(result, function(key, val) {
							x++; 
									addRow4();
									$('#kesimpulan'+x).val(val.kesimpulan);
									$('#opsi'+x).val(val.opsi);
									$('#lainnya'+x).val(val.lainnya);
							});	
				}
				});
		}
		function ambilDatadaftarhadir(id){
				var sd="id="+id;
					var i=0;
					$.ajax({
						type: "GET",
						url: '<?php echo base_url();?>audit/Laporan/getdaftarhadir',
						data: sd,
						dataType:"json",
						success: function(result){
							var x=0;
							$.each(result, function(key, val) {
							x++; 
									addRow3();
									$('#id_unitdet'+x).val(val.id_unit);
									$('#nm_hadir'+x).val(val.nm_karyawan);
							});	
				}
				});
		}
        function ambilDatabidang(id){
			 var sd="id="+id;
				var i=0;
		 		$.ajax({
                    type: "GET",
                    url: '<?php echo base_url();?>audit/Laporan/getbidang',
                    data: sd,
                    dataType:"json",
                    success: function(result){
                        var x=0;
                        $.each(result, function(key, val) {
                        x++; 
                                addRow2();
                                $('#bidang'+x).val(val.bidang);
                                $('#kelebihan'+x).val(val.kelebihan);
                                $('#peluang'+x).val(val.peluang);
                        });	
			}
			});
	}


  function operateFormatter(value, row, index) {
      return [
			'<?php echo aksesUbahdetail() ?>',
			'<?php echo aksesHapussatu() ?>'
        ].join('');
    }
    
    // add dokumen
	function bersih() {
		//alert("x")
				var y = totalrow + 1;
				
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat" + x)) {
						hapusBaris("rowmat" + x);
					}
				}
				totalrow = 0;
				
				var y = totalrow2 + 1;
				
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat_2" + x)) {
						hapusBaris("rowmat_2" + x);
					}
				}
				totalrow2 = 0;
				var y = totalrow3 + 1;
				
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat_3" + x)) {
						hapusBaris("rowmat_3" + x);
					}
				}
				totalrow3 = 0;
				var y = totalrow4 + 1;
				
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat_4" + x)) {
						hapusBaris("rowmat_4" + x);
					}
				}
				totalrow4 = 0;
			}
		
		function hapusBaris(x) {
				if (document.getElementById(x) != null) {
					var $row    = $(this).parents('.form-group'),
					 $option = $row.find('[name="option[]"]');
					 $('#' + x).remove();
				}
			}

		  var totalrow= 0;
		  var totalrow2= 0;
		  var totalrow3= 0;
		  var totalrow4= 0;
		function addRow() {
			totalrow++;
			var $template = $('#bookTemplate'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow)
					.attr('id', 'rowmat'+totalrow)
					.insertBefore($template);
			$clone 
				.find('[name="btnDelRowX"]').attr('onClick','hapusBaris("rowmat' + totalrow + '")').end() 
				.find('[name="id_kts_ob"]').attr('name', 'ktsob[' + totalrow + '][id_kts_ob]').attr('id', 'id_kts_ob'+totalrow).end()
				.find('[name="kts_ob"]').attr('name', 'ktsob[' + totalrow + '][kts_ob]').attr('id', 'kts_ob'+totalrow).end()
				.find('[name="id_dok"]').attr('name', 'ktsob[' + totalrow + '][id_dok]').attr('id', 'id_dok'+totalrow).end()
				.find('[name="id_standar"]').attr('name', 'ktsob[' + totalrow + '][id_standar]').attr('id', 'id_standar'+totalrow).end()
				.find('[name="id_pernyataan"]').attr('name', 'ktsob[' + totalrow + '][id_pernyataan]').attr('id', 'id_pernyataan'+totalrow).end()
				.find('[name="pernyataan"]').attr('name', 'ktsob[' + totalrow + '][pernyataan]').attr('id', 'pernyataan'+totalrow).end();
					
		}
        function addRow2() {
			totalrow2++;
			var $template = $('#bookTemplate2'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow2)
					.attr('id', 'rowmat_2'+totalrow2)
					.insertBefore($template);
			$clone 
				.find('[name="btnDelRowX2"]').attr('onClick','hapusBaris("rowmat_2' + totalrow2 + '")').end() 
				.find('[name="id_bidang"]').attr('name', 'bidangdet[' + totalrow2 + '][id_bidang]').attr('id', 'id_bidang'+totalrow2).end()
				.find('[name="bidang"]').attr('name', 'bidangdet[' + totalrow2 + '][bidang]').attr('id', 'bidang'+totalrow2).end()
				.find('[name="id_kelebihan"]').attr('name', 'bidangdet[' + totalrow2 + '][id_kelebihan]').attr('id', 'id_kelebihan'+totalrow2).end()
				.find('[name="kelebihan"]').attr('name', 'bidangdet[' + totalrow2 + '][kelebihan]').attr('id', 'kelebihan'+totalrow2).end()
				.find('[name="id_peluang"]').attr('name', 'bidangdet[' + totalrow2 + '][id_peluang]').attr('id', 'id_peluang'+totalrow2).end()
				.find('[name="peluang"]').attr('name', 'bidangdet[' + totalrow2 + '][peluang]').attr('id', 'peluang'+totalrow2).end();

		}
		function addRow3() {
			totalrow3++;
			var $template = $('#bookTemplate3'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow3)
					.attr('id', 'rowmat_3'+totalrow3)
					.insertBefore($template);
			$clone 
				.find('[name="btnDelRowX3"]').attr('onClick','hapusBaris("rowmat_3' + totalrow3 + '")').end() 
				.find('[name="id_unitdet"]').attr('name', 'kehadiran[' + totalrow3 + '][id_unitdet]').attr('id', 'id_unitdet'+totalrow3).end()
				.find('[name="nm_hadir"]').attr('name', 'kehadiran[' + totalrow3 + '][nm_hadir]').attr('id', 'nm_hadir'+totalrow3).end();

		}
		function addRow4() {
			totalrow4++;
			var $template = $('#bookTemplate4'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow4)
					.attr('id', 'rowmat_4'+totalrow4)
					.insertBefore($template);
			$clone 
				.find('[name="btnDelRowX4"]').attr('onClick','hapusBaris("rowmat_4' + totalrow4 + '")').end() 
				.find('[name="kesimpulan"]').attr('name', 'kesimpulans[' + totalrow4 + '][kesimpulan]').attr('id', 'kesimpulan'+totalrow4).end()
				.find('[name="opsi"]').attr('name', 'kesimpulans[' + totalrow4 + '][opsi]').attr('id', 'opsi'+totalrow4).end()
				.find('[name="lainnya"]').attr('name', 'kesimpulans[' + totalrow4 + '][lainnya]').attr('id', 'lainnya'+totalrow4).end();


		}
		function cariJadwal(){
            $('#modalTable').modal('show'); 
            $("#modalTable").css({"z-index":"1060"});
            $('html,body').scrollTop(0);
            loadDatajadwalaudit(1, 15,'desc');
    }	
    function tutupFormpopup(){
      	$('#modalTable').modal('hide'); // show bootstrap modal
		bukaModal();
    }
    function loadDatajadwalaudit(number, size,order){
			var $table = $('#tabledata');
            var offset=(number - 1) * size;
            var limit=size;
            $.ajax({
                    type: "POST",
                    url: "laporan/loaddatajadwal?order="+order+"&limit="+limit+"&offset="+offset,
                    dataType:"JSON",
                    success: function(result){
    				 $table.bootstrapTable('load', result);
    			
                    }
                });
    }
    function operateFormatterPilih(value, row, index) {
       return [
            '<a class="btn btn-sm btn-primary btn-xs" id="pilih" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Pilih" >',
            'Pilih',
            '</a> '
        ].join('');
    }
    function tutupFormpopup(){
      	$('#modalTable').modal('hide'); // show bootstrap modal
		bukaModal();
	}
	
	function bukaModal(){
		
		$("#modal_formdetail").css({"overflow-y":"scroll"});
	
    }
    window.operateEventspilih = {
        'click #pilih': function (e, value, row, index) {
            cariDatapopup(row);
        }
    };

    function cariDatapopup(row){
		
		//alert(baris);
	//	 alert(row.nm_grup_tiga);
		$("#id_plan").val(row.id);
         $("#jadwal").val(row.tglmulai);
		 $("#periode").val(row.periode);
         Dataplan(row.id);
		 tutupFormpopup();
     }
	 function Dataplan(periode){
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getDataplanstandar/"+periode,
				success: function(result) {
                                if(result==""){
                                    $("#id_plan").val('');
                                    $("#id_unit").val('');
                                    $("#nm_unit").val('');
                                    $("#id_ketuatim").val('');
                                    $("#nm_karyawan").val('');
                                    $("#tglmulai").val('');
                                    $("#tglselesai").val('');
                                    $("#show_tab_1").html('');
                                    $("#show_tab_2").html('');
                                } else {
                                    $.each(result, function(key, val) {
                                    $("#id_plan").val(val.id);
                                    $("#id_unit").val(val.id_unit);
                                    $("#nm_unit").val(val.nm_unit);
                                    $("#id_ketuatim").val(val.id_ketuatim);
									$("#id_auditie").val(val.id_auditie);
                                    $("#tglmulai").val(val.tglmulai);
                                    $("#tglselesai").val(val.tglselesai);
                                    // Planstandar(val.id);
                                    // ambilDataanggota(val.id);
                                    });
                                }}
				});
		 }
  </script>
  
  
  