<?php
    ob_start();
?>
<style>
.halaman{
    margin-left:20%;
    margin-right:20px;
    margin-bottom:30px;
}
.pojok-kanan{
    right:0px;
}
.center{
    text-align: center;
}

.marg-bot-10{
    margin-bottom:10px;
}



th{
    height:30px;
    width:50%;
    text-align:center;
}

td{
    height:20px;
}
</style>

<body style="margin-left:30px;">
<h2 class="center">
<br>
<br>
<br>
SURAT TUGAS
</h2>
<div class="center">
---------------------------------------------- 
</div>
<div class="center">
    Nomor :xxx/xxxx/xxxxx/xx/2018
    <br><br>
    Tentang
    <br><br>
    AUDIT UNIT :    
    <?php 
            foreach($unit as $row2){
            echo "
            ".$row2['nm_unit']."";
            
        ?>
    <br>
    PADA PERIODE : <?php  echo "
            ".$row2['periode']."";
            
     } ?>
    <br><br>
    --oo0oo--
    <br><br>
</div>
<div class="marg-bot-10">
    Pusat Penjamin Mutu Universitas Mercu Buana, memberikan tugas kepada : 
</div>
    <table border="1" cellspacing="0" cellpadding="0" style="width:100%;">
        <tr style="background-color: #b2bec3;">
            <th>Nama</th>
            <th>Jabatan</th>
        </tr>
        <?php 
            foreach($ketuatim as $row2){
            echo "<tr>
            <td align='center'>".$row2['nm_karyawan']."</td>
            <td align='center'>"."Ketua TIm"."</td>           
            </tr>";
            }
        ?>
        <?php 
            foreach($query as $row){
            echo "<tr>
            <td align='center'>".$row['nm_karyawan']."</td>
            <td align='center'>"."Anggota"."</td>           
            </tr>";
            }
        ?>
    </table>
    <br>
    <div class="marg-bot-10">
    Tugas audit ini mencakup seluruh aspek sesuai dengan standar –standar akademik yang berlaku.
    Demikian, agar penugasan audit ini dapat dilaksanakan dengan penuh tanggungjawab. 
    </div>
    <table style="margin-left:500px;">
        <tr>
            <td></td>
            <td>
            Dikeluarkan di :  Jakarta xxxx <br>
            Pada Tanggal 	:  <?php echo date("D F Y")?> <br>
            ---------------------------------------------- <br>
            Dekan, <br><br>
            ttd<br><br>
            <u>Prof. Dr. XXXXXXXXXXXXXXXX </u><br>
            NIDN/NIK : XXXXXXX / XXXXXX <br>
            </td>
        </tr>
    </table>    
    <br>
    <br>
    <br>
    <br>
    <br>
    <div>
    <p><b>Pusat Penjamin Mutu</b><br>
     <b>KAMPUS MENARA BHAKTI</b><br>
    Jl. Raya Meruya Selatan No. 01, Kembangan, Jakarta Barat 11650 Telp.<br>
     021-5840815 / 021-5840816 (Hunting), Fax. 021-587 1312<br>
    http://www.mercubuana.ac.id, e-mail: feb@mercubuana.ac.id </p>
    </div>
<?php
    $content = ob_get_clean();

    // convert to PDF
    //$dirpdf = str_replace('audit', 'prinpdf', dirname(__FILE__));
    //require_once(dirname(__FILE__).'/html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr');
        $html2pdf->pdf->IncludeJS("print(true);");
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('print.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
?>
</body>
</div>