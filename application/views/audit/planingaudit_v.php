<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					echo aksesDetail();
					?>
					 <?php
					echo aksesHapus();
					?>
                    </div><table id="table" 
                            data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                            data-filter-control="true"
                           data-pagination="true"
                           data-url="Planingaudit/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
                            data-sort-name="id"
                            data-sort-order="desc">
                        <thead>	
                        <tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
                            <th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
                            <th data-field="nm_unit" data-halign="center" data-align="left" data-sortable="true">Unit</th>
                            <th data-field="nm_karyawan" data-halign="center" data-align="left" data-sortable="true">Ketua Tim</th>
                            <th data-field="periode" data-halign="center" data-align="center" data-sortable="true">Periode</th>
                            <th data-field="tglmulai" data-halign="center" data-align="center" data-sortable="true">Tgl Mulai</th>
                            <th data-field="tglselesai" data-halign="center" data-align="center" data-sortable="true">Tgl Selesai</th>
                        </tr>
			</thead>
                    </table>
               <!-- <a href="<?php echo base_url("audit/planingaudit/export"); ?>">
                        Export ke Excel</a> -->
            </div>
          </div>  
</div> <div class="modal fade" id="modal_formdetail" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formdetail" name="formdetail" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
                  <div class="form-group ">
                        <label class="control-label col-md-3" for1="menudes">Unit</label> 
                        <div class="col-md-9">
                            <select type="select" name="id_unit" class="form-control select2 input-sm" id="id_unit" required="required"  style="width: 100%;" >
                                <option value=''>----- Pilih -----</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="control-label col-md-3" for1="menudes">Ketua Tim Audit  </label> 
                        <div class="col-md-9">
                            <select type="select" name="id_ketuatim" class="form-control select2 input-sm" id="id_ketuatim" required="required"  style="width: 100%;" >
                                <option value=''>----- Pilih ----- </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="control-label col-md-3" for1="menudes">Periode </label> 
                        <div class="col-md-9">
                            <input name="periode" class="form-control input-sm" id="periode" required="required" type="text">
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="control-label col-md-3" for1="menudes">Tgl Mulai </label> 
                        <div class="col-md-9">
                            <input name="tglmulai" class="form-control date-picker input-sm" id="tglmulai" required="required" type="text">
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="control-label col-md-3" for1="menudes">Tgl Selesai</label> 
                        <div class="col-md-9">
                            <input name="tglselesai" class="form-control date-picker input-sm" id="tglselesai" required="required" type="text">
                        </div>
                    </div>
		 <div class="col-md-12">
			<!-- <div class="tab-pane active" id="tab_1"> -->
			<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
                              <li class="active"><a href="#tab_1" data-toggle="tab">Anggota</a></li>
                              <li><a href="#tab_2" data-toggle="tab">Standar</a></li>
							  <li><a href="#tab_3" data-toggle="tab">Auditie</a></li>
							  <li><a href="#tab_4" data-toggle="tab">Tujuan Audit</a></li>
							  <li><a href="#tab_5" data-toggle="tab">Jadwal Audit</a></li>
                            </ul>
						<!-- tab content open -->
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1">
								<div class="panel panel-default">
									<div class="panel-heading" >
							 		  	<button type="button" id="btnNewRow" name="btnNewRow" class="btn btn-xs btn-success" onclick="addRow();"><i class="fa fa-plus"></i>
										Baris Baru
										</button>
									</div>
									<div class="panel-body">
										<div class="table-responsive">
											<table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
												<thead> 
													<tr>
														<td align="center" class="ganjil">Action </td>
														<td align="center" class="ganjil">Nama Anggota</td>
													</tr>
													<tr id="bookTemplate" name="rowda" class="hide">
														<td  style="text-align: center;" valign="top">
														<button type='button' style='text-align: center;' id='btnDelRowX' name='btnDelRowX' class='btn btn-sm btn-danger btn-xs btn-round' title='Hapus' ><i class='fa fa-minus'></i></button> </td>
														<td align="center" style="text-align: center;" class="form-group " valign="top">
														<input  class='form-control input-sm '  type='hidden'  name='id_dok' id='id_dok' >	
														<select type="select" name="id_anggota" class="form-control select2 input-sm" id="id_anggota" style="width: 100%;" >
																<option <?php echo 'selected';?> value=''>--Pilih-- </option>
														</select>
														</td>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
										<!-- /.table-responsive -->
									</div>
									<!-- /.panel-body -->
								</div>
							</div><!-- tab_1 pane close -->
							<div class="tab-pane" id="tab_2">
								<div class="panel panel-default">
									<div class="panel-heading" >
									<button type="button" id="btnNewRow2" name="btnNewRow2" class="btn btn-xs btn-success" onclick="addRow2();"><i class="fa fa-plus"></i>
										Baris Baru
										</button>
									</div>
									<div class="panel-body">
										<div class="table-responsive">
											<table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
												<thead> 
													<tr>
														<td align="center" class="ganjil">Action </td>
														<td align="center" class="ganjil">Nama Standar</td>
													</tr>
													
													<tr id="bookTemplate2" name="rowda2" class="hide">
														<td  style="text-align: center;" valign="top">
														<button type='button' style='text-align: center;' id='btnDelRowX2' name='btnDelRowX2' class='btn btn-sm btn-danger btn-xs btn-round' title='Hapus' ><i class='fa fa-minus'></i></button> </td>
														
														<td align="center" style="text-align: center;" class="form-group " valign="top">
														<input  class='form-control input-sm '  type='hidden'  name='id_dokm' id='id_dokm' >	
														<select type="select" name="id_standar" class="form-control select2 input-sm" id="id_standar" style="width: 100%;" >
															<option <?php echo 'selected';?> value=''>--Pilih-- </option>
														</select>
														</td>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
										<!-- /.table-responsive -->
									</div>
									<!-- /.panel-body -->
								</div>
							</div><!-- tab_2 pane close -->
							<div class="tab-pane" id="tab_4">
								<div class="panel panel-default">
									<div class="panel-heading" >
									<button type="button" id="btnNewRow4" name="btnNewRow4" class="btn btn-xs btn-success" onclick="addRow4();"><i class="fa fa-plus"></i>
										Baris Baru
										</button>
									</div>
									<div class="panel-body">
										<div class="table-responsive">
											<table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
												<thead> 
													<tr>
														<td align="center" class="ganjil">Action </td>
														<td align="center" class="ganjil">Tujuan Audit</td>
													</tr>
													<tr id="bookTemplate4" name="rowda4" class="hide">
														<td  style="text-align: center;" valign="top">
														<button type='button' style='text-align: center;' id='btnDelRowX4' name='btnDelRowX4' class='btn btn-sm btn-danger btn-xs btn-round' title='Hapus' ><i class='fa fa-minus'></i></button> </td>
														<td align="center" style="text-align: center;" class="form-group " valign="top">
                                                        <input  class='form-control input-sm '  type='hidden'  name='id_tujuan' id='id_tujuan' >
														<input  class='form-control input-sm '  type='text'  name='tujuan' id='tujuan'>	
														</td>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
										<!-- /.table-responsive -->
									</div>
									<!-- /.panel-body -->
								</div>
							</div><!-- tab_4 pane close -->
							<div class="tab-pane" id="tab_5">
								<div class="panel panel-default">
									<div class="panel-heading" >
									<button type="button" id="btnNewRow5" name="btnNewRow5" class="btn btn-xs btn-success" onclick="addRow5();"><i class="fa fa-plus"></i>
										Baris Baru
										</button>
									</div>
									<div class="panel-body">
										<div class="table-responsive">
											<table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
												<thead> 
													<tr>
														<td align="center" class="ganjil">Action </td>
														<td align="center" class="ganjil">Tanggal</td>
														<td align="center" class="ganjil">Jam Mulai</td>
														<td align="center" class="ganjil">Jam Selesai</td>
														<td align="center" class="ganjil">Keterangan Jadwal</td>
													</tr>
												
													<tr id="bookTemplate5" name="rowda5" class="hide">
														<td  style="text-align: center;" valign="top">
														<button type='button' style='text-align: center;' id='btnDelRowX5' name='btnDelRowX5' class='btn btn-sm btn-danger btn-xs btn-round' title='Hapus' ><i class='fa fa-minus'></i></button> </td>
														<td align="center" style="text-align: center;" class="form-group " valign="top">
														<input  class='form-control input-sm '  type='hidden'  name='id_jadwal' id='id_jadwal' >
														<input  class='form-control input-sm '  type='date'  name='tanggal' id='tanggal' ></td>
														<td style="text-align: center;" valign="top"><input  class='form-control input-sm '  type='text'  name='jam_mulai' id='jam_mulai' ></td>
														<td style="text-align: center;" valign="top"><input  class='form-control input-sm '  type='text'  name='jam_selesai' id='jam_selesai' ></td>
														<td align="center" style="text-align: center;" class="form-group " valign="top"><input  class='form-control input-sm '  type='text'  name='ket_jadwal' id='ket_jadwal' ></td>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
										<!-- /.table-responsive -->
									</div>
									<!-- /.panel-body -->
								</div>
							</div><!-- tab_4 pane close -->
							<div class="tab-pane" id = "tab_3">
								<div class="panel panel-default">
									<div class="panel-heading" >
										<button type="button" id="btnNewRow3" name="btnNewRow3" class="btn btn-xs btn-success" onclick="addRow3();"><i class="fa fa-plus"></i>
											Baris Baru
										</button>
									</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
											<thead> 
												<tr>
													<td align="center" class="ganjil">Action </td>
													<td align="center" class="ganjil">Nama Auditie</td>
												</tr>
												
												<tr id="bookTemplate3" name="rowda3" class="hide">
													<td  style="text-align: center;" valign="top">
													<button type='button' style='text-align: center;' id='btnDelRowX3' name='btnDelRowX3' class='btn btn-sm btn-danger btn-xs btn-round' title='Hapus' ><i class='fa fa-minus'></i></button> </td>
													
													<td align="center" style="text-align: center;" class="form-group " valign="top">
													<input  class='form-control input-sm '  type='hidden'  name='id_dokmn' id='id_dokmn' >	
													<select type="select" name="id_auditie" class="form-control select2 input-sm" id="id_auditie" style="width: 100%;" >
														<option <?php echo 'selected';?> value=''>--Pilih-- </option>
													</select>
													</td>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
									<!-- /.table-responsive -->
								</div>
								<!-- /.panel-body -->
							</div><!-- tab_3 pane close -->
							
					  </div>
							</div>
						</div><!-- tab content close -->	
			</div>
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> 
     <script src="<?php echo base_url();?>js/atribut.js"></script>
  <script>
		$(document).ready(function ($) {
			// alert("s")
				Unit();
				KetuaTim();
				Anggota();
				Standar();
				Auditie();
		});
		$(document).ready(function ($) {
			$(".date-picker").datepicker({ autoclose: true});
			$('.date-picker').on('changeDate show', function(e) {
			$('#formdetail').bootstrapValidator('revalidateField', 'tglmulai');
			$('#formdetail').bootstrapValidator('revalidateField', 'tglselesai');
			});
		});
	
		function Unit(){
			
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getUnitdep",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_unit").append('<option value="'+val.id+'">'+val.nm_unit+'</option>');
					
				});
								  
				}
				});
		 }

        function KetuaTim(){
			$.ajax({
                        type: "POST",
                        dataType: "JSON",
                        url: "<?php echo base_url(); ?>global_combo/getKaryawan",
                        success: function (result) {
                            $.each(result, function (key, val) {
                                $("#id_ketuatim").append('<option value="' + val.id + '">' + val.nm_karyawan + '</option>');

                            });

                        }
                    });
			  
		 }
        function Standar(){
			
			$.ajax({
			type: "POST",
			dataType:"JSON",
			url: "<?php echo base_url();?>global_combo/getStandar",
			success: function(result) {  
			$.each(result, function(key, val) {	
			$("#id_standar").append('<option value="'+val.id+'">'+val.nm_dokumen+'</option>');
				
			});
							  
			}
			});
		 }
        function Anggota(){
			$.ajax({
                        type: "POST",
                        dataType: "JSON",
                        url: "<?php echo base_url(); ?>global_combo/getKaryawan",
                        success: function (result) {
                            $.each(result, function (key, val) {
                                $("#id_anggota").append('<option value="' + val.id + '">' + val.nm_karyawan + '</option>');

                            });

                        }
                    });
		 }
        function Auditie(){
			$.ajax({
                        type: "POST",
                        dataType: "JSON",
                        url: "<?php echo base_url(); ?>global_combo/getKaryawan",
                        success: function (result) {
                            $.each(result, function (key, val) {
                                $("#id_auditie").append('<option value="' + val.id + '">' + val.nm_karyawan + '</option>');

                            });

                        }
                    });
		 }
	  function editFormtambah(row){
                   $("#id_unit").val(row.id_unit);
                    $("#id_ketuatim").val(row.id_ketuatim);
                    $("#periode").val(row.periode);
                    $("#tglmulai").val(row.tglmulai);
                    $("#tglselesai").val(row.tglselesai);
		  			bersih();
					ambilDataanggota(row.id);
					ambilDatastandar(row.id);
					ambilDataauditie(row.id);
					ambilDatatujuan(row.id);
					ambilDatajadwal(row.id);
			  }
			  
          function ambilDataanggota(id){
			 var sd="id="+id;
				var i=0;
		 		$.ajax({
                    type: "GET",
                    url: '<?php echo base_url();?>audit/planingaudit/getAnggota',
                    data: sd,
                    dataType:"json",
                    success: function(result){
                        var x=0;
                        $.each(result, function(key, val) {
                        x++; 
                                addRow();
                                $('#id_anggota'+x).val(val.id_anggota);
                        });	
			}
			});
	}
        function ambilDatastandar(id){
			 var sd="id="+id;
				var i=0;
		 		$.ajax({
                    type: "GET",
                    url: '<?php echo base_url();?>audit/planingaudit/getStandar',
                    data: sd,
                    dataType:"json",
                    success: function(result){
                        var x=0;
                        $.each(result, function(key, val) {
                        x++; 
                                addRow2();
                                $('#id_standar'+x).val(val.id_standar);
                        });	
			}
			});
	}
        function ambilDataauditie(id){
			 var sd="id="+id;
				var i=0;
		 		$.ajax({
                    type: "GET",
                    url: '<?php echo base_url();?>audit/planingaudit/getAuditie',
                    data: sd,
                    dataType:"json",
                    success: function(result){
                        var x=0;
                        $.each(result, function(key, val) {
                        x++; 
                        addRow3();
						$('#id_auditie'+x).val(val.id_auditie);
						
                        });	
			}
			});
		}
		function ambilDatajadwal(id){
			 var sd="id="+id;
				var i=0;
		 		$.ajax({
                    type: "GET",
                    url: '<?php echo base_url();?>audit/planingaudit/getJadwal',
                    data: sd,
                    dataType:"json",
                    success: function(result){
                        var x=0;
                        $.each(result, function(key, val) {
                        x++; 
                        addRow5();
						$('#id_jadwal'+x).val(val.id_jadwal);
						$('#tanggal'+x).val(val.tanggal);
						$('#jam_mulai'+x).val(val.jam_mulai);
						$('#jam_selesai'+x).val(val.jam_selesai);
						$('#ket_jadwal'+x).val(val.ket_jadwal);
                        });	
			}
			});
		}
		function ambilDatatujuan(id){
			 var sd="id="+id;
				var i=0;
		 		$.ajax({
                    type: "GET",
                    url: '<?php echo base_url();?>audit/planingaudit/getTujuan',
                    data: sd,
                    dataType:"json",
                    success: function(result){
                        var x=0;
                        $.each(result, function(key, val) {
                        x++; 
                        addRow4();
						$('#id_tujuan'+x).val(val.id);
                                                $('#tujuan'+x).val(val.tujuan);
						
					    });	
			}
			});
		}
  function operateFormatter(value, row, index) {
      return [
			'<?php echo aksesUbahdetail() ?>',
			'<?php echo aksesHapussatu() ?>'
        ].join('');
    }
    
    // add dokumen
	function bersih() {
		//alert("x")
				var y = totalrow + 1;
				
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat" + x)) {
						hapusBaris("rowmat" + x);
					}
				}
				totalrow = 0;
                                var y = totalrow2 + 1;
				
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat_2" + x)) {
						hapusBaris("rowmat_2" + x);
					}
				}
				totalrow2 = 0;
                                var y = totalrow3 + 1;
				
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat_3" + x)) {
						hapusBaris("rowmat_3" + x);
					}
				}
				totalrow3 = 0;
				var y = totalrow4 + 1;
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat_4" + x)) {
						hapusBaris("rowmat_4" + x);
					}
				}
				totalrow4 = 0;
				var y = totalrow5 + 1;
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat_5" + x)) {
						hapusBaris("rowmat_5" + x);
					}
				}
				totalrow5 = 0;
			}
		
		function hapusBaris(x) {
				if (document.getElementById(x) != null) {
					
					var $row    = $(this).parents('.form-group'),
					 $option = $row.find('[name="option[]"]');
					 $('#' + x).remove();
				}
		}

		  var totalrow= 0;
          var totalrow2= 0;
          var totalrow3= 0;
		  var totalrow4=0;
		  var totalrow5=0;
		function addRow() {
			totalrow++;
			var $template = $('#bookTemplate'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow)
					.attr('id', 'rowmat'+totalrow)
					.insertBefore($template);
			$clone 
				.find('[name="btnDelRowX"]').attr('onClick','hapusBaris("rowmat' + totalrow + '")').end() 
				.find('[name="id_dok"]').attr('name', 'anggota[' + totalrow + '][id_dok]').attr('id', 'id_dok'+totalrow).end()
				.find('[name="id_anggota"]').attr('name', 'anggota[' + totalrow + '][id_anggota]').attr('id', 'id_anggota'+totalrow).end();
					
		}
        function addRow2() {
			totalrow2++;
			var $template = $('#bookTemplate2'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow2)
					.attr('id', 'rowmat_2'+totalrow2)
					.insertBefore($template);
			$clone 
				.find('[name="btnDelRowX2"]').attr('onClick','hapusBaris("rowmat_2' + totalrow2 + '")').end() 
				.find('[name="id_dok"]').attr('name', 'standar[' + totalrow2 + '][id_dokm]').attr('id', 'id_dokm'+totalrow2).end()
				.find('[name="id_standar"]').attr('name', 'standar[' + totalrow2 + '][id_standar]').attr('id', 'id_standar'+totalrow2).end();

		}
        function addRow3() {
			totalrow3++;
			var $template = $('#bookTemplate3'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow3)
					.attr('id', 'rowmat_3'+totalrow3)
					.insertBefore($template);
			$clone 
				.find('[name="btnDelRowX3"]').attr('onClick','hapusBaris("rowmat_3' + totalrow3 + '")').end() 
				.find('[name="id_dokmn"]').attr('name', 'auditie[' + totalrow3 + '][id_dokmn]').attr('id', 'id_dokmn'+totalrow3).end()
				.find('[name="id_auditie"]').attr('name', 'auditie[' + totalrow3 + '][id_auditie]').attr('id', 'id_auditie'+totalrow3).end();
		}
		function addRow4() {
			totalrow4++;
			var $template = $('#bookTemplate4'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow4)
					.attr('id', 'rowmat_4'+totalrow4)
					.insertBefore($template);
			$clone 
				.find('[name="btnDelRowX4"]').attr('onClick','hapusBaris("rowmat_4' + totalrow4 + '")').end() 
                .find('[name="id_tujuan"]').attr('name', 'tujuandet[' + totalrow4 + '][id_tujuan]').attr('id', 'id_tujuan'+totalrow4).end()
				.find('[name="tujuan"]').attr('name', 'tujuandet[' + totalrow4 + '][tujuan]').attr('id', 'tujuan'+totalrow4).end();

		}
		function addRow5() {
			totalrow5++;
			var $template = $('#bookTemplate5'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow5)
					.attr('id', 'rowmat_5'+totalrow5)
					.insertBefore($template);
			$clone 
				.find('[name="btnDelRowX5"]').attr('onClick','hapusBaris("rowmat_5' + totalrow5 + '")').end() 
				.find('[name="id_jadwal"]').attr('name', 'jadwal[' + totalrow5 + '][id_jadwal]').attr('id', 'id_jadwal'+totalrow5).end()
				.find('[name="tanggal"]').attr('name', 'jadwal[' + totalrow5 + '][tanggal]').attr('id', 'tanggal'+totalrow5).end()
				.find('[name="jam_mulai"]').attr('name', 'jadwal[' + totalrow5 + '][jam_mulai]').attr('id', 'jam_mulai'+totalrow5).end()
				.find('[name="jam_selesai"]').attr('name', 'jadwal[' + totalrow5 + '][jam_selesai]').attr('id', 'jam_selesai'+totalrow5).end()
				.find('[name="ket_jadwal"]').attr('name', 'jadwal[' + totalrow5 + '][ket_jadwal]').attr('id', 'ket_jadwal'+totalrow5).end();

		}
  </script>