<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
        </section>
    <!-- Modal PopUp Data -->
    <div class="modal fade" id="modalTable2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:70%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4>Jadwal Audit </h4>
                    </div>
                    <div class="modal-body">
                        <table id="tabledata2"
                           data-toggle="table"
                           data-search="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                           data-pagination="true"
						   data-height="500"
                           data-url="<?php echo base_url();?>audit/Persiapanpelaksanaan/loaddatajadwal2"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="asc">
                            <thead>
                            <tr>
                                                     
                            <th data-field="kts_ob"  data-halign="center" data-align="center"  data-sortable="true">KTS/OB </th>
                            <th data-field="nm_dokumen"  data-halign="center" data-align="center"  data-sortable="true">Standar </th>
                            <th data-field="pernyataan"  data-halign="center" data-align="center"  data-sortable="true">Pernyataan  </th>
                      
						    <th data-field="cari"  id="pilih2" data-halign="center" data-align="center"
                    data-formatter="operateFormatterPilih2" data-events="operateEventspilih2">Pilih Data</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->	

        <!-- Modal PopUp Data -->
        <div class="modal fade" id="modalTable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:70%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4>Jadwal Audit </h4>
                    </div>
                    <div class="modal-body">
                        <table id="tabledata"
                           data-toggle="table"
                           data-search="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                           data-pagination="true"
						   data-height="500"
                           data-url="<?php echo base_url();?>audit/Persiapanpelaksanaan/loaddatajadwal"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="asc">
                            <thead>
                            <tr>
                                                     
                            <th data-field="tglmulai"  data-halign="center" data-align="center"  data-sortable="true">Jadwal </th>
                                    <th data-field="periode"  data-halign="center" data-align="center"  data-sortable="true">Periode </th>
                                    <th data-field="nm_unit"  data-halign="center" data-align="center"  data-sortable="true">Unit  </th>
                                    <th data-field="nm_karyawan"  data-halign="center" data-align="center"  data-sortable="true">Ketua Tim Auditor  </th>
						    <th data-field="cari"  id="pilih" data-halign="center" data-align="center"
                    data-formatter="operateFormatterPilih" data-events="operateEventspilih">Pilih Data</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->	

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					echo aksesTambahhtml();
					?>
					 <?php
					echo aksesHapus();
					?>
                    </div><table id="table" 
                            data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                            data-filter-control="true"
                           data-pagination="true"
                           data-url="pemantauanptk/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
                            data-sort-name="id"
                            data-sort-order="desc">
                        <thead>	
                        <tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
                            <th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
                            <th data-field="periode" data-halign="center" data-align="center" data-sortable="true">Periode</th>
                            <th data-field="nm_unit" data-halign="center" data-align="center" data-sortable="true">Unit</th>
                            <th data-field="nm_karyawan" data-halign="center" data-align="center" data-sortable="true">Ketua Tim</th>
                            <th data-field="periode" data-halign="center" data-align="center" data-sortable="true">Periode</th>
                            <th data-field="kategori_ptk" data-halign="center" data-align="center" data-sortable="true">Kategori PTK</th>
                            <th data-field="pernyataan" data-halign="center" data-align="center" data-sortable="true">Pernyataan</th>
                        </tr>
                        </thead>
                    </table> 
            </div><!-- /.col -->
          </div>    
</div> <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div><!-- end of modal header -->
      <div class="modal-body form">
            <form  id="form" name="form" class="form-horizontal" onsubmit="return false;"     >
                <input type="hidden" value="" name="id" id="id"/> 
                <input type="hidden" value="" name="set" id="set"/> 
            <div class="form-group ">
			    <label class="control-label col-md-3" for1="menudes">Periode Audit</label> 
                <div class="col-md-6">
                    <input name="id_plan" class="form-control input-sm" id="id_plan" required="required" readonly type="hidden">
                    <input name="jadwal" class="form-control date-picker input-sm" id="jadwal" type="hidden">
                    <input name="periode" class="form-control  input-sm" id="periode" type="text">
                </div>  
                <div class="col-md-3">
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onclick="cariJadwal()"><i class="fa fa-search"></i>Cari</button>	
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label col-md-3" for1="menudes">Unit </label> 
                    <div class="col-md-9">
                        <select type="select" name="id_unit" class="form-control select2 input-sm" id="id_unit" required="required" readonly style="width: 100%;" >
                            <option value=''>----- Pilih -----</option>
                        </select>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label col-md-3" for1="menudes">Ketua Tim Auditor </label> 
                    <div class="col-md-9">
                        <select type="select" name="id_ketuatim" class="form-control select2 input-sm" id="id_ketuatim" required="required" readonly style="width: 100%;" >
                            <option value=''>----- Pilih -----</option>
                        </select>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label col-md-3" for1="menudes">Auditie  </label> 
                    <div class="col-md-9">
                    <select type="select" name="id_auditie" class="form-control select2 input-sm" id="id_auditie" required="required" readonly style="width: 100%;" >
                            <option value=''>----- Pilih -----</option>
                        </select>
                    </div>
                </div>
                <div class="form-group ">
			    <label class="control-label col-md-3" for1="menudes">KTS/OB</label> 
                <div class="col-md-6">
                    
                    <input name="kts_ob" class="form-control input-sm" id="kts_ob" required="required" type="text">
                </div>  
                <div class="col-md-3">
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onclick="cariJadwal2()"><i class="fa fa-search"></i>Cari</button>	
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label col-md-3" for1="menudes">Standar</label> 
                    <div class="col-md-9">
                    <select type="select" name="id_standar" class="form-control select2 input-sm" id="id_standar" required="required"  style="width: 100%;" >
                            <option value=''>----- Pilih -----</option>
                        </select>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label col-md-3" for1="menudes">Pernyataan</label> 
                    <div class="col-md-9">
                    <textarea name="pernyataan" class="form-control input-sm" id="pernyataan" type="text"></textarea>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label col-md-3" for1="menudes">PTK NO</label> 
                    <div class="col-md-9">
                    <input name="ptk_no" class="form-control input-sm" id="ptk_no"  type="text">
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label col-md-3" for1="menudes">Kategori PTK </label> 
                    <div class="col-md-9">
                        <input type='radio' name="kategori" id="kategori_mayor" value="Mayor" /> Mayor
                        <input type='radio' name="kategori" id="kategori_minor" value="Minor" /> Minor
                        <input type='radio' name="kategori"  id="kategori_observasi" value="Observasi" /> Observasi
                    </div>
		        </div>
                <div class="form-group ">
                    <label class="control-label col-md-3" for1="menudes">Uraian Temuan</label> 
                    <div class="col-md-9">
                    <textarea name="uraian_temuan" rows="3" class="form-control input-sm" id="uraian_temuan"  type="text"></textarea>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label col-md-3" for1="menudes">Rencana Penyelesaian</label> 
                    <div class="col-md-9">
                    <textarea name="rencana_penyelesaian" rows="3" class="form-control input-sm" id="rencana_penyelesaian"  type="text"></textarea>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label col-md-3" for1="menudes">Realisasi PTK</label> 
                    <div class="col-md-9">
                    <textarea name="realisasi" rows="3" class="form-control input-sm" id="realisasi"  type="text"></textarea>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label col-md-3" for1="menudes">Penanggung Jawab</label> 
                    <div class="col-md-9">
                    <input name="pj" class="form-control input-sm" id="pj"  type="text">
                    </div>
                </div>
            </div><!--modal body form -->
            <div class="modal-footer">
                <button type="submit" id="btnSave" class="btn btn-primary"><i class="fa fa-save"></i>Simpan</button>
                <button type="button" class="btn btn-danger" id="btnCancel" data-dismiss="modal"><i class="fa fa-repeat"></i>Batal</button>
            </div><!-- /.modal-footer -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	    </form> 
  <script src="<?php echo base_url();?>js/atribut.js"></script>
  <script>
	 $(document).ready(function ($) {
		  //alert("s")
                Auditie();
                KetuaTim();
                Ktsob();
                Unit();
                Standar();
	
              
		});
		 $(document).ready(function ($) {
		  	$(".date-picker").datepicker({ autoclose: true});
			$('.date-picker').on('changeDate show', function(e) {
            $('#form').bootstrapValidator('revalidateField', 'jadwal');
			});
		});
        function Datastandar(ktsob){
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getDataktsob/"+ktsob,
				success: function(result) {
					if(result==""){
						$("#id_standar").val('');
						
					} else {
						$.each(result, function(key, val) {
						$("#id_standar").val(val.jmb);
						
						});
					}}
				});
		 }
		function KetuaTim(){
			$.ajax({
				type: "POST",
				dataType: "JSON",
				url: "<?php echo base_url(); ?>global_combo/getKaryawan",
				success: function (result) {
					$.each(result, function (key, val) {
						$("#id_ketuatim").append('<option value="' + val.id + '">' + val.nm_karyawan + '</option>');

					});

				}
			});
		 }
		 function Ktsob(){
			$.ajax({
				type: "POST",
				dataType: "JSON",
				url: "<?php echo base_url(); ?>global_combo/getKtsob",
				success: function (result) {
					$.each(result, function (key, val) {
						$("#cari_ktsob").append('<option value="' + val.id + '">' + val.kts_ob + '</option>');

					});

				}
			});
		 }
		 function Auditie(){
			
            $.ajax({
              type: "POST",
              dataType:"JSON",
              url: "<?php echo base_url();?>global_combo/getAuditie",
              success: function(result) {  
              $.each(result, function(key, val) {	
              $("#id_auditie").append('<option value="'+val.id+'">'+val.nm_karyawan+'</option>');
              });
                                
              }
              });
       }
		function Unit(){
		
			$.ajax({
			type: "POST",
			dataType:"JSON",
			url: "<?php echo base_url();?>global_combo/getUnitdep",
			success: function(result) {  
			$.each(result, function(key, val) {	
			$("#id_unit").append('<option value="'+val.id+'">'+val.nm_unit+'</option>');
			$("#id_unitdet").append('<option value="'+val.id+'">'+val.nm_unit+'</option>');
			});
								
			}
			});
		}
        function Standar(){
			
			$.ajax({
			type: "POST",
			dataType:"JSON",
			url: "<?php echo base_url();?>global_combo/getStandar",
			success: function(result) {  
			$.each(result, function(key, val) {	
			$("#id_standar").append('<option value="'+val.id+'">'+val.nm_dokumen+'</option>');	
				});				  
				}
			});
		 }
		// function ambilDataanggota(id){
		// 	 var sd="id="+id;
		// 		var i=0;
		//  		$.ajax({
        //             type: "GET",
        //             url: '<?php echo base_url();?>audit/Persiapanpelaksanaan/getanggotaperencanaan',
        //             data: sd,
        //             dataType:"json",
        //             success: function(result){
        //                 var x=0;
        //                 $.each(result, function(key, val) {
        //                 x++; 
        //                         addRow();
        //                         $('#id_anggota'+x).val(val.id_anggota);
        //                         $('#nm_anggota'+x).val(val.nm_karyawan);
        //                 });	
		// 	}
		// 	});
		// }
	
            function Formulirstandar(formulir){
                $.ajax({
                	type: "POST",
                    dataType:"JSON",
					url: "<?php echo base_url();?>global_combo/getFormulirstandar/"+formulir,
					success: function(result) {
						if(result==""){
							$("#show_tab_1").html('');
						} else {
						var standar = '';
						var datastandar = '';
						var rowstandar = [];
						$.each(result, function(key, val) {
							if(val.id_persiapan_formulir==standar){
								rowstandar[val.id]=rowstandar[val.id]+1;
							} else {
								rowstandar[val.id]=1;
							}
							standar = val.id_persiapan_formulir;
						});
						standar = '';
						$.each(result, function(key, val) {
						if(val.nm_dokumen==null){val.nm_dokumen='';}else{}
						if(val.materi==null){val.materi='';}else{}
						if(val.document==null){val.document='';}else{}
						if(val.nm_dokumen!=standar){
							datastandar = datastandar+
								"<tr><td rowspan='"+rowstandar[val.id]+"'>"+val.nm_dokumen+
								"</td><td rowspan='"+rowstandar[val.id]+"'>"+val.materi+
								"</td><td>"+val.document+"</td></tr>";
						} else {
							datastandar = datastandar+"<tr><td>"+val.document+"</td></tr>"
						}
						standar = val.nm_dokumen;
						});
						var table = 
						'<div class="panel panel-default">'+
							'<div class="panel-body">'+
								'<div class="table-responsive">'+
										'<table class="table table-striped table-bordered table-hover">'+
										'<thead>'+
											'<tr>'+
												'<td align="center">Standar</td>'+
												'<td align="center">Materi Audit</td>'+
												'<td align="center">Document</td>'+
											'</tr>'+
										'</thead>'+
										'<tbody>'+
											datastandar+
										'</tbody>'+
									'</table>'+
								'</div>'+
							'</div>'+
						'</div>';
						$("#show_tab_1").html(table);
					}}
				});
			}  
	
          function Dataplan(periode){
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getDataplanstandar/"+periode,
				success: function(result) {
                                if(result==""){
                                    $("#id_plan").val('');
                                    $("#id_unit").val('');
                                    $("#nm_unit").val('');
                                    $("#id_ketuatim").val('');
                                    $("#nm_karyawan").val('');
                                    $("#periode").val('');
                                    $("#id_auditie").val('');
                                    $("#show_tab_1").html('');
                                    $("#show_tab_2").html('');
                                } else {
                                    $.each(result, function(key, val) {
                                    $("#id_plan").val(val.id);
                                    $("#id_unit").val(val.id_unit);
                                    $("#nm_unit").val(val.nm_unit);
                                    $("#id_ketuatim").val(val.id_ketuatim);
                                    $("#nm_karyawan").val(val.nm_karyawan);
                                    $("#id_auditie").val(val.id_auditie);
                                    
                                    Planstandar(val.id);
                                    // ambilDataanggota(val.id);
                                    });
                                }}
				});
		 }
         function Planstandar(plan){
			$.ajax({
			type: "POST",
			dataType:"JSON",
			url: "<?php echo base_url();?>global_combo/getPlanstandar/"+plan,
			success: function(result) {
				if(result==""){
					$("#show_tab_1").html('');
				} else {
					$.each(result, function(key, val) {
					Formulirstandar(val.id_standar);
				});
			}}
			});
		}
	  function editFormtambah(row){
             $("#id_plan").val(row.id);
           $("#periode").val(row.periode);
           $("#jadwal").val(row.jadwal);
		   $("#id_unit").val(row.id_unit);
		   $("#id_ketuatim").val(row.id_ketuatim);
           $("#periode").val(row.periode);
           $("#id_auditie").val(row.id_auditie);
           $("#kts_ob").val(row.ktsob);
           $("#id_standar").val(row.id_standar);
           $("#pernyataan").val(row.pernyataan);
           $("#ptk_no").val(row.ptk_no);
           $("#uraian_temuan").val(row.uraian_temuan);
           $("#rencana_penyelesaian").val(row.rencana_penyelesaian);
           $("#realisasi").val(row.realisasi_ptk);
           $("#pj").val(row.pj_ptk);
           if(row.kategori_ptk=='Minor'){
				document.getElementById("kategori_minor").checked = true;
			} 
			else if(row.kategori_ptk=='Observasi'){
				document.getElementById("kategori_observasi").checked = true;
			} 
			else if(row.kategori_ptk=='Mayor'){
				document.getElementById("kategori_mayor").checked = true;
			} 
		   bersih();
		//    Planstandar(row.id_planingaudit);
		//    ambilDataanggota(row.id_planingaudit);
	  }
  function operateFormatter(value, row, index) {
      return [
			'<?php echo aksesLihat() ?>',
			'<?php echo aksesHapussatu() ?>'
        ].join('');
    }
    function cariJadwal(){
            $('#modalTable').modal('show'); 
            $("#modalTable").css({"z-index":"1060"});
            $('html,body').scrollTop(0);
            loadDatajadwalaudit(1, 15,'desc');
    }	
    function cariJadwal2(){
            $('#modalTable2').modal('show'); 
            $("#modalTable2").css({"z-index":"1060"});
            $('html,body').scrollTop(0);
            loadDatajadwalaudit2(1, 15,'desc');
    }	
    function loadDatajadwalaudit2(number, size,order){
		var $table = $('#tabledata2');
		var offset=(number - 1) * size;
		var limit=size;
		$.ajax({
				type: "POST",
				url: "persiapanpelaksanaan/loaddatajadwal2?order="+order+"&limit="+limit+"&offset="+offset,
				dataType:"JSON",
				success: function(result){
					$table.bootstrapTable('load', result);
			
				}
			});
    }
    function tutupFormpopup(){
      	$('#modalTable').modal('hide'); // show bootstrap modal
		bukaModal();
    }
    function loadDatajadwalaudit(number, size,order){
		var $table = $('#tabledata');
		var offset=(number - 1) * size;
		var limit=size;
		$.ajax({
				type: "POST",
				url: "persiapanpelaksanaan/loaddatajadwal?order="+order+"&limit="+limit+"&offset="+offset,
				dataType:"JSON",
				success: function(result){
					$table.bootstrapTable('load', result);
			
				}
			});
    }
    function operateFormatterPilih(value, row, index) {
       return [
            '<a class="btn btn-sm btn-primary btn-xs" id="pilih" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Pilih" >',
            'Pilih',
            '</a> '
        ].join('');
    }
    function operateFormatterPilih2(value, row, index) {
       return [
            '<a class="btn btn-sm btn-primary btn-xs" id="pilih2" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Pilih2" >',
            'Pilih',
            '</a> '
        ].join('');
    }
    function tutupFormpopup(){
      	$('#modalTable').modal('hide'); // show bootstrap modal
		bukaModal();
	}
    function tutupFormpopup2(){
      	$('#modalTable2').modal('hide'); // show bootstrap modal
		bukaModal2();
	}
	
	function bukaModal(){
		
		$("#modal_form").css({"overflow-y":"scroll"});
	
    }
    function bukaModal2(){
		
		$("#modal_form").css({"overflow-y":"scroll"});
	
    }
    window.operateEventspilih = {
        'click #pilih': function (e, value, row, index) {
            cariDatapopup(row);
        }
    };
    window.operateEventspilih2 = {
        'click #pilih2': function (e, value, row, index) {
            cariDatapopup2(row);
        }
    };

    function cariDatapopup(row){
         $("#periode").val(row.periode);
         $("#id_plan").val(row.id);
         $("#jadwal").val(row.tglmulai);
         Dataplan(row.id);
        //  ambilDataanggota(row.id);
		 tutupFormpopup();
	 }
     function cariDatapopup2(row){
         $("#kts_ob").val(row.kts_ob);
         $("#id_standar").val(row.id_standar);
		 tutupFormpopup2();
	 }
	 function bersih() {
		//alert("x")
				var y = totalrow + 1;
				
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat" + x)) {
						hapusBaris("rowmat" + x);
					}
				}
				totalrow = 0;
	}
		
	function hapusBaris(x) {
			if (document.getElementById(x) != null) {
				
				var $row    = $(this).parents('.form-group'),
					$option = $row.find('[name="option[]"]');
					$('#' + x).remove();
			}
	}
	 var totalrow= 0;
	 function addRow() {
			totalrow++;
			var $template = $('#bookTemplate'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow)
					.attr('id', 'rowmat'+totalrow)
					.insertBefore($template);
			$clone 
				.find('[name="btnDelRowX"]').attr('onClick','hapusBaris("rowmat' + totalrow + '")').end() 
				.find('[name="id_anggota"]').attr('name', 'anggota[' + totalrow + '][id_anggota]').attr('id', 'id_anggota'+totalrow).end()
				.find('[name="nm_anggota"]').attr('name', 'anggota[' + totalrow + '][nm_anggota]').attr('id', 'nm_anggota'+totalrow).end();
					
		}
  </script>
  
  
  