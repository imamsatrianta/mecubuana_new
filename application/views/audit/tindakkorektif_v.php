<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
        </section>

		<!-- Modal PopUp Data -->
		<div class="modal fade" id="modalTable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog" style="width:70%">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span></button>
						<h4>Jadwal Audit </h4>
					</div>
					<div class="modal-body">
						<table id="tabledata"
						data-toggle="table"
						data-search="true"
						data-show-export="true"
						data-minimum-count-columns="2"
						data-pagination="true"
						data-height="500"
						data-url="<?php echo base_url();?>audit/tindakkorektif/loaddatajadwal"
						data-side-pagination="server"
						data-pagination="true"
						data-sort-name="id"
						data-sort-order="asc">
							<thead>
							<tr>
						<th data-field="tglmulai"  data-halign="center" data-align="center"  data-sortable="true">Jadwal </th>
						<th data-field="periode"  data-halign="center" data-align="center"  data-sortable="true">Periode </th>
						<th data-field="nm_unit"  data-halign="center" data-align="center"  data-sortable="true">Unit  </th>
						<th data-field="nm_karyawan"  data-halign="center" data-align="center"  data-sortable="true">Ketua Tim Auditor  </th>
							<th data-field="cari"  id="pilih" data-halign="center" data-align="center"
					data-formatter="operateFormatterPilih" data-events="operateEventspilih">Pilih Data</th>
							</tr>
							</thead>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->	
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					echo aksesDetail();
					?>
					 <?php
					echo aksesHapus();
					?>
                    </div><table id="table" 
                           data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                            data-filter-control="true"
                           data-pagination="true"
                           data-url="tindakkorektif/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
                            data-sort-name="id"
                            data-sort-order="desc">
                        <thead>	
                        <tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
                            <th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
                            <th data-field="nm_unit" data-halign="center" data-align="center" data-sortable="true">Unit</th>
							<th data-field="nm_karyawan" data-halign="center" data-align="center" data-sortable="true">Ketua Tim</th>
                            <th data-field="tgl_input" data-halign="center" data-align="center" data-sortable="true">Tanggal Audit</th>
                            <th data-field="kategori_ptk" data-halign="center" data-align="center" data-sortable="true">Kategori</th>
                            <th data-field="realisasi_ptk" data-halign="center" data-align="center" data-sortable="true">Realisasi PTK</th>
                        </tr>
			</thead>
                    </table>
                
              
            </div><!-- /.col -->
          </div>  
       
		
</div> 
<div class="modal fade" id="modal_formdetail" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formdetail" name="formdetail" class="form-horizontal" onsubmit="return false;"     >
		<input name="set" id="set" type="hidden">
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Periode</label> 
			<div class="col-md-7">
			<input name="id_plan" class="form-control input-sm" id="id_plan"  type="hidden">
				<input name="jadwal" class="form-control date-picker input-sm" id="jadwal"  type="hidden">
                 <input name="periode" class="form-control input-sm" id="periode" required="required" type="text">
		    </div>
            <div class="col-md-2">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onclick="cariJadwal()"><i class="fa fa-search"></i>Cari</button>	
        </div>
		</div> 
        <div class="form-group ">
		    <label class="control-label col-md-3" for1="menudes">Unit </label> 
			<div class="col-md-9">
				<select type="select" name="id_unit" class="form-control select2 input-sm" id="id_unit" required="required" readonly style="width: 100%;" >
					 <option value=''>----- Pilih -----</option>
				</select>
			</div>
		</div>
		<div class="form-group ">
		    <label class="control-label col-md-3" for1="menudes">Ketua Tim Auditor </label> 
			<div class="col-md-9">
				<select type="select" name="id_ketuatim" class="form-control select2 input-sm" id="id_ketuatim" required="required" readonly style="width: 100%;" >
					 <option value=''>----- Pilih -----</option>
				</select>
			</div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Auditie  </label> 
			<div class="col-md-9">
			<select type="select" name="id_auditie" class="form-control select2 input-sm" id="id_auditie" required="required" readonly style="width: 100%;" >
					 <option value=''>----- Pilih -----</option>
				</select>
			</div>
		</div>
        <div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Temuan KTS/OB</label> 
			<div class="col-md-9">
			<select type="select" name="cari_ktsob" class="form-control select2 input-sm" id="cari_ktsob" required="required"  style="width: 100%;" >
					 <option value=''>----- Pilih -----</option>
				</select>
			</div>
        </div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Standar</label> 
			<div class="col-md-9">
			
			<select type="select" name="id_standar" class="form-control select2 input-sm" id="id_standar" required="required"  style="width: 100%;" >
					 <option value=''>----- Pilih -----</option>
				</select>
			</div>
        </div>
		<div class="form-group ">
            <label class="control-label col-md-3" for1="menudes">Pernyataan </label> 
			<div class="col-md-9">
               <textarea class="form-control" rows="5" id="pernyataan" name="pernyataan" required="required"></textarea>
		    </div>
		</div>
		<div class="form-group ">
            <label class="control-label col-md-3" for1="menudes">PTK NO </label> 
			<div class="col-md-9">
				<input name="ptk_no" class="form-control input-sm" id="ptk_no" required="required" type="text">
		    </div>
		</div>
		<div class="form-group ">
            <label class="control-label col-md-3" for1="menudes">Kategori PTK </label> 
			<div class="col-md-9">
				<input type='radio' name="kategori" id="kategori_mayor" value="Mayor" /> Mayor
				<input type='radio' name="kategori" id="kategori_minor" value="Minor" /> Minor
				<input type='radio' name="kategori"  id="kategori_observasi" value="Observasi" /> Observasi
		    </div>
		</div>
		<div class="form-group ">
            <label class="control-label col-md-3" for1="menudes">Uraian Temuan </label> 
			<div class="col-md-9">
               <textarea class="form-control" rows="3" id="uraian_temuan" name="uraian_temuan" required="required"></textarea>
		    </div>
		</div>
		<div class="form-group ">
            <label class="control-label col-md-3" for1="menudes">Rencana Temuan </label> 
			<div class="col-md-9">
               <textarea class="form-control" rows="3" id="rencana_temuan" name="rencana_temuan" required="required"></textarea>
		    </div>
		</div>
		<div class="form-group ">
            <label class="control-label col-md-3" for1="menudes">Realisasi PTK </label> 
			<div class="col-md-9">
               <textarea class="form-control" rows="3" id="realisasi" name="realisasi" required="required"></textarea>
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Penanggung Jawab </label> 
			<div class="col-md-9">
				<input name="pj" class="form-control input-sm" id="pj" required="required" type="text">
		    </div>
		</div>
	       <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> 
     <script src="<?php echo base_url();?>js/atribut.js"></script>
  <script>
	 $(document).ready(function ($) {
		$("#periode").change(function () {
				var periode = this.value;
				Dataplan(periode);
		});

		$("#cari_ktsob").change(function () {
				var ktsob = this.value;
				Datastandar(ktsob);
		});
		 Unit();
		 Standar();
		 KetuaTim();
		 Auditie();
		 Ktsob();
		 });
		 $(document).ready(function ($) {
				$(".date-picker").datepicker({ autoclose: true});
				$('.date-picker').on('changeDate show', function(e) {
					$('#formdetail').bootstrapValidator('revalidateField', 'tglaudit');
				});
				$(".timepicker").timepicker({ autoclose: true, showInputs: false, showMeridian: false, showSeconds: true});
				$('.timepicker').on('changeTime.timepicker', function(e) {
				$('#formdetail').bootstrapValidator('revalidateField', 'jam');
		});
    	});
		function Datastandar(ktsob){
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getDataktsob/"+ktsob,
				success: function(result) {
					if(result==""){
						$("#id_standar").val('');
						
					} else {
						$.each(result, function(key, val) {
						$("#id_standar").val(val.jmb);
						
						});
					}}
				});
		 }
		function KetuaTim(){
			$.ajax({
				type: "POST",
				dataType: "JSON",
				url: "<?php echo base_url(); ?>global_combo/getKaryawan",
				success: function (result) {
					$.each(result, function (key, val) {
						$("#id_ketuatim").append('<option value="' + val.id + '">' + val.nm_karyawan + '</option>');

					});

				}
			});
		 }
		 function Ktsob(){
			$.ajax({
				type: "POST",
				dataType: "JSON",
				url: "<?php echo base_url(); ?>global_combo/getKtsob",
				success: function (result) {
					$.each(result, function (key, val) {
						$("#cari_ktsob").append('<option value="' + val.id + '">' + val.kts_ob + '</option>');

					});

				}
			});
		 }
		 function Auditie(){
			
            $.ajax({
              type: "POST",
              dataType:"JSON",
              url: "<?php echo base_url();?>global_combo/getAuditie",
              success: function(result) {  
              $.each(result, function(key, val) {	
              $("#id_auditie").append('<option value="'+val.id+'">'+val.nm_karyawan+'</option>');
              });
                                
              }
              });
       }
		function Unit(){
		
			$.ajax({
			type: "POST",
			dataType:"JSON",
			url: "<?php echo base_url();?>global_combo/getUnitdep",
			success: function(result) {  
			$.each(result, function(key, val) {	
			$("#id_unit").append('<option value="'+val.id+'">'+val.nm_unit+'</option>');
			$("#id_unitdet").append('<option value="'+val.id+'">'+val.nm_unit+'</option>');
			});
								
			}
			});
		}
        function Standar(){
			
			$.ajax({
			type: "POST",
			dataType:"JSON",
			url: "<?php echo base_url();?>global_combo/getStandar",
			success: function(result) {  
			$.each(result, function(key, val) {	
			$("#id_standar").append('<option value="'+val.id+'">'+val.nm_dokumen+'</option>');	
				});				  
				}
			});
		 }
	  function editFormtambah(row){
			$("#id_plan").val(row.id);
			$("#id_unit").val(row.id_unit);
			$("#tglaudit").val(row.tgl_audit);
			$("#jam").val(row.jam);
			$("#kegiatan").val(row.kegiatan);
			$("#jadwal").val(row.jadwal); 
			$("#periode").val(row.periode); 
			$("#id_ketuatim").val(row.id_ketuatim);
			$("#uraian_temuan").val(row.uraian_temuan);
			$("#rencana_temuan").val(row.rencana_temuan);
			$("#realisasi").val(row.realisasi_ptk);
			$("#pj").val(row.pj_ptk);
			$("#pernyataan").val(row.pernyataan);
			$("#id_auditie").val(row.id_auditie);
			$("#cari_ktsob").val(row.ktsob);
			$("#id_standar").val(row.id_standar);
		   if(row.kategori_ptk=='Minor'){
				document.getElementById("kategori_minor").checked = true;
			} 
			else if(row.kategori_ptk=='Observasi'){
				document.getElementById("kategori_observasi").checked = true;
			} 
			else if(row.kategori_ptk=='Mayor'){
				document.getElementById("kategori_mayor").checked = true;
			} 
                                 
		   bersih();
	  }

	function operateFormatter(value, row, index) {
		return [
				'<?php echo aksesUbahdetail() ?>',
				'<?php echo aksesHapussatu() ?>'
			].join('');
		}
    
    // add dokumen
		function bersih() {
		//alert("x")
				var y = totalrow + 1;
				
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat" + x)) {
						hapusBaris("rowmat" + x);
					}
				}
				totalrow = 0;
				
		}
		
		function hapusBaris(x) {
				if (document.getElementById(x) != null) {
					var $row    = $(this).parents('.form-group'),
					 $option = $row.find('[name="option[]"]');
					 $('#' + x).remove();
				}
			}

		  var totalrow= 0;

		function cariJadwal(){
            $('#modalTable').modal('show'); 
            $("#modalTable").css({"z-index":"1060"});
            $('html,body').scrollTop(0);
            loadDatajadwalaudit(1, 15,'desc');
        }	
 
		function tutupFormpopup(){
			$('#modalTable').modal('hide'); // show bootstrap modal

			bukaModal();
		}

		function loadDatajadwalaudit(number, size,order){
				var $table = $('#tabledata');
				var offset=(number - 1) * size;
				var limit=size;
				$.ajax({
						type: "POST",
						url: "laporan/loaddatajadwal?order="+order+"&limit="+limit+"&offset="+offset,
						dataType:"JSON",
						success: function(result){
						$table.bootstrapTable('load', result);
					
						}
				});
		}

		function operateFormatterPilih(value, row, index) {
		return [
				'<a class="btn btn-sm btn-primary btn-xs" id="pilih" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Pilih" >',
				'Pilih',
				'</a> '
			].join('');
		}
		function tutupFormpopup(){
			$('#modalTable').modal('hide'); // show bootstrap modal
			bukaModal();
		}
		
		function bukaModal(){
			
			$("#modal_formdetail").css({"overflow-y":"scroll"});
		
		}
		window.operateEventspilih = {
			'click #pilih': function (e, value, row, index) {
				cariDatapopup(row);
			}
		};


		function cariDatapopup(row){
			$("#id_plan").val(row.id);
			$("#jadwal").val(row.tglmulai);
			$("#periode").val(row.periode);
			Dataplan(row.id);
			tutupFormpopup();
		}

		function Dataplan(periode){
				$.ajax({
					type: "POST",
					dataType:"JSON",
					url: "<?php echo base_url();?>global_combo/getDataplanstandar/"+periode,
					success: function(result) {
									if(result==""){
										$("#id_plan").val('');
										$("#id_unit").val('');
										$("#nm_unit").val('');
										$("#id_ketuatim").val('');
										$("#nm_karyawan").val('');
										$("#tglmulai").val('');
										$("#tglselesai").val('');
										$("#show_tab_1").html('');
										$("#show_tab_2").html('');
									} else {
										$.each(result, function(key, val) {
										$("#id_plan").val(val.id);
										$("#id_unit").val(val.id_unit);
										$("#nm_unit").val(val.nm_unit);
										$("#id_ketuatim").val(val.id_ketuatim);
										$("#id_auditie").val(val.id_auditie);
										$("#tglmulai").val(val.tglmulai);
										$("#tglselesai").val(val.tglselesai);
										// Planstandar(val.id);
										// ambilDataanggota(val.id);
										});
									}}
					});
			}


  </script>
  
  
  