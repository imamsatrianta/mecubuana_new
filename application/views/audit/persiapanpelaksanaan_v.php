<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
        </section>
         <!-- Modal PopUp Data -->
        <div class="modal fade" id="modalTable2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:70%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4>Jadwal Audit </h4>
                    </div>
                    <div class="modal-body">
                        <table id="tabledata2"
                           data-toggle="table"
                           data-search="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                           data-pagination="true"
						   data-height="500"
                           data-url="<?php echo base_url();?>audit/Persiapanpelaksanaan/loaddatastandar"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="asc">
                            <thead>
                            <tr>
                                   <th data-field="nm_dokumen" data-halign="center" data-align="center" data-sortable="true">Standar</th>                  
                           
                            <th data-field="materi" data-halign="center" data-align="center" data-sortable="true">Materi</th>
                             <th data-field="document" data-halign="center" data-align="center" data-sortable="true">Document</th>
						    <th data-field="cari"  id="pilih2" data-halign="center" data-align="center"
                    data-formatter="operateFormatterPilih2" data-events="operateEventspilih2">Pilih Data</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->	
        <!-- Modal PopUp Data -->
        <div class="modal fade" id="modalTable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:70%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4>Jadwal Audit </h4>
                    </div>
                    <div class="modal-body">
                        <table id="tabledata"
                           data-toggle="table"
                           data-search="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                           data-pagination="true"
						   data-height="500"
                           data-url="<?php echo base_url();?>audit/Persiapanpelaksanaan/loaddatajadwal"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="asc">
                            <thead>
                            <tr>
                                                     
                            <th data-field="tglmulai"  data-halign="center" data-align="center"  data-sortable="true">Jadwal </th>
                            <th data-field="periode"  data-halign="center" data-align="center"  data-sortable="true">Periode </th>
                            <th data-field="nm_unit"  data-halign="center" data-align="center"  data-sortable="true">Unit  </th>
                            <th data-field="nm_karyawan"  data-halign="center" data-align="center"  data-sortable="true">Ketua Tim Auditor  </th>
						    <th data-field="cari"  id="pilih" data-halign="center" data-align="center"
                    data-formatter="operateFormatterPilih" data-events="operateEventspilih">Pilih Data</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->	

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					echo aksesTambahhtml();
					?>
					 <?php
					echo aksesHapus();
					?>
                    </div><table id="table" 
                            data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                            data-filter-control="true"
                           data-pagination="true"
                           data-url="Persiapanpelaksanaan/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
                            data-sort-name="id"
                            data-sort-order="desc">
                        <thead>	
                        <tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
                            <th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
                            <th data-field="jadwal" data-halign="center" data-align="center" data-sortable="true">Jadwal</th>
                            <th data-field="nm_unit" data-halign="center" data-align="center" data-sortable="true">Unit</th>
                            <th data-field="nm_karyawan" data-halign="center" data-align="center" data-sortable="true">Ketua Tim</th>
                            <th data-field="periode" data-halign="center" data-align="center" data-sortable="true">Periode</th>
                            <th data-field="tglmulai" data-halign="center" data-align="center" data-sortable="true">Tgl Mulai</th>
                            <th data-field="tglselesai" data-halign="center" data-align="center" data-sortable="true">Tgl Selesai</th>
                        </tr>
                        </thead>
                    </table> 
            </div><!-- /.col -->
          </div>    
</div> <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div><!-- end of modal header -->
      <div class="modal-body form">
            <form  id="form" name="form" class="form-horizontal" onsubmit="return false;"     >
                <input type="hidden" value="" name="id" id="id"/> 
                <input type="hidden" value="" name="set" id="set"/> 
                <input type="hidden" value="" name="idstandar" id="idstandar"/> 
            <div class="form-group ">
			    <label class="control-label col-md-3" for1="menudes">Periode Audit</label> 
                <div class="col-md-6">
                <input name="id_plan" class="form-control  input-sm" id="id_plan"  type="hidden">
                <input name="jadwal" class="form-control date-picker input-sm" id="jadwal"  type="hidden">
                    <input name="periode" class="form-control input-sm" id="periode" required="required" type="text">
                </div>  
                <div class="col-md-3">
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onclick="cariJadwal()"><i class="fa fa-search"></i>Cari</button>	
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label col-md-3" for1="menudes">Unit</label> 
                    <div class="col-md-9">
                        <input name="id_unit" class="form-control input-sm" id="id_unit" required="required" readonly type="hidden">
                        <input name="nm_unit" class="form-control input-sm" id="nm_unit" required="required" readonly type="text">
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label col-md-3" for1="menudes">Ketua Tim Audit  </label> 
                    <div class="col-md-9">
                    <input name="id_ketuatim" class="form-control input-sm" id="id_ketuatim" required="required" readonly type="hidden">
                        <input name="nm_karyawan" class="form-control input-sm" id="nm_karyawan" required="required" readonly type="text">
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label col-md-3" for1="menudes">Tgl Mulai </label> 
                    <div class="col-md-9">
                        <input name="tglmulai" class="form-control input-sm" id="tglmulai" required="required" readonly type="text">
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label col-md-3" for1="menudes">Tgl Selesai</label> 
                    <div class="col-md-9">
                        <input name="tglselesai" class="form-control input-sm" id="tglselesai" required="required" readonly type="text">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#show_tab_3" data-toggle="tab">Standar</a></li>
                            <li><a href="#show_tab_2" data-toggle="tab">Anggota</a></li>
                        </ul>
                        <div class="tab-content"> <!-- tab content open -->
                            <div class="tab-pane active" id="show_tab_3">
                            <div class="panel-heading" >
							<button type="button" id="btnNewRow4" name="btnNewRow4" class="btn btn-xs btn-success" onclick="addRow();"><i class="fa fa-plus"></i>
								Baris Baru
								</button>
							</div>
                                <div class="panel panel-default">
										<div class="panel-body">
											<div class="table-responsive">
                                            <!-- <table id="grakses" class="easyui-datagrid" style="width:757px;height:350px" toolbar="#tbakses"
                                                rownumbers="true" pagination="true" singleSelect="true" >
                                                    
                                            </table> -->
												<table class="table table-striped table-bordered table-hover" id="tabelstandar">
													<thead> 
														<tr>
                                                            <td align="center" class="ganjil">Action</td>
                                                            <td align="center" class="ganjil">Cari</td>
															<td align="center" class="ganjil">Standar</td>
                                                            <td align="center" class="ganjil">Materi Audit</td>
                                                            <td align="center" class="ganjil">Document</td>
														</tr>
														<tr id="bookTemplate2" name="rowda2" class="hide">
                                                            <td><button type='button' style='text-align: center;' id='btnDelRowX3' name='btnDelRowX3' class='btn btn-sm btn-danger btn-xs btn-round' title='Hapus' ><i class='fa fa-minus'></i></button> </td></td>
                                                            <td  align="center"> <button type="button" class="btn btn-primary btn-sm" id='btnDelRowX4' name='btnDelRowX4' data-toggle="modal" onclick="caristandar()"><i class="fa fa-search"></i>Cari</button>	</td>
                                                             <input  class='form-control input-sm '  type='hidden' readonly name='idstn' id='idstn' >
                                                            <td  align="center"><input  class='form-control input-sm '  type='text' readonly name='crstandar' id='crstandar' ></td>
                                                            <td  align="center"><input  class='form-control input-sm '  type='text' readonly name='crmateri' id='crmateri' ></td>
                                                            <td  align="center"><input  class='form-control input-sm '  type='text' readonly name='crdocument' id='crdocument' >
															</td>   
														</tr>
													</thead>
													<tbody>
													</tbody>
												</table>
											</div><!-- /.table-responsive -->	
										</div><!-- /.panel-body -->
								</div><!-- /.panel-default -->
                            </div>
                            <div class="tab-pane" id="show_tab_2">
								<div class="panel panel-default">
										<div class="panel-body">
											<div class="table-responsive">
												<table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
													<thead> 
														<tr>
															<td align="center" class="ganjil">Nama Anggota</td>
														</tr>
														<tr id="bookTemplate" name="rowda" class="hide">
															<td align="center" style="text-align: center;" class="form-group " valign="top">
                                                            <input  class='form-control input-sm '  type='hidden' readonly name='id_anggota' id='id_anggota' >
                                                            <input  class='form-control input-sm '  type='text' readonly name='nm_anggota' id='nm_anggota' >	
															
															</td>
														</tr>
													</thead>
													<tbody>
													</tbody>
												</table>
											</div><!-- /.table-responsive -->	
										</div><!-- /.panel-body -->
								</div><!-- /.panel-default -->
                            </div><!-- tab pane 2 close -->
                        </div><!-- tab content close -->
                    </div>   
                </div>
            </div><!--modal body form -->
            <div class="modal-footer">
                <button type="submit" id="btnSave" class="btn btn-primary"><i class="fa fa-save"></i>Simpan</button>
                <button type="button" class="btn btn-danger" id="btnCancel" data-dismiss="modal"><i class="fa fa-repeat"></i>Batal</button>
            </div><!-- /.modal-footer -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	    </form> 
  <script src="<?php echo base_url();?>js/atribut.js"></script>
  
  <script>
 		 $(document).ready(function ($) {
		  	$(".date-picker").datepicker({ autoclose: true});
			$('.date-picker').on('changeDate show', function(e) {
            $('#form').bootstrapValidator('revalidateField', 'periode');
			});
		});
        function ambilDatastandar(id){
                    var sd="id="+id;
                        var i=0;
                        $.ajax({
                            type: "GET",
                            url: '<?php echo base_url();?>audit/Persiapanpelaksanaan/getstandarperencanaan',
                            data: sd,
                            dataType:"json",
                            success: function(result){
                                var x=0;
                                $.each(result, function(key, val) {
                                x++; 
                                        addRow();
                                        $('#idstn'+x).val(val.id_standar);
                                        $('#crstandar'+x).val(val.nm_dokumen);
                                        $('#crmateri'+x).val(val.materi);
                                        $('#crdocument'+x).val(val.document);
                                });	
                    }
                    });
		}	
		function ambilDataanggota(id){
			 var sd="id="+id;
				var i=0;
		 		$.ajax({
                    type: "GET",
                    url: '<?php echo base_url();?>audit/Persiapanpelaksanaan/getanggotaperencanaan',
                    data: sd,
                    dataType:"json",
                    success: function(result){
                        var x=0;
                        $.each(result, function(key, val) {
                        x++; 
                                addRow2();
                                $('#id_anggota'+x).val(val.id_anggota);
                                $('#nm_anggota'+x).val(val.nm_karyawan);
                        });	
			}
			});
		}	
          function Dataplan(periode){
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getDataplan/"+periode,
				success: function(result) {
                    if(result==""){
                        $("#id_plan").val('');
                        $("#id_unit").val('');
                        $("#nm_unit").val('');
                        $("#id_ketuatim").val('');
                        $("#nm_karyawan").val('');
                        $("#periode").val('');
                        $("#tglmulai").val('');
                        $("#tglselesai").val('');
                        $("#show_tab_1").html('');
                        $("#show_tab_2").html('');
                    } else {
                        $.each(result, function(key, val) {

                        $("#id_plan").val(val.id);
                        $("#id_unit").val(val.id_unit);
                        $("#nm_unit").val(val.nm_unit);
                        $("#id_ketuatim").val(val.id_ketuatim);
                        $("#nm_karyawan").val(val.nm_karyawan);
                        $("#jadwal").val(val.jadwal);
                        $("#tglmulai").val(val.tglmulai);
                        $("#tglselesai").val(val.tglselesai);
                        $("#jadwal").val(val.tglmulai);
                        Planstandar(val.id);
                        ambilDataanggota(val.id);
                        });
                    }}
				});
		 }
         function Planstandar(plan){
			$.ajax({
			type: "POST",
			dataType:"JSON",
			url: "<?php echo base_url();?>global_combo/getPlanstandar/"+plan,
			success: function(result) {
				if(result==""){
					$("#show_tab_1").html('');
				} else {
					$.each(result, function(key, val) {
					// Formulirstandar(val.id_standar);
				});
			}}
			});
		}
	  function editFormtambah(row){
           $("#jadwal").val(row.jadwal);
           $("#periode").val(row.periode);
		   $("#id_unit").val(row.id_unit);
		   $("#id_ketuatim").val(row.id_ketuatim);
		   $("#tglmulai").val(row.tglmulai);
		   $("#tglselesai").val(row.tglselesai);
		//     var idstandar=$("#idstandar").val();
		// //  alert(idstandar);
        //  $("#crstandar"+idstandar).val(row.nm_dokumen);
        //  $("#crmateri"+idstandar).val(row.materi);
        //  $("#crdocument"+idstandar).val(row.document);
         bersih();
           ambilDatastandar(row.id_planingaudit)
		   Planstandar(row.id_planingaudit);
		   ambilDataanggota(row.id_planingaudit);
          
	  }
  function operateFormatter(value, row, index) {
      return [
			'<?php echo aksesLihat() ?>',
			'<?php echo aksesHapussatu() ?>'
        ].join('');
    }
    function cariJadwal(){
            $('#modalTable').modal('show'); 
            $("#modalTable").css({"z-index":"1060"});
            $('html,body').scrollTop(0);
            loadDatajadwalaudit(1, 15,'desc');
    }
    function caristandar(row,idstandar){
			$("#idstandar").val(idstandar);
            $('#modalTable2').modal('show'); 
            $("#modalTable2").css({"z-index":"1060"});
            $('html,body').scrollTop(0);
            loadDatastandar(1, 15,'desc');
    }
    function tutupFormpopup(){
      	$('#modalTable').modal('hide'); // show bootstrap modal
		bukaModal();
    }
    function tutupFormpopup2(){
      	$('#modalTable2').modal('hide'); // show bootstrap modal
		bukaModal2();
    }
    function loadDatastandar(number, size,order){
		var $table = $('#tabledata2');
		var offset=(number - 1) * size;
		var limit=size;
		$.ajax({
				type: "POST",
				url: "persiapanpelaksanaan/loaddatastandar?order="+order+"&limit="+limit+"&offset="+offset,
				dataType:"JSON",
				success: function(result){
					$table.bootstrapTable('load', result);
			
				}
			});
    }
    function loadDatajadwalaudit(number, size,order){
		var $table = $('#tabledata');
		var offset=(number - 1) * size;
		var limit=size;
		$.ajax({
				type: "POST",
				url: "persiapanpelaksanaan/loaddatajadwal?order="+order+"&limit="+limit+"&offset="+offset,
				dataType:"JSON",
				success: function(result){
					$table.bootstrapTable('load', result);
			
				}
			});
    }
    function operateFormatterPilih(value, row, index) {
       return [
            '<a class="btn btn-sm btn-primary btn-xs" id="pilih" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Pilih" >',
            'Pilih',
            '</a> '
        ].join('');
    }
    function operateFormatterPilih2(value, row, index) {
       return [
            '<a class="btn btn-sm btn-primary btn-xs" id="pilih2" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Pilih" >',
            'Pilih',
            '</a> '
        ].join('');
    }
  
	function bukaModal2(){
		
		$("#modal_form").css({"overflow-y":"scroll"});
	
    }
	function bukaModal(){
		
		$("#modal_form").css({"overflow-y":"scroll"});
	
    }
    window.operateEventspilih = {
        'click #pilih': function (e, value, row, index) {
            cariDatapopup(row);
        }
    };
    window.operateEventspilih2 = {
        'click #pilih2': function (e, value, row, index) {
            cariDatapopup2(row);
        }
    };

    function cariDatapopup(row){
         $("#periode").val(row.periode);
         $("#id_plan").val(row.id);
         $("#jadwal").val(row.tglmulai);
         Dataplan(row.id);
         ambilDataanggota(row.id);
		 tutupFormpopup();
	 }
   
    function cariDatapopup2(row){
		 var idstandar=$("#idstandar").val();
		//  alert(idstandar);
         $("#idstn"+idstandar).val(row.id);
         $("#crstandar"+idstandar).val(row.nm_dokumen);
         $("#crmateri"+idstandar).val(row.materi);
         $("#crdocument"+idstandar).val(row.document);
        //  Dataplan(row.id);
        //  ambilDataanggota(row.id);
		 tutupFormpopup2();
     
	 }

	 function bersih() {
		//alert("x")
				var y = totalrow + 1;
				
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat" + x)) {
						hapusBaris("rowmat" + x);
					}
				}
				totalrow = 0;

                var y = totalrow2 + 1;
				
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat_2" + x)) {
						hapusBaris("rowmat_2" + x);
					}
				}
				totalrow2 = 0;
	}
		
	function hapusBaris(x) {
			if (document.getElementById(x) != null) {
				
				var $row    = $(this).parents('.form-group'),
					$option = $row.find('[name="option[]"]');
					$('#' + x).remove();
			}
	}
	 var totalrow = 0;
     var totalrow2= 0;

	 function addRow() {
			totalrow++;
			var $template = $('#bookTemplate2'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow)
					.attr('id', 'rowmat'+totalrow)
					.insertBefore($template);
			$clone 
                .find('[name="btnDelRowX3"]').attr('onClick','hapusBaris("rowmat' + totalrow + '")').end() 
				.find('[name="btnDelRowX4"]').attr('onClick','caristandar("rowmat' + totalrow + '",'+totalrow+')').attr('id', 'caristandar'+totalrow).end() 
				.find('[name="idstn"]').attr('name', 'standar[' + totalrow + '][idstn]').attr('id', 'idstn'+totalrow).end()
                .find('[name="crstandar"]').attr('name', 'standar[' + totalrow + '][crstandar]').attr('id', 'crstandar'+totalrow).end()
                .find('[name="crmateri"]').attr('name', 'standar[' + totalrow + '][crmateri]').attr('id', 'crmateri'+totalrow).end()
                .find('[name="crdocument"]').attr('name', 'standar[' + totalrow + '][crdocument]').attr('id', 'crdocument'+totalrow).end();
					
		}
    function addRow2() {
			totalrow2++;
			var $template = $('#bookTemplate'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow2)
					.attr('id', 'rowmat_2'+totalrow2)
					.insertBefore($template);
			$clone 
				// .find('[name="btnDelRowX"]').attr('onClick','hapusBaris("rowmat_2' + totalrow2 + '")').end() 
				.find('[name="id_anggota"]').attr('name', 'anggota[' + totalrow2 + '][id_anggota]').attr('id', 'id_anggota'+totalrow2).end()
				.find('[name="nm_anggota"]').attr('name', 'anggota[' + totalrow2 + '][nm_anggota]').attr('id', 'nm_anggota'+totalrow2).end();

		}
  </script>
  
  
  