
	<style>
.hiddenfilez{
	 width: 0px;

 height: 0px;

 overflow: hidden;
}

</style>

			<div class="col-md-10" style="background-color: #fff;height:auto;min-height:600px;margin-bottom:-15px;margin-top:-15px;">
				<section class="content-header">
				<h1>
				<div class="caption">
					<i class="fa fa-plus-square-o font-blue-chambray"></i>
					<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
					Borang 3A Standar Tiga (3.3.2)
					
					</span>
				</div>
				</h1>
				 
				</section>
		
					<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					$parameter=$_GET['parameter'];
					if($parameter<>0){
						echo aksesTambahborang();
					}
					
					?>
					 <?php
					 $parameter=$_GET['parameter'];
					if($parameter<>0){
						echo aksesHapusborang();
					}
					?>
                    </div><table id="table" 
					data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
						   data-filter-control="true"
                           data-pagination="true"
						    data-query-params="getParamstandar"
                           data-url="tiga_tiga_dua/loaddataTabel"
						   data-detail-view="true"
						   data-detail-formatter="operateDetail"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="desc">
                        <thead>	
						<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th> 
							<?php
							$parameter=$_GET['parameter'];
							if($parameter<>0){
							?>
							<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatterborang" data-events="operateEventsborang">Action</th>
							<?php } ?>
							
							<th data-field="nm_tahun_akademik"  data-halign="center" data-align="left"  data-sortable="true" >Tahun Akademik  </th>
							<th data-field="nm_jenjangprodi"  data-halign="center" data-align="left"  data-sortable="true" >Jenjang Prodi  </th>
							<th data-field="nm_prodi"  data-halign="center" data-align="left"  data-sortable="true" >Nama Prodi  </th>
							<th data-field="status"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="statusBorang">Status Evaluasi </th>
							 <th data-field="status_evaluasi"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="statusEvaluasi">Status Data </th>
							 <th data-field="note_status"  data-halign="center" data-align="left"  data-sortable="true" >Catatan Evaluasi </th>
                        </tr>
						</thead>
                    </table>
			</div>
		
		
		</div>
 </section>

</div>




<div class="modal fade" id="modal_formbor" role="dialog">
   <div class="modal-dialog" style="width:90%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formbor" name="formbor" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
        <input type="hidden" value="" name="id_parameter" id="id_parameter"/> 
		<input type="hidden" value="" name="id_jenjangprodi" id="id_jenjangprodi"/> 
		<div class="row">
			<div class="col-md-6">
				<div class="form-group " id="jab">
					<label class="control-label col-md-4" for1="menudes">Tahun Akademik  </label> 
					<div class="col-md-6">
						<select type="select" name="id_tahunakademik" class="form-control select2 input-sm" id="id_tahunakademik" required="required"  style="width: 100%;" >
							 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
						</select>
					</div>				
				</div>
				<div class="form-group " id="jab">
					<label class="control-label col-md-4" for1="menudes">Prodi  </label> 
					<div class="col-md-8">
						<select type="select" name="id_prodi" class="form-control select2 input-sm" id="id_prodi" required="required"  style="width: 100%;" >
							 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Tanggal  Pengisian</label> 
					<div class="col-md-3">
						<input type="text" class="form-control date-picker input-sm" id="tanggal" name="tanggal"  placeholder="yyyy-mm-dd" style="width: 100%;" >
					</div>
				</div>
				
			</div>
			<div class="col-md-12">
				<div class="tab-pane active" id="tab_1">
						
						<div class="panel panel-default">
							<div class="panel-heading" >
							   <button type="button" id="btnNewRow" name="btnNewRow" class="btn btn-xs btn-success" onclick="getData();"><i class="fa fa-plus"></i>
								Ambil Data SIA
								</button>
							</div>
							
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
										<thead> 
											<tr>
												<td align="center" class="ganjil" rowspan="2" width="3%">No </td>
												<td align="center" class="ganjil" rowspan="2" width="30%">Jenis Kemampuan </td>
												<td align="center" class="ganjil" colspan="4" width="48%">Tanggapan Pihak Pengguna </td>
												<td align="center" class="ganjil" rowspan="2" width="20%">Pemanfaatan Hasil Pelacakan  </td>
												
											</tr>
											<tr>
												<td align="center" class="ganjil">Sangat Baik</td>
												<td align="center" class="ganjil">Baik</td>
												<td align="center" class="ganjil">Cukup</td>
												<td align="center" class="ganjil">Kurang</td>
												
											</tr>
											
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="1"  name='detail[1][no]'  id='no1' readonly >	
												</td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="Integritas (etika dan moral)"  name='detail[1][jenis_kemampuan]' id='jenis_kemampuan1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][sangat_baik]' id='sangat_baik1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][baik]' id='baik1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][cukup]' id='cukup1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][kurang]' id='kurang1' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[1][hasil_pelacakan]' id='hasil_pelacakan1' >	
												</td>
												
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="2"  name='detail[2][no]'  id='no2' readonly >	
												</td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="Keahlian berdasarkan bidang ilmu (profesionalisme)"  name='detail[2][jenis_kemampuan]' id='jenis_kemampuan2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][sangat_baik]' id='sangat_baik2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][baik]' id='baik2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][cukup]' id='cukup2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][kurang]' id='kurang2' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[2][hasil_pelacakan]' id='hasil_pelacakan2' >	
												</td>
												
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="3"  name='detail[3][no]'  id='no3' readonly >	
												</td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="Keluasan wawasan antar disiplin ilmu"  name='detail[3][jenis_kemampuan]' id='jenis_kemampuan3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][sangat_baik]' id='sangat_baik3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][baik]' id='baik3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][cukup]' id='cukup13' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][kurang]' id='kurang3' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[3][hasil_pelacakan]' id='hasil_pelacakan3' >	
												</td>
												
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="4"  name='detail[4][no]'  id='no4' readonly >	
												</td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="Kepemimpinan"  name='detail[4][jenis_kemampuan]' id='jenis_kemampuan4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][sangat_baik]' id='sangat_baik4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][baik]' id='baik4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][cukup]' id='cukup4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][kurang]' id='kurang4' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[4][hasil_pelacakan]' id='hasil_pelacakan4' >	
												</td>
												
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="5"  name='detail[5][no]'  id='no5' readonly >	
												</td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="Kerjasama dalam tim"  name='detail[5][jenis_kemampuan]' id='jenis_kemampuan5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][sangat_baik]' id='sangat_baik5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][baik]' id='baik5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][cukup]' id='cukup5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][kurang]' id='kurang5' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[5][hasil_pelacakan]' id='hasil_pelacakan5' >	
												</td>
												
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="6"  name='detail[6][no]'  id='no6' readonly >	
												</td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="Bahasa asing"  name='detail[6][jenis_kemampuan]' id='jenis_kemampuan6' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[6][sangat_baik]' id='sangat_baik6' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[6][baik]' id='baik6' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[6][cukup]' id='cukup6' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[6][kurang]' id='kurang6' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[6][hasil_pelacakan]' id='hasil_pelacakan6' >	
												</td>
												
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="7"  name='detail[7][no]'  id='no7' readonly >	
												</td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="Komunikasi"  name='detail[7][jenis_kemampuan]' id='jenis_kemampuan7' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[7][sangat_baik]' id='sangat_baik7' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[7][baik]' id='baik7' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[7][cukup]' id='cukup7' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[7][kurang]' id='kurang7' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[7][hasil_pelacakan]' id='hasil_pelacakan7' >	
												</td>
												
											</tr>
											
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="8"  name='detail[8][no]'  id='no8' readonly >	
												</td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="Komunikasi"  name='detail[8][jenis_kemampuan]' id='jenis_kemampuan8' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[8][sangat_baik]' id='sangat_baik8' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[8][baik]' id='baik8' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[8][cukup]' id='cukup8' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[8][kurang]' id='kurang8' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[8][hasil_pelacakan]' id='hasil_pelacakan8' >	
												</td>
												
											</tr>
											<tr id="bookTemplate" name="rowda" >
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="9"  name='detail[9][no]'  id='no9' readonly >	
												</td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text' value="Komunikasi"  name='detail[9][jenis_kemampuan]' id='jenis_kemampuan9' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[9][sangat_baik]' id='sangat_baik9' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[9][baik]' id='baik9' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[9][cukup]' id='cukup9' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[9][kurang]' id='kurang9' >	
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">
												<input  class='form-control input-sm '  type='text'  name='detail[9][hasil_pelacakan]' id='hasil_pelacakan9' >	
												</td>
												
											</tr>
											
									
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
								<!-- /.table-responsive -->
								
							</div>
							<!-- /.panel-body -->
						</div>
					</div>
			
			</div>
			
		</div>
		
		
		  
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> 
	
	

		<div class="modal fade" id="modal_formref" role="dialog">
  <div class="modal-dialog" style="width:60%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formref" name="formref" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id_ref" id="id_ref"/> 
		  <input type="hidden" value="" name="id_prodi_ref" id="id_prodi_ref"/> 
		  <div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Status Evaluasi  </label> 
			<div class="col-md-5">
				<select type="select" name="status" class="form-control text-input input-sm" id="status"  style="width: 100%;" >
			
					  <option value="">--Pilih--</option>
					  <option value="3">Dikembalikan</option>
					  <option value="2">Disetujui</option>
					  </select>
				
		    </div>
		</div>
		
	
		
     
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Keterangan  </label> 
			<div class="col-md-9">
				<textarea class="form-control" rows="5" id="note_status" name="note_status"></textarea>
		    </div>
		</div>
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSaveref" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
		<!--- kebijakan-->
	
		<div class="modal fade" id="modal_formsit" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formsit" name="formsit" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id_borang" id="id_borang"/> 
		  <input type="hidden" value="" name="setsit" id="setsit"/> 
          <input type="hidden" value="" name="id_keb" id="id_keb"/> 
		  
		  <div class="form-body">
		  
		  <div class="form-group field-username">
			  <label class="control-label col-md-3" for1="username">No  </label>
			  <div class="col-md-2">
			  <input  name="no" class="form-control text-input input-sm" id="no" required="required" type="text">
			  </div>
		  </div>
		  <div class="form-group field-username">
			  <label class="control-label col-md-3" for1="username">Judul Lampiran  </label>
			  <div class="col-md-9">
			  <textarea class="form-control" rows="5" id="judul_lampiran" name="judul_lampiran"></textarea>
			  </div>
		  </div>
		  
		 <div class="form-body">
			  <div class="form-group field-username">
				  <label class="control-label col-md-3" for1="username">Lampiran   </label> 
				  <div class="col-md-9">
					
					<a id="test" onclick="takenFile()"> 
					<embed src="" width="200px"  value="Ambil Lampiran" height="200px" id="pic" >Upload</embed>
					
					</a>
					<div class="hiddenfilez"><input name="imgdata"  id="imgdata"  onchange="loadFile(event)" type="file"></div>
					
				  </div>
			  </div>
          </div>
		
			
          </div>
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSavesit" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" id="btlset" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
  <script src="<?php echo base_url();?>js/atribut.js"></script>