

<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					echo aksesTambah();
					?>
					 <?php
					echo aksesHapus();
					?>
                    </div><table id="table" 
					data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
						   data-filter-control="true"
                           data-pagination="true"
                           data-url="identitasborang/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="desc"
						   data-detail-view="true"
						   data-detail-formatter="operateDetail">
                        <thead>	
						<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
							<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
							<th data-field="nm_jenjangprodi"  data-halign="center" data-align="left"  data-sortable="true" >Jenjang Prodi  </th>
							<th data-field="nm_prodi"  data-halign="center" data-align="left"  data-sortable="true" >Nama Prodi  </th>
                             <th data-field="nm_perguruantingi"  data-halign="center" data-align="left"  data-sortable="true" >Nama Perguruan Tinggi  </th>
							 <th data-field="no_sk_pendirian"  data-halign="center" data-align="left"  data-sortable="true" >No SK Pendirian </th>
							 <th data-field="tgl_sk_pendirian"  data-halign="center" data-align="left"  data-sortable="true" >Tgl SK Pendirian  </th>
							 <th data-field="pejabat_penandatangan_sk"  data-halign="center" data-align="left"  data-sortable="true" >Pejabat Penandatangan SK  </th>
							 <th data-field="bulan_tahun_peneyelengara"  data-halign="center" data-align="left"  data-sortable="true" >Bulan Tahun Penyelengaraan  </th>
							 <th data-field="no_skijin_op"  data-halign="center" data-align="left"  data-sortable="true" >No SK Ijin Operasional  </th>
							 <th data-field="tgl_sk_op"  data-halign="center" data-align="left"  data-sortable="true" >Tanggal SK Operasional  </th>
							 <th data-field="peringkat_ak_terakhir"  data-halign="center" data-align="left"  data-sortable="true" >Peringkat Akreditasi Terakhir  </th>
							 <th data-field="no_sk_banpt"  data-halign="center" data-align="left"  data-sortable="true" >No SK BANPT  </th>
							 <th data-field="alamat_ps"  data-halign="center" data-align="left"  data-sortable="true" >Alamat PS  </th>
							 <th data-field="no_telp_ps"  data-halign="center" data-align="left"  data-sortable="true" >NO Telp PS  </th>
							 <th data-field="no_fak_ps"  data-halign="center" data-align="left"  data-sortable="true" >NO Fax PS  </th>
							 <th data-field="hp_dan_email_ps"  data-halign="center" data-align="left"  data-sortable="true" >HP dan Email PS  </th>
                        </tr>
						</thead>
                    </table>
                
              
            </div><!-- /.col -->
          </div>  
       
		
</div> <div class="modal fade" id="modal_form" role="dialog">
   <div class="modal-dialog" style="width:90%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="form" name="form" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
        
		<div class="row">
			<div class="col-md-6">
				<div class="form-group " id="jab">
					<label class="control-label col-md-4" for1="menudes">Jenjang Prodi  </label> 
					<div class="col-md-8">
						<select type="select" name="id_jenjang" class="form-control select2 input-sm" id="id_jenjang" required="required"  style="width: 100%;" onchange="getProdi('0')">
							 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
						</select>
					</div>
					 
									
				</div>
				
				<div class="form-group " id="jab">
					<label class="control-label col-md-4" for1="menudes">Prodi  </label> 
					<div class="col-md-8">
						<select type="select" name="id_prodi" class="form-control select2 input-sm" id="id_prodi" required="required"  style="width: 100%;" >
							 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
						</select>
					</div>
				</div>
				
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Tanggal  </label> 
					<div class="col-md-8">
						<input type="text" class="form-control date-picker input-sm" id="tgl" name="tgl"  placeholder="yyyy-mm-dd" style="width: 100%;" >
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Nama Perguruan Tinggi  </label> 
					<div class="col-md-8">
						<input name="nm_perguruantingi" class="form-control input-sm" id="nm_perguruantingi"  type="text">
					</div>
				</div>
				
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Nomor SK Pendirian</label> 
					<div class="col-md-8">
						<input name="no_sk_pendirian" class="form-control input-sm" id="no_sk_pendirian"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Tanggal SK Pendirian  </label> 
					<div class="col-md-8">
						<input type="text" class="form-control date-picker input-sm" id="tgl_sk_pendirian" name="tgl_sk_pendirian"  placeholder="yyyy-mm-dd" style="width: 100%;" >
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Pejabat Penandatangan SK  </label> 
					<div class="col-md-8">
						<input name="pejabat_penandatangan_sk" class="form-control input-sm" id="pejabat_penandatangan_sk"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Bulan Tahun Penyelengaraan  </label> 
					<div class="col-md-8">
						<input name="bulan_tahun_peneyelengara" class="form-control input-sm" id="bulan_tahun_peneyelengara"  type="text">
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">No SK Ijin Operasional  </label> 
					<div class="col-md-8">
						<input name="no_skijin_op" class="form-control input-sm" id="no_skijin_op"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Tanggal SK Operasional  </label> 
					<div class="col-md-8">
						<input type="text" class="form-control date-picker input-sm" id="tgl_sk_op" name="tgl_sk_op"  placeholder="yyyy-mm-dd" style="width: 100%;" >
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Peringkat Nilai Akreditasi Terakhir  </label> 
					<div class="col-md-8">
						<input name="peringkat_ak_terakhir" class="form-control input-sm" id="peringkat_ak_terakhir"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">No SK BANPT  </label> 
					<div class="col-md-8">
						<input name="no_sk_banpt" class="form-control input-sm" id="no_sk_banpt"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Alamat PS  </label> 
					<div class="col-md-8">
						<input name="alamat_ps" class="form-control input-sm" id="alamat_ps"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">No Telp PS  </label> 
					<div class="col-md-8">
						<input name="no_telp_ps" class="form-control input-sm" id="no_telp_ps"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">No Fax PS  </label> 
					<div class="col-md-8">
						<input name="no_fak_ps" class="form-control input-sm" id="no_fak_ps"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">NP dan Email PS </label> 
					<div class="col-md-8">
						<input name="hp_dan_email_ps" class="form-control input-sm" id="hp_dan_email_ps"  type="text">
					</div>
				</div>
			</div>
		</div>
		
		
		  
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> 
	
	
	<div class="modal fade" id="modal_formkeg" role="dialog">
   <div class="modal-dialog" style="width:50%">
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formkeg" name="formkeg" class="form-horizontal" onsubmit="return false;"     >
		<input type="hidden" value="" name="id_iddos" id="id_iddos"/> 
          <input type="hidden" value="" name="id_identitas" id="id_identitas"/> 
          <input type="hidden" value="" name="setkeg" id="setkeg"/> 
		 
		
		  <div class="form-body">
			  <div class="form-group field-username">
				  <label class="control-label col-md-4" for1="username">Nama Dosen </label> 
				  <div class="col-md-8">
				  <select type="select" name="id_dosen" class="form-control text-input input-sm" id="id_dosen"  style="width: 100%;"  >
					  <option value="">--Pilih--</option>
				   </select>
				
				  </div>
			  </div>
          </div>
		  <div class="form-group ">
			<label class="control-label col-md-4" for1="menudes">Tanggal Pengisian  </label> 
			<div class="col-md-8">
				<input type="text" class="form-control date-picker input-sm" id="tgl_pengisian" name="tgl_pengisian"  placeholder="yyyy-mm-dd" style="width: 100%;" >
			</div>
		</div>
		
		  </div>
          <div class="modal-footer">
            <button type="submit" id="btnSavekeg" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" id="btlkeg" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
	
	<script src="<?php echo base_url();?>js/atribut.js"></script>
  <script>
  $(document).ready(function ($) {
	getJenjang();
	getDosen();
	$(".date-picker").datepicker({ autoclose: true});
	  });
	
	function getJenjang(){
		$("#id_jenjang").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getJenjangprodi",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_jenjang").append('<option value="'+val.id+'">'+val.nm_jenjangprodi+'</option>');
					
				});
			
								  
				}
				});
	  
	}
	
	function getDosen(){
		$("#id_dosen").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getDosen",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_dosen").append('<option value="'+val.id+'">'+val.nm_karyawan+'</option>');
					
				});
			
								  
				}
				});
	  
	}
	function editFormtambah(row){
		  $("#id_jenjang").val(row.id_jenjang);
		  getProdi(row.id_prodi)
	  }
	function getProdi(x){
		$("#id_prodi").html('');
		var id_jenjang=$("#id_jenjang").val();
		$("#id_prodi").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getProdijenjang/"+id_jenjang,
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_prodi").append('<option value="'+val.id+'">'+val.nm_prodi+'</option>');
					
				});
					if(x!="0"){
					 $("#id_prodi").val(x);
					}	  
				}
				});	
	}
	
  function operateFormatter(value, row, index) {
       return [
			'<a class="btn btn-sm btn-primary btn-xs" id="dosen" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Tambah Dosen" ><i id="keg" class="glyphicon glyphicon-plus-sign" ></i></a> ',
			'<?php echo aksesUbah() ?>',
			'<?php echo aksesHapussatu() ?>'
        ].join('');
    }
	
	
	function tambahDosen(row){
		$('#formkeg')[0].reset(); 
		$('#modal_formkeg').modal('show'); 
		$('.modal-title').text('Form Tambah Dosen'); 
		document.getElementById('btnSavekeg').disabled=false;
		$('#formkeg').bootstrapValidator('resetForm',false);
		   $('#id_identitas').val(row.id);
	      document.getElementById('setkeg').value=0;
	}
	
	// kegiatan
	$('#formkeg')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDatakeg();
		 e.preventDefault();
    });
	
	function simpanDatakeg(){
	document.getElementById("btnSavekeg").disabled=true;

		var fd = new FormData(document.getElementById("formkeg"));
			fd.append("kegiatanisi",'');
			$.ajax({
			  url: "<?php echo base_url();?>masterdata/identitasborang/simpanDatadosen",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						 keluarLogin();
					}
					
					 tutupFormkeg();
					// loadData(1, 15,'desc');
			});
			return false;		
 
}

 function tutupFormkeg(){
		 $('#formkeg')[0].reset(); // reset form on modals 
      	$('#modal_formkeg').modal('hide'); // show bootstrap modal
	}
	
	function operateDetail(index, row,element){
		$.ajax({
				  type: "POST",
				  url: "identitasborang/loaddatadetail?id="+row['id']+"&index="+index,
				  dataType:"JSON",
				  success: function(result) { 

				  	var x1 = "no_baris_"+index;
				  	var el = "tr[data-index='"+index+"']"
					$(el).next('tr.detail-view').addClass(x1);

					var id,nm_karyawan,id_dosen,tgl_pengisian,id_identitas;
					
					
					var tbl_det;
					tbl_det = '<td colspan="18" align="center"><table border="1" width="100%" style="border-style: solid;border-color:#9F9492;">';
					tbl_det += '<tr style="border-color:#9F9492;">';
					tbl_det += '<td align="center" width="5%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Action</td>';
					tbl_det += '<td align="center" width="30%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;"> Nama Dosen</td>';
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Tanggal Pengisian</td>'
					tbl_det += '</tr>';
        			tbl_det += '<tr style="border-color:#9F9492;">';

        			$.each(eval(result), function (key,value) {
						
        				id = value['id'];
        				nm_karyawan = value['nm_karyawan'];
        				id_dosen = value['id_dosen'];
        				tgl_pengisian= value['tgl_pengisian'];
						id_identitas= value['id_identitas'];
					//	alert(idkeg);
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp;<a class="btn btn-sm btn-primary btn-xs" onclick="editFormkegiatan(\''+id+'\',\''+id_dosen+'\',\''+tgl_pengisian+'\',\''+id_identitas+'\')" id="editdata" class="btn btn-sm btn-primary"  href="#" title="Ubah Kegiatan" ><i id="edtkeg" class="glyphicon glyphicon-edit" ></i></a>';
						tbl_det += '&nbsp;&nbsp;<a class="btn btn-sm btn-danger btn-xs" onclick="hapusKegiatan(\''+id+'\')" id="hapuskegiatan" class="btn btn-sm btn-primary"  href="#" title="Hapus Kegiatan" ><i id="edtkeg" class="glyphicon glyphicon-trash" ></i></a> </td>';
        				tbl_det += '&nbsp;&nbsp; <td style="border-color:#9F9492;" id="kegiatan'+id+'">'+nm_karyawan+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;'+tgl_pengisian+'</td>';
        				tbl_det += '</tr>';

        			});
        			tbl_det += '</table><td>';

					$(".detail-view."+x1).html(tbl_det);

				  }


		});

    }
	
	function editFormkegiatan(id,id_dosen,tgl_pengisian,id_identitas){
		 // $('#formkeg')[0].reset(); 
      $('#modal_formkeg').modal('show'); 
      $('.modal-title').text('Form Ubah Kegiatan MKP'); 
	  document.getElementById('btnSavekeg').disabled=false;
	    
		
		  
		   $('#id_identitas').val(id_identitas);
	      document.getElementById('setkeg').value=1;
		   $('#id_iddos').val(id);
		    $("#id_dosen").val(id_dosen).trigger('change');
			 $("#tgl_pengisian").val(tgl_pengisian);
			 
	}
	
	function hapusKegiatan(id){
		bootbox.confirm("Yakin Data akan di Hapus?", function(result) {
			if(result==true){
				 $.ajax({
                    url: 'identitasborang/hapusdatadosen?id='+id,
                    type: 'POST',
                    success: function (data) {
					//alert(data);
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
					
						
						loadData(1, 15,'desc');
                        
                    }
                })
			}
               
        });
	}
	
  </script>