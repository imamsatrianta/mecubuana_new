
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height:293px;" >


		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-pencil-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu"><?php echo callmenudess()?></span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <input type="hidden" value="0" name="buttonedit" id="buttonedit"/>
                
                    <div id="toolbar">
                    <?php
					echo aksesTambah();
					echo aksesAktivasi();
					?>
                    </div>
                   
                  
					<table id="table" 
                     data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                           data-pagination="true"
                           data-url="rool/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id_rol"
						   data-sort-order="desc">
                        <thead>	
						
                     
                        <tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
							<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
                            <th data-field="nm_rol" data-halign="center" data-align="left" data-sortable="true">Nama Rool</th>
                            <th data-field="status" data-sortable="true" data-halign="center" data-formatter="statusFormat" data-align="left">Status</th>
                             
                        </tr>
                        </thead>
                    </table>
                
              
            </div><!-- /.col -->
          </div>  
       
		
</div><!-- /.content-wrapper -->


 <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:70%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
	  
      <div class="modal-body form">
        <form  id="form" name="form" class="form-horizontal"      >
          <input type="hidden" value="" name="id_rol" id="id_rol"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Nama Rool</label>
              <div class="col-md-6">
                <input name="nm_rol" id="nm_rol" placeholder="Username" class="form-control" type="text" required="required">
              </div>
            </div>
            </div>
         
			<div class="modal-body form">
				<div id="accordion" class="accordion-style1 panel-group">
				
				<?php
				$x=0;
				 foreach($menu as $datamenu){
					 $x++;
					 $idmenusatu=$datamenu->id;
					 $nmmenusatu=$datamenu->menudes;
					 $jmlsatu=$jmldet[$idmenusatu]+1;
				 ?>
					
				
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $idmenusatu;?>">
								<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
								&nbsp; <?php echo $nmmenusatu?> 
							</a>
						</h4>
					</div>
					
					<div class="panel-collapse collapse" id="collapse<?php echo $idmenusatu;?>">
						
						<div class="panel-body">
							<table class="footertable" width="100%">
								<tr  style="background-color:#3C8DBC">
								<td class="judulkolomfootertable" style="text-align: center; font-size: 13px" height="30" ><?php echo $nmmenusatu?> 
								<input type='hidden' name='detail[<?php echo $x;?>][id_menusatu]'   id='menusatuatas<?php echo $x;?>' value='<?php echo $idmenusatu;?>' />
								</td>
								<td class="judulkolomfootertable" style="text-align: center; font-size: 13px" height="30">
								<input type='checkbox' name='detail[<?php echo $x;?>][nm_menusatu]' id='viewHeaderatas<?php echo $x?>' />
								
								</td>
								</tr>
							</table>
							
							
							<table class="footertable" width="100%" border="1" cellpadding="0" cellspacing="0" >
								<tr bgcolor="#CCCCCC">
								<td class="judulkolomfootertable2" width="30" style="text-align: center;; font-size: 13px">No</td>
								<td class="judulkolomfootertable2" width="30" style="text-align: center;; font-size: 13px">Menu
								<input type="hidden" id="jumlahcek<?php echo $idmenusatu?>" value="<?php echo $jmlsatu?>" />
								</td>
								<td class="judulkolomfootertable2" width="30" style="text-align: center;; font-size: 13px">View   
									<br><input type='checkbox' id='viewHeaderview<?php echo $idmenusatu;?><?php echo $x;?>' onClick="checkAll('viewHeaderview<?php echo $idmenusatu;?><?php echo $x;?>','viewAkses<?php echo $idmenusatu;?>','<?php echo $jmlsatu;?>')" />
								</td>
								
								
								
								<td class="judulkolomfootertable2" width="30" style="text-align: center;; font-size: 13px">Tambah 
								<br><input type='checkbox' id='viewHeaderTambah<?php echo $idmenusatu;?><?php echo $x;?>' onClick="checkAll('viewHeaderTambah<?php echo $idmenusatu;?><?php echo $x;?>','viewTambah<?php echo $idmenusatu;?>',<?php echo $jmlsatu;?> )"  />
								</td>
								<td class="judulkolomfootertable2" width="30" style="text-align: center;; font-size: 13px">Ubah
								<br><input type='checkbox' id='viewHeaderUbah<?php echo $idmenusatu;?><?php echo $x;?>' onClick="checkAll('viewHeaderUbah<?php echo $idmenusatu;?><?php echo $x;?>','viewSunting<?php echo $idmenusatu;?>',<?php echo $jmlsatu;?> )" />
								</td>
								<td class="judulkolomfootertable2" width="30" style="text-align: center;; font-size: 13px">Hapus
								<br><input type='checkbox' id='viewHeaderHapus<?php echo $idmenusatu;?><?php echo $x;?>' onClick="checkAll('viewHeaderHapus<?php echo $idmenusatu;?><?php echo $x;?>','viewHapus<?php echo $idmenusatu;?>',<?php echo $jmlsatu;?> )" />
								
								</td>
								<td class="judulkolomfootertable2" width="30" style="text-align: center;; font-size: 13px">Approve
								<br><input type='checkbox' id='viewHeaderProses<?php echo $idmenusatu;?><?php echo $x;?>' onClick="checkAll('viewHeaderProses<?php echo $idmenusatu;?><?php echo $x;?>','viewProses<?php echo $idmenusatu;?>',<?php echo $jmlsatu;?> )" />
								</td>
								<td class="judulkolomfootertable2" width="30" style="text-align: center;; font-size: 13px">Cetak
								<br><input type='checkbox' id='viewHeaderCetak<?php echo $idmenusatu;?><?php echo $x;?>' onClick="checkAll('viewHeaderCetak<?php echo $idmenusatu;?><?php echo $x;?>','viewCetak<?php echo $idmenusatu;?>',<?php echo $jmlsatu;?> )" />
								</td>
								
								</td>
							
								
								</tr>
								
								
								
								<?php
										//print_r($detmenu[$idmen]);
											$j=0;
											foreach($detmenu[$idmenusatu] as $parentdet){
											$j++;
											$nm_menudua=$parentdet->menudes;
											$id_menudua=$parentdet->id;
											
											
											
											
												
												 $jmldua=$jmldetdua[$idmenusatu][$id_menudua]+1;
												 
												//foreach($detmenudua[$idmenusatu][$id_menudua] as $parentdetdua){
													
													
													
												//}
											?>
											
											<?php
												if($jmldua==1){
											?>
											
											
											<tr bgcolor="#3C8DBC">
												<td class="kontentKolom1"><?php echo $j?> </td>
												<td class="kontentKolom" width="350px"><?php echo $nm_menudua?>  
													<input type="hidden" name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][menudet]'  id="menudua<?php echo $idmenusatu?><?php echo $j?>" value="<?php echo $id_menudua?>" />
													</td>
												<td class="kontentKolom" style="text-align:center">
												
												<input type='checkbox' id='viewAkses<?php echo $idmenusatu;?><?php echo $j;?>' name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][tampil]'  />
												 </td>
												<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox'  name='detail[".$x."][detaildua][".$j."][tambah]' id='viewTambah".$idmenusatu,$j."' >"?>
												 </td>
												<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox' name='detail[".$x."][detaildua][".$j."][ubah]' id='viewSunting".$idmenusatu,$j."' >"?>
												</td>
												<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox' name='detail[".$x."][detaildua][".$j."][hapus]' id='viewHapus".$idmenusatu,$j."' >"?>
												
												</td>
												<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox' name='detail[".$x."][detaildua][".$j."][approve]' id='viewProses".$idmenusatu,$j."' >"?>
												</td>
												<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox' name='detail[".$x."][detaildua][".$j."][cetak]' id='viewCetak".$idmenusatu,$j."' >"?>
												</td>
												
											</tr>
											
												<?php } else{ ?>
												
												
												
												<tr bgcolor="#3C8DBC">
												<td class="kontentKolom1"><?php echo $j?> </td>
												<td class="kontentKolom" width="350px"><?php echo $nm_menudua?> <?php echo $jmldua?>
													<input type="hidden" name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][menudet]'  id="menudua<?php echo $idmenusatu?><?php echo $j?>" value="<?php echo $id_menudua?>" />
													</td>
												<td class="kontentKolom" style="text-align:center">
												
												<input type='checkbox' id='viewAkses<?php echo $idmenusatu;?><?php echo $j;?>' name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][tampil]' onClick="checkAlltiga('viewAkses<?php echo $idmenusatu;?><?php echo $j;?>','viewAksestiga<?php echo $id_menudua;?>','<?php echo $jmldua;?>')"  />
												 </td>
												<td class="kontentKolom" style="text-align:center"><input type='checkbox' name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][tambah]' id='viewTambah<?php echo $idmenusatu;?><?php echo $j;?>'  onClick="checkAlltiga('viewTambah<?php echo $idmenusatu;?><?php echo $j;?>','viewTambahtiga<?php echo $id_menudua;?>','<?php echo $jmldua;?>')" /> 
												 </td>
												<td class="kontentKolom" style="text-align:center"><input type='checkbox' name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][ubah]' id='viewSunting<?php echo $idmenusatu;?><?php echo $j;?>'  onClick="checkAlltiga('viewSunting<?php echo $idmenusatu;?><?php echo $j;?>','viewSuntingtiga<?php echo $id_menudua;?>','<?php echo $jmldua;?>')" /> 
												</td>
												<td class="kontentKolom" style="text-align:center"><input type='checkbox' name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][hapus]' id='viewHapus<?php echo $idmenusatu;?><?php echo $j;?>'  onClick="checkAlltiga('viewHapus<?php echo $idmenusatu;?><?php echo $j;?>','viewHapustiga<?php echo $id_menudua;?>','<?php echo $jmldua;?>')" /> 
												</td>
												<td class="kontentKolom" style="text-align:center"><input type='checkbox' name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][approve]' id='viewProses<?php echo $idmenusatu;?><?php echo $j;?>'  onClick="checkAlltiga('viewProses<?php echo $idmenusatu;?><?php echo $j;?>','viewProsestiga<?php echo $id_menudua;?>','<?php echo $jmldua;?>')" /> 
												</td>
												<td class="kontentKolom" style="text-align:center"><input type='checkbox' name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][cetak]' id='viewCetak<?php echo $idmenusatu;?><?php echo $j;?>'  onClick="checkAlltiga('viewCetak<?php echo $idmenusatu;?><?php echo $j;?>','viewCetaktiga<?php echo $id_menudua;?>','<?php echo $jmldua;?>')" /> 
												</td>
											</tr>
												
												<?php
													$n=0;
													foreach($detmenudua[$idmenusatu][$id_menudua] as $datadua){
													$n++;
													$nm_menutiga=$datadua->menudes;
													$id_menutiga=$datadua->id;
												?>
													<tr >
														<td class="kontentKolom2"><?php echo $n?> </td>
														<td class="kontentKolom" width="350px"><?php echo $nm_menutiga?>
															<input type="text" name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][detailtiga][<?php echo $n;?>][menudettiga]'  id="menutiga<?php echo $id_menudua?><?php echo $n?>" value="<?php echo $id_menutiga?>" />
														</td>
														<td class="kontentKolom" style="text-align:center">
														<input type='checkbox' id='viewAksestiga<?php echo $id_menudua;?><?php echo $n;?>' name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][detailtiga][<?php echo $n;?>][tampiltiga]'  />
														 </td>
														<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox'  name='detail[".$x."][detaildua][".$j."][detailtiga][".$n."][tambahtiga]' id='viewTambahtiga".$id_menudua,$n."' >"?>
														 </td>
														<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox' name='detail[".$x."][detaildua][".$j."][detailtiga][".$n."][ubahtiga]' id='viewSuntingtiga".$id_menudua,$n."' >"?>
														</td>
														<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox' name='detail[".$x."][detaildua][".$j."][detailtiga][".$n."][hapustiga]' id='viewHapustiga".$id_menudua,$n."' >"?>
														
														</td>
														<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox' name='detail[".$x."][detaildua][".$j."][detailtiga][".$n."][approvetiga]' id='viewProsestiga".$id_menudua,$n."' >"?>
														</td>
														<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox' name='detail[".$x."][detaildua][".$j."][detailtiga][".$n."][cetaktiga]' id='viewCetaktiga".$id_menudua,$n."' >"?>
														</td>
													</tr>
													
													<?php } ?>
												
												
												
												
												<?php } ?>
												
							
										<?php
										}
									
									?>
							</table>
							
							
							
						</div>
					</div>
				</div>	
					<?php } ?>
					
					
					
					
					
					
											
				
				
				
			</div>
		  
		  
		  
			</div>
		 
		 
		 
		 
		 
		 
		 
		 
		 
        </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
  <!-- End Bootstrap modal -->
   
    <script>
	function checkAll(headerName,idDetail,rowNum){
	//alert(headerName);
    var headerId = document.getElementById(headerName).checked;
		var i = 1;
		if(headerId == true){
		
			for(i=1;i<rowNum; i++){
			//	alert(idDetail+i);
				document.getElementById(idDetail+i).checked = true;
			}
		}else if (headerId == false){
			for(i=1;i<rowNum; i++){
				document.getElementById(idDetail+i).checked = false;
			}
		}
	   
	}
	
	function checkAlltiga(headerName,idDetail,rowNum){
	//alert(headerName);
    var headerId = document.getElementById(headerName).checked;
		var i = 1;
		if(headerId == true){
		
			for(i=1;i<rowNum; i++){
			//	alert(idDetail+i);
				document.getElementById(idDetail+i).checked = true;
			}
		}else if (headerId == false){
			for(i=1;i<rowNum; i++){
				document.getElementById(idDetail+i).checked = false;
			}
		}
	   
	}
	 function statusFormat(value, row, index) {
	   	if(row.status=='0'){
			return ['Tidak Aktiv'];
		}else{
			return [' Aktiv'];
		}
    
    }





$( document ).ready( function () {

  $('#form')
        .bootstrapValidator({
		 //excluded: ':enabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
        
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanData();
		 e.preventDefault();
    });
	$aktivasi.prop('disabled', true);
} );		


	 function simpanData(){
		// alert("xx")
	 	 document.getElementById("btnSave").disabled=true;
 	 		var data = $('#form').serializeArray();
			  $.ajax({
				  type: "POST",
				  url: "rool/simpanData",
				  data: data,
				  success: function(result) { 
				  try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
					Command: toastr[status](pesan);
					  } catch (e) {
						//  keluarLogin();
					}
					
				  
					 tutupForm();
					 loadData(1, 15,'desc');
				
				  }
			  });
			  return false;
			  
	
 }
 
	function tambahData(){
       $('#form')[0].reset(); 
      $('#modal_form').modal('show'); 
      $('.modal-title').text('Tambah User'); 
	  document.getElementById('set').value=0;
	  document.getElementById('btnSave').disabled=false;
    }
    
	function tutupForm(){
		 $('#form')[0].reset(); // reset form on modals 
      	$('#modal_form').modal('hide'); // show bootstrap modal
	}
	
    function loadData(number, size,order){
            var offset=(number - 1) * size;
            var limit=size;
			
            $.ajax({
                    type: "POST",
                    url: "rool/loaddataTabel?order="+order+"&limit="+limit+"&offset="+offset,
                    dataType:"JSON",
                    success: function(result){
    				 $table.bootstrapTable('load', result);
    			
                    }
                });
        }
		
   
    
    function editForm(row){
        $('#form')[0].reset(); 
      $('#modal_form').modal('show'); 
      $('.modal-title').text('Ubah User'); 
	  document.getElementById('set').value=1;
	  document.getElementById('btnSave').disabled=false;
		 
	    for (var name in row) {
			 $('#modal_form').find('input[name="' + name + '"]').val(row[name]);
             
        }
      var id_rol=row.id_rol;
	 // alert(id_rol);
	  editDatagrup(id_rol);

    }
	
	
    
    
   
     var $table = $('#table'),
        $remove = $('#remove'),
		$aktivasi = $('#aktivasi')
   

   
    $(function () {
        $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
            $aktivasi.prop('disabled', !$table.bootstrapTable('getSelections').length);
        });
		
        $aktivasi.click(function () {
		var aktiv="";
            var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
				aktiv=row.id+","+aktiv;
            });
			
			aktifArray(aktiv);
            $table.bootstrapTable('aktivasi', {
                field: 'id',
                values: ids
            });
            $aktivasi.prop('disabled', true);
        });
		
		
    });
   
    function operateFormatter(value, row, index) {
       return [
            '<a class="btn btn-sm btn-primary btn-xs" id="edit" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Ubah" >',
            '<i class="glyphicon glyphicon-edit" ></i>',
            '</a> ',
            '<a class="btn btn-sm btn-danger btn-xs" id="aktivasi" href="javascript:void(0)" title="Aktivasi">',
            '<i class="glyphicon glyphicon-ok-sign"></i>',
            '</a>'
        ].join('');
    }
    window.operateEvents = {
        'click #edit': function (e, value, row, index) {
            editForm(row);
         //   alert('You click like action, row: ' + JSON.stringify(row));
        },
        
        'click #aktivasi': function (e, value, row, index) {
            hapusData(row.id);
        }
    };
      
    function hapusData(id){
		//alert(id);
		bootbox.confirm("Yakin akan Aktivasi Data?", function(result) {
			if(result==true){
				 $.ajax({
                    url: 'rool/hapusData?id='+id,
                    type: 'delete',
                    success: function (data) {
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
					
					//alert(data);
						obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						loadData(1, 15,'desc');
                        
                    }
                })
			}
               
        });
	}
	
	function aktifArray(hapus){
		//alert(hapus);
		bootbox.confirm("Yakin akan Aktivasi Data?", function(result) {
			if(result==true){
				 $.ajax({
                    url: 'rool/hapusDataarray?data='+hapus,
                    type: 'delete',
                    success: function (data) {
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
						obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						loadData(1, 15,'desc');
                    }
                })
			}
               
        });
	}
	
	function editDatagrup(id_rol){
		var sd="id_rol="+id_rol;
				var i=0;
		 		$.ajax({
                    type: "GET",
                    url: "rool/editDatarool",
                    data: sd,
                    dataType:"json",
                    success: function(result){
						$.each(result, function(key, val) {
						i++;
						 	var id=val.id;
							var u_access=val.u_access;
								if(u_access=='1'){
									document.getElementById('viewHeaderatas'+i).checked = true;
								}else{
									document.getElementById('viewHeaderatas'+i).checked = false;
								}
								var n=0;
								$.each(val.menusatu, function(index, value) {
									n++;
									var iddua=value.id;
									var u_accessdua=value.u_accessdua;
									var u_insertdua=value.u_insertdua;
									var u_updatedua=value.u_updatedua;
									var u_deletedua=value.u_deletedua;
									var u_prosesdua=value.u_prosesdua;
									var u_printdua=value.u_printdua;
			
									if(u_accessdua=="1"){
										document.getElementById('viewAkses'+id+''+n).checked = true;
									}else{
										document.getElementById('viewAkses'+id+''+n).checked = false;
									}
									if(u_insertdua=="1"){
										document.getElementById('viewTambah'+id+''+n).checked = true;
									}else{
										document.getElementById('viewTambah'+id+''+n).checked = false;
									}
									if(u_updatedua=="1"){
										document.getElementById('viewSunting'+id+''+n).checked = true;
									}else{
										document.getElementById('viewSunting'+id+''+n).checked = false;
									}
									if(u_deletedua=="1"){
										document.getElementById('viewHapus'+id+''+n).checked = true;
									}else{
										document.getElementById('viewHapus'+id+''+n).checked = false;
									}
									if(u_prosesdua=="1"){
										document.getElementById('viewProses'+id+''+n).checked = true;
									}else{
										document.getElementById('viewProses'+id+''+n).checked = false;
									}
									if(u_printdua=="1"){
										document.getElementById('viewCetak'+id+''+n).checked = true;
									}else{
										document.getElementById('viewCetak'+id+''+n).checked = false;
									}
								
										var a=0;
										$.each(value.menudua, function(index, baris) {
											a++;
											var idtiga=baris.id;
											var u_accesstiga=baris.u_accesstiga;
											var u_inserttiga=baris.u_inserttiga;
											var u_updatetiga=baris.u_updatetiga;
											var u_deletetiga=baris.u_deletetiga;
											var u_prosestiga=baris.u_prosestiga;
											var u_printtiga=baris.u_printtiga;
											
											if(u_accesstiga=="1"){
												document.getElementById('viewAksestiga'+iddua+''+a).checked = true;
											}else{
												document.getElementById('viewAksestiga'+iddua+''+a).checked = false;
											}
											if(u_inserttiga=="1"){
												document.getElementById('viewTambahtiga'+iddua+''+a).checked = true;
											}else{
												document.getElementById('viewTambahtiga'+iddua+''+a).checked = false;
											}
											if(u_updatetiga=="1"){
												document.getElementById('viewSuntingtiga'+iddua+''+a).checked = true;
											}else{
												document.getElementById('viewSuntingtiga'+iddua+''+a).checked = false;
											}
											if(u_deletetiga=="1"){
												document.getElementById('viewHapustiga'+iddua+''+a).checked = true;
											}else{
												document.getElementById('viewHapustiga'+iddua+''+a).checked = false;
											}
											if(u_prosestiga=="1"){
												document.getElementById('viewProsestiga'+iddua+''+a).checked = true;
											}else{
												document.getElementById('viewProsestiga'+iddua+''+a).checked = false;
											}
											if(u_printtiga=="1"){
												document.getElementById('viewCetaktiga'+iddua+''+a).checked = true;
											}else{
												document.getElementById('viewCetaktiga'+iddua+''+a).checked = false;
											}
										
											//alert(idtiga)
										});
									
									
									
									
									
							
									});
								
								
								
								
					});
				}
			});
	}
	
    </script>