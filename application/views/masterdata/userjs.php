
	
<script>

$('#formtest')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		 alert("ok");
		//simpanData();
		 e.preventDefault();
		 
		 
    });
	
function clearcombo(){
	$("#id_direktorat").html('');
		$("#id_unit").html('');
		$("#id_rektorat").val('');
		$("#id_prodi").html('');
}
function cekLevel(rektorat){
	var level=$("#id_level").val();
	clearcombo();
	if(rektorat!='0'){
		$("#id_rektorat").val(rektorat);
	}
	if(level=="1"){
		document.getElementById('rektor').style.display = 'none';
		document.getElementById('direktor').style.display = 'none';
		document.getElementById('unit').style.display = 'none';
		document.getElementById('prodi').style.display = 'none';
	}else if(level=="2"){
		document.getElementById('rektor').style.display = 'block';
		document.getElementById('direktor').style.display = 'none';
		document.getElementById('unit').style.display = 'none';
		document.getElementById('prodi').style.display = 'none'
	}else if(level=="3"){
		document.getElementById('rektor').style.display = 'block';
		document.getElementById('direktor').style.display = 'block';
		document.getElementById('unit').style.display = 'none';
		document.getElementById('prodi').style.display = 'none';
	}else if(level=="4"){
		document.getElementById('rektor').style.display = 'block';
		document.getElementById('direktor').style.display = 'block';
		document.getElementById('unit').style.display = 'block';
		document.getElementById('prodi').style.display = 'none';
	}
	else if(level=="5"){
		document.getElementById('rektor').style.display = 'none';
		document.getElementById('direktor').style.display = 'none';
		document.getElementById('unit').style.display = 'none';
		document.getElementById('prodi').style.display = 'block';
		comboProdi('0');
	}
	
}

function cariDirektorat(direktorat){
	var level=$("#id_level").val();
	var id_rektorat=$("#id_rektorat").val();
	if(level>2){
		$("#id_direktorat").html('');
		$("#id_unit").html('');
			$("#id_direktorat").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getDirektoratdua/"+id_rektorat,
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_direktorat").append('<option value="'+val.id+'">'+val.nm_direktorat+'</option>');
					
				});
					if(direktorat!='0'){
					//	alert(DIKLAT_LEVEL)  
					$("#id_direktorat").val(direktorat).trigger('change');	
					}			  
				}
				});	
	}
	
}

function cariUnit(direktorat,unit){
	//alert(unit)
	var level=$("#id_level").val();
	var id_direktorat=$("#id_direktorat").val();
	if(level>3){
		$("#id_unit").html('');
			$("#id_unit").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getUnitdua/"+id_direktorat,
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_unit").append('<option value="'+val.id+'">'+val.nm_unit+'</option>');
					
				});
					if(unit!='0'){
					//	alert(DIKLAT_LEVEL)  
					$("#id_unit").val(unit).trigger('change');	
					}			  
				}
				});	
	}
}
function comboProdi(prodi){
	//var id_rektorat=$("#id_rektorat").val();
		$("#id_prodi").html('');
			$("#id_prodi").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getProdidua",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_prodi").append('<option value="'+val.id+'">'+val.nm_prodi+'</option>');
					
				});
					if(prodi!='0'){
					//	alert(DIKLAT_LEVEL)  
					$("#id_prodi").val(prodi).trigger('change');	
					}			  
				}
				});	
}
function editFormtambah(row){
	//alert(row['id_level']);
	$("#idrool").val(row['idrool']);
	$("#id_level").val(row['id_level']);
	var id_rektorat=row['id_rektorat'];
	var id_direktorat=row['id_direktorat'];
	var id_unit=row['id_unit'];
	
	$("#id_prodi").val(row['id_prodi']);
	var id_prodi=row['id_prodi'];
	cekLevel(id_rektorat);
	cariDirektorat(id_direktorat);
	cariUnit(id_direktorat,id_unit);
	comboProdi(id_prodi);
	
}

$(document).ready(function ($) {
	
	 document.getElementById('id_chckshowPasswordinput').checked = false;
	 
	 $("#id_showPasswordinout").click(function () {
		// alert("x");
        if ($('#id_chckshowPasswordinput').is(':checked')) {
            $('.clsPasswd').attr('type', 'text');
        } else {
            $('.clsPasswd').attr('type', 'password');
        }
    });
	
	 $('#formmenu')
        .bootstrapValidator({
		 //excluded: ':enabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
        
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDatadetailmenu();
		 e.preventDefault();
    });
	

});
 
 function simpanDatadetailmenu(){
	// alert("d")
  document.getElementById("btnSavemenu").disabled=true;
 	 var data = $('#formmenu').serializeArray();
			  $.ajax({
				  type: "POST",
				  url: ""+page+"/simpanDatadetail",
				  data: data,
				  success: function(result) { 
				  try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
					Command: toastr[status](pesan);
					  } catch (e) {
						//  keluarLogin();
					}
				  /*obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);*/
					 tutupFormdetdata();
				
				  }
			  });
			  return false;

 }
 
 
	
	
	function tutupFormdetdata(){
		 $('#formmenu')[0].reset(); // reset form on modals 
      	$('#modal_formmenu').modal('hide'); // show bootstrap modal
	}
   
    
   
	function menuForm(row){
		alert("x")
		 $('#formmenu')[0].reset(); 
		  $('#modal_formmenu').modal('show'); 
		  $('.modal-title').text('Form '+page+''); 
		  
		  document.getElementById('id_userrol').value=row.id;
		  document.getElementById('btnSavemenu').disabled=false;
		  editAksesmenu(row.id);
	}
	
	function editAksesmenu(id){
		var sd="id_user="+id;
				var i=0;
		 		$.ajax({
                    type: "GET",
                    url: "user/editDatarool",
                    data: sd,
                    dataType:"json",
                    success: function(result){
						$.each(result, function(key, val) {
						i++;
						 	var id=val.id;
							var u_access=val.u_access;
								if(u_access=='1'){
									document.getElementById('viewHeaderatas'+i).checked = true;
								}else{
									document.getElementById('viewHeaderatas'+i).checked = false;
								}
								var n=0;
								$.each(val.menusatu, function(index, value) {
									n++;
									var iddua=value.id;
									var u_accessdua=value.u_accessdua;
									var u_insertdua=value.u_insertdua;
									var u_updatedua=value.u_updatedua;
									var u_deletedua=value.u_deletedua;
									var u_prosesdua=value.u_prosesdua;
									var u_printdua=value.u_printdua;
			
									if(u_accessdua=="1"){
										document.getElementById('viewAkses'+id+''+n).checked = true;
									}else{
										document.getElementById('viewAkses'+id+''+n).checked = false;
									}
									if(u_insertdua=="1"){
										document.getElementById('viewTambah'+id+''+n).checked = true;
									}else{
										document.getElementById('viewTambah'+id+''+n).checked = false;
									}
									if(u_updatedua=="1"){
										document.getElementById('viewSunting'+id+''+n).checked = true;
									}else{
										document.getElementById('viewSunting'+id+''+n).checked = false;
									}
									if(u_deletedua=="1"){
										document.getElementById('viewHapus'+id+''+n).checked = true;
									}else{
										document.getElementById('viewHapus'+id+''+n).checked = false;
									}
									if(u_prosesdua=="1"){
										document.getElementById('viewProses'+id+''+n).checked = true;
									}else{
										document.getElementById('viewProses'+id+''+n).checked = false;
									}
									if(u_printdua=="1"){
										document.getElementById('viewCetak'+id+''+n).checked = true;
									}else{
										document.getElementById('viewCetak'+id+''+n).checked = false;
									}
								
										var a=0;
										$.each(value.menudua, function(index, baris) {
											a++;
											var idtiga=baris.id;
											var u_accesstiga=baris.u_accesstiga;
											var u_inserttiga=baris.u_inserttiga;
											var u_updatetiga=baris.u_updatetiga;
											var u_deletetiga=baris.u_deletetiga;
											var u_prosestiga=baris.u_prosestiga;
											var u_printtiga=baris.u_printtiga;
											
											if(u_accesstiga=="1"){
												document.getElementById('viewAksestiga'+iddua+''+a).checked = true;
											}else{
												document.getElementById('viewAksestiga'+iddua+''+a).checked = false;
											}
											if(u_inserttiga=="1"){
												document.getElementById('viewTambahtiga'+iddua+''+a).checked = true;
											}else{
												document.getElementById('viewTambahtiga'+iddua+''+a).checked = false;
											}
											if(u_updatetiga=="1"){
												document.getElementById('viewSuntingtiga'+iddua+''+a).checked = true;
											}else{
												document.getElementById('viewSuntingtiga'+iddua+''+a).checked = false;
											}
											if(u_deletetiga=="1"){
												document.getElementById('viewHapustiga'+iddua+''+a).checked = true;
											}else{
												document.getElementById('viewHapustiga'+iddua+''+a).checked = false;
											}
											if(u_prosestiga=="1"){
												document.getElementById('viewProsestiga'+iddua+''+a).checked = true;
											}else{
												document.getElementById('viewProsestiga'+iddua+''+a).checked = false;
											}
											if(u_printtiga=="1"){
												document.getElementById('viewCetaktiga'+iddua+''+a).checked = true;
											}else{
												document.getElementById('viewCetaktiga'+iddua+''+a).checked = false;
											}
										
											//alert(idtiga)
										});
									
									
									
									
									
							
									});
								
								
								
								
					});
				}
			});
	}
	
	
    
    
   
	

    function operateFormatter(value, row, index) {
       return [
			'<?php echo aksesUbah() ?>',
			'<?php echo aksesHapussatu() ?>',
	   		'<a class="btn btn-sm btn-primary btn-xs" id="edimenu" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Akses Menu" >',
            '<i class="fa  fa-user-plus" ></i>',
            '</a> '
        ].join('');
    }

      
   
   function checkAll(headerName,idDetail,rowNum){
	//alert(headerName);
    var headerId = document.getElementById(headerName).checked;
		var i = 1;
		if(headerId == true){
		
			for(i=1;i<rowNum; i++){
			//	alert(idDetail+i);
				document.getElementById(idDetail+i).checked = true;
			}
		}else if (headerId == false){
			for(i=1;i<rowNum; i++){
				document.getElementById(idDetail+i).checked = false;
			}
		}
	   
	}
		function checkAlltiga(headerName,idDetail,rowNum){
	//alert(headerName);
    var headerId = document.getElementById(headerName).checked;
		var i = 1;
		if(headerId == true){
		
			for(i=1;i<rowNum; i++){
			//	alert(idDetail+i);
				document.getElementById(idDetail+i).checked = true;
			}
		}else if (headerId == false){
			for(i=1;i<rowNum; i++){
				document.getElementById(idDetail+i).checked = false;
			}
		}
	   
	}
	
  </script>


<script src="<?php echo base_url();?>js/atribut.js"></script>