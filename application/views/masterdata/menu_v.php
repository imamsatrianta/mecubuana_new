<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree/themes/default/easyui1.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree/themes/icon.css">
	<script type="text/javascript" src="<?php echo base_url();?>assets/tree/jquery.easyui.min.js"></script>
	<script src="<?php echo base_url();?>assets/filter-control/bootstrap-table-filter-control.js"></script>

<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu"><?php echo callmenudess()?></span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="0" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					echo aksesTambah();
					?>
                    </div><table id="table" 
					data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
						   data-filter-control="true"
                           data-pagination="true"
                           data-url="menu/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="desc">
                        <thead>	
						<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
							<th data-field="menudes"  data-halign="center" data-align="left"  data-sortable="true" data-filter-control="input">Nama Menu  </th>
							<th data-field="menutool"  data-halign="center" data-align="left"  data-sortable="true" data-filter-control="input">Menu Deskripsi  </th>
							<th data-field="url"  data-halign="center" data-align="left"  data-sortable="true" data-filter-control="input">URL  </th>
							<th data-field="parent"  data-halign="center" data-align="left"  data-sortable="true" data-filter-control="select">Parent  </th>
							<th data-field="no"  data-halign="center" data-align="left"  data-sortable="true" data-filter-control="input">No Urut  </th>
                             <th data-field="selling"  data-halign="center" data-align="center"
                    data-formatter="operateFormatter" data-events="operateEvents">Action</th>
                        </tr>
						</thead>
                    </table>
                
              
            </div><!-- /.col -->
          </div>  
       
		
</div> <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="form" name="form" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
          
		  
		  <div class="form-body"><div class="form-group field-menudes"><label class="control-label col-md-3" for1="menudes">Nama Menu  </label> <div class="col-md-9"><input subtype="text" name="menudes" class="form-control text-input" id="menudes" required="required" type="text"></div></div><div class="form-group field-menutool"><label class="control-label col-md-3" for1="menutool">Menu Deskripsi  </label> <div class="col-md-9"><input subtype="text" name="menutool" class="form-control text-input" id="menutool" required="required" type="text"></div></div><div class="form-group field-url"><label class="control-label col-md-3" for1="url">URL  </label> <div class="col-md-9"><input subtype="text" name="url" class="form-control text-input" id="url" required="required" type="text"></div></div>
		  
		  <div class="form-group field-parent"><label class="control-label col-md-3" for1="parent">Parent  </label>
		  <div class="col-md-9">
		  
		  <input class="easyui-combotree" name="parent" id="parent"  data-options="url:'menu/getCombo',method:'get'" style="width: auto;height: 34px;padding: 6px 12px; font-size: 14px;" >
		  
		  </div></div>
		  
		  <div class="form-group field-no"><label class="control-label col-md-3" for1="no">No Urut  </label> <div class="col-md-9"><input subtype="text" name="no" class="form-control text-input" id="no" required="required" type="text"></div></div>
		  
			
          </div>
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> <script src="<?php echo base_url();?>js/atribut.js"></script>
  <script>
  function operateFormatter(value, row, index) {
       return [
			'<?php echo aksesUbah() ?>'
        ].join('');
    }
  </script>