<div class="content-wrapper" style="min-height:293px;" >
	<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">Master Grup Menu</span>
		</div>
		</h1>
         
        </section>
		
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				
				<div id="toolbar">
					 <input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
					<?php
					echo aksesTambah();
					echo aksesAktivasi();
					?>
               
                    </div>
					<table id="table" 
                     data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                           data-pagination="true"
                           data-url="grupmenu/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="desc">
                        <thead>	<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
							<th data-field="username"  data-halign="center" data-align="left"  data-sortable="true">Tahun Akademik  </th>
							<th data-field="nm_perusahaan"  data-halign="center" data-align="left"  data-sortable="true">Level User  </th>
							<th data-field="nm_perusahaan"  data-halign="center" data-align="left"  data-sortable="true">Direktorat  </th>
							<th data-field="nm_perusahaan"  data-halign="center" data-align="left"  data-sortable="true">Unit  </th>
							<th data-field="nm_perusahaan"  data-halign="center" data-align="left"  data-sortable="true">Fakultas  </th>
							<th data-field="nm_perusahaan"  data-halign="center" data-align="left"  data-sortable="true">Prodi </th>
							<th data-field="nm_perusahaan"  data-halign="center" data-align="left"  data-sortable="true">Jurusan</th>
							<th data-field="nm_perusahaan"  data-halign="center" data-align="left"  data-sortable="true">Nama Menu </th>
							<th data-field="status"  data-halign="center" data-align="center"
                    data-formatter="statusFormat">Status</th>
                  
                             <th data-field="selling"  data-halign="center" data-align="center"
                    data-formatter="operateFormatter" data-events="operateEvents">Action</th>
                        </tr></thead>
                    </table>
			</div>
		</div>
 
	</section>
 
</div>


<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="form" name="form" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
          
		  
		  <div class="form-body">
		  <div class="form-group field-idrool">
		  <label class="control-label col-md-3" for1="idrool">Tahun Akademik  </label>
			<div class="col-md-9">
			
			  <select type="select" name="idrool" class="form-control select2" id="idrool" required="required"  style="width: 100%;" >
			  <?php foreach($rool as $rl):?>
			  <option <?php echo 'selected';?> value='<?php echo $rl->id_rol?>'><?php echo $rl->nm_rol;?> </option>
			  <?php endforeach ?>
			  </select>
			</div>
		  </div>
		  
		  <div class="form-group field-idrool">
		  <label class="control-label col-md-3" for1="idrool">Level User  </label>
			<div class="col-md-9">
			  <select type="select" name="idrool" class="form-control select2" id="idrool" required="required"  style="width: 100%;" >
			  <?php foreach($rool as $rl):?>
			  <option <?php echo 'selected';?> value='<?php echo $rl->id_rol?>'><?php echo $rl->nm_rol;?> </option>
			  <?php endforeach ?>
			  </select>
			</div>
		  </div>
		  
		  <div class="form-group field-idrool">
		  <label class="control-label col-md-3" for1="idrool">Direktorat</label>
			<div class="col-md-9">
			  <select type="select" name="idrool" class="form-control select2" id="idrool" required="required"  style="width: 100%;" >
			  <?php foreach($rool as $rl):?>
			  <option <?php echo 'selected';?> value='<?php echo $rl->id_rol?>'><?php echo $rl->nm_rol;?> </option>
			  <?php endforeach ?>
			  </select>
			</div>
		  </div>
		  
		  <div class="form-group field-idrool">
		  <label class="control-label col-md-3" for1="idrool">Unit</label>
			<div class="col-md-9">
			  <select type="select" name="idrool" class="form-control select2" id="idrool" required="required"  style="width: 100%;" >
			  <?php foreach($rool as $rl):?>
			  <option <?php echo 'selected';?> value='<?php echo $rl->id_rol?>'><?php echo $rl->nm_rol;?> </option>
			  <?php endforeach ?>
			  </select>
			</div>
		  </div>
		  
		  <div class="form-group field-idrool">
		  <label class="control-label col-md-3" for1="idrool">Prodi</label>
			<div class="col-md-9">
			  <select type="select" name="idrool" class="form-control select2" id="idrool" required="required"  style="width: 100%;" >
			  <?php foreach($rool as $rl):?>
			  <option <?php echo 'selected';?> value='<?php echo $rl->id_rol?>'><?php echo $rl->nm_rol;?> </option>
			  <?php endforeach ?>
			  </select>
			</div>
		  </div>
		  
		  <div class="form-group field-idrool">
		  <label class="control-label col-md-3" for1="idrool">Fakultas</label>
			<div class="col-md-9">
			  <select type="select" name="idrool" class="form-control select2" id="idrool" required="required"  style="width: 100%;" >
			  <?php foreach($rool as $rl):?>
			  <option <?php echo 'selected';?> value='<?php echo $rl->id_rol?>'><?php echo $rl->nm_rol;?> </option>
			  <?php endforeach ?>
			  </select>
			</div>
		  </div>
		  
		  <div class="form-group field-idrool">
		  <label class="control-label col-md-3" for1="idrool">Jurusan</label>
			<div class="col-md-9">
			  <select type="select" name="idrool" class="form-control select2" id="idrool" required="required"  style="width: 100%;" >
			  <?php foreach($rool as $rl):?>
			  <option <?php echo 'selected';?> value='<?php echo $rl->id_rol?>'><?php echo $rl->nm_rol;?> </option>
			  <?php endforeach ?>
			  </select>
			</div>
		  </div>
		  
		  
		  
		  <div class="form-group field-username">
		  <label class="control-label col-md-3" for1="username">Username  </label> <div class="col-md-9">
		  <input subtype="text" name="username" class="form-control text-input" id="username" required="required" type="text">
		  </div></div><div class="form-group field-password">
		  
		  <label class="control-label col-md-3 " for1="password">Password  </label>
		  <div class="col-md-9">
		  <input subtype="text" name="password" class="form-control text-input clsPasswd" id="password" required="required" type="password">
		  <label class="checkbox" id="id_showPasswordinout">
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" name="remember" value="1" id="id_chckshowPasswordinput" /> Show password 
		  </label>
		  </div>
		  </div>
		  <div class="form-group field-idrool">
		  <label class="control-label col-md-3" for1="idrool">Perusahaan  </label><div class="col-md-9">
			
		  <select type="select" name="id_perusahaan" class="form-control select2" id="id_perusahaan" required="required"  style="width: 100%;" >
		  <?php foreach($perusahaan as $rl):?>
		  <option <?php echo 'selected';?> value='<?php echo $rl->id_perusahaan?>'><?php echo $rl->nm_perusahaan;?> </option>
		  <?php endforeach ?>
		  </select></div>
		  </div>
		
		  <div class="form-group field-idrool">
		  <label class="control-label col-md-3" for1="idrool">Rool Menu  </label>
			<div class="col-md-9">
			
			  <select type="select" name="idrool" class="form-control select2" id="idrool" required="required"  style="width: 100%;" >
			  <?php foreach($rool as $rl):?>
			  <option <?php echo 'selected';?> value='<?php echo $rl->id_rol?>'><?php echo $rl->nm_rol;?> </option>
			  <?php endforeach ?>
			  </select>
			</div>
		  
		  
		  </div>
		  
		  
		  
			
          </div>
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" id="btl" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
<script>
function editFormtambah(row){
	//alert(row['id_perusahaan']);
	$("#id_perusahaan").select2("val", row['id_perusahaan']);
	$("#idrool").select2("val", row['idrool']);
}



  </script>
<script src="<?php echo base_url();?>js/atribut.js"></script>