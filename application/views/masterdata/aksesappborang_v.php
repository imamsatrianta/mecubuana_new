

<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					echo aksesTambah();
					?>
					 <?php
					echo aksesHapus();
					?>
                    </div><table id="table" 
					data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
						   data-filter-control="true"
                           data-pagination="true"
                           data-url="aksesappborang/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="desc">
                        <thead>	
						<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
							<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
							<th data-field="nm_prodi"  data-halign="center" data-align="left"  data-sortable="true" data-filter-control="input"> Prodi  </th>
                            <th data-field="kode_approvel"  data-halign="center" data-align="left"  data-sortable="true" data-filter-control="input">Kode Approvel Borang </th> 
                        </tr>
						</thead>
                    </table>
                
              
            </div><!-- /.col -->
          </div>  
       
		
</div> <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:50%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="form" name="form" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
        
		<div class="form-group ">
			<label class="control-label col-md-4" for1="menudes">Prodi  </label> 
			<div class="col-md-8">
				<select type="select" name="id_prodi" class="form-control select2 input-sm" id="id_prodi" required="required"  style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
					 
				</select>
		    </div>
		</div>
		
	
		<div class="form-group ">
			<label class="control-label col-md-4" for1="menudes"> Kode Approvel  Sarmut</label> 
			<div class="col-md-8">
				<input name="kode_approvel" class="form-control text-input input-sm nomor" id="kode_approvel" required="required" maxlength="10" type="text" >
				
				
		    </div>
		</div>
	
		  
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> <script src="<?php echo base_url();?>js/atribut.js"></script>
  <script>
	 $("#tbh").click(function(){
			
			 $('#kode_approvel_realisasi').val('').trigger('change');
			  $('#kode_approvel').val('').trigger('change');
		});
		function getProdi(){
			
			$("#id_prodi").html('');
			$("#id_prodi").append('<option value="">--Pilih--</option>');
			$.ajax({
			type: "POST",
			dataType:"JSON",
			url: "<?php echo base_url();?>global_combo/getProdidua",
			success: function(result) {  
			$.each(result, function(key, val) {	
			$("#id_prodi").append('<option value="'+val.id+'">'+val.nm_prodi+'</option>');
				
			});
							  
			}
			});
		}
	$(document).ready(function ($) {
		 $("select").select2();
		 
		 $("select").on("select2:select", function (evt) {
		  var element = evt.params.data.element;
		  var $element = $(element);
		  
		  $element.detach();
		  $(this).append($element);
		  $(this).trigger("change");
		});


			getProdi();
			/*$.ajax({
			type: "POST",
			dataType:"JSON",
			url: "<?php echo base_url();?>global_combo/getKaryawan",
			success: function(result) {  
			$.each(result, function(key, val) {	
			$("#kode_approvel").append('<option value="'+val.kode_approve+'">'+val.nm_karyawan+'</option>');
			});
							  
			}
			});*/
		 });
	  function editFormtambah(row){
		//  alert(row.kode_approvel_pg)
		  $("#jenis_approvel").val(row.jenis_approvel).trigger("change");
		  $("#id_prodi").val(row.id_prodi).trigger("change");
		  
		  
		 
		  $('#kode_approvel').val(row.kode_approvel).trigger('change');
		  
		  
		  
	  }
	 
  function operateFormatter(value, row, index) {
       return [
			'<?php echo aksesUbah() ?>',
			'<?php echo aksesHapussatu() ?>'
        ].join('');
    }
  </script>