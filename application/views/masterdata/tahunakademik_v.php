

<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					echo aksesTambah();
					?>
					 <?php
					echo aksesHapus();
					?>
                    </div><table id="table" 
					data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
						   data-filter-control="true"
                           data-pagination="true"
                           data-url="tahunakademik/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="desc">
                        <thead>	
						<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
							<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
							<th data-field="nm_tahun_akademik"  data-halign="center" data-align="left"  data-sortable="true" data-filter-control="input">Nama Tahun Akademik  </th>
							<th data-field="semester"  data-halign="center" data-align="left"  data-sortable="true" data-filter-control="input">Semester  </th>
                            <th data-field="keterangan"  data-halign="center" data-align="left"  data-sortable="true" data-filter-control="input">Keterangan  </th> 
                        </tr>
						</thead>
                    </table>
                
              
            </div><!-- /.col -->
          </div>  
       
		
</div> <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="form" name="form" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
          
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Nama Tahun Akademik  </label> 
			<div class="col-md-9">
				<input name="nm_tahun_akademik" class="form-control text-input" id="nm_tahun_akademik" required="required" type="text">
		    </div>
		</div>
		
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Semester  </label> 
			<div class="col-md-9">
				<select type="select" name="semester" class="form-control select2" id="semester" required="required"  style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
					  <option value='1'>1</option>
					  <option value='2'>2</option>
					  <option value='3'>3</option>
					  <option value='4'>4</option>
					  <option value='5'>5</option>
					  <option value='6'>6</option>
					  <option value='7'>7</option>
					  <option value='8'>8</option>
					  <option value='9'>9</option>
					  <option value='10'>10</option>
				</select>
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Keterangan  </label> 
			<div class="col-md-9">
				<select type="select" name="keterangan" class="form-control select2" id="keterangan" required="required"  style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
					  <option value='Ganjil'>Ganjil</option>
					  <option value='Genap'>Genap</option>
					 
				</select>
		    </div>
		</div>
		  
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> <script src="<?php echo base_url();?>js/atribut.js"></script>
  <script>

	  
	  function editFormtambah(row){
		  $("#semester").val(row.semester);
		  $("#keterangan").val(row.keterangan);
	  }
  function operateFormatter(value, row, index) {
       return [
			'<?php echo aksesUbah() ?>',
			'<?php echo aksesHapussatu() ?>'
        ].join('');
    }
  </script>