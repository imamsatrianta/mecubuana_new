

<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					echo aksesTambah();
					?>
					 <?php
					echo aksesHapus();
					?>
                    </div><table id="table" 
					data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
						   data-filter-control="true"
                           data-pagination="true"
                           data-url="karyawan/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="desc">
                        <thead>	
						<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
							<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
							<th data-field="nm_karyawan"  data-halign="center" data-align="left"  data-sortable="true" >Nama Karyawan  </th>
							<th data-field="nidn"  data-halign="center" data-align="left"  data-sortable="true" >NIDN  </th>
                             <th data-field="jenis_jabatan"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="formatJabatan">Jenis Jabatan  </th>
							 <th data-field="nm_jabatan"  data-halign="center" data-align="left"  data-sortable="true" >Jabatan  </th>
                        </tr>
						</thead>
                    </table>
                
              
            </div><!-- /.col -->
          </div>  
       
		
</div> <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="form" name="form" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
          
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Nama Karyawan  </label> 
			<div class="col-md-9">
				<input name="nm_karyawan" class="form-control input-sm" id="nm_karyawan" required="required" type="text">
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">NIDN  </label> 
			<div class="col-md-9">
				<input name="nidn" class="form-control input-sm" id="nidn"  type="text">
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Jenis Jabatan  </label> 
			<div class="col-md-9">
				<select type="select" name="jenis_jabatan" class="form-control select2 input-sm" id="jenis_jabatan" onchange="cekJenisjabatan('0')" required="required"  style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
					 <option  value='1'>Rektorat</option>
					 <option  value='2'>Direktorat</option>
					 <option  value='3'>Kepala Unit</option>
					 <option  value='4'>Staf</option>
				</select>
		    </div>
		</div>
		
		<div class="form-group " id="jab">
			<label class="control-label col-md-3" for1="menudes">Jabatan  </label> 
			<div class="col-md-9">
				<select type="select" name="id_jabatan" class="form-control select2 input-sm" id="id_jabatan" required="required"  style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
				</select>
		    </div>
		</div>
		  
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> <script src="<?php echo base_url();?>js/atribut.js"></script>
  <script>
  $(document).ready(function ($) {

	  });
	  function editFormtambah(row){
		  $("#jenis_jabatan").val(row.jenis_jabatan);
		  $("#id_jabatan").val(row.id_jabatan);
		  cekJenisjabatan(row.id_jabatan)
	  }
	  function cekJenisjabatan(x){
		  $("#id_jabatan").html('');
		  var jenis_jabatan=$("#jenis_jabatan").val();
		  if(jenis_jabatan=="1"){
			  $("#jab").show();
			  getRektorat(x);
		  }else if(jenis_jabatan=="2"){
			   $("#jab").show();
			   getDirektorat(x);
		  }else if(jenis_jabatan=="3"){
			   $("#jab").show();
			   getUnit(x);
		  }else{
			   $("#jab").hide();
		  }
	  }
	function getRektorat(x){
		$("#id_jabatan").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getRektorat",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_jabatan").append('<option value="'+val.id+'">'+val.nm_rektorat+'</option>');
					
				});
				if(x!="0"){
					 $("#id_jabatan").val(x);
				}
								  
				}
				});
	  
	}
	function getDirektorat(x){
		$("#id_jabatan").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getDirektoratdua/"+id_rektorat,
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_jabatan").append('<option value="'+val.id+'">'+val.nm_direktorat+'</option>');
					
				});
					if(x!="0"){
					 $("#id_jabatan").val(x);
					}	  
				}
				});	
	}
	function getUnit(x){
		$("#id_jabatan").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getUnitdua/"+id_direktorat,
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_jabatan").append('<option value="'+val.id+'">'+val.nm_unit+'</option>');
					
				});
					if(x!="0"){
					 $("#id_jabatan").val(x);
					}		  
				}
				});	
	}
  function operateFormatter(value, row, index) {
       return [
			'<?php echo aksesUbah() ?>',
			'<?php echo aksesHapussatu() ?>'
        ].join('');
    }
	
	function formatJabatan(value, row, index){
		if(value=="1"){
			return "Rektorat";
		}else if(value=="2"){
			return "Direktorat";
		}else if(value=="3"){
			return "Kepala Unit";
		}else{
			
		}
	}
  </script>