
  <script>
  $(document).ready(function ($) {
	  $(".select2").select2();
	getJenjang();
	getDosen();
	$(".date-picker").datepicker({ autoclose: true});
	
	$("#tbh").click(function(){
	bersih();
	});
	
	$.ajax({
			type: "POST",
			dataType:"JSON",
			url: "<?php echo base_url();?>global_combo/getJenjangprodi",
			success: function(result) {  
			$.each(result, function(key, val) {	
			$("#id_jenjang_det").append('<option value="'+val.id+'">'+val.nm_jenjangprodi+'</option>');
				
			});
							  
			}
			});
			
	  });
	
	function getJenjang(){
		$("#id_jenjang").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getJenjangprodi",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_jenjang").append('<option value="'+val.id+'">'+val.nm_jenjangprodi+'</option>');
					
				});
			
								  
				}
				});
	  
	}
	
	function getDosen(){
		$("#id_dosen").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getDosen",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_dosen").append('<option value="'+val.id+'">'+val.nm_karyawan+'</option>');
					
				});
			
								  
				}
				});
	  
	}
	function editFormtambah(row){
		  $("#id_jenjang").val(row.id_jenjang).trigger('change');
		  getProdi(row.id_prodi);
		  bersih();
		  getDetail(row.id);
	  }
	function getProdi(x){
		$("#id_prodi").html('');
		var id_jenjang=$("#id_jenjang").val();
		$("#id_prodi").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getProdijenjang/"+id_jenjang,
				success: function(result) {  
				if(x!="0"){
					$("#id_prodi").html('');
					}	
				$.each(result, function(key, val) {	
				$("#id_prodi").append('<option value="'+val.id+'">'+val.nm_prodi+'</option>');
					
				});
					if(x!="0"){
					 $("#id_prodi").val(x).trigger('change');
					}	  
				}
				});	
	}
	
	
	
	
	
  function operateFormatter(value, row, index) {
       return [
		//	'<a class="btn btn-sm btn-primary btn-xs" id="dosen" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Tambah Dosen" ><i id="keg" class="glyphicon glyphicon-plus-sign" ></i></a> ',
			'<?php echo aksesUbah() ?>',
			'<?php echo aksesHapussatu() ?>'
        ].join('');
    }
	
	
	function tambahDosen(row){
		$('#formkeg')[0].reset(); 
		$('#modal_formkeg').modal('show'); 
		$('.modal-title').text('Form Tambah Dosen'); 
		document.getElementById('btnSavekeg').disabled=false;
		$('#formkeg').bootstrapValidator('resetForm',false);
		   $('#id_identitas').val(row.id);
	      document.getElementById('setkeg').value=0;
	}
	
	// kegiatan
	$('#formkeg')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDatakeg();
		 e.preventDefault();
    });
	
	function simpanDatakeg(){
	document.getElementById("btnSavekeg").disabled=true;

		var fd = new FormData(document.getElementById("formkeg"));
			fd.append("kegiatanisi",'');
			$.ajax({
			  url: "<?php echo base_url();?>masterdata/identitasborang3b/simpanDatadosen",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						 keluarLogin();
					}
					
					 tutupFormkeg();
					 loadData(1, 15,'desc');
			});
			return false;		
 
}

 function tutupFormkeg(){
		 $('#formkeg')[0].reset(); // reset form on modals 
      	$('#modal_formkeg').modal('hide'); // show bootstrap modal
	}
	
	function operateDetail(index, row,element){
		$.ajax({
				  type: "POST",
				  url: "identitasborang3b/loaddatadetail?id="+row['id']+"&index="+index,
				  dataType:"JSON",
				  success: function(result) { 

				  	var x1 = "no_baris_"+index;
				  	var el = "tr[data-index='"+index+"']"
					$(el).next('tr.detail-view').addClass(x1);

					var id,nm_karyawan,id_dosen,tgl_pengisian,id_identitas;
					
					
					var tbl_det;
					tbl_det = '<td colspan="18" align="center">';
					tbl_det += '<table border="1" width="100%" style="border-style: solid;border-color:#9F9492;">';
					tbl_det += '<tr style="border-color:#9F9492;">';
					tbl_det += '<td align="center" width="5%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Action</td>';
					tbl_det += '<td align="center" width="30%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;"> Nama Dosen</td>';
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Tanggal Pengisian</td>'
					tbl_det += '</tr>';
        			tbl_det += '<tr style="border-color:#9F9492;">';

        			$.each(eval(result), function (key,value) {
						
        				id = value['id'];
        				nm_karyawan = value['nm_karyawan'];
        				id_dosen = value['id_dosen'];
        				tgl_pengisian= value['tgl_pengisian'];
						id_identitas= value['id_identitas'];
					//	alert(idkeg);
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp;<a class="btn btn-sm btn-primary btn-xs" onclick="editFormkegiatan(\''+id+'\',\''+id_dosen+'\',\''+tgl_pengisian+'\',\''+id_identitas+'\')" id="editdata" class="btn btn-sm btn-primary"  href="#" title="Ubah Kegiatan" ><i id="edtkeg" class="glyphicon glyphicon-edit" ></i></a>';
						tbl_det += '&nbsp;&nbsp;<a class="btn btn-sm btn-danger btn-xs" onclick="hapusKegiatan(\''+id+'\')" id="hapuskegiatan" class="btn btn-sm btn-primary"  href="#" title="Hapus Kegiatan" ><i id="edtkeg" class="glyphicon glyphicon-trash" ></i></a> </td>';
        				tbl_det += '<td style="border-color:#9F9492;" id="kegiatan'+id+'">'+nm_karyawan+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;'+tgl_pengisian+'</td>';
        				tbl_det += '</tr>';

        			});
        			tbl_det += '</table><td>';

					$(".detail-view."+x1).html(tbl_det);

				  }


		});

    }
	
	function editFormkegiatan(id,id_dosen,tgl_pengisian,id_identitas){
		 // $('#formkeg')[0].reset(); 
      $('#modal_formkeg').modal('show'); 
      $('.modal-title').text('Form Ubah Kegiatan MKP'); 
	  document.getElementById('btnSavekeg').disabled=false;
	    
		
		  
		   $('#id_identitas').val(id_identitas);
	      document.getElementById('setkeg').value=1;
		   $('#id_iddos').val(id);
		    $("#id_dosen").val(id_dosen).trigger('change');
			 $("#tgl_pengisian").val(tgl_pengisian);
			 
	}
	
	function hapusKegiatan(id){
		bootbox.confirm("Yakin Data akan di Hapus?", function(result) {
			if(result==true){
				 $.ajax({
                    url: 'identitasborang3b/hapusdatadosen?id='+id,
                    type: 'POST',
                    success: function (data) {
					//alert(data);
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
					
						
						loadData(1, 15,'desc');
                        
                    }
                })
			}
               
        });
	}
	
	
	// add rodi
	function bersih() {
		//alert("x")
				var y = totalrow + 1;
				
				for (x = 0; x < y; x++) {
					if (document.getElementById("rowmat" + x)) {
						hapusBaris("rowmat" + x);
					}
				}
				totalrow = 0;
			}
		
		function hapusBaris(x) {
				if (document.getElementById(x) != null) {
					
					var $row    = $(this).parents('.form-group'),
					 $option = $row.find('[name="option[]"]');
					 $('#' + x).remove();
				}
			}

		  var totalrow= 0;
		function addRow() {
			totalrow++;
			var $template = $('#bookTemplate'),
			$clone    = $template
					.clone()
					.removeClass('hide')
					.removeAttr('id')
					.attr('data-book-index', totalrow)
					.attr('id', 'rowmat'+totalrow)
					.insertBefore($template);
			$clone 
				.find('[name="btnDelRowX"]').attr('onClick','hapusBaris("rowmat' + totalrow + '")').end() 
				.find('[name="id_jenjang_det"]').attr('name', 'detail[' + totalrow + '][id_jenjang_det]').attr('id', 'id_jenjang_det'+totalrow).attr('onClick','getProdidetdet(' + totalrow + ')').end()
				.find('[name="id_prodi_det"]').attr('name', 'detail[' + totalrow + '][id_prodi_det]').attr('id', 'id_prodi_det'+totalrow).end();
				
			
		}
		
		
	
	function getProdidetdet(x){
		$("#id_prodi_det"+x).html('');
		var id_jenjang=$("#id_jenjang_det"+x).val();
		$("#id_prodi_det"+x).append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getProdijenjang/"+id_jenjang,
				success: function(result) {  
					
				$.each(result, function(key, val) {	
				$("#id_prodi_det"+x).append('<option value="'+val.id+'">'+val.nm_prodi+'</option>');
					
				});
						  
				}
				});	
	}
	
	function getProdidetdetedit(id_jenjang,x,id_prodi){
		$("#id_prodi_det"+x).html('');
	//	alert(id_jenjang)
		$("#id_prodi_det"+x).append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getProdijenjang/"+id_jenjang,
				success: function(result) {  
					
				$.each(result, function(key, val) {	
				$("#id_prodi_det"+x).append('<option value="'+val.id+'">'+val.nm_prodi+'</option>');
					
				});
				$('#id_prodi_det'+x).val(id_prodi).trigger('change');
						  
				}
				});	
	}
	
	function getDetail(id){
		var sd="id="+id;
				var i=0;
		 		$.ajax({
                    type: "GET",
					  url: '<?php echo base_url();?>masterdata/identitasborang3b/getDetail',
                    data: sd,
                    dataType:"json",
                    success: function(result){
					var x=0;
					$.each(result, function(key, val) {
					x++; 
						addRow();
							getProdidetdetedit(val.id_jenjang,x,val.id_prodi);
							$('#id_jenjang_det'+x).val(val.id_jenjang).trigger('change');
							$('#id_prodi_det'+x).val(val.id_prodi).trigger('change');
							
					});	
			}
			});
	}
  </script>