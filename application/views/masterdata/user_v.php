<div class="content-wrapper" style="min-height:293px;" >
	<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">Master User</span>
		</div>
		</h1>
         
        </section>
		
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				
				<div id="toolbar">
					 <input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
					<?php
					echo aksesTambah();
					echo aksesHapus();
					?>
               
                    </div>
					<table id="table" 
                     data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                           data-pagination="true"
                           data-url="user/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="desc">
                        <thead>	<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
							<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
							<th data-field="username"  data-halign="center" data-align="left"  data-sortable="true">Username  </th>
							<th data-field="level"  data-halign="center" data-align="left"  data-sortable="true">Level Menu  </th>
							<th data-field="nm_rol"  data-halign="center" data-align="left"  data-sortable="true">Rool Menu  </th>
							<th data-field="nm_rektorat"  data-halign="center" data-align="left"  data-sortable="true">Rektorat  </th>
							<th data-field="nm_direktorat"  data-halign="center" data-align="left"  data-sortable="true">Direktorat  </th>
							
							<th data-field="nm_unit"  data-halign="center" data-align="left"  data-sortable="true">Unit  </th>
							<th data-field="nm_prodi"  data-halign="center" data-align="left"  data-sortable="true">Prodi  </th>
                  
                             
                        </tr></thead>
                    </table>
			</div>
		</div>
 
	</section>
 
</div>


<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="form" name="form" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
          
		  
		  <div class="form-body">
		  
		  <div class="form-group field-username">
		  <label class="control-label col-md-3" for1="username">Username  </label> <div class="col-md-9">
		  <input  name="username" class="form-control input-sm" id="username" required="required" type="text">
		  </div></div>
		  
		  <div class="form-group field-password">
		  
		  <label class="control-label col-md-3 " for1="password">Password  </label>
		  <div class="col-md-9">
		  <input name="password" class="form-control input-sm clsPasswd" id="password" required="required" type="password">
		  <label class="checkbox" id="id_showPasswordinout">
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" name="remember" value="1" id="id_chckshowPasswordinput" /> Show password 
		  </label>
		  </div>
		  </div>
		  <div class="form-group field-idrool">
			  <label class="control-label col-md-3" for1="idrool">Rool Menu  </label>
			  <div class="col-md-9">
				
				  <select type="select" name="idrool" class="form-control select2 input-sm" id="idrool" required="required"  style="width: 100%;" >
				  <?php foreach($rool as $rl):?>
				  <option <?php echo 'selected';?> value='<?php echo $rl->id_rol?>'><?php echo $rl->nm_rol;?> </option>
				  <?php endforeach ?>
				  </select>
			  </div>
		  
		  
		  </div>
		  
		  <div class="form-group ">
			  <label class="control-label col-md-3" for1="idrool">Level Menu  </label>
			  <div class="col-md-9">
				
				  <select type="select" name="id_level" class="form-control select2 input-sm" id="id_level" required="required"  style="width: 100%;" onchange="cekLevel('0')">
				  <option <?php echo 'selected';?> value=''>-- Pilih -- </option>
				  <?php foreach($level as $rl):?>
				  <option value='<?php echo $rl->id?>'><?php echo $rl->level;?> </option>
				  <?php endforeach ?>
				  </select>
			  </div>
		  </div>
		   <div class="form-group " id="rektor">
			  <label class="control-label col-md-3" for1="idrool">Rektorat  </label>
			  <div class="col-md-9">
				
				  <select type="select" name="id_rektorat" class="form-control select2 input-sm" id="id_rektorat" required="required"  style="width: 100%;" onchange="cariDirektorat('0')">
				  <option <?php echo 'selected';?> value=''>-- Pilih -- </option>
				  <?php foreach($rektorat as $rl):?>
				  <option value='<?php echo $rl->id?>'><?php echo $rl->nm_rektorat;?> </option>
				  <?php endforeach ?>
				  </select>
			  </div>
		  </div>
		   <div class="form-group" id="direktor">
			  <label class="control-label col-md-3" for1="idrool">Direktorat  </label>
			  <div class="col-md-9">
				
				  <select type="select" name="id_direktorat" class="form-control select2 input-sm" id="id_direktorat"  style="width: 100%;" onchange="cariUnit('0',0')">
				  <option <?php echo 'selected';?> value=''>-- Pilih -- </option>
				  <?php foreach($level as $rl):?>
				  <option value='<?php echo $rl->id?>'><?php echo $rl->level;?> </option>
				  <?php endforeach ?>
				  </select>
			  </div>
		  </div>
		  <div class="form-group " id="unit">
			  <label class="control-label col-md-3" for1="idrool">Unit  </label>
			  <div class="col-md-9">
				
				  <select type="select" name="id_unit" class="form-control select2 input-sm" id="id_unit"  style="width: 100%;" >
				  <option <?php echo 'selected';?> value=''>-- Pilih -- </option>
				  <?php foreach($level as $rl):?>
				  <option value='<?php echo $rl->id?>'><?php echo $rl->level;?> </option>
				  <?php endforeach ?>
				  </select>
			  </div>
		  </div>
		 
		  <div class="form-group " id="prodi">
			  <label class="control-label col-md-3" for1="idrool">Prodi  </label>
			  <div class="col-md-9">
				
				  <select type="select" name="id_prodi" class="form-control select2 input-sm" id="id_prodi"  style="width: 100%;" >
				  <option <?php echo 'selected';?> value=''>-- Pilih -- </option>
				  <?php foreach($level as $rl):?>
				  <option value='<?php echo $rl->id?>'><?php echo $rl->level;?> </option>
				  <?php endforeach ?>
				  </select>
			  </div>
		  </div>
		 
		
		  
		  
		  
		  
			
          </div>
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" id="btl" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	

    <div class="modal fade" id="modal_formmenu" role="dialog">
  <div class="modal-dialog" style="width:70%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>  
      </div>
	  
      <div class="modal-body form">
        <form  id="formmenu" name="form" class="form-horizontal"      >
          <input type="hidden" value="" name="id_userrol" id="id_userrol"/> 
		  
         
			<div class="modal-body form">
				<div id="accordion" class="accordion-style1 panel-group">
				
				<?php
				$x=0;
				 foreach($menu as $datamenu){
					 $x++;
					 $idmenusatu=$datamenu->id;
					 $nmmenusatu=$datamenu->menudes;
					 $jmlsatu=$jmldet[$idmenusatu]+1;
				 ?>
					
				
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $idmenusatu;?>">
								<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
								&nbsp; <?php echo $nmmenusatu?> 
							</a>
						</h4>
					</div>
					
					<div class="panel-collapse collapse" id="collapse<?php echo $idmenusatu;?>">
						
						<div class="panel-body">
							<table class="footertable" width="100%">
								<tr  style="background-color:#3C8DBC">
								<td class="judulkolomfootertable" style="text-align: center; font-size: 13px" height="30" ><?php echo $nmmenusatu?> 
								<input type='hidden' name='detail[<?php echo $x;?>][id_menusatu]'   id='menusatuatas<?php echo $x;?>' value='<?php echo $idmenusatu;?>' />
								</td>
								<td class="judulkolomfootertable" style="text-align: center; font-size: 13px" height="30">
								<input type='checkbox' name='detail[<?php echo $x;?>][nm_menusatu]' id='viewHeaderatas<?php echo $x?>' />
								
								</td>
								</tr>
							</table>
							
							
							<table class="footertable" width="100%" border="1" cellpadding="0" cellspacing="0" >
								<tr bgcolor="#CCCCCC">
								<td class="judulkolomfootertable2" width="30" style="text-align: center;; font-size: 13px">No</td>
								<td class="judulkolomfootertable2" width="30" style="text-align: center;; font-size: 13px">Menu
								<input type="hidden" id="jumlahcek<?php echo $idmenusatu?>" value="<?php echo $jmlsatu?>" />
								</td>
								<td class="judulkolomfootertable2" width="30" style="text-align: center;; font-size: 13px">View   
									<br><input type='checkbox' id='viewHeaderview<?php echo $idmenusatu;?><?php echo $x;?>' onClick="checkAll('viewHeaderview<?php echo $idmenusatu;?><?php echo $x;?>','viewAkses<?php echo $idmenusatu;?>','<?php echo $jmlsatu;?>')" />
								</td>
								
								
								
								<td class="judulkolomfootertable2" width="30" style="text-align: center;; font-size: 13px">Tambah 
								<br><input type='checkbox' id='viewHeaderTambah<?php echo $idmenusatu;?><?php echo $x;?>' onClick="checkAll('viewHeaderTambah<?php echo $idmenusatu;?><?php echo $x;?>','viewTambah<?php echo $idmenusatu;?>',<?php echo $jmlsatu;?> )"  />
								</td>
								<td class="judulkolomfootertable2" width="30" style="text-align: center;; font-size: 13px">Ubah
								<br><input type='checkbox' id='viewHeaderUbah<?php echo $idmenusatu;?><?php echo $x;?>' onClick="checkAll('viewHeaderUbah<?php echo $idmenusatu;?><?php echo $x;?>','viewSunting<?php echo $idmenusatu;?>',<?php echo $jmlsatu;?> )" />
								</td>
								<td class="judulkolomfootertable2" width="30" style="text-align: center;; font-size: 13px">Hapus
								<br><input type='checkbox' id='viewHeaderHapus<?php echo $idmenusatu;?><?php echo $x;?>' onClick="checkAll('viewHeaderHapus<?php echo $idmenusatu;?><?php echo $x;?>','viewHapus<?php echo $idmenusatu;?>',<?php echo $jmlsatu;?> )" />
								
								</td>
								<td class="judulkolomfootertable2" width="30" style="text-align: center;; font-size: 13px">Approve
								<br><input type='checkbox' id='viewHeaderProses<?php echo $idmenusatu;?><?php echo $x;?>' onClick="checkAll('viewHeaderProses<?php echo $idmenusatu;?><?php echo $x;?>','viewProses<?php echo $idmenusatu;?>',<?php echo $jmlsatu;?> )" />
								</td>
								<td class="judulkolomfootertable2" width="30" style="text-align: center;; font-size: 13px">Cetak
								<br><input type='checkbox' id='viewHeaderCetak<?php echo $idmenusatu;?><?php echo $x;?>' onClick="checkAll('viewHeaderCetak<?php echo $idmenusatu;?><?php echo $x;?>','viewCetak<?php echo $idmenusatu;?>',<?php echo $jmlsatu;?> )" />
								</td>
								
								</td>
							
								
								</tr>
								
								
								
								<?php
										//print_r($detmenu[$idmen]);
											$j=0;
											foreach($detmenu[$idmenusatu] as $parentdet){
											$j++;
											$nm_menudua=$parentdet->menudes;
											$id_menudua=$parentdet->id;
											
											
											
											
												
												 $jmldua=$jmldetdua[$idmenusatu][$id_menudua]+1;
												 
												//foreach($detmenudua[$idmenusatu][$id_menudua] as $parentdetdua){
													
													
													
												//}
											?>
											
											<?php
												if($jmldua==1){
											?>
											
											
											<tr bgcolor="#3C8DBC">
												<td class="kontentKolom1"><?php echo $j?> </td>
												<td class="kontentKolom" width="350px"><?php echo $nm_menudua?>  
													<input type="hidden" name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][menudet]'  id="menudua<?php echo $idmenusatu?><?php echo $j?>" value="<?php echo $id_menudua?>" />
													</td>
												<td class="kontentKolom" style="text-align:center">
												
												<input type='checkbox' id='viewAkses<?php echo $idmenusatu;?><?php echo $j;?>' name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][tampil]'  />
												 </td>
												<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox'  name='detail[".$x."][detaildua][".$j."][tambah]' id='viewTambah".$idmenusatu,$j."' >"?>
												 </td>
												<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox' name='detail[".$x."][detaildua][".$j."][ubah]' id='viewSunting".$idmenusatu,$j."' >"?>
												</td>
												<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox' name='detail[".$x."][detaildua][".$j."][hapus]' id='viewHapus".$idmenusatu,$j."' >"?>
												
												</td>
												<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox' name='detail[".$x."][detaildua][".$j."][approve]' id='viewProses".$idmenusatu,$j."' >"?>
												</td>
												<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox' name='detail[".$x."][detaildua][".$j."][cetak]' id='viewCetak".$idmenusatu,$j."' >"?>
												</td>
												
											</tr>
											
												<?php } else{ ?>
												
												
												
												<tr bgcolor="#3C8DBC">
												<td class="kontentKolom1"><?php echo $j?> </td>
												<td class="kontentKolom" width="350px"><?php echo $nm_menudua?> <?php echo $jmldua?>
													<input type="hidden" name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][menudet]'  id="menudua<?php echo $idmenusatu?><?php echo $j?>" value="<?php echo $id_menudua?>" />
													</td>
												<td class="kontentKolom" style="text-align:center">
												
												<input type='checkbox' id='viewAkses<?php echo $idmenusatu;?><?php echo $j;?>' name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][tampil]' onClick="checkAlltiga('viewAkses<?php echo $idmenusatu;?><?php echo $j;?>','viewAksestiga<?php echo $id_menudua;?>','<?php echo $jmldua;?>')"  />
												 </td>
												<td class="kontentKolom" style="text-align:center"><input type='checkbox' name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][tambah]' id='viewTambah<?php echo $idmenusatu;?><?php echo $j;?>'  onClick="checkAlltiga('viewTambah<?php echo $idmenusatu;?><?php echo $j;?>','viewTambahtiga<?php echo $id_menudua;?>','<?php echo $jmldua;?>')" /> 
												 </td>
												<td class="kontentKolom" style="text-align:center"><input type='checkbox' name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][ubah]' id='viewSunting<?php echo $idmenusatu;?><?php echo $j;?>'  onClick="checkAlltiga('viewSunting<?php echo $idmenusatu;?><?php echo $j;?>','viewSuntingtiga<?php echo $id_menudua;?>','<?php echo $jmldua;?>')" /> 
												</td>
												<td class="kontentKolom" style="text-align:center"><input type='checkbox' name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][hapus]' id='viewHapus<?php echo $idmenusatu;?><?php echo $j;?>'  onClick="checkAlltiga('viewHapus<?php echo $idmenusatu;?><?php echo $j;?>','viewHapustiga<?php echo $id_menudua;?>','<?php echo $jmldua;?>')" /> 
												</td>
												<td class="kontentKolom" style="text-align:center"><input type='checkbox' name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][approve]' id='viewProses<?php echo $idmenusatu;?><?php echo $j;?>'  onClick="checkAlltiga('viewProses<?php echo $idmenusatu;?><?php echo $j;?>','viewProsestiga<?php echo $id_menudua;?>','<?php echo $jmldua;?>')" /> 
												</td>
												<td class="kontentKolom" style="text-align:center"><input type='checkbox' name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][cetak]' id='viewCetak<?php echo $idmenusatu;?><?php echo $j;?>'  onClick="checkAlltiga('viewCetak<?php echo $idmenusatu;?><?php echo $j;?>','viewCetaktiga<?php echo $id_menudua;?>','<?php echo $jmldua;?>')" /> 
												</td>
											</tr>
												
												<?php
													$n=0;
													foreach($detmenudua[$idmenusatu][$id_menudua] as $datadua){
													$n++;
													$nm_menutiga=$datadua->menudes;
													$id_menutiga=$datadua->id;
												?>
													<tr >
														<td class="kontentKolom2"><?php echo $n?> </td>
														<td class="kontentKolom" width="350px"><?php echo $nm_menutiga?>
															<input type="text" name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][detailtiga][<?php echo $n;?>][menudettiga]'  id="menutiga<?php echo $id_menudua?><?php echo $n?>" value="<?php echo $id_menutiga?>" />
														</td>
														<td class="kontentKolom" style="text-align:center">
														<input type='checkbox' id='viewAksestiga<?php echo $id_menudua;?><?php echo $n;?>' name='detail[<?php echo $x;?>][detaildua][<?php echo $j;?>][detailtiga][<?php echo $n;?>][tampiltiga]'  />
														 </td>
														<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox'  name='detail[".$x."][detaildua][".$j."][detailtiga][".$n."][tambahtiga]' id='viewTambahtiga".$id_menudua,$n."' >"?>
														 </td>
														<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox' name='detail[".$x."][detaildua][".$j."][detailtiga][".$n."][ubahtiga]' id='viewSuntingtiga".$id_menudua,$n."' >"?>
														</td>
														<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox' name='detail[".$x."][detaildua][".$j."][detailtiga][".$n."][hapustiga]' id='viewHapustiga".$id_menudua,$n."' >"?>
														
														</td>
														<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox' name='detail[".$x."][detaildua][".$j."][detailtiga][".$n."][approvetiga]' id='viewProsestiga".$id_menudua,$n."' >"?>
														</td>
														<td class="kontentKolom" style="text-align:center"><?php echo "<input type='checkbox' name='detail[".$x."][detaildua][".$j."][detailtiga][".$n."][cetaktiga]' id='viewCetaktiga".$id_menudua,$n."' >"?>
														</td>
													</tr>
													
													<?php } ?>
												
												
												
												
												<?php } ?>
												
							
										<?php
										}
									
									?>
							</table>
							
							
							
						</div>
					</div>
				</div>	
					<?php } ?>
					
					
					
					
					
					
											
				
				
				
			</div>
		  
		  
		  
			</div>
		 
		 
		 
		 
		 
		 
		 
		 
		 
        </div>
          <div class="modal-footer">
            <button type="submit" id="btnSavemenu" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
  <!-- End Bootstrap modal
  
  

<script src="<?php echo base_url();?>js/atribut.js"></script>  -->