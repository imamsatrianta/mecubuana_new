

<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					echo aksesTambah();
					?>
					 <?php
					echo aksesHapus();
					?>
                    </div><table id="table" 
					data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
						   data-filter-control="true"
                           data-pagination="true"
                           data-url="identitasborang3b/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="desc">
						 <!--  data-detail-view="true"
						   data-detail-formatter="operateDetail"-->
                        <thead>	
						<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
							<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
							<th data-field="nm_jenjangprodi"  data-halign="center" data-align="left"  data-sortable="true" >Jenjang Prodi  </th>
							<th data-field="nm_prodi"  data-halign="center" data-align="left"  data-sortable="true" >Nama Prodi  </th>
                             <th data-field="nm_perguruantingi"  data-halign="center" data-align="left"  data-sortable="true" >Nama Perguruan Tinggi  </th>
							 <th data-field="no_telp_ps"  data-halign="center" data-align="left"  data-sortable="true" >No Telp </th> 
							  <th data-field="no_fak_ps"  data-halign="center" data-align="left"  data-sortable="true" >Fax  </th>
							 <th data-field="no_sk_pendirian"  data-halign="center" data-align="left"  data-sortable="true" >SK Pendirian  </th>
							<!-- 
							<th data-field="no_sk_pendirian"  data-halign="center" data-align="left"  data-sortable="true" >No SK Pendirian </th>
							 <th data-field="tgl_sk_pendirian"  data-halign="center" data-align="left"  data-sortable="true" >Tgl SK Pendirian  </th>
							 <th data-field="pejabat_penandatangan_sk"  data-halign="center" data-align="left"  data-sortable="true" >Pejabat Penandatangan SK  </th>
							<th data-field="no_sk_banpt"  data-halign="center" data-align="left"  data-sortable="true" >No SK BANPT  </th>
							 <th data-field="bulan_tahun_peneyelengara"  data-halign="center" data-align="left"  data-sortable="true" >Bulan Tahun Penyelengaraan  </th>
							 <th data-field="no_skijin_op"  data-halign="center" data-align="left"  data-sortable="true" >No SK Ijin Operasional  </th>
							 <th data-field="alamat_ps"  data-halign="center" data-align="left"  data-sortable="true" >Alamat PS  </th>
							 <th data-field="no_telp_ps"  data-halign="center" data-align="left"  data-sortable="true" >NO Telp PS  </th>
							 <th data-field="no_fak_ps"  data-halign="center" data-align="left"  data-sortable="true" >NO Fax PS  </th>
							 <th data-field="hp_dan_email_ps"  data-halign="center" data-align="left"  data-sortable="true" >HP dan Email PS  </th>-->
                        </tr>
						</thead>
                    </table>
                
              
            </div><!-- /.col -->
          </div>  
       
		
</div> <div class="modal fade" id="modal_form" role="dialog">
   <div class="modal-dialog" style="width:90%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="form" name="form" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
        
		<div class="row">
			<div class="col-md-6">
				<div class="form-group " id="jab">
					<label class="control-label col-md-4" for1="menudes">Jenjang Prodi  </label> 
					<div class="col-md-8">
						<select type="select" name="id_jenjang" class="form-control select2 input-sm" id="id_jenjang" required="required"  style="width: 100%;" onchange="getProdi('0')">
							 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
						</select>
					</div>
					 
									
				</div>
				
				<div class="form-group " id="jab">
					<label class="control-label col-md-4" for1="menudes">Prodi  </label> 
					<div class="col-md-8">
						<select type="select" name="id_prodi" class="form-control select2 input-sm" id="id_prodi" required="required"  style="width: 100%;" >
							 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
						</select>
					</div>
				</div>
				
				
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Nama Perguruan Tinggi  </label> 
					<div class="col-md-8">
						<input name="nm_perguruantingi" class="form-control input-sm" id="nm_perguruantingi"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Alamat PS  </label> 
					<div class="col-md-8">
						<input name="alamat_ps" class="form-control input-sm" id="alamat_ps"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">No Telp PS  </label> 
					<div class="col-md-8">
						<input name="no_telp_ps" class="form-control input-sm" id="no_telp_ps"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">No Fax PS  </label> 
					<div class="col-md-8">
						<input name="no_fak_ps" class="form-control input-sm" id="no_fak_ps"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Homepage</label> 
					<div class="col-md-8">
						<input name="homepage" class="form-control input-sm" id="homepage"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Email</label> 
					<div class="col-md-8">
						<input name="email" class="form-control input-sm" id="email"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Tanggal  </label> 
					<div class="col-md-8">
						<input type="text" class="form-control date-picker input-sm" id="tgl" name="tgl"  placeholder="yyyy-mm-dd" style="width: 100%;" >
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Nomor SK Pendirian</label> 
					<div class="col-md-8">
						<input name="no_sk_pendirian" class="form-control input-sm" id="no_sk_pendirian"  type="text">
					</div>
				</div>
				
				<div class="tab-pane active" id="tab_1">
						
						<div class="panel panel-default">
							<div class="panel-heading" >
							   <button type="button" id="btnNewRow" name="btnNewRow" class="btn btn-xs btn-success" onclick="addRow();"><i class="fa fa-plus"></i>
								Baris Baru
								</button>
							</div>
							
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" id="tabeleliminasi">
										<thead> 
											<tr>
												<td align="center" class="ganjil">Action </td>
												<td align="center" class="ganjil">Jenjang Prodi</td>
												<td align="center" class="ganjil">Prodi</td>
											</tr>
											
											<tr id="bookTemplate" name="rowda" class="hide">
												<td  style="text-align: center;" valign="top">
												<button type='button' style='text-align: center;padding:1px 3px;' id='btnDelRowX' name='btnDelRowX' class='btn btn-sm btn-danger btn-xs btn-round' title='Hapus' ><i class='fa fa-minus'></i></button> 
												</td>
												
												<td align="center" style="text-align: center;" class="form-group " valign="top">	
												<select type="select" name="id_jenjang_det" class="form-control  input-sm" id="id_jenjang_det" style="width: 100%;" >
													 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
												</select>
												</td>
												<td align="center" style="text-align: center;" class="form-group " valign="top">	
												<select type="select" name="id_prodi_det" class="form-control  input-sm" id="id_prodi_det" style="width: 100%;"  >
													 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
												</select>
												</td>
												
											</tr>
											
									
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
								<!-- /.table-responsive -->
								
							</div>
							<!-- /.panel-body -->
						</div>
					  </div>
				
			</div>
			
			<div class="col-md-6">
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Tanggal SK Pendirian  </label> 
					<div class="col-md-8">
						<input type="text" class="form-control date-picker input-sm" id="tgl_sk_pendirian" name="tgl_sk_pendirian"  placeholder="yyyy-mm-dd" style="width: 100%;" >
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Pejabat Penerbit SK  </label> 
					<div class="col-md-8">
						<input name="pejabat_penerbit_sk" class="form-control input-sm" id="pejabat_penerbit_sk"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Unit Pengelola  </label> 
					<div class="col-md-8">
						<input name="unit_pengelola" class="form-control input-sm" id="unit_pengelola"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Alamat Pengelola  </label> 
					<div class="col-md-8">
						<input name="alamat_pengelola" class="form-control input-sm" id="alamat_pengelola"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">No Telp Pengelola  </label>  
					<div class="col-md-8">
						<input name="no_telp_pengelola" class="form-control input-sm" id="no_telp_pengelola"  type="text">
						
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">No Fax Pengelola</label> 
					<div class="col-md-8">
						<input name="no_fax_pengelola" class="form-control input-sm" id="no_fax_pengelola"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Homepage Pengelola</label> 
					<div class="col-md-8">
						<input name="homepage_pengelola" class="form-control input-sm" id="homepage_pengelola"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Email Pengelola</label> 
					<div class="col-md-8">
						<input name="email_pengelola" class="form-control input-sm" id="email_pengelola"  type="text">
					</div>
				</div>
				
				
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Nomor SK Pengelola</label> 
					<div class="col-md-8">
						<input name="no_skpengelola" class="form-control input-sm" id="no_skpengelola"  type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Tanggal SK Pengelola  </label> 
					<div class="col-md-8">
						<input type="text" class="form-control date-picker input-sm" id="tgl_sk_pengelola" name="tgl_sk_pengelola"  placeholder="yyyy-mm-dd" style="width: 100%;" >
					</div>
				</div>
				
				<div class="form-group ">
					<label class="control-label col-md-4" for1="menudes">Pejabat SK Pengelola  </label> 
					<div class="col-md-8">
						<input name="pejabat_sk_pengelola" class="form-control input-sm" id="pejabat_sk_pengelola"  type="text">
					</div>
				</div>
				
				
			</div>
		
		
		
		
		
		  
			</div>
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> 
	
	
	<div class="modal fade" id="modal_formkeg" role="dialog">
   <div class="modal-dialog" style="width:50%">
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formkeg" name="formkeg" class="form-horizontal" onsubmit="return false;"     >
		<input type="hidden" value="" name="id_iddos" id="id_iddos"/> 
          <input type="hidden" value="" name="id_identitas" id="id_identitas"/> 
          <input type="hidden" value="" name="setkeg" id="setkeg"/> 
		 
		
		  <div class="form-body">
			  <div class="form-group field-username">
				  <label class="control-label col-md-4" for1="username">Nama Dosen </label> 
				  <div class="col-md-8">
				  <select type="select" name="id_dosen" class="form-control text-input input-sm" id="id_dosen"  style="width: 100%;"  >
					  <option value="">--Pilih--</option>
				   </select>
				
				  </div>
			  </div>
          </div>
		  <div class="form-group ">
			<label class="control-label col-md-4" for1="menudes">Tanggal Pengisian  </label> 
			<div class="col-md-8">
				<input type="text" class="form-control date-picker input-sm" id="tgl_pengisian" name="tgl_pengisian"  placeholder="yyyy-mm-dd" style="width: 100%;" >
			</div>
		</div>
		
		  </div>
          <div class="modal-footer">
            <button type="submit" id="btnSavekeg" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" id="btlkeg" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
	
	<script src="<?php echo base_url();?>js/atribut.js"></script>
  