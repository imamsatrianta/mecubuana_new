
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree/themes/default/easyui1.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree/themes/icon.css">
	<script type="text/javascript" src="<?php echo base_url();?>assets/tree/jquery.easyui.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					echo aksesTambahck();
					?>
					 <?php
					echo aksesHapus();
					?>
                    </div><table id="table" 
					data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
						   data-filter-control="true"
                           data-pagination="true"
                           data-url="settingborang/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="desc">
                        <thead>	
						<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
							<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
							<th data-field="standar"  data-halign="center" data-align="left"  data-sortable="true" >Borang Standar  </th>
							<th data-field="poin"  data-halign="center" data-align="left"  data-sortable="true" >Poin  </th>
							<th data-field="judul"  data-halign="center" data-align="left"  data-sortable="true" >Judul  </th>
							<th data-field="uraian"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="htmlFormat" >Uraian  </th>
                             <th data-field="jenis"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="jenisForm">Jenis  </th>
							 <th data-field="url"  data-halign="center" data-align="left"  data-sortable="true" >Url  </th>
							 <th data-field="poinind"  data-halign="center" data-align="left"  data-sortable="true" >Poin Induk  </th>
							 <th data-field="nm_jenjangprodi"  data-halign="center" data-align="left"  data-sortable="true" >Jenjang Prodi  </th>
							 <th data-field="no"  data-halign="center" data-align="left"  data-sortable="true" >No  </th>
							 <th data-field="level"  data-halign="center" data-align="left"  data-sortable="true" >Level  </th>
                        </tr>
						</thead>
                    </table>
                
              
            </div><!-- /.col -->
          </div>  
       
		
</div> <div class="modal fade" id="modal_formck" role="dialog">
  <div class="modal-dialog" style="width:80%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formck" name="formck" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
          
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Borang Standar </label> 
			<div class="col-md-3">
				<select class="form-control input-sm" id="standar" name="standar" style="width: 100%;" >
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
				</select>
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Poin  </label> 
			<div class="col-md-3">
				<input name="poin" class="form-control input-sm" id="poin" required="required" type="text">
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Judul </label> 
			<div class="col-md-9">
				<input name="judul" class="form-control input-sm" id="judul" required="required" type="text">
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Jenis Form </label> 
			<div class="col-md-3">
				<select class="form-control input-sm" id="jenis" name="jenis" style="width: 100%;" onchange="cekJenis()">
					<option value="0">Tanpa Form</option>
					<option value="1">Form Input</option>
					<option value="2">Dari System Lain</option>
					
				</select>
		    </div>
		</div>
		<div class="form-group " id="ur">
			<label class="control-label col-md-3" for1="menudes">Url </label> 
			<div class="col-md-9">
				<input name="url" class="form-control input-sm" id="url"  type="text">
		    </div>
		</div>
		
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Jenjang Prodi </label> 
			<div class="col-md-3">
				<select type="select" name="id_jenjangprodi" class="form-control select2 input-sm" id="id_jenjangprodi" required="required"  style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
				</select>
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Level Poin </label> 
			<div class="col-md-3">
				<select class="form-control input-sm" id="level" name="level" style="width: 100%;" >
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
				</select>
		    </div>
		</div>
		
		<div class="form-group ">	
			<label class="control-label col-md-3" for1="menudes">Poin Induk    </label> 
			<div class="col-md-9">
				 <input class="easyui-combotree" name="parent_id" id="parent_id"  data-options="url:'settingborang/getCombo',method:'get'" style="width: auto;height: 24px;padding: 6px 12px; font-size: 14px;" >
				
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">No </label> 
			<div class="col-md-3">
				<input name="no" class="form-control input-sm" id="no" required="required" type="text">
		    </div>
		</div>
		
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Uraian  </label> 
			<div class="col-md-9">
				<textarea class="form-control" rows="5" id="uraianck" name="uraian"></textarea>
		    </div>
		</div>
		  
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> 
  <script>
	function tambahDatack(){
		var halaman=document.getElementById("judulmenu").innerHTML; 
		//alert(halaman);
		 
     $('#formck')[0].reset(); 
	 $('#formck').bootstrapValidator('resetForm', true);
      $('#modal_formck').modal('show'); 
	  /* $('#modal_form').modal('show')
              .draggable({ handle: ".modal-header" });
			  */
      $('.modal-title').text('Form Tambah Parameter Borang 3B'); 
	  document.getElementById('set').value=0;
	  document.getElementById('btnSave').disabled=false;
	}
	
	function editFormck(row){
		var halaman=document.getElementById("judulmenu").innerHTML; 
        $('#formck')[0].reset(); 
      $('#modal_formck').modal('show'); 
      $('.modal-title').text('Form Ubah Parameter Borang 3B'); 
	  document.getElementById('set').value=1;
	  document.getElementById('btnSave').disabled=false;
	  
		 var buttonedit=document.getElementById('buttonedit').value;
		 
	    for (var name in row) {
		//alert(name);
			 $('#modal_formck').find('input[name="' + name + '"]').val(row[name]);
        }
		//alert(row.uraian);
		CKEDITOR.instances['uraianck'].setData(row.uraian);
		if (buttonedit==1){
			//alert("s");
			editFormtambah(row);
		}
	}
	
	$('#formck')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDatack();
		 e.preventDefault();
		 
		 
    });
	
	
	function simpanDatack(){
	 var edituraian = CKEDITOR.instances.uraianck;
	var uraianok=edituraian.getData();
	var uraian = uraianok.replace("&nbsp;", "");
	
	 document.getElementById("btnSave").disabled=true;
	 var fd = new FormData(document.getElementById("formck"));
			fd.append("uraianck",uraian);
			$.ajax({
				 url: "settingborang/simpanData",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						  keluarLogin();
					}
					
					tutupFormckdata();
					loadData(1, 15,'desc');
			});
			return false;	
			
	 
 }
 
	function tutupFormckdata(){
		//alert("c")
		// $('#formck')[0].reset(); // reset form on modals 
      	$('#modal_formck').modal('hide'); // show bootstrap modal
		
	}
	 $("#tbh").click(function () {
		CKEDITOR.instances['uraianck'].setData("" );
    });
 
	function strip_html_tags(str)
{
   if ((str===null) || (str===''))
       return false;
  else
   str = str.toString();
  return str.replace(/<[^>]*>/g, '');
}


function htmlFormat(value, row, index){
	var mutu=row.uraian;
	
	var res = mutu.substr(0, 100);
	return strip_html_tags(res);
}
	
	 $(document).ready(function ($) {
		 // alert("s")
		 jenjangProdi();
			 $("#tbh").click(function(){
			 var set=$("#set").val();
			 if(set=="0"){
				 $('#parent_id').combotree('setValue',0);
				 $("#ur").hide();
			 }
			});
		CKEDITOR.replace('uraianck');
		
		 });
		 function cekJenis(){
			 var jenis=$("#jenis").val();
			 if(jenis=="2"){
				 $("#ur").show();
			 }else{
				  $("#ur").hide();
				  $("#url").val("");
			 }
		 }
		 function jenjangProdi(){
			
			 $("#id_jenjangprodi").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getJenjangprodi",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_jenjangprodi").append('<option value="'+val.id+'">'+val.nm_jenjangprodi+'</option>');
					
				});
								  
				}
				});
		 }
	  function editFormtambah(row){
		  $('#parent_id').combotree('setValue',row.parent_id);
		   $("#uraian").val(row.uraian);
		   $("#jenis").val(row.jenis);
		   $("#id_jenjangprodi").val(row.id_jenjangprodi);
		   $("#level").val(row.level);
		   $("#standar").val(row.standar);
		   var jenis=row.jenis;
		   if(jenis=="2"){
				 $("#ur").show();
			 }else{
				  $("#ur").hide();
				  $("#url").val("");
			 }
			 
		   
	  }
  function operateFormatter(value, row, index) {
      return [
			'<?php echo aksesUbahck() ?>',
			'<?php echo aksesHapussatu() ?>'
        ].join('');
    }
 function isiKolom(value, row, index){
	    var res = value.substr(0, 50);
		return res;
 }
 function jenisForm(value, row, index){
	 if(value=="0"){
		 return "Tanpa Form";
	 }else if(value=="1"){
		 return "Form Input";
	 }else{
		 return "Dari System Lain";
	 }
 }
  </script>
  
  
  <script src="<?php echo base_url();?>js/atribut.js"></script>