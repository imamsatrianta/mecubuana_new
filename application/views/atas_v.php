<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 --> 
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrapadmin.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/Font-Awesome-master/css/font-awesome.min.css">
	  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/AdminLTE.css">
	   <link rel="stylesheet" href="<?php echo base_url();?>assets/css/skins/_all-skins.css">
	 <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-table.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/plugin/select2/select2.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugin/timepicker/bootstrap-timepicker.min.css">
	
	
	
	<script src="<?php echo base_url();?>assets/js/jquery-2.2.3.min.js"></script>
	

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	    <script src="<?php echo base_url();?>assets/js/app.js"></script>
    <!-- templet for demo purposes -->
    <script src="<?php echo base_url();?>assets/js/demo.js"></script>
	
	 <script src="<?php echo base_url();?>assets/js/bootstrap-table.js"></script>
 <script src="<?php echo base_url();?>assets/js/toastr.js"></script>
 <link href="<?php echo base_url();?>assets/css/toastr.css" rel="stylesheet" type="text/css" />
	 <script src="<?php echo base_url();?>assets/plugin/select2/select2.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrapValidator.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootbox.js"></script>
	<script src="<?php echo base_url();?>assets/js/function.js"></script>
	<script src="<?php echo base_url();?>assets/plugin/datepicker/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url();?>assets/plugin/timepicker/bootstrap-timepicker.min.js"></script>
    </head>
	
	<style>
	.dropdown-submenu {
position: relative;
}

.dropdown-submenu>.dropdown-menu {
top: 0;
left: 100%;
margin-top: -6px;
margin-left: -1px;
-webkit-border-radius: 0 6px 6px 6px;
-moz-border-radius: 0 6px 6px;
border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
display: block;
}
.dropdown-submenu>a:after {
display: block;
content: " ";
float: right;
width: 0;
height: 0;
border-color: transparent;
border-style: solid;
border-width: 5px 0 5px 5px;
border-left-color: #ccc;
margin-top: 5px;
margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
border-left-color: #fff;
}
	</style>
   <body class="hold-transition skin-blue-light layout-top-nav">
    <div class="wrapper">
		<header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="../../index2.html" class="navbar-brand"><b><!--MECUBUANA --> </b></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            
			<?php echo menuAksescoba(); ?>
		
			 <!-- 
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Seting System <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo base_url();?>masterdata/user?idm=<?php echo paramEncrypt("7")?>"> User</a></li>
                <li><a href="<?php echo base_url();?>masterdata/menu?idm=<?php echo paramEncrypt("2")?>">Menu</a></li>
				<li><a href="<?php echo base_url();?>masterdata/grupmenu?idm=<?php echo paramEncrypt("2")?>">Grup Menu</a></li>
                <li><a href="<?php echo base_url();?>masterdata/rol?idm=<?php echo paramEncrypt("5")?>">Menu Akses</a></li>
                
              </ul>
            </li>
			
			<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Maste Data <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo base_url();?>master/tahunakademik">Tahun Akademik</a></li>
                <li><a href="#">Rektorat</a></li>
                <li><a href="#">Direktorat</a></li>
				<li><a href="#">Fakultas</a></li>
				<li><a href="#">Prodi</a></li>
				<li><a href="#">Jurusan</a></li>
                
              </ul>
            </li>
			<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">SARMUT <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Input Sarmut</a></li>
                <li><a href="#">Realisais Sarmut</a></li>
                <li><a href="#">Laporan Rencana Sarmut</a></li>
				<li><a href="#">Laporan Realisais Sarmut</a></li>
              </ul>
            </li>-->
			
          </ul>
         
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            
          
           
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="#" class="user-image" alt="">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs">ADMIN</span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                
               
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Ubah Password</a>
                  </div>
                  <div class="pull-right">
                    <a href="#" onclick="logoutData()" class="btn btn-default btn-flat">Keluar</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
	 
	 
	  
	  
</body>
</html>

<script>

function logoutData(){
		 bootbox.confirm("Yakin akan menutup aplikasi?", function(result) {
			 if(result==true){
				 document.location='<?php echo base_url().'ad_log/logout'?>';
			 }
			 
			  });
		 
	 }
	 function keluarLogin(){
		document.location='<?php echo base_url().'ad_log/logout'?>';
	}
</script>