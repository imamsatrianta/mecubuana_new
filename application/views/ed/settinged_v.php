
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree/themes/default/easyui1.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree/themes/icon.css">
	<script type="text/javascript" src="<?php echo base_url();?>assets/tree/jquery.easyui.min.js"></script>
	
	
<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
                    <div id="toolbar">
                    <?php
					echo aksesTambah();
					?>
					 <?php
					echo aksesHapus();
					?>
                    </div><table id="table" 
					data-toolbar="#toolbar"
                           data-toggle="table"
                           data-search="true"
                           data-show-refresh="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
						   data-filter-control="true"
                           data-pagination="true"
                           data-url="settinged/loaddataTabel"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="desc">
                        <thead>	
						<tr>
                            <th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
							<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
							<th data-field="nm_group"  data-halign="center" data-align="left"  data-sortable="true" >Group ED  </th>
							<th data-field="poin"  data-halign="center" data-align="left"  data-sortable="true" >Poin  </th>
							<th data-field="nm_evaluasidiri"  data-halign="center" data-align="left"  data-sortable="true" >Nama ED  </th>
							 <th data-field="poinind"  data-halign="center" data-align="left"  data-sortable="true" >ED Induk  </th>
							 <th data-field="nm_jenjangprodi"  data-halign="center" data-align="left"  data-sortable="true" >Jenjang Prodi  </th>
							 <th data-field="no"  data-halign="center" data-align="left"  data-sortable="true" >No  </th>
							 <th data-field="level"  data-halign="center" data-align="left"  data-sortable="true" >Level  </th>
                        </tr>
						</thead>
                    </table>
                
              
            </div><!-- /.col -->
          </div>  
       
		
</div> <div class="modal fade" id="modal_form" role="dialog">
   <div class="modal-dialog" style="width:70%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="form" name="form" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
          
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Group ED </label> 
			<div class="col-md-9">
				<select class="form-control input-sm" id="id_group" name="id_group" style="width: 100%;" >
					<option <?php echo 'selected';?> value=''>--Pilih-- </option>
				</select>
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Poin  </label> 
			<div class="col-md-3">
				<input name="poin" class="form-control input-sm" id="poin" required="required" type="text">
		    </div>
		</div>
		
		
		
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Jenjang Prodi </label> 
			<div class="col-md-9">
				<select type="select" name="id_jenjangprodi" class="form-control select2 input-sm" id="id_jenjangprodi" required="required"  style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
				</select>
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Level Poin </label> 
			<div class="col-md-9">
				<select class="form-control input-sm" id="level" name="level" style="width: 100%;" >
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
				</select>
		    </div>
		</div>
		
		<div class="form-group ">	
			<label class="control-label col-md-3" for1="menudes">ED Induk    </label> 
			<div class="col-md-9">
				 <input class="easyui-combotree" name="parent_id" id="parent_id"  data-options="url:'settinged/getCombo',method:'get'" style="width: 400px;height: 24px;padding: 6px 12px; font-size: 14px;" >
				
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">No </label> 
			<div class="col-md-3">
				<input name="no" class="form-control input-sm" id="no" required="required" type="text">
		    </div>
		</div>
		
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Nama ED  </label> 
			<div class="col-md-9">
				<textarea class="form-control" rows="5" id="nm_evaluasidiri" name="nm_evaluasidiri"></textarea>
		    </div>
		</div>
		  
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form> 
  <script>
	 $(document).ready(function ($) {
		 // alert("s")
		 jenjangProdi();
		 groupEd();
			 $("#tbh").click(function(){
			 var set=$("#set").val();
			 if(set=="0"){
				 $('#parent_id').combotree('setValue',0);
			 }
			});
		
		
		 });
	
		 function jenjangProdi(){
			
			 $("#id_jenjangprodi").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getJenjangprodi",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_jenjangprodi").append('<option value="'+val.id+'">'+val.nm_jenjangprodi+'</option>');
					
				});
								  
				}
				});
		 }
		 
		  function groupEd(){
			
			// $("#id_group").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getGrouped",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_group").append('<option value="'+val.id+'">'+val.nm_group+'</option>');
					
				});
								  
				}
				});
		 }
		 
	  function editFormtambah(row){
		  $('#parent_id').combotree('setValue',row.parent_id);
		   $("#nm_evaluasidiri").val(row.nm_evaluasidiri);
		   $("#id_group").val(row.id_group);
		   $("#id_jenjangprodi").val(row.id_jenjangprodi);
		   $("#level").val(row.level);
		  
		   
	  }
  function operateFormatter(value, row, index) {
      return [
			'<?php echo aksesUbah() ?>',
			'<?php echo aksesHapussatu() ?>'
        ].join('');
    }

  </script>
  
  
  <script src="<?php echo base_url();?>js/atribut.js"></script>