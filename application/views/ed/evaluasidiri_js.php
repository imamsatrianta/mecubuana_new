
	<script>
var parameter="<?php echo $_GET['parameter'];?>";
var level="<?php echo $_GET['level'];?>";
var levelprodi="<?php echo $this->session->userdata('levelprodi');?>";
function loadDataborang(number, size,order){
			var $table = $('#table');
            var offset=(number - 1) * size;
            var limit=size;
			
            $.ajax({
                    type: "POST",
                    url: "evaluasidiri/loaddataTabel?order="+order+"&limit="+limit+"&offset="+offset+"&parameter="+parameter+"&level="+level,
                    dataType:"JSON",
                    success: function(result){
    				 $table.bootstrapTable('load', result);
    			
                    }
                });
        }
		
function getParamevaluasidiri(params) {
	//	alert("s")
		    return {
				level: level,
		        parameter: parameter,
				limit: params.limit,
				offset: params.offset,
				search: params.search,
				sort: params.sort,
				order: params.order
		    };
		}
function strip_html_tags(str)
{
   if ((str===null) || (str===''))
       return false;
  else
   str = str.toString();
  return str.replace(/<[^>]*>/g, '');
}


function htmlMutu(value, row, index){
	var mutu=row.uraian;
	var res = mutu.substr(0, 200);
	return strip_html_tags(res);
}

	 function operateFormatterborang(value, row, index) {
		 if(levelprodi!="2"){
			 return [
			'<a class="btn btn-sm btn-primary btn-xs" id="refisi" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Evaluasi" ><i id="edt" class="glyphicon glyphicon-pencil" ></i></a> ',
			'<a class="btn btn-sm btn-primary btn-xs" id="editborang" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Ubah" ><i id="edt" class="glyphicon glyphicon-edit" ></i></a> ',
			'<a class="btn btn-sm btn-danger btn-xs" id="hapusborang" href="javascript:void(0)" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a> '
			
        ].join('');
		 }else{
       return [
			'<a class="btn btn-sm btn-primary btn-xs" id="editborang" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Ubah" ><i id="edt" class="glyphicon glyphicon-edit" ></i></a> ',
			'<a class="btn btn-sm btn-danger btn-xs" id="hapusborang" href="javascript:void(0)" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a> '
			
        ].join('');
	 }
       
	}
	
	window.operateEventsborang = {
        'click #kebijakan': function (e, value, row, index) {
				formKebijakan(row);
        },'click #editborang': function (e, value, row, index) {
				editFormborang(row);
        },'click #hapusborang': function (e, value, row, index) {
				hapusBorang(row.id);
        },'click #refisi': function (e, value, row, index) {
				refisiForm(row);
        }
    };
	
	
	  $(document).ready(function ($) {
		  readyToStart();
		getTahunakademik();
		 //CKEDITOR.replace('uraian');
		 var editor = CKEDITOR.replace( 'editor1', {
			filebrowserBrowseUrl : '<?php echo base_url();?>assets/ckfinder/ckfinder.html',
			filebrowserImageBrowseUrl : '<?php echo base_url();?>assets/ckfinder/ckfinder.html?type=Images',
			filebrowserFlashBrowseUrl : '<?php echo base_url();?>assets/ckfinder/ckfinder.html?type=Flash',
			filebrowserUploadUrl : '<?php echo base_url();?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
			filebrowserImageUploadUrl : '<?php echo base_url();?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
			filebrowserFlashUploadUrl : '<?php echo base_url();?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
		});
		CKFinder.setupCKEditor( editor, '' );
		

		$(".date-picker").datepicker({ autoclose: true});
	  });
	  
	 
	function hapusBorang(id){
		bootbox.confirm("Yakin Data akan di Hapus?", function(result) {
			if(result==true){
				 $.ajax({
                    url: 'evaluasidiri/hapusData?id='+id,
                    type: 'POST',
                    success: function (data) {
					//alert(data);
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
					
						
						loadDataborang(1, 15,'desc');
                        
                    }
                })
			}
               
        });
	}
	function tambahDataborang(){
     $('#formbor')[0].reset(); 
	 $('#formbor').bootstrapValidator('resetForm', true); 
      $('#modal_formbor').modal('show'); 
      $('.modal-title').text('Form Tambah Evaluasi Diri '); 
	  document.getElementById('set').value=0;
	  document.getElementById('btnSave').disabled=false;
	 $("#id_parameter").val("<?php echo $_GET['parameter'];?>");
	 $("#id_jenjangprodi").val("<?php echo $_GET['level'];?>");	
	 var jenjang="<?php echo $_GET['level'];?>";
	 getProdi(jenjang,0);
	 CKEDITOR.instances['editor1'].setData("" );
	 $("#cke_1_contents").css({"height":"400px"});
	 
    }
	
	function editFormborang(row){
        $('#formbor')[0].reset(); 
      $('#modal_formbor').modal('show'); 
      $('.modal-title').text('Form Ubah Evaluasi Diri '); 
	  document.getElementById('set').value=1;
	  document.getElementById('btnSave').disabled=false;
	  
		 var buttonedit=document.getElementById('buttonedit').value;
		 
	    for (var name in row) {
		//alert(name);
			 $('#modal_formbor').find('input[name="' + name + '"]').val(row[name]);
        }
		$("#id_tahunakademik").val(row.id_tahunakademik);
		
		
		
		getProdi(row.id_jenjangprodi,row.id_prodi);
		CKEDITOR.instances['editor1'].setData(row['uraian'] );
		$("#cke_1_contents").css({"height":"400px"});
		$('#formbor').bootstrapValidator('resetForm',false);
		
    }
	
	function getTahunakademik(){
		$("#id_tahunakademik").append('<option value="">Pilih</option>');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getTahunakademik",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_tahunakademik").append('<option value="'+val.id+'">'+val.nm_tahun_akademik+'</option>');
					
				});
			
								  
				}
				});
	  
	}
	
	function tutupFormborang(){
		 $('#formbor')[0].reset(); // reset form on modals 
      	$('#modal_formbor').modal('hide'); // show bootstrap modal
	}
	
	function getProdi(jenjang,id_prodi){
		$("#id_prodi").html('');
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getProdijenjang/"+jenjang,
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_prodi").append('<option value="'+val.id+'">'+val.nm_prodi+'</option>');
					
				});
					if(id_prodi!='0'){
						$("#id_prodi").val(id_prodi);
					}
					
				}
				});	
	}
	
	$('#formbor')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDataborang();
		 e.preventDefault();
		 
		 
    });
	
	function simpanDataborang(){
	var edituraian = CKEDITOR.instances.editor1;
	var uraianok=edituraian.getData();
	var uraian = uraianok.replace("&nbsp;", "");
 
	 document.getElementById("btnSave").disabled=true;
	 var fd = new FormData(document.getElementById("formbor"));
			fd.append("uraiandata",uraian);
			$.ajax({
				 url: "evaluasidiri/simpanData",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						//  keluarLogin();
					}
					
					tutupFormborang();
					loadDataborang(1, 15,'desc');
			});
			return false;	
			
	 
	}
	
	function refisiForm(row){
		 $('#formref')[0].reset(); 
		 $('#formref').bootstrapValidator('resetForm', true);
		  $('#modal_formref').modal('show'); 
		  $('.modal-title').text('Form Evaluasi Diri'); 
		  document.getElementById('btnSaveref').disabled=false;
		   $('#id_ref').val(row.id); 
		   $('#id_prodi_ref').val(row.id_prodi); 
	}
	
	$('#formref')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDatarefisi();
		 e.preventDefault();
		 
		 
    });
	
	
	function simpanDatarefisi(){
	
	 document.getElementById("btnSaveref").disabled=true;
	 var fd = new FormData(document.getElementById("formref"));
			fd.append("kegiatanrel",'');
			$.ajax({
				 url: "evaluasidiri/simpanDatarefisi",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						  keluarLogin();
					}
					
					tutupFormref();
					loadDataborang(1, 15,'desc');
			});
			return false;	
			
	 
 }
 function tutupFormref(){
	 $('#formref')[0].reset(); // reset form on modals 
      	$('#modal_formref').modal('hide'); // show bootstrap modal
 }

	

  function statusBorang(value, row, index){
	if(row.status=="3"){
		return "Dikembalikan";
	}else if(row.status=="2"){
		return "Disetujui";
	}else if(row.status=="1"){
		return "Diajukan";
	}else{
		return "Baru";
	}
}

function statusEvaluasi(value, row, index){
	if(row.status_evaluasi=="2"){
		return "PAEE";
	}else if(row.status_evaluasi=="1"){
		return "Wakil Dekan/Dekan";
	}else{
		return "Prodi";
	}
}
	

	
	
		
	
		
	
	



	</script>
 
  