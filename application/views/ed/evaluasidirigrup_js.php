
	<script>
var parameter="<?php echo $_GET['parameter'];?>";
var level="<?php echo $_GET['level'];?>";
var levelprodi="<?php echo $this->session->userdata('levelprodi');?>";
function loadDataborang(number, size,order){
			var $table = $('#table');
            var offset=(number - 1) * size;
            var limit=size;
			
            $.ajax({
                    type: "POST",
                    url: "evaluasidirigrup/loaddataTabel?order="+order+"&limit="+limit+"&offset="+offset+"&parameter="+parameter+"&level="+level,
                    dataType:"JSON",
                    success: function(result){
    				 $table.bootstrapTable('load', result);
    			
                    }
                });
        }
		
function getParamevaluasidiri(params) {
	//	alert("s")
		    return {
				level: level,
		        parameter: parameter,
				limit: params.limit,
				offset: params.offset,
				search: params.search,
				sort: params.sort,
				order: params.order
		    };
		}
function strip_html_tags(str)
{
   if ((str===null) || (str===''))
       return false;
  else
   str = str.toString();
  return str.replace(/<[^>]*>/g, '');
}


function htmlMutu(value, row, index){
	var mutu=row.uraian;
	return strip_html_tags(mutu);
}

	 function statusBorang(value, row, index){
	if(row.status=="3"){
		return "Dikembalikan";
	}else if(row.status=="2"){
		return "Disetujui";
	}else if(row.status=="1"){
		return "Diajukan";
	}else{
		return "Baru";
	}
}
function statusEvaluasi(value, row, index){
	if(row.status_evaluasi=="2"){
		return "PAEE";
	}else if(row.status_evaluasi=="1"){
		return "Wakil Dekan/Dekan";
	}else{
		return "Prodi";
	}
}

	
	
	 function operateFormatterborangevl(value, row, index) {
			 return [
			'<a class="btn btn-sm btn-primary btn-xs" id="refisiev" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Evaluasi" ><i id="edt" class="glyphicon glyphicon-pencil" ></i></a> ',
			'<a class="btn btn-sm btn-primary btn-xs" id="cetak" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Cetak Evaluasi" ><i id="nilai" class="fa fa-file-word-o" ></i></a> '
			].join('');
		
	}
	
	window.operateEventsborangevl = {
        'click #refisiev': function (e, value, row, index) {
				refisiFormevaluasi(row);
        },'click #cetak': function (e, value, row, index) {
				//penilaianData(row);
				cetakEvaluasi(row);
        }
    };
	
	function cekEvaluasi(){
		var status_evaluasi=$('#status_evaluasi').val(); 
		if(status_evaluasi=="0"){
			$("#cekkirim").show();
			$(".date-picker").datepicker({ autoclose: true});
		}else{
			$("#cekkirim").hide();
			$('#tgl_dodate').val(''); 
		}
	}
	function refisiFormevaluasi(row){
		$("#cekkirim").hide();
		$('#tgl_dodate').val(''); 
		
		 $('#formevl')[0].reset(); 
		 $('#formevl').bootstrapValidator('resetForm', true);
		  $('#modal_formevl').modal('show'); 
		  $('.modal-title').text('Form Evaluasi Borang'); 
		  document.getElementById('btnSaveevl').disabled=false;
		   $('#id_tahunakademikevl').val(row.id_tahunakademik); 
		   $('#id_jenjangprodievl').val(row.id_jenjangprodi); 
		    $('#id_prodievl').val(row.id_prodi); 
			
	}
	
	$('#formevl')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDataevaluasi();
		 e.preventDefault();
		 
		 
    });
	
	
	function simpanDataevaluasi(){
	
	 document.getElementById("btnSaveevl").disabled=true;
	 var fd = new FormData(document.getElementById("formevl"));
			fd.append("kegiatanrel",'');
			$.ajax({
				 url: "evaluasidirigrup/simpanDatareevaluasi",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						  keluarLogin();
					}
					
					tutupFormevl();
					loadDataborang(1, 15,'desc');
			});
			return false;	
			
	 
 }
 function tutupFormevl(){
	 $('#formevl')[0].reset(); // reset form on modals 
      	$('#modal_formevl').modal('hide'); // show bootstrap modal
 }	
	
		
function cetakEvaluasi(row){
	var id_jenjangprodi=row.id_jenjangprodi;
		var id_tahunakademik=row.id_tahunakademik;
		var id_prodi=row.id_prodi;
		//alert(standarda)
		window.open("<?php echo base_url(); ?>ed/evaluasidirigrup/cetakEvaluasi?id_jenjangprodi="+id_jenjangprodi+"&id_tahunakademik="+id_tahunakademik+"&id_prodi="+id_prodi);
}
	



	</script>
 
  