<div class="content-wrapper" style="min-height:293px;" >
	<section class="content" style="background-color: #ecf0f5;">
		<div class="row" style="background-color: #ecf0f5;" >
			<div class="col-md-2">
					<section class="sidebar" style="margin-top:-6%;margin-left:-6%;border: 0px solid black;">
						<ul class="sidebar-menu" data-widget="tree" >
						
						<?php
							foreach($grupmenu as $data){ 
								$id=$data->id; 
								$nm_group=$data->nm_group;
							?>
						<li class="treeview">
						  <a href="#">
							<i class="fa fa-folder"></i>
							<span><?php echo $nm_group;?></span>
							<span class="pull-right-container">
							  <i class="fa fa-angle-left pull-right"></i>
							</span>
						  </a>
						  
							<?php
							foreach($menu[$id] as $mn){ 
								$nm_evaluasidiri=$mn->nm_evaluasidiri;
								$poin=$mn->poin; 
								$id_parameter=$mn->id;
								$menu_det=$mn->menu_det;
								
								if($id=="3"){
									if($menu_det=="1"){
										
										
								?>
									<ul class="treeview-menu">
										<li class="treeview">
										  <a href="#">
											<i class="fa fa-folder"></i>
											<span><?php echo $nm_evaluasidiri;?></span>
											<span class="pull-right-container">
											  <i class="fa fa-angle-left pull-right"></i>
											</span>
										  </a>
										  
										  <ul class="treeview-menu">
											<li><a href="<?php echo base_url();?>ed/evaluasidiri?parameter=<?php echo $id_parameter;?>&judul=<?php echo $nm_evaluasidiri;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-arrow-right"></i> <?php echo $nm_evaluasidiri;?></a></li>
										   </ul>
										   
										   <?php
											foreach($menudua[$id][$id_parameter] as $mndua){
												$nm_evaluasidiridua=$mndua->nm_evaluasidiri;
												$id_parameterdua=$mndua->id;
												
										   ?>
												<ul class="treeview-menu">
													<li><a href="<?php echo base_url();?>ed/evaluasidiri?parameter=<?php echo $id_parameterdua;?>&judul=<?php echo $nm_evaluasidiridua;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-arrow-right"></i> <?php echo $nm_evaluasidiridua;?></a></li>
												 </ul>
											<?php } ?>
										</li>
									</ul>
									<?php }else{ ?>
								<ul class="treeview-menu">
									<li><a href="<?php echo base_url();?>ed/evaluasidiri?parameter=<?php echo $id_parameter;?>&judul=<?php echo $nm_evaluasidiri;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-arrow-right"></i> <?php echo $nm_evaluasidiri;?></a></li>
								</ul>
								
									<?php } ?>
							
							
								<?php }else{ ?>
							  <ul class="treeview-menu">
								<li><a href="<?php echo base_url();?>ed/evaluasidiri?parameter=<?php echo $id_parameter;?>&judul=<?php echo $nm_evaluasidiri;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-arrow-right"></i> <?php echo $nm_evaluasidiri;?></a></li>
							  </ul>
							  
							<?php } ?>
							<?php } ?>
						</li>
						
						<?php } ?>
						
						<!--
						
						
							<?php
							foreach($menu as $data){ 
								$poin=$data->poin; 
								$id_parameter=$data->id;
								$jenis=$data->jenis;
								$judul=$data->judul; 
								$standar=$data->standar; 
								$nama=$poin;
								$url=$data->url;
								if($standar=="1"){
									if($jenis=="1"){
								?>
									<li><a title="<?php echo $judul;?>" href="<?php echo base_url();?>borang/standarsatu?parameter=<?php echo $id_parameter;?>&judul=<?php echo $judul;?>&jenis=<?php echo $jenis;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-th text-red"></i> <span><?php echo $nama;?></span></a></li>
								<?php
									}
									else{
										?>
										<li><a title="<?php echo $judul;?>" href="<?php echo base_url();?><?php echo $url;?>?jenis=<?php echo $jenis;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-th text-red"></i> <span><?php echo $nama;?></span></a></li>
									<?php
									}
								}else if($standar=="2"){
									if($jenis=="1"){
									?>
									<li><a title="<?php echo $judul;?>" href="<?php echo base_url();?>borang/standardua?parameter=<?php echo $id_parameter;?>&judul=<?php echo $judul;?>&jenis=<?php echo $jenis;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-th text-red"></i> <span><?php echo $nama;?></span></a></li>
								<?php
									}
									else{
										?>
										<li><a title="<?php echo $judul;?>" href="<?php echo base_url();?><?php echo $url;?>?jenis=<?php echo $jenis;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-th text-red"></i> <span><?php echo $nama;?></span></a></li>
									<?php
									}
								}else if($standar=="3"){
									if($jenis=="1"){
								?>
									<li><a title="<?php echo $judul;?>" href="<?php echo base_url();?>borang/standartiga?parameter=<?php echo $id_parameter;?>&judul=<?php echo $judul;?>&jenis=<?php echo $jenis;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-th text-red"></i> <span><?php echo $nama;?></span></a></li>
								<?php
									}
									else{
										?>
										<li><a title="<?php echo $judul;?>" href="<?php echo base_url();?><?php echo $url;?>?jenis=<?php echo $jenis;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-th text-red"></i> <span><?php echo $nama;?></span></a></li>
									<?php
									}
								}else if($standar=="4"){
									if($jenis=="1"){
								?>
									<li><a title="<?php echo $judul;?>" href="<?php echo base_url();?>borang/standarempat?parameter=<?php echo $id_parameter;?>&judul=<?php echo $judul;?>&jenis=<?php echo $jenis;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-th text-red"></i> <span><?php echo $nama;?></span></a></li>
								<?php
									}
									else{
										?>
										<li><a title="<?php echo $judul;?>" href="<?php echo base_url();?><?php echo $url;?>?jenis=<?php echo $jenis;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-th text-red"></i> <span><?php echo $nama;?></span></a></li>
									<?php
									}
								}else if($standar=="5"){
									if($jenis=="1"){
								?>
									<li><a title="<?php echo $judul;?>" href="<?php echo base_url();?>borang/standarlima?parameter=<?php echo $id_parameter;?>&judul=<?php echo $judul;?>&jenis=<?php echo $jenis;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-th text-red"></i> <span><?php echo $nama;?></span></a></li>
								<?php
									}
									else{
										?>
										<li><a title="<?php echo $judul;?>" href="<?php echo base_url();?><?php echo $url;?>?jenis=<?php echo $jenis;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-th text-red"></i> <span><?php echo $nama;?></span></a></li>
									<?php
									}
								}else if($standar=="6"){
									if($jenis=="1"){
								?>
									<li><a title="<?php echo $judul;?>" href="<?php echo base_url();?>borang/standarenam?parameter=<?php echo $id_parameter;?>&judul=<?php echo $judul;?>&jenis=<?php echo $jenis;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-th text-red"></i> <span><?php echo $nama;?></span></a></li>
								<?php
									}
									else{
										?>
										<li><a title="<?php echo $judul;?>" href="<?php echo base_url();?><?php echo $url;?>?jenis=<?php echo $jenis;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-th text-red"></i> <span><?php echo $nama;?></span></a></li>
									<?php
									}
								}else if($standar=="7"){
									if($jenis=="1"){
								?>
									<li><a title="<?php echo $judul;?>" href="<?php echo base_url();?>borang/standartujuah?parameter=<?php echo $id_parameter;?>&judul=<?php echo $judul;?>&jenis=<?php echo $jenis;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-th text-red"></i> <span><?php echo $nama;?></span></a></li>
									<?php
									}
									else{
										?>
										<li><a title="<?php echo $judul;?>" href="<?php echo base_url();?><?php echo $url;?>?jenis=<?php echo $jenis;?>&level=<?php echo $level;?>&idm=<?php echo $idm;?>"><i class="fa fa-th text-red"></i> <span><?php echo $nama;?></span></a></li>
									<?php
									}
								}
							?>
							 
							<?php
							}
							?>
							
							
							-->
						</ul>
					</section>
			</div>
			
			