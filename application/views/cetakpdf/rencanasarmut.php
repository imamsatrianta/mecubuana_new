<style>
	
	@page { margin: 105px 10px 60px 10px; }
	 #content{
                font-size: 11px;
				height:auto;
				font-family: Calibri, sans-serif;
				background-color:#fff;  
				
            }
    #header { position: fixed; left: 0px; top: -100px; right: 0px; height: 100px;  text-align: left;font-family:Arial;background-color:waite; }
    #footer { position: fixed; left: 0%; bottom: -50px; right: 0px; height: 50px; text-align: left;font-size: 11px;font-family: Calibri, sans-serif;background-color:#fff; }
    #footer .page:after { content: counter(page, upper-roman); }
	
}
.page_break { page-break-before: always; }

/*
.mytable{
                border:1px solid black; 
                border-collapse: collapse;
            }
            .mytable tr th, .mytable tr td{
                border:1px solid black; 
                padding: 2px 2px;
            }
	

.mytabledata{
                
                border-collapse: collapse;
            }
            .mytabledata tr th, .mytabledata tr td{
                border-bottom: 1px solid #000;
				border-right: 1px solid #000;
                padding: 2px 2px;
            }


.mytabledatadua{
                
                border-collapse: collapse;
            }
            .mytabledatadua tr th, .mytabledatadua tr td{
                border-bottom: 1px solid #000;
				border-right: 1px solid #000;
				
            }
*/		

</style>
<div id="header"  >
	 <table style="width:100%;"   cellpadding="0" cellspacing="0" border="1">
  <tr>
    <td colspan="3" style="width:13%" align="center"  >
	<img src="assets/images/logo_mercu.png" alt="Logo" style="width:80px;height:60px;">
	</td>
    <td colspan="16" style="width:75.5%" align="center">
		<strong>
			RENCANA PROGRAM PENCAPAIAN SASARAN MUTU TAHUN <?php echo $tahunakademik;?> <br />

			TAHUN AKADEMIK <?php echo $tahunakademik;?><br />

			UNIT : <?php echo $unit;?><br />

			<br />

			</strong>
	</td>
	<td style="width:11.5%" align="center">
		<img src="assets/images/logo_q.png" alt="Logo" style="width:50px;height:40px;">
	</td>
  </tr>
  <tr>
		<td colspan="20"  style="width:auto;" >&nbsp;</td>
   </tr>
 </table>  
</div>
<div id="content"  >

   
 <table style="width:100%;"   cellpadding="0" cellspacing="0" border="1">
  <tr>
    <td rowspan="2" style="width:70px" align="center" >Standard UBM</td>
    <td rowspan="2" style="width:10px" align="center" >No</td>
	<td rowspan="2" style="width:50px" align="center" >MKP</td>
	
	<td rowspan="2" style="width:250px" align="center" >Pernyataan Sasaran Mutu</td>
	<td rowspan="2" style="width:50px" align="center">Target</td>
	<td rowspan="2" style="width:253px;" align="center">Kegiatan</td>
	<td colspan="12" style="width:180px;" align="center">Skedul Kegiatan</td>
	<td rowspan="2" style="width:60px" align="center">Personil Pelaksana</td>
	
	
	<td rowspan="2" style="width:60px" align="center" >Anggaran</td>
  </tr>
  
	<tr>
			
			<td style="width:14px" align="center" >1</td>
			<td style="width:14px" align="center">2</td>
			<td style="width:14px" align="center">3</td>
			<td style="width:14px" align="center">4</td>
			<td style="width:14px" align="center">5</td>
			<td style="width:14px" align="center">6</td>
			<td style="width:14px" align="center">7</td>
			<td style="width:14px" align="center">8</td>
			<td style="width:14px" align="center">9</td>
			<td style="width:14px" align="center">10</td>
			<td style="width:14px" align="center">11</td>
			<td style="width:14px" align="center">12</td>
		
	  </tr>
	   
	   
	   
	  <?php 
		foreach($datastandar as $std){
			$id_mkp=$std->id_mkp;
			$nm_dokumen=$std->nm_dokumen;
		
	  ?>
	  
	  <tr>
		<td width="70px" valign="top"><?php echo $nm_dokumen;?></td>
		<td colspan="19" style="width:auto;" >
			<?php 
					$no=0;
					foreach($datasarmut[$id_mkp] as $sarmut){
						$no++;
						$id=$sarmut->id;
						$uraian_sarmut=$sarmut->uraian_sarmut;
						$target_kualitatif=$sarmut->target_kualitatif;
						$nm_unit=$sarmut->nm_unit;
				  ?>
				  
			<table border="0" class="mytabledata" width="990xp" rules="all">
				<tr>
					<td width="12px" ><?php echo $no;?></td>
					<td width="48px">SSS</td>
					
					<td  style="width:247px" ><?php echo $uraian_sarmut;?></td>
					<td  style="width:49px;" ><?php echo $target_kualitatif;?></td>
					<td style="width:434px;text-align:right;" colspan="13" valign="top" >
						<div style="margin-left: -2px;margin-top:-2px;">
						<table  class="mytabledatadua" border="0" style="margin-right:-2px; background-color:waite;" rules="all">
								<?php 
									foreach($datasarmutdet[$id_mkp][$id] as $detail){
										$uraian_kegiatan=$detail->uraian_kegiatan;
										$tgl_mulai=$detail->tgl_mulai;
										$tgl_selesai=$detail->tgl_selesai;
										$jumlah=$detail->jumlah;
										$awal=(int)$detail->awal;
										$jmlakhir=$awal+$jumlah;
										$satu=0;
										$dua=0;
										$tiga=0;
										$empat=0;
										$lima=0;
										$enam=0;
										$tujuh=0;
										$delapan=0;
										$sembilan=0;
										$sepuluh=0;
										$sebelas=0;
										$duabelas=0;
										for ($x = $awal; $x <= $jmlakhir; $x++) {
											if($x==1){
												$satu=1;
											}
											if($x==2){
												$dua=2;
											}
											if($x==3){
												$tiga=3;
											}
											if($x==4){
												$empat=4;
											}
											if($x==5){
												$lima=5;
											}
											if($x==6){
												$enam=6;
											}
											if($x==7){
												$tujuh=7;
											}
											if($x==8){
												$delapan=8;
											}
											if($x==9){
												$sembilan=9;
											}
											if($x==10){
												$sepuluh=10;
											}
											if($x==11){
												$sebelas=11;
											}
											if($x==12){
												$duabelas=12;
											}
										}
								?>
					
							<tr valign="top">
									<td width="233px;"><?php echo $uraian_kegiatan;?> </td>
									
									<?php if($awal==1){?>
										<td width="12px" style="background-color:red;">&nbsp;</td>
									<?php }else{ ?>
										<?php if($satu==1){?>
											<td width="12px" style="background-color:red;">&nbsp;</td>
										<?php }else{ ?>	
											<td width="12px" >&nbsp;</td>
										<?php } ?>	
									<?php } ?>
									
									<?php if($awal==2){?>
										<td width="11px" style="background-color:red;">&nbsp;</td>
									<?php }else{ ?>
										<?php if($dua==2){?>
											<td width="11px" style="background-color:red;">&nbsp;</td>
										<?php }else{ ?>	
											<td width="11px" >&nbsp;</td>
										<?php } ?>	
									<?php } ?>	
									
									<?php if($awal==3){?>
										<td width="11px" style="background-color:red;">&nbsp;</td>
									<?php }else{ ?>
										<?php if($tiga==3){?>
											<td width="11px" style="background-color:red;">&nbsp;</td>
										<?php }else{ ?>	
											<td width="11px" >&nbsp;</td>
										<?php } ?>	
									<?php } ?>	
									
									<?php if($awal==4){?>
										<td width="12px" style="background-color:red;">&nbsp;</td>
									<?php }else{ ?>
										<?php if($empat==4){?>
											<td width="12px" style="background-color:red;">&nbsp;</td>
										<?php }else{ ?>	
											<td width="12px" >&nbsp;</td>
										<?php } ?>	
									<?php } ?>	
									
									<?php if($awal==5){?>
										<td width="11px" style="background-color:red;">&nbsp;</td>
									<?php }else{ ?>
										<?php if($lima==5){?>
											<td width="11px" style="background-color:red;">&nbsp;</td>
										<?php }else{ ?>	
											<td width="11px" >&nbsp;</td>
										<?php } ?>	
									<?php } ?>	
									
									<?php if($awal==6){?>
										<td width="11px" style="background-color:red;">&nbsp;</td>
									<?php }else{ ?>
										<?php if($enam==6){?>
											<td width="11px" style="background-color:red;">&nbsp;</td>
										<?php }else{ ?>	
											<td width="11px" >&nbsp;</td>
										<?php } ?>	
									<?php } ?>	
									
									
									
									<?php if($awal==7){?>
									<td width="12px" style="background-color:red;">&nbsp;</td>
									<?php }else{ ?>
										<?php if($tujuh==7){?>
											<td width="12px" style="background-color:red;">&nbsp;</td>
										<?php }else{ ?>	
											<td width="12px" >&nbsp;</td>
										<?php } ?>
										
									<?php } ?>	
									
									
									
									<?php if($awal==8){?>
										<td width="11px" style="background-color:red;">&nbsp;</td>
									<?php }else{ ?>
										<?php if($delapan==8){?>
											<td width="11px" style="background-color:red;">&nbsp;</td>
										<?php }else{ ?>	
											<td width="11px" >&nbsp;</td>
										<?php } ?>	
									<?php } ?>	
									
									<?php if($awal==9){?>
										<td width="11px" style="background-color:red;">&nbsp;</td>
									<?php }else{ ?>
										<?php if($sembilan==9){?>
											<td width="11px" style="background-color:red;">&nbsp;</td>
										<?php }else{ ?>	
											<td width="11px" >&nbsp;</td>
										<?php } ?>	
									<?php } ?>	
									
									<?php if($awal==10){?>
										<td width="12px" style="background-color:red;">&nbsp;</td>
									<?php }else{ ?>
										<?php if($sepuluh==10){?>
											<td width="12px" style="background-color:red;">&nbsp;</td>
										<?php }else{ ?>	
											<td width="12px" >&nbsp;</td>
										<?php } ?>	
									<?php } ?>	
									
									
									<?php if($awal==11){?>
										<td width="11px" style="background-color:red;">&nbsp;</td>
									<?php }else{ ?>
										<?php if($sebelas==11){?>
											<td width="11px" style="background-color:red;">&nbsp;</td>
										<?php }else{ ?>	
											<td width="11px" >&nbsp;</td>
										<?php } ?>	
									<?php } ?>	
									
									<?php if($awal==12){?>
										<td width="12px" style="background-color:red;">&nbsp;</td>
									<?php }else{ ?>
										<?php if($duabelas==12){?>
											<td width="12px" style="background-color:red;">&nbsp;</td>
										<?php }else{ ?>	
											<td width="12px" >&nbsp;</td>
										<?php } ?>	
									<?php } ?>	
									
									
							</tr>
							<?php } ?>
						</table>
					
						</div>
					</td>
					<td width="58px" valign="top">
						<?php
							/*foreach($pelaksana[$id_mkp][$id] as $plk){
								echo $plk->nm_unit.", ";
							}*/
							echo $nm_unit;
						?>
					
					</td>
					<td width="57px" valign="top">
						<table rules="rows" width="100%">
							<?php 
								foreach($datasarmutdet[$id_mkp][$id] as $detail){
									$anggaran=$detail->anggaran;
							?>
							<tr valign="top">
							<td width="15px;" valign="top"><?php echo $anggaran;?></td>
							</tr>
								<?php } ?>
						</table>
					</td>
					
				</tr>
				
			</table>
				<?php } ?>
		
		</td>
	  </tr>
	
	  <?php } ?> 
		
</table>





 

	
  </div>