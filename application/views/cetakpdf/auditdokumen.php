<style>
	
@page { margin: 35px 10px 60px 10px; }
#content{
   font-size: 14px;
   height:auto;
   font-family: Calibri, sans-serif;
   background-color:#fff;  

   }
#header { text-align: left;font-family:Arial; background-color:white;}
#judul {

    margin-top:200px;
    margin-bottom:200px;
    font-size: 14px;
    height:auto;
    font-family: Calibri, sans-serif;
    background-color:#fff;
     }
#judul2 {

    margin-top:50px;
    font-size: 14px;
    height:auto;
    font-family: Calibri, sans-serif;
    background-color:#fff;
     }
     
#page2 {
    
margin-top:40px;
height:auto;
font-family: Calibri, sans-serif;
background-color:#fff;
}

#page3 {
    
margin-top:40px;
height:auto;
font-family: Calibri, sans-serif;
background-color:#fff;
}

#footer { 
left: 0%;
bottom: -50px; 
right: 0px; 
height: 30px; 
text-align: 
left;font-size: 
11px;font-family: 
Calibri, sans-serif;
background-color:#fff; 
}
#footer .page:after { content: counter(page, upper-roman); }
h1{ font-family:Arial;font-size:25px;}
.center {margin-left:30px;}

td{ 
        font-family:Arial;
        font-size:12px;
        height: 20px;
        padding: 3px;
        }

th{ 
        font-size:10px;
        }

tr{
        height:30px;
}

.page_break { page-break-before: always; }

.content2{
    font-size: 12px;
    height:auto;
    font-family: Calibri, sans-serif;
    background-color:#fff;
}

.content2 td{
    height: 20px;
    padding: 2px;
}




</style>
<div id="header">
    <div class="center"> 
        <img src="assets/images/logo_mercu.png" style="width:100px;height:100px;">
        <div id="judul">
        <strong><h1> LAPORAN</h1>
            <h1> Audit Mutu Internal (AMI) </h1>
        </strong>
        </div>
    </div>
</div>
<div id="content" >
    <hr style="margin-left:20px;margin-right:20px;">
    <table style="width:100%; margin-left:20px;"  cellpadding="0" cellspacing="0">	
        <tr>
            <th class="font10">Unit</th>
            <td style="width:10px;">:</td>
            <td style="font-size: 12"><?php echo $nm_unit; ?></td>
        </tr>
        <tr>
            <th>Ketua Unit</th>
            <td>:</td>
            <td style="font-size: 12"><?php echo $nm_ketua_unit;?></td>
        </tr>
        <tr>
            <th>Ketua tim auditor</th>
            <td>:</td>
            <td style="font-size: 12"><?php echo $nm_ketua_auditor;?></td>
        </tr>
	
		<?php
			$baris=0;
			foreach($nm_anggota as $dtanggota){
				$baris++;
				echo '
				<tr>
					<th>Anggota '.$baris.'</th>
					<td>:</td>
					<td style="font-size: 12">'.$dtanggota->nm_anggota.'</td>
				</tr>
				';

			}
		?>
		<tr>
            <th>Periode</th>
            <td>:</td>
            <td style="font-size: 12"><?php echo $periode;?></td>
        </tr>
    </table>

</div>
<div class="page_break">
	<div id="judul2">
		<strong>
			<h1 style="text-align:center;">LAPORAN AUDIT MUTU INTERNAL</h1>
		</strong>
	</div>
</div>
<!-- Page 2 -->
<div id="header">
    <div class="center"> 
        <div id="page2">
            <table>
                <tr>
                    <td><strong>1</strong></td>
                    <td><strong>Pendahuluan</strong></td>
                </tr>
            </table>
            <table width="540" height="20" border="1" style=" margin-left:15px;"  cellpadding="0" cellspacing="0">	
                <tr>
                    <td colspan="2">Unit</td>
                    <td colspan="10"><?php echo $nm_unit;?></td>
                </tr>
                <tr>
                    <td colspan="2">Alamat</td>
                    <td colspan="10">Universitas Mercubuana, Jl Maruya Selatan, Kambangan, Jakarta Barat</td>
                </tr>
                <tr>
                    <td colspan="2">Nama Kepala</td>
                    <td colspan="4">Hadi Sudarwanto, BT, M.Ds</td>
                    <td colspan="6"></td>
                </tr>

                <tr>
                    <td colspan="2">Tanggal Audit</td>
                    <td colspan="10"><?php echo $tglmulai;?> <?php echo "-"; ?> <?php echo $tglselesai; ?></td>
                </tr>

                <tr>
                    <td rowspan="2" colspan="2">Kepala Audit</td>
                    <td rowspan="2" colspan="4"><?php echo $nm_ketua_auditor?></td>
                    <td colspan="6"></td>
                </tr>
                <tr>
                    <td colspan="6"></td>
                </tr>
                <tr>
				
                    <td rowspan="2" colspan="2">Anggota Audit</td>
					<td rowspan="2" colspan="4">
					<?php
						$baris=0;
						foreach($nm_anggota as $dtanggota){
							$baris++;
							echo '
									'.$dtanggota->nm_anggota.' , 
							
							';

						}
					?>
                   </td>
                    <td colspan="6"></td>
                </tr>
                <tr>
                    <td colspan="6"></td>
                </tr>
                <tr >
                    <td colspan="2" style="height: 50px;">Tanda Tangan Ketua Audit</td>
                    <td colspan="3"></td>
                    <td colspan="2">Tanda Tangan Ketua Auditer</td>
                    <td colspan="5"></td>
                </tr>
            </table>
            <br>
            <table>
                <tr>
                    <td><strong>2</strong></td>
                    <td><strong>Tujuan Audit</strong></td>
                </tr>
                <tr>
                    <td></td>
                    <td style="width: 200px"><strong>Tujuan audit yang diwajibkan</strong></td>
                </tr>
            </table>
            <table width="540" border="1" style=" margin-left:15px;"  cellpadding="0" cellspacing="0">	
				<?php
				$baris=0;
				foreach($tujuan as $dttujuan){
						$baris++;
						echo '
						<tr>
							<td style="font-size: 12">'.$baris.'. '.$dttujuan->tujuan.'</td>
							<td style="text-align: center"></td>
						</tr>
						';

					}
				?>
             
			
            </table>
            <br>
            <table>
                <tr>
                    <td><strong>3</strong></td>
                    <td><strong>Lingkup Audit</strong></td>
                </tr>
            </table>
            <table width="540" style=" margin-left:13px;"  cellpadding="0" cellspacing="0">
			<?php
				$baris=0;
				foreach($standardok as $dttstandar){
						$baris++;
						echo '
							<tr>
								<td style="font-size: 12">'.$baris.'. '.$dttstandar->standardok.'</td>
								<td style="text-align: center"></td>
							</tr>
						';

					}
				?>
            </table>
        </div>
    </div>
</div>
<!-- Hal 3 -->
<div class="page_break">
</div>
<div id="header">
    <div class="center"> 
        <div id="page3">
        <table border="1" width="720px" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width:100px;"><img src="assets/images/logo_mercu.png" style="width:100px;height:100px;"></td>
                <td style="text-align: center"><h2 ><b>FORMULIR AUDIT</b></h2></td>
                <td style="width:75px; font-size: 70; padding-left: 10px; padding-right: 10px;">Q</td>
            </tr>
        </table>
        <br>
  
        <table border="1" width="720px" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width:200px; padding: 3px;">TOPIK AUDIT</td>
                <td style="padding: 3px;">      <?php 
                $baris=0;
                foreach($materiaudit as $dtmateri){
                    $baris++;
                    $materi=$dtmateri->materi;
                    echo '
                    '.$materi.',';
                }
                 ?></td>
            </tr>
              
            <tr>
                <td style="width:200px; padding: 3px;">UNIT</td>
                <td style="padding: 3px;"><?php echo $nm_unit;?></td>
            </tr>
            <tr>
                <td style="width:200px; padding: 3px;">AUDITOR</td>
                <td style="padding: 3px;"><?php echo $nm_ketua_auditor;?>,	<?php
						$baris=0;
						foreach($nm_anggota as $dtanggota){
							$baris++;
							echo '
									'.$dtanggota->nm_anggota.' , 
							';
						}
					?></td>
            </tr>
            <tr>
                <td style="width:200px; padding: 3px;">AUDTIEE</td>
                <td style="padding: 3px;"><?php echo $auditiedata;?></td>
            </tr>
            <tr>
                <td style="width:200px; padding: 3px;">PERIODE AUDIT</td>
                <td style="padding: 3px;"><?php echo $periode;?></td>
            </tr>
        </table>
        <br>
        <table border="1" style="width:720px;" cellpadding="0" cellspacing="0">
            <tr>
                <th rowspan="2" style="text-align: center">No</th>
                <th rowspan="2" style="text-align: center">Materi Audit Standar</th>
                <th rowspan="2" style="text-align: center">Dokumen</th>
                <th colspan="3" style="text-align: center">Check List</th>
                <th rowspan="2" style="text-align: center">Catatan/Keterangan</th>
                <th rowspan="2" style="text-align: center">Akar Masalah</th>
                <th rowspan="2" style="text-align: center">Rencana Tindakan Korektif</th>
            </tr>
            <tr>
                <th style="text-align: center">Ada</th>
                <th style="text-align: center">Tidak Ada</th>
                <th style="text-align: center">Ada dengan Catatan</th>
            </tr>
            <?php 
                $baris=0;
                foreach($materiaudit as $dtmateri){
                    $baris++;
                    $id_materi=$dtmateri->id_materi;
                    $materi=$dtmateri->materi;
            ?>
            <tr>
                <td><?php echo ''.$baris.''; ?></td>
                <td><?php echo ''.$materi.''; ?></td>
                <td  style="width:auto;">
                <?php 
                $no=0;
                foreach($document[$id_materi] as $dtdocument){
                    $no++;
                    $documents=$dtdocument->document;
                  
                ?>
                    <table>
                        <tr>
                            <td><?php echo $no;?>.</td>
                            <td><?php echo $documents;?></td>
                        </tr>

                    </table>
                <?php } ?>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            <?php } ?>
          
         
            </tr>
			<tr>
                    <td style="height: 70px" colspan="2">Tanda tangan auditor</td>
                    <td colspan="2">Tanggal</td>
                    <td></td>
                    <td colspan="2">Tanda tangan Auditie</td>
                    <td>Tanggal</td>
                    <td></td>
                </tr>
        </table>
		<table border="1" style="width:720px;" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2" style="height: 100px" valign="top">Catatan Auditor</td>
                </tr>
                <tr>
                    <td style="height: 100px; width: 50%" valign="top">
                        Rencana closing<br><br><br><br><br><br>
                        Auditor &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        Auditoree
                    </td>
                    <td valign="top">
                        Rencana closing<br><br><br><br><br><br>
                        Auditor &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        Auditoree
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
