<style>
	
	@page { margin: 55px 20px 60px 20px; }
	 #content{
                font-size: 11px;
				height:auto;
				font-family: Calibri, sans-serif;
				background-color:#fff;  
				
            }
    #header { position: fixed; left: 0px; top: -50px; right: 0px; height: 50px;  text-align: left;font-family:Arial;background-color:waite; }
    #footer { position: fixed; left: 0%; bottom: -50px; right: 0px; height: 30px; text-align: left;font-size: 11px;font-family: Calibri, sans-serif;background-color:#fff; }
    #footer .page:after { content: counter(page, upper-roman); }
	
}
.page_break { page-break-before: always; }

.mytable{
                border:1px solid black; 
                border-collapse: collapse;
                width: 100%;
            }
            .mytable tr th, .mytable tr td{
                border:1px solid black; 
                padding: 2px 2px;
            }
</style>

<div id="content"  >
	<table width="100%"  class="mytable">
	  <tr>
		<td colspan="3" align="center" width="20%">
			<img src="assets/images/logo_mercu1.png" alt="Logo" style="width:80px;height:60px;">
		</td>
		<td colspan="16" align="center" style="font-size:14px" width="70%">
		<strong>
			RENCANA PROGRAM PENCAPAIAN SASARAN MUTU TAHUN <?php echo $tahunakademik;?> <br />

			TAHUN AKADEMIK <?php echo $tahunakademik;?><br />

			UNIT : <?php echo $unit;?><br />

			<br />

			</strong>

			</td>

		<td width="10%" align="center">
			<img src="assets/images/logo_q.png" alt="Logo" style="width:50px;height:40px;">
		</td>

	  </tr>
	  <tr>
		<td colspan="20">&nbsp;</td>
	  </tr>
	   <tr>
			<td rowspan="2" width="60px">Standard   UMB</td>
			<td rowspan="2" width="10px">No</td>
			<td rowspan="2" width="60px">MKP</td>
			<td rowspan="2" width="180px;">Pernyataan Sasaran Mutu</td>
			<td rowspan="2" width="50px;">Target</td>
			<td rowspan="2" width="200px;">Kegiatan</td>
			<td colspan="12" width="120px;">Skedul Kegiatan</td>
			
			
			
			<td rowspan="2" width="50px;">Personil Pelaksana</td>
			<td rowspan="2" width="50px;">Anggaran</td>
		
	  </tr>
	  <tr>
			
			<td width="10px;">1</td>
			<td width="10px;">2</td>
			<td width="10px;">3</td>
			<td width="10px;">4</td>
			<td width="10px;">5</td>
			<td width="10px;">6</td>
			<td width="10px;">7</td>
			<td width="10px;">8</td>
			<td width="10px;">9</td>
			<td width="10px;">10</td>
			<td width="10px;">11</td>
			<td width="10px;">12</td>
		
	  </tr>
	  <?php 
		foreach($datastandar as $std){
			$id_group=$std->id_group;
			$nm_dokumen=$std->nm_dokumen;
		
	  ?>
		<tr>
			<td width="60px;"><?php echo $nm_dokumen;?></td>
			<td width="1020px;" colspan="19" valign="top" >
				<?php 
					$no=0;
					foreach($datasarmut[$id_group] as $sarmut){
						$no++;
						$id=$sarmut->id;
						$uraian_sarmut=$sarmut->uraian_sarmut;
						$target_kualitatif=$sarmut->target_kualitatif;
						//$target_kualitatif=$sarmut->target_kualitatif;
				  ?>
				<table border="1" cellpadding="0" cellspacing="-2.2">
					<tr valign="top">
						<td width="16px"><?php echo $no;?></td>
						<td width="62px">11</td>
						<td width="197px;"><?php echo $uraian_sarmut;?></td>
						<td width="52px;"><?php echo $target_kualitatif;?></td>
						<td width="500px;" colspan="13" valign="top">
							<table  cellpadding="0" cellspacing="0" border="1">
								<?php 
									foreach($datasarmutdet[$id_group][$id] as $detail){
										$uraian_kegiatan=$detail->uraian_kegiatan;
										$tgl_mulai=$detail->tgl_mulai;
										$tgl_selesai=$detail->tgl_selesai;
								?>
								<tr valign="top">
									<td width="20px;"><?php echo $uraian_kegiatan;?></td>
									<td width="15px;">1</td>
									<td width="15px;">2</td>
									<td width="15px;">3</td>
									<td width="15px;">4</td>
									<td width="15px;">5</td>
									<td width="15px;">6</td>
									<td width="15px;">7</td>
									<td width="15px;">8</td>
									<td width="15px;">9</td>
									<td width="15px;">10</td>
									<td width="15px;">11</td>
									<td width="15px;">12</td>
								</tr>
									<?php } ?>
							</table>
						
						</td>
						<td valign="top" width="50px;">
						<?php
							foreach($pelaksana[$id_group][$id] as $plk){
								echo $plk->nm_unit.",";
							}
							?>
						</td>
						<td valign="top" width="50px;" >
							<table >
								<?php 
									foreach($datasarmutdet[$id_group][$id] as $detail){
										$anggaran=$detail->anggaran;
								?>
								<tr valign="top">
								<td width="15px;"><?php echo $anggaran;?></td>
								</tr>
									<?php } ?>
							</table>
							
								
						</td>
			
					</tr>
				</table>
				
				<?php } ?>
			</td>
		</tr>
	  
	  <?php } ?>
	 
  
  </table>
  
  </div>