<style>
	
	@page { margin: 55px 10px 60px 10px; }
	 #content{
                font-size: 11px;
				height:auto;
				font-family: Calibri, sans-serif;
				background-color:#fff;  
				
            }
    #header { position: fixed; left: 0px; top: -50px; right: 0px; height: 50px;  text-align: left;font-family:Arial;background-color:waite; }
    #footer { position: fixed; left: 0%; bottom: -50px; right: 0px; height: 30px; text-align: left;font-size: 11px;font-family: Calibri, sans-serif;background-color:#fff; }
    #footer .page:after { content: counter(page, upper-roman); }
	
}
.page_break { page-break-before: always; }

.mytable{
                border:1px solid black; 
                border-collapse: collapse;
            }
            .mytable tr th, .mytable tr td{
                border:1px solid black; 
                padding: 2px 2px;
            }
	

.mytabledata{
                
                border-collapse: collapse;
            }
            .mytabledata tr th, .mytabledata tr td{
                border-bottom: 1px solid #000;
				border-right: 1px solid #000;
                padding: 2px 2px;
            }



		table.fixed { table-layout:fixed; }

</style>
<div id="content"  >
 <table style="width:100%;"   cellpadding="0" cellspacing="0" border="1">
  <tr>
    <td colspan="3" style="width:20%"  >Month</td>
    <td colspan="16" style="width:70%" >Savings</td>
	<td style="width:10%" >Savings</td>
  </tr>
  <tr>
		<td colspan="20"  style="width:auto;" >&nbsp;</td>
   </tr>
 </table>  
   
 <table style="width:100%;"   cellpadding="0" cellspacing="0" border="1">
  <tr>
    <td rowspan="2" style="width:70px" >Standard UBM</td>
    <td rowspan="2" style="width:10px" >No</td>
	<td rowspan="2" style="width:50px"  >MKP</td>
	
	<td rowspan="2" style="width:250px" >Pernyataan Sasaran Mutu</td>
	<td rowspan="2" style="width:50px" >Target</td>
	<td rowspan="2" style="width:303px;">Kegiatan</td>
	<td colspan="12" style="width:180px;">Skedul Kegiatan</td>
	<td rowspan="2" style="width:60px" >Personil Pelaksana</td>
	
	
	<td rowspan="2" style="width:60px"  >Anggaran</td>
  </tr>
  
	<tr>
			
			<td style="width:14px" >1</td>
			<td style="width:14px">2</td>
			<td style="width:14px">3</td>
			<td style="width:14px">4</td>
			<td style="width:14px">5</td>
			<td style="width:14px">6</td>
			<td style="width:14px">7</td>
			<td style="width:14px">8</td>
			<td style="width:14px">9</td>
			<td style="width:14px">10</td>
			<td style="width:14px">11</td>
			<td style="width:14px">12</td>
		
	  </tr>
	   
	   
	   
	  <?php 
		foreach($datastandar as $std){
			$id_group=$std->id_group;
			$nm_dokumen=$std->nm_dokumen;
		
	  ?>
	  
	  <tr>
		<td width="70px"><?php echo $nm_dokumen;?></td>
		<td colspan="19" style="width:auto;" >
			<?php 
					$no=0;
					foreach($datasarmut[$id_group] as $sarmut){
						$no++;
						$id=$sarmut->id;
						$uraian_sarmut=$sarmut->uraian_sarmut;
						$target_kualitatif=$sarmut->target_kualitatif;
						//$target_kualitatif=$sarmut->target_kualitatif;
				  ?>
				  
			<table border="0" class="mytabledata" width="990xp" >
				<tr>
					<td width="13px" ><?php echo $no;?></td>
					<td width="49px">SSS</td>
					
					<td  style="width:250px" ><?php echo $uraian_sarmut;?></td>
					<td  style="width:49px" ><?php echo $target_kualitatif;?></td>
					<td style="width:302px;" colspan="13">Kegiatan</td>
					
					<td width="13px">3</td>
					<td width="13px">4</td>
					<td width="13px">3</td>
					<td width="13px">4</td>
					<td width="13px">5</td>
					<td width="13px">6</td>
					<td width="13px">7</td>
					<td width="13px">8</td>
					<td width="13px">9</td>
					<td width="13px">1</td>
					<td width="13px">1</td>
					<td width="15px">1</td>
					
					
					<td width="59px">1xxx</td>
					<td width="59px">1x</td>
					
				</tr>
				
			</table>
				<?php } ?>
		
		</td>
	  </tr>
	
	  <?php } ?> 
		
</table>




<!DOCTYPE html>
<html>
<head>
<title>Contoh pemakaian attribut rules dalam Tabel HTML</title>
</head>
<body>
<h2>Belajar atribut rules dalam Tabel HTML</h2>
<h4>rules = "rows"</h4>
<table rules="rows">
    <tr>
        <th>Judul 1</th>
        <th>Judul 2</th>
        <th>Judul 3</th>
    </tr>
    <tr>
        <td>Baris 1, Kolom 1</td>
        <td>Baris 1, Kolom 2</td>
        <td>Baris 1, Kolom 3</td>
    </tr>
    <tr>
        <td>Baris 2, Kolom 1</td>
        <td>Baris 2, Kolom 2</td>
        <td>Baris 2, Kolom 3</td>
    </tr>
    <tr>
        <td>Baris 3, Kolom 1</td>
        <td>Baris 3, Kolom 2</td>
        <td>Baris 3, Kolom 3</td>
    </tr>
</table>
<h4>rules = "cols"</h4>
<table rules="cols">
    <tr>
        <th>Judul 1</th>
        <th>Judul 2</th>
        <th>Judul 3</th>
    </tr>
    <tr>
        <td>Baris 1, Kolom 1</td>
        <td>Baris 1, Kolom 2</td>
        <td>Baris 1, Kolom 3</td>
    </tr>
    <tr>
        <td>Baris 2, Kolom 1</td>
        <td>Baris 2, Kolom 2</td>
        <td>Baris 2, Kolom 3</td>
    </tr>
    <tr>
        <td>Baris 3, Kolom 1</td>
        <td>Baris 3, Kolom 2</td>
        <td>Baris 3, Kolom 3</td>
    </tr>
</table>
<h4>rules = "all"</h4>
<table rules="all"> 
    <tr>
        <th>Judul 1</th>
        <th>Judul 2</th>
        <th>Judul 3</th>
    </tr>
    <tr>
        <td>Baris 1, Kolom 1</td>
        <td>Baris 1, Kolom 2</td>
        <td>Baris 1, Kolom 3</td>
    </tr>
    <tr>
        <td>Baris 2, Kolom 1</td>
        <td>Baris 2, Kolom 2</td>
        <td>Baris 2, Kolom 3</td>
    </tr>
    <tr>
        <td>Baris 3, Kolom 1</td>
        <td>Baris 3, Kolom 2</td>
        <td>Baris 3, Kolom 3</td>
    </tr>
</table>
<h4>rules="none", border="1"</h4>
<table rules="none" border="1" >
    <tr>
        <th>Judul 1</th>
        <th>Judul 2</th>
        <th>Judul 3</th>
    </tr>
    <tr>
        <td>Baris 1, Kolom 1</td>
        <td>Baris 1, Kolom 2</td>
        <td>Baris 1, Kolom 3</td>
    </tr>
    <tr>
        <td>Baris 2, Kolom 1</td>
        <td>Baris 2, Kolom 2</td>
        <td>Baris 2, Kolom 3</td>
    </tr>
                <tr>
        <td>Baris 3, Kolom 1</td>
        <td>Baris 3, Kolom 2</td>
        <td>Baris 3, Kolom 3</td>
    </tr>
</table>
</body>
</html>
 

	
  </div>