<style>
	
	@page { margin: 55px 20px 60px 20px; }
	 #content{
                font-size: 11px;
				height:auto;
				font-family: Calibri, sans-serif;
				background-color:#fff;  
				
            }
    #header { position: fixed; left: 0px; top: -50px; right: 0px; height: 50px;  text-align: left;font-family:Arial;background-color:waite; }
    #footer { position: fixed; left: 0%; bottom: -50px; right: 0px; height: 30px; text-align: left;font-size: 11px;font-family: Calibri, sans-serif;background-color:#fff; }
    #footer .page:after { content: counter(page, upper-roman); }
	
}
.page_break { page-break-before: always; }
</style>

<div id="content"  >
	<table width="100%" cellpadding="0" cellspacing="0" border="1" class="gridtable">
	  <tr>
		<td colspan="3" align="center" width="20%">
			<img src="assets/images/logo_mercu.png" alt="Logo" style="width:80px;height:60px;">
		</td>
		<td colspan="16" align="center" style="font-size:14px" width="70%">
		<strong>
			RENCANA PROGRAM PENCAPAIAN SASARAN MUTU TAHUN <?php echo $tahunakademik;?> <br />

			TAHUN AKADEMIK <?php echo $tahunakademik;?><br />

			UNIT : <?php echo $unit;?><br />

			<br />

			</strong>

			</td>

		<td width="10%" align="center">
			<img src="assets/images/logo_q.png" alt="Logo" style="width:50px;height:40px;">
		</td>

	  </tr>
	  <tr>
		<td colspan="20">&nbsp;</td>
	  </tr>
	   <tr>
			<td rowspan="2">Standard   BAN-PT</td>
			<td rowspan="2">No</td>
			<td rowspan="2">MKP</td>
			<td rowspan="2">Pernyataan Sasaran Mutu</td>
			<td rowspan="2">Target</td>
			<td rowspan="2" width="360px;">Kegiatan</td>
			<td colspan="12" width="90px;">Skedul Kegiatan</td>
			
			<td rowspan="2">Personil Pelaksana</td>
			<td rowspan="2">Anggaran</td>
		
	  </tr>
	  <tr>
			
			<td width="15px;">1</td>
			<td width="15px;">2</td>
			<td width="15px;">3</td>
			<td width="15px;">4</td>
			<td width="15px;">5</td>
			<td width="15px;">6</td>
			<td width="15px;">7</td>
			<td width="15px;">8</td>
			<td width="15px;">9</td>
			<td width="15px;">10</td>
			<td width="15px;">11</td>
			<td width="15px;">12</td>
		
	  </tr>
	  <?php 
		$no=0;
		foreach($datasarmut as $sarmut){
			$no++;
			$id=$sarmut->id;
			$uraian_sarmut=$sarmut->uraian_sarmut;
			$target_kualitatif=$sarmut->target_kualitatif;
			//$target_kualitatif=$sarmut->target_kualitatif;
	  ?>
	  <tr>
			<td >11</td>
			<td ><?php echo $no;?></td>
			<td >11</td>
			<td width="250px;"><?php echo $uraian_sarmut;?></td>
			<td width="50px;"><?php echo $target_kualitatif;?></td>
				
			<td width="450px;" colspan="13" valign="top">
				<table  cellpadding="0" cellspacing="0" border="1">
					<?php 
						foreach($datasarmutdet[$id] as $detail){
							$uraian_kegiatan=$detail->uraian_kegiatan;
							$tgl_mulai=$detail->tgl_mulai;
							$tgl_selesai=$detail->tgl_selesai;
					?>
					<tr valign="top">
						<td width="360px;"><?php echo $uraian_kegiatan;?></td>
						<td width="15px;">1</td>
						<td width="15px;">2</td>
						<td width="15px;">3</td>
						<td width="15px;">4</td>
						<td width="15px;">5</td>
						<td width="15px;">6</td>
						<td width="15px;">7</td>
						<td width="15px;">8</td>
						<td width="15px;">9</td>
						<td width="15px;">10</td>
						<td width="15px;">11</td>
						<td width="15px;">12</td>
					</tr>
						<?php } ?>
				</table>
			
			</td>
			<!--
			<td width="10px;">1</td>
			<td >2</td>
			<td >3</td>
			<td >4</td>
			<td >5</td>
			<td >6</td>
			<td >7</td>
			<td >8</td>
			<td >9</td>
			<td >10</td>
			<td >11</td>
			<td >12</td>
			-->
			<td valign="top">
			<?php
				foreach($pelaksana[$id] as $plk){
					echo $plk->nm_unit.",";
				}
				?>
			</td>
			<td valign="top" >
				<table >
					<?php 
						foreach($datasarmutdet[$id] as $detail){
							$anggaran=$detail->anggaran;
					?>
					<tr valign="top">
					<td width="15px;"><?php echo $anggaran;?></td>
					</tr>
						<?php } ?>
				</table>
				
					
			</td>
	  </tr>
	  <?php 
		}
	  ?>
	  
	  
  
  </table>
  
  </div>
  
  
  
  <td width="57px" valign="top" >XXXX</td>
								<td width="66px" ><?php echo $tgl_mulai;?> - <?php echo $tgl_selesai;?></td>
								<td width="65px" ><?php echo $anggaran;?></td>