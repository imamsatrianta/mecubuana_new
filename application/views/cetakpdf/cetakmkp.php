<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">
        <style>
			@page { margin: 55px 20px 20px 20px; }
	 #content{
                font-size: 11px;
				height:auto;
				font-family: Calibri, sans-serif;
				background-color:#fff;  
				
            }
    #header { position: fixed; left: 0px; top: -50px; right: 0px; height: 50px;  text-align: left;font-family:Arial;background-color:waite; }
    #footer { position: fixed; left: 0%; bottom: -20px; right: 0px; height: 10px; text-align: left;font-size: 11px;font-family: Calibri, sans-serif;background-color:#fff; }
    #footer .page:after { content: counter(page, upper-roman); }
	
}

.mytable{
	border:1px solid black; 
	border-collapse: collapse;
	width: 100%;
}
.mytable tr th, .mytable tr td{
	border:1px solid black; 
	padding: 5px 10px;
}
		
.mytabledua{
	border:0px solid black; 
	border-collapse: collapse;
	width: 100%;
}
.mytabledua tr th, .mytabledua tr td{
	border:0px solid black; 
	padding: 5px 10px;
}		



  </style>
    </head>
    <body>
	<div id="content"  >
		<?php
		$i=0;
		foreach($mkpdata as $mkp){
			$i++;
			$id=$mkp->id;
			$nm_mkp=$mkp->nm_mkp;
			?>
				<p style="text-align:center;"><u><?php echo angkaRomawi($i).".&nbsp; ".$nm_mkp;?></u></p>
				<div>A. Target</div>
			
			<?php
				 $n=0;
			  foreach($mkpdatatarget[$id] as $target){
				  $n++;
				  $uraian_target=$target->uraian_target;
				  $uraian_targetdata = str_replace('p>', 'span>', trim($uraian_target));
				 ?> 
				 
				 <table class="mytabledua" >
					<tr>
						<td valign="top" width="2%"><?php echo $n;?></td>
						<td valign="top" width="98%"><?php echo $uraian_targetdata;?></td>
					</tr>
				 </table>
				
				 
				<!-- <div style="width:100%;">
					<div style="width:1%;float:left;"><?php echo $n;?></div>
					<div style="width:99%;float:right;"><?php echo $uraian_targetdata;?> </div>
				 </div>-->
				
				  
			<?php	
			  }				
			?>
			
			<div>B. Kegiatan</div>
				<table class="mytable">
					<tr align="center">
						<th>No</th>
						<th>Kegiatan</th>
						<th>Unit Terkait</th>
						<th>Waktu</th>
					</tr>
			
				<?php
					$k=0;
				foreach($mkpdatakegiatan[$id] as $keg){
					$k++;
					$kegiatan=$keg->kegiatan;
					$waktu=$keg->waktu;
					$nm_unit=$keg->nm_unit;
				?>
				  <tr>
                    <td align="center"><?php echo $k; ?></td>
                    <td><?php echo $kegiatan; ?></td>
					<td><?php echo $nm_unit; ?></td>
                    <td><?php echo $waktu; ?></td>
                </tr>
					
			<?php
				}			
			?>
			</table>
        
		<?php 
		}
		?>
		</div>
    </body>
</html>