<style>
	
	@page { margin: 105px 20px 60px 20px; }
	 #content{
                font-size: 11px;
				height:auto;
				font-family: Calibri, sans-serif;
				background-color:#fff;  
				
            }
    #header { position: fixed; left: 0px; top: -100px; right: 0px; height: 100px;  text-align: left;font-family:Arial;background-color:waite; }
    #footer { position: fixed; left: 0%; bottom: -50px; right: 0px; height: 30px; text-align: left;font-size: 11px;font-family: Calibri, sans-serif;background-color:#fff; }
    #footer .page:after { content: counter(page, upper-roman); }
	
}
.page_break { page-break-before: always; }

.mytable{
                border:1px solid black; 
                border-collapse: collapse;
            }
            .mytable tr th, .mytable tr td{
                border:1px solid black; 
                padding: 2px 2px;
            }
	

.mytabledata{
                
                border-collapse: collapse;
            }
            .mytabledata tr th, .mytabledata tr td{
                border-bottom: 1px solid #000;
				border-right: 1px solid #000;
                padding: 2px 2px;
            }


.mytabledatadua{
                
                border-collapse: collapse;
            }
            .mytabledatadua tr th, .mytabledatadua tr td{
                border-bottom: 1px solid #000;
				border-right: 1px solid #000;
				
            }
		table.fixed { table-layout:fixed; }

</style>
<div id="header"  >
 <table style="width:100.5%;"   cellpadding="0" cellspacing="0" border="1">
  <tr>
    <td colspan="3" style="width:13%" align="center"  >
	<img src="assets/images/logo_mercu.png" alt="Logo" style="width:80px;height:60px;">
	</td>
    <td colspan="16" style="width:75.5%" align="center">
		<strong>
			LAPORAN PENCAPAIAN SASARAN MUTU TAHUN <br />

			TAHUN AKADEMIK <?php echo $tahunakademik;?><br />

			UNIT : <?php echo $unit;?><br />

			<br />

			</strong>
	</td>
	<td style="width:11.5%" align="center">
		<img src="assets/images/logo_q.png" alt="Logo" style="width:50px;height:40px;">
	</td>
  </tr>
  <tr>
		<td colspan="20"  style="width:auto;" >&nbsp;</td>
   </tr>
 </table>  
 </div>
<div id="content"  >

   
 <table style="width:100%;"   cellpadding="0" cellspacing="0" border="1">
  <tr>
	<td rowspan="2" style="width:15px" align="center" >No</td>
    <td rowspan="2" style="width:70px" align="center" >Standard UBM</td>
	
	<td rowspan="2" style="width:290px" align="center" >Pernyataan Sasaran Mutu</td>
	<td colspan="2" style="width:120px" align="center"  >Target</td>
	
	<td rowspan="2" style="width:335px;" align="center" >Rencana Kegiatan</td>
	<td rowspan="2" style="width:60px" align="center"  >Tanggal Kegiatan</td>
	
	<td rowspan="2" style="width:70px;" align="center" >Realisasi</td>
	<td rowspan="2" style="width:70px" align="center"  >Kendala dan Tindakan Korektif</td>
	
	
	
  </tr>
  
	<tr>
			
			<td style="width:58px" align="center" >Kuantitatif</td>
			<td style="width:63px" align="center" >Kualitatif</td>
		
	</tr>
	
	<?php 
		$baris=0;
		foreach($datastandar as $std){
			$baris++;
			$id_mkp=$std->id_mkp;
			$nm_dokumen=$std->nm_dokumen;
		
	  ?>
	  
		<tr>
			<td width="15px" valign="top"><?php echo $baris;?></td>
			<td width="70px" valign="top"><?php echo $nm_dokumen;?></td>
			<td colspan="7" style="width:auto;" >
				<?php 
					$no=0;
					foreach($datasarmut[$id_mkp] as $sarmut){
						$no++;
						$id=$sarmut->id;
						$uraian_sarmut=$sarmut->uraian_sarmut;
						$target_kuantitatif=$sarmut->target_kuantitatif;
						$target_kualitatif=$sarmut->target_kualitatif;
						$kinerja_lalu=$sarmut->kinerja_lalu;
				  ?>
				<table border="0" class="mytabledata" width="990xp" >
				<tr>
					<td width="292px" ><?php echo $uraian_sarmut;?></td>
					<td width="62px" valign="top"><?php echo $target_kuantitatif;?> </td>
					<td width="65px" valign="top"><?php echo $target_kualitatif;?></td>
					
					<td colspan="3" width="558px" valign="top">
						<table  class="mytabledatadua" border="0" style="margin-right:-2px;margin-left:-2px;margin-top:-2px; background-color:waite;">
								<?php 
									foreach($datasarmutdet[$id_mkp][$id] as $detail){
										$uraian_kegiatan=$detail->uraian_kegiatan;
										$tgl_mulai=$detail->tgl_mulai;
										$tgl_selesai=$detail->tgl_selesai;
										$anggaran=$detail->anggaran;
										$iddet=$detail->id;
								?>
					
							<tr valign="top" >
								<td width="338px;"><?php echo $uraian_kegiatan;?> </td>
								<td width="215px" valign="top" colspan="3" >
									<table  class="mytabledatadua" border="0" style="margin-right:-6px;margin-left:-2px;margin-top:-2px; background-color:waite;">
										<?php 
											foreach($datasarmutrel[$id_mkp][$id][$iddet] as $detailrel){
												$tgl_realisasi=$detailrel->tgl_realisasi;
												$presentase_realisasi=$detailrel->presentase_realisasi;
												$kendala=$detailrel->kendala;
												$tindakan=$detailrel->tindakan;
										?>
										<tr valign="top">
											<td width="61px" valign="top" ><?php echo tgl_indo($tgl_realisasi);?></td>
											<td width="70px" valign="top" ><?php echo $presentase_realisasi;?></td>
											<td width="72px" valign="top" ><?php echo $kendala;?>, <?php echo $tindakan;?></td>
										</tr>
										<?php } ?>
									</table>
								</td>
								
							</tr>
							<?php } ?>
						</table>	
							
					</td>
					
				</tr>
				
				</table>
				<?php } ?>
			
			
			</td>
		</tr>
	
	  <?php } ?> 
	   
	   
	   
	 
		
</table>





 

	
  </div>