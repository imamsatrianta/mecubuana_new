<html
    xmlns:o='urn:schemas-microsoft-com:office:office'
    xmlns:w='urn:schemas-microsoft-com:office:word'
    xmlns='http://www.w3.org/TR/REC-html40'>
    <head>
        <title>Generate a document Word</title>
        <!--[if gte mso 9]-->
    <xml>
        <w:WordDocument>
            <w:View>Print</w:View>
            <w:Zoom>90</w:Zoom>
            <w:DoNotOptimizeForBrowser/>
        </w:WordDocument>
    </xml>
	


<style>

p.MsoHeader, li.MsoHeader, div.MsoHeader{
    margin:0in;
    margin-top:.0001pt;
    mso-pagination:widow-orphan;
    tab-stops:center 3.0in right 6.0in;
}
p.MsoFooter, li.MsoFooter, div.MsoFooter{
    margin:0in 0in 1in 0in;
    margin-bottom:.0001pt;
    mso-pagination:widow-orphan;
    tab-stops:center 3.0in right 6.0in;
}
.footer {
    font-size: 9pt;
}
@page Section1{
    size: 29.7cm 21cm;
    margin:0.5in 0.5in 0.5in 0.5in;
    mso-header-margin:0.5in;
    mso-header:h1;
    mso-footer:f1;
    mso-footer-margin:0.5in;
    mso-paper-source:0;
}
div.Section1{
    page:Section1;
}
table#hrdftrtbl{
    margin:0in 0in 0in 15in;
} 


 
	
</style>
<style type="text/css" media="screen,print">
body {
    font-family: "Calibri", "Verdana","HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    font-size:12pt;
}
pageBreak {
  clear:all;
  page-break-before:always;
  mso-special-character:line-break;
}

.mytable{
                border:1px solid black;
                border-collapse: collapse;
                width: 100%;
            }
            .mytable tr th, .mytable tr td{
                border:1px solid black; 
                padding: 2px 2px;
            }
</style>
</head>
<body style='tab-interval:.5in'>
<div class="Section1">

	<p style="font-weight:bold;">STANDAR 1.  VISI, MISI, TUJUAN DAN SASARAN, SERTA STRATEGI PENCAPAIAN</p>
	
		<?php
		//print_r($databorang);
		$i=0;
		foreach($databorang as $borang){
		$id=$borang->id;
		$poin=$borang->poin;
		$judul=trim($borang->judul);
		$uraian=$borang->uraian;
		$level=$borang->level;
		$uraianborang=$datauraian[$id];
		$poinlain=substr($poin,-2,1);
		$jenis=$borang->jenis;
		//$uraianborang=$borang->uraianborang;
		//echo $judul;
		?>
		<table border="0" width="100%">
			
			<?php if($judul=="-"){?>
				<?php if($poinlain=='-'){?>
					<tr valign="top">
					<td style="width:2%;" >&nbsp;</td>
					<td style="width:95%;" colspan="2" ><?php echo $uraian;?></td>
					</tr>
					
					<tr valign="top">
						<td >&nbsp;</td>
						<td colspan="2"><table cellpadding="3" cellspacing="0" style="height:100px;min-height:100px;width:100%;"  class="mytable" ><tr valign="top"><td><?php echo $uraianborang;?></td></tr></table></td>
					</tr>
				
				<?php }else{ ?> 
					<?php if($jenis==0){?>
						<tr valign="top">
							<td style="width:2%;" ><?php echo  trim($poin);?></td>
							<td style="width:95%;" colspan="2" ><?php echo $uraian;?></td>
						</tr>
					<?php }else{ ?> 
						<tr valign="top">
							<td style="width:2%;" ><?php echo  trim($poin);?></td>
							<td style="width:95%;" colspan="2" ><?php echo $uraian;?></td>
						</tr>
						
						<tr valign="top" >
							<td >&nbsp;</td>
							<td colspan="2"><table cellpadding="3" cellspacing="0" style="height:100px;min-height:100px;width:100%;"  class="mytable" ><tr valign="top"><td><?php echo $uraianborang;?></td></tr></table></td>
						</tr>
					<?php } ?> 
					
				
				
				<?php }?>
			<?php }else{ ?> 
				<tr valign="top">
					<td style="width:2%;" ><?php echo trim($poin);?></td>
					<td style="width:95%;" colspan="2" ><?php echo $judul;?></td>
				</tr valign="top">
				<tr valign="top">
					<td colspan="2">&nbsp;</td>
					<td style="width:100%;" ><?php echo $uraian;?></td>
				</tr>
				
				<tr valign="top">
					<td colspan="2">&nbsp;</td>
					<td><table class="mytable" cellpadding="3" cellspacing="0" style="height:100px;min-height:100px;width:100%;"  ><tr valign="top"><td><?php echo $uraianborang;?></td></tr></table></td>
				</tr>
			
			<?php } ?> 
			
			
			
		</table>
		
		
		<?php	
			  }				
			?>


</div>
 

			   
			   
 

	<table id='hrdftrtbl' border='0' cellspacing='0' cellpadding='0'>
    <tr>
        <td>
	<div style='mso-element:footer' id="f1">
                <p class="MsoFooter">
                 
                            Halaman
                                <g:message code="offer.letter.page.label"/> <span style='mso-field-code: PAGE '></span> dari <span style='mso-field-code: NUMPAGES '></span>
                           
                </p>
    </div>
			
		  </td>
    </tr>
</table>	
			
  
</body>
</html>