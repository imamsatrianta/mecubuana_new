<html
    xmlns:o='urn:schemas-microsoft-com:office:office'
    xmlns:w='urn:schemas-microsoft-com:office:word'
    xmlns='http://www.w3.org/TR/REC-html40'>
    <head>
        <title>Generate a document Word</title>
        <!--[if gte mso 9]-->
    <xml>
        <w:WordDocument>
            <w:View>Print</w:View>
            <w:Zoom>90</w:Zoom>
            <w:DoNotOptimizeForBrowser/>
        </w:WordDocument>
    </xml>
	


<style>

p.MsoHeader, li.MsoHeader, div.MsoHeader{
    margin:0in;
    margin-top:.0001pt;
    mso-pagination:widow-orphan;
    tab-stops:center 3.0in right 6.0in;
}
p.MsoFooter, li.MsoFooter, div.MsoFooter{
    margin:0in 0in 1in 0in;
    margin-bottom:.0001pt;
    mso-pagination:widow-orphan;
    tab-stops:center 3.0in right 6.0in;
}
.footer {
    font-size: 9pt;
}
@page Section1{
    size:21cm 29.7cm ;
    margin:0.3in 0.3in 0.3in 0.3in;
    mso-header-margin:0.5in;
    mso-header:h1;
    mso-footer:f1;
    mso-footer-margin:0.5in;
    mso-paper-source:0;
}
div.Section1{
    page:Section1;
}
table#hrdftrtbl{
    margin:0in 0in 0in 15in;
} 


 
	
</style>
<style type="text/css" media="screen,print">
body {
    font-family: "Calibri", "Verdana","HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    font-size:12pt;
}
pageBreak {
  clear:all;
  page-break-before:always;
  mso-special-character:line-break;
}

.mytable{
                border:1px solid black; 
                border-collapse: collapse;
            }
            .mytable tr th, .mytable tr td{
                border:1px solid black; 
                padding: 2px 2px;
            }
</style>
</head>
<body style='tab-interval:.5in'>
<div class="Section1">

	<p style="font-weight:bold;">STANDAR 3. KEMAHASISWAAN DAN LULUSAN</p>
	
		<?php
		//print_r($databorang);
		$i=0;
		foreach($databorang as $borang){
		$id=$borang->id;
		$poin=$borang->poin;
		$judul=trim($borang->judul);
		$uraian=$borang->uraian;
		$level=$borang->level;
		$jenis=$borang->jenis;
		$uraianborang=$datauraian[$id];
		$poinlain=substr($poin,-2,1);
		$poinbeda=substr($poin,0,5);
		//$uraianborang=$borang->uraianborang;
		//echo $poinbeda;
		?>
		<table border="0" width="100%">
			
			<?php if($judul=="-"){?>
				<?php if($poinlain=='-'){?>
					<tr valign="top">
					<td style="width:2%;" >&nbsp;</td>
					<td style="width:95%;" colspan="2" ><?php echo $uraian;?></td>
					</tr>
					
					<tr valign="top">
						<td >&nbsp;</td>
						<td colspan="2"><table class="mytable" cellpadding="3" cellspacing="0" style="height:auto; min-height: 200px;width:100%;"   ><tr valign="top"><td><?php echo $uraianborang;?>&nbsp;</td></tr></table></td>
					</tr>
				
				<?php }else{ ?> 
					<?php if($jenis==0){?>
						<tr valign="top">
							<td style="width:2%;" ><?php echo  trim($poin);?></td>
							<td style="width:95%;" colspan="2" ><?php echo $uraian;?></td>
						</tr>
					<?php }else{ ?> 
						<tr valign="top">
							<td style="width:2%;" ><?php echo  trim($poin);?></td>
							<td style="width:95%;" colspan="2" ><?php echo $uraian;?></td>
						</tr>
						
						<tr valign="top" >
							<td >&nbsp;</td>
							<td colspan="2"><table cellpadding="3" cellspacing="0" style="height:auto; min-height: 200px;width:100%;" class="mytable" ><tr valign="top"><td><?php echo $uraianborang;?></td></tr></table></td>
						</tr>
					<?php } ?> 
					
				
				
				<?php }?>
			<?php }else{ ?> 
			
				<?php if($jenis==1){?>
				<tr valign="top">
					<td style="width:2%;" ><?php echo trim($poin);?></td>
					<td style="width:95%;" colspan="2" ><?php echo $judul;?></td>
				</tr valign="top">
				<tr valign="top">
					<td colspan="2">&nbsp;</td>
					<td style="width:100%;" ><?php echo $uraian;?></td>
				</tr>
				
				<tr valign="top">
					<td colspan="2">&nbsp;</td>
					<td><table class="mytable"  cellpadding="3" cellspacing="0" style="height:auto; min-height: 200px;width:100%;"  ><tr valign="top"><td><?php echo $uraianborang;?></td></tr></table></td>
				</tr>
				<?php }else {?>
					<?php if($id<>"48"){?>
					<tr valign="top">
						<td style="width:2%;" ><?php echo trim($poinbeda);?></td>
						<td style="width:95%;" colspan="2" ><?php echo $judul;?></td>
					</tr valign="top">
					<?php }else {?>
					<tr valign="top">
						<td style="width:2%;" ></td>
						<td style="width:95%;" colspan="2" ><?php echo $judul;?></td>
					</tr valign="top">
					<?php }?>
				<tr valign="top">
					<td colspan="2">&nbsp;</td>
					<td>
						
						
						
						<table border="0" cellpadding="3" cellspacing="0" class="mytable" >
							
							<?php  if($id=="96"){ ?>
								<tr>
									<td align="center" rowspan="2"  width="20%">Nama Program Studi Magister </td>
									<td align="center" colspan="3"  width="60%">Aspek</td>
									<td align="center" rowspan="2" width="20%">Rata-Rata Lama Studi (tahun) </td>
								</tr>
								<tr>
									<td align="center" class="ganjil"  >Jumlah Mahasiswa</td>
									<td align="center" class="ganjil"  >Banyaknya Lulusan </td>
									<td align="center" class="ganjil" >Rata-rata IPK  </td>
								</tr>
								<tr>
									<td align="center" class="ganjil">(1)</td>
									<td align="center" class="ganjil">(2)</td>
									<td align="center" class="ganjil">(3)</td>
									<td align="center" class="ganjil">(4)</td>
									<td align="center" class="ganjil">(5)</td>
								</tr>
							
							<?php } ?>
						
						<?php if($id=="96"){?>	
						<?php
						$nodua=0;
						foreach($standarsarjana[$id] as $tigasatu){
						$nodua++;
						$id=$tigasatu->id;
						$nm_prodi=$tigasatu->nm_prodi;
						$jml_mahasiswa=$tigasatu->jml_mahasiswa;
						$banyak_lulusan=$tigasatu->banyak_lulusan;
						$rata_ipk=$tigasatu->rata_ipk;
						$rata_lama_studi=$tigasatu->rata_lama_studi;
						
						?>
							<tr>
								<td ><?php echo $nm_prodi;?></td>
								<td ><?php echo $jml_mahasiswa;?></td>
								<td ><?php echo $banyak_lulusan;?></td>
								<td ><?php echo $rata_ipk;?></td>
								<td ><?php echo $rata_lama_studi;?></td>
								
												
							</tr>
						<?php } ?> 
						<?php } ?> 
						
					
						
						
						
						
						</table>
						
						
					</td>
				</tr>
				
				
				<tr valign="top">
					<td colspan="2">&nbsp;</td>
					<td style="width:100%;" ><?php echo $uraian;?></td>
				</tr>
				
				
				<?php } ?> 
			<?php } ?> 
			
			
			
		</table>
		
		
		<?php	
			  }				
			?>


</div>
 

			   
			   
 
	
			
  
</body>
</html>