<html
    xmlns:o='urn:schemas-microsoft-com:office:office'
    xmlns:w='urn:schemas-microsoft-com:office:word'
    xmlns='http://www.w3.org/TR/REC-html40'>
    <head>
        <title>Generate a document Word</title>
        <!--[if gte mso 9]-->
    <xml>
        <w:WordDocument>
            <w:View>Print</w:View>
            <w:Zoom>90</w:Zoom>
            <w:DoNotOptimizeForBrowser/>
        </w:WordDocument>
    </xml>
	


<style>

p.MsoHeader, li.MsoHeader, div.MsoHeader{
    margin:0in;
    margin-top:.0001pt;
    mso-pagination:widow-orphan;
    tab-stops:center 3.0in right 6.0in;
}
p.MsoFooter, li.MsoFooter, div.MsoFooter{
    margin:0in 0in 1in 0in;
    margin-bottom:.0001pt;
    mso-pagination:widow-orphan;
    tab-stops:center 3.0in right 6.0in;
}
.footer {
    font-size: 9pt;
}
@page Section1{
    size:21cm 29.7cm ;
    margin:0.3in 0.3in 0.3in 0.3in;
    mso-header-margin:0.5in;
    mso-header:h1;
    mso-footer:f1;
    mso-footer-margin:0.5in;
    mso-paper-source:0;
}
div.Section1{
    page:Section1;
}
table#hrdftrtbl{
    margin:0in 0in 0in 15in;
} 


 
	
</style>
<style type="text/css" media="screen,print">
body {
    font-family: "Calibri", "Verdana","HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    font-size:12pt;
}
pageBreak {
  clear:all;
  page-break-before:always;
  mso-special-character:line-break;
}

.mytable{
                border:1px solid black; 
                border-collapse: collapse;
            }
            .mytable tr th, .mytable tr td{
                border:1px solid black; 
                padding: 2px 2px;
            }
</style>
</head>
<body style='tab-interval:.5in'>
<div class="Section1">

	<p style="font-weight:bold;">STANDAR 3. KEMAHASISWAAN DAN LULUSAN</p>
	
		<?php
		//print_r($databorang);
		$i=0;
		foreach($databorang as $borang){
		$id=$borang->id;
		$poin=$borang->poin;
		$judul=trim($borang->judul);
		$uraian=$borang->uraian;
		$level=$borang->level;
		$jenis=$borang->jenis;
		$uraianborang=$datauraian[$id];
		$poinlain=substr($poin,-2,1);
		$poinbeda=substr($poin,0,5);
		//$uraianborang=$borang->uraianborang;
		//echo $poinbeda;
		?>
		<table border="0" width="100%">
			
			<?php if($judul=="-"){?>
				<?php if($poinlain=='-'){?>
					<tr valign="top">
					<td style="width:2%;" >&nbsp;</td>
					<td style="width:95%;" colspan="2" ><?php echo $uraian;?></td>
					</tr>
					
					<tr valign="top">
						<td >&nbsp;</td>
						<td colspan="2"><table class="mytable" cellpadding="3" cellspacing="0" style="height:auto; min-height: 200px;width:100%;"   ><tr valign="top"><td><?php echo $uraianborang;?>&nbsp;</td></tr></table></td>
					</tr>
				
				<?php }else{ ?> 
					<?php if($jenis==0){?>
						<tr valign="top">
							<td style="width:2%;" ><?php echo  trim($poin);?></td>
							<td style="width:95%;" colspan="2" ><?php echo $uraian;?></td>
						</tr>
					<?php }else{ ?> 
						<tr valign="top">
							<td style="width:2%;" ><?php echo  trim($poin);?></td>
							<td style="width:95%;" colspan="2" ><?php echo $uraian;?></td>
						</tr>
						
						<tr valign="top" >
							<td >&nbsp;</td>
							<td colspan="2"><table cellpadding="3" cellspacing="0" style="height:auto; min-height: 200px;width:100%;" class="mytable" ><tr valign="top"><td><?php echo $uraianborang;?></td></tr></table></td>
						</tr>
					<?php } ?> 
					
				
				
				<?php }?>
			<?php }else{ ?> 
			
				<?php if($jenis==1){?>
				<tr valign="top">
					<td style="width:2%;" ><?php echo trim($poin);?></td>
					<td style="width:95%;" colspan="2" ><?php echo $judul;?></td>
				</tr valign="top">
				<tr valign="top">
					<td colspan="2">&nbsp;</td>
					<td style="width:100%;" ><?php echo $uraian;?></td>
				</tr>
				
				<tr valign="top">
					<td colspan="2">&nbsp;</td>
					<td><table class="mytable"  cellpadding="3" cellspacing="0" style="height:auto; min-height: 200px;width:100%;"  ><tr valign="top"><td><?php echo $uraianborang;?></td></tr></table></td>
				</tr>
				<?php }else {?>
					<?php if($id<>"48"){?>
					<tr valign="top">
						<td style="width:2%;" ><?php echo trim($poinbeda);?></td>
						<td style="width:95%;" colspan="2" ><?php echo $judul;?></td>
					</tr valign="top">
					<?php }else {?>
					<tr valign="top">
						<td style="width:2%;" ></td>
						<td style="width:95%;" colspan="2" ><?php echo $judul;?></td>
					</tr valign="top">
					<?php }?>
				<tr valign="top">
					<td colspan="2">&nbsp;</td>
					<td>
						
						
						
						<table border="0" cellpadding="3" cellspacing="0" class="mytable" >
							<?php if($id=="38"){?>
								<tr>
									<td align="center" class="ganjil" rowspan="2">Tahun Akade-mik </td>
									<td align="center" class="ganjil" rowspan="2" >Daya Tampung</td>
									<td align="center" class="ganjil" colspan="2">Jumlah Calon Mahasiswa Reguler  </td>
									<td align="center" class="ganjil" colspan="2">Jumlah Mahasiswa Baru</td>
									<td align="center" class="ganjil" colspan="2">Jumlah Total Mahasiswa</td>
									<td align="center" class="ganjil" colspan="2">Jumlah Lulusan </td>
									<td align="center" class="ganjil" colspan="3">IPK Lulusan Reguler</td>
									<td align="center" class="ganjil" colspan="3">Persentase Lulusan Reguler dengan IPK :</td>
								</tr>
								<tr>
									<td align="center" class="ganjil">Ikut Seleksi</td>
									<td align="center" class="ganjil">Lulus Seleksi</td>
									<td align="center" class="ganjil">Regular bukan Transfer</td>
									<td align="center" class="ganjil">Transfer</td>
									<td align="center" class="ganjil">Reguler bukan Transfer</td>
									<td align="center" class="ganjil">Transfer</td>
									<td align="center" class="ganjil">Reguler bukan Transfer</td>
									<td align="center" class="ganjil">Transfer</td>
									<td align="center" class="ganjil">Min</td>
									<td align="center" class="ganjil">Rata2</td>
									<td align="center" class="ganjil">Mak</td>
									<td align="center" class="ganjil"> < 2,75</td>
									<td align="center" class="ganjil"> 2,75-3,50</td>
									<td align="center" class="ganjil"> > 3,50 </td>
								</tr>
								<tr>
									<td align="center" class="ganjil">(1)</td>
									<td align="center" class="ganjil">(2)</td>
									<td align="center" class="ganjil">(3)</td>
									<td align="center" class="ganjil">(4)</td>
									<td align="center" class="ganjil">(5)</td>
									<td align="center" class="ganjil">(6)</td>
									<td align="center" class="ganjil">(7)</td>
									<td align="center" class="ganjil">(8)</td>
									<td align="center" class="ganjil">(9)</td>
									<td align="center" class="ganjil">(10)</td>
									<td align="center" class="ganjil">(11)</td>
									<td align="center" class="ganjil">(12)</td>
									<td align="center" class="ganjil">(13)</td>
									<td align="center" class="ganjil">(14)</td>
									<td align="center" class="ganjil">(15)</td>
									<td align="center" class="ganjil">(16)</td>
								</tr>
							<?php } else if($id=="40"){ ?>
								<tr>
										<td align="center" class="ganjil" rowspan="2" width="10%">Tahun Masuk </td>
										<td align="center" class="ganjil" colspan="7" width="75%">Jumlah Mahasiswa Reguler per Angkatan pada Tahun  </td>
										<td align="center" class="ganjil" rowspan="2" width="15%">Jumlah Lulusan s.d. TS (dari Mahasiswa Reguler)  </td>
									</tr>
									<tr>
										<td align="center" class="ganjil">TS-6</td>
										<td align="center" class="ganjil">TS-5</td>
										<td align="center" class="ganjil">TS-4</td>
										<td align="center" class="ganjil">TS-3</td>
										<td align="center" class="ganjil">TS-2</td>
										<td align="center" class="ganjil">TS-1</td>
										<td align="center" class="ganjil">TS</td>
										
								</tr>
								<tr>
									<td align="center" class="ganjil">(1)</td>
									<td align="center" class="ganjil">(2)</td>
									<td align="center" class="ganjil">(3)</td>
									<td align="center" class="ganjil">(4)</td>
									<td align="center" class="ganjil">(5)</td>
									<td align="center" class="ganjil">(6)</td>
									<td align="center" class="ganjil">(7)</td>
									<td align="center" class="ganjil">(8)</td>
									<td align="center" class="ganjil">(9)</td>
								</tr>
							<?php } else if($id=="41"){ ?>
								<tr>
									<td align="center" class="ganjil" rowspan="2" width="10%">Tahun Masuk </td>
									<td align="center" class="ganjil" colspan="5" width="70%">Jumlah Mahasiswa Reguler per Angkatan pada Tahun  </td>
									<td align="center" class="ganjil" rowspan="2" width="30%">Jumlah Lulusan s.d. TS (dari Mahasiswa Reguler)  </td>
									
								</tr>
								<tr>
									<td align="center" class="ganjil">TS-4</td>
									<td align="center" class="ganjil">TS-3</td>
									<td align="center" class="ganjil">TS-2</td>
									<td align="center" class="ganjil">TS-1</td>
									<td align="center" class="ganjil">TS</td>
									
								</tr>
								<tr>
									<td align="center" class="ganjil">(1)</td>
									<td align="center" class="ganjil">(2)</td>
									<td align="center" class="ganjil">(3)</td>
									<td align="center" class="ganjil">(4)</td>
									<td align="center" class="ganjil">(5)</td>
									<td align="center" class="ganjil">(6)</td>
									<td align="center" class="ganjil">(7)</td>
								</tr>
							<?php } else if($id=="42"){ ?>
								<tr>
									<td align="center" class="ganjil" rowspan="2" width="10%">Tahun Masuk </td>
									<td align="center" class="ganjil" colspan="3" width="70%">Jumlah Mahasiswa Reguler per Angkatan pada Tahun  </td>
									<td align="center" class="ganjil" rowspan="2" width="20%">Jumlah Lulusan s.d. TS (dari Mahasiswa Reguler)  </td>
									
								</tr>
								<tr>
									<td align="center" class="ganjil">TS-2</td>
									<td align="center" class="ganjil">TS-1</td>
									<td align="center" class="ganjil">TS</td>
									
								</tr>
								<tr>
									<td align="center" class="ganjil">(1)</td>
									<td align="center" class="ganjil">(2)</td>
									<td align="center" class="ganjil">(3)</td>
									<td align="center" class="ganjil">(4)</td>
									<td align="center" class="ganjil">(5)</td>
								</tr>
							<?php } else if($id=="43"){ ?>
								<tr>
									<td align="center" class="ganjil" rowspan="2" width="10%">Tahun Masuk </td>
									<td align="center" class="ganjil" colspan="2" width="70%">Jumlah Mahasiswa Reguler per Angkatan pada Tahun  </td>
									<td align="center" class="ganjil" rowspan="2" width="20%">Jumlah Lulusan s.d. TS (dari Mahasiswa Reguler)  </td>
									
								</tr>
								<tr>
									<td align="center" class="ganjil">TS-1</td>
									<td align="center" class="ganjil">TS</td>
									
								</tr>
								<tr>
									<td align="center" class="ganjil">(1)</td>
									<td align="center" class="ganjil">(2)</td>
									<td align="center" class="ganjil">(3)</td>
									<td align="center" class="ganjil">(4)</td>
								</tr>
							<?php } else if($id=="48"){ ?>
								<tr>
									<td align="center" class="ganjil" rowspan="2" width="3%">No </td>
									<td align="center" class="ganjil" rowspan="2" width="30%">Jenis Kemampuan </td>
									<td align="center" class="ganjil" colspan="4" width="48%">Tanggapan Pihak Pengguna </td>
									<td align="center" class="ganjil" rowspan="2" width="20%">Rencana Tindak Lanjut oleh Program Studi </td>
									
								</tr>
								<tr>
									<td align="center" class="ganjil">Sangat Baik</td>
									<td align="center" class="ganjil">Baik</td>
									<td align="center" class="ganjil">Cukup</td>
									<td align="center" class="ganjil">Kurang</td>
									
								</tr>
								<tr>
									<td align="center" class="ganjil">(1)</td>
									<td align="center" class="ganjil">(2)</td>
									<td align="center" class="ganjil">(3)</td>
									<td align="center" class="ganjil">(4)</td>
									<td align="center" class="ganjil">(5)</td>
									<td align="center" class="ganjil">(6)</td>
									<td align="center" class="ganjil">(7)</td>
								</tr>
							<?php } else if($id=="52"){ ?>
								<tr>
									<td align="center" class="ganjil" width="10%">Tahun</td>
									<td align="center" class="ganjil" width="30%">Jumlah Lulusan yang Diwisuda pada</td>
									<td align="center" class="ganjil" width="30%">Nama Lembaga (Instansi/Industri)</td>
									<td align="center" class="ganjil" width="30%">Jumlah Lulusan yang Dipesan dan Diterima</td>
									
								</tr>
								<tr>
									<td align="center" class="ganjil">(1)</td>
									<td align="center" class="ganjil">(2)</td>
									<td align="center" class="ganjil">(3)</td>
									<td align="center" class="ganjil">(4)</td>
								</tr>
							<?php } ?>
						<?php if($id=="38"){?>	
						<?php
						foreach($datatigasatusatu[$id] as $tigasatu){
						$id=$tigasatu->id;
						$tahun_akademik=$tigasatu->tahun_akademik;
						$daya_tampung=$tigasatu->daya_tampung;
						$mhs_ikut_seleksi=$tigasatu->mhs_ikut_seleksi;
						$mhs_lulus_seleksi=$tigasatu->mhs_lulus_seleksi;
						$jml_reg_bkn_transfer=$tigasatu->jml_reg_bkn_transfer;
						$jml_transfer=$tigasatu->jml_transfer;
						$total_reg_bkn_transfer=$tigasatu->total_reg_bkn_transfer;
						$total_transfer=$tigasatu->total_transfer;
						$lulus_reg_bkn_transfer=$tigasatu->lulus_reg_bkn_transfer;
						$lulus_transfer=$tigasatu->lulus_transfer;
						$ipk_lulusan_reg_min=$tigasatu->ipk_lulusan_reg_min;
						$ipk_lulusan_reg_rata=$tigasatu->ipk_lulusan_reg_rata;
						$ipk_lulusan_reg_mak=$tigasatu->ipk_lulusan_reg_mak;
						$ipk_kurang=$tigasatu->ipk_kurang;
						$ipk=$tigasatu->ipk;
						$ipk_lebih=$tigasatu->ipk_lebih;
						?>
							<tr>
								<td ><?php echo $tahun_akademik;?></td>
								<td ><?php echo $daya_tampung;?></td>
								<td ><?php echo $mhs_ikut_seleksi;?></td>
								<td ><?php echo $mhs_lulus_seleksi;?></td>
								<td ><?php echo $jml_reg_bkn_transfer;?></td>
								<td ><?php echo $jml_transfer;?></td>
								<td ><?php echo $total_reg_bkn_transfer;?></td>
								<td ><?php echo $total_transfer;?></td>
								<td ><?php echo $lulus_reg_bkn_transfer;?></td>
								<td ><?php echo $lulus_transfer;?></td>
								<td ><?php echo $ipk_lulusan_reg_min;?></td>
								<td ><?php echo $ipk_lulusan_reg_rata;?></td>
								<td ><?php echo $ipk_lulusan_reg_mak;?></td>
								<td ><?php echo $ipk_kurang;?></td>
								<td ><?php echo $ipk;?></td>
								<td ><?php echo $ipk_lebih;?></td>
												
							</tr>
						<?php } ?> 
						<?php } ?> 
						
						<?php if($id=="40"){?>	
						<?php
						foreach($datatigasatusatu[$id] as $tigasatu){
						$id=$tigasatu->id;
						$tahun_akademik=$tigasatu->tahun_akademik;
						$ts_enam=$tigasatu->ts_enam;
						$ts_lima=$tigasatu->ts_lima;
						$ts_empat=$tigasatu->ts_empat;
						$ts_tiga=$tigasatu->ts_tiga;
						$ts_dua=$tigasatu->ts_dua;
						$ts_satu=$tigasatu->ts_satu;
						$ts=$tigasatu->ts;
						$ts_jumlah=$tigasatu->ts_jumlah;
						
						?>
							<tr>
								<td ><?php echo $tahun_akademik;?></td>
								<td ><?php echo $ts_enam;?></td>
								<td ><?php echo $ts_lima;?></td>
								<td ><?php echo $ts_empat;?></td>
								<td ><?php echo $ts_tiga;?></td>
								<td ><?php echo $ts_dua;?></td>
								<td ><?php echo $ts_satu;?></td>
								<td ><?php echo $ts;?></td>
								<td ><?php echo $ts_jumlah;?></td>
								
												
							</tr>
						<?php } ?> 
						<?php } ?> 
						
						<?php if($id=="41"){?>	
						<?php
						foreach($datatigasatusatu[$id] as $tigasatu){
						$id=$tigasatu->id;
						$tahun_akademik=$tigasatu->tahun_akademik;
						$ts_empat=$tigasatu->ts_empat;
						$ts_tiga=$tigasatu->ts_tiga;
						$ts_dua=$tigasatu->ts_dua;
						$ts_satu=$tigasatu->ts_satu;
						$ts=$tigasatu->ts;
						$ts_jumlah=$tigasatu->ts_jumlah;
						
						?>
							<tr>
								<td ><?php echo $tahun_akademik;?></td>
								<td ><?php echo $ts_empat;?></td>
								<td ><?php echo $ts_tiga;?></td>
								<td ><?php echo $ts_dua;?></td>
								<td ><?php echo $ts_satu;?></td>
								<td ><?php echo $ts;?></td>
								<td ><?php echo $ts_jumlah;?></td>
								
												
							</tr>
						<?php } ?> 
						<?php } ?> 
						
						<?php if($id=="42"){?>	
						<?php
						foreach($datatigasatusatu[$id] as $tigasatu){
						$id=$tigasatu->id;
						$tahun_akademik=$tigasatu->tahun_akademik;
						$ts_dua=$tigasatu->ts_dua;
						$ts_satu=$tigasatu->ts_satu;
						$ts=$tigasatu->ts;
						$ts_jumlah=$tigasatu->ts_jumlah;
						
						?>
							<tr>
								<td ><?php echo $tahun_akademik;?></td>
								<td ><?php echo $ts_dua;?></td>
								<td ><?php echo $ts_satu;?></td>
								<td ><?php echo $ts;?></td>
								<td ><?php echo $ts_jumlah;?></td>
								
												
							</tr>
						<?php } ?> 
						<?php } ?> 
						
						<?php if($id=="43"){?>	
						<?php
						foreach($datatigasatusatu[$id] as $tigasatu){
						$id=$tigasatu->id;
						$tahun_akademik=$tigasatu->tahun_akademik;
						$ts_satu=$tigasatu->ts_satu;
						$ts=$tigasatu->ts;
						$ts_jumlah=$tigasatu->ts_jumlah;
						
						?>
							<tr>
								<td ><?php echo $tahun_akademik;?></td>
								<td ><?php echo $ts_satu;?></td>
								<td ><?php echo $ts;?></td>
								<td ><?php echo $ts_jumlah;?></td>
								
												
							</tr>
						<?php } ?> 
						<?php } ?> 
						
						<?php if($id=="48"){?>	
						<?php
						foreach($datatigasatusatu[$id] as $tigasatu){
						$id=$tigasatu->id;
						$no=$tigasatu->no;
						$jenis_kemampuan=$tigasatu->jenis_kemampuan;
						$sangat_baik=$tigasatu->sangat_baik;
						$baik=$tigasatu->baik;
						$cukup=$tigasatu->cukup;
						$kurang=$tigasatu->kurang;
						$rencana_tindaklanjut=$tigasatu->rencana_tindaklanjut;
						
						?>
							<tr>
								<td ><?php echo $no;?></td>
								<td ><?php echo $jenis_kemampuan;?></td>
								<td ><?php echo $sangat_baik;?></td>
								<td ><?php echo $baik;?></td>
								<td ><?php echo $cukup;?></td>
								<td ><?php echo $kurang;?></td>
								<td ><?php echo $rencana_tindaklanjut;?></td>
												
							</tr>
						<?php } ?> 
						<?php } ?> 
						
						<?php if($id=="52"){?>	
						<?php
						foreach($datatigasatusatu[$id] as $tigasatu){
						$id=$tigasatu->id;
						$tahun_akademik=$tigasatu->tahun_akademik;
						$jml_lulusan_diwisuda=$tigasatu->jml_lulusan_diwisuda;
						$nm_lembaga=$tigasatu->nm_lembaga;
						$jml_lulusan_pesanterima=$tigasatu->jml_lulusan_pesanterima;
						$jumlah_lulusanditerima=$tigasatu->jumlah_lulusanditerima;						
						
						?>
							<tr>
								<td ><?php echo $tahun_akademik;?></td>
								<td ><?php echo $jml_lulusan_diwisuda;?></td>
								<td ><?php echo $nm_lembaga;?></td>
								<td ><?php echo $jml_lulusan_pesanterima;?>, <?php echo $jumlah_lulusanditerima;?></td>
								
												
							</tr>
						<?php } ?> 
						<?php } ?> 
						
						
						
						
						</table>
						
						
					</td>
				</tr>
				
				
				<tr valign="top">
					<td colspan="2">&nbsp;</td>
					<td style="width:100%;" ><?php echo $uraian;?></td>
				</tr>
				
				
				<?php } ?> 
			<?php } ?> 
			
			
			
		</table>
		
		
		<?php	
			  }				
			?>


</div>
 

			   
			   
 
	
			
  
</body>
</html>