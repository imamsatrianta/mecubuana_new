<html
    xmlns:o='urn:schemas-microsoft-com:office:office'
    xmlns:w='urn:schemas-microsoft-com:office:word'
    xmlns='http://www.w3.org/TR/REC-html40'>
    <head>
        <title>Generate a document Word</title>
        <!--[if gte mso 9]-->
    <xml>
        <w:WordDocument>
            <w:View>Print</w:View>
            <w:Zoom>90</w:Zoom>
            <w:DoNotOptimizeForBrowser/>
        </w:WordDocument>
    </xml>
	


<style>

p.MsoHeader, li.MsoHeader, div.MsoHeader{
    margin:0in;
    margin-top:.0001pt;
    mso-pagination:widow-orphan;
    tab-stops:center 3.0in right 6.0in;
}
p.MsoFooter, li.MsoFooter, div.MsoFooter{
    margin:0in 0in 1in 0in;
    margin-bottom:.0001pt;
    mso-pagination:widow-orphan;
    tab-stops:center 3.0in right 6.0in;
}
.footer {
    font-size: 9pt;
}
@page Section1{
    size:21cm 29.7cm ;
    margin:0.3in 0.3in 0.3in 0.3in;
    mso-header-margin:0.5in;
    mso-header:h1;
    mso-footer:f1;
    mso-footer-margin:0.5in;
    mso-paper-source:0;
}
div.Section1{
    page:Section1;
}
table#hrdftrtbl{
    margin:0in 0in 0in 15in;
} 


 
	
</style>
<style type="text/css" media="screen,print">
body {
    font-family: "Calibri", "Verdana","HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    font-size:12pt;
}
pageBreak {
  clear:all;
  page-break-before:always;
  mso-special-character:line-break;
}

.mytable{
                border:1px solid black;
                border-collapse: collapse;
                width: 100%;
            }
            .mytable tr th, .mytable tr td{
                border:1px solid black; 
                padding: 2px 2px;
            }
</style>
</head>
<body style='tab-interval:.5in'>
<div class="Section1">
	<p style="font-weight:bold;font-size:26px;text-align:center;">BORANG PROGRAM STUDI <BR> IDENTITAS</p>
	<table border="0" width="100%">
		<tr valign="center">
			<td style="width:35%;" >Program Studi (PS)</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->nm_prodi;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Jurusan/Departemen</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->nm_jenjangprodi;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Fakultas</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->fskultas;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Perguruan Tinggi</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" >Mercubuana</td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Nomor SK pendirian PS (*)</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->no_sk_pendirian;?></td>
		</tr><tr valign="center">
			<td style="width:35%;" >Tanggal SK pendirian PS</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->tgl_sk_pendirian;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Pejabat Penandatangan SK Pendirian PS </td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->pejabat_penandatangan_sk;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Bulan & Tahun Dimulainya Penyelenggaraan PS </td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->bulan_tahun_peneyelengara;?> <?php echo $identitasborang->tahun_peneyelengara;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Nomor SK Izin Operasional (*) </td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->no_skijin_op;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Tanggal SK Izin Operasional</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->tgl_sk_op;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Peringkat (Nilai) Akreditasi Terakhir</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->peringkat_ak_terakhir;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Nomor SK BAN-PT</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->no_sk_banpt;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Alamat PS</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->alamat_ps;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >No. Telepon PS</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->no_telp_ps;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >No. Faksimili PS</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->no_fak_ps;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Homepage dan E-mail PS</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->homepag;?>, <?php echo $identitasborang->hp_dan_email_ps;?></td>
		</tr>
		<tr valign="center">
			<td colspan="3">(*) : Lampirkan fotokopi SK terakhir</td>
			
		</tr>
	</table>
	<p style="margin-left:5%;">Bagi PS yang dibina oleh Departemen Pendidikan Nasional, sebutkan nama dosen tetap institusi yang terdaftar sebagai dosen tetap PS berdasarkan SK 034/DIKTI/Kep/2002, dalam tabel di bawah ini.</p>
	
	<table border="0" cellpadding="3" cellspacing="0" class="mytable" >
	
		<tr>
			<td align="center" class="ganjil">No</td>
			<td align="center" class="ganjil">Nama Dosen Tetap)</td>
			<td align="center" class="ganjil">NIDN</td>
			<td align="center" class="ganjil">Tgl. Lahir</td>
			<td align="center" class="ganjil">Jabatan Akademik</td>
			<td align="center" class="ganjil">Gelar Akademik</td>
			<td align="center" class="ganjil">Pendidikan D4, S1, S2, S3  dan Asal Perguruan Tinggi</td>
			<td align="center" class="ganjil">Bidang Keahlian untuk Setiap Jenjang Pendidikan</td>
			
		</tr>
		<tr>
			<td align="center" class="ganjil">(1)</td>
			<td align="center" class="ganjil">(2)</td>
			<td align="center" class="ganjil">(3)</td>
			<td align="center" class="ganjil">(4)</td>
			<td align="center" class="ganjil">(5)</td>
			<td align="center" class="ganjil">(6)</td>
			<td align="center" class="ganjil">(7)</td>
			<td align="center" class="ganjil">(8)</td>
		</tr>
		<?php 
			  $nodeosen=0;
			  foreach($datadosen as $dosen){
				  $nodeosen++;
		?>
		<tr>
			<td align="center" class="ganjil"><?php echo $nodeosen;?></td>
			<td align="left" class="ganjil"><?php echo $dosen->nm_karyawan;?></td>
			<td align="left" class="ganjil"><?php echo $dosen->nidn;?></td>
			<td align="left" class="ganjil"><?php echo $dosen->tgl_lahir;?></td>
			<td align="left" class="ganjil"><?php echo $dosen->jabatan_akademik;?></td>
			<td align="left" class="ganjil"><?php echo $dosen->gelar_akademim;?></td>
			<td align="left" class="ganjil"><?php echo $dosen->jenjang_prodi;?>, <?php echo $dosen->asal_ps;?></td>
			<td align="left" class="ganjil"><?php echo $dosen->bidang_keahlian;?></td>
		</tr>
		<?php }?>
	</table>
	<p style="margin-left:5%;">** NIDN : Nomor Induk Dosen Nasional </p>
	<p style="font-weight:bold;text-align:center;">IDENTITAS PENGISI BORANG PROGRAM STUDI</p>
	
	<?php 
			foreach($datapengisi as $pengisi){
				 
		?>
	<table border="0" width="100%">
		<tr valign="center">
			<td style="width:20%;" >Nama</td>
			<td style="width:1%;" >:</td>
			<td style="width:79%;" ><?php echo $pengisi->nm_karyawan;?></td>
		</tr>
		<tr valign="center">
			<td style="width:20%;" >NIDN</td>
			<td style="width:1%;" >:</td>
			<td style="width:79%;" ><?php echo $pengisi->nidn;?></td>
		</tr>
		<tr valign="center">
			<td style="width:20%;" >Jabatan</td>
			<td style="width:1%;" >:</td>
			<td style="width:79%;" ><?php echo $pengisi->nm_jabatan;?></td>
		</tr>
		<tr valign="center">
			<td style="width:20%;" >Tanggal Pengisian</td>
			<td style="width:1%;" >:</td>
			<td style="width:79%;" ><?php echo $pengisi->tgl_penisian;?></td>
		</tr>
		<tr valign="center">
			<td style="width:20%;" >Tanda Tangan</td>
			<td style="width:1%;" >:</td>
			<td style="width:20%;" >
				<table border="0" style="width:100px;" class="mytable">
					<tr valign="center">
					<td>&nbsp;<br>&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	
	</table>
	<?php }?>
	<p style="font-weight:bold;">STANDAR 1.  VISI, MISI, TUJUAN DAN SASARAN, SERTA STRATEGI PENCAPAIAN</p>
	
		<?php
		//print_r($databorang);
		$i=0;
		foreach($databorang as $borang){
		$id=$borang->id;
		$poin=$borang->poin;
		$judul=trim($borang->judul);
		$uraian=$borang->uraian;
		$level=$borang->level;
		$uraianborang=$datauraian[$id];
		$poinlain=substr($poin,-2,1);
		$jenis=$borang->jenis;
		//$uraianborang=$borang->uraianborang;
		//echo $judul;
		?>
		<table border="0" width="100%">
			
			<?php if($judul=="-"){?>
				<?php if($poinlain=='-'){?>
					<tr valign="top">
					<td style="width:2%;" >&nbsp;</td>
					<td style="width:95%;" colspan="2" ><?php echo $uraian;?></td>
					</tr>
					
					<tr valign="top">
						<td >&nbsp;</td>
						<td colspan="2"><table cellpadding="3" cellspacing="0" style="height:100px;min-height:100px;width:100%;"  class="mytable" ><tr valign="top"><td><?php echo $uraianborang;?></td></tr></table></td>
					</tr>
				
				<?php }else{ ?> 
					<?php if($jenis==0){?>
						<tr valign="top">
							<td style="width:2%;" ><?php echo  trim($poin);?></td>
							<td style="width:95%;" colspan="2" ><?php echo $uraian;?></td>
						</tr>
					<?php }else{ ?> 
						<tr valign="top">
							<td style="width:2%;" ><?php echo  trim($poin);?></td>
							<td style="width:95%;" colspan="2" ><?php echo $uraian;?></td>
						</tr>
						
						<tr valign="top" >
							<td >&nbsp;</td>
							<td colspan="2"><table cellpadding="3" cellspacing="0" style="height:100px;min-height:100px;width:100%;"  class="mytable" ><tr valign="top"><td><?php echo $uraianborang;?></td></tr></table></td>
						</tr>
					<?php } ?> 
					
				
				
				<?php }?>
			<?php }else{ ?> 
				<tr valign="top">
					<td style="width:2%;" ><?php echo trim($poin);?></td>
					<td style="width:95%;" colspan="2" ><?php echo $judul;?></td>
				</tr valign="top">
				<tr valign="top">
					<td colspan="2">&nbsp;</td>
					<td style="width:100%;" ><?php echo $uraian;?></td>
				</tr>
				
				<tr valign="top">
					<td colspan="2">&nbsp;</td>
					<td><table class="mytable" cellpadding="3" cellspacing="0" style="height:100px;min-height:100px;width:100%;"  ><tr valign="top"><td><?php echo $uraianborang;?></td></tr></table></td>
				</tr>
			
			<?php } ?> 
			
			
			
		</table>
		
		
		<?php	
			  }				
			?>


</div>
 

			   
			   
 

	<table id='hrdftrtbl' border='0' cellspacing='0' cellpadding='0'>
    <tr>
        <td>
	<div style='mso-element:footer' id="f1">
                <p class="MsoFooter">
                 
                            Halaman
                                <g:message code="offer.letter.page.label"/> <span style='mso-field-code: PAGE '></span> dari <span style='mso-field-code: NUMPAGES '></span>
                           
                </p>
    </div>
			
		  </td>
    </tr>
</table>	
			
  
</body>
</html>