<html
    xmlns:o='urn:schemas-microsoft-com:office:office'
    xmlns:w='urn:schemas-microsoft-com:office:word'
    xmlns='http://www.w3.org/TR/REC-html40'>
    <head>
        <title>Generate a document Word</title>
        <!--[if gte mso 9]-->
    <xml>
        <w:WordDocument>
            <w:View>Print</w:View>
            <w:Zoom>90</w:Zoom>
            <w:DoNotOptimizeForBrowser/>
        </w:WordDocument>
    </xml>
	


<style>

p.MsoHeader, li.MsoHeader, div.MsoHeader{
    margin:0in;
    margin-top:.0001pt;
    mso-pagination:widow-orphan;
    tab-stops:center 3.0in right 6.0in;
}
p.MsoFooter, li.MsoFooter, div.MsoFooter{
    margin:0in 0in 1in 0in;
    margin-bottom:.0001pt;
    mso-pagination:widow-orphan;
    tab-stops:center 3.0in right 6.0in;
}
.footer {
    font-size: 9pt;
}
@page Section1{
    size:21cm 29.7cm ;
    margin:0.3in 0.3in 0.3in 0.3in;
    mso-header-margin:0.5in;
    mso-header:h1;
    mso-footer:f1;
    mso-footer-margin:0.5in;
    mso-paper-source:0;
}
div.Section1{
    page:Section1;
}
table#hrdftrtbl{
    margin:0in 0in 0in 15in;
} 


 
	
</style>
<style type="text/css" media="screen,print">
body {
    font-family: "Calibri", "Verdana","HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    font-size:12pt;
}
pageBreak {
  clear:all;
  page-break-before:always;
  mso-special-character:line-break;
}

.mytable{
                border:1px solid black;
                border-collapse: collapse;
                width: 100%;
            }
            .mytable tr th, .mytable tr td{
                border:1px solid black; 
                padding: 2px 2px;
            }
</style>
</head>
<body style='tab-interval:.5in'>
<div class="Section1">
		<?php 
			  foreach($gruped as $grup){
				  $id=$grup->id;
		?>
	<p style="font-weight:bold;font-size:16px;text-align:center;"><?php echo $grup->poin;?>. <?php echo $grup->nm_group;?></p>
		<?php 
			foreach($dataedparameter[$id] as $parameter){
				 $idprm=$parameter->id;
		?>
		<p style="font-weight:bold;font-size:16px;"><?php echo $parameter->poin;?>. <?php echo $parameter->nm_evaluasidiri;?></p>
			
			
			<?php 
			foreach($dataedisi[$id][$idprm] as $isi){
				 $uraian=$isi->uraian;
			?>
				<div style="margin-left:1%;"><?php echo $uraian; ?></div>
			<?php }?>
			
			<?php 
			foreach($dataedparameterdua[$id][$idprm] as $prmdua){
				 $idprmdua=$prmdua->id;
			?>
				<p style="font-weight:bold;font-size:16px;margin-left:1%;"><?php echo $prmdua->nm_evaluasidiri;?></p>
				
				<?php 
				foreach($dataedisidua[$id][$idprm][$idprmdua] as $isidua){
					 $uraiandua=$isidua->uraian;
				?>
					<div style="margin-left:2%;"><?php echo $uraiandua; ?></div>
				<?php }?>
			
			<?php }?>
			
			
		
		
		<?php }?>
		
	<?php }?>

</div>
 

			   
			   
 

	<table id='hrdftrtbl' border='0' cellspacing='0' cellpadding='0'>
    <tr>
        <td>
	<div style='mso-element:footer' id="f1">
                <p class="MsoFooter">
                 
                            Halaman
                                <g:message code="offer.letter.page.label"/> <span style='mso-field-code: PAGE '></span> dari <span style='mso-field-code: NUMPAGES '></span>
                           
                </p>
    </div>
			
		  </td>
    </tr>
</table>	
			
  
</body>
</html>