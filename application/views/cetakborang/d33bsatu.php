<html
    xmlns:o='urn:schemas-microsoft-com:office:office'
    xmlns:w='urn:schemas-microsoft-com:office:word'
    xmlns='http://www.w3.org/TR/REC-html40'>
    <head>
        <title>Generate a document Word</title>
        <!--[if gte mso 9]-->
    <xml>
        <w:WordDocument>
            <w:View>Print</w:View>
            <w:Zoom>90</w:Zoom>
            <w:DoNotOptimizeForBrowser/>
        </w:WordDocument>
    </xml>
	


<style>

p.MsoHeader, li.MsoHeader, div.MsoHeader{
    margin:0in;
    margin-top:.0001pt;
    mso-pagination:widow-orphan;
    tab-stops:center 3.0in right 6.0in;
}
p.MsoFooter, li.MsoFooter, div.MsoFooter{
    margin:0in 0in 1in 0in;
    margin-bottom:.0001pt;
    mso-pagination:widow-orphan;
    tab-stops:center 3.0in right 6.0in;
}
.footer {
    font-size: 9pt;
}
@page Section1{
    size:21cm 29.7cm ;
    margin:0.3in 0.3in 0.3in 0.3in;
    mso-header-margin:0.5in;
    mso-header:h1;
    mso-footer:f1;
    mso-footer-margin:0.5in;
    mso-paper-source:0;
}
div.Section1{
    page:Section1;
}
table#hrdftrtbl{
    margin:0in 0in 0in 15in;
} 


 
	
</style>
<style type="text/css" media="screen,print">
body {
    font-family: "Calibri", "Verdana","HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    font-size:12pt;
}
pageBreak {
  clear:all;
  page-break-before:always;
  mso-special-character:line-break;
}

.mytable{
                border:1px solid black;
                border-collapse: collapse;
                width: 100%;
            }
            .mytable tr th, .mytable tr td{
                border:1px solid black; 
                padding: 2px 2px;
            }
</style>
</head>
<body style='tab-interval:.5in'>
<div class="Section1">
	<p style="font-weight:bold;font-size:26px;text-align:center;">BORANG <BR> UNIT PENGELOLA PROGRAM STUDI DIPLOMA</p>
	<p style="font-weight:bold;text-align:center;">IDENTITAS</p>
	<table border="0" width="100%">
		<tr valign="center">
			<td style="width:35%;" >Nama Perguruan Tinggi</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->nm_prodi;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Alamat</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->nm_jenjangprodi;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >No. Telepon</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->no_telp_ps;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >No. Faksimili</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->no_fak_ps;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Homepage dan E-Mail</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->homepage;?>, <?php echo $identitasborang->email;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" colspan="3" >Nomor dan Tanggal</td>
			
		</tr>
		
		<tr valign="center">
			<td style="width:35%;" >SK Pendirian Institusi</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->no_sk_pendirian;?>, <?php echo $identitasborang->tgl_sk_pendirian;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Pejabat yang Menerbitkan SK</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->pejabat_penerbit_sk;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" colspan="3" >Identitas berikut ini mengenai Unit Pengelola Program Studi Diploma dari Perguruan Tinggi :</td>
			
		</tr>
		
		<tr valign="center">
			<td style="width:35%;" >Nama Unit Pengelola Program Studi</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->unit_pengelola;?> </td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Alamat</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->alamat_pengelola;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >No. Telepon</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->no_telp_pengelola;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >No. Faksimili	</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->no_fax_pengelola;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Homepage dan E-Mail</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->homepage;?>,<?php echo $identitasborang->email;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" colspan="3" >Nomor dan Tanggal</td>
			
		</tr>
		
		<tr valign="center">
			<td style="width:35%;" >SK Pendirian Unit Pengelola Program Studi </td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->no_skpengelola;?>, <?php echo $identitasborang->tgl_sk_pengelola;?></td>
		</tr>
		<tr valign="center">
			<td style="width:35%;" >Pejabat yang Menerbitkan SK</td>
			<td style="width:1%;" >:</td>
			<td style="width:65%;" ><?php echo $identitasborang->pejabat_sk_pengelola;?></td>
		</tr>
		
	</table>
	<p style="margin-left:5%;">Program studi yang dikelola oleh unit pengelola program studi diploma:</p>
	<table border="0" cellpadding="3" cellspacing="0"  >
		<?php 
			  $nodeosen=0;
			  foreach($dataprodi as $prodi){
				  $nodeosen++;
		?>
		
		<tr>
			<td align="center" class="ganjil"><?php echo $nodeosen;?></td>
			<td align="center" class="ganjil"><?php echo $prodi->nm_prodi;?></td>
			<td align="center" class="ganjil">(<?php echo $prodi->nm_jenjangprodi;?>)</td>
			
		</tr>
		<?php }?>
	</table>	
	<p style="margin-left:0%;font-weight:bold; text-align:center;">Isian selanjutnya adalah informasi yang mencakup semua program studi 
yang dikelola oleh unit pengelola program studi diploma.
</p>
	<div style="margin-left:2%;">Keterangan:</div>
	<div style="margin-left:3%;">	Borang  ini diisi oleh unit pengelola program studi diploma.  Unit pengelola adalah lembaga yang melakukan fungsi manajemen (perencanaan, pengorganisasian, pengembangan staf, pengawasan, pengarahan, representasi, dan penganggaran) terutama dalam rangka resource deployment and mobilization, untuk penjaminan mutu program studi.  Unit pengelola program studi ditentukan oleh perguruan tinggi, misalnya pada  jurusan, departemen, fakultas, direktorat, atau sekolah tinggi.:</div>
	
	<p style="font-weight:bold;text-align:center;">IDENTITAS PENGISI BORANG PROGRAM STUDI</p>
	
	
	<?php 
			foreach($datapengisi as $pengisi){
				 
		?>
	<table border="0" width="100%">
		<tr valign="center">
			<td style="width:20%;" >Nama</td>
			<td style="width:1%;" >:</td>
			<td style="width:79%;" ><?php echo $pengisi->nm_karyawan;?></td>
		</tr>
		<tr valign="center">
			<td style="width:20%;" >NIDN</td>
			<td style="width:1%;" >:</td>
			<td style="width:79%;" ><?php echo $pengisi->nidn;?></td>
		</tr>
		<tr valign="center">
			<td style="width:20%;" >Jabatan</td>
			<td style="width:1%;" >:</td>
			<td style="width:79%;" ><?php echo $pengisi->nm_jabatan;?></td>
		</tr>
		<tr valign="center">
			<td style="width:20%;" >Tanggal Pengisian</td>
			<td style="width:1%;" >:</td>
			<td style="width:79%;" ><?php echo $pengisi->tgl_penisian;?></td>
		</tr>
		<tr valign="center">
			<td style="width:20%;" >Tanda Tangan</td>
			<td style="width:1%;" >:</td>
			<td style="width:20%;" >
				<table border="0" style="width:100px;" class="mytable">
					<tr valign="center">
					<td>&nbsp;<br>&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	
	</table>
	<?php }?>
	
	
	<p style="font-weight:bold;">STANDAR 1.  VISI, MISI, TUJUAN DAN SASARAN, SERTA STRATEGI PENCAPAIAN</p>
		<?php
		//print_r($databorang);
		$i=0;
		foreach($databorang as $borang){
		$id=$borang->id;
		$poin=$borang->poin;
		$judul=trim($borang->judul);
		$uraian=$borang->uraian;
		$level=$borang->level;
		$uraianborang=$datauraian[$id];
		$poinlain=substr($poin,-2,1);
		$jenis=$borang->jenis;
		//$uraianborang=$borang->uraianborang;
		//echo $judul;
		?>
		<table border="0" width="100%">
			
			<?php if($judul=="-"){?>
				<?php if($poinlain=='-'){?>
					<tr valign="top">
					<td style="width:2%;" >&nbsp;</td>
					<td style="width:95%;" colspan="2" ><?php echo $uraian;?></td>
					</tr>
					
					<tr valign="top">
						<td >&nbsp;</td>
						<td colspan="2"><table cellpadding="3" cellspacing="0" style="height:100px;min-height:100px;width:100%;"  class="mytable" ><tr valign="top"><td><?php echo $uraianborang;?></td></tr></table></td>
					</tr>
				
				<?php }else{ ?> 
					<?php if($jenis==0){?>
						<tr valign="top">
							<td style="width:2%;" ><?php echo  trim($poin);?></td>
							<td style="width:95%;" colspan="2" ><?php echo $uraian;?></td>
						</tr>
					<?php }else{ ?> 
						<tr valign="top">
							<td style="width:2%;" ><?php echo  trim($poin);?></td>
							<td style="width:95%;" colspan="2" ><?php echo $uraian;?></td>
						</tr>
						
						<tr valign="top" >
							<td >&nbsp;</td>
							<td colspan="2"><table cellpadding="3" cellspacing="0" style="height:100px;min-height:100px;width:100%;"  class="mytable" ><tr valign="top"><td><?php echo $uraianborang;?></td></tr></table></td>
						</tr>
					<?php } ?> 
					
				
				
				<?php }?>
			<?php }else{ ?> 
				<tr valign="top">
					<td style="width:2%;" ><?php echo trim($poin);?>111</td>
					<td style="width:95%;" colspan="2" ><?php echo $judul;?></td>
				</tr valign="top">
				<tr valign="top">
					<td colspan="2">&nbsp;</td>
					<td style="width:100%;" ><?php echo $uraian;?></td>
				</tr>
				
				<tr valign="top">
					<td colspan="2">&nbsp;</td>
					<td><table class="mytable" cellpadding="3" cellspacing="0" style="height:100px;min-height:100px;width:100%;"  ><tr valign="top"><td><?php echo $uraianborang;?></td></tr></table></td>
				</tr>
			
			<?php } ?> 
			
			
			
		</table>
		
		
		<?php	
			  }				
			?>


</div>
 

			   
			   
 

	<table id='hrdftrtbl' border='0' cellspacing='0' cellpadding='0'>
    <tr>
        <td>
	<div style='mso-element:footer' id="f1">
                <p class="MsoFooter">
                 
                            Halaman
                                <g:message code="offer.letter.page.label"/> <span style='mso-field-code: PAGE '></span> dari <span style='mso-field-code: NUMPAGES '></span>
                           
                </p>
    </div>
			
		  </td>
    </tr>
</table>	
			
  
</body>
</html>