<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Balaicms</title>
    
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 --> 
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/modern-business.css">
	<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<script src="<?php echo base_url();?>assets/js/jquery-1.10.2.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	 
	 
    </head>
 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Menu</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">
				<img class="slide-image" src="<?php echo base_url();?><?php echo $perusahaan->logo; ?>" width="100%" alt="rose">
				</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li  id="satu">
                        <a href="<?php echo base_url();?>home">Home</a>
                    </li>
					 <li  id="dua">
                        <a href="<?php echo base_url();?>profil">Profil</a>
                    </li>
					<li id="tiga">
                        <a href="<?php echo base_url();?>produk">Produk</a>
                    </li>
					
                    <li id="empat">
                        <a href="<?php echo base_url();?>visimisi">Visi Misi</a>
                    </li>
					
					<li id="lima">
                        <a href="<?php echo base_url();?>kegiatan">Kegiatan</a>
                    </li>
					 <li  id="enam">
                        <a href="<?php echo base_url();?>galeri">Galeri</a>
                    </li>
                    <li id="tujuh">
                        <a href="<?php echo base_url();?>kontak">Kontak</a>
                    </li>
                    
                    
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">404
                    <small>Page Not Found</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="home">Home</a>
                    </li>
                    <li class="active">404</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->



        <hr>

    </div>
	
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
	<script>
	$(document).ready(function ($) {
		var path = window.location.pathname;
		var page = path.split("/").pop();
		if(page=="home"){
			 $("#satu").addClass('active');
		}
		else if(page=="profil"){
			 $("#dua").addClass('active');
		}
		else if(page=="produk"){
			 $("#tiga").addClass('active');
		}
		else if(page=="visimisi"){
			 $("#empat").addClass('active');
		}
		else if(page=="kegiatan"){
			 $("#lima").addClass('active');
		}
		else if(page=="galeri"){
			 $("#enam").addClass('active');
		}
		else if(page=="kontak"){
			 $("#tujuh").addClass('active');
		}else{
			$("#satu").addClass('active');
		}

 });
	</script>
</html>
