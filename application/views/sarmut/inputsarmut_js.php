<script>

	  $(document).ready(function ($) {
		  CKEDITOR.replace('uraian_sarmutformat');
		  $("select").select2();
		   $("#jenis_sarmut").val('').trigger('change');
		  
		$(".date-picker").datepicker({ autoclose: true});
			
		//  $("#waktu").select2();
		  $.ajax({
			type: "POST",
			dataType:"JSON",
			url: "<?php echo base_url();?>global_combo/getStandar",
			success: function(result) {  
			$.each(result, function(key, val) {	
			$("#id_standar").append('<option value="'+val.id+'">'+val.nm_dokumen+'</option>');
				
			});
							  
			}
			});
			
			  $.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getTahunakademik",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_tahunakademik").append('<option value="'+val.id+'">'+val.nm_tahun_akademik+'</option>');
				$("#id_tahunakademikctk").append('<option value="'+val.id+'">'+val.nm_tahun_akademik+'</option>');	
				});
								  
				}
				});
				
			
			
			$.ajax({
			type: "POST",
			dataType:"JSON",
			url: "<?php echo base_url();?>global_combo/getUnitdepprodifilter",
			success: function(result) {  
			$.each(result, function(key, val) {	
			$("#id_unitcetak").append('<option value="'+val.id+"-"+val.jenis+'">'+val.nm_unit+'</option>');	
			});
							  
			}
			});
			
	  });
	  
	function cekMkp(){
		var jenisarmut=$('#jenis_sarmut').val(); 
		if(jenisarmut=="1"){
			$('#drsm').show();
		}else if(jenisarmut=="2"){
			$('#drsm').hide();
			$('#id_mkp_kegiatan').val('');
			$('#no').val('');
		}else{
			$('#drsm').hide();
			$('#id_mkp_kegiatan').val('');
			$('#no').val('');
		}
		
	}
	 $("#tbh").click(function () {
		$('#drsm').hide();
		$('#id_mkp_kegiatan').val('');
		$('#uraian_sarmut').val('');
		$('#id_tahunakademik').val('').trigger('change');
		$('#jenis_sarmut').val('').trigger('change');
    });

	  
	function cariDatamkp(){
		$('#modalTable').modal('show'); 
		$("#modalTable").css({"z-index":"1060"});
		$('html,body').scrollTop(0);
		loadDatamkpkegiatan(1, 15,'desc');
	}	
	  
	 function tutupFormpopup(){
      	$('#modalTable').modal('hide'); // show bootstrap modal
		bukaModal();
	}
	function bukaModal(){
		
		$("#modal_formck").css({"overflow-y":"scroll"});
	
	}

	function getParammkp(params) {
			var id_mkp=$('#id_mkp').val();

		    return {
		        id_mkp: id_mkp,
				limit: params.limit,
				offset: params.offset,
				search: params.search,
				sort: params.sort,
				order: params.order
		    };
		}	
	  
	 function operateFormatterPilih(value, row, index) {
       return [
            '<a class="btn btn-sm btn-primary btn-xs" id="pilih" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Pilih" >',
            'Pilih',
            '</a> '
        ].join('');
    }
	
	
  window.operateEventspilih = {
        'click #pilih': function (e, value, row, index) {
            cariDatapopup(row);
        }
    };
		
	function cariDatapopup(row){
	//	 alert(row.nm_grup_tiga);
		// $("#uraian_sarmut").val(row.kegiatan);
		 CKEDITOR.instances['uraian_sarmutformat'].setData(row.kegiatan);
		 $("#id_kegiatan_mkp").val(row.id);
		 tutupFormpopup();
	 }
	  
	  function loadDatamkpkegiatan(number, size,order){
		  var id_mkp=$('#id_mkp').val();
			 var $table = $('#tabledata');
            var offset=(number - 1) * size;
            var limit=size;
            $.ajax({
                    type: "POST",
                    url: "inputsarmut/loaddatamkp?order="+order+"&limit="+limit+"&offset="+offset+"&id_mkp="+id_mkp,
                    dataType:"JSON",
                    success: function(result){
    				 $table.bootstrapTable('load', result);
    			
                    }
                });
        }
	  
	   function editFormtambah(row){
		    $("#id").val(row.id).trigger('change');
		  $("#id_tahunakademik").val(row.id_tahunakademik).trigger('change');
		  $("#id_mkp").val(row.id_mkp).trigger('change');
		   $("#nm_mkp").val(row.nm_mkp).trigger('change');
		   $("#target_kuantitatif").val(row.target_kuantitatif).trigger('change');
		   $("#target_kualitatif").val(row.target_kualitatif).trigger('change');
		   $("#kinerja_lalu").val(row.kinerja_lalu).trigger('change');
		  $('#uraian_sarmutformat').val(row.uraian_sarmut).trigger('change');
		 // alert(row.uraian_sarmut)
		  $('#jenis_sarmut').val(row.jenis_sarmut).trigger('change');
		 var jenisarmut=row.jenis_sarmut; 
		  if(jenisarmut=="1"){
			$('#drsm').show();
			$('#id_kegiatan_mkp').val(row.id_kegiatan_mkp);
			$('#no').val(row.no);
		}else if(jenisarmut=="2"){
			$('#drsm').hide();
			$('#id_kegiatan_mkp').val('');
		}else{
			$('#drsm').hide();
			$('#id_kegiatan_mkp').val('');
		}
		
		CKEDITOR.instances['uraian_sarmutformat'].setData(row.uraian_sarmut);
		
		/*
		var idunitarray = new Array();
		  var unitid=row.unit_id;
	//	alert(unitid)
		 // var unitid=row.id_unit;
		   var splits = unitid.split(",");
		   for(i = 0; i < splits.length; i++){
				//alert(splits[i]); 
				idunitarray.push (splits[i]);
			}
			
		 var idunitok=idunitarray;
		// alert(idunitok)
		 
		$('#id_stdr').val(row.id_standar).trigger('change');
		$('#nm_standar').val(row.nm_dokumen).trigger('change');
		*/
	  }
	
	  
	function operateFormatter(value, row, index) {
       return [
			'<a class="btn btn-sm btn-primary btn-xs" id="kegiatan" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Kegiatan" ><i id="keg" class="glyphicon glyphicon-plus-sign" ></i></a> ',
			'<?php echo aksesUbahck() ?>',
			'<?php echo aksesHapussatu() ?>'
        ].join('');
    }  
	  
	function kegiatanData(row){
//	alert(row.id);
        $('#formkeg')[0].reset(); 
      $('#modal_formkeg').modal('show'); 
      $('.modal-title').text('Form Tambah Kegiatan Sarmut'); 
	  document.getElementById('btnSavekeg').disabled=false;
	    
		$('#formkeg').bootstrapValidator('resetForm',false);
		  
		   $('#id_sarmut').val(row.id);
	      document.getElementById('setkeg').value=0;
		  
    }  
	  
	  
	// kegiatan
	$('#formkeg')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDatakeg();
		 e.preventDefault();
    });
	
	function simpanDatakeg(){
	document.getElementById("btnSavekeg").disabled=true;

		var fd = new FormData(document.getElementById("formkeg"));
			fd.append("kegiatanisi",'');
			$.ajax({
			  url: "<?php echo base_url();?>sarmut/inputsarmut/simpanDatakeg",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						 keluarLogin();
					}
					
					 tutupFormkeg();
					 loadData(1, 15,'desc');
			});
			return false;		
 
}

 function tutupFormkeg(){
		 $('#formkeg')[0].reset(); // reset form on modals 
      	$('#modal_formkeg').modal('hide'); // show bootstrap modal
	}
	function operateDetail(index, row,element){
		$.ajax({
				  type: "POST",
				  url: "inputsarmut/loaddatadetailkegiatan?id="+row['id']+"&index="+index,
				  dataType:"JSON",
				  success: function(result) { 

				  	var x1 = "no_baris_"+index;
				  	var el = "tr[data-index='"+index+"']"
					$(el).next('tr.detail-view').addClass(x1);

					var id,id_sarmut,uraian_kegiatan,tgl_mulai,tgl_selesai,angaran,score,presentase,anggaran;
					
					
					var tbl_det;
					tbl_det = '<td colspan="10" align="center"><table border="1" width="100%" style="border-style: solid;border-color:#9F9492;">';
					tbl_det += '<tr style="border-color:#9F9492;">';
					tbl_det += '<td align="center" width="7%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Action</td>';
					tbl_det += '<td align="center" width="10%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;"> Tanggal Mulai</td>';
					tbl_det += '<td align="center" width="10%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Tanggal Selesai </td>';
					tbl_det += '<td align="center" width="50%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Nama Kegiatan</td>'
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Anggaran</td>'
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Ket Proses</td>'
					tbl_det += '<td align="center" width="10%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Presentase</td>'
					tbl_det += '</tr>';
        			tbl_det += '<tr style="border-color:#9F9492;">';

        			$.each(eval(result), function (key,value) {
						
        				id = value['id'];
						id_sarmut = value['id_sarmut'];
        				uraian_kegiatan = value['uraian_kegiatan'];
        				tgl_mulai = value['tgl_mulai'];
        				tgl_selesai= value['tgl_selesai'];
						score=value['score'];
						presentase=value['presentase'];
						anggaran=value['anggaran'];
					//	alert(idkeg);
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp;<a class="btn btn-sm btn-primary btn-xs" onclick="editFormkegiatan(\''+id+'\',\''+id_sarmut+'\',';
						tbl_det += '\''+uraian_kegiatan+'\',\''+tgl_mulai+'\',\''+tgl_selesai+'\',\''+anggaran+'\')" id="editdata" class="btn btn-sm btn-primary"  href="#" title="Ubah Kegiatan" ><i id="edtkeg" class="glyphicon glyphicon-edit" ></i></a>';
						tbl_det += '&nbsp;&nbsp;<a class="btn btn-sm btn-danger btn-xs" onclick="hapusKegiatan(\''+id+'\')" id="hapuskegiatan" class="btn btn-sm btn-primary"  href="#" title="Hapus Kegiatan" ><i id="edtkeg" class="glyphicon glyphicon-trash" ></i></a> </td>';
        				tbl_det += '<td style="border-color:#9F9492;" >&nbsp;&nbsp; '+tgl_mulai+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+tgl_selesai+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+uraian_kegiatan+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+anggaran+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+score+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+presentase+'</td>';
        				tbl_det += '</tr>';

        			});
        			tbl_det += '</table></td>';

					$(".detail-view."+x1).html(tbl_det);

				  }


		});

    }
	
	function editFormkegiatan(id_keg,id_sarmut,uraian_kegiatan,tgl_mulai,tgl_selesai,anggaran){
		 // $('#formkeg')[0].reset(); 
      $('#modal_formkeg').modal('show'); 
      $('.modal-title').text('Form Ubah Kegiatan Sarmut'); 
	  document.getElementById('btnSavekeg').disabled=false;
	    
		
		  
		   $('#id_sarmut').val(id_sarmut);
	      document.getElementById('setkeg').value=1;
		   $('#id_keg').val(id_keg);
		    $("#tgl_mulai").val(tgl_mulai);
			$("#tgl_selesai").val(tgl_selesai);
			$("#uraian_kegiatan").val(uraian_kegiatan);
			$("#anggaran").val(anggaran);
		//$('#formkeg').bootstrapValidator('resetForm',false);
	}
	
	function hapusKegiatan(id){
		bootbox.confirm("Yakin Data akan di Hapus?", function(result) {
			if(result==true){
				 $.ajax({
                    url: 'inputsarmut/hapusdatakegiatan?id='+id,
                    type: 'POST',
                    success: function (data) {
					//alert(data);
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
					
						
						loadData(1, 15,'desc');
                        
                    }
                })
			}
               
        });
	}
	
	function operateDetailkegiatan(index, row,element){
		$.ajax({
				  type: "POST",
				  url: "inputsarmut/loaddatadetailkegiatanrel?id="+row['id']+"&index="+index,
				  dataType:"JSON",
				  success: function(result) { 

				  	var x1 = "no_baris_"+index;
				  	var el = "tr[data-index='"+index+"']"
					$(el).next('tr.detail-view').addClass(x1);

					var id,tgl_realisasi,presentase_realisasi,kendala,tindakan;
					
					
					var tbl_det;
					tbl_det = '<td colspan="13" align="center"><table border="1" width="100%" style="border-style: solid;border-color:#9F9492;">';
					tbl_det += '<tr style="border-color:#9F9492;">';
					tbl_det += '<td align="center" width="30%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;"> Tanggal Relisasi</td>';
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Presentase </td>';
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Kendala</td>'
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Tindakan</td>'
					tbl_det += '</tr>';
        			tbl_det += '<tr style="border-color:#9F9492;">';

        			$.each(eval(result), function (key,value) {
						
        				id = value['id'];
						tgl_realisasi = value['tgl_realisasi'];
        				kendala= value['kendala'];
						tindakan=value['tindakan'];
						presentase_realisasi=value['presentase_realisasi'];
					//	alert(idkeg);
        				tbl_det += '<td style="border-color:#9F9492;" >&nbsp;&nbsp; '+tgl_realisasi+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+presentase_realisasi+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+kendala+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+tindakan+'</td>';
        				tbl_det += '</tr>';

        			});
        			tbl_det += '</table></td>';

					$(".detail-view."+x1).html(tbl_det);

				  }


		});

    }
	
	
	function scoreKet(value, row, index){
		if(value=="0"){
			return "Open yang belum waktunya";
		}else if(value=="1"){
			return "Open Terlambat";
		}else if(value=="2"){
			return "Open sudah waktu";
		}else if(value=="3"){
			return "In Progress Terlambat";
		}else if(value=="4"){
			return "In Progress Sudah waktu";
		}else if(value=="5"){
			return "Done Terlambat";
		}else if(value=="6"){
			return "Done";
		}
	}
	
	function buatWaena(value, row, index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];
		if(row.score=="0"){
			return {css: {"background-color": "#fff",}};
		}else if(row.score=="1"){
			return {css: {"background-color": "#FF0000",}};
		}else if(row.score=="2"){
			return {css: {"background-color": "#FFD700",}};
		}else if(row.score=="3"){
			return {css: {"background-color": "#4682B4",}};
		}else if(row.score=="4"){
			return {css: {"background-color": "#87CEEB",}};
		}else if(row.score=="5"){
			return {css: {"background-color": "#32CD32",}};
		}else if(row.score=="6"){
			return {css: {"background-color": "#228B22",}};
		}
		return {};
}
	function buatWaenapresentase(value, row, index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];
		if(row.presentase<=20){
			return {css: {"background-color": "#FF4500",}};
		}else if(row.presentase<=40){
			return {css: {"background-color": "#FFFF00",}};
		}else if(row.presentase<=60){
			return {css: {"background-color": "#00BFFF",}};
		}else if(row.presentase<=80){
			return {css: {"background-color": "#1E90FF",}};
		}else if(row.presentase<=100){
			return {css: {"background-color": "#008000",}};
		}
		return {};
}

function strip_html_tags(str)
{
   if ((str===null) || (str===''))
       return false;
  else
   str = str.toString();
  return str.replace(/<[^>]*>/g, '');
}


function htmlMutu(value, row, index){
	var mutu=row.uraian_target;
	return strip_html_tags(mutu);
}

function printHeder(){ 
	$('#formctk')[0].reset(); 
	 $('#formctk').bootstrapValidator('resetForm', true); 
      $('#modal_formctk').modal('show'); 
      $('.modal-title').text('Form Cetak Rencana Sarmut'); 
	  
		//window.open('<?php echo base_url();?>master/mkp/cetakMkp?id=1','_blank');
		//window.open('<?php echo base_url();?>master/mkp/cetakMkpword?id=1');
	}

	function cetakDatamkp(){
		var id_tahunakademikctk=$('#id_tahunakademikctk').val();
		var id_unitcetak=$('#id_unitcetak').val();
		window.open('<?php echo base_url();?>sarmut/inputsarmut/cetakRencanasarmut?id_tahunakademi='+id_tahunakademikctk+'&id_unit='+id_unitcetak);
	}
	
	function cariDatastandar(){
	$('#modalTablestd').modal('show'); 
	$("#modalTablestd").css({"z-index":"1060"});
	$('html,body').scrollTop(0);
	loadDatastandar(1, 15,'desc');
	}	
	  
	 function tutupFormpopupstd(){
      	$('#modalTablestd').modal('hide'); // show bootstrap modal
		bukaModal();
	}
	function getParamstandar(params) {
		    return {
				limit: params.limit,
				offset: params.offset,
				search: params.search,
				sort: params.sort,
				order: params.order
		    };
		}	
	  
	 function operateFormatterPilihstd(value, row, index) {
       return [
            '<a class="btn btn-sm btn-primary btn-xs" id="pilihstd" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Pilih" >',
            'Pilih',
            '</a> '
        ].join('');
    }
	
	
  window.operateEventspilihstd = {
        'click #pilihstd': function (e, value, row, index) {
            cariDatapopupstd(row);
        }
    };
		
	function cariDatapopupstd(row){
	//	 alert(row.nm_grup_tiga); 
		 $("#id_mkp").val(row.id);
		 $("#nm_mkp").val(row.nm_mkp);
		 tutupFormpopupstd();
	 }
	 
	  function loadDatastandar(number, size,order){
			 var $table = $('#tabledatastd');
            var offset=(number - 1) * size;
            var limit=size;
            $.ajax({
                    type: "POST",
                    url: "inputsarmut/loaddatastandar?order="+order+"&limit="+limit+"&offset="+offset,
                    dataType:"JSON",
                    success: function(result){
    				 $table.bootstrapTable('load', result);
    			
                    }
                });
        }
		


function htmlFormat(value, row, index){
	var mutu=row.kegiatan;
	var res = mutu.substr(0, 200);
	return strip_html_tags(res);
}
function htmlFormatsarmut(value, row, index){
	var mutu=row.uraian_sarmut;
	var res = mutu.substr(0, 200);
	return strip_html_tags(res);
}



function tambahDatack(){
		var halaman=document.getElementById("judulmenu").innerHTML; 
		//alert(halaman);
		 
     $('#formck')[0].reset(); 
	 $('#formck').bootstrapValidator('resetForm', true);
      $('#modal_formck').modal('show'); 
	  /* $('#modal_form').modal('show')
              .draggable({ handle: ".modal-header" });
			  */
      $('.modal-title').text('Form Tambah Sarmut'); 
	  document.getElementById('set').value=0;
	  CKEDITOR.instances['uraian_sarmutformat'].setData('');
	  document.getElementById('btnSave').disabled=false;
	}
function editFormck(row){
	var halaman=document.getElementById("judulmenu").innerHTML; 
        $('#formck')[0].reset(); 
      $('#modal_formck').modal('show'); 
      $('.modal-title').text('Form Ubah Sarmut'); 
	  document.getElementById('set').value=1;
	  document.getElementById('btnSave').disabled=false;
	  editFormtambah(row);
}	
$('#formck')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDatack();
		 e.preventDefault();
		 
		 
    });
	
	
	function simpanDatack(){
	 var edituraian = CKEDITOR.instances.uraian_sarmutformat;
	var uraianok=edituraian.getData();
	var uraian = uraianok.replace("&nbsp;", "");
	
	 document.getElementById("btnSave").disabled=true;
	 var fd = new FormData(document.getElementById("formck"));
			fd.append("uraian_sarmutformat",uraian);
			$.ajax({
				 url: "inputsarmut/simpanData",
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  processData: false,  // tell jQuery not to process the data
			  contentType: false   // tell jQuery not to set contentType
			}).done(function( result ) {
				try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						  keluarLogin();
					}
					
					tutupFormckdata();
					loadData(1, 15,'desc');
			});
			return false;	
			
	 
 }
	
	function tutupFormckdata(){
		//alert("c")
		// $('#formck')[0].reset(); // reset form on modals 
      	$('#modal_formck').modal('hide'); // show bootstrap modal
		
	}
	
  </script>