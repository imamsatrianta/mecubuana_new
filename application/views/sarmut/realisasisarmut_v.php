
	<style>
.hiddenfilez{
	 width: 0px;

 height: 0px;

 overflow: hidden;
}

</style>
<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
			
			<div class="nav-tabs-custom">
				
						<div id="toolbar">
						<?php
						$id_level=$this->session->userdata('id_level');
						 if( $id_level=="3" or  $id_level=="4" or  $id_level=="5"){
							 echo aksesTambahupl();
						 }
						?>
						 <?php
						echo aksesHapus();
						?>
						</div><table id="table" 
						data-toolbar="#toolbar"
							   data-toggle="table"
							   data-search="true"
							   data-show-refresh="true"
							   data-show-columns="true"
							   data-show-export="true"
							   data-minimum-count-columns="2"
							   data-filter-control="true"
							   data-pagination="true"
							   data-url="realisasisarmut/loaddataTabel"
							   data-side-pagination="server"
							   data-pagination="true"
							   data-sort-name="id"
							   data-sort-order="desc">
							<thead>	
							<tr>
								<th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
								<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
								<th data-field="tgl_mulai"  data-halign="center" data-align="left"  data-sortable="true" >Tanggal Mulai  </th>
								<th data-field="tgl_selesai"  data-halign="center" data-align="left"  data-sortable="true" >Tanggal Selesai  </th>
								<th data-field="uraian_kegiatan"  data-halign="center" data-align="left"  data-sortable="true" >Nama Kegiatan  </th>
								<th data-field="kendala"  data-halign="center" data-align="left"  data-sortable="true" >Kendala  </th> 
								<th data-field="presentase_realisasi"  data-halign="center" data-align="left"  data-sortable="true" >Presentase  </th> 
								<th data-field="tindakan"  data-halign="center"  data-align="left"  data-sortable="true" >Tindakan Korektif  </th>
								<th data-field="file_upload"  data-halign="center"  data-align="left"  data-sortable="true" data-formatter="namaDok" >Dokumen  </th>
							</tr>
							</thead>
						</table>
					
					
					 
					 
			</div>
              
            </div><!-- /.col -->
			
			
			
			
			
			
          </div>  
       
		
</div> 




<div class="modal fade" id="modal_formupl" role="dialog">
  <div class="modal-dialog" style="width:60%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formupl" name="formupl" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
		  <div class="form-group ">
			<label class="control-label col-md-4" for1="menudes">Kegiatan Sarmut  </label> 
			<div class="col-md-6"> 
				 <input type="hidden" value="" name="id_sarmut_kegiatan" id="id_sarmut_kegiatan"/>
				<input name="kegiatansarmut" class="form-control input-sm nomor" id="kegiatansarmut" required="required" type="text">
		    </div>
			<div class="col-md-2">
					<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onclick="cariKegiatan()"  >
					<i class="fa fa-search"></i>
					Cari
					</button>
				
				</div>
		</div>
		
         <div class="form-group ">
			<label class="control-label col-md-4" for1="menudes">Tanggal  </label> 
			<div class="col-md-4">
			<input type="text" class="form-control date-picker input-sm" id="tgl_realisasi" name="tgl_realisasi"  placeholder="yyyy-mm-dd" style="width: 100%;" >
		    </div>
		</div>
		<!--
		<div class="form-group ">
			<label class="control-label col-md-4" for1="menudes">Kegiatan Realisasi  </label> 
			<div class="col-md-8">
				<textarea class="form-control" rows="2" id="uraian_realisasi" name="uraian_realisasi"></textarea>
		    </div>
		</div>
		-->
		<div class="form-group ">
			<label class="control-label col-md-4" for1="menudes">Presentase  </label> 
			<div class="col-md-3">
				<input name="presentase_realisasi" class="form-control input-sm nomor" id="presentase_realisasi" required="required" type="text">
		    </div>
		</div>
		
		
		<div class="form-group ">
			<label class="control-label col-md-4" for1="menudes">Kendala  </label> 
			<div class="col-md-8">
				<input name="kendala" class="form-control input-sm" id="kendala" required="required" type="text">
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-4" for1="menudes">Tindakan Korektif  </label> 
			<div class="col-md-8">
				<input name="tindakan" class="form-control input-sm" id="tindakan" required="required" type="text">
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-4" for1="menudes">Dokumen  </label> 
			<div class="col-md-8">
				<a id="test" onclick="takenFile()"> 
					<embed src="" width="200px"  value="Ambil Lampiran" height="200px" id="pic" >Upload</embed>
					
					</a>
					<div class="hiddenfilez"><input name="file_uploadda"  id="file_uploadda"  onchange="loadFile(event)" type="file"></div> 	
		    </div>
		</div>
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
	<div class="modal fade" id="modalTable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:70%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4>Data Kegiatan </h4>
                    </div>
                    <div class="modal-body">
                        <table id="tabledata"
                           data-toggle="table"
                           data-search="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                           data-pagination="true"
						   data-height="500"
                           data-url="<?php echo base_url();?>sarmut/realisasisarmut/loaddatakegiatan"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-sort-order="asc">
                            <thead>
                            <tr>
                            
							<th data-field="uraian_kegiatan"  data-halign="center" data-align="left"  data-sortable="true">Nama Kegiatan </th>
							<th data-field="tgl_mulai"  data-halign="center" data-align="left"  data-sortable="true">Tanggal Mulai  </th>
							<th data-field="tgl_selesai"  data-halign="center" data-align="left"  data-sortable="true">Tanggal Selesai  </th>
								<th data-field="cari"  id="pilih" data-halign="center" data-align="center"
                    data-formatter="operateFormatterPilih" data-events="operateEventspilih">Pilih Data</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->		
	
	
	
	
	
	
	<script src="<?php echo base_url();?>js/atribut.js"></script>
  