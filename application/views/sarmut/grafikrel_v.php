<script src="<?php echo base_url();?>assets/code/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/code/modules/data.js"></script>
<script src="<?php echo base_url();?>assets/code/modules/exporting.js"></script>


<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
				<div class="nav-tabs-custom">
					
					 <div class="form-group ">	
						<label class="control-label col-md-3" for1="menudes">Tahun Akademik    </label> 
						<div class="col-md-5">
							<select type="select" name="id_tahunakademik" class="form-control select2 input-sm" id="id_tahunakademik" required="required"  style="width: 100%;" >
								 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
							</select>
							
						</div>
						<div class="col-md-2">
								<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onclick="cariGrafik()"  >
								<i class="fa fa-search"></i>
								Cari
								</button>
							
						</div>
					</div>
		
					<div id="garisbatang" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
					
						 
				</div>
              
            </div><!-- /.col -->
			
			
			
			
			
			
          </div>  
       
		
</div> 



	<script>
	
	$("select").select2();
	$("#id_tahunakademik").val('').trigger('change');
	$.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getTahunakademik",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_tahunakademik").append('<option value="'+val.id+'">'+val.nm_tahun_akademik+'</option>');
				});
								  
				}
				});
				
	//grafikGugatan(0);
	
	
	function cariGrafik(){
		var id_tahunakademik=$("#id_tahunakademik").val();
		grafikGugatan(id_tahunakademik);
	}
	function grafikGugatan(id_tahunakademik){
	var arrayunit=[];
	var datatarget=[];
	var datarealisasi=[];
	$.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>sarmut/grafikrel/getData?id_tahunakademik="+id_tahunakademik,
				success: function(result) {  
				var total=0;
				$.each(result, function(key, val) {	
				 var jmlpersen=parseFloat(val.jmlpersen);
				 var jml=parseFloat(val.jml);
				 var nm_unit=val.nm_unit;
				 var nilai=jmlpersen/jml;
				 var nilaisisa=100-nilai;
				 datatarget.push(nilaisisa);
				 datarealisasi.push(nilai);
				 arrayunit.push(nm_unit);
					});
					
	
Highcharts.chart('garisbatang', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Pencapaian Target dan Realisasi'
    },
    xAxis: {
        categories: arrayunit
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Presentase Target dan Realisasi'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
    series: [{
        name: 'Target',
        data: datatarget
    }, {
        name: 'Realisasi',
        data: datarealisasi
    }]
});
						
						

						}
						});	

}

	
	
</script>
	
	
	
	
	<script src="<?php echo base_url();?>js/atribut.js"></script>
  