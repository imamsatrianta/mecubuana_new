<script type="text/javascript" src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
	
<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
			
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
				  <li class="active"><a href="#tab_1" data-toggle="tab">Sarmut	</a></li>
				  <li><a href="#tab_2" data-toggle="tab">Monitoring Kegiatan Sarmut</a></li>
				
				  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
				</ul>
				<div class="tab-content">
				
					<div class="tab-pane active" id="tab_1">
						<div id="toolbar">
						<?php
						$id_level=$this->session->userdata('id_level');
						 if( $id_level=="3" or  $id_level=="4" or  $id_level=="5"){
							 echo aksesTambahck();
						 }
						?>
						 <?php
							 echo aksesHapus();
						?>
						<?php
						echo aksesPrinheder();
						?>
						</div><table id="table" 
						data-toolbar="#toolbar"
							   data-toggle="table"
							   data-search="true"
							   data-show-refresh="true"
							   data-show-columns="true"
							   data-show-export="true"
							   data-minimum-count-columns="2"
							   data-filter-control="true"
							   data-pagination="true"
							   data-url="inputsarmut/loaddataTabel"
							   data-side-pagination="server"
							   data-pagination="true"
							   data-sort-name="id"
							   data-sort-order="desc"
							   data-detail-view="true"
						   data-detail-formatter="operateDetail">
							<thead>	
							<tr>
								<th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
								<th data-field="selling"  data-halign="center" data-align="center" data-formatter="operateFormatter" data-events="operateEvents">Action</th>
								<th data-field="nmunit"  data-halign="center" data-align="left"  data-sortable="true" >Unit/Direktorat/Prodi  </th>
								<th data-field="nm_mkp"  data-halign="center" data-align="left"  data-sortable="true" >Nama MKP  </th>
								<th data-field="nm_tahun_akademik"  data-halign="center" data-align="left"  data-sortable="true" >Tahun Akademik  </th>
								<th data-field="uraian_sarmut"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="htmlFormatsarmut" >Uraian Sarmut  </th>
								<th data-field="target_kuantitatif"  data-halign="center" data-align="left"  data-sortable="true" >Target Kuantitatif  </th>
								<th data-field="target_kualitatif"  data-halign="center" data-align="left"  data-sortable="true" >Cara Mengukur  </th>
								<th data-field="kinerja_lalu"  data-halign="center" data-align="left"  data-sortable="true" >Kinerja Lalu  </th>
							
							</tr>
							</thead>
						</table>
					</div>
					
					 <div class="tab-pane" id="tab_2">
					 <div id="toolbartarget">
					
						</div>
						<table id="tabletarget" 
						data-toolbar="#toolbartarget"
							   data-toggle="table"
							   data-search="true"
							   data-show-refresh="true"
							   data-show-columns="true"
							   data-show-export="true"
							   data-minimum-count-columns="2"
							   data-filter-control="true"
							   data-pagination="true"
							   data-url="inputsarmut/loaddataTabelkegiatan"
							   data-side-pagination="server"
							   data-pagination="true"
							   data-sort-name="id"
							   data-sort-order="desc"
							   data-detail-view="true"
						   data-detail-formatter="operateDetailkegiatan"
							   >
							<thead>	
							<tr>
								<th data-field="nm_tahun_akademik"  data-halign="center" data-align="left"  data-sortable="true" >Tahun Akademik  </th>
								
								<th data-field="uraian_sarmut"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="htmlFormatsarmut" >Uraian Sarmut  </th>
								<th data-field="target_kuantitatif"  data-halign="center" data-align="left"  data-sortable="true" >Target Kuantitatif  </th>
								<th data-field="target_kualitatif"  data-halign="center" data-align="left"  data-sortable="true" >Cara Mengukur  </th>
								<th data-field="kinerja_lalu"  data-halign="center" data-align="left"  data-sortable="true" >Kinerja Lalu  </th>
								
								<th data-field="tgl_mulai"  data-halign="center" data-align="left"  data-sortable="true" >Tanggal Mulai  </th>
								<th data-field="tgl_selesai"  data-halign="center" data-align="left"  data-sortable="true" >Tanggal Selesai  </th>
								<th data-field="uraian_kegiatan"  data-halign="center" data-align="left"  data-sortable="true" >Nama Kegiatan  </th>
								<th data-field="anggaran"  data-halign="center"  data-align="left"  data-sortable="true" >Anggaran  </th>
								<th data-field="score"  data-halign="center" data-align="left"  data-sortable="true"  data-formatter="scoreKet" data-cell-style="buatWaena" >Keterangan  </th> 
								
								<th data-field="presentase"  data-halign="center"  data-align="left"  data-sortable="true" data-cell-style="buatWaenapresentase"  >Presentase  </th>
								<th data-field="score"  data-halign="center" data-align="left"  data-sortable="true" >Score  </th>
							</tr>
							</thead>
						</table>
					 
					 </div>
					 
				</div>
			</div>
              
            </div><!-- /.col -->
			
			
			
			
			
			
          </div>  
       
		
</div> 




<div class="modal fade" id="modal_formck" role="dialog">
  <div class="modal-dialog" style="width:60%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formck" name="formck" class="form-horizontal" onsubmit="return false;"     >
          <input type="hidden" value="" name="id" id="id"/> 
		  <input type="hidden" value="" name="set" id="set"/> 
		   <input type="hidden" value="" name="baris" id="baris"/> 
         
		 <div class="form-group ">	
			<label class="control-label col-md-3" for1="menudes">Tahun Akademik    </label> 
			<div class="col-md-5">
				<select type="select" name="id_tahunakademik" class="form-control select2 input-sm" id="id_tahunakademik" required="required"  style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
				</select>
				
		    </div>
		</div>
		<!--
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Tanggal  </label> 
			<div class="col-md-4">
			<input type="text" class="form-control date-picker input-sm" id="tgl" name="tgl"  placeholder="yyyy-mm-dd" style="width: 100%;" >
		    </div>
		</div>
		-->
		<div class="form-group ">	
			<label class="control-label col-md-3" for1="menudes">MKP </label> 
			<div class="col-md-6"> 
				 <input type="hidden" value="" name="id_mkp" id="id_mkp"/>
				 <input type="text" value="" name="nm_mkp" id="nm_mkp" class="form-control input-sm" readonly />
		    </div>
			<div class="col-md-2">
					<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onclick="cariDatastandar()"  >
					<i class="fa fa-search"></i>
					Cari
					</button>
				
			</div>
			
		</div>
		<div class="form-group ">	
			<label class="control-label col-md-3" for1="menudes">Jenis Sarmut    </label> 
			<div class="col-md-5">
				<select type="select" name="jenis_sarmut" class="form-control select2 input-sm" id="jenis_sarmut" onchange="cekMkp()" required="required"  style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
					 <option  value='1'>Dari MKP</option>
					 <option value='2'>Lainya</option>
				</select>
				
		    </div>
		</div>
		
		<div class="form-group" id="drsm">
			<label class="control-label col-md-3" for1="menudes">Kegiatan MKP  </label> 
			<div class="col-md-6"> 
				 <input type="text" value="" name="id_kegiatan_mkp" id="id_kegiatan_mkp" class="form-control input-sm" readonly />
		    </div>
			<div class="col-md-2">
					<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onclick="cariDatamkp()"  >
					<i class="fa fa-search"></i>
					Cari
					</button>
				
			</div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Uraian Sarmut  </label> 
			<div class="col-md-9">
				<textarea class="form-control" rows="3" id="uraian_sarmutformat" name="uraian_sarmut"></textarea>
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Target Kuantitatif  </label> 
			<div class="col-md-3">
				<input name="target_kuantitatif" class="form-control input-sm nomor" id="target_kuantitatif" required="required" type="text">
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Cara Mengukur  </label> 
			<div class="col-md-9">
				<input name="target_kualitatif" class="form-control input-sm" id="target_kualitatif" required="required" type="text">
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Kinerja Lalu  </label> 
			<div class="col-md-9">
				<input name="kinerja_lalu" class="form-control input-sm" id="kinerja_lalu" required="required" type="text">
		    </div>
		</div>
		
		
		
        
          </div>
          <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
	<div class="modal fade" id="modalTable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:70%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4>Data Kegiatan MKP</h4>
                    </div>
                    <div class="modal-body">
                        <table id="tabledata"
                           data-toggle="table"
                           data-search="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                           data-pagination="true"
						   data-height="500"
                           data-url="<?php echo base_url();?>sarmut/inputsarmut/loaddatamkp"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-query-params="getParammkp"
						   data-sort-order="asc">
                            <thead>
                            <tr>
							<th data-field="uraian_target"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="htmlMutu">Target </th>
							<th data-field="kegiatan"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="htmlFormat">Kegiatan </th> 
								<th data-field="cari"  id="pilih" data-halign="center" data-align="center"
                    data-formatter="operateFormatterPilih" data-events="operateEventspilih">Pilih Data</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->		
	
	
	
	
	
	
	<div class="modal fade" id="modal_formkeg" role="dialog">
   <div class="modal-dialog" style="width:60%">
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formkeg" name="formkeg" class="form-horizontal" onsubmit="return false;"     >
		<input type="hidden" value="" name="id_keg" id="id_keg"/> 
          <input type="hidden" value="" name="id_sarmut" id="id_sarmut"/> 
          <input type="hidden" value="" name="setkeg" id="setkeg"/> 
		 <div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Tanggal  Mulai</label> 
			<div class="col-md-4">
			<input type="text" class="form-control date-picker input-sm" id="tgl_mulai" name="tgl_mulai"  placeholder="yyyy-mm-dd" style="width: 100%;" >
		    </div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Tanggal  Selesai </label> 
			<div class="col-md-4">
			<input type="text" class="form-control date-picker input-sm" id="tgl_selesai" name="tgl_selesai"  placeholder="yyyy-mm-dd" style="width: 100%;" >
		    </div>
		</div>
		
		  <div class="form-body">
			  <div class="form-group field-username">
				  <label class="control-label col-md-3" for1="username">Nama Kegiatan  </label> 
				  <div class="col-md-9">
				   <textarea class="form-control" rows="3" id="uraian_kegiatan" name="uraian_kegiatan"></textarea>
				  </div>
			  </div>
          </div> 
		  <div class="form-group ">
			<label class="control-label col-md-3" for1="menudes">Anggaran  </label> 
			<div class="col-md-9">
				<input name="anggaran" class="form-control input-sm" id="anggaran" required="required" type="text">
		    </div>
		</div>
		 
		
		
		
		  </div>
          <div class="modal-footer">
            <button type="submit" id="btnSavekeg" class="btn btn-primary"   >
			<i class="fa fa-save"></i>
			Simpan</button>
            <button type="button" id="btlkeg" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
	<div class="modal fade" id="modal_formctk" role="dialog">
  <div class="modal-dialog" style="width:40%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formctk" name="formctk" class="form-horizontal" onsubmit="return false;"     >
        
		<div class="form-group ">	
			<label class="control-label col-md-5" for1="menudes">Tahun Akademik    </label> 
			<div class="col-md-7">
				<select type="select" name="id_tahunakademikctk" class="form-control select2 input-sm" id="id_tahunakademikctk"   style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
				</select>
				
		    </div>
		</div>
		<div class="form-group ">	
			<label class="control-label col-md-5" for1="menudes">Unit    </label> 
			<div class="col-md-7">
				<select type="select" name="id_unitcetak" class="form-control select2 input-sm" id="id_unitcetak"   style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
				</select>
				
		    </div>
		</div>
		
        
          </div>
          <div class="modal-footer">
            <button type="button" id="btnctk" class="btn btn-primary" onclick="cetakDatamkp()"  >
			<i class="fa fa-print"></i>
			Cetak</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	
	
	<div class="modal fade" id="modalTablestd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:70%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4>Data MKP</h4>
                    </div>
                    <div class="modal-body">
                        <table id="tabledatastd"
                           data-toggle="table"
                           data-search="true"
                           data-show-export="true"
                           data-minimum-count-columns="2"
                           data-pagination="true"
						   data-height="500"
                           data-url="<?php echo base_url();?>sarmut/inputsarmut/loaddatastandar"
                           data-side-pagination="server"
                           data-pagination="true"
						   data-sort-name="id"
						   data-query-params="getParamstandar"
						   data-sort-order="asc">
                            <thead>
                            <tr>
							<th data-field="romawi"  data-halign="center" data-align="left"  data-sortable="true">No</th>
							<th data-field="nm_mkp"  data-halign="center" data-align="left"  data-sortable="true">Nama Mkp </th>
								<th data-field="cari"  id="pilih" data-halign="center" data-align="center"
                    data-formatter="operateFormatterPilihstd" data-events="operateEventspilihstd">Pilih Data</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->	
		
	<script src="<?php echo base_url();?>js/atribut.js"></script>
  