
	<style>
.hiddenfilez{
	 width: 0px;

 height: 0px;

 overflow: hidden;
}

</style>
<div class="content-wrapper" style="min-height:293px;" >
		<section class="content-header">
		<h1>
		<div class="caption">
			<i class="fa fa-plus-square-o font-blue-chambray"></i>
			<span class="caption-subject font-blue-chambray bold uppercase" id="judulmenu">
			<?php echo callmenudess()?>
			
			</span>
		</div>
		</h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<input type="hidden" value="1" name="buttonedit" id="buttonedit"/>
			
			<div class="nav-tabs-custom">
				
						<div id="toolbar">
					
						 <?php
						echo aksesPrinheder();
						?>
						</div><table id="table" 
						data-toolbar="#toolbar"
							   data-toggle="table"
							   data-search="true"
							   data-show-refresh="true"
							   data-show-columns="true"
							   data-show-export="true"
							   data-minimum-count-columns="2"
							   data-filter-control="true"
							   data-pagination="true"
							   data-url="lapsarmut/loaddataTabel"
							   data-side-pagination="server"
							   data-pagination="true"
							   data-sort-name="id"
							   data-sort-order="desc"
							   data-detail-view="true"
						   data-detail-formatter="operateDetail">
							<thead>	
							<tr>
								<th data-field="state" data-checkbox="true" data-halign="center" data-align="center"></th>
								
								<th data-field="nm_tahun_akademik"  data-halign="center" data-align="left"  data-sortable="true" >Tahun Akademik  </th>
								<th data-field="uraian_sarmut"  data-halign="center" data-align="left"  data-sortable="true" data-formatter="htmlFormatsarmut">Uraian Sarmut  </th>
								<th data-field="target_kuantitatif"  data-halign="center" data-align="left"  data-sortable="true" >Target Kuantitatif  </th>
								<th data-field="target_kualitatif"  data-halign="center" data-align="left"  data-sortable="true" >Cara Mengukur  </th>
								<th data-field="kinerja_lalu"  data-halign="center" data-align="left"  data-sortable="true" >Kinerja Lalu  </th>
								<th data-field="nm_unit"  data-halign="center" data-align="left"  data-sortable="true" >Pelaksana  </th>
							</tr>
							</thead>
						</table>
					
					
					 
					 
			</div>
              
            </div><!-- /.col -->
			
			
			
			
			
			
          </div>  
       
		
</div> 



<div class="modal fade" id="modal_formctk" role="dialog">
  <div class="modal-dialog" style="width:40%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"></h4>
      </div>
      <div class="modal-body form">
        <form  id="formctk" name="formctk" class="form-horizontal" onsubmit="return false;"     >
        
		<div class="form-group ">	
			<label class="control-label col-md-5" for1="menudes">Tahun Akademik    </label> 
			<div class="col-md-7">
				<select type="select" name="id_tahunakademikctk" class="form-control select2 input-sm" id="id_tahunakademikctk"   style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
				</select>
				
		    </div>
		</div>
		<div class="form-group ">	
			<label class="control-label col-md-5" for1="menudes">Unit    </label> 
			<div class="col-md-7">
				<select type="select" name="id_unitcetak" class="form-control select2 input-sm" id="id_unitcetak"   style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
				</select>
				
		    </div>
		</div>
		<div class="form-group ">	
			<label class="control-label col-md-5" for1="menudes">Jenis    </label> 
			<div class="col-md-7">
				<select type="select" name="jenis" class="form-control select2 input-sm" id="jenis"   style="width: 100%;" >
					 <option <?php echo 'selected';?> value=''>--Pilih-- </option>
					 <option  value='1'>Sarmut</option>
					 <option  value='2'>Laporan Sarmut</option>
				</select>
				
		    </div>
		</div>
		
        
          </div>
          <div class="modal-footer">
            <button type="button" id="btnctk" class="btn btn-primary" onclick="cetakDatasarmut()"  >
			<i class="fa fa-print"></i>
			Cetak</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-repeat"></i> 
			Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
	



	<script>
		$(document).ready(function ($) {
		  $("select").select2();
		  $.ajax({
			type: "POST",
			dataType:"JSON",
			url: "<?php echo base_url();?>global_combo/getUnitdepprodifilter",
			success: function(result) {  
			$.each(result, function(key, val) {	
			$("#id_unitcetak").append('<option value="'+val.id+"-"+val.jenis+'">'+val.nm_unit+'</option>');	
			});
							  
			}
			});
			$.ajax({
				type: "POST",
				dataType:"JSON",
				url: "<?php echo base_url();?>global_combo/getTahunakademik",
				success: function(result) {  
				$.each(result, function(key, val) {	
				$("#id_tahunakademikctk").append('<option value="'+val.id+'">'+val.nm_tahun_akademik+'</option>');	
				});
								  
				}
				});
			
	  });
	  
		function operateDetail(index, row,element){
		$.ajax({
				  type: "POST",
				  url: "inputsarmut/loaddatadetailkegiatan?id="+row['id']+"&index="+index,
				  dataType:"JSON",
				  success: function(result) { 

				  	var x1 = "no_baris_"+index;
				  	var el = "tr[data-index='"+index+"']"
					$(el).next('tr.detail-view').addClass(x1);

					var id,id_sarmut,uraian_kegiatan,tgl_mulai,tgl_selesai,angaran,score,presentase,anggaran;
					
					
					var tbl_det;
					tbl_det = '<td colspan="10" align="center"><table border="1" width="100%" style="border-style: solid;border-color:#9F9492;">';
					tbl_det += '<tr style="border-color:#9F9492;">';
					tbl_det += '<td align="center" width="10%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;"> Tanggal Mulai</td>';
					tbl_det += '<td align="center" width="10%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Tanggal Selesai </td>';
					tbl_det += '<td align="center" width="50%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Nama Kegiatan</td>'
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Anggaran</td>'
					tbl_det += '<td align="center" width="15%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Ket Proses</td>'
					tbl_det += '<td align="center" width="10%" style="border-color:#9F9492;font-weight: bold;background-color: #DFD3D3;">Presentase</td>'
					tbl_det += '</tr>';
        			tbl_det += '<tr style="border-color:#9F9492;">';

        			$.each(eval(result), function (key,value) {
						
        				id = value['id'];
						id_sarmut = value['id_sarmut'];
        				uraian_kegiatan = value['uraian_kegiatan'];
        				tgl_mulai = value['tgl_mulai'];
        				tgl_selesai= value['tgl_selesai'];
						score=value['score'];
						presentase=value['presentase'];
						anggaran=value['anggaran'];
					//	alert(idkeg);
						tbl_det += '<td style="border-color:#9F9492;" >&nbsp;&nbsp; '+tgl_mulai+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+tgl_selesai+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+uraian_kegiatan+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+anggaran+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+score+'</td>';
						tbl_det += '<td style="border-color:#9F9492;">&nbsp;&nbsp; '+presentase+'</td>';
        				tbl_det += '</tr>';

        			});
        			tbl_det += '</table></td>';

					$(".detail-view."+x1).html(tbl_det);

				  }


		});

    }
	
	function printHeder(){ 
	$('#formctk')[0].reset(); 
	 $('#formctk').bootstrapValidator('resetForm', true); 
      $('#modal_formctk').modal('show'); 
      $('.modal-title').text('Form Cetak Rencana Sarmut'); 
	  
		//window.open('<?php echo base_url();?>master/mkp/cetakMkp?id=1','_blank');
		//window.open('<?php echo base_url();?>master/mkp/cetakMkpword?id=1');
	}
	
	function cetakDatasarmut(){
		var id_tahunakademikctk=$('#id_tahunakademikctk').val();
		var id_unitcetak=$('#id_unitcetak').val();
		var jenis=$('#jenis').val();
		if(jenis=="1"){
			window.open('<?php echo base_url();?>sarmut/lapsarmut/cetakSarmut?id_tahunakademi='+id_tahunakademikctk+'&id_unit='+id_unitcetak);
		}else if(jenis=="2"){
			window.open('<?php echo base_url();?>sarmut/lapsarmut/cetakLapsarmut?id_tahunakademi='+id_tahunakademikctk+'&id_unit='+id_unitcetak);
		}else{
			bootbox.alert("Pilih Jenis Laporan!"); 
		}
		
	}
function htmlFormatsarmut(value, row, index){
	var mutu=row.uraian_sarmut;
	var res = mutu.substr(0, 200);
	return strip_html_tags(res);
}

function strip_html_tags(str)
{
   if ((str===null) || (str===''))
       return false;
  else
   str = str.toString();
  return str.replace(/<[^>]*>/g, '');
}

	</script>
	
	
	
	
	<script src="<?php echo base_url();?>js/atribut.js"></script>
  