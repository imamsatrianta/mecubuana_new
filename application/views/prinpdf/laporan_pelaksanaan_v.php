<?php
    ob_start();
?>
<page>
    <table border="0">
        <tr>
            <td>nama</td>
            <td>deskripsi</td>
        </tr>
<?php 
//    echo "<tr><td>".$data['tglmulai']."</td><td>".$data['tglselesai']."</td></tr>";
?>
    </table>
</page>
<?php
    $content = ob_get_clean();

    // convert to PDF
    //$dirpdf = str_replace('audit', 'prinpdf', dirname(__FILE__));
    require_once(dirname(__FILE__).'/html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr');
        $html2pdf->pdf->IncludeJS("print(true);");
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('print.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
?>