<?php
Class Persiapanformulir_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_persiapan_formulir as a');
			$this->db->join('sp_standar AS b', 'b.id = a.id_standar');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*,b.nm_dokumen');
			$this->db->from('sp_persiapan_formulir as a');
			$this->db->join('sp_standar AS b', 'b.id = a.id_standar');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	function simpanData($data){
	//	print_r($data);exit;
		$status=$this->db->insert('sp_persiapan_formulir', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('sp_persiapan_formulir', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('sp_persiapan_formulir'); 
	if(!$status) return false;
		else return true;
	}	
	
	function simpanDatadetail($datadetdua){
		$status=$this->db->insert('sp_persiapan_formulir_doc', $datadetdua);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function hapusDetail($iddet){
		$this->db->where('id_persiapan_formulir', $iddet);
	$status=$this->db->delete('sp_persiapan_formulir_doc'); 
	if(!$status) return false;
		else return true;
	}
	
	function getDetail($id){
		$result = array();
		$this->db->select('a.*');
		$this->db->from('sp_persiapan_formulir_doc as a');
		$this->db->where('id_persiapan_formulir',$id); 
		$hasil = $this->db->get();
		
		$rs = $hasil->result(); 
		 $items = array();
		 foreach($rs as $row){
			 array_push($items, $row);
		}
		return json_encode($items);
	}
	
	function export_excel(){
		$sql='SELECT u.nm_unit, k.nm_karyawan, a.periode, a.tglmulai, a.tglselesai FROM sp_planing_audit as a 
				JOIN ms_unit as u ON a.id_unit = u.id
				JOIN ms_karyawan as k ON k.id = a.id_ketuatim';
		return $this->db->query($sql);
	}	
	
	
		
}
?>