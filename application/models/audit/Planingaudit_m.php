<?php
Class Planingaudit_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
        if (isset($_GET['sort'])) {
        $sort = $this->input->get("sort");
        }else{
                $sort = 'a.id';
        }
        $this->db->select('a.id');
        $this->db->from('sp_planing_audit AS a');
        $this->db->join('ms_unit AS b', 'b.id = a.id_unit');
        $this->db->join('ms_karyawan AS c', 'c.id = a.id_ketuatim');
        $hasil = $this->db->get();
        $total=$hasil->num_rows();


        $this->db->select('a.*,b.nm_unit,c.nm_karyawan');
        $this->db->from('sp_planing_audit AS a');
        $this->db->join('ms_unit AS b', 'b.id = a.id_unit');
        $this->db->join('ms_karyawan AS c', 'c.id = a.id_ketuatim');
        $this->db->where($where);
        $this->db->order_by($sort, $order);
         $this->db->limit($limit, $offset);

        $hasil = $this->db->get();
        //	echo $this->db->last_query(); exit;
        $rs = $hasil->result(); 

        $result["total"] = $total;
                     $items = array();
                    foreach($rs as $row){
                             array_push($items, $row);
                    }

        $result["rows"] = $items;
        echo json_encode($result);
        
    }
	function simpanData($data){
	//	print_r($data);exit;
		$status=$this->db->insert('sp_planing_audit', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('sp_planing_audit', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('sp_planing_audit'); 
	if(!$status) return false;
		else return true;
	}		
    function hapusstandar($iddet){
	$this->db->where('id_planingaudit', $iddet);
	$status=$this->db->delete('sp_planing_audit_standar'); 
	if(!$status) return false;
		else return true;
	}
	function hapusauditie($iddet){
		$this->db->where('id_planingaudit', $iddet);
		$status=$this->db->delete('sp_planing_audit_auditie'); 
		if(!$status) return false;
			else return true;
	}
	function hapustujuan($iddet){
		$this->db->where('id_planingaudit', $iddet);
		$status=$this->db->delete('sp_planing_audit_tujuan'); 
		if(!$status) return false;
			else return true;
	}
	function hapusjadwal($iddet){
		$this->db->where('id_planingaudit', $iddet);
		$status=$this->db->delete('sp_planing_audit_jadwal'); 
		if(!$status) return false;
			else return true;
	}
	function hapusanggota($iddet){
		$this->db->where('id_planingaudit', $iddet);
		$status=$this->db->delete('sp_planing_audit_anggota'); 
		if(!$status) return false;
			else return true;
		}
	
	//simpan data 
	function simpanDataanggota($datadetdua){
		$status=$this->db->insert('sp_planing_audit_anggota', $datadetdua);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function simpanDataauditie($datadetempat){
	$status=$this->db->insert('sp_planing_audit_auditie', $datadetempat);
	//echo $this->db->last_query(); exit;
		if(!$status) return false;
		else return true;
	}
	function simpanDatastandar($datadettiga){
		$status=$this->db->insert('sp_planing_audit_standar', $datadettiga);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function simpanDatatujuan($datadettujuh){
		$status=$this->db->insert('sp_planing_audit_tujuan', $datadettujuh);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function simpanDatajadwal($datadetenam){
		$status=$this->db->insert('sp_planing_audit_jadwal', $datadetenam);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
    
	function getauditie($id){
		$result = array();
		$this->db->select('a.*');
		$this->db->from('sp_planing_audit_auditie as a');
                $this->db->join('sp_planing_audit AS b', 'b.id = a.id_planingaudit');
		$this->db->where('id_planingaudit',$id); 
		$hasil = $this->db->get();
		
		$rs = $hasil->result(); 
		 $items = array();
		 foreach($rs as $row){
			 array_push($items, $row);
		}
		return json_encode($items);
	}
	function getstandar($id){
		$result = array();
		$this->db->select('a.*');
		$this->db->from('sp_planing_audit_standar as a');
                $this->db->join('sp_planing_audit AS b', 'b.id = a.id_planingaudit');
		$this->db->where('id_planingaudit',$id); 
		$hasil = $this->db->get();
		
		$rs = $hasil->result(); 
		 $items = array();
		 foreach($rs as $row){
			 array_push($items, $row);
		}
		return json_encode($items);
	}
	function getanggota($id){
		$result = array();
		$this->db->select('a.*');
		$this->db->from('sp_planing_audit_anggota as a');
        $this->db->join('sp_planing_audit AS b', 'b.id = a.id_planingaudit');
		$this->db->where('id_planingaudit',$id); 
		$hasil = $this->db->get();
		
		$rs = $hasil->result(); 
		 $items = array();
		 foreach($rs as $row){
			 array_push($items, $row);
		}
		return json_encode($items);
	}
	function getjadwal($id){
		$result = array();
		$this->db->select('a.*');
		$this->db->from('sp_planing_audit_jadwal as a');
        $this->db->join('sp_planing_audit AS b', 'b.id = a.id_planingaudit');
		$this->db->where('id_planingaudit',$id); 
		$hasil = $this->db->get();
		
		$rs = $hasil->result(); 
		 $items = array();
		 foreach($rs as $row){
			 array_push($items, $row);
		}
		return json_encode($items);
	}	
	function gettujuan($id){
		$result = array();
		$this->db->select('a.*');
		$this->db->from('sp_planing_audit_tujuan as a');
        $this->db->join('sp_planing_audit AS b', 'b.id = a.id_planingaudit');
		$this->db->where('id_planingaudit',$id); 
		$hasil = $this->db->get();
		
		$rs = $hasil->result(); 
		 $items = array();
		 foreach($rs as $row){
			 array_push($items, $row);
		}
		return json_encode($items);
	}	
	
	function export_excel(){
		$sql='SELECT u.nm_unit, k.nm_karyawan, a.periode, a.tglmulai, a.tglselesai FROM sp_planing_audit as a 
				JOIN ms_unit as u ON a.id_unit = u.id
				JOIN ms_karyawan as k ON k.id = a.id_ketuatim';
		return $this->db->query($sql);
	}	
}
?>