<?php
Class Surattugas_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
        if (isset($_GET['sort'])) {
        $sort = $this->input->get("sort");
        }else{
                $sort = 'a.id';
        }
        $this->db->select('a.id');
        $this->db->from('sp_planing_audit AS a');
        $this->db->join('ms_unit AS b', 'b.id = a.id_unit');
        $this->db->join('ms_karyawan AS c', 'c.id = a.id_ketuatim');
        $hasil = $this->db->get();
        $total=$hasil->num_rows();


        $this->db->select('a.*,b.nm_unit,c.nm_karyawan');
        $this->db->from('sp_planing_audit AS a');
        $this->db->join('ms_unit AS b', 'b.id = a.id_unit');
        $this->db->join('ms_karyawan AS c', 'c.id = a.id_ketuatim');
        $this->db->where($where);
        $this->db->order_by($sort, $order);
         $this->db->limit($limit, $offset);

        $hasil = $this->db->get();
        //	echo $this->db->last_query(); exit;
        $rs = $hasil->result(); 

        $result["total"] = $total;
                     $items = array();
                    foreach($rs as $row){
                             array_push($items, $row);
                    }

        $result["rows"] = $items;
        echo json_encode($result);
        
    }
	function export_pdf($id){
            $this->db->select('b.nm_karyawan');
            $this->db->from('sp_planing_audit_anggota AS a');
            $this->db->join('ms_karyawan AS b', 'b.id = a.id_anggota');
            $this->db->where('a.id_planingaudit', $id);
            $this->db->order_by('a.id');

            $hasil = $this->db->get();
            return $hasil;
        }
        function export_pdf2($id){
            $this->db->select('b.nm_karyawan');
            $this->db->from('sp_planing_audit AS a');
            $this->db->join('ms_karyawan AS b', 'b.id = a.id_ketuatim');
            $this->db->where('a.id', $id);
            $this->db->order_by('a.id');

            $hasil = $this->db->get();
            return $hasil;
        }
        function export_pdf3($id){
            $this->db->select('b.nm_unit,a.periode');
            $this->db->from('sp_planing_audit as a');
            $this->db->join('ms_unit AS b', 'b.id = a.id_unit');
            $this->db->where('a.id', $id);
            $hasil = $this->db->get();
            return $hasil;
        }
}
?>