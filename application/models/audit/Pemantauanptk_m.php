<?php
Class Pemantauanptk_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_pemantauan AS a');
			$this->db->join('ms_unit AS b', 'b.id = a.id_unit');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			$this->db->select('a.*,b.nm_unit,c.nm_karyawan');
			$this->db->from('sp_pemantauan AS a');
			$this->db->join('ms_unit AS b', 'b.id = a.id_unit');
			$this->db->join('ms_karyawan c', 'c.id = a.id_ketuatim');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	function simpanData($data){
	//	print_r($data);exit;
		$status=$this->db->insert('sp_pemantauan', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('sp_pemantauan', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('sp_pemantauan'); 
	if(!$status) return false;
		else return true;
	}	
}
?>