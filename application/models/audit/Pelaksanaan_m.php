<?php
Class Pelaksanaan_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_persiapan_pelaksanaan AS a');
			$this->db->join('sp_planing_audit AS b', 'b.id = a.id_planingaudit');
			$this->db->join('ms_unit AS c', 'c.id = b.id_unit');
			$this->db->join('ms_karyawan AS d', 'd.id = b.id_ketuatim');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('b.*,c.nm_unit,d.nm_karyawan');
			$this->db->from('sp_persiapan_pelaksanaan AS a');
			$this->db->join('sp_planing_audit AS b', 'b.id = a.id_planingaudit');
			$this->db->join('ms_unit AS c', 'c.id = b.id_unit');
			$this->db->join('ms_karyawan AS d', 'd.id = b.id_ketuatim');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
    function loaddataTabelFormulir($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_formulir_audit AS a');
			$this->db->join('ms_unit AS b', 'b.id = a.id_unit');
			$this->db->join('sp_standar AS c', 'c.id = a.id_standar');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*,b.nm_unit,c.nm_dokumen');
			$this->db->from('sp_formulir_audit AS a');
			$this->db->join('ms_unit AS b', 'b.id = a.id_unit');
			$this->db->join('sp_standar AS c', 'c.id = a.id_standar');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
	}
	function loaddatajadwal($offset,$limit,$order,$where){
		if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_planing_audit as a');
			$this->db->join('ms_unit as b ', 'b.id = a.id_unit');
			$this->db->join('ms_karyawan as c ', 'c.id = a.id_ketuatim');
			$this->db->where($where);
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			//yg bawah restnya
			$this->db->select('a.*,b.nm_unit,c.nm_karyawan');
			$this->db->from('sp_planing_audit as a');
			$this->db->join('ms_unit as b ', 'b.id = a.id_unit');
			$this->db->join('ms_karyawan as c ', 'c.id = a.id_ketuatim');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			$this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
	}
	function getUnit($periode){
		
		$sqldata = "SELECT b.nm_unit as nm_unit FROM sp_planing_audit a join ms_unit b on a.id_unit = b.id WHERE  a.id ='$periode' ";
		$query = $this->db->query($sqldata);
		$rowa = $query->row(); 
		$nm_unit=$rowa->nm_unit;
		return $nm_unit;
	}

	function getKetuaUnit($periode){
		
		$sqldata = "SELECT c.nm_karyawan as nm_ketua_unit FROM sp_planing_audit a
		join ms_unit b on a.id_unit = b.id 
		join ms_karyawan c on b.id_direktorat = c.id
		WHERE  a.id ='$periode' ";
		$query = $this->db->query($sqldata);
		$rowa = $query->row(); 
		$nm_ketua_unit=$rowa->nm_ketua_unit;
		return $nm_ketua_unit;
	}

	function getKetuatimauditor($periode){
		
		$sqldata = "SELECT c.nm_karyawan as nm_ketua_auditor FROM sp_persiapan_pelaksanaan a join sp_planing_audit b on a.id_planingaudit = b.id join ms_karyawan c on b.id_ketuatim=c.id WHERE  a.id_planingaudit ='$periode' ";
		$query = $this->db->query($sqldata);
		$rowa = $query->row(); 
		$nm_ketua_auditor=$rowa->nm_ketua_auditor;
		return $nm_ketua_auditor;
	}
	function getTglmulai($periode){
		
		$sqldata = "SELECT DATE_FORMAT(tglmulai,'%d %M %Y') as tglmulai From sp_planing_audit ";
		$query = $this->db->query($sqldata);
		$rowa = $query->row(); 
		$tglmulai=$rowa->tglmulai;
		return $tglmulai;
	}
	function getTglselesai($periode){
		
		$sqldata = "SELECT DATE_FORMAT(tglselesai,'%d %M %Y') as tglselesai From sp_planing_audit ";
		$query = $this->db->query($sqldata);
		$rowa = $query->row(); 
		$tglselesai=$rowa->tglselesai;
		return $tglselesai;
	}
	function getStandardok($periode){
		
		$sqldata = "SELECT a.nm_dokumen as standardok from sp_planing_audit_standar b join sp_standar a on a.id = b.id_standar WHERE  b.id_planingaudit ='$periode' ";
		$query = $this->db->query($sqldata);
		return $query->result();
	}

	function getMateriaudit($periode){
		
		$sqldata = "SELECT b.id as id_materi , b.materi from sp_planing_audit_standar a join sp_persiapan_formulir b on a.id_standar = b.id_standar join sp_persiapan_formulir_doc c on b.id = c.id_persiapan_formulir WHERE  a.id_planingaudit ='$periode' group by b.id ";
		$query = $this->db->query($sqldata);
		return $query->result();
	}
	function getDocumentaudit($periode,$id_materi){
		
			
		$sqldata = "SELECT c.document as document from sp_planing_audit_standar a join sp_persiapan_formulir b on a.id_standar = b.id_standar join sp_persiapan_formulir_doc c on b.id = c.id_persiapan_formulir WHERE  a.id_planingaudit ='$periode' AND b.id='$id_materi' ";
		$query = $this->db->query($sqldata);
		return $query->result();
	}

	function getAuditiedata($periode){
		
		$sqldata = "SELECT b.nm_karyawan as auditiedata from sp_planing_audit_auditie a join ms_karyawan b on a.id_auditie = b.id WHERE  a.id_planingaudit ='$periode' ";
		$query = $this->db->query($sqldata);
		$rowa = $query->row(); 
		$auditiedata=$rowa->auditiedata;
		return $auditiedata;
	}
	function getPlaningpersiapananggota($periode){
		
		$sqldata = "SELECT c.nm_karyawan as nm_anggota FROM sp_persiapan_pelaksanaan a join sp_planing_audit_anggota b on a.id_planingaudit = b.id_planingaudit join ms_karyawan c on b.id_anggota = c.id WHERE  a.id_planingaudit ='$periode' ";
		$query = $this->db->query($sqldata);
		return $query->result();

	}
	function getPlaningpersiapan($id_planingaudit){
		$sqldata = "SELECT a.periode as nm_tahun_akademik FROM sp_persiapan_pelaksanaan a WHERE  id_planingaudit ='$id_planingaudit' ";
		$query = $this->db->query($sqldata);
		$rowa = $query->row(); 
		$nm_tahun_akademik=$rowa->nm_tahun_akademik;
		return $nm_tahun_akademik;
	}
	function getTujuanaudit($id_planingaudit){
		$sqldata = "SELECT tujuan FROM sp_planing_audit_tujuan WHERE  id_planingaudit ='$id_planingaudit' ";
		$query = $this->db->query($sqldata);
		return $query->result();
	}
	function getLapstandar($periode,$id_unit,$id_ketuatim){
		$data = array();
			
		$this->db->select('a.id');
		$this->db->from('sp_planing_audit as a');
		$this->db->where('a.periode',$periode);
		$this->db->where('a.id_unit',$id_unit);
		$this->db->where('a.id_ketuatim',$id_ketuatim);
		// $this->db->group_by("a.id_group");
		$this->db->order_by("a.id", "asc");
		$hasil = $this->db->get();
		$rs = $hasil->result(); 
			$items = array();
			$oData = new stdClass;
		foreach($rs as $row){
			$id_group=$row->id_group;
			$standar=$this->dataStandar($id_group);
			//print_r();
			$nm_dokumen=array();
			$id_standar=array();
			foreach($standar as $stan){
				$id_standar[]=$stan->id_standar;
				$nm_dokumen[]=$stan->nm_dokumen;
			}
			$id_stan=implode(',',$id_standar);
			$nmstan=implode(',',$nm_dokumen);
			
			$oData->id_standar  = $id_stan;
			$oData->nm_dokumen  = $nmstan;
			
			$obj_merged = (object) array_merge((array) $oData, (array) $row);
				array_push($items, $obj_merged);
		}
		//print_r($items);
		return $items;
		//echo $this->db->last_query();exit;
		/*if($hasil->num_rows() > 0){
		$data = $hasil->result();
		}
		
		$hasil->free_result();
					
		return $data;*/
}
	
	function simpanData($data){
	//	print_r($data);exit;
		$status=$this->db->insert('sp_formulir_audit', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
        function simpanDatadetail($datadetdua){
		$status=$this->db->insert('sp_formulir_audit_doc', $datadetdua);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
        
        
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('sp_formulir_audit', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('sp_formulir_audit'); 
	if(!$status) return false;
		else return true;
	}	
    function hapusDetail($iddet){
	$this->db->where('id_formulir_audit', $iddet);
	$status=$this->db->delete('sp_formulir_audit_doc'); 
	if(!$status) return false;
		else return true;
	}
	function export_pdf($id){
            $this->db->select('a.*,b.nm_unit,c.nm_karyawan');
            $this->db->from('sp_planing_audit AS a');
            $this->db->join('ms_unit AS b', 'b.id = a.id_unit');
            $this->db->join('ms_karyawan AS c', 'c.id = a.id_ketuatim');
            $this->db->where('a.id', $id);
            $this->db->order_by('a.id');

            $hasil = $this->db->get();
            return $hasil;
        }
}
?>