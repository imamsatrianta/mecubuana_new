<?php
Class Approvalpelaksanaan_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_persiapan_pelaksanaan AS a');
			$this->db->join('sp_planing_audit AS b', 'b.id = a.id_planingaudit');
			$this->db->join('ms_unit AS c', 'c.id = b.id_unit');
			$this->db->join('ms_karyawan AS d', 'd.id = b.id_ketuatim');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('b.*,c.nm_unit,d.nm_karyawan');
			$this->db->from('sp_persiapan_pelaksanaan AS a');
			$this->db->join('sp_planing_audit AS b', 'b.id = a.id_planingaudit');
			$this->db->join('ms_unit AS c', 'c.id = b.id_unit');
			$this->db->join('ms_karyawan AS d', 'd.id = b.id_ketuatim');
			
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	function getanggotaperencanaan($id){
		$result = array();
		$this->db->select('a.*,c.nm_karyawan');
		$this->db->from('sp_planing_audit_anggota as a ');
		$this->db->join('sp_persiapan_pelaksanaan as b', 'a.id_planingaudit = b.id_planingaudit');
		$this->db->join('ms_karyawan as c', 'a.id_anggota = c.id');
		$this->db->where('b.id_planingaudit',$id); 
		$hasil = $this->db->get();
		$rs = $hasil->result(); 
		$items = array();
		foreach($rs as $row){
			array_push($items, $row);
		}
		return json_encode($items);
	}
	function getstandarperencanaan($id){
		$result = array();
		$this->db->select('b.id_standar,c.nm_dokumen,d.materi,e.document');
		$this->db->from('sp_persiapan_pelaksanaan as a');
		$this->db->join('sp_persiapan_pelaksanaan_standar as b ', 'b.id_persiapan_pelaksanaan = a.id');
		$this->db->join('sp_standar as c ', 'c.id = b.id_standar');
		$this->db->join('sp_persiapan_formulir as d ', 'd.id_standar = c.id');
		$this->db->join('sp_persiapan_formulir_doc as e ', 'e.id_persiapan_formulir = d.id');
		$this->db->where('a.id_planingaudit',$id); 
		$hasil = $this->db->get();
		$rs = $hasil->result(); 
		$items = array();
		foreach($rs as $row){
			array_push($items, $row);
		}
		return json_encode($items);
	}

	function loaddatajadwal($offset,$limit,$order,$where){
		if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_planing_audit as a');
			$this->db->join('ms_unit as b ', 'b.id = a.id_unit');
			$this->db->join('ms_karyawan as c ', 'c.id = a.id_ketuatim');
			$this->db->where($where);
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			//yg bawah restnya
			$this->db->select('a.*,b.nm_unit,c.nm_karyawan');
			$this->db->from('sp_planing_audit as a');
			$this->db->join('ms_unit as b ', 'b.id = a.id_unit');
			$this->db->join('ms_karyawan as c ', 'c.id = a.id_ketuatim');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			$this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
	}
	function loaddatastandar($offset,$limit,$order,$where,$idstandar){
		if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_standar as a');
			$this->db->join('sp_persiapan_formulir as b ', 'b.id_standar = a.id');
			$this->db->join('sp_persiapan_formulir_doc as c ', 'c.id_persiapan_formulir = b.id');
			$this->db->where($where);
			$hasil = $this->db->get();
			// print_r($hasil);exit;
			$total=$hasil->num_rows();
			//yg bawah restnya
			$this->db->select('c.document,b.materi,a.nm_dokumen,a.id');
			$this->db->from('sp_standar as a');
			$this->db->join('sp_persiapan_formulir as b ', 'b.id_standar = a.id');
			$this->db->join('sp_persiapan_formulir_doc as c ', 'c.id_persiapan_formulir = b.id');
			$this->db->order_by('b.id');
			$this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
	}
	function loaddatajadwal2($offset,$limit,$order,$where){
		if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_laporan_audit_kts_ob AS a');
			$this->db->join('sp_standar as b ', 'b.id = a.id_standar');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			$this->db->limit($limit, $offset);
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			//yg bawah restnya
			$this->db->select('b.nm_dokumen ,a.*');
			$this->db->from('sp_laporan_audit_kts_ob AS a');
			$this->db->join('sp_standar as b ', 'b.id = a.id_standar');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			$this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
	}
	function simpanData($data){
	//	print_r($data);exit;
		$status=$this->db->insert('sp_persiapan_pelaksanaan', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('sp_persiapan_pelaksanaan', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('sp_persiapan_pelaksanaan'); 
	if(!$status) return false;
		else return true;
	}	
	function hapusstandar($id){
		$this->db->where('id_persiapan_pelaksanaan', $id);
	$status=$this->db->delete('sp_persiapan_pelaksanaan_standar'); 
	if(!$status) return false;
		else return true;
	}
	function simpanDatastandar($datadetdua){
		$status=$this->db->insert('sp_persiapan_pelaksanaan_standar', $datadetdua);
		// echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
}
?>