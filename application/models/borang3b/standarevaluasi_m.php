<?php
Class standarevaluasi_m extends CI_Model{

    function loaddataTabel($offset,$limit,$order,$where,$parameter,$namatabel,$standar){
			$id_prodi=$this->session->userdata('id_prodi');
			$levelprodi=$this->session->userdata('levelprodi');
			$level=$_GET['level'];
		//	$id_level=$this->session->userdata('id_level');
			
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from(" $namatabel as a");
			$this->db->join('ms_jenjangprodi as b ', 'b.id = a.id_jenjangprodi');
			$this->db->join('tahun_akademik as c ', 'c.id = a.id_tahunakademik');
			$this->db->join('ms_prodi as d ', 'd.id = a.id_prodi');
			
			
			$this->db->where('a.id_jenjangprodi',$level);
			
			if ($parameter<>0) {
				$this->db->where('id_parameter',$parameter);
			}
			$this->db->where($where);
			if($levelprodi=="2"){
				$this->db->where('a.id_prodi',$id_prodi);
			}else if($levelprodi=="1"){
				$this->db->where('d.parent',$id_prodi);
			}
			$this->db->group_by('a.id_tahunakademik');
			$this->db->group_by('a.id_jenjangprodi');
			$this->db->group_by('a.id_prodi');
			
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select("a.*,b.nm_jenjangprodi,c.nm_tahun_akademik,d.nm_prodi,e.note_evaluasi,e.tgl_dodate as dodate,e.status_evaluasi",false);
			$this->db->from(" $namatabel as a");
			$this->db->join('ms_jenjangprodi as b ', 'b.id = a.id_jenjangprodi');
			$this->db->join('tahun_akademik as c ', 'c.id = a.id_tahunakademik');
			$this->db->join('ms_prodi as d ', 'd.id = a.id_prodi');
			$this->db->join("br_3bevaluasi as e ", "e.id_tahunakademik = a.id_tahunakademik and e.id_jenjangprodi = a.id_jenjangprodi and e.id_prodi = a.id_prodi and e.status_data=1 and e.standar='$standar'","left");
			$this->db->where('a.id_jenjangprodi',$level);
			if ($parameter<>0) {
				$this->db->where('id_parameter',$parameter);
			}
			$this->db->where($where);
			
			//echo $levelprodi;
			if($levelprodi=="2"){
				$this->db->where('a.id_prodi',$id_prodi);
				
			}else if($levelprodi=="1"){
				$this->db->where('d.parent',$id_prodi);
			//	$this->db->where('e.status_evaluasi !=','0');
			}
			$this->db->order_by($sort, $order);
			
			$this->db->group_by('a.id_tahunakademik');
			$this->db->group_by('a.id_jenjangprodi');
			$this->db->group_by('a.id_prodi');
			
			
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
				//echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	
	function editData($id_tahunakademikevl,$id_jenjangprodievl,$id_prodievl,$data,$standar){
		$this->db->where('id_tahunakademik',$id_tahunakademikevl);
		$this->db->where('id_jenjangprodi',$id_jenjangprodievl);
		$this->db->where('id_prodi',$id_prodievl);
		$this->db->where('standar',$standar);
		$status=$this->db->update('br_3bevaluasi', $data);
		//echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}
	
	function simpanDatarevaluasi($dataevaluasi){
		$status=$this->db->insert('br_3bevaluasi', $dataevaluasi);
		//echo $this->db->last_query();exit;
			if(!$status) return false;
			else return true;
	}
	
	
		// borang
	function getBorang($id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel,$standar){
		$data = array();
			
			$this->db->select('a.*');
			$this->db->from('br_parameterborang3b as a');
			//$this->db->join("$tabel as b ", "a.id = b.id_parameter","left");
			$this->db->where('a.id_jenjangprodi',$id_jenjangprodi);
			$this->db->where('a.standar',$standar);
			//$this->db->where('b.id_tahunakademik',$id_tahunakademik);
			//$this->db->where('b.id_prodi',$id_prodi);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
		
	}
	
	
		function getIsiborang($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel){
		$uraian = "";
			
			$this->db->select("a.*");
			$this->db->from("$tabel as a");
			$this->db->where('a.id_parameter',$id);
			$this->db->where('a.id_jenjangprodi',$id_jenjangprodi);
			$this->db->where('a.id_tahunakademik',$id_tahunakademik);
			$this->db->where('a.id_prodi',$id_prodi);
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$rowa = $hasil->row(); 
			$uraian=$rowa->uraian;
			}
			
                       
			return $uraian;
	}
	
	function getTigasatusatu($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel){
		$data = array();
			
			$this->db->select('b.*');
			$this->db->from('br_3bd3 as a');
			$this->db->join("$tabel b ", "a.id = b.id_borangd3");
			$this->db->where('a.id_jenjangprodi',$id_jenjangprodi);
			$this->db->where('a.id_tahunakademik',$id_tahunakademik);
			$this->db->where('a.id_prodi',$id_prodi);
			$this->db->order_by("b.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	function getTigasarjana($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel){
		$data = array();
			
			$this->db->select('b.*');
			$this->db->from('br_3bs1 as a');
			$this->db->join("$tabel b ", "a.id = b.id_borang");
			$this->db->where('a.id_jenjangprodi',$id_jenjangprodi);
			$this->db->where('a.id_tahunakademik',$id_tahunakademik);
			$this->db->where('a.id_prodi',$id_prodi);
			$this->db->order_by("b.id", "asc");
			$hasil = $this->db->get();
			//echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	
	function getTigamagister($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel){
		$data = array();
			
			$this->db->select('b.*');
			$this->db->from('br_3bs2 as a');
			$this->db->join("$tabel b ", "a.id = b.id_borang");
			$this->db->where('a.id_jenjangprodi',$id_jenjangprodi);
			$this->db->where('a.id_tahunakademik',$id_tahunakademik);
			$this->db->where('a.id_prodi',$id_prodi);
			$this->db->order_by("b.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	function getTigadoktor($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi,$tabel){
		$data = array();
			
			$this->db->select('b.*');
			$this->db->from('br_3bs3 as a');
			$this->db->join("$tabel b ", "a.id = b.id_borang");
			$this->db->where('a.id_jenjangprodi',$id_jenjangprodi);
			$this->db->where('a.id_tahunakademik',$id_tahunakademik);
			$this->db->where('a.id_prodi',$id_prodi);
			$this->db->order_by("b.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	
	function getIdentitas($id_jenjangprodi,$id_prodi){
		$data = array();
			
			$this->db->select('a.*,b.nm_jenjangprodi,c.nm_prodi,d.nm_prodi as fskultas');
			$this->db->from('br_3bidentitas_borang as a');
			$this->db->join('ms_jenjangprodi as b ', 'a.id_jenjang = b.id','left');
			$this->db->join('ms_prodi as c ', 'a.id_prodi = c.id','left');
			$this->db->join('ms_prodi as d ', 'd.id = c.parent','left');
			$this->db->where('a.id_jenjang',$id_jenjangprodi);
			$this->db->where('a.id_prodi',$id_prodi);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->row();
			}
			
			//$hasil->free_result();
                       
			return $data;
		
	}
	function getIdentitasprodi($id_jenjangprodi,$id_prodi){
		$data = array();
			
			$this->db->select('c.nm_prodi,d.nm_jenjangprodi');
			$this->db->from('br_3bidentitas_borang as a');
			$this->db->join('br_3bidentitas_borang_prodi as b ', 'a.id = b.id_identitas','left');
			$this->db->join('ms_prodi as c ', 'b.id_prodi = c.id','left');
			$this->db->join('ms_jenjangprodi as d ', 'c.id_jenjangprodi = d.id','left');
			
			$this->db->where('a.id_jenjang',$id_jenjangprodi);
			$this->db->where('a.id_prodi',$id_prodi);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}	
	
	
	function getPengisiborang($id_jenjangprodi,$id_prodi,$id_tahunakademik){
		$data = array();
			
			$this->db->select("c.nm_karyawan,c.nidn,b.tgl_penisian,CASE c.jenis_jabatan WHEN '1'
THEN  d.nm_rektorat
WHEN '2'
THEN e.nm_direktorat
WHEN '3'
THEN f.nm_unit
WHEN '4'
THEN 'Staf'
WHEN '5'
THEN 'Kepala Prodi'
END
AS nm_jabatan",false);
			$this->db->from('br_pengisi as a'); 
			$this->db->join('br_pengisi_det as b ', 'a.id = b.id_pengisi','left');
			$this->db->join('ms_karyawan as c ', 'b.id_karyawan = c.id','left');
			
			$this->db->join('ms_rektorat as d ', 'd.id = c.id_jabatan','left');
			$this->db->join('ms_direktorat as e ', 'e.id = c.id_jabatan','left');
			$this->db->join('ms_unit as f ', 'f.id = c.id_jabatan','left');
			
			
			$this->db->where('a.id_jenjangprodi',$id_jenjangprodi);
			$this->db->where('a.id_prodi',$id_prodi);
			$this->db->where('a.id_tahunakademik',$id_tahunakademik);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
		
}
?>