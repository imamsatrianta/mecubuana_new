<?php
Class settingborang_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('br_parameterborang3b as a');
			$this->db->join('ms_jenjangprodi as b ', 'b.id = a.id_jenjangprodi');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			
			$this->db->select('a.*,pr.poinind,b.nm_jenjangprodi  FROM br_parameterborang3b as a 
			left JOIN 
			(SELECT id as iddua, `poin` as poinind FROM br_parameterborang3b ) pr on pr.iddua=a.parent_id
			inner join ms_jenjangprodi as b on b.id=a.id_jenjangprodi');


		//	$this->db->select('a.*,b.nm_jenjangprodi,(select poin from br_parameterborang3b where parent_id=a.id) as poininduk',false);
		//	$this->db->from('br_parameterborang3b as a');
		//	$this->db->join('ms_jenjangprodi as b ', 'b.id = a.id_jenjangprodi');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	function simpanData($data){
	//	print_r($data);exit;
		$status=$this->db->insert('br_parameterborang3b', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('br_parameterborang3b', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('br_parameterborang3b'); 
	if(!$status) return false;
		else return true;
	}	
	
	function getLevelsatu(){
		$data = array();
			$this->db->select('a.*');
			$this->db->from('br_parameterborang3b as a');
			$this->db->where('a.parent_id',0);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
		
	}
	function levelDetail($id){
		
		$this->load->database();
			$data = array();
			$this->db->select('a.*');
			$this->db->from('br_parameterborang3b as a');
			$this->db->where('a.parent_id', $id);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
		
}
?>