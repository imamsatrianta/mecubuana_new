<?php
Class global_m extends CI_Model{
	
	
	function getMenuheder(){
			$this->load->database();
		//echo $id_user;
			$data = array();
			
			$this->db->select('a.*');
			$this->db->from('menu as a');
			$this->db->where('a.parent',0);
			$this->db->order_by("a.no", "asc");
			$hasil = $this->db->get();
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	function getMenuduacoba($id){
			$this->load->database();
		//echo $id_user;
			$data = array();
			$sql="select a.*,(select count(id) from menu where parent=a.id) as jumlah from menu as a  where 
			a.parent='$id' order by no asc";
		
		$hasil = $this->db->query($sql);
			
			/*$this->db->select('a.*');
			$this->db->from('menu as a');
			$this->db->where('a.parent',$id);
			$this->db->order_by("a.no", "asc");
			$hasil = $this->db->get();
			*/
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	function getMenuduacobadua($id){
			$this->load->database();
		//echo $id_user;
			$data = array();
			
			$this->db->select('a.*');
			$this->db->from('menu as a');
			$this->db->where('a.parent',$id);
			$this->db->order_by("a.no", "asc");
			$hasil = $this->db->get();
			
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	function getMenuhedergrup(){
			$this->load->database();
			$data = array();
			
			$this->db->select('a.*');
			$this->db->from('menu_grup_heder as a');
			$hasil = $this->db->get();
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	function getMenudua($id_tahunakademik,$id_grup){
		$this->load->database();
			$data = array();
			
			$this->db->select('a.*');
			$this->db->from('menu_group as a');
			$this->db->where('a.id_tahunakademik', $id_tahunakademik);
			$this->db->where('a.id_menugrup', $id_grup);
			$this->db->where('a.parent',0);
			$hasil = $this->db->get();
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	
	
	
	function getGrupmenu(){
		$data = array();
			//$sql="select * from menu where parent='0' order by no asc ";
			//$hasil = $this->db->query($sql);
			
			$this->db->select('a.nm_grup,a.id_tahunakademik');
			$this->db->from('menu_group as a');
			$this->db->group_by("a.nm_grup");
			$hasil = $this->db->get();
			
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	function getTahunakademik(){
		$data = array(); 
			
			$this->db->select('a.id_tahunakademik,b.nm_tahun_akademik');
			$this->db->from('menu_group as a');
			$this->db->join('tahun_akademik as b ', 'a.id_tahunakademik = b.id');
			$this->db->group_by("a.id_tahunakademik");
			$hasil = $this->db->get();
			
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	function getDirektoratdata($id_tahunakademik){
		$data = array(); 
			
			$this->db->select('a.id_direktoorat,b.nm_direktorat');
			$this->db->from('menu_group as a');
			$this->db->join('ms_direktorat as b ', 'a.id_direktoorat = b.id');
			$this->db->group_by("a.id_direktoorat");
			$this->db->where('a.id_tahunakademik', $id_tahunakademik);
			$hasil = $this->db->get();
			
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	function getUnitdata($id_direktoorat){
		$data = array(); 
			
			$this->db->select('a.id_unit,b.nm_unit,a.id');
			$this->db->from('menu_group as a');
			$this->db->join('ms_unit as b ', 'a.id_unit = b.id');
			$this->db->group_by("a.id_unit");
			$this->db->where('a.id_direktoorat', $id_direktoorat);
			$hasil = $this->db->get();
			
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	function getMenudetdata($id){
		$data = array(); 
			
			$this->db->select('a.nm_menu,a.id_menu,b.url');
			$this->db->from('menu_grup_det as a');
			$this->db->join('menu as b ', 'a.id_menu = b.id');
			$this->db->where('a.id_menugroup', $id);
			$hasil = $this->db->get();
			//echo $this->db->last_query(); exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	
	function getDirektorat(){
		$this->load->database();
		//echo $id_user;
			$data = array();
			$sql="select id,nm_direktorat,id_rektorat,1 as direk from ms_direktorat as a 
					union ALL
						select id,nm_fakultas,id_rektorat, 2 as fak from ms_fakultas as a ";
			//echo $sql;
		$hasil = $this->db->query($sql);
			
			/*$this->db->select('a.*');
			$this->db->from('menu as a');
			$this->db->join('user_menu as b ', 'a.id = b.id_menu');
			$this->db->where('a.parent',0);
			$this->db->where('b.id_user', $id_user);
			$this->db->order_by("a.no", "asc");
			$hasil = $this->db->get();*/
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
			return $data;
		
	}
	function getUnitdir($id_direktorat,$direk){
		$this->load->database();
		//echo $id_user;
			$data = array();
			$sql="select id,nm_unit,id_direktorat from ms_unit as  a where id_direktorat='$id_direktorat' and flek='$direk'
					union ALL
						select id,nm_prodi,id_fakultas from ms_prodi as a where id_fakultas='$id_direktorat' and flek='$direk'";
			//echo $sql;
		$hasil = $this->db->query($sql);
			
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
		
	}
		
	
	function menuDetail($id_user,$id_menu){
		$this->load->database();
		$data = array();
		//	$sql="select a.* from menu as a inner join user_menu as b on a.id=b.id_menu where a.parent='$id_menu' and b.id_user='$id_user' order by no asc";
		
		//	$hasil = $this->db->query($sql);
			
			$this->db->select('a.*');
			$this->db->from('menu as a');
			$this->db->join('user_menu as b ', 'a.id = b.id_menu');
			$this->db->where('a.parent', $id_menu);
			$this->db->where('b.id_user', $id_user);
			$this->db->order_by("a.no", "asc");
			
			$hasil = $this->db->get();
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	function getMenuakses($id_menu,$id_user){
		$this->load->database();
		
		 $this->db->select('user_menu.*,menu.parent,menu.id AS idmenu,menu.menudes');
        $this->db->from('user_menu');
		$this->db->join('menu', 'user_menu.id_menu = menu.id');
		$this->db->where('user_menu.id_user', $id_user);
		$this->db->where('user_menu.id_menu', $id_menu);
		$hasil = $this->db->get();
		//echo $this->db->last_query(); 
		//$sql="select a.*,b.parent,b.id AS idmenu,b.menudes from  user_menu as a  left join menu as b on a.id_menu=b.id where a.id_user='$id_user' and a.id_menu='$id_menu' ";
		//echo $sql;
		//	$hasil = $this->db->query($sql);
			
            if ($hasil->num_rows() === 0){
			
			return 0;
			}else{
				$row = $hasil->row();
				//return 0;
				return $row;
			}
			
		
	}
		function getParent(){
		$data = array();
			//$sql="select * from menu where parent='0' order by no asc ";
			//$hasil = $this->db->query($sql);
			
			$this->db->select('menu.*');
			$this->db->from('menu');
			$this->db->where('menu.parent',0);
			$this->db->order_by("menu.no", "asc");
			$hasil = $this->db->get();
			
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
		function getMenudet($id){
		$data = array();
			//$sql="select * from menu where parent='$id' order by no asc ";
			//$hasil = $this->db->query($sql);
			
			$this->db->select('menu.*');
			$this->db->from('menu');
			$this->db->where('menu.parent',$id);
			$this->db->order_by("menu.no", "asc");
			$hasil = $this->db->get();
			
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}

	function getJumlahMenudet($id){
		$jumlah=0;
		//$query = $this->db->query("select count(*) as jumlah from menu where parent='$id'");
		
			$this->db->select('count(*) as jumlah');
			$this->db->from('menu');
			$this->db->where('menu.parent',$id);
			$query = $this->db->get();
			
		
		if ($query->num_rows() > 0){
   		$row = $query->row_array(); 
			$jml=$row['jumlah'];
			$jumlah=$jml+1;
		}
		return $jumlah;
	}
function getRool(){
		$data = array();
		//	$sql="select * from rol where status='1' ";
			//$hasil = $this->db->query($sql);
			
			$this->db->select('rol.*');
			$this->db->from('rol');
			$this->db->where('rol.status',1);
			$hasil = $this->db->get();
			
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
function getUnit(){
	$data = array();
			$this->db->select('a.*');
			$this->db->from('unit as a');
			$hasil = $this->db->get();
			
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
}





}



?>