<?php
Class global_combomodel extends CI_Model{
	function getUnitdep(){
		$data = array();
		$sql="select id,nm_unit,1 as jenis  from ms_unit 
			union all 
			select id,nm_direktorat,2 as jenis  from ms_direktorat";
		
		$hasil = $this->db->query($sql);
		return $hasil;
	}
	function getDosen(){
		$data = array();
		$this->db->select('a.id,a.nm_karyawan');
		$this->db->from('ms_karyawan as a');
		$this->db->where('nidn !=','0');
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getProdijenjang($id_jenjang){
		$data = array();
		$this->db->select('a.id,a.nm_prodi');
		$this->db->from('ms_prodi as a');
		$this->db->where('id_jenjangprodi',$id_jenjang);
		$this->db->where('level','2');
		$this->db->order_by('id','asc'); 
		$hasil = $this->db->get();
		return $hasil;
	}
	function getMkp($id_tahunakademik){
		$data = array();
		$this->db->select('a.id,a.nm_mkp');
		$this->db->from('sp_mkp as a');
		$this->db->where('id_tahunakademik',$id_tahunakademik);
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getTahunakademikAuditLaporan(){
		$data = array();
		$this->db->select('a.id,a.periode,c.nm_unit');
		$this->db->from('sp_persiapan_pelaksanaan as a');
		$this->db->join('sp_planing_audit b','b.id = a.id_planingaudit');
		$this->db->join('ms_unit c ','c.id = b.id_unit');
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getTahunakademik(){
		$data = array();
		$this->db->select('a.id,a.nm_tahun_akademik,a.keterangan');
		$this->db->from('tahun_akademik as a');
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getPlaningpersiapan(){
		$data = array();
		$this->db->select('a.id_planingaudit,a.periode,c.nm_unit as nm_unit,d.nm_karyawan as ketua_tim');
		$this->db->from('sp_persiapan_pelaksanaan a');
		$this->db->join('sp_planing_audit b','a.id_planingaudit = b.id');
		$this->db->join('ms_unit c','b.id_unit = c.id');
		$this->db->join('ms_karyawan d','b.id_ketuatim = d.id');
		$this->db->order_by('a.id_planingaudit','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	// function getUnitdeplap(){
	// 	$data = array();
	// 	$this->db->select('a.id,a.nm_tahun_akademik,a.keterangan');
	// 	$this->db->from('tahun_akademik as a');
	// 	$this->db->order_by('id','asc');
	// 	$hasil = $this->db->get();
	// 	return $hasil;
	// }
	function getRektorat(){
		$data = array();
		$this->db->select('a.id,a.nm_rektorat');
		$this->db->from('ms_rektorat as a');
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getSop(){
		$data = array();
		$this->db->select('a.id,a.nm_sop');
		$this->db->from('sp_sop as a');
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getStandar(){
		$data = array();
		$this->db->select('a.id,a.nm_dokumen');
		$this->db->from('sp_standar as a');
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getAuditie(){
		$data = array();
		$this->db->select('a.id,a.nm_karyawan');
		$this->db->from('ms_karyawan as a');
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
        function getPlanstandar($where){
		$data = array();
		$this->db->select('group_concat(id_standar) as id_standar');
		$this->db->from('sp_planing_audit_standar as a');
        $this->db->where('a.id_planingaudit',$where);
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
        function getFormulirstandar($where){
		$data = array();
		$this->db->select('a.id,c.id as id_doc,c.id_persiapan_formulir,a.nm_dokumen,b.materi,c.document');
		$this->db->from('sp_standar as a');
		$this->db->join('sp_persiapan_formulir as b ', 'a.id = b.id_standar', 'left');
		$this->db->join('sp_persiapan_formulir_doc as c ', 'b.id = c.id_persiapan_formulir', 'left');
		$this->db->where('a.id in ('.$where.')');
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getFormuliranggota($where){
		$data = array();
		$this->db->select('*');
		$this->db->from('sp_planing_audit_anggota as a');
		$this->db->join('sp_persiapan_pelaksanaan as b ', 'a.id_planingaudit = b.id_planingaudit', 'left');
		$this->db->join('ms_karyawan as c','a.id_anggota = c.id','left');
		$this->db->where('b.id in ('.$where.')');
		$this->db->order_by('b.id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
        function getFormulirstandaraudit($where){
		$data = array();
		$this->db->select('a.*');
		$this->db->from('sp_formulir_audit_doc as a');
		$this->db->where('a.id_formulir_audit in ('.$where.')');
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getRenstra(){
		$data = array();
		$this->db->select('a.id,a.nm_dokumen');
		$this->db->from('sp_renstra as a');
		$this->db->order_by('id','asc'); 
		$hasil = $this->db->get();
		return $hasil;
	}
	function getDirektorat(){
		$data = array();
		$this->db->select('a.id,a.nm_direktorat');
		$this->db->from('ms_direktorat as a');
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getDirektoratdua($id_rektorat){
		$data = array();
		$this->db->select('a.id,a.nm_direktorat');
		$this->db->from('ms_direktorat as a');
		$this->db->where('id_rektorat',$id_rektorat);
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getUnitdua($id_direktorat){
		$data = array();
		$this->db->select('a.id,a.nm_unit');
		$this->db->from('ms_unit as a');
		$this->db->where('id_direktorat',$id_direktorat);
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getProdidua(){
		$data = array();
		$this->db->select('a.id,a.nm_prodi');
		$this->db->from('ms_prodi as a');
	//	$this->db->where('id_rektorat',$id_rektorat);
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getFakultas(){
		$data = array();
		$this->db->select('a.id,a.nm_fakultas');
		$this->db->from('ms_fakultas as a');
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	
	function getJenjangprodi(){
		$data = array();
		$this->db->select('a.id,a.nm_jenjangprodi');
		$this->db->from('ms_jenjangprodi as a');
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getProdi($id_fakultas){
		$data = array();
		$this->db->select('a.id,a.nm_prodi');
		$this->db->from('ms_prodi as a');
		$this->db->where('id_fakultas',$id_fakultas);
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getLevelmenu(){
		$data = array();
			
			$this->db->select('id,level');
			$this->db->from('ms_level');
			$hasil = $this->db->get();
			
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	function getRektoratdua(){
		$data = array();
			
			$this->db->select('id,nm_rektorat');
			$this->db->from('ms_rektorat');
			$hasil = $this->db->get();
			
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	//==========Riyan===============
	function getKaryawan(){
		$data = array();
		$this->db->select('a.id,a.nm_karyawan');
		$this->db->from('ms_karyawan as a');
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getKtsob(){
		$data = array();
		$this->db->select('a.id,a.id_standar,a.kts_ob');
		$this->db->from('sp_laporan_audit_kts_ob as a');
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
        function getDataplan($where){
		$data = array();
		$this->db->select('a.*,b.nm_unit,c.nm_karyawan');
		$this->db->from('sp_planing_audit AS a');
		$this->db->join('ms_unit AS b', 'b.id = a.id_unit');
		$this->db->join('ms_karyawan AS c', 'c.id = a.id_ketuatim');
		$this->db->where('a.id',$where);
		$this->db->order_by('a.id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getDataktsob($where){
		$data = array();
		$this->db->select('a.id_standar as jmb,b.nm_dokumen');
		$this->db->from('sp_laporan_audit_kts_ob AS a');
		$this->db->join('sp_standar	b', 'b.id = a.id_standar');
		$this->db->where('a.id',$where);
		$this->db->order_by('a.id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getDataplanstandar($where){
		$data = array();
		$this->db->select('a.*,b.nm_unit,c.nm_karyawan,d.id_standar,f.id_auditie');
		$this->db->from('sp_planing_audit AS a');
		$this->db->join('ms_unit AS b', 'b.id = a.id_unit');
		$this->db->join('ms_karyawan AS c', 'c.id = a.id_ketuatim');
		$this->db->join('sp_planing_audit_standar d', 'd.id_planingaudit = a.id');
		$this->db->join('sp_planing_audit_auditie f', 'f.id_planingaudit = a.id');
		$this->db->where('a.id',$where);
		$this->db->order_by('a.id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	function getFormulir(){
		$data = array();
		$this->db->select('a.id,a.nm_formulir');
		$this->db->from('sp_formulir as a');
		$this->db->order_by('id','asc');
		$hasil = $this->db->get();
		return $hasil;
	}
	}
?>