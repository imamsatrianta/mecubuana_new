<?php
Class lapborang_m extends CI_Model{


	
	function getIdentitas($id_jenjangprodi,$id_prodi){
		$data = array();
			
			$this->db->select('a.*,b.nm_jenjangprodi,c.nm_prodi,d.nm_prodi as fskultas');
			$this->db->from('br_identitas_borang as a');
			$this->db->join('ms_jenjangprodi as b ', 'a.id_jenjang = b.id','left');
			$this->db->join('ms_prodi as c ', 'a.id_prodi = c.id','left');
			$this->db->join('ms_prodi as d ', 'd.id = c.parent','left');
			$this->db->where('a.id_jenjang',$id_jenjangprodi);
			$this->db->where('a.id_prodi',$id_prodi);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}	
	
	
	function getIdentitasdosen($id_jenjangprodi,$id_prodi){
		$data = array();
			
			$this->db->select('c.nm_karyawan,c.nidn,c.tgl_lahir,c.jabatan_akademik,c.jenjang_prodi,c.gelar_akademim,c.asal_ps,c.bidang_keahlian');
			$this->db->from('br_identitas_borang as a');
			$this->db->join('br_identitas_borang_dosen as b ', 'a.id = b.id_identitas','left');
			$this->db->join('ms_karyawan as c ', 'b.id_dosen = c.id','left');
			
			$this->db->where('a.id_jenjang',$id_jenjangprodi);
			$this->db->where('a.id_prodi',$id_prodi);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}	
	
	function getPengisiborang($id_jenjangprodi,$id_prodi,$id_tahunakademik){
		$data = array();
			
			$this->db->select("c.nm_karyawan,c.nidn,b.tgl_penisian,CASE c.jenis_jabatan WHEN '1'
THEN  d.nm_rektorat
WHEN '2'
THEN e.nm_direktorat
WHEN '3'
THEN f.nm_unit
WHEN '4'
THEN 'Staf'
WHEN '5'
THEN 'Kepala Prodi'
END
AS nm_jabatan",false);
			$this->db->from('br_pengisi as a'); 
			$this->db->join('br_pengisi_det as b ', 'a.id = b.id_pengisi','left');
			$this->db->join('ms_karyawan as c ', 'b.id_karyawan = c.id','left');
			
			$this->db->join('ms_rektorat as d ', 'd.id = c.id_jabatan','left');
			$this->db->join('ms_direktorat as e ', 'e.id = c.id_jabatan','left');
			$this->db->join('ms_unit as f ', 'f.id = c.id_jabatan','left');
			
			
			$this->db->where('a.id_jenjangprodi',$id_jenjangprodi);
			$this->db->where('a.id_prodi',$id_prodi);
			$this->db->where('a.id_tahunakademik',$id_tahunakademik);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	function getBorang_tiga($id_jenjangprodi,$id_prodi,$id_tahunakademik,$id_parameter,$tabel){
		$data = array();
			
			$this->db->select('b.*');
			$this->db->from('br_d3 as a');
			$this->db->join("$tabel  as b ", 'a.id = b.id_borangd3','left');
			
			$this->db->where('a.id_tahunakademik',$id_tahunakademik);
			$this->db->where('a.id_prodi',$id_prodi);
			$this->db->where('a.id_jenjangprodi',$id_jenjangprodi);
			$this->db->where('a.id_parameter',$id_parameter);
			$this->db->order_by("b.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	
}
?>