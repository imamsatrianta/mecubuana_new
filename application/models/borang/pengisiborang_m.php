<?php
Class pengisiborang_m extends CI_Model{
	

	
    function loaddataTabel($offset,$limit,$order,$where){
			$id_prodi=$this->session->userdata('id_prodi');
			$levelprodi=$this->session->userdata('levelprodi');
		//	$id_level=$this->session->userdata('id_level');
			$level=$_GET['level'];
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('br_pengisi as a');
			$this->db->join('ms_jenjangprodi as b ', 'b.id = a.id_jenjangprodi');
			$this->db->join('tahun_akademik as c ', 'c.id = a.id_tahunakademik');
			$this->db->join('ms_prodi as d ', 'd.id = a.id_prodi');
			$this->db->where('a.id_jenjangprodi',$level);
			
			
			$this->db->where($where);
			if($levelprodi=="2"){
				$this->db->where('a.id_prodi',$id_prodi);
			}else if($levelprodi=="1"){
				$this->db->where('d.parent',$id_prodi);
			}
			
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*,b.nm_jenjangprodi,c.nm_tahun_akademik,d.nm_prodi,d.kd_server');
			$this->db->from('br_pengisi as a');
			$this->db->join('ms_jenjangprodi as b ', 'b.id = a.id_jenjangprodi');
			$this->db->join('tahun_akademik as c ', 'c.id = a.id_tahunakademik');
			$this->db->join('ms_prodi as d ', 'd.id = a.id_prodi');
			
			$this->db->where('a.id_jenjangprodi',$level);
			
			$this->db->where($where);
			if($levelprodi=="2"){
				$this->db->where('a.id_prodi',$id_prodi);
			}else if($levelprodi=="1"){
				$this->db->where('d.parent',$id_prodi);
			}
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	function cekBorang($id_tahunakademik,$id_jenjangprodi,$id_prodi){
		$this->db->select('a.id');
	$this->db->from('br_pengisi as a');
	$this->db->where('a.id_tahunakademik',$id_tahunakademik);
	$this->db->where('a.id_jenjangprodi',$id_jenjangprodi);
	$this->db->where('a.id_prodi',$id_prodi);	
	$hasil = $this->db->get();
	$result = $hasil->result_array();
	return $result;
	}
	function simpanData($data){
		$this->db->insert('br_pengisi', $data);
		  $insert_id = $this->db->insert_id();
		  return  $insert_id;
		  
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('br_pengisi', $data);
		//echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('br_pengisi'); 
	if(!$status) return false;
		else return true;
	}	
	
	function simpanDatadetail($datasimpan){
		 $this->db->insert('br_pengisi_det', $datasimpan);
		  $status = $this->db->insert_id();
	//	echo $this->db->last_query();exit;
			if(!$status) return false;
			else return true;
	}
	function hapusDetail($id){
		$this->db->where('id_pengisi', $id);
        $result = $this->db->delete('br_pengisi_det');
		if(!$result) return false;
		else return true;
	}
	function getEditdetail($id){
		$result = array();
		$this->db->select('a.*,b.nm_karyawan');
		$this->db->from('br_pengisi_det as a');
		$this->db->join('ms_karyawan as b ', 'b.id = a.id_karyawan','left');
		$this->db->where('id_pengisi',$id); 
		$hasil = $this->db->get();
		
		$rs = $hasil->result(); 
		 $items = array();
		 foreach($rs as $row){
			 array_push($items, $row);
		}
		return json_encode($items);
	}
	
	
	
	
	
	
	
}
?>