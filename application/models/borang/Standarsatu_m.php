<?php
Class Standarsatu_m extends CI_Model{
	
	function loadDatamenu($level){
		$data = array();
			
			$this->db->select('a.*');
			$this->db->from('br_parameterborang3a as a');
			$this->db->where('a.id_jenjangprodi',$level);
			$this->db->where('a.standar','1');
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
    function loaddataTabel($offset,$limit,$order,$where,$parameter){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('br_standarsatu as a');
			$this->db->join('ms_jenjangprodi as b ', 'b.id = a.id_jenjangprodi');
			$this->db->join('tahun_akademik as c ', 'c.id = a.id_tahunakademik');
			$this->db->join('ms_prodi as d ', 'd.id = a.id_prodi');
			$this->db->where('id_parameter',$parameter);
			$this->db->where($where);
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*,b.nm_jenjangprodi,c.nm_tahun_akademik,d.nm_prodi');
			$this->db->from('br_standarsatu as a');
			$this->db->join('ms_jenjangprodi as b ', 'b.id = a.id_jenjangprodi');
			$this->db->join('tahun_akademik as c ', 'c.id = a.id_tahunakademik');
			$this->db->join('ms_prodi as d ', 'd.id = a.id_prodi');
			$this->db->where('id_parameter',$parameter);
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	function cekBorang($id_tahunakademik,$id_parameter,$id_jenjangprodi,$id_prodi){
		$this->db->select('a.id');
	$this->db->from('br_standarsatu as a');
	$this->db->where('a.id_tahunakademik',$id_tahunakademik);
	$this->db->where('a.id_parameter',$id_parameter); 
	$this->db->where('a.id_jenjangprodi',$id_jenjangprodi);
	$this->db->where('a.id_prodi',$id_prodi);	
	$hasil = $this->db->get();
	$result = $hasil->result_array();
	return $result;
	}
	function simpanData($data){
	//	print_r($data);exit;
		$status=$this->db->insert('br_standarsatu', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('br_standarsatu', $data);
		//echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('br_standarsatu'); 
	if(!$status) return false;
		else return true;
	}	
	
	function simpanDatakeb($data){
		$status=$this->db->insert('br_standarsatu_kebijakan', $data);
		//echo $this->db->last_query();exit;
			if(!$status) return false;
			else return true;
	}
	function editDatakeb($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('br_standarsatu_kebijakan', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}
	
	function loaddatakebijakan($id){
		$data = array();
		$this->db->select("a.*");
		$this->db->from("br_standarsatu_kebijakan as a");
		$this->db->where('a.id_borangsatu',$id);
		$hasil = $this->db->get();
		$rs = $hasil->result(); 
		
		$items = array();
			
		foreach($rs as $row){
			
			array_push($items, $row);
				 
		}
		 echo json_encode($items);
		
		
	}
	function hapusKebijakanheder($id){
		$this->db->where('id_borangsatu', $id);
		$status=$this->db->delete('br_standarsatu_kebijakan'); 
		if(!$status) return false;
			else return true;
	}
	function hapusKebijakan($id){
		$this->db->where('id', $id);
		$status=$this->db->delete('br_standarsatu_kebijakan'); 
		if(!$status) return false;
			else return true;
	}
		
}
?>