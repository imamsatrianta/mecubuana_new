<?php
Class penilaianborang_m extends CI_Model{


	
	function getParametersatu($id_jenjangprodi,$id_tahunakademik,$id_prodi){
		$data = array();
			
			$this->db->select('a.*,b.nilai');
			$this->db->from('br_parameterborang3a as a');
			$this->db->join('br_standarsatu as b ', 'a.id = b.id_parameter','left');
			$this->db->where('a.id_jenjangprodi',$id_jenjangprodi);
			$this->db->where('b.id_tahunakademik',$id_tahunakademik);
			$this->db->where('b.id_prodi',$id_prodi);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}	
	
	function getParameterdua($id_jenjangprodi,$id_tahunakademik,$id_prodi){
		$data = array();
			
			$this->db->select('a.*,b.nilai');
			$this->db->from('br_parameterborang3a as a');
			$this->db->join('br_standardua as b ', 'a.id = b.id_parameter','left');
			$this->db->where('a.id_jenjangprodi',$id_jenjangprodi);
			$this->db->where('b.id_tahunakademik',$id_tahunakademik);
			$this->db->where('b.id_prodi',$id_prodi);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
		
	}
	
	function getStandartigasatu($id_jenjangprodi,$id_tahunakademik,$id_prodi){
		$data = array();
			
			$this->db->select('sum(daya_tampung) as daya_tampung,sum(mhs_ikut_seleksi) as mhs_ikut_seleksi,sum(jml_reg_bkn_transfer) as jml_reg_bkn_transfer,
			sum(mhs_lulus_seleksi) as mhs_lulus_seleksi,sum(ipk_lulusan_reg_rata/5) as ipk_lulusan_reg_rata');
			$this->db->from('br_d3 as a');
			$this->db->join('br_d3_tiga_satu_satu as b ', 'a.id = b.id_borangd3','left');
			$this->db->where('a.id_jenjangprodi',$id_jenjangprodi);
			$this->db->where('a.id_tahunakademik',$id_tahunakademik);
			$this->db->where('a.id_prodi',$id_prodi);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	function getStandartigasatudua($id_jenjangprodi,$id_tahunakademik,$id_prodi){
		$data = array();
			
			$this->db->select('sum(daya_tampung) as daya_tampung,sum(mhs_ikut_seleksi) as mhs_ikut_seleksi,sum(jml_reg_bkn_transfer) as jml_reg_bkn_transfer,
			sum(mhs_lulus_seleksi) as mhs_lulus_seleksi,sum(ipk_lulusan_reg_rata/5) as ipk_lulusan_reg_rata');
			$this->db->from('br_d3 as a');
			$this->db->join('br_d3_tiga_satu_satu as b ', 'a.id = b.id_borangd3','left');
			$this->db->where('a.id_jenjangprodi',$id_jenjangprodi);
			$this->db->where('a.id_tahunakademik',$id_tahunakademik);
			$this->db->where('a.id_prodi',$id_prodi);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	
}
?>