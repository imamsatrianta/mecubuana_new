<?php
Class Renstra_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_renstra as a');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*');
			$this->db->from('sp_renstra as a');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	function simpanData($data){
	//	print_r($data);exit;
		$status=$this->db->insert('sp_renstra', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('sp_renstra', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('sp_renstra'); 
	if(!$status) return false;
		else return true;
	}	
	
	function simpanDatadetail($datadetdua){
		$status=$this->db->insert('sp_renstra_det', $datadetdua);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function hapusDetail($iddet){
		$this->db->where('id_renstra', $iddet);
	$status=$this->db->delete('sp_renstra_det'); 
	if(!$status) return false;
		else return true;
	}
	
	function getDetail($id){
		$result = array();
		$this->db->select('a.*');
		$this->db->from('sp_renstra_det as a');
		$this->db->where('id_renstra',$id); 
		$hasil = $this->db->get();
		
		$rs = $hasil->result(); 
		 $items = array();
		 foreach($rs as $row){
			 array_push($items, $row);
		}
		return json_encode($items);
	}
	
	
	
		
}
?>