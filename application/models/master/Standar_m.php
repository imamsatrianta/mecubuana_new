<?php
Class Standar_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_standar as a');
			$this->db->join('sp_renstra as b ', 'a.id_renstra = b.id');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			$this->db->select('a.*,pr.nm_dokumen as dokumeninduk,b.nm_dokumen as renstra FROM sp_standar as a left JOIN 
(SELECT id as iddua, `nm_dokumen` FROM sp_standar ) pr on pr.iddua=a.parent_id');
			$this->db->join('sp_renstra as b ', 'a.id_renstra = b.id');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	function simpanData($data){
	//	print_r($data);exit;
		$status=$this->db->insert('sp_standar', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('sp_standar', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('sp_standar'); 
	if(!$status) return false;
		else return true;
	}	
	
	function simpanDatadetail($datadetdua){
		$status=$this->db->insert('sp_standar_det', $datadetdua);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function editDatadetail($datadetdua,$id_dok){
		$this->db->where('id',$id_dok);
		$status=$this->db->update('sp_standar_det', $datadetdua);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}
	
	function hapusDetaildata($iddet){
		$this->db->where('id_standar', $iddet);
	$status=$this->db->delete('sp_standar_det'); 
	if(!$status) return false;
		else return true;
	}
	function hapusDetail($iddet,$databapus){
		$where="id  not in($databapus)";
		$this->db->where($where);
		$this->db->where('id_standar', $iddet);
	$status=$this->db->delete('sp_standar_det'); 
	if(!$status) return false;
		else return true;
	}
	
	function getDetail($id){
		$result = array();
		$this->db->select('a.*');
		$this->db->from('sp_standar_det as a');
		$this->db->where('id_standar',$id); 
		$hasil = $this->db->get();
		
		$rs = $hasil->result(); 
		 $items = array();
		 foreach($rs as $row){
			 array_push($items, $row);
		}
		return json_encode($items);
	}
	
	function getStandarutama(){
		$data = array();
			$this->db->select('a.*');
			$this->db->from('sp_standar as a');
			$this->db->where('a.parent_id',0);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
		
	}
	function standarDetail($id){
		
		$this->load->database();
			$data = array();
			$this->db->select('a.*');
			$this->db->from('sp_standar as a');
			$this->db->where('a.parent_id', $id);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	
		
}
?>