<?php
Class Mkp_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_mkp as a');
			$this->db->join('tahun_akademik as b ', 'a.id_tahunakademik = b.id');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*,b.nm_tahun_akademik,b.keterangan');
			$this->db->from('sp_mkp as a');
			$this->db->join('tahun_akademik as b ', 'a.id_tahunakademik = b.id');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	function simpanData($data){
	//	print_r($data);exit;
		$status=$this->db->insert('sp_mkp', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function simpanDatatarget($datasimpan){
		$status=$this->db->insert('sp_mkp_target', $datasimpan);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('sp_mkp', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	function editDatatarget($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('sp_mkp_target', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	function editDatakegiatan($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('sp_mkp_kegiatan', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}
	function hapusdaunit($iddet){
		$this->db->where('id_mkp_kegiatan', $iddet);
		$status=$this->db->delete('sp_mkp_unitdet'); 
		if(!$status) return false;
		else return true;
	}
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('sp_mkp'); 
	if(!$status) return false;
		else return true;
	}	
	function hapusdatatarget($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('sp_mkp_target'); 
	if(!$status) return false;
		else return true;
	}
	
	function simpanDataunit($datadetdua){
		$status=$this->db->insert('sp_mkp_unitdet', $datadetdua);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function simpanDakegiatan($datadetdua){
		$status=$this->db->insert('sp_mkp_kegiatan', $datadetdua);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function simpanDatadetail($datadetdua){
		$status=$this->db->insert('sp_mkp_standar', $datadetdua);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function hapusDetail($iddet){
		$this->db->where('id_mkp', $iddet);
	$status=$this->db->delete('sp_mkp_standar'); 
	if(!$status) return false;
		else return true;
	}
	
	function getDetail($id){
		$result = array();
		$this->db->select('a.*');
		$this->db->from('sp_mkp_standar as a');
		$this->db->where('id_mkp',$id); 
		$hasil = $this->db->get();
		
		$rs = $hasil->result(); 
		 $items = array();
		 foreach($rs as $row){
			 array_push($items, $row);
		}
		return json_encode($items);
	}
	
	
	// target
	function loaddataTabeltarget($offset,$limit,$order,$where){
		if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_mkp_target as a');
			$this->db->join('sp_mkp as b ', 'a.id_mkp = b.id');
			$this->db->join('tahun_akademik as c ', 'b.id_tahunakademik = c.id');
			$this->db->where($where);
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*,b.id_tahunakademik,b.nm_mkp,c.nm_tahun_akademik,c.keterangan');
			$this->db->from('sp_mkp_target as a');
			$this->db->join('sp_mkp as b ', 'a.id_mkp = b.id');
			$this->db->join('tahun_akademik as c ', 'b.id_tahunakademik = c.id');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
	}
	
	function loaddatadetailkegiatan($id){
		$data = array();
		$this->db->select("a.*");
		$this->db->from("sp_mkp_kegiatan as a");
		$this->db->where('a.id_mkp_target',$id);
		$hasil = $this->db->get();
		
		
		$rs = $hasil->result(); 
		
		$items = array();
			$oData = new stdClass;
			
		foreach($rs as $row){
			$id=$row->id;
			$kegiatandata=$this->dataKegiatanunit($id);
			//print_r($kegiatandata);exit;
			$id_unit=array();
			$jenis=array();
			$nm_unit=array();
			foreach($kegiatandata as $kegiatan){
				$id_unit[]=$kegiatan->id_unit."-".$kegiatan->jenis;
				$jenis[]=$kegiatan->jenis;
				$nm_unit[]=$kegiatan->nm_unit;
				
			}
			$id_unitdata=implode(',',$id_unit);
			$jenisdata=implode(',',$jenis);
			$nm_unitdata=implode(',',$nm_unit);
			
			$oData->id_unit  = $id_unitdata;
			$oData->jenis  = $jenisdata;
			$oData->nm_unit  = $nm_unitdata;
			$obj_merged = (object) array_merge((array) $oData, (array) $row);
			array_push($items, $obj_merged);
				 
		}
		 echo json_encode($items);
		
		
		//$this->db->join("kategori_sarmut as b","a.id_kategorisarmut=b.id");
		
		/*
		foreach ($hasil->result_array() as $row){
			$data[] = $row;
			}
		$json = json_encode($data);
		 
			echo $json;
			*/
	}
	
	function dataKegiatanunit($id){
		$sql="select a.id_unit,a.jenis, 
CASE a.jenis when '1'
THEN
(SELECT nm_unit from ms_unit WHERE id=a.id_unit)
when '2'
then
(SELECT nm_direktorat from ms_direktorat WHERE id=a.id_unit)
end
 as nm_unit
 FROM sp_mkp_unitdet as a where a.id_mkp_kegiatan='$id'";
 $hasil = $this->db->query($sql);
		
		
		$rs = $hasil->result();
		return $rs;
	}
	
	
	function hapusdatakegiatan($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('sp_mkp_kegiatan'); 
	if(!$status) return false;
		else return true;
	}
	function hapusdatakegiatanunit($id){
		$this->db->where('id_mkp_kegiatan', $id);
	$status=$this->db->delete('sp_mkp_unitdet'); 
	if(!$status) return false;
		else return true;
	}
		
}
?>