<?php
Class kegiatanmkp_m extends CI_Model{
	function loaddatakegiatan($offset,$limit,$order,$where){
	
		if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_mkp as a');
			$this->db->join('sp_mkp_target as b','b.id_mkp = a.id');
			$this->db->where($where);
			
			
			
			//$this->db->where('c.id_standar',$id_standar);
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select("b.*,a.nm_mkp");
			$this->db->from('sp_mkp as a');
			$this->db->join('sp_mkp_target as b','b.id_mkp = a.id');
			$this->db->where($where);
			
			//$this->db->where('c.id_standar',$id_standar);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
	}
	
	
	
    function loaddataTabel($offset,$limit,$order,$where){
			$id_level=$this->session->userdata('id_level');
		$id_unitses=$this->session->userdata('id_unit');
		$id_direktorat=$this->session->userdata('id_direktorat');
		$id_prodi=$this->session->userdata('id_prodi');
		
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_mkp_kegiatan as a');
			$this->db->join('sp_mkp_target as b ', 'a.id_mkp_target = b.id');
			
			$this->db->where($where);
			if($id_level=="3"){
				$jenis=2;
				$id_unit=$id_direktorat;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}else if($id_level=="4"){
				$jenis=1;
				$id_unit=$id_unitses;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}else if($id_level=="5"){
				$jenis=3;
				$id_unit=$id_prodi;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}
			
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select("a.*,b.uraian_target,(select count(id) from sp_mkp_kegiatan_ref where id_keg_mkp=a.id ) as jmlref,
CASE a.jenis when '1'
THEN
(SELECT nm_unit from ms_unit WHERE id=a.id_unit)
when '2'
then
(SELECT nm_direktorat from ms_direktorat WHERE id=a.id_unit)
when '3'
then
(SELECT nm_prodi from ms_prodi WHERE id=a.id_unit)
end
 as nm_unit",false);
			$this->db->from('sp_mkp_kegiatan as a');
			$this->db->join('sp_mkp_target as b ', 'a.id_mkp_target = b.id');
			$this->db->where($where);
			if($id_level=="3"){
				$jenis=2;
				$id_unit=$id_direktorat;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}else if($id_level=="4"){
				$jenis=1;
				$id_unit=$id_unitses;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}else if($id_level=="5"){
				$jenis=3;
				$id_unit=$id_prodi;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			 $oData = new stdClass;
			foreach($rs as $row){
				
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	

	
	function simpanData($data){
	//	print_r($data);exit;
		$status=$this->db->insert('sp_mkp_kegiatan', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('sp_mkp_kegiatan', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	
	function hapusData($id){
		$this->db->where('id', $id);
		$status=$this->db->delete('sp_mkp_kegiatan'); 
		if(!$status) return false;
		else return true;
	}

	
	
	function simpanDatarefisi($datasimpanref){
		$status=$this->db->insert('sp_mkp_kegiatan_ref', $datasimpanref);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function cekRefisi($id){
		 $sqldata = "SELECT count(id) as jumlah FROM sp_mkp_kegiatan_ref  WHERE  id_keg_mkp= '$id' ";
	//	echo  $sqldata;
		$query = $this->db->query($sqldata);
		$rowa = $query->row(); 
		$jumlah=$rowa->jumlah;
		
        return $jumlah;
	}
	
	
		
}
?>