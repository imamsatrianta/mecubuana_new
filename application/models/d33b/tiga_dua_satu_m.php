<?php
Class tiga_dua_satu_m extends CI_Model{
	
	function loadDatamenu($level){
		$data = array();
			
			$this->db->select('a.*');
			$this->db->from('br_parameterborang3b as a');
			$this->db->where('a.id_jenjangprodi',$level);
			$this->db->where('a.standar','3');
			$this->db->where('a.jenis !=','0');
			$this->db->order_by("a.no", "asc");
			$hasil = $this->db->get();
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
    function loaddataTabel($offset,$limit,$order,$where,$parameter){
			$id_prodi=$this->session->userdata('id_prodi');
			$levelprodi=$this->session->userdata('levelprodi');
		//	$id_level=$this->session->userdata('id_level');
			$level=$_GET['level'];
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('br_3bd3 as a');
			$this->db->join('ms_jenjangprodi as b ', 'b.id = a.id_jenjangprodi');
			$this->db->join('tahun_akademik as c ', 'c.id = a.id_tahunakademik');
			$this->db->join('ms_prodi as d ', 'd.id = a.id_prodi');
			$this->db->join("br_3bevaluasi as e ", "e.id_tahunakademik = a.id_tahunakademik and e.id_jenjangprodi = a.id_jenjangprodi and e.id_prodi = a.id_prodi and e.status_data=1 and e.standar='3'","left");
			$this->db->where('a.id_jenjangprodi',$level);
			
			if ($parameter<>0) {
				$this->db->where('id_parameter',$parameter);
			}
			$this->db->where($where);
			if($levelprodi=="2"){
				$this->db->where('a.id_prodi',$id_prodi);
			}else if($levelprodi=="1"){
				$this->db->where('d.parent',$id_prodi);
			}
			
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*,b.nm_jenjangprodi,c.nm_tahun_akademik,d.nm_prodi,d.kd_server,e.note_evaluasi,e.tgl_dodate as dodate,e.status_evaluasi');
			$this->db->from('br_3bd3 as a');
			$this->db->join('ms_jenjangprodi as b ', 'b.id = a.id_jenjangprodi');
			$this->db->join('tahun_akademik as c ', 'c.id = a.id_tahunakademik');
			$this->db->join('ms_prodi as d ', 'd.id = a.id_prodi');
			$this->db->join("br_3bevaluasi as e ", "e.id_tahunakademik = a.id_tahunakademik and e.id_jenjangprodi = a.id_jenjangprodi and e.id_prodi = a.id_prodi and e.status_data=1 and e.standar='3'","left");
			$this->db->where('a.id_jenjangprodi',$level);
			if ($parameter<>0) {
				$this->db->where('id_parameter',$parameter);
			}
			$this->db->where($where);
			if($levelprodi=="2"){
				$this->db->where('a.id_prodi',$id_prodi);
			}else if($levelprodi=="1"){
				$this->db->where('d.parent',$id_prodi);
			}
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	function cekBorang($id_tahunakademik,$id_parameter,$id_jenjangprodi,$id_prodi){
		$this->db->select('a.id');
	$this->db->from('br_3bd3 as a');
	$this->db->where('a.id_tahunakademik',$id_tahunakademik);
	$this->db->where('a.id_parameter',$id_parameter); 
	$this->db->where('a.id_jenjangprodi',$id_jenjangprodi);
	$this->db->where('a.id_prodi',$id_prodi);	
	$hasil = $this->db->get();
	$result = $hasil->result_array();
	return $result;
	}
	function simpanData($data){
		$this->db->insert('br_3bd3', $data);
		  $insert_id = $this->db->insert_id();
		  return  $insert_id;
		  
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('br_3bd3', $data);
		//echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('br_3bd3'); 
	if(!$status) return false;
		else return true;
	}	
	
	function simpanDatadetail($datasimpan){
		 $this->db->insert('br_3bd3_tiga_dua_satu', $datasimpan);
		  $status = $this->db->insert_id();
	//	echo $this->db->last_query();exit;
			if(!$status) return false;
			else return true;
	}
	function hapusDetail($id){
		$this->db->where('id_borangd3', $id);
        $result = $this->db->delete('br_3bd3_tiga_dua_satu');
		if(!$result) return false;
		else return true;
	}
	function getEditdetail($id){
		$result = array();
		$this->db->select('a.*');
		$this->db->from('br_3bd3_tiga_dua_satu as a');
		$this->db->where('id_borangd3',$id); 
		$hasil = $this->db->get();
		
		$rs = $hasil->result(); 
		 $items = array();
		 foreach($rs as $row){
			 array_push($items, $row);
		}
		return json_encode($items);
	}
	
	function getDataserver($id_prodi){
		$result = array();
		$this->db->select('a.*');
		$this->db->from('br_3bd3_tiga_dua_satu as a');
		$this->db->where('id_borangd3','0'); 
		$hasil = $this->db->get();
		
		$rs = $hasil->result(); 
		 $items = array();
		 foreach($rs as $row){
			 array_push($items, $row);
		}
		return json_encode($items);
	}
	
	function simpanDatarefisi($datasimpanref){
		$status=$this->db->insert('br_3bnot', $datasimpanref);
		//echo $this->db->last_query();exit;
			if(!$status) return false;
			else return true;
	}
	
	// kebijakan
	
	function simpanDatakeb($data){
		$status=$this->db->insert('br_3bd3_kebijakan', $data);
		//echo $this->db->last_query();exit;
			if(!$status) return false;
			else return true;
	}
	function editDatakeb($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('br_3bd3_kebijakan', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}
	
	function loaddatakebijakan($id){
		$data = array();
		$this->db->select("a.*");
		$this->db->from("br_3bd3_kebijakan as a");
		$this->db->where('a.id_borang',$id);
		$hasil = $this->db->get();
		$rs = $hasil->result(); 
		
		$items = array();
			
		foreach($rs as $row){
			
			array_push($items, $row);
				 
		}
		 echo json_encode($items);
		
		
	}
	function hapusKebijakanheder($id){
		$this->db->where('id_borang', $id);
		$status=$this->db->delete('br_3bd3_kebijakan'); 
		if(!$status) return false;
			else return true;
	}
	function hapusKebijakan($id){
		$this->db->where('id', $id);
		$status=$this->db->delete('br_3bd3_kebijakan'); 
		if(!$status) return false;
			else return true;
	}
	
}
?>