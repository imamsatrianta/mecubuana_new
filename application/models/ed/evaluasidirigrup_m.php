<?php
Class evaluasidirigrup_m extends CI_Model{
	
	
    function loaddataTabel($offset,$limit,$order,$where,$parameter){
			$level=$_GET['level'];
			$id_level=$this->session->userdata('id_level');
			$id_prodi=$this->session->userdata('id_prodi');
			$levelprodi=$this->session->userdata('levelprodi');
			
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('ed_isi as a');
			$this->db->join('ms_jenjangprodi as b ', 'b.id = a.id_jenjangprodi');
			$this->db->join('tahun_akademik as c ', 'c.id = a.id_tahunakademik');
			$this->db->join('ms_prodi as d ', 'd.id = a.id_prodi');
			$this->db->join("ed_evaluasi as e ", "e.id_tahunakademik = a.id_tahunakademik and e.id_jenjangprodi = a.id_jenjangprodi and e.id_prodi = a.id_prodi and e.status_data=1 ","left");
			$this->db->where('a.id_jenjangprodi',$level);
			if ($parameter<>0) {
				$this->db->where('id_parameter',$parameter);
			}
			$this->db->where($where);
			if($levelprodi=="2"){
				$this->db->where('a.id_prodi',$id_prodi);
			}else if($levelprodi=="1"){
				$this->db->where('d.parent',$id_prodi);
				$this->db->where('e.status_evaluasi !=','0');
			}
			$this->db->group_by('a.id_tahunakademik');
			$this->db->group_by('a.id_jenjangprodi');
			$this->db->group_by('a.id_prodi');
			
			$hasil = $this->db->get();
			//echo $this->db->last_query(); exit;
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*,b.nm_jenjangprodi,c.nm_tahun_akademik,d.nm_prodi,e.note_evaluasi,e.tgl_dodate as dodate,e.status_evaluasi');
			$this->db->from('ed_isi as a');
			$this->db->join('ms_jenjangprodi as b ', 'b.id = a.id_jenjangprodi');
			$this->db->join('tahun_akademik as c ', 'c.id = a.id_tahunakademik');
			$this->db->join('ms_prodi as d ', 'd.id = a.id_prodi');
			$this->db->join("ed_evaluasi as e ", "e.id_tahunakademik = a.id_tahunakademik and e.id_jenjangprodi = a.id_jenjangprodi and e.id_prodi = a.id_prodi and e.status_data=1 ","left");
			$this->db->where('a.id_jenjangprodi',$level);
			if ($parameter<>0) {
				$this->db->where('id_parameter',$parameter);
			}
			
			
			$this->db->where($where);
			if($levelprodi=="2"){
				$this->db->where('a.id_prodi',$id_prodi);
			}else if($levelprodi=="1"){
				$this->db->where('d.parent',$id_prodi);
				$this->db->where('e.status_evaluasi !=','0');
			}
			
			$this->db->group_by('a.id_tahunakademik');
			$this->db->group_by('a.id_jenjangprodi');
			$this->db->group_by('a.id_prodi');
			
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	function editData($id_tahunakademikevl,$id_jenjangprodievl,$id_prodievl,$data){
		$this->db->where('id_tahunakademik',$id_tahunakademikevl);
		$this->db->where('id_jenjangprodi',$id_jenjangprodievl);
		$this->db->where('id_prodi',$id_prodievl);
		$status=$this->db->update('ed_evaluasi', $data);
		//echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}
	
	function simpanDatarevaluasi($dataevaluasi){
		$status=$this->db->insert('ed_evaluasi', $dataevaluasi);
		//echo $this->db->last_query();exit;
			if(!$status) return false;
			else return true;
	}
	
	function getGroup($id_jenjangprodi,$id_tahunakademik,$id_prodi){
		$data = array();
			$this->db->select('a.*');
			$this->db->from('ed_group as a');
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}	
	function getEvaluasidiriparameter($id,$id_jenjangprodi){
		$data = array();
			$this->db->select('a.*');
			$this->db->from('ed_parameter as a');
			$this->db->where('id_group',$id);
			$this->db->where('parent_id',0);
			$this->db->where('id_jenjangprodi',$id_jenjangprodi);
			$hasil = $this->db->get(); 
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	function getEvaluasidiriparameterdua($idprm,$id,$id_jenjangprodi){
		$data = array();
			$this->db->select('a.*');
			$this->db->from('ed_parameter as a');
			$this->db->where('id_group',$id);
			$this->db->where('parent_id',$idprm);
			$this->db->where('id_jenjangprodi',$id_jenjangprodi);
			$hasil = $this->db->get(); 
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	function getEvaluasidiriisi($id,$id_jenjangprodi,$id_tahunakademik,$id_prodi){
		$data = array();
			$this->db->select('a.uraian');
			$this->db->from('ed_isi as a');
			$this->db->where('id_parameter',$id);
			$this->db->where('id_jenjangprodi',$id_jenjangprodi);
			$this->db->where('id_tahunakademik',$id_tahunakademik);
			$this->db->where('id_prodi',$id_prodi);
			$hasil = $this->db->get(); 
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
		
}
?>