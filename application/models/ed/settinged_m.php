<?php
Class settinged_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('ed_parameter as a');
			$this->db->join('ms_jenjangprodi as b ', 'b.id = a.id_jenjangprodi');
			$this->db->join('ed_group as c ', 'c.id = a.id_group');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			
			$this->db->select('a.*,pr.poinind,b.nm_jenjangprodi,c.nm_group  FROM ed_parameter as a 
			left JOIN 
			(SELECT id as iddua, `nm_evaluasidiri` as poinind FROM ed_parameter ) pr on pr.iddua=a.parent_id
			inner join ms_jenjangprodi as b on b.id=a.id_jenjangprodi
			inner join ed_group as c on c.id=a.id_group'
			);


			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	function simpanData($data){
	//	print_r($data);exit;
		$status=$this->db->insert('ed_parameter', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('ed_parameter', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('ed_parameter'); 
	if(!$status) return false;
		else return true;
	}	
	
	function getLevelsatu(){
		$data = array();
			$this->db->select('a.*');
			$this->db->from('ed_parameter as a');
			$this->db->where('a.parent_id',0);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
		
	}
	function levelDetail($id){
		
		$this->load->database();
			$data = array();
			$this->db->select('a.*');
			$this->db->from('ed_parameter as a');
			$this->db->where('a.parent_id', $id);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
		
}
?>