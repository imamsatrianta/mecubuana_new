<?php
Class aksesappborang_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('br_aksesapprove as a');
			$this->db->join('ms_prodi as b ', 'b.id = a.id_prodi');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*,b.nm_prodi');
			$this->db->from('br_aksesapprove as a');
			$this->db->join('ms_prodi as b ', 'b.id = a.id_prodi');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			  $oData = new stdClass;
			foreach($rs as $row){
				
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	
	function simpanData($data){
		$status=$this->db->insert('br_aksesapprove', $data);
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('br_aksesapprove', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('br_aksesapprove'); 
	if(!$status) return false;
		else return true;
	}	
	
	function cekData($id_prodi){
		$this->db->select('*');
			$this->db->from('br_aksesapprove');
			$this->db->where('id_prodi', $id_prodi);
			//$this->db->limit(1);
    	$Q2 = $this->db->get();
		return $Q2;
	}
	
		
}
?>