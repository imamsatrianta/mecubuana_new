<?php
Class prodi_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('ms_prodi as a');
		//	$this->db->join('ms_rektorat as b ', 'b.id = a.id_rektorat');
			$this->db->join('ms_jenjangprodi as c ', 'c.id = a.id_jenjangprodi');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*,pr.nm_prodi as parentinduk,c.nm_jenjangprodi FROM ms_prodi as a left JOIN 
(SELECT id as iddua, `nm_prodi` FROM ms_prodi ) pr on pr.iddua=a.parent');
		/*	$this->db->select('a.*,b.nm_rektorat,c.nm_jenjangprodi');
			$this->db->from('ms_prodi as a');*/
		//	$this->db->join('ms_rektorat as b ', 'b.id = a.id_rektorat');
			$this->db->join('ms_jenjangprodi as c ', 'c.id = a.id_jenjangprodi');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	function simpanData($data){
		$status=$this->db->insert('ms_prodi', $data);
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('ms_prodi', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('ms_prodi'); 
	if(!$status) return false;
		else return true;
	}	
	
	function getProdiutama(){
		$data = array();
			$this->db->select('a.*');
			$this->db->from('ms_prodi as a');
			$this->db->where('a.parent',0);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
		
	}
	function prodiDetail($id){
		
		$this->load->database();
			$data = array();
			$this->db->select('a.*');
			$this->db->from('ms_prodi as a');
			$this->db->where('a.parent', $id);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
		
}
?>