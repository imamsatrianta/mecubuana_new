<?php
Class grupmenu_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
		
		  if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'id';
			}
			
			//echo $where;
			//exit;
			$this->db->select('user.*');
			$this->db->from('user');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
            $result = array();
			$this->db->select('a.id,a.status,a.username,a.password as passwordisi,a.idrool,a.idrool as idrool_select,b.nm_rol');
			$this->db->from('user as a');
			$this->db->join('rol as b ', 'a.idrool = b.id_rol');
		//	$this->db->join('perusahaan as c ', 'a.id_perusahaan = c.id_perusahaan', 'left');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			$oData = new stdClass;
			foreach($rs as $row){
				//	print_r($row);
				  $pas=$row->passwordisi;
				   $oData->password  = paramDecrypt($pas);
				 //  print_r($oData);
				   
				   $obj_merged = (object) array_merge((array) $oData, (array) $row);
				   // print_r($obj_merged);
				   
				 array_push($items, $obj_merged);
			}
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	function simpanData($id,$data){
		$status=$this->db->insert('user', $data);
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('user', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function aktivData($id){
		
		$this->db->select('user.status');
		$this->db->from('user');
		$this->db->where('id',$id);	
		$query = $this->db->get();
		
		$row = $query->row(); 
		$statusda= $row->status;
		 if($statusda==0){
		 	$setdata=1;
		 }else{
		 $setdata=0;
		 }
		 
		 $data=array(
		 'status'=>$setdata);
		 $this->db->where('id', $id);
		 $status=$this->db->update('user',$data); 
		 
	if(!$status) return false;
		else return true;
}	

function getMenudata($id,$idrool){
	//$sql="select id from user where id='$id' and idrool='$idrool'";
	//echo $sql;
	//$hasil = $this->db->query($sql);
	
	$this->db->select('user.id');
		$this->db->from('user');
		$this->db->where('id',$id);	
		$this->db->where('idrool',$idrool);	
		$hasil = $this->db->get();
		
	return $hasil;
	
}


function hapusDatamenu($id){
	$this->db->where('id_user', $id);
	$status=$this->db->delete('user_menu'); 
	if(!$status) return false;
		else return true;

}

function getMenurool($idrool){
	$data = array();
			//$sql="select * from rol_menu where id_rol='$idrool'";
		//	$hasil = $this->db->query($sql);
			
			
		$this->db->select('rol_menu.*');
		$this->db->from('rol_menu');
		$this->db->where('id_rol',$idrool);	
		$hasil = $this->db->get();
		
		
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;

}

/*function simpanDatadetdua($datarol){
	$status=$this->db->insert('user_menu', $datarol);
			if(!$status) return false;
			else return true;
}*/

function hapusRoolmenu($rolid){
	$this->db->where('id_user', $rolid);
	$status=$this->db->delete('user_menu'); 
	if(!$status) return false;
		else return true;
}


function simpanDatadet($rolid,$id_menu,$tampil){
		$data=array(
		 'id_user'=>$rolid,
		 'id_menu'=>$id_menu,
		  'tampil'=>$tampil
		 );
		 
		$status=$this->db->insert('user_menu', $data);
			if(!$status) return false;
			else return true;
	}

function simpanDatadetdua($datarol){
		$status=$this->db->insert('user_menu', $datarol);
			if(!$status) return false;
			else return true;
	}

	
}
?>