<?php
Class Karyawan_m extends CI_Model{
	
	
    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('ms_karyawan as a');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select("a.*,CASE a.jenis_jabatan WHEN '1'
THEN  b.nm_rektorat
WHEN '2'
THEN c.nm_direktorat
WHEN '3'
THEN d.nm_unit
WHEN '4'
THEN 'Staf'
END
AS nm_jabatan",false);
			$this->db->from('ms_karyawan as a');
			$this->db->join('ms_rektorat as b ', 'b.id = a.id_jabatan');
			$this->db->join('ms_direktorat as c ', 'c.id = a.id_jabatan');
			$this->db->join('ms_unit as d ', 'd.id = a.id_jabatan');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	
	/*
	SELECT a.*, 
CASE a.jenis_jabatan WHEN '1'
THEN  b.nm_rektorat
WHEN '2'
THEN c.nm_direktorat
WHEN '3'
THEN d.nm_unit
WHEN '4'
THEN 'Staf'
END
AS nm_jabatan
 FROM ms_karyawan AS a 
LEFT JOIN ms_rektorat AS b ON a.id_jabatan=b.id AND a.jenis_jabatan='1'
LEFT JOIN ms_direktorat AS c ON a.id_jabatan=c.id AND a.jenis_jabatan='2'
LEFT JOIN ms_unit AS d ON a.id_jabatan=d.id AND a.jenis_jabatan='3'
*/
	function simpanData($data){
		$status=$this->db->insert('ms_karyawan', $data);
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('ms_karyawan', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('ms_karyawan'); 
	if(!$status) return false;
		else return true;
	}	
	
	
		
}
?>