<?php
Class jurusan_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('ms_jurusan as a');
			$this->db->join('ms_prodi as b ', 'b.id = a.id_prodi');
			$this->db->join('ms_fakultas as c ', 'c.id = b.id_fakultas');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*,b.nm_prodi,c.nm_fakultas,b.id_fakultas');
			$this->db->from('ms_jurusan as a');
			$this->db->join('ms_prodi as b ', 'b.id = a.id_prodi');
			$this->db->join('ms_fakultas as c ', 'c.id = b.id_fakultas');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	function simpanData($data){
		$status=$this->db->insert('ms_jurusan', $data);
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('ms_jurusan', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('ms_jurusan'); 
	if(!$status) return false;
		else return true;
	}	
	
	
		
}
?>