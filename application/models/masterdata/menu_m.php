<?php
Class menu_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'id';
			}
			$this->db->select('menu.*');
			$this->db->from('menu');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('menu.*');
			$this->db->from('menu');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	function simpanData($id,$data){$status=$this->db->insert('menu', $data);
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){$this->db->where('id',$id);
		$status=$this->db->update('menu', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){$this->db->where('id', $id);$status=$this->db->delete('menu'); 
	if(!$status) return false;
		else return true;
	}	
	
	function getMenuutama(){
		$data = array();
			$this->db->select('a.*');
			$this->db->from('menu as a');
			$this->db->where('a.parent',0);
			//$this->db->where('a.idmenu',1);
			$this->db->order_by("a.no", "asc");
			$hasil = $this->db->get();
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
		
	}
	function menuDetail($idmenu){
		
		$this->load->database();
			$data = array();
			$this->db->select('a.*');
			$this->db->from('menu as a');
			$this->db->where('a.parent', $idmenu);
			$this->db->order_by("a.no", "asc");
			$hasil = $this->db->get();
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
		
}
?>