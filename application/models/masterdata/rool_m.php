<?php
Class rool_m extends CI_Model{


    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'id_rol';
			}
			$this->db->select('rol.*');
			$this->db->from('rol');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
		
            $result = array();
			
			$this->db->select('rol.*,id_rol as id');
			$this->db->from('rol');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 $hasil = $this->db->get();
			 
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
		

function simpanData($id_rol,$data){
		$status=$this->db->insert('rol', $data);
			if(!$status) return false;
			else return true;
	}
	
function editData($id_rol,$data){
		$this->db->where('id_rol', $id_rol);
		$status=$this->db->update('rol', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}
	
function simpanDatadet($data){
		
		 
		$status=$this->db->insert('rol_menu', $data);
			if(!$status) return false;
			else return true;
	}
		
function simpanDatadetdua($rolid,$id_menu,$tampildet,$simpan,$ubah,$hapus,$approve,$cetak,$aktivasi){
		$data=array(
		 'id_rol'=>$rolid,
		 'id_menu'=>$id_menu,
		  'tampil'=>$tampildet,
		  'simpan'=>$simpan,
		  'ubah'=>$ubah,
		  'hapus'=>$hapus,
		  'approve'=>$approve,
		  'cetak'=>$cetak,
		  'aktivasi'=>$aktivasi
		 );
		 
		$status=$this->db->insert('rol_menu', $data);
			if(!$status) return false;
			else return true;
	}	
	

function hapusRoolmenu($rolid){
	$this->db->where('id_rol', $rolid);
	$status=$this->db->delete('rol_menu'); 
	if(!$status) return false;
		else return true;
}	

	function aktivData($id){
		
		$this->db->select('rol.status');
		$this->db->from('rol');
		$this->db->where('id_rol',$id);	
		$query = $this->db->get();
		
		$row = $query->row(); 
		$statusda= $row->status;
		 if($statusda==0){
		 	$setdata=1;
		 }else{
		 $setdata=0;
		 }
		 
		 $data=array(
		 'status'=>$setdata);
		 $this->db->where('id_rol', $id);
		 $status=$this->db->update('rol',$data); 
		 
	if(!$status) return false;
		else return true;
}	

function getMenuutamaedit($id_rol){
	$data = array();
			$this->db->select('a.*');
			$this->db->from('rol_menu as a');
			$this->db->join('menu as b ', 'a.id_menu = b.id');
			
			$this->db->where('b.parent',0);
			$this->db->where('a.id_rol', $id_rol);
			
			$this->db->order_by("b.no", "asc");
			$hasil = $this->db->get();
			//echo $this->db->last_query(); exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data; 
}
	
	function menuDetailedit($idmenu,$id_rol){
		//echo $idmenu;
			$data = array();
			$this->db->select('a.*');
			$this->db->from('rol_menu as a');
			$this->db->join('menu as b ', 'a.id_menu = b.id');
			
			$this->db->where('a.id_rol', $id_rol);
			$this->db->where('b.parent', $idmenu);
			$this->db->order_by("b.no", "asc");
			$hasil = $this->db->get();
			//echo $this->db->last_query(); exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
}
?>