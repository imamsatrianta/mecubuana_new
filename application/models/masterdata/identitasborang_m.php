<?php
Class Identitasborang_m extends CI_Model{
	
	
    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('br_identitas_borang as a');
			$this->db->join('ms_jenjangprodi as b ', 'b.id = a.id_jenjang');
			$this->db->join('ms_prodi as c ', 'c.id = a.id_prodi');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select("a.*,c.nm_prodi,b.nm_jenjangprodi",false);
			$this->db->from('br_identitas_borang as a');
			$this->db->join('ms_jenjangprodi as b ', 'b.id = a.id_jenjang');
			$this->db->join('ms_prodi as c ', 'c.id = a.id_prodi');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	

	function simpanData($data){
		$status=$this->db->insert('br_identitas_borang', $data);
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('br_identitas_borang', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('br_identitas_borang'); 
	if(!$status) return false;
		else return true;
	}	
	
	function simpanDatadosen($data){
		$status=$this->db->insert('br_identitas_borang_dosen', $data);
			if(!$status) return false;
			else return true;
	}
	function editDatadosen($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('br_identitas_borang_dosen', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}
	function loaddatadetail($id){
		$data = array();
		$this->db->select("a.*,b.nm_karyawan");
		$this->db->from("br_identitas_borang_dosen as a");
		$this->db->join('ms_karyawan as b ', 'b.id = a.id_dosen');
		$this->db->where('a.id_identitas',$id);
		$hasil = $this->db->get();
		
		
		$rs = $hasil->result(); 
		
		$items = array();
			
		foreach($rs as $row){
			
			array_push($items, $row);
				 
		}
		 echo json_encode($items);
	}
	function hapusdatadosen($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('br_identitas_borang_dosen'); 
	if(!$status) return false;
		else return true;
	}
	
		
}
?>