<?php
Class lapsarmut_m extends CI_Model{

	function loaddataTabel($offset,$limit,$order,$where){
			
	$id_level=$this->session->userdata('id_level');
	$id_unitses=$this->session->userdata('id_unit');
	$id_direktorat=$this->session->userdata('id_direktorat');
	$id_prodi=$this->session->userdata('id_prodi');
	$kode_approve=$this->session->userdata('kode_approve');
	//echo $kode_approve;exit;
	
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_sarmut as a');
			$this->db->join('tahun_akademik as b ', 'a.id_tahunakademik = b.id');
			//$this->db->join('sp_sarmut_group as c ', 'a.id_group = c.id');
			$this->db->join('sp_mkp_kegiatan as d ', 'a.id_kegiatan_mkp = d.id','left');
			$this->db->where($where);
			if($id_level=="3"){
				$jenis=2;
				$id_unit=$id_direktorat;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}else if($id_level=="4"){
				$jenis=1;
				$id_unit=$id_unitses;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}else if($id_level=="5"){
				$jenis=3;
				$id_unit=$id_prodi;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*,b.nm_tahun_akademik,b.keterangan,d.no');
			$this->db->from('sp_sarmut as a');
			$this->db->join('tahun_akademik as b ', 'a.id_tahunakademik = b.id');
			//$this->db->join('sp_sarmut_group as c ', 'a.id_group = c.id');
			$this->db->join('sp_mkp_kegiatan as d ', 'a.id_kegiatan_mkp = d.id','left');
			$this->db->where($where);
			if($id_level=="3"){
				$jenis=2;
				$id_unit=$id_direktorat;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}else if($id_level=="4"){
				$jenis=1;
				$id_unit=$id_unitses;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}else if($id_level=="5"){
				$jenis=3;
				$id_unit=$id_prodi;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
				//echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			  $oData = new stdClass;
			foreach($rs as $row){
				$id=$row->id;
				$id_unit=$row->id_unit;
				$jenis=$row->jenis;
				$nmunit=$this->getUnit($id_unit,$jenis);
				$oData->nm_unit  = $nmunit;
			
				$obj_merged = (object) array_merge((array) $oData, (array) $row);
				
				//print_r($obj_merged);exit;
				 array_push($items, $obj_merged);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	
	function dataKegiatanunitsarmut($id){
		$sql="select a.id_unit,a.jenis, 
CASE a.jenis when '1'
THEN
(SELECT nm_unit from ms_unit WHERE id=a.id_unit)
when '2'
then
(SELECT nm_direktorat from ms_direktorat WHERE id=a.id_unit)
when '3'
then
(SELECT nm_prodi from ms_prodi WHERE id=a.id_unit)
end
 as nm_unit
 FROM sp_sarmut_unitdet as a where a.id_sarmut='$id'";
 $hasil = $this->db->query($sql);
		
		//echo $this->db->last_query(); exit;
		$rs = $hasil->result();
		return $rs;
	}
	
	function dataStandar($id){
		$sql="select a.id_standar,b.nm_dokumen
		FROM sp_mkp_standar as a 
		inner join sp_standar as b on a.id_standar=b.id where a.id_mkp='$id'";
		$hasil = $this->db->query($sql);
		
		//echo $this->db->last_query(); exit;
		$rs = $hasil->result();
		return $rs;
	}
	
	
	function getUnit($id_unit,$jenis){
		if($jenis=="1"){
			$sqldata = "SELECT nm_unit FROM ms_unit  WHERE  id ='$id_unit' ";
		}else if($jenis=="2"){
			$sqldata = "SELECT nm_direktorat as nm_unit FROM ms_direktorat  WHERE  id ='$id_unit' ";
		}else if($jenis=="3"){
			$sqldata = "SELECT nm_prodi as nm_unit FROM ms_prodi  WHERE  id ='$id_unit' ";
		}
		
		$query = $this->db->query($sqldata);
		$rowa = $query->row(); 
		$nm_unit=$rowa->nm_unit;
		return $nm_unit;
	}
	function getTahunakademik($id_tahunakademi){
		$sqldata = "SELECT nm_tahun_akademik FROM tahun_akademik  WHERE  id ='$id_tahunakademi' ";
		$query = $this->db->query($sqldata);
		$rowa = $query->row(); 
		$nm_tahun_akademik=$rowa->nm_tahun_akademik;
		return $nm_tahun_akademik;
	}
	
	function getSarmutstandar($id_tahunakademi,$id_unit,$jenis){
		$data = array();
			
			$this->db->select('a.id_mkp');
			$this->db->from('sp_sarmut as a');
			//$this->db->join('sp_mkp as b ', 'a.id_mkp = b.id');
			$this->db->where('a.id_tahunakademik',$id_tahunakademi);
			//$this->db->where('a.id_unit',$id_unit);
			//$this->db->where('a.jenis',$jenis);
			$this->db->group_by('id_mkp'); 
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
			$rs = $hasil->result(); 
			 $items = array();
			 $oData = new stdClass;
			foreach($rs as $row){
				$id_mkp=$row->id_mkp;
				$standar=$this->dataStandar($id_mkp);
				//print_r();
				$nm_dokumen=array();
				$id_standar=array();
				foreach($standar as $stan){
					$id_standar[]=$stan->id_standar;
					$nm_dokumen[]=$stan->nm_dokumen;
				}
				$id_stan=implode(',',$id_standar);
				$nmstan=implode(',',$nm_dokumen);
				
				$oData->id_standar  = $id_stan;
				$oData->nm_dokumen  = $nmstan;
				
				$obj_merged = (object) array_merge((array) $oData, (array) $row);
				 array_push($items, $obj_merged);
			}
			//print_r($items);
			return $items;
			//echo $this->db->last_query();exit;
			/*if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;*/
	}
	
	function getSarmut($id_tahunakademi,$id_unit,$jenis,$id_mkp){
		$data = array();
			
			$this->db->select('a.*');
			$this->db->from('sp_sarmut as a');
			
			$this->db->where('a.id_tahunakademik',$id_tahunakademi);
			//$this->db->where('a.id_unit',$id_unit);
			//$this->db->where('a.jenis',$jenis);
			$this->db->where('a.id_mkp',$id_mkp);
			$this->db->order_by("a.id", "asc");
			//$this->db->limit(2, 0);
			$hasil = $this->db->get();
			
			$rs = $hasil->result(); 
			 $items = array();
			 $oData = new stdClass;
			foreach($rs as $row){
				$nmunit=$this->getUnit($id_unit,$jenis);
				$oData->nm_unit  = $nmunit;
				$obj_merged = (object) array_merge((array) $oData, (array) $row);
				array_push($items, $obj_merged);
			}
			return $items;
	}
	

	
	function getSarmutdua($id){
		$data = array();
			
			$this->db->select('a.*');
			$this->db->from('sp_sarmut_kegiatan as a');
			$this->db->where('a.id_sarmut',$id);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	function getSarmutrel($iddet){
		$data = array();
			
			$this->db->select('a.*');
			$this->db->from('sp_sarmut_realisasi as a');
			$this->db->where('a.id_sarmut_kegiatan',$iddet);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
		
}
?>