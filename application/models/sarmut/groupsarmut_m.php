<?php
Class groupsarmut_m extends CI_Model{
    function loaddataTabel($offset,$limit,$order,$where){
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_sarmut_group as a');
			$this->db->join('tahun_akademik as b ', 'b.id = a.id_tahunakademik');
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*,b.nm_tahun_akademik');
			$this->db->from('sp_sarmut_group as a');
			$this->db->join('tahun_akademik as b ', 'b.id = a.id_tahunakademik');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			 $oData = new stdClass;
			foreach($rs as $row){
				$id=$row->id;
				$standar=$this->dataStandar($id);
				$nm_dokumen=array();
				$id_standar=array();
				foreach($standar as $stan){
					$id_standar[]=$stan->id_standar;
					$nm_dokumen[]=$stan->nm_dokumen;
				}
				$id_stan=implode(',',$id_standar);
				$nmstan=implode(',',$nm_dokumen);
				
				$oData->id_standar  = $id_stan;
				$oData->nm_dokumen  = $nmstan;
				$obj_merged = (object) array_merge((array) $oData, (array) $row);
				 array_push($items, $obj_merged);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	
	function dataStandar($id){
		$sql="select a.id_standar,b.nm_dokumen
		FROM sp_sarmut_standar as a 
		inner join sp_standar as b on a.id_standar=b.id where a.id_group='$id'";
		$hasil = $this->db->query($sql);
		
		//echo $this->db->last_query(); exit;
		$rs = $hasil->result();
		return $rs;
	}
	
	function simpanData($data){
		$status=$this->db->insert('sp_sarmut_group', $data);
			if(!$status) return false;
			else return true;
	}
	function simpanDatastandar($datadetdua){
		$status=$this->db->insert('sp_sarmut_standar', $datadetdua);
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('sp_sarmut_group', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function hapusData($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('sp_sarmut_group'); 
	if(!$status) return false;
		else return true;
	}

	function hapusDatastandar($iddet){
		$this->db->where('id_group', $iddet);
	$status=$this->db->delete('sp_sarmut_standar'); 
	if(!$status) return false;
		else return true;
	}
	
	
		
}
?>