<?php
Class appsarmut_m extends CI_Model{

	function loaddataTabel($offset,$limit,$order,$where){
   $id_level=$this->session->userdata('id_level');
  $approve=$this->dataApprove();
  //ECHO  $id_level;
  $appdata=array();
  foreach($approve as $app){
		$appdata[]="'".$app->kode_approve."'";
	
	}
	$kode_approve=implode(',',$appdata);
//	print_r($kode_approve);
	//exit;
	$wheredua="((LEFT(kode_approvel,2) in($kode_approve) AND level_app='0') or (SUBSTRING(kode_approvel,3,2)in($kode_approve) AND level_app='1') or
 (SUBSTRING(kode_approvel,5,2) in($kode_approve) AND level_app='2') or (SUBSTRING(kode_approvel,7,2) in($kode_approve) AND level_app='4') or 
(SUBSTRING(kode_approvel,9,2) in($kode_approve) AND level_app='5')

)";
	//echo $kode_approve;exit;
	
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_sarmut as a');
			$this->db->join('tahun_akademik as b ', 'a.id_tahunakademik = b.id');
			$this->db->join('sp_mkp as c ', 'a.id_mkp = c.id');
			$this->db->join('sp_mkp_kegiatan as d ', 'a.id_kegiatan_mkp = d.id','left');
			$this->db->where($where);
			if($id_level!="1"){
				$this->db->where($wheredua);
			}
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select("a.*,c.nm_mkp,b.nm_tahun_akademik,b.keterangan,d.no,CASE a.level_app WHEN '0' THEN 
LEFT(kode_approvel,2)
WHEN '1' THEN
SUBSTRING(kode_approvel,3,2)
WHEN '2' THEN
SUBSTRING(kode_approvel,5,2)
WHEN '3' THEN
SUBSTRING(kode_approvel,7,2)
WHEN '4' THEN
SUBSTRING(kode_approvel,9,2)
else 
''
END AS kodeapp,
CASE a.level_app WHEN '0' THEN 
SUBSTRING(kode_approvel,3,2)
WHEN '1' THEN
SUBSTRING(kode_approvel,5,2)
WHEN '2' THEN
SUBSTRING(kode_approvel,7,2)
WHEN '3' THEN
SUBSTRING(kode_approvel,9,2)
else 
''
END AS kodeappemail",false);
			$this->db->from('sp_sarmut as a');
			$this->db->join('tahun_akademik as b ', 'a.id_tahunakademik = b.id');
			$this->db->join('sp_mkp as c ', 'a.id_mkp = c.id');
			$this->db->join('sp_mkp_kegiatan as d ', 'a.id_kegiatan_mkp = d.id','left');
			$this->db->where($where);
			if($id_level!="1"){  // echo $wheredua;
				$this->db->where($wheredua);
			}
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			  $oData = new stdClass;
			foreach($rs as $row){
				$id=$row->id;
			
				//print_r($obj_merged);exit;
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	
		function dataKegiatanunitsarmut($id){
		$sql="select a.id_unit,a.jenis, 
CASE a.jenis when '1'
THEN
(SELECT nm_unit from ms_unit WHERE id=a.id_unit)
when '2'
then
(SELECT nm_direktorat from ms_direktorat WHERE id=a.id_unit)
when '3'
then
(SELECT nm_prodi from ms_prodi WHERE id=a.id_unit)
end
 as nm_unit
 FROM sp_sarmut_unitdet as a where a.id_sarmut='$id'";
 $hasil = $this->db->query($sql);
		
		//echo $this->db->last_query(); exit;
		$rs = $hasil->result();
		return $rs;
	}
	
	function dataApprove(){
		$id_karyawan=$this->session->userdata('id_karyawan');
		$sql="select a.kode_approve FROM ms_karyawan_app as a where a.id_karyawan='$id_karyawan'";
		 $hasil = $this->db->query($sql);
		
		//echo $this->db->last_query(); exit;
		$rs = $hasil->result();
		return $rs;
		
	}
	

	function approveData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('sp_sarmut', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function getBy_ArrayID($ID = array()){
		 
		if(count($ID)){
			$data = array();
			$this->db->select("id,kode_approvel_rel,level_app,user_app,CASE level_app WHEN '0' THEN 
LEFT(kode_approvel,2)
WHEN '1' THEN
SUBSTRING(kode_approvel,3,2)
WHEN '2' THEN
SUBSTRING(kode_approvel,5,2)
WHEN '3' THEN
SUBSTRING(kode_approvel,7,2)
WHEN '4' THEN
SUBSTRING(kode_approvel,9,2)
else 
''
END AS kodeapp",false);
  		$this->db->from("sp_sarmut");
  		$this->db->where_in('id',$ID);
  		$Q = $this->db->get('');
		// echo $this->db->last_query();exit;
  		if($Q->num_rows() > 0){
  			foreach ($Q->result_array() as $row){
  				$data[] = $row;
  	  	}
  		}
    	$Q->free_result();
    	return $data;
		}
	}
	
	function  simpanapproveData($data){
		$status=$this->db->insert('sp_sarmut_app', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
			
	}
		
}
?>