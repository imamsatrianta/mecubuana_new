<?php
Class apprealisasisarmut_m extends CI_Model{

	function loaddataTabel($offset,$limit,$order,$where){
   $id_level=$this->session->userdata('id_level');
 $approve=$this->dataApprove();
  //ECHO  $id_level;
  $appdata=array();
  foreach($approve as $app){
		$appdata[]="'".$app->kode_approve."'";
	
	}
	$kode_approve=implode(',',$appdata);
	
	$wheredua="((LEFT(a.kode_approvel,2) in($kode_approve) AND a.level_app='0') or (SUBSTRING(a.kode_approvel,3,2)in($kode_approve) AND a.level_app='1') or
 (SUBSTRING(a.kode_approvel,5,2) in($kode_approve) AND a.level_app='2') or (SUBSTRING(a.kode_approvel,7,2) in($kode_approve) AND a.level_app='4') or 
(SUBSTRING(a.kode_approvel,9,2) in($kode_approve) AND a.level_app='5')

)";
	//echo $kode_approve;exit;
	
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_sarmut_realisasi as a');
			$this->db->join('sp_sarmut_kegiatan as b ', 'a.id_sarmut_kegiatan = b.id');
			$this->db->join('sp_sarmut as c ', 'c.id = b.id_sarmut');
			$this->db->where($where);
			if($id_level!="1"){
				$this->db->where($wheredua);
			}
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select("a.*,b.uraian_kegiatan,b.tgl_mulai,b.tgl_selesai,CASE a.level_app WHEN '0' THEN 
LEFT(a.kode_approvel,2)
WHEN '1' THEN
SUBSTRING(a.kode_approvel,3,2)
WHEN '2' THEN
SUBSTRING(a.kode_approvel,5,2)
WHEN '3' THEN
SUBSTRING(a.kode_approvel,7,2)
WHEN '4' THEN
SUBSTRING(a.kode_approvel,9,2)
else 
''
END AS kodeapp,
CASE a.level_app WHEN '0' THEN 
SUBSTRING(a.kode_approvel,3,2)
WHEN '1' THEN
SUBSTRING(a.kode_approvel,5,2)
WHEN '2' THEN
SUBSTRING(a.kode_approvel,7,2)
WHEN '3' THEN
SUBSTRING(a.kode_approvel,9,2)
else 
''
END AS kodeappemail",false);
			$this->db->from('sp_sarmut_realisasi as a');
			$this->db->join('sp_sarmut_kegiatan as b ', 'a.id_sarmut_kegiatan = b.id');
			$this->db->join('sp_sarmut as c ', 'c.id = b.id_sarmut');
			
			$this->db->where($where);
			if($id_level!="1"){
				$this->db->where($wheredua);
			}
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
				//echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				$id=$row->id;
				//print_r($obj_merged);exit;
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	
		
	
	function dataApprove(){
		$id_karyawan=$this->session->userdata('id_karyawan');
		$sql="select a.kode_approve FROM ms_karyawan_app as a where a.id_karyawan='$id_karyawan'";
		 $hasil = $this->db->query($sql);
		
		//echo $this->db->last_query(); exit;
		$rs = $hasil->result();
		return $rs;
		
	}
	

	function approveData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('sp_sarmut_realisasi', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	
	function getBy_ArrayID($ID = array()){
		 
		if(count($ID)){
			$data = array();
			$this->db->select("id,kode_approvel_rel,level_app,user_app,CASE level_app WHEN '0' THEN 
LEFT(kode_approvel,2)
WHEN '1' THEN
SUBSTRING(kode_approvel,3,2)
WHEN '2' THEN
SUBSTRING(kode_approvel,5,2)
WHEN '3' THEN
SUBSTRING(kode_approvel,7,2)
WHEN '4' THEN
SUBSTRING(kode_approvel,9,2)
else 
''
END AS kodeapp",false);
  		$this->db->from("sp_sarmut_realisasi");
  		$this->db->where_in('id',$ID);
  		$Q = $this->db->get('');
		// echo $this->db->last_query();exit;
  		if($Q->num_rows() > 0){
  			foreach ($Q->result_array() as $row){
  				$data[] = $row;
  	  	}
  		}
    	$Q->free_result();
    	return $data;
		}
	}
	
	function  simpanapproveData($data){
		$status=$this->db->insert('sp_sarmut_realisasi_app', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
			
	}
		
}
?>