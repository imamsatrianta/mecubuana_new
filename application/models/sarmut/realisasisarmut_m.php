<?php
Class realisasisarmut_m extends CI_Model{
	function loaddatakegiatan($offset,$limit,$order,$where){
		$id_level=$this->session->userdata('id_level');
		$id_unitses=$this->session->userdata('id_unit');
		$id_direktorat=$this->session->userdata('id_direktorat');
		$id_prodi=$this->session->userdata('id_prodi');
	
		if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_sarmut_kegiatan as a');
			$this->db->join('sp_sarmut as b ', 'b.id = a.id_sarmut');
			$this->db->where($where);
			if($id_level=="3"){
				$jenis=2;
				$id_unit=$id_direktorat;
				$this->db->where('b.id_unit',$id_unit);
				$this->db->where('b.jenis',$jenis);
			}else if($id_level=="4"){
				$jenis=1;
				$id_unit=$id_unitses;
				$this->db->where('b.id_unit',$id_unit);
				$this->db->where('b.jenis',$jenis);
			}else if($id_level=="5"){
				$jenis=3;
				$id_unit=$id_prodi;
				$this->db->where('b.id_unit',$id_unit);
				$this->db->where('b.jenis',$jenis);
			}
			
			
			//$this->db->where('c.id_standar',$id_standar);
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*');
			$this->db->from('sp_sarmut_kegiatan as a');
			$this->db->join('sp_sarmut as b ', 'b.id = a.id_sarmut');
			$this->db->where($where);
			if($id_level=="3"){
				$jenis=2;
				$id_unit=$id_direktorat;
				$this->db->where('b.id_unit',$id_unit);
				$this->db->where('b.jenis',$jenis);
			}else if($id_level=="4"){
				$jenis=1;
				$id_unit=$id_unitses;
				$this->db->where('b.id_unit',$id_unit);
				$this->db->where('b.jenis',$jenis);
			}else if($id_level=="5"){
				$jenis=3;
				$id_unit=$id_prodi;
				$this->db->where('b.id_unit',$id_unit);
				$this->db->where('b.jenis',$jenis);
			}
			//$this->db->where('c.id_standar',$id_standar);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
	}
	
	
	
    function loaddataTabel($offset,$limit,$order,$where){
			$id_level=$this->session->userdata('id_level');
		$id_unitses=$this->session->userdata('id_unit');
		$id_direktorat=$this->session->userdata('id_direktorat');
		$id_prodi=$this->session->userdata('id_prodi');
		
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_sarmut_realisasi as a');
			$this->db->join('sp_sarmut_kegiatan as b ', 'a.id_sarmut_kegiatan = b.id');
			$this->db->join('sp_sarmut as c ', 'c.id = b.id_sarmut');
			$this->db->where($where);
			if($id_level=="3"){
				$jenis=2;
				$id_unit=$id_direktorat;
				$this->db->where('c.id_unit',$id_unit);
				$this->db->where('c.jenis',$jenis);
			}else if($id_level=="4"){
				$jenis=1;
				$id_unit=$id_unitses;
				$this->db->where('c.id_unit',$id_unit);
				$this->db->where('c.jenis',$jenis);
			}else if($id_level=="5"){
				$jenis=3;
				$id_unit=$id_prodi;
				$this->db->where('c.id_unit',$id_unit);
				$this->db->where('c.jenis',$jenis);
			}
			
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*,b.uraian_kegiatan,b.tgl_mulai,b.tgl_selesai');
			$this->db->from('sp_sarmut_realisasi as a');
			$this->db->join('sp_sarmut_kegiatan as b ', 'a.id_sarmut_kegiatan = b.id');
			$this->db->join('sp_sarmut as c ', 'c.id = b.id_sarmut');
			$this->db->where($where);
			if($id_level=="3"){
				$jenis=2;
				$id_unit=$id_direktorat;
				$this->db->where('c.id_unit',$id_unit);
				$this->db->where('c.jenis',$jenis);
			}else if($id_level=="4"){
				$jenis=1;
				$id_unit=$id_unitses;
				$this->db->where('c.id_unit',$id_unit);
				$this->db->where('c.jenis',$jenis);
			}else if($id_level=="5"){
				$jenis=3;
				$id_unit=$id_prodi;
				$this->db->where('c.id_unit',$id_unit);
				$this->db->where('c.jenis',$jenis);
			}
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			 $oData = new stdClass;
			foreach($rs as $row){
				
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	

	
	function simpanData($data){
	//	print_r($data);exit;
		$status=$this->db->insert('sp_sarmut_realisasi', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function editData($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('sp_sarmut_realisasi', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}	
	function simpanDataunit($datadetdua){
		$status=$this->db->insert('sp_sarmut_realisasi_unitdet', $datadetdua);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	
	
	function hapusData($id){
		$this->db->where('id', $id);
		$status=$this->db->delete('sp_sarmut_realisasi'); 
		if(!$status) return false;
		else return true;
	}
	function hapusDataunit($id){
		$this->db->where('id_sarmut_realisasi', $id);
		$status=$this->db->delete('sp_sarmut_realisasi_unitdet'); 
		if(!$status) return false;
		else return true;
	}
	
	function cekPresenkegiatan($id_sarmut_kegiatan){
		$sqldata = "SELECT IFNULL(presentase ,0) AS presentase FROM sp_sarmut_kegiatan  WHERE  id = '$id_sarmut_kegiatan' ";
		$query = $this->db->query($sqldata);
		$rowa = $query->row(); 
		$presentase=$rowa->presentase;
       
        return $presentase;
	}
	
	function editDatakegiatan($id_sarmut_kegiatan,$jmlpersen){
		$sqldata = "update  sp_sarmut_kegiatan set presentase='$jmlpersen'  WHERE  id = '$id_sarmut_kegiatan' ";
		$query = $this->db->query($sqldata);
		if(!$query) return false;
		else return true;
	}
	
	function getAksesmenu($jenis,$id_unit){
		 $sqldata = "SELECT kode_approvel_realisasi FROM sp_sarmut_akses  WHERE  id_unit = '$id_unit' and jenis = '$jenis' ";
		$query = $this->db->query($sqldata);
		$rowa = $query->row(); 
		$kode_approvel=$rowa->kode_approvel_realisasi;
		return $kode_approvel;
	}
	
	
	
		
}
?>