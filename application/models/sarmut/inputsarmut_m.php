<?php
Class inputsarmut_m extends CI_Model{
	function loaddatamkp($offset,$limit,$order,$where,$id_mkp){
		$id_direktorat=$this->session->userdata('id_direktorat');
		$id_unit=$this->session->userdata('id_unit');
		$id_prodi=$this->session->userdata('id_prodi');
		$id_level=$this->session->userdata('id_level');
		
		//$idstn=(string)$id_standar;
		//$wherein="c.id_mkp in ($id_mkp)";
		if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_mkp_kegiatan as a');
			$this->db->join('sp_mkp_target as b ', 'a.id_mkp_target = b.id');
		//	$this->db->join('sp_mkp_standar as c ', 'b.id_mkp = c.id_mkp');
			
			$this->db->where($where);
			if($id_level=="3"){
				$jenis=2;
				$id_unit=$id_direktorat;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}else if($id_level=="4"){
				$jenis=1;
				$id_unit=$id_unitses;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}else if($id_level=="5"){
				$jenis=3;
				$id_unit=$id_prodi;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}
			
			$this->db->where('b.id_mkp',$id_mkp);
			//$this->db->where($wherein);
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*,b.uraian_target');
			$this->db->from('sp_mkp_kegiatan as a');
			$this->db->join('sp_mkp_target as b ', 'a.id_mkp_target = b.id');
			//$this->db->join('sp_mkp_standar as c ', 'b.id_mkp = c.id_mkp');
			$this->db->where($where);
			if($id_level=="3"){
				$jenis=2;
				$id_unit=$id_direktorat;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}else if($id_level=="4"){
				$jenis=1;
				$id_unit=$id_unitses;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}else if($id_level=="5"){
				$jenis=3;
				$id_unit=$id_prodi;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}
			//$this->db->where($wherein);
			$this->db->where('b.id_mkp',$id_mkp);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
				//echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
	}
	
	
	
    function loaddataTabel($offset,$limit,$order,$where){
			
	$id_level=$this->session->userdata('id_level');
	$id_unitses=$this->session->userdata('id_unit');
	$id_direktorat=$this->session->userdata('id_direktorat');
	$id_prodi=$this->session->userdata('id_prodi');
	$kode_approve=$this->session->userdata('kode_approve');
	//echo $id_direktorat;
	//echo $kode_approve;exit;
	
			if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_sarmut as a');
			$this->db->join('tahun_akademik as b ', 'a.id_tahunakademik = b.id');
			$this->db->join('sp_mkp as c ', 'a.id_mkp = c.id');
			$this->db->join('sp_mkp_kegiatan as d ', 'a.id_kegiatan_mkp = d.id','left');
			$this->db->where($where);
			if($id_level=="3"){
				$jenis=2;
				$id_unit=$id_direktorat;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}else if($id_level=="4"){
				$jenis=1;
				$id_unit=$id_unitses;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}else if($id_level=="5"){
				$jenis=3;
				$id_unit=$id_prodi;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*,b.nm_tahun_akademik,b.keterangan,d.no,c.nm_mkp');
			$this->db->from('sp_sarmut as a');
			$this->db->join('tahun_akademik as b ', 'a.id_tahunakademik = b.id');
			$this->db->join('sp_mkp as c ', 'a.id_mkp = c.id');
			$this->db->join('sp_mkp_kegiatan as d ', 'a.id_kegiatan_mkp = d.id','left');
			$this->db->where($where);
			if($id_level=="3"){
				$jenis=2;
				$id_unit=$id_direktorat;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}if($id_level=="4"){
				$jenis=1;
				$id_unit=$id_unitses;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}if($id_level=="5"){
				$jenis=3;
				$id_unit=$id_prodi;
				$this->db->where('a.id_unit',$id_unit);
				$this->db->where('a.jenis',$jenis);
			}
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
				//echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			  $oData = new stdClass;
			foreach($rs as $row){
				$id=$row->id;
				$id_unit=$row->id_unit;
				$jenis=$row->jenis;
				$namaunit=$this->dataUnit($id_unit,$jenis);
				$oData->nmunit  = $namaunit;
				$obj_merged = (object) array_merge((array) $oData, (array) $row);
				/*$id_group=$row->id_group;
				
				$standar=$this->dataStandar($id_group);
				$nm_dokumen=array();
				$id_standar=array();
				foreach($standar as $stan){
					$id_standar[]=$stan->id_standar;
					$nm_dokumen[]=$stan->nm_dokumen;
				}
				$id_stan=implode(',',$id_standar);
				$nmstan=implode(',',$nm_dokumen);
				
				$oData->id_standar  = $id_stan;
				$oData->nm_dokumen  = $nmstan;
				
				$obj_merged = (object) array_merge((array) $oData, (array) $row);
				*/
				//print_r($obj_merged);exit;
				 array_push($items, $obj_merged);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
        
    }
	
	function dataUnit($id_unit,$jenis){
		if($jenis=="1"){
			$sql="SELECT nm_unit as unit from ms_unit WHERE id='$id_unit'";
		}else if($jenis=="2"){
			$sql="SELECT nm_direktorat as unit from ms_direktorat WHERE id='$id_unit'";
		}else{
			$sql="SELECT nm_prodi as unit from ms_prodi WHERE id='$id_unit'";
		}
		
		$query = $this->db->query($sql);
		$rowa = $query->row(); 
		$unit=$rowa->unit;
		
		return $unit;
	}
	
	
	
	function simpanData($data){
	//	print_r($data);exit;
		$status=$this->db->insert('sp_sarmut', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	function editDatasarmut($id,$data){
	//	print_r($data);exit;
		$this->db->where('id',$id);
		$status=$this->db->update('sp_sarmut', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
		//if(!$status) return false;
	//	else return true;
	}	
	
	
	
	function hapusData($id){
		$this->db->where('id', $id);
		$status=$this->db->delete('sp_sarmut'); 
		if(!$status) return false;
		else return true;
	}
	
	
	function simpanDakegiatan($datadetdua){
		$status=$this->db->insert('sp_sarmut_kegiatan', $datadetdua);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
	}
	
	function editDatakegiatan($id,$data){
		$this->db->where('id',$id);
		$status=$this->db->update('sp_sarmut_kegiatan', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}
	

	
	
	
	// kegiatan
	
	
	function loaddatadetailkegiatan($id){
		$data = array();
		$this->db->select("a.*",false);
		$this->db->from("sp_sarmut_kegiatan as a");
		$this->db->where('a.id_sarmut',$id);
		$hasil = $this->db->get();
		
		$rs = $hasil->result(); 
		
		$items = array();
			$oData = new stdClass;
			
		foreach($rs as $row){
			
			array_push($items, $row);
				 
		}
		 echo json_encode($items);
		
		
	}
	
	function loaddatadetailkegiatanrel($id){
		$data = array();
		$this->db->select("a.*",false);
		$this->db->from("sp_sarmut_realisasi as a");
		$this->db->where('a.id_sarmut_kegiatan',$id);
		$hasil = $this->db->get();
		//echo $this->db->last_query(); exit;
		$rs = $hasil->result(); 
		
		$items = array();
			$oData = new stdClass;
			
		foreach($rs as $row){
			array_push($items, $row);
				 
		}
		 echo json_encode($items);
		
		
	}
	

	
	

	function hapusdatakegiatan($id){
		$this->db->where('id', $id);
	$status=$this->db->delete('sp_sarmut_kegiatan'); 
	if(!$status) return false;
		else return true;
	}
	
	
	function loaddataTabelkegiatan($offset,$limit,$order,$where){
		$id_level=$this->session->userdata('id_level');
	$id_unitses=$this->session->userdata('id_unit');
	$id_direktorat=$this->session->userdata('id_direktorat');
	$id_prodi=$this->session->userdata('id_prodi');
	
		if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			
			$this->db->select('a.id');
			$this->db->from('sp_sarmut_kegiatan as a');
			$this->db->join('sp_sarmut as b ', 'a.id_sarmut = b.id');
			$this->db->join('tahun_akademik as c ', 'b.id_tahunakademik = c.id');
			$this->db->where($where);
			if($id_level=="3"){
				$jenis=2;
				$id_unit=$id_direktorat;
				$this->db->where('b.id_unit',$id_unit);
				$this->db->where('b.jenis',$jenis);
			}else if($id_level=="4"){
				$jenis=1;
				$id_unit=$id_unitses;
				$this->db->where('b.id_unit',$id_unit);
				$this->db->where('b.jenis',$jenis);
			}else if($id_level=="5"){
				$jenis=3;
				$id_unit=$id_prodi;
				$this->db->where('b.id_unit',$id_unit);
				$this->db->where('b.jenis',$jenis);
			}
			//$this->db->where('c.id_standar',$id_standar);
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select("a.*,b.uraian_sarmut,b.target_kuantitatif,b.target_kualitatif,b.kinerja_lalu,c.nm_tahun_akademik",false);
			$this->db->from('sp_sarmut_kegiatan as a');
			$this->db->join('sp_sarmut as b ', 'a.id_sarmut = b.id');
			$this->db->join('tahun_akademik as c ', 'b.id_tahunakademik = c.id');
			
			if($id_level=="3"){
				$jenis=2;
				$id_unit=$id_direktorat;
				$this->db->where('b.id_unit',$id_unit);
				$this->db->where('b.jenis',$jenis);
			}else if($id_level=="4"){
				$jenis=1;
				$id_unit=$id_unitses;
				$this->db->where('b.id_unit',$id_unit);
				$this->db->where('b.jenis',$jenis);
			}else if($id_level=="5"){
				$jenis=3;
				$id_unit=$id_prodi;
				$this->db->where('b.id_unit',$id_unit);
				$this->db->where('b.jenis',$jenis);
			}
			$this->db->where($where);
			//$this->db->where('c.id_standar',$id_standar);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			foreach($rs as $row){
				 array_push($items, $row);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
	}
	
	function getAksesmenu($jenis,$id_unit){
		 $sqldata = "SELECT kode_approvel FROM sp_sarmut_akses  WHERE  id_unit = '$id_unit' and jenis = '$jenis' ";
		$query = $this->db->query($sqldata);
		$rowa = $query->row(); 
		$kode_approvel=$rowa->kode_approvel;
		return $kode_approvel;
	}
	
	function cekScore(){
		$tgl=date('Y-m-d');
		$tgltime = (int)strtotime($tgl);
			
		
		$kegiatandata=$this->kegiatanSarmut();
		foreach($kegiatandata as $kegiatan){
			$id=$kegiatan->id;
			$presentase=(int)$kegiatan->presentase;
			$tgl_mulai=$kegiatan->tgl_mulai;
			$tgl_mulaitime = (int)strtotime($tgl_mulai);
			$tgl_selesai=$kegiatan->tgl_selesai;
			$tgl_selesaitime = (int)strtotime($tgl_selesai);
			
			$score=$kegiatan->score;
			if($tgl_mulaitime>$tgltime and $presentase==0){
				$score=0;
			//	echo $id.",";
				$this->updateKegiatan($id,$score,$presentase);
				$this->simpanStatuskegiatan($id,$score);
				
			}else if($tgl_mulaitime<$tgltime and $tgl_selesaitime>$tgltime and $presentase==0){
				$score=2;
				//echo $id.",";
				$this->updateKegiatan($id,$score,$presentase);
				$this->simpanStatuskegiatan($id,$score);
				
			}else if($tgl_selesaitime<$tgltime and $presentase==0){
				$score=1;
				//echo $id.",";
				$this->updateKegiatan($id,$score,$presentase);
				$this->simpanStatuskegiatan($id,$score);
				
			}else if($tgl_mulaitime<$tgltime and $tgl_selesaitime>$tgltime and $presentase==100){
				$score=6;
				//echo $id.",";
				$this->updateKegiatan($id,$score,$presentase);
				$this->simpanStatuskegiatan($id,$score);
				
			}else if($tgl_selesaitime<$tgltime and $presentase==100){
				$score=5;
				//echo $id.",";
				$this->updateKegiatan($id,$score,$presentase);
				$this->simpanStatuskegiatan($id,$score);
				
			}else if($tgl_mulaitime<$tgltime and $tgl_selesaitime>$tgltime and ($presentase>1 and $presentase<100)){
				$score=4;
			//	echo $id.",";
				$this->updateKegiatan($id,$score,$presentase);
				$this->simpanStatuskegiatan($id,$score);
				
			}else if($tgl_selesaitime<$tgltime and ($presentase>1 and $presentase<100)){
				$score=3;
				//echo $id.",";
				$this->updateKegiatan($id,$score,$presentase);
				$this->simpanStatuskegiatan($id,$score);
				
			}
			
					//echo $tgl_mulai;
		}
	}
	
	function kegiatanSarmut(){
		$tgl=date('Y-m-d');
		$data = array();
			
			$this->db->select('*');
			$this->db->from('sp_sarmut_kegiatan');
			$this->db->where('status_kegiatan',0);
			$hasil = $this->db->get();
		//	echo $this->db->last_query(); exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
			
	}
	
	function updateKegiatan($id,$score,$presentase){
		if($presentase=="100"){
			$data=array(
			'score'=>$score,
			'status_kegiatan'=>1);
		}else{
			$data=array(
			'score'=>$score);
		}
		
		$this->db->where('id',$id);
		$status=$this->db->update('sp_sarmut_kegiatan', $data);
	///	echo $this->db->last_query(); 
		if(!$status) return false;
		else return true;
	}
	
	function simpanStatuskegiatan($id,$score){
		$tgl=date('Y-m-d');
		$data=array(
		'score_kegiatan'=>$score,
		'id_kegiatan_sarmut'=>$id,
		'tgl'=>$tgl);
		
		$status=$this->db->insert('sp_sarmut_kegiatan_status', $data);
		//echo $this->db->last_query(); exit;
			if(!$status) return false;
			else return true;
			
			
	}
	function getUnit($id_unit,$jenis){
		if($jenis=="1"){
			$sqldata = "SELECT nm_unit FROM ms_unit  WHERE  id ='$id_unit' ";
		}else if($jenis=="2"){
			$sqldata = "SELECT nm_direktorat as nm_unit FROM ms_direktorat  WHERE  id ='$id_unit' ";
		}else if($jenis=="3"){
			$sqldata = "SELECT nm_prodi as nm_unit FROM ms_prodi  WHERE  id ='$id_unit' ";
		}
		
		$query = $this->db->query($sqldata);
		$rowa = $query->row(); 
		$nm_unit=$rowa->nm_unit;
		return $nm_unit;
	}
	function getTahunakademik($id_tahunakademi){
		$sqldata = "SELECT nm_tahun_akademik FROM tahun_akademik  WHERE  id ='$id_tahunakademi' ";
		$query = $this->db->query($sqldata);
		$rowa = $query->row(); 
		$nm_tahun_akademik=$rowa->nm_tahun_akademik;
		return $nm_tahun_akademik;
	}
	
	function getSarmutstandar($id_tahunakademi,$id_unit,$jenis){
		$data = array();
			
			$this->db->select('a.id_mkp');
			$this->db->from('sp_sarmut as a');
			//$this->db->join('sp_mkp as b ', 'a.id_mkp = b.id');
			$this->db->where('a.id_tahunakademik',$id_tahunakademi);
			$this->db->where('a.id_unit',$id_unit);
			$this->db->where('a.jenis',$jenis);
			$this->db->group_by('id_mkp'); 
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
			$rs = $hasil->result(); 
			 $items = array();
			 $oData = new stdClass;
			foreach($rs as $row){
				$id_mkp=$row->id_mkp;
				$standar=$this->dataStandar($id_mkp);
				//print_r();
				$nm_dokumen=array();
				$id_standar=array();
				foreach($standar as $stan){
					$id_standar[]=$stan->id_standar;
					$nm_dokumen[]=$stan->nm_dokumen;
				}
				$id_stan=implode(',',$id_standar);
				$nmstan=implode(',',$nm_dokumen);
				
				$oData->id_standar  = $id_stan;
				$oData->nm_dokumen  = $nmstan;
				
				$obj_merged = (object) array_merge((array) $oData, (array) $row);
				 array_push($items, $obj_merged);
			}
			//print_r($items);
			return $items;
			//echo $this->db->last_query();exit;
			/*if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;*/
	}
	
	function getSarmut($id_tahunakademi,$id_unit,$jenis,$id_mkp){
		$data = array();
			
			$this->db->select('a.*');
			$this->db->from('sp_sarmut as a');
			
			$this->db->where('a.id_tahunakademik',$id_tahunakademi);
			$this->db->where('a.id_unit',$id_unit);
			$this->db->where('a.jenis',$jenis);
			$this->db->where('a.id_mkp',$id_mkp);
			$this->db->order_by("a.id", "asc");
			//$this->db->limit(2, 0);
			$hasil = $this->db->get();
			
			$rs = $hasil->result(); 
			 $items = array();
			 $oData = new stdClass;
			foreach($rs as $row){
				$nmunit=$this->getUnit($id_unit,$jenis);
				$oData->nm_unit  = $nmunit;
				$obj_merged = (object) array_merge((array) $oData, (array) $row);
				array_push($items, $obj_merged);
			}
			return $items;
			
			
			/*
		//	echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
				//$nmunit=$this->getUnit($id_unit,$jenis);
			$data = $hasil->result();
			
			}
			
			$hasil->free_result();
                       
			return $data;*/
	}
	
	function getSarmutdua($id){
		$data = array();
			
			$this->db->select("a.*,MONTH(tgl_mulai)as awal,TIMESTAMPDIFF(MONTH,tgl_mulai,tgl_selesai) AS jumlah",false);
			$this->db->from('sp_sarmut_kegiatan as a');
			$this->db->where('a.id_sarmut',$id);
			$this->db->order_by("a.id", "asc");
			$hasil = $this->db->get();
			//echo $this->db->last_query();exit;
			if($hasil->num_rows() > 0){
			$data = $hasil->result();
			}
			
			$hasil->free_result();
                       
			return $data;
	}
	
	
	function loaddatastandar($offset,$limit,$order,$where){
		if (isset($_GET['sort'])) {
			$sort = $this->input->get("sort");
			}else{
				$sort = 'a.id';
			}
			$this->db->select('a.id');
			$this->db->from('sp_mkp as a');
			
			
			$this->db->where($where);
			//$this->db->where('c.id_standar',$id_standar);
			$hasil = $this->db->get();
			$total=$hasil->num_rows();
			
			
			$this->db->select('a.*');
			$this->db->from('sp_mkp as a');
			$this->db->where($where);
			$this->db->order_by($sort, $order);
			 $this->db->limit($limit, $offset);
			 
			$hasil = $this->db->get();
			//	echo $this->db->last_query(); exit;
			$rs = $hasil->result(); 
	
            $result["total"] = $total;
			 $items = array();
			$oData = new stdClass;
			foreach($rs as $row){
				$id=$row->id;
				$standar=$this->dataStandar($id);
				$nm_dokumen=array();
				$id_standar=array();
				foreach($standar as $stan){
					$id_standar[]=$stan->id_standar;
					$nm_dokumen[]=$stan->nm_dokumen;
				}
				$id_stan=implode(',',$id_standar);
				$nmstan=implode(',',$nm_dokumen);
				
				$oData->id_standar  = $id_stan;
				$oData->nm_dokumen  = $nmstan;
				$obj_merged = (object) array_merge((array) $oData, (array) $row);
				 array_push($items, $obj_merged);
			}
			
            $result["rows"] = $items;
            echo json_encode($result);
	}
	
	function dataStandar($id){
		$sql="select a.id_standar,b.nm_dokumen
		FROM sp_mkp_standar as a 
		inner join sp_standar as b on a.id_standar=b.id where a.id_mkp='$id'";
		$hasil = $this->db->query($sql);
		
		//echo $this->db->last_query(); exit;
		$rs = $hasil->result();
		return $rs;
	}
	
		
}
?>