<?php

global $tampil;
global $simpan;
global $ubah;
global $hapus;
global $approve;
global $cetak;
global $aktivasi;
global $parent;
global $id;
global $menudes;


if (isset($_GET['idm'])) {
 $idmen=$_GET['idm'];
 $idokedata =paramDecrypt($idmen);

 backButtonHandle();
$CI =& get_instance();
$CI->load->model('global_m');
$CI->load->library('session');
$id_user=$CI->session->userdata('userid');
$akses=$CI->global_m->getMenuakses($idokedata,$id_user);
//print_r($akses);exit;


if($akses=='0'){
//	redirect(base_url().'ad_log/logout');
}

if (is_object($akses)){

	$tampil=$akses->tampil;
	$simpan=$akses->simpan;
	$ubah=$akses->ubah;
	$hapus=$akses->hapus;
	$approve=$akses->approve;
	$cetak=$akses->cetak;
	$aktivasi=$akses->aktivasi;
	$parent=$akses->parent;
	$id=$akses->idmenu;
	$menudes=$akses->menudes;


function cekParent(){
	global $parent;
	return $parent;
	
	
}	
function cekidaktif(){
	global $id;
	return $id;

}

function callmenudess(){
	global $menudes;
	return $menudes;

}
function aksesTambah(){
	global $simpan;
	$tambah='';
	
	if ($simpan==1){
				
		$tambah='<button id="tbh" type="button" class="btn btn-primary " data-toggle="modal" onclick="tambahData()" >
			  <i class="glyphicon glyphicon-plus"> </i>   Tambah
			</button>   ';
		
	}else{
		$tambah='';
	}
	return $tambah;
	
}
function aksesTambahhtml(){
	global $simpan;
	$tambah='';
	
	if ($simpan==1){
				
		$tambah='<button id="tbh" type="button" class="btn btn-primary " data-toggle="modal" onclick="tambahDatahtml()" >
			  <i class="glyphicon glyphicon-plus"> </i>   Tambah
			</button>   ';
		
	}else{
		$tambah='';
	}
	return $tambah;
	
}

function aksesTambahborang(){
	global $simpan;
	$tambah='';
	
	if ($simpan==1){
				
		$tambah='<button id="tbh" type="button" class="btn btn-primary " data-toggle="modal" onclick="tambahDataborang()" >
			  <i class="glyphicon glyphicon-plus"> </i>   Tambah
			</button>   ';
		
	}else{
		$tambah='';
	}
	return $tambah;
	
}

function aksesTambahupl(){
	global $simpan;
	$tambah='';
	
	if ($simpan==1){
				
		$tambah='<button id="tbh" type="button" class="btn btn-primary " data-toggle="modal" onclick="tambahDataupl()" >
			  <i class="glyphicon glyphicon-plus"> </i>   Tambah
			</button>   ';
		
	}else{
		$tambah='';
	}
	return $tambah;
	
}
function aksesTambahdetail(){
	global $simpan;
	$tambah='';
	
	if ($simpan==1){
				
		$tambah='<button id="tbh" type="button" class="btn btn-primary " data-toggle="modal" onclick="tambahDatadetail()" >
			  <i class="glyphicon glyphicon-plus"> </i>   Tambah
			</button>   ';
		
	}else{
		$tambah='';
	}
	return $tambah;
	
}

function aksesDetail(){
	global $simpan;
	$tambah='';
	
	if ($simpan==1){
				
		$tambah='<button id="tbh" type="button" class="btn btn-primary " data-toggle="modal" onclick="tambahDetail()" >
			  <i class="glyphicon glyphicon-plus"> </i>   Tambah
			</button>   ';
		
	}else{
		$tambah='';
	}
	return $tambah;
	
}
function aksesApproveheder(){
	global $approve;
	$app='';
	
	if ($approve==1){
				
		$app='<button id="appheder" type="button" class="btn btn-success " data-toggle="modal" onclick="appHeder()" >
			  <i class="fa  fa-check-square"> </i>   Approve
			</button>   ';
		
	}else{
		$app='';
	}
	return $app;
	
}

function aksesApprovedetail(){
	global $approve;
	$app='';
	
	if ($approve==1){
				
		$app='<a class="btn btn-sm btn-success btn-xs" id="appdet" href="javascript:void(0)" title="Approve"><i class="fa fa-check-square-o"></i></a>  ';
		
	}else{
		$app='';
	}
	return $app;
	
}


function aksesPrinheder(){
	global $cetak;
	$print='';
	
	if ($cetak==1){
				
		$print='<button id="prinhed" type="button" class="btn btn-info " data-toggle="modal" onclick="printHeder()" >
			  <i class="fa fa-print"> </i>   Print
			</button>   ';
		
	}else{
		$print='';
	}
	return $print;
	
}

function aksesPrindetail($page){
	global $cetak;
	$print='';
	
	if ($cetak==1){
				
		$print='<a class="btn btn-sm btn-info btn-xs" id="printdet" href="./'.$page.'/printDetail/\'+row.id+\'" target="_blank" title="Print"><i class="fa fa-print"></i></a>  ';
		
	}else{
		$print='';
	}
	return $print;
	
}


function aksesUbah(){
	global $ubah;
	$ubahda='';
	
	if ($ubah==1){
				
		$ubahda='<a class="btn btn-sm btn-primary btn-xs" id="edit" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Ubah" ><i id="edt" class="glyphicon glyphicon-edit" ></i></a>  ';
		
	}else{
		$ubahda='';
	}
	return $ubahda;
	
}
function aksesLihat(){
	global $tampil;
	$tampilda='';
	
	if ($tampil==1){
				
		$tampilda='<a class="btn btn-sm btn-primary btn-xs" id="lihat" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Lihat" ><i id="lht" class="glyphicon glyphicon-eye-open" ></i></a>  ';
		
	}else{
		$tampilda='';
	}
	return $tampilda;
	
}

function aksesUbahdua(){
	global $ubah;
	$ubahda='';
	
	if ($ubah==1){
				
		$ubahda='<a class="btn btn-sm btn-primary btn-xs" id="editupl" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Ubah Data" ><i class="glyphicon glyphicon-edit" ></i></a>   ';
		
	}else{
		$ubahda='';
	}
	return $ubahda;
	
}

function aksesUbahtiga(){
	global $ubah;
	$ubahda='';
	
	if ($ubah==1){
				
		$ubahda='<a class="btn btn-sm btn-primary btn-xs" id="editdua" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Ubah" ><i id="edt" class="glyphicon glyphicon-edit" ></i></a>  ';
		
	}else{
		$ubahda='';
	}
	return $ubahda;
	
}
function aksesUbahdetail(){
	global $ubah;
	$ubahda='';
	
	if ($ubah==1){
				
		$ubahda='<a class="btn btn-sm btn-primary btn-xs" id="editdet" class="btn btn-sm btn-primary"  href="javascript:void(0)" title="Ubah" ><i id="edt" class="glyphicon glyphicon-edit" ></i></a>  ';
		
	}else{
		$ubahda='';
	}
	return $ubahda;
	
}

function aksesHapus(){
	global $hapus;
	$hapusda='';
	
	if ($hapus==1){
				
		$hapusda='<button onclick="" id="remove" class="btn btn-danger" disabled>
                            <i class="glyphicon glyphicon-remove"></i> Hapus
                        </button>  ';
		
	}else{
		$hapusda='';
	}
	return $hapusda;
	
}
function aksesHapusFormulir(){
	global $hapus;
	$hapusda='';
	
	if ($hapus==1){
				
		$hapusda='<button onclick="" id="removeformulir" class="btn btn-danger" disabled>
                            <i class="glyphicon glyphicon-remove"></i> Hapus
                        </button>  ';
		
	}else{
		$hapusda='';
	}
	return $hapusda;
	
}
function aksesHapussatu(){
	global $hapus;
	$hapusda='';
	
	if ($hapus==1){
				
		$hapusda='<a class="btn btn-sm btn-danger btn-xs" id="remove" href="javascript:void(0)" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a>  ';
		
	}else{
		$hapusda='';
	}
	return $hapusda;
	
}

function aksesPrint(){
	global $hapus;
	$hapusda='';
	
	if ($hapus==1){
				
		$hapusda='<a class="btn btn-sm btn-default btn-xs" id="remove" href="javascript:void(0)" title="Hapus"><i class="glyphicon glyphicon-print"></i></a>  ';
		
	}else{
		$hapusda='';
	}
	return $hapusda;
	
}

function aksesAktivasi(){
	global $aktivasi;
	$aktif='';
	
	if ($aktivasi==1){
				
		$aktif='<button onclick="" id="aktivasi" class="btn btn-danger" disabled>
					<i class="glyphicon glyphicon-ok"></i> Aktivasi
				</button>   ';
		
	}else{
		$aktif='';
	}
	return $aktif;
	
}

function aksesAktivasisatu(){
	global $aktivasi;
	$aktif='';
	
	if ($aktivasi==1){
				
		$aktif='<a class="btn btn-sm btn-danger btn-xs" id="aktivasi" href="javascript:void(0)" title="Aktivasi"><i class="glyphicon glyphicon-ok-sign"></i></a>  ';
		
	}else{
		$aktif='';
	}
	return $aktif;
	
}


function aksesAktivasikondisi(){
	global $aktivasi;
	$aktif='';
	
	if ($aktivasi==1){
				
		$aktif='<button onclick="" id="aktivasikon" class="btn btn-danger" disabled>
					<i class="glyphicon glyphicon-ok"></i> Aktivasi
				</button>   ';
		
	}else{
		$aktif='';
	}
	return $aktif;
	
}

function aksesAktivasisatukondisi(){
	global $aktivasi;
	$aktif='';
	
	if ($aktivasi==1){
				
		$aktif='<a class="btn btn-sm btn-danger btn-xs" id="aktivasikon" href="javascript:void(0)" title="Aktivasi"><i class="glyphicon glyphicon-ok-sign"></i></a>  ';
		
	}else{
		$aktif='';
	}
	return $aktif;
	
}


	

}else {
	
function callmenudess(){
	return "Dashboard";

}

function cekParent(){
	//global $parent;
	return 0;
	
	
}
function cekidaktif(){
	return "11";

}

}

}else{
	//redirect(base_url().'admin/logout');
}


function backButtonHandle(){ // nama fungsinya juga bisa d ganti "suka-suka lo" XD (y)
  $CI =& get_instance();
  $CI->load->library(array('output'));
  $CI->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
  $CI->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
  $CI->output->set_header('Pragma: no-cache');
  $CI->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
 }
 

 
 function menuAksescoba(){
	$CI =& get_instance();
	$CI->load->model('global_m');
	$CI->load->library('session');
	$id_user=$CI->session->userdata('userid'); 
	$grup=$CI->global_m->getMenuheder(); 
	foreach($grup as $gr){ 
		$id=$gr->id; 
		$menudes=$gr->menudes;
		?>
		<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $menudes;?> <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
				<?php
				$menudua=$CI->global_m->getMenuduacoba($id);
				
				foreach($menudua as $mndua){ 
					$idmenudua=$mndua->id; 
					$menudesdua=$mndua->menudes;
					$parent=$mndua->parent; 
					$jumlah=$mndua->jumlah;
					$idmdua=paramEncrypt($idmenudua);
					$url=$mndua->url."?idm=".$idmdua;
					
					if($jumlah==0){
						?>
						<li><a href="<?php echo base_url();?><?php echo $url;?>"><?php echo $menudesdua;?></a></li>	
					<?php	
					}else{
					?>
					<li class="dropdown-submenu">
					<a tabindex="-1" href="#"><?php echo $menudesdua;?></a>
					  <ul class="dropdown-menu">
							<?php
							$menutiga=$CI->global_m->getMenuduacobadua($idmenudua);
							foreach($menutiga as $mntiga){ 	
								$idmenutiga=$mntiga->id; 
								
								$menudestiga=$mntiga->menudes;
								$idmtiga=paramEncrypt($idmenutiga);
								$urldua=$mntiga->url."?idm=".$idmtiga;
								
								if($parent=="44"){
									$urltambah=$mntiga->url."?level=1&idm=".$idmtiga;
								?>
								<li><a href="<?php echo base_url();?><?php echo $urltambah;?>"><?php echo $menudestiga;?></a></li>	
								<?php
								}else{
								?>	
								<li><a href="<?php echo base_url();?><?php echo $urldua;?>"><?php echo $menudestiga;?></a></li>		
								<?php	
								}
								?>
								
							<?php }?>
						
						</ul>
					</li>
					
				<?php }} ?>	
			  </ul>
		</li>
		<?php
	}
 }
 
 

			

 
 
?>