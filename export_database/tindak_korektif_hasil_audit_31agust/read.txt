menu
	id: 108
	menudes: Tindak Korektif
	url: audit/tindakkorektif
	parent: 57
	menutool: Tindak Korektif Hasil Audit
	no: 7

rol_menu
	id: 411
	id_rol: 3
	id_menu: 108
	tampil: 1
	simpan: 1
	ubah: 1
	hapus: 1
	approve: 1
	cetak: 1
	aktivasi: 0

user_menu
	id: 1205
	id_user: 3
	id_menu: 108
	tampil: 1
	simpan: 1
	ubah: 1
	hapus: 1
	approve: 1
	cetak: 1
	aktivasi: NULL