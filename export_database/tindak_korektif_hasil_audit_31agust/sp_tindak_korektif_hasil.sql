-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2018 at 03:17 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `spmi_19082018`
--

-- --------------------------------------------------------

--
-- Table structure for table `sp_tindak_korektif_hasil`
--

CREATE TABLE IF NOT EXISTS `sp_tindak_korektif_hasil` (
  `id` int(11) NOT NULL,
  `id_unit` int(11) DEFAULT NULL,
  `no_ptk` varchar(255) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  `penyelesaian` text,
  `realisasi` varchar(255) DEFAULT NULL,
  `pj_ptk` varchar(255) DEFAULT NULL,
  `tgl_input` datetime DEFAULT NULL,
  `user_input` varchar(255) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_tindak_korektif_hasil`
--

INSERT INTO `sp_tindak_korektif_hasil` (`id`, `id_unit`, `no_ptk`, `kategori`, `penyelesaian`, `realisasi`, `pj_ptk`, `tgl_input`, `user_input`, `tgl_update`, `user_update`) VALUES
(1, 4, 'Knsu2', 'minor', 'selesai', 'real', 'dia', '2018-08-29 00:00:00', 'admin', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sp_tindak_korektif_hasil`
--
ALTER TABLE `sp_tindak_korektif_hasil`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sp_tindak_korektif_hasil`
--
ALTER TABLE `sp_tindak_korektif_hasil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
