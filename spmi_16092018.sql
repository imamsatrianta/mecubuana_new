-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2018 at 05:57 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `spmi_19082018`
--

-- --------------------------------------------------------

--
-- Table structure for table `br_identitas_borang`
--

CREATE TABLE IF NOT EXISTS `br_identitas_borang` (
  `id` int(11) NOT NULL,
  `id_jenjang` int(11) DEFAULT NULL,
  `id_prodi` int(11) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `nm_perguruantingi` varchar(200) DEFAULT NULL,
  `no_sk_pendirian` varchar(200) DEFAULT NULL,
  `tgl_sk_pendirian` date DEFAULT NULL,
  `pejabat_penandatangan_sk` varchar(200) DEFAULT NULL,
  `bulan_tahun_peneyelengara` varchar(200) DEFAULT NULL,
  `no_skijin_op` varchar(200) DEFAULT NULL,
  `tgl_sk_op` date DEFAULT NULL,
  `peringkat_ak_terakhir` varchar(200) DEFAULT NULL,
  `no_sk_banpt` varchar(200) DEFAULT NULL,
  `alamat_ps` varchar(255) DEFAULT NULL,
  `no_telp_ps` varchar(50) DEFAULT NULL,
  `no_fak_ps` varchar(50) DEFAULT NULL,
  `hp_dan_email_ps` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `br_identitas_borang`
--

INSERT INTO `br_identitas_borang` (`id`, `id_jenjang`, `id_prodi`, `tgl`, `nm_perguruantingi`, `no_sk_pendirian`, `tgl_sk_pendirian`, `pejabat_penandatangan_sk`, `bulan_tahun_peneyelengara`, `no_skijin_op`, `tgl_sk_op`, `peringkat_ak_terakhir`, `no_sk_banpt`, `alamat_ps`, `no_telp_ps`, `no_fak_ps`, `hp_dan_email_ps`, `user_input`, `tgl_input`, `user_update`, `tgl_update`) VALUES
(2, 2, 6, '2018-08-09', 'd', 'ds', '2018-09-02', 'sd', 'sd', 'sd', '2018-08-01', 'sd', 'sd', 'sd', 'sd', 'sd', 'd', 'admin', '2018-08-09', 'admin', '2018-08-09');

-- --------------------------------------------------------

--
-- Table structure for table `br_identitas_borang_dosen`
--

CREATE TABLE IF NOT EXISTS `br_identitas_borang_dosen` (
  `id` int(11) NOT NULL,
  `id_identitas` int(11) DEFAULT NULL,
  `id_dosen` int(11) DEFAULT NULL,
  `tgl_pengisian` date DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `br_identitas_borang_dosen`
--

INSERT INTO `br_identitas_borang_dosen` (`id`, `id_identitas`, `id_dosen`, `tgl_pengisian`, `user_input`, `tgl_input`, `user_update`, `tgl_update`) VALUES
(2, 2, 2, '2018-08-14', 'admin', '2018-08-09', NULL, NULL),
(3, 2, 1, '2018-08-15', 'admin', '2018-08-12', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `br_parameterborang3a`
--

CREATE TABLE IF NOT EXISTS `br_parameterborang3a` (
  `id` int(11) NOT NULL,
  `standar` smallint(2) DEFAULT NULL,
  `poin` varchar(20) DEFAULT NULL,
  `judul` varchar(200) DEFAULT NULL,
  `uraian` text,
  `jenis` smallint(1) DEFAULT '1' COMMENT '1=form ambil dari parameter(standar),2=custome',
  `parent_id` int(11) DEFAULT NULL,
  `id_jenjangprodi` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `br_parameterborang3a`
--

INSERT INTO `br_parameterborang3a` (`id`, `standar`, `poin`, `judul`, `uraian`, `jenis`, `parent_id`, `id_jenjangprodi`, `level`, `user_input`, `tgl_input`, `user_update`, `tgl_update`) VALUES
(1, 1, '1.1.1', 'Visi misi dan tujuan', 'Jelaskan mekanisme penyusunan visi, misi, tujuan dan sasaran program studi, serta pihak-pihak yang dilibatkan', 1, 0, 1, 1, 'admin', '2018-08-09', 'admin', '2018-08-09'),
(2, 1, '1.1.1.a', ' Visi program studi', ' Visi program studi', 1, 1, 1, 1, 'admin', '2018-08-09', 'admin', '2018-08-09'),
(3, 1, '1.1.1.b', 'Misi program studi', 'Misi program studi', 1, 1, 1, 1, 'admin', '2018-08-09', NULL, NULL),
(4, 1, '1.1.1.c', 'Tujuan program studi', 'Tujuan program studi', 1, 1, 1, 1, 'admin', '2018-08-09', NULL, NULL),
(5, 1, '1.1.2', 'sasaran', 'Sasaran dan strategi pencapaian', 1, 0, 1, 1, 'admin', '2018-08-09', NULL, NULL),
(6, 1, '1.2', 'Sosialisasi ', 'Uraikan upaya penyebaran/sosialisasi visi, misi dan tujuan program studi serta pemahaman sivitas akademika (dosen dan mahasiswa) dan tenaga kependidikan.', 1, 0, 1, 1, 'admin', '2018-08-09', NULL, NULL),
(7, 2, '2.1', 'Sistem Tata Pamong', 'Sistem tata pamong berjalan secara efektif melalui mekanisme yang disepakati bersama, serta dapat memelihara dan mengakomodasi semua unsur, fungsi, dan peran dalam program studi. Tata pamong didukung dengan budaya organisasi yang dicerminkan dengan ada dan tegaknya aturan, tatacara pemilihan pimpinan, etika dosen, etika mahasiswa, etika tenaga kependidikan, sistem penghargaan dan sanksi serta pedoman dan prosedur pelayanan (administrasi, perpustakaan, laboratorium, dan studio). Sistem tata pamong (input, proses, output dan outcome serta lingkungan eksternal yang menjamin terlaksananya tata pamong yang baik) harus diformulasikan, disosialisasikan, dilaksanakan,  dipantau dan dievaluasi dengan peraturan dan prosedur yang jelas. \r\n\r\nUraikan secara ringkas sistem dan pelaksanaan tata pamong di program studi untuk  membangun sistem tata pamong yang kredibel, transparan, akuntabel, bertanggung jawab dan adil.\r\n', 1, 0, 1, 1, 'admin', '2018-08-09', NULL, NULL),
(8, 2, '2.1', 'Kepemimpinan', 'Kepemimpinan efektif mengarahkan dan mempengaruhi perilaku semua unsur dalam program studi, mengikuti nilai, norma, etika, dan budaya organisasi yang disepakati bersama, serta mampu membuat keputusan yang tepat dan cepat.\r\nKepemimpinan mampu memprediksi masa depan, merumuskan dan mengartikulasi visi yang realistik, kredibel, serta mengkomunikasikan visi ke depan, yang menekankan pada keharmonisan hubungan manusia dan mampu menstimulasi secara intelektual dan arif bagi anggota untuk mewujudkan visi organisasi, serta mampu memberikan arahan, tujuan, peran, dan tugas kepada seluruh unsur dalam perguruan tinggi.  Dalam menjalankan fungsi kepemimpinan dikenal kepemimpinan operasional, kepemimpinan organisasi, dan kepemimpinan publik.  Kepemimpinan operasional berkaitan dengan kemampuan menjabarkan visi, misi ke dalam kegiatan operasional program studi.  Kepemimpinan organisasi berkaitan dengan pemahaman tata kerja antar unit dalam organisasi perguruan tinggi.  Kepemimpinan publik berkaitan dengan kemampuan menjalin kerjasama dan menjadi rujukan bagi publik.\r\n\r\nJelaskan pola kepemimpinan dalam program studi, mencakup informasi tentang kepemimpinan operasional, kepemimpinan organisasi, dan kepemimpinan publik. \r\n', 1, 0, 1, 1, 'admin', '2018-08-09', NULL, NULL),
(9, 2, '2.3', 'Sistem Pengelolaan', 'Sistem pengelolaan fungsional dan operasional program studi mencakup perencanaan, pengorganisasian, pengembangan staf, pengawasan, pengarahan, representasi, dan penganggaran.\r\n\r\nJelaskan sistem pengelolaan program studi serta dokumen pendukungnya.\r\n\r\n', 1, 0, 1, 1, 'admin', '2018-08-09', NULL, NULL),
(10, 2, '2.4', 'Penjaminan Mutu', 'Jelaskan penjaminan mutu pada program studi yang mencakup informasi tentang kebijakan, sistem dokumentasi, dan tindak lanjut atas laporan pelaksanaannya', 1, 0, 1, 1, 'admin', '2018-08-09', NULL, NULL),
(11, 2, '2.5', 'Umpan Balik', 'Apakah program studi telah melakukan kajian tentang proses pembelajaran melalui umpan balik dari dosen, mahasiswa, alumni, dan pengguna lulusan mengenai harapan dan persepsi mereka?  Jika Ya, jelaskan isi umpan balik dan tindak lanjutnya', 2, 0, 1, 1, 'admin', '2018-08-09', NULL, NULL),
(12, 2, '2.6', 'Keberlanjutan', 'Jelaskan upaya untuk menjamin keberlanjutan (sustainability) program studi ini berikut hasilnya, khususnya dalam hal:\r\na.	Upaya untuk peningkatan animo calon mahasiswa:\r\n\r\nb.	Upaya peningkatan mutu manajemen:\r\n\r\nc.	Upaya untuk peningkatan mutu lulusan:\r\n\r\nd.	Upaya untuk pelaksanaan dan hasil kerjasama kemitraan:\r\n\r\ne.	Upaya dan prestasi memperoleh dana selain dari mahasiswa:', 1, 0, 1, 1, 'admin', '2018-08-09', 'admin', '2018-08-09');

-- --------------------------------------------------------

--
-- Table structure for table `br_standardua`
--

CREATE TABLE IF NOT EXISTS `br_standardua` (
  `id` int(11) NOT NULL,
  `id_tahunakademik` int(11) DEFAULT NULL,
  `id_jenjangprodi` int(11) DEFAULT NULL,
  `id_prodi` int(11) DEFAULT NULL,
  `id_parameter` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `uraian` text,
  `nilai` decimal(5,2) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `br_standardua`
--

INSERT INTO `br_standardua` (`id`, `id_tahunakademik`, `id_jenjangprodi`, `id_prodi`, `id_parameter`, `tanggal`, `uraian`, `nilai`, `user_input`, `tgl_input`, `user_update`, `tgl_update`) VALUES
(6, 1, 1, 11, 7, '2018-08-08', '<p>isi prodi data xx</p>\r\n', '2.00', 'admin', '2018-08-12', 'admin', '2018-08-12');

-- --------------------------------------------------------

--
-- Table structure for table `br_standardua_kebijakan`
--

CREATE TABLE IF NOT EXISTS `br_standardua_kebijakan` (
  `id` int(11) NOT NULL,
  `id_borangsatu` int(11) DEFAULT NULL,
  `kebijakan` varchar(200) DEFAULT NULL,
  `prosedur` varchar(200) DEFAULT NULL,
  `lampiran` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `br_standardua_kebijakan`
--

INSERT INTO `br_standardua_kebijakan` (`id`, `id_borangsatu`, `kebijakan`, `prosedur`, `lampiran`, `user_input`, `tgl_input`, `user_update`, `tgl_update`) VALUES
(4, 6, 'sdsd xx', 'sds', '', 'admin', '2018-08-12', 'admin', '2018-08-12');

-- --------------------------------------------------------

--
-- Table structure for table `br_standarsatu`
--

CREATE TABLE IF NOT EXISTS `br_standarsatu` (
  `id` int(11) NOT NULL,
  `id_tahunakademik` int(11) DEFAULT NULL,
  `id_jenjangprodi` int(11) DEFAULT NULL,
  `id_prodi` int(11) DEFAULT NULL,
  `id_parameter` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `uraian` text,
  `nilai` decimal(5,2) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `br_standarsatu`
--

INSERT INTO `br_standarsatu` (`id`, `id_tahunakademik`, `id_jenjangprodi`, `id_prodi`, `id_parameter`, `tanggal`, `uraian`, `nilai`, `user_input`, `tgl_input`, `user_update`, `tgl_update`) VALUES
(3, 1, 1, 11, 1, '2018-08-22', '<p>dfdfd fdfd fdfd dfdfd dfd fd</p>\r\n', '4.00', 'admin', '2018-08-11', NULL, NULL),
(4, 3, 1, 11, 1, '2018-07-04', '<p>dfdfdf</p>\r\n', '0.00', 'admin', '2018-08-11', NULL, NULL),
(5, 1, 1, 11, 2, '2018-08-08', '<p>coba</p>\r\n', '0.00', 'admin', '2018-08-11', 'admin', '2018-08-11');

-- --------------------------------------------------------

--
-- Table structure for table `br_standarsatu_kebijakan`
--

CREATE TABLE IF NOT EXISTS `br_standarsatu_kebijakan` (
  `id` int(11) NOT NULL,
  `id_borangsatu` int(11) DEFAULT NULL,
  `kebijakan` varchar(200) DEFAULT NULL,
  `prosedur` varchar(200) DEFAULT NULL,
  `lampiran` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `br_standarsatu_kebijakan`
--

INSERT INTO `br_standarsatu_kebijakan` (`id`, `id_borangsatu`, `kebijakan`, `prosedur`, `lampiran`, `user_input`, `tgl_input`, `user_update`, `tgl_update`) VALUES
(2, 3, 'xxxxxxxxx', 'dsds', '41452030072018 FSD Aplikasi SPMI Marcubuana.pdf', NULL, NULL, NULL, NULL),
(3, 3, 'ada', '323', '9372861.1. STANDAR KOMPETENSI LULUSAN UMB  (April 2018).pdf', 'admin', '2018-08-11', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `formulir`
--

CREATE TABLE IF NOT EXISTS `formulir` (
  `id` int(11) NOT NULL,
  `id_sop` int(11) DEFAULT NULL,
  `nm_formulir` varchar(200) DEFAULT NULL,
  `file_upload` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kebijakan_spmi`
--

CREATE TABLE IF NOT EXISTS `kebijakan_spmi` (
  `id` int(11) NOT NULL,
  `file_upload` varchar(200) DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(9) NOT NULL,
  `menudes` varchar(100) NOT NULL,
  `url` varchar(100) NOT NULL,
  `parent` int(9) NOT NULL,
  `menutool` varchar(100) NOT NULL,
  `no` int(9) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menudes`, `url`, `parent`, `menutool`, `no`) VALUES
(1, 'Setting', '#', 76, 'Setting', 2),
(2, 'Menu ', 'masterdata/menu', 1, 'Menu System', 5),
(5, 'Rool Menu', 'masterdata/rool', 1, 'Master Rool Menu', 7),
(7, 'Master User', 'masterdata/user', 1, 'Master User', 8),
(33, 'Master', '#', 76, 'Master Data', 2),
(34, 'SPMI', '#', 0, 'Master SPMI', 3),
(35, 'Statuta', 'master/statuta', 34, 'Statuta', 1),
(36, 'Renstra', 'master/renstra', 73, 'Renstra', 2),
(37, 'Sarmut', '#', 34, 'Sarmut', 5),
(38, 'Input Sarmut', 'sarmut/inputsarmut', 37, 'Input Sarmut', 1),
(39, 'Realisasi Sarmut', 'sarmut/realisasisarmut', 37, 'Realisasi Sarmut', 2),
(40, 'Grupmenu', 'masterdata/grupmenu', 1, 'Grupmenu', 4),
(43, 'Borang APT', '#', 0, 'Borang APT', 7),
(44, 'Borang D3', '#', 0, 'Borang D3', 8),
(45, 'Borang S1', '#', 0, 'Borang S1', 9),
(46, 'Borang S2', '#', 0, 'Borang S2', 10),
(47, 'Borang S3', '#', 0, 'Borang S3', 11),
(48, 'Borang 3A', '#', 43, 'Borang 3A', 1),
(49, 'Borang 3B', '#', 43, 'Borang 3B', 2),
(50, 'Evaluasi Diri', '#', 43, 'Evaluasi Diri', 3),
(51, 'Laporan ', '#', 43, 'Laporan ', 4),
(52, 'Borang 3A', '#', 44, 'Borang 3A', 3),
(53, 'Borang 3B', '#', 44, 'Borang 3B', 4),
(54, 'Evaluasi Diri', '#', 44, 'Evaluasi Diri', 5),
(55, 'Laporan ', '#', 44, 'Laporan ', 6),
(56, 'Borang 3A', '#', 45, 'Borang 3A', 1),
(57, 'Audit', '#', 0, 'Audit', 4),
(58, 'Tahun Akademik', 'masterdata/tahunakademik', 33, 'Tahun Akademik', 1),
(59, 'Direktorat', 'masterdata/direktorat', 33, 'Direktorat', 2),
(60, 'Planing Audit', 'audit/Planingaudit', 57, 'Planing Audit', 2),
(61, 'Persiapan Formulir', 'audit/Persiapanformulir', 57, 'Persiapan Formulir', 2),
(62, 'Standar 3', 'xx', 43, 'Standar 3', 3),
(63, 'Rektorat', 'masterdata/rektorat', 33, 'Rektorat', 1),
(64, 'Unit', 'masterdata/unit', 33, 'Unit', 3),
(65, 'Karyawan', 'masterdata/karyawan', 33, 'Karyawan', 4),
(66, 'Prodi', 'masterdata/prodi', 33, 'Prodi', 5),
(67, 'Identitas Borang Prodi', 'masterdata/identitasborang', 33, 'Identitas Borang Prodi', 6),
(68, 'Kebijakan SPMI', 'master/kebijakanspmi', 73, 'Kebijakan SPMI', 3),
(69, 'Standar', 'master/standar', 74, 'Standar', 4),
(70, 'SOP', 'master/sop', 74, 'SOP', 6),
(71, 'Formulir', 'master/formulir', 74, 'Formulir', 7),
(72, 'MKP', 'master/mkp', 34, 'MKP', 8),
(73, 'Renstra', '#', 34, 'Renstra', 2),
(74, 'Standar', '#', 34, 'Standar', 3),
(75, 'Laporan Sarmut', '#', 37, 'Sarmut', 4),
(76, 'Master', '#', 0, 'Master', 1),
(77, 'Borang 3B', '#', 45, 'Borang 3B', 2),
(78, 'Evaluasi Diri', '#', 45, 'Evaluasi Diri', 3),
(79, 'Laporan', '#', 45, 'Laporan', 4),
(80, 'Borang 3A', '#', 47, 'Borang 3A', 1),
(81, 'Borang 3B', '#', 47, 'Borang 3B', 2),
(82, 'Evaluasi Diri', '#', 47, 'Evaluasi Diri', 3),
(83, 'Laporan', '#', 47, 'Evaluasi Diri', 4),
(84, 'Standar 1', '#', 48, 'Standar 1', 1),
(85, 'Standar 2', '#', 48, 'Standar 1', 2),
(86, 'Borang 3A', '#', 46, 'Borang 3A', 1),
(87, 'Borang 3B', '#', 46, 'Borang 3B', 2),
(88, 'Evaluasi Diri', '#', 46, 'Evaluasi Diri', 3),
(89, 'Laporan', '#', 46, 'Laporan', 4),
(90, 'Parameter Borang 3A', 'borang/settingborang', 44, 'Parameter Borang 3A', 1),
(91, 'Standar 1', 'borang/standarsatu', 52, 'Standar 1', 2),
(92, 'Standar 2', 'borang/standardua', 52, 'Standar 2', 3),
(93, 'Standar 3', 'borang/standartiga', 48, 'Standar 3', 4),
(94, 'Standar 4', 'borang/standarempat', 48, 'Standar 4', 5),
(95, 'Standar 5', 'borang/standarlima', 48, 'Standar 5', 6),
(96, 'Standar 6', 'borang/standarenam', 48, 'Standar 6', 7),
(97, 'Standar 7', 'borang/standartujuh', 48, 'Standar 7', 8),
(98, 'Standar 3', 'borang/standartiga', 52, 'Standar 3', 3),
(99, 'Standar 4', 'borang/standarempat', 52, 'Standar 4', 4),
(100, 'Standar 5', 'borang/standarlima', 52, 'Standar 5', 5),
(101, 'Standar 6', 'borang/standarenam', 52, 'Standar 6', 6),
(102, 'Standar 7', 'borang/standartujuh', 52, 'Standar 7', 7),
(103, 'Master Audit', '#', 37, 'Master Audit', 1),
(104, 'Surat Tugas Audit', 'audit/Surattugas', 57, 'Surat Tugas Audit', 3),
(105, 'Persiapan Pelaksanaan', 'audit/Persiapanpelaksanaan', 57, 'Persiapan Pelaksanaan', 4),
(106, 'Pelaksanaan Audit', 'audit/pelaksanaan', 57, 'Pelaksanaan Audit', 5),
(107, 'Laporan Audit', 'audit/laporan', 57, 'Laporan Audit', 6),
(108, 'Tindak Korektif', 'audit/tindakkorektif', 57, 'Tindak Korektif Hasil Audit', 7);

-- --------------------------------------------------------

--
-- Table structure for table `menu_group`
--

CREATE TABLE IF NOT EXISTS `menu_group` (
  `id` int(11) NOT NULL,
  `parent` int(11) DEFAULT '0',
  `id_menugrup` int(11) DEFAULT NULL,
  `nm_grup` varchar(255) DEFAULT NULL,
  `id_tahunakademik` int(11) DEFAULT NULL,
  `id_jenjang` int(11) DEFAULT '0',
  `id_direktoorat` int(11) DEFAULT '0',
  `id_unit` int(11) DEFAULT '0',
  `id_fakultas` int(11) DEFAULT '0',
  `id_prodi` int(11) DEFAULT '0',
  `id_jurusan` int(11) DEFAULT '0',
  `id_menu` int(11) DEFAULT NULL,
  `nm_menu` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_group`
--

INSERT INTO `menu_group` (`id`, `parent`, `id_menugrup`, `nm_grup`, `id_tahunakademik`, `id_jenjang`, `id_direktoorat`, `id_unit`, `id_fakultas`, `id_prodi`, `id_jurusan`, `id_menu`, `nm_menu`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`) VALUES
(7, 0, 1, 'Sarmut', 1, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, 1),
(8, 0, 2, 'Borang', 1, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, 1),
(9, 7, 7, 'Direktoral IT', 1, 0, 0, 0, 0, 0, 0, NULL, '', NULL, NULL, NULL, NULL, 1),
(10, 7, 2, 'Direktorat Inovasi', 1, 0, 0, 0, 0, 0, 0, NULL, '', NULL, NULL, NULL, NULL, 1),
(11, 9, 2, 'Input Sarmut', 1, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, 1),
(12, 9, 3, 'Realisasi Sarmut', 1, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, 1),
(13, 10, 3, 'Input Sarmut', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(14, 10, NULL, 'Realisasi Sarmut', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(15, 0, NULL, 'Unit PBSI', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(16, 8, NULL, 'Unit PASI', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(17, 9, NULL, 'UNIT SATU', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(18, 9, NULL, 'UNIT DUA', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(19, 10, NULL, 'Direktoral IT', 1, 4, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(20, 10, NULL, 'Direktorat Inovasi', 1, 4, 2, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(21, 19, NULL, 'Input Sarmut', 1, 4, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(22, 19, NULL, 'Realisasi Sarmut', 1, 4, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(23, 20, NULL, 'Input Sarmut', 1, 4, 2, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(24, 20, NULL, 'Realisasi Sarmut', 1, 4, 2, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(25, 19, NULL, 'Unit PBSI', 1, 6, 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(26, 19, NULL, 'Unit PASI', 1, 6, 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(27, 20, NULL, 'UNIT SATU', 1, 6, 0, 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(28, 20, NULL, 'UNIT DUA', 1, 6, 0, 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(29, 25, NULL, 'Input Sarmut', 1, 6, 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(30, 25, NULL, 'Realisasi Sarmut', 1, 6, 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_group_copy`
--

CREATE TABLE IF NOT EXISTS `menu_group_copy` (
  `id` int(11) NOT NULL,
  `parent` int(11) DEFAULT '0',
  `id_menugrup` int(11) DEFAULT NULL,
  `nm_grup` varchar(255) DEFAULT NULL,
  `id_tahunakademik` int(11) DEFAULT NULL,
  `id_jenjang` int(11) DEFAULT '0',
  `id_direktoorat` int(11) DEFAULT '0',
  `id_unit` int(11) DEFAULT '0',
  `id_fakultas` int(11) DEFAULT '0',
  `id_prodi` int(11) DEFAULT '0',
  `id_jurusan` int(11) DEFAULT '0',
  `id_menu` int(11) DEFAULT NULL,
  `nm_menu` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_group_copy`
--

INSERT INTO `menu_group_copy` (`id`, `parent`, `id_menugrup`, `nm_grup`, `id_tahunakademik`, `id_jenjang`, `id_direktoorat`, `id_unit`, `id_fakultas`, `id_prodi`, `id_jurusan`, `id_menu`, `nm_menu`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`) VALUES
(7, 0, 1, 'User', 1, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, 1),
(8, 0, 1, 'Menu', 1, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, 1),
(9, 0, 1, 'Rool', 1, 0, 0, 0, 0, 0, 0, NULL, '', NULL, NULL, NULL, NULL, 1),
(10, 0, 2, 'Tahun Ajaran', 1, 0, 0, 0, 0, 0, 0, NULL, '', NULL, NULL, NULL, NULL, 1),
(11, 0, 2, 'Borang', 1, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, 1),
(12, 0, 3, 'Rencana Audit', 1, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, 1),
(13, 0, 3, 'Pelaksanaan Audit', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(14, 0, NULL, 'Direktoral IT', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(15, 0, NULL, 'Direktorat Inovasi', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(16, 8, NULL, 'Tahun Ajaran', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(17, 9, NULL, 'Rencana Audit', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(18, 9, NULL, 'Pelaksanaan Audit', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(19, 10, NULL, 'Direktoral IT', 1, 4, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(20, 10, NULL, 'Direktorat Inovasi', 1, 4, 2, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(21, 19, NULL, 'Input Sarmut', 1, 4, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(22, 19, NULL, 'Realisasi Sarmut', 1, 4, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(23, 20, NULL, 'Input Sarmut', 1, 4, 2, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(24, 20, NULL, 'Realisasi Sarmut', 1, 4, 2, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(25, 19, NULL, 'Unit PBSI', 1, 6, 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(26, 19, NULL, 'Unit PASI', 1, 6, 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(27, 20, NULL, 'UNIT SATU', 1, 6, 0, 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(28, 20, NULL, 'UNIT DUA', 1, 6, 0, 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(29, 25, NULL, 'Input Sarmut', 1, 6, 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(30, 25, NULL, 'Realisasi Sarmut', 1, 6, 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_grup_det`
--

CREATE TABLE IF NOT EXISTS `menu_grup_det` (
  `id` int(11) NOT NULL,
  `id_menugroup` int(11) DEFAULT NULL,
  `nm_menu` varchar(200) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_grup_det`
--

INSERT INTO `menu_grup_det` (`id`, `id_menugroup`, `nm_menu`, `id_menu`) VALUES
(1, 1, 'Input Sarmut', 38),
(2, 1, 'Realisasi Sarmut', 39),
(3, 2, 'Input Sarmut', 38),
(4, 2, 'Realisasi Sarmut', 39),
(5, 3, 'Input Sarmut', 38),
(6, 3, 'Realisasi Sarmut', 39),
(7, 4, 'Input Sarmut', 38),
(8, 4, 'Realisasi Sarmut', 39),
(9, 5, 'Master User', 7),
(10, 5, 'Menu ', 2),
(11, 5, 'Rool Menu', 5);

-- --------------------------------------------------------

--
-- Table structure for table `menu_grup_heder`
--

CREATE TABLE IF NOT EXISTS `menu_grup_heder` (
  `id` int(11) NOT NULL,
  `nm_menugrup` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_grup_heder`
--

INSERT INTO `menu_grup_heder` (`id`, `nm_menugrup`) VALUES
(1, 'Setting'),
(2, 'Master Data'),
(3, 'Audit Internal'),
(4, 'Sarmut'),
(5, 'Borang');

-- --------------------------------------------------------

--
-- Table structure for table `mkp`
--

CREATE TABLE IF NOT EXISTS `mkp` (
  `id` int(11) NOT NULL,
  `id_standar` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mkp_kegiatan`
--

CREATE TABLE IF NOT EXISTS `mkp_kegiatan` (
  `id` int(11) NOT NULL,
  `id_mkp_target` int(11) DEFAULT NULL,
  `no` int(11) DEFAULT NULL,
  `kegiatan` text,
  `waktu` varchar(255) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mkp_target`
--

CREATE TABLE IF NOT EXISTS `mkp_target` (
  `id` int(11) NOT NULL,
  `id_mkp` int(11) DEFAULT NULL,
  `uraian_target` varchar(255) DEFAULT NULL,
  `cek` smallint(6) DEFAULT NULL COMMENT '0=target carakter,1=target angka, di target harus diisi',
  `target` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mkp_unitdet`
--

CREATE TABLE IF NOT EXISTS `mkp_unitdet` (
  `id` int(11) NOT NULL,
  `id_mkp_kegiatan` int(11) DEFAULT NULL,
  `id_unit` int(11) DEFAULT NULL,
  `jenis` smallint(1) DEFAULT NULL COMMENT '1=unit,2=prodi,3=direktorat,4=fakultas',
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_direktorat`
--

CREATE TABLE IF NOT EXISTS `ms_direktorat` (
  `id` int(11) NOT NULL,
  `nm_direktorat` varchar(200) DEFAULT NULL,
  `id_rektorat` int(11) DEFAULT NULL,
  `kd_server` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1',
  `flek` smallint(1) unsigned DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_direktorat`
--

INSERT INTO `ms_direktorat` (`id`, `nm_direktorat`, `id_rektorat`, `kd_server`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`, `flek`) VALUES
(1, 'Direktorat IT', 1, NULL, NULL, NULL, NULL, NULL, 1, 1),
(2, 'Direktorat Inovasi', 1, NULL, NULL, NULL, NULL, NULL, 1, 1),
(3, 'Direktorat Pemasaran R1', 1, NULL, NULL, NULL, NULL, NULL, 1, 1),
(4, 'Direktorat Keuangan', 1, NULL, NULL, NULL, NULL, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ms_fakultas`
--

CREATE TABLE IF NOT EXISTS `ms_fakultas` (
  `id` int(11) NOT NULL,
  `nm_fakultas` varchar(200) DEFAULT NULL,
  `id_rektorat` int(11) DEFAULT NULL,
  `id_server` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1',
  `flek` smallint(1) DEFAULT '2'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_fakultas`
--

INSERT INTO `ms_fakultas` (`id`, `nm_fakultas`, `id_rektorat`, `id_server`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`, `flek`) VALUES
(1, 'Fakultas Teknik', 2, NULL, NULL, NULL, NULL, NULL, 1, 2),
(2, 'Fakultas Akuntansi', 2, NULL, NULL, NULL, NULL, NULL, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ms_fakultas_hapus`
--

CREATE TABLE IF NOT EXISTS `ms_fakultas_hapus` (
  `id` int(11) NOT NULL,
  `nm_fakultas` varchar(200) DEFAULT NULL,
  `id_rektorat` int(11) DEFAULT NULL,
  `id_server` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1',
  `flek` smallint(1) DEFAULT '2'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_fakultas_hapus`
--

INSERT INTO `ms_fakultas_hapus` (`id`, `nm_fakultas`, `id_rektorat`, `id_server`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`, `flek`) VALUES
(1, 'Fakultas Teknik', 2, NULL, NULL, NULL, NULL, NULL, 1, 2),
(2, 'Fakultas Akuntansi', 2, NULL, NULL, NULL, NULL, NULL, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ms_jenjang`
--

CREATE TABLE IF NOT EXISTS `ms_jenjang` (
  `id` int(11) NOT NULL,
  `jenjang` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_jenjang`
--

INSERT INTO `ms_jenjang` (`id`, `jenjang`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`) VALUES
(1, 'Administrator', 'admin', NULL, NULL, NULL, 1),
(2, 'Rektor', 'admin', NULL, NULL, NULL, 1),
(3, 'Wakil Rektor', 'admin', NULL, NULL, NULL, 1),
(4, 'Direktorat', 'admin', NULL, NULL, NULL, 1),
(5, 'Fakultas', 'admin', NULL, NULL, NULL, 1),
(6, 'Unit', 'admin', NULL, NULL, NULL, 1),
(7, 'Prodi', 'admin', NULL, NULL, NULL, 1),
(8, 'Jurusan', 'admin', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ms_jenjangprodi`
--

CREATE TABLE IF NOT EXISTS `ms_jenjangprodi` (
  `id` int(11) NOT NULL,
  `nm_jenjangprodi` varchar(20) DEFAULT NULL,
  `kd_server` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_jenjangprodi`
--

INSERT INTO `ms_jenjangprodi` (`id`, `nm_jenjangprodi`, `kd_server`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`) VALUES
(1, 'D3', NULL, NULL, NULL, NULL, NULL, 1),
(2, 'S1', NULL, NULL, NULL, NULL, NULL, 1),
(3, 'S2', NULL, NULL, NULL, NULL, NULL, 1),
(4, 'S3', NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ms_jurusan`
--

CREATE TABLE IF NOT EXISTS `ms_jurusan` (
  `id` int(11) NOT NULL,
  `nm_jurusan` varchar(200) DEFAULT NULL,
  `id_prodi` int(11) DEFAULT NULL,
  `kd_server` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_jurusan`
--

INSERT INTO `ms_jurusan` (`id`, `nm_jurusan`, `id_prodi`, `kd_server`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`) VALUES
(1, 'System Informasi', 1, NULL, NULL, NULL, NULL, NULL, 1),
(2, 'Teknik Informatika', 1, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ms_jurusan_hapus`
--

CREATE TABLE IF NOT EXISTS `ms_jurusan_hapus` (
  `id` int(11) NOT NULL,
  `nm_jurusan` varchar(200) DEFAULT NULL,
  `id_prodi` int(11) DEFAULT NULL,
  `kd_server` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_jurusan_hapus`
--

INSERT INTO `ms_jurusan_hapus` (`id`, `nm_jurusan`, `id_prodi`, `kd_server`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`) VALUES
(1, 'System Informasi', 1, NULL, NULL, NULL, NULL, NULL, 1),
(2, 'Teknik Informatika', 1, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ms_karyawan`
--

CREATE TABLE IF NOT EXISTS `ms_karyawan` (
  `id` int(11) NOT NULL,
  `nidn` varchar(50) DEFAULT '0',
  `nm_karyawan` varchar(200) DEFAULT NULL,
  `jenis_jabatan` int(11) DEFAULT '0' COMMENT '1=rektorat,2=direktorat,4=kepala unit,4=staf',
  `id_jabatan` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_karyawan`
--

INSERT INTO `ms_karyawan` (`id`, `nidn`, `nm_karyawan`, `jenis_jabatan`, `id_jabatan`, `user_input`, `tgl_input`, `user_update`, `tgl_update`) VALUES
(1, '123', 'adam', 1, 1, NULL, NULL, NULL, NULL),
(2, '231', 'ali', 2, 2, NULL, NULL, NULL, NULL),
(3, '333', 'hasan', 3, 1, NULL, NULL, NULL, NULL),
(4, '44', 'malik', 4, NULL, NULL, NULL, NULL, NULL),
(5, '123', 'bayu12', 1, 1, 'admin', '2018-08-09', 'admin', '2018-08-09'),
(6, '0', 'Riyan Apriyanto', 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ms_level`
--

CREATE TABLE IF NOT EXISTS `ms_level` (
  `id` int(11) NOT NULL,
  `level` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_level`
--

INSERT INTO `ms_level` (`id`, `level`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`) VALUES
(1, 'Administrator', 'admin', NULL, NULL, NULL, 1),
(2, 'Rektorat', 'admin', NULL, NULL, NULL, 1),
(3, 'Direktorat', 'admin', NULL, NULL, NULL, 1),
(4, 'Unit', 'admin', NULL, NULL, NULL, 1),
(5, 'Prodi', 'admin', NULL, NULL, NULL, 1),
(6, 'PAE', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ms_prodi`
--

CREATE TABLE IF NOT EXISTS `ms_prodi` (
  `id` int(11) NOT NULL,
  `nm_prodi` varchar(200) DEFAULT NULL,
  `id_rektorat` int(11) DEFAULT NULL,
  `id_jenjangprodi` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT '0',
  `kd_server` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1',
  `flek` smallint(1) DEFAULT '2',
  `level` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_prodi`
--

INSERT INTO `ms_prodi` (`id`, `nm_prodi`, `id_rektorat`, `id_jenjangprodi`, `parent`, `kd_server`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`, `flek`, `level`) VALUES
(1, 'PERTANIAN', 3, 2, 0, NULL, NULL, NULL, 'admin', '2018-07-29', 1, 2, 1),
(2, 'AGRIBISNIS', 3, 2, 1, NULL, NULL, NULL, 'admin', '2018-07-29', 1, 2, 2),
(3, 'AGRONOMI', 3, 2, 1, NULL, 'admin', '2018-07-21', 'admin', '2018-07-29', 1, 2, 2),
(4, 'Ilmu Komunikasi', 3, 2, 0, NULL, 'admin', '2018-07-27', 'admin', '2018-07-29', 1, 2, 1),
(5, 'Penyiaran', 1, 2, 4, NULL, 'admin', '2018-07-27', 'admin', '2018-07-29', 1, 2, 2),
(6, 'Hubungan Masyarakat', 3, 2, 4, NULL, 'admin', '2018-07-29', NULL, NULL, 1, 2, 2),
(7, 'Periklanan dan Komunikasi Pemasaran', 3, 2, 4, NULL, 'admin', '2018-07-29', NULL, NULL, 1, 2, 2),
(8, 'Komunikasi Visual', 3, 2, 4, NULL, 'admin', '2018-07-29', NULL, NULL, 1, 2, 2),
(9, 'Komunikasi Digital', 3, 2, 4, NULL, 'admin', '2018-07-29', NULL, NULL, 1, 2, 2),
(10, 'Ekonomi dan Bisnis', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 1, 2, 1),
(11, 'Manajemen (D3)', NULL, 1, 10, NULL, 'admin', '2018-08-10', NULL, NULL, 1, 2, 2),
(12, 'Akuntansi (D3)', NULL, 1, 10, NULL, 'admin', '2018-08-10', NULL, NULL, 1, 2, 2),
(13, 'Desain dan Seni Kreatif', NULL, 2, 0, NULL, 'admin', '2018-08-10', NULL, NULL, 1, 2, 1),
(14, 'TEKNIK DESAIN DAN INTERIOR (D3)', NULL, 1, 13, NULL, 'admin', '2018-08-10', NULL, NULL, 1, 2, 2),
(15, 'Desain Produk', NULL, 2, 13, NULL, 'admin', '2018-08-10', NULL, NULL, 1, 2, 2),
(16, 'Desain Interior', NULL, 2, 13, NULL, 'admin', '2018-08-10', NULL, NULL, 1, 2, 2),
(17, 'Desain Komunikasi Visual', NULL, 2, 13, NULL, 'admin', '2018-08-10', NULL, NULL, 1, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ms_rektorat`
--

CREATE TABLE IF NOT EXISTS `ms_rektorat` (
  `id` int(11) NOT NULL,
  `nm_rektorat` varchar(20) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_rektorat`
--

INSERT INTO `ms_rektorat` (`id`, `nm_rektorat`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`) VALUES
(1, 'WD Sumberdaya', NULL, NULL, NULL, NULL, 1),
(2, 'WD Inovasi dan Kemah', NULL, NULL, 'admin', '2018-07-27', 1),
(3, 'WS Kemahasiswaan', 'admin', '2018-07-29', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ms_unit`
--

CREATE TABLE IF NOT EXISTS `ms_unit` (
  `id` int(11) NOT NULL,
  `nm_unit` varchar(200) DEFAULT NULL,
  `id_direktorat` int(11) DEFAULT NULL,
  `kd_server` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1',
  `flek` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_unit`
--

INSERT INTO `ms_unit` (`id`, `nm_unit`, `id_direktorat`, `kd_server`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`, `flek`) VALUES
(1, 'Unit PBSI', 1, NULL, NULL, NULL, NULL, NULL, 1, 1),
(2, 'Unit PASI', 1, NULL, NULL, NULL, NULL, NULL, 1, 1),
(3, 'Unit Satu', 2, NULL, NULL, NULL, NULL, NULL, 1, 1),
(4, 'Unit 2', 2, NULL, NULL, NULL, 'admin', '2018-07-27', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `renstra`
--

CREATE TABLE IF NOT EXISTS `renstra` (
  `id` int(11) NOT NULL,
  `nm_dokumen` varchar(200) DEFAULT NULL,
  `file_upload` varchar(200) DEFAULT NULL,
  `tgl_awal` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  `tgl_publish` date DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `renstra_det`
--

CREATE TABLE IF NOT EXISTS `renstra_det` (
  `id` int(11) NOT NULL,
  `id_renstra` int(11) DEFAULT NULL,
  `nomor` varchar(100) DEFAULT NULL,
  `uraian` varchar(200) DEFAULT NULL,
  `target` int(11) DEFAULT NULL,
  `tahun_awal` int(11) DEFAULT NULL,
  `tahun_tengah` int(11) DEFAULT NULL,
  `tahun_akhir` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rol`
--

CREATE TABLE IF NOT EXISTS `rol` (
  `id_rol` int(11) NOT NULL,
  `nm_rol` varchar(300) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `user_c` int(11) DEFAULT NULL,
  `tgl_c` date DEFAULT NULL,
  `user_u` int(11) DEFAULT NULL,
  `tgl_u` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rol`
--

INSERT INTO `rol` (`id_rol`, `nm_rol`, `status`, `user_c`, `tgl_c`, `user_u`, `tgl_u`) VALUES
(1, 'user', 1, NULL, NULL, 0, '2018-08-09'),
(2, 'dua', 1, NULL, NULL, 0, '2016-08-06'),
(3, 'admin', 1, 0, '2017-02-21', 0, '2018-08-19'),
(4, 'xxx', 1, 0, '2018-07-20', 0, '2018-07-20'),
(5, 'xcxcxc', 1, 0, '2018-07-20', 0, '2018-07-20');

-- --------------------------------------------------------

--
-- Table structure for table `rol_menu`
--

CREATE TABLE IF NOT EXISTS `rol_menu` (
  `id` int(11) NOT NULL,
  `id_rol` int(11) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `tampil` tinyint(1) NOT NULL DEFAULT '0',
  `simpan` tinyint(1) NOT NULL DEFAULT '0',
  `ubah` tinyint(1) NOT NULL DEFAULT '0',
  `hapus` tinyint(1) NOT NULL DEFAULT '0',
  `approve` tinyint(1) NOT NULL DEFAULT '0',
  `cetak` tinyint(1) NOT NULL DEFAULT '0',
  `aktivasi` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=412 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rol_menu`
--

INSERT INTO `rol_menu` (`id`, `id_rol`, `id_menu`, `tampil`, `simpan`, `ubah`, `hapus`, `approve`, `cetak`, `aktivasi`) VALUES
(12, 2, 2, 1, 1, 0, 0, 0, 0, 0),
(13, 2, 4, 1, 1, 0, 0, 0, 0, 0),
(14, 2, 5, 1, 1, 0, 0, 0, 0, 0),
(15, 2, 7, 1, 1, 0, 0, 0, 0, 0),
(101, 4, 1, 1, 0, 0, 0, 0, 0, 0),
(102, 4, 40, 1, 1, 0, 0, 0, 0, 0),
(103, 4, 2, 1, 1, 0, 0, 0, 0, 0),
(104, 4, 5, 1, 1, 0, 0, 0, 0, 0),
(105, 4, 7, 1, 1, 0, 0, 0, 0, 0),
(106, 4, 33, 1, 0, 0, 0, 0, 0, 0),
(107, 4, 34, 1, 0, 0, 0, 0, 0, 0),
(108, 4, 37, 1, 0, 0, 0, 0, 0, 0),
(109, 4, 57, 1, 0, 0, 0, 0, 0, 0),
(110, 4, 41, 1, 0, 0, 0, 0, 0, 0),
(121, 5, 1, 1, 0, 0, 0, 0, 0, 0),
(122, 5, 40, 1, 1, 1, 1, 1, 1, 0),
(123, 5, 2, 1, 1, 1, 1, 1, 1, 0),
(124, 5, 5, 1, 1, 1, 1, 1, 1, 0),
(125, 5, 7, 1, 1, 1, 1, 1, 1, 0),
(126, 5, 33, 1, 0, 0, 0, 0, 0, 0),
(127, 5, 58, 1, 1, 0, 0, 0, 0, 0),
(128, 5, 59, 1, 1, 0, 0, 0, 0, 0),
(129, 5, 34, 1, 0, 0, 0, 0, 0, 0),
(130, 5, 37, 1, 0, 0, 0, 0, 0, 0),
(131, 5, 57, 1, 0, 0, 0, 0, 0, 0),
(132, 5, 41, 1, 0, 0, 0, 0, 0, 0),
(133, 1, 76, 1, 0, 0, 0, 0, 0, 0),
(134, 1, 1, 1, 0, 0, 0, 0, 0, 0),
(135, 1, 33, 1, 0, 0, 0, 0, 0, 0),
(136, 1, 34, 1, 0, 0, 0, 0, 0, 0),
(137, 1, 57, 1, 0, 0, 0, 0, 0, 0),
(138, 1, 43, 1, 0, 0, 0, 0, 0, 0),
(139, 1, 44, 1, 0, 0, 0, 0, 0, 0),
(140, 1, 45, 1, 0, 0, 0, 0, 0, 0),
(141, 1, 46, 1, 0, 0, 0, 0, 0, 0),
(142, 1, 47, 1, 0, 0, 0, 0, 0, 0),
(347, 3, 76, 1, 0, 0, 0, 0, 0, 0),
(348, 3, 1, 1, 1, 1, 1, 1, 1, 0),
(349, 3, 40, 1, 1, 1, 1, 1, 1, 0),
(350, 3, 2, 1, 1, 1, 1, 1, 1, 0),
(351, 3, 5, 1, 1, 1, 1, 1, 1, 0),
(352, 3, 7, 1, 1, 1, 1, 1, 1, 0),
(353, 3, 33, 1, 1, 1, 1, 1, 1, 0),
(354, 3, 58, 1, 1, 1, 1, 1, 1, 0),
(355, 3, 63, 1, 1, 1, 1, 1, 1, 0),
(356, 3, 59, 1, 1, 1, 1, 1, 1, 0),
(357, 3, 64, 1, 1, 1, 1, 1, 1, 0),
(358, 3, 65, 1, 1, 1, 1, 1, 1, 0),
(359, 3, 66, 1, 1, 1, 1, 1, 1, 0),
(360, 3, 67, 1, 1, 1, 1, 1, 1, 0),
(361, 3, 34, 1, 0, 0, 0, 0, 0, 0),
(362, 3, 35, 1, 1, 1, 1, 1, 1, 0),
(363, 3, 73, 1, 1, 1, 1, 1, 1, 0),
(364, 3, 36, 1, 1, 1, 1, 1, 1, 0),
(365, 3, 68, 1, 1, 1, 1, 1, 1, 0),
(366, 3, 74, 1, 1, 1, 1, 1, 1, 0),
(367, 3, 69, 1, 1, 1, 1, 1, 1, 0),
(368, 3, 70, 1, 1, 1, 1, 1, 1, 0),
(369, 3, 71, 1, 1, 1, 1, 1, 1, 0),
(370, 3, 37, 1, 1, 1, 1, 1, 1, 0),
(371, 3, 38, 1, 1, 1, 1, 1, 1, 0),
(372, 3, 103, 1, 1, 1, 1, 1, 1, 0),
(373, 3, 39, 1, 1, 1, 1, 1, 1, 0),
(374, 3, 72, 1, 1, 1, 1, 1, 1, 0),
(375, 3, 57, 1, 0, 0, 0, 0, 0, 0),
(376, 3, 60, 1, 1, 1, 1, 1, 1, 0),
(377, 3, 61, 1, 1, 1, 1, 1, 1, 0),
(378, 3, 104, 1, 1, 1, 1, 1, 1, 0),
(379, 3, 105, 1, 1, 1, 1, 1, 1, 0),
(380, 3, 106, 1, 1, 1, 1, 1, 1, 0),
(381, 3, 43, 1, 0, 0, 0, 0, 0, 0),
(382, 3, 48, 1, 1, 1, 1, 1, 1, 0),
(383, 3, 84, 1, 1, 1, 1, 1, 1, 0),
(384, 3, 85, 1, 1, 1, 1, 1, 1, 0),
(385, 3, 49, 1, 1, 1, 1, 1, 1, 0),
(386, 3, 50, 1, 1, 1, 1, 1, 1, 0),
(387, 3, 62, 1, 1, 1, 1, 1, 1, 0),
(388, 3, 51, 1, 1, 1, 1, 1, 1, 0),
(389, 3, 44, 1, 0, 0, 0, 0, 0, 0),
(390, 3, 90, 1, 1, 1, 1, 1, 1, 0),
(391, 3, 52, 1, 1, 1, 1, 1, 1, 0),
(392, 3, 53, 1, 1, 1, 1, 1, 1, 0),
(393, 3, 54, 1, 1, 1, 1, 1, 1, 0),
(394, 3, 55, 1, 1, 1, 1, 1, 1, 0),
(395, 3, 45, 1, 0, 0, 0, 0, 0, 0),
(396, 3, 56, 1, 1, 1, 1, 1, 1, 0),
(397, 3, 77, 1, 1, 1, 1, 1, 1, 0),
(398, 3, 78, 1, 1, 1, 1, 1, 1, 0),
(399, 3, 79, 1, 1, 1, 1, 1, 1, 0),
(400, 3, 46, 1, 0, 0, 0, 0, 0, 0),
(401, 3, 86, 1, 1, 1, 1, 1, 1, 0),
(402, 3, 87, 1, 1, 1, 1, 1, 1, 0),
(403, 3, 88, 1, 1, 1, 1, 1, 1, 0),
(404, 3, 89, 1, 1, 1, 1, 1, 1, 0),
(405, 3, 47, 1, 0, 0, 0, 0, 0, 0),
(406, 3, 80, 1, 1, 1, 1, 1, 1, 0),
(407, 3, 81, 1, 1, 1, 1, 1, 1, 0),
(408, 3, 82, 1, 1, 1, 1, 1, 1, 0),
(409, 3, 83, 1, 1, 1, 1, 1, 1, 0),
(410, 3, 107, 1, 1, 1, 1, 1, 1, 0),
(411, 3, 108, 1, 1, 1, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sarmut`
--

CREATE TABLE IF NOT EXISTS `sarmut` (
  `id` int(11) NOT NULL,
  `id_mkp_kegiatan` int(11) DEFAULT NULL,
  `kegiatan` text,
  `target_kuantitatif` int(11) DEFAULT NULL,
  `target_kualitatif` varchar(200) DEFAULT NULL,
  `kinerja_tahunlalu` varchar(200) DEFAULT NULL,
  `angaran` int(11) DEFAULT NULL,
  `id_unit` int(11) DEFAULT NULL,
  `jenis` smallint(6) DEFAULT NULL COMMENT '1=unit,2=prodi,3=direktorat,4=fakultas',
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `id_tahunakademik` int(11) DEFAULT NULL,
  `pelaksana` varchar(200) DEFAULT NULL,
  `kode_rencanaapprove` varchar(255) DEFAULT NULL,
  `kode_approve` varchar(255) DEFAULT NULL,
  `presentase_realisasi` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sarmut_realisasi`
--

CREATE TABLE IF NOT EXISTS `sarmut_realisasi` (
  `id` int(11) NOT NULL,
  `id_sarmut` int(11) DEFAULT NULL,
  `tgl_kegiatan` date DEFAULT NULL,
  `realisasi` varchar(200) DEFAULT NULL,
  `file_upload` varchar(200) DEFAULT NULL,
  `ket_proges` varchar(250) DEFAULT NULL,
  `presentase` int(10) DEFAULT NULL COMMENT 'mengcu ke kolom target kuantitatif',
  `kendala` varchar(200) DEFAULT NULL,
  `tindakan_korelatif` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sop`
--

CREATE TABLE IF NOT EXISTS `sop` (
  `id` int(11) NOT NULL,
  `nm_sop` varchar(200) DEFAULT NULL,
  `id_standar` int(11) DEFAULT NULL,
  `file_upload` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sp_formulir`
--

CREATE TABLE IF NOT EXISTS `sp_formulir` (
  `id` int(11) NOT NULL,
  `id_sop` int(11) DEFAULT NULL,
  `nm_formulir` varchar(200) DEFAULT NULL,
  `file_upload` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_formulir`
--

INSERT INTO `sp_formulir` (`id`, `id_sop`, `nm_formulir`, `file_upload`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`) VALUES
(1, 1, 'Formulir 1', 'dokfolmulir/557708gbr.png', 'admin', '2018-07-29', NULL, NULL, 1),
(2, 2, 'Formulir 2', 'dokfolmulir/', 'admin', '2018-07-29', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_formulir_audit`
--

CREATE TABLE IF NOT EXISTS `sp_formulir_audit` (
  `id` int(11) NOT NULL,
  `id_unit` int(11) DEFAULT NULL,
  `id_standar` int(11) DEFAULT NULL,
  `catatan` text,
  `rencana` text,
  `tglclosing` date DEFAULT NULL,
  `tgl_input` datetime DEFAULT NULL,
  `user_input` varchar(255) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_formulir_audit`
--

INSERT INTO `sp_formulir_audit` (`id`, `id_unit`, `id_standar`, `catatan`, `rencana`, `tglclosing`, `tgl_input`, `user_input`, `tgl_update`, `user_update`) VALUES
(42, 3, 4, 'cek', 'where', '2018-09-25', '2018-09-16 00:00:00', 'admin', NULL, NULL),
(43, 3, 2, 'ds', 'ss', '2018-09-01', '2018-09-16 00:00:00', 'admin', '2018-09-16 00:00:00', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `sp_formulir_audit_doc`
--

CREATE TABLE IF NOT EXISTS `sp_formulir_audit_doc` (
  `id` int(11) NOT NULL,
  `id_formulir_audit` int(11) DEFAULT NULL,
  `id_persiapan_formulir_doc` int(11) DEFAULT NULL,
  `checklist` int(11) DEFAULT NULL,
  `keterangan` text,
  `masalah` text,
  `tindakan` text,
  `tgl_input` datetime DEFAULT NULL,
  `user_input` varchar(255) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_formulir_audit_doc`
--

INSERT INTO `sp_formulir_audit_doc` (`id`, `id_formulir_audit`, `id_persiapan_formulir_doc`, `checklist`, `keterangan`, `masalah`, `tindakan`, `tgl_input`, `user_input`, `tgl_update`, `user_update`) VALUES
(58, 42, 15, 3, 'cek', 'ya', 'kan', '2018-09-16 00:00:00', 'admin', NULL, NULL),
(63, 43, 14, 0, '', '', '', '2018-09-16 00:00:00', 'admin', NULL, NULL),
(64, 43, 1, 2, 'asa', 'ss', 'ss', '2018-09-16 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sp_kebijakan_spmi`
--

CREATE TABLE IF NOT EXISTS `sp_kebijakan_spmi` (
  `id` int(11) NOT NULL,
  `file_upload` varchar(200) DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_kebijakan_spmi`
--

INSERT INTO `sp_kebijakan_spmi` (`id`, `file_upload`, `keterangan`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`) VALUES
(1, 'dokkebijakan/711700gbr.png', 'Kebijakan 2', 'admin', '2018-07-29', NULL, NULL, 1),
(2, 'dokkebijakan/742401gbr.png', 'Kebijakan 1', 'admin', '2018-07-29', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_laporan_audit`
--

CREATE TABLE IF NOT EXISTS `sp_laporan_audit` (
  `id` int(11) NOT NULL,
  `id_unit` int(11) DEFAULT NULL,
  `tgl_audit` date DEFAULT NULL,
  `jam` time NOT NULL,
  `kegiatan` text,
  `tgl_input` datetime DEFAULT NULL,
  `user_input` varchar(255) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_laporan_audit`
--

INSERT INTO `sp_laporan_audit` (`id`, `id_unit`, `tgl_audit`, `jam`, `kegiatan`, `tgl_input`, `user_input`, `tgl_update`, `user_update`) VALUES
(1, 4, '2018-08-28', '06:30:00', 'Checking', '2018-08-31 00:00:00', 'admin', NULL, NULL),
(7, 1, '2018-08-09', '01:15:15', 'sada', '2018-08-31 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sp_laporan_audit_bidang`
--

CREATE TABLE IF NOT EXISTS `sp_laporan_audit_bidang` (
  `id` int(11) NOT NULL,
  `id_laporan_audit` int(11) DEFAULT NULL,
  `bidang` varchar(255) DEFAULT NULL,
  `kelebihan` varchar(255) DEFAULT NULL,
  `peluang` varchar(255) DEFAULT NULL,
  `tgl_input` datetime DEFAULT NULL,
  `user_input` varchar(255) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_laporan_audit_bidang`
--

INSERT INTO `sp_laporan_audit_bidang` (`id`, `id_laporan_audit`, `bidang`, `kelebihan`, `peluang`, `tgl_input`, `user_input`, `tgl_update`, `user_update`) VALUES
(9, 1, 'kebersihan', 'bersih', 'tes', '2018-08-31 00:00:00', 'admin', NULL, NULL),
(10, 1, 'cek', 'yap', 'ya', '2018-08-31 00:00:00', 'admin', NULL, NULL),
(11, 7, 'adas', 'ada', 'sss', '2018-08-31 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sp_laporan_audit_kts_ob`
--

CREATE TABLE IF NOT EXISTS `sp_laporan_audit_kts_ob` (
  `id` int(11) NOT NULL,
  `id_laporan_audit` int(11) DEFAULT NULL,
  `id_standar` int(11) DEFAULT NULL,
  `kts_ob` varchar(255) DEFAULT NULL,
  `pernyataan` varchar(255) DEFAULT NULL,
  `tgl_input` datetime DEFAULT NULL,
  `user_input` varchar(255) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_laporan_audit_kts_ob`
--

INSERT INTO `sp_laporan_audit_kts_ob` (`id`, `id_laporan_audit`, `id_standar`, `kts_ob`, `pernyataan`, `tgl_input`, `user_input`, `tgl_update`, `user_update`) VALUES
(13, 1, 4, 'sesuai', 'yakin', '2018-08-31 00:00:00', 'admin', NULL, NULL),
(14, 1, 5, 'tidak sesuai', 'baik', '2018-08-31 00:00:00', 'admin', NULL, NULL),
(15, 7, 4, 'asda', 'adas', '2018-08-31 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sp_mkp`
--

CREATE TABLE IF NOT EXISTS `sp_mkp` (
  `id` int(11) NOT NULL,
  `id_tahunakademik` int(11) DEFAULT NULL,
  `nm_mkp` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_mkp`
--

INSERT INTO `sp_mkp` (`id`, `id_tahunakademik`, `nm_mkp`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`) VALUES
(3, 1, 'tes', 'admin', '2018-08-28', NULL, NULL, 1),
(5, 2, 'sdsd', 'admin', '2018-08-28', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_mkp_kegiatan`
--

CREATE TABLE IF NOT EXISTS `sp_mkp_kegiatan` (
  `id` int(11) NOT NULL,
  `id_mkp_target` int(11) DEFAULT NULL,
  `no` int(11) DEFAULT NULL,
  `kegiatan` text,
  `waktu` varchar(255) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_mkp_kegiatan`
--

INSERT INTO `sp_mkp_kegiatan` (`id`, `id_mkp_target`, `no`, `kegiatan`, `waktu`, `user_input`, `tgl_input`, `user_update`, `tgl_update`) VALUES
(2, 3, NULL, 'kegiatan 2 edit', 'Sm Ganjil', 'admin', '2018-08-07', NULL, NULL),
(3, 3, NULL, 'kegiatan 3', 'Sm Ganjil,Sm Genap', 'admin', '2018-08-05', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sp_mkp_standar`
--

CREATE TABLE IF NOT EXISTS `sp_mkp_standar` (
  `id` int(11) NOT NULL,
  `id_mkp` int(11) NOT NULL,
  `id_standar` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_mkp_standar`
--

INSERT INTO `sp_mkp_standar` (`id`, `id_mkp`, `id_standar`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`) VALUES
(1, 1, 2, 'admin', '2018-08-04', NULL, NULL, 1),
(2, 2, 4, 'admin', '2018-08-04', NULL, NULL, 1),
(3, 3, 4, 'admin', '2018-08-28', NULL, NULL, 1),
(4, 6, 4, 'admin', '2018-08-28', NULL, NULL, 1),
(5, 6, 5, 'admin', '2018-08-28', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_mkp_target`
--

CREATE TABLE IF NOT EXISTS `sp_mkp_target` (
  `id` int(11) NOT NULL,
  `id_mkp` int(11) DEFAULT NULL,
  `uraian_target` varchar(255) DEFAULT NULL,
  `cek` smallint(6) DEFAULT NULL COMMENT '0=target carakter,1=target angka, di target harus diisi',
  `target` int(11) DEFAULT '0',
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_mkp_target`
--

INSERT INTO `sp_mkp_target` (`id`, `id_mkp`, `uraian_target`, `cek`, `target`, `user_input`, `tgl_input`, `user_update`, `tgl_update`) VALUES
(3, 2, 'wwww', 0, 0, 'admin', '2018-08-04', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sp_mkp_unitdet`
--

CREATE TABLE IF NOT EXISTS `sp_mkp_unitdet` (
  `id` int(11) NOT NULL,
  `id_mkp_kegiatan` int(11) DEFAULT NULL,
  `id_unit` int(11) DEFAULT NULL,
  `jenis` smallint(1) DEFAULT NULL COMMENT '1=unit,2=direktorat',
  `nm_unit` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_mkp_unitdet`
--

INSERT INTO `sp_mkp_unitdet` (`id`, `id_mkp_kegiatan`, `id_unit`, `jenis`, `nm_unit`, `user_input`, `tgl_input`) VALUES
(5, 3, 1, 1, NULL, 'admin', '2018-08-05'),
(6, 3, 1, 2, NULL, 'admin', '2018-08-05'),
(7, 3, 2, 2, NULL, 'admin', '2018-08-05'),
(8, 2, 1, 1, NULL, 'admin', '2018-08-07'),
(9, 2, 2, 1, NULL, 'admin', '2018-08-07');

-- --------------------------------------------------------

--
-- Table structure for table `sp_persiapan_formulir`
--

CREATE TABLE IF NOT EXISTS `sp_persiapan_formulir` (
  `id` int(11) NOT NULL,
  `id_standar` int(11) DEFAULT NULL,
  `materi` text,
  `tgl_input` datetime DEFAULT NULL,
  `user_input` varchar(255) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_persiapan_formulir`
--

INSERT INTO `sp_persiapan_formulir` (`id`, `id_standar`, `materi`, `tgl_input`, `user_input`, `tgl_update`, `user_update`) VALUES
(1, 2, 'Pembahasan ABC', NULL, NULL, NULL, NULL),
(13, 5, 'desa', '2018-08-29 00:00:00', 'admin', NULL, NULL),
(14, 4, 'Pembelajaran', '2018-09-09 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sp_persiapan_formulir_doc`
--

CREATE TABLE IF NOT EXISTS `sp_persiapan_formulir_doc` (
  `id` int(11) NOT NULL,
  `id_persiapan_formulir` int(11) DEFAULT NULL,
  `document` text,
  `tgl_input` datetime DEFAULT NULL,
  `user_input` varchar(255) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_persiapan_formulir_doc`
--

INSERT INTO `sp_persiapan_formulir_doc` (`id`, `id_persiapan_formulir`, `document`, `tgl_input`, `user_input`, `tgl_update`, `user_update`) VALUES
(1, 1, 'pembahasan', '2018-08-28 00:00:00', 'admin', NULL, NULL),
(13, 13, 'cek', '2018-08-29 00:00:00', 'admin', NULL, NULL),
(14, 1, 'bahas', NULL, NULL, NULL, NULL),
(15, 14, 'doc 1', '2018-09-09 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sp_persiapan_pelaksanaan`
--

CREATE TABLE IF NOT EXISTS `sp_persiapan_pelaksanaan` (
  `id` int(11) NOT NULL,
  `jadwal` date DEFAULT NULL,
  `id_planingaudit` int(11) DEFAULT NULL,
  `tgl_input` datetime DEFAULT NULL,
  `user_input` varchar(255) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_persiapan_pelaksanaan`
--

INSERT INTO `sp_persiapan_pelaksanaan` (`id`, `jadwal`, `id_planingaudit`, `tgl_input`, `user_input`, `tgl_update`, `user_update`) VALUES
(5, '2018-07-30', 3, '2018-09-07 00:00:00', 'admin', NULL, NULL),
(6, '2018-09-02', 5, '2018-09-07 00:00:00', 'admin', NULL, NULL),
(7, '2018-09-02', 5, '2018-09-07 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sp_planing_audit`
--

CREATE TABLE IF NOT EXISTS `sp_planing_audit` (
  `id` int(11) NOT NULL,
  `id_unit` int(11) DEFAULT NULL,
  `id_ketuatim` int(11) DEFAULT NULL,
  `periode` varchar(255) DEFAULT NULL,
  `tglmulai` date DEFAULT NULL,
  `tglselesai` date DEFAULT NULL,
  `tgl_input` datetime DEFAULT NULL,
  `user_input` varchar(255) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_planing_audit`
--

INSERT INTO `sp_planing_audit` (`id`, `id_unit`, `id_ketuatim`, `periode`, `tglmulai`, `tglselesai`, `tgl_input`, `user_input`, `tgl_update`, `user_update`) VALUES
(2, 1, 6, '2018', '2018-07-01', '2018-08-29', '2018-08-17 00:00:00', 'admin', NULL, NULL),
(3, 4, 4, '2018', '2018-07-30', '2018-08-20', '2018-08-17 00:00:00', 'admin', '2018-08-17 00:00:00', 'admin'),
(5, 2, 4, '2018', '2018-09-02', '2018-09-05', '2018-09-05 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sp_planing_audit_anggota`
--

CREATE TABLE IF NOT EXISTS `sp_planing_audit_anggota` (
  `id` int(11) NOT NULL,
  `id_planingaudit` int(11) DEFAULT NULL,
  `id_anggota` int(11) DEFAULT NULL,
  `tgl_input` datetime DEFAULT NULL,
  `user_input` varchar(255) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_planing_audit_anggota`
--

INSERT INTO `sp_planing_audit_anggota` (`id`, `id_planingaudit`, `id_anggota`, `tgl_input`, `user_input`, `tgl_update`, `user_update`) VALUES
(5, 5, 3, '2018-09-05 00:00:00', 'admin', NULL, NULL),
(6, 5, 5, '2018-09-05 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sp_planing_audit_auditie`
--

CREATE TABLE IF NOT EXISTS `sp_planing_audit_auditie` (
  `id` int(11) NOT NULL,
  `id_planingaudit` int(11) DEFAULT NULL,
  `id_auditie` int(11) DEFAULT NULL,
  `tgl_input` datetime DEFAULT NULL,
  `user_input` varchar(255) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_planing_audit_auditie`
--

INSERT INTO `sp_planing_audit_auditie` (`id`, `id_planingaudit`, `id_auditie`, `tgl_input`, `user_input`, `tgl_update`, `user_update`) VALUES
(5, 5, 6, '2018-09-05 00:00:00', 'admin', NULL, NULL),
(6, 5, 5, '2018-09-05 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sp_planing_audit_standar`
--

CREATE TABLE IF NOT EXISTS `sp_planing_audit_standar` (
  `id` int(11) NOT NULL,
  `id_planingaudit` int(11) DEFAULT NULL,
  `id_standar` int(11) DEFAULT NULL,
  `tgl_input` datetime DEFAULT NULL,
  `user_input` varchar(255) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_planing_audit_standar`
--

INSERT INTO `sp_planing_audit_standar` (`id`, `id_planingaudit`, `id_standar`, `tgl_input`, `user_input`, `tgl_update`, `user_update`) VALUES
(4, 3, 4, '2018-08-21 00:00:00', 'admin', NULL, NULL),
(5, 5, 4, '2018-09-05 00:00:00', 'admin', NULL, NULL),
(6, 5, 5, '2018-09-05 00:00:00', 'admin', NULL, NULL),
(7, 3, 2, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sp_renstra`
--

CREATE TABLE IF NOT EXISTS `sp_renstra` (
  `id` int(11) NOT NULL,
  `nm_dokumen` varchar(200) DEFAULT NULL,
  `file_upload` varchar(200) DEFAULT NULL,
  `tgl_awal` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  `tgl_publish` date DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_renstra`
--

INSERT INTO `sp_renstra` (`id`, `nm_dokumen`, `file_upload`, `tgl_awal`, `tgl_akhir`, `tgl_publish`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`) VALUES
(1, 'Dokumen Renstra 1', 'dokrenstra/450744gbr.png', '2018-07-03', '2018-07-24', '2018-07-09', 'admin', '2018-07-29', NULL, NULL, 1),
(3, 'Dokumen Renstra 2', 'dokrenstra/976654Picture1.png', '2018-06-26', '2018-06-27', '2018-06-27', 'admin', '2018-07-29', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_renstra_det`
--

CREATE TABLE IF NOT EXISTS `sp_renstra_det` (
  `id` int(11) NOT NULL,
  `id_renstra` int(11) DEFAULT NULL,
  `nomor` varchar(100) DEFAULT NULL,
  `uraian` varchar(200) DEFAULT NULL,
  `target` int(11) DEFAULT NULL,
  `tahun_awal` int(11) DEFAULT NULL,
  `tahun_tengah` int(11) DEFAULT NULL,
  `tahun_akhir` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_renstra_det`
--

INSERT INTO `sp_renstra_det` (`id`, `id_renstra`, `nomor`, `uraian`, `target`, `tahun_awal`, `tahun_tengah`, `tahun_akhir`, `user_input`, `tgl_input`, `user_update`, `tgl_update`) VALUES
(5, 3, '1', 'test', 20, 2001, 2009, 2019, 'admin', '2018-07-29', NULL, NULL),
(6, 3, '2', 'test', 3, 203, 22, 222, 'admin', '2018-07-29', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sp_sarmut`
--

CREATE TABLE IF NOT EXISTS `sp_sarmut` (
  `id` int(11) NOT NULL,
  `id_mkp_kegiatan` int(11) DEFAULT NULL,
  `kegiatan` text,
  `target_kuantitatif` int(11) DEFAULT NULL,
  `target_kualitatif` varchar(200) DEFAULT NULL,
  `kinerja_tahunlalu` varchar(200) DEFAULT NULL,
  `angaran` int(11) DEFAULT NULL,
  `id_unit` int(11) DEFAULT NULL,
  `jenis` smallint(6) DEFAULT NULL COMMENT '1=unit,2=prodi,3=direktorat,4=fakultas',
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `id_tahunakademik` int(11) DEFAULT NULL,
  `pelaksana` varchar(200) DEFAULT NULL,
  `kode_rencanaapprove` varchar(255) DEFAULT NULL,
  `kode_approve` varchar(255) DEFAULT NULL,
  `presentase_realisasi` int(11) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sp_sarmut_realisasi`
--

CREATE TABLE IF NOT EXISTS `sp_sarmut_realisasi` (
  `id` int(11) NOT NULL,
  `id_sarmut` int(11) DEFAULT NULL,
  `tgl_kegiatan` date DEFAULT NULL,
  `realisasi` varchar(200) DEFAULT NULL,
  `file_upload` varchar(200) DEFAULT NULL,
  `ket_proges` varchar(250) DEFAULT NULL,
  `presentase` int(10) DEFAULT NULL COMMENT 'mengcu ke kolom target kuantitatif',
  `kendala` varchar(200) DEFAULT NULL,
  `tindakan_korelatif` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sp_sop`
--

CREATE TABLE IF NOT EXISTS `sp_sop` (
  `id` int(11) NOT NULL,
  `nm_sop` varchar(200) DEFAULT NULL,
  `id_standar` int(11) DEFAULT NULL,
  `file_upload` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_sop`
--

INSERT INTO `sp_sop` (`id`, `nm_sop`, `id_standar`, `file_upload`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`) VALUES
(1, 'SOP 1', 2, 'doksop/116394gbr.png', 'admin', '2018-07-29', NULL, NULL, 1),
(2, 'SOP2', 2, 'doksop/', 'admin', '2018-07-29', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_standar`
--

CREATE TABLE IF NOT EXISTS `sp_standar` (
  `id` int(11) NOT NULL,
  `id_renstra` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `no_dokumen` varchar(200) DEFAULT NULL,
  `nm_dokumen` varchar(200) DEFAULT NULL,
  `file_upload` varchar(200) DEFAULT NULL,
  `no_refisi` varchar(200) DEFAULT NULL,
  `tgl_publish` date DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_standar`
--

INSERT INTO `sp_standar` (`id`, `id_renstra`, `parent_id`, `no_dokumen`, `nm_dokumen`, `file_upload`, `no_refisi`, `tgl_publish`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`) VALUES
(2, 1, 4, '123', 'Dokumen Standar 2', 'dokstandar/689758Picture1.png', '33', '2018-07-31', 'admin', '2018-07-29', NULL, NULL, 1),
(4, 1, 0, '234', 'Dokumen Standar 1', 'dokstandar/322296gbr.png', 'ss', '2018-08-01', 'admin', '2018-07-29', NULL, NULL, 1),
(5, 1, 0, 'd23', 'dokumen standar 5', 'dokstandar/461853gbr.png', '-', '2018-07-31', 'admin', '2018-07-30', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_standar_det`
--

CREATE TABLE IF NOT EXISTS `sp_standar_det` (
  `id` int(11) NOT NULL,
  `id_standar` int(11) DEFAULT NULL,
  `nomor` varchar(100) DEFAULT NULL,
  `uraian` varchar(200) DEFAULT NULL,
  `indikaor` int(11) DEFAULT NULL,
  `target_waktu` int(11) DEFAULT NULL,
  `file_upload` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_standar_det`
--

INSERT INTO `sp_standar_det` (`id`, `id_standar`, `nomor`, `uraian`, `indikaor`, `target_waktu`, `file_upload`, `user_input`, `tgl_input`, `user_update`, `tgl_update`) VALUES
(3, 4, '1', '11', 22, 33, 'dokstandar/775726Picture1.png', 'admin', '2018-07-29', NULL, NULL),
(4, 5, '1', 'rencana standar 1', 123, 2019, NULL, 'admin', '2018-07-30', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sp_standar_unitdet`
--

CREATE TABLE IF NOT EXISTS `sp_standar_unitdet` (
  `id` int(11) NOT NULL,
  `id_standar_det` int(11) DEFAULT NULL,
  `id_unit` int(11) DEFAULT NULL,
  `jenis` smallint(1) DEFAULT NULL COMMENT '1=unit,2=prodi,3=direktorat,4=fakultas',
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sp_statuta`
--

CREATE TABLE IF NOT EXISTS `sp_statuta` (
  `id` int(11) NOT NULL,
  `kode_dokumen` varchar(50) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `nama_dokumen` varchar(200) DEFAULT NULL,
  `file_upload` varchar(200) DEFAULT NULL,
  `no_refisi` varchar(20) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_statuta`
--

INSERT INTO `sp_statuta` (`id`, `kode_dokumen`, `parent_id`, `nama_dokumen`, `file_upload`, `no_refisi`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`) VALUES
(2, 'D1', 0, 'Doukumen 1', 'dokstatuta/307342gbr.png', '123', 'admin', '2018-07-28', NULL, NULL, 1),
(3, 'D11', 2, 'Dokumen Revisi 1', 'dokstatuta/244842gbr.png', 'R123', 'admin', '2018-07-29', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_tindak_korektif_hasil`
--

CREATE TABLE IF NOT EXISTS `sp_tindak_korektif_hasil` (
  `id` int(11) NOT NULL,
  `id_unit` int(11) DEFAULT NULL,
  `no_ptk` varchar(255) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  `penyelesaian` text,
  `realisasi` varchar(255) DEFAULT NULL,
  `pj_ptk` varchar(255) DEFAULT NULL,
  `tgl_input` datetime DEFAULT NULL,
  `user_input` varchar(255) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_tindak_korektif_hasil`
--

INSERT INTO `sp_tindak_korektif_hasil` (`id`, `id_unit`, `no_ptk`, `kategori`, `penyelesaian`, `realisasi`, `pj_ptk`, `tgl_input`, `user_input`, `tgl_update`, `user_update`) VALUES
(1, 4, 'Knsu2', 'minor', 'selesai', 'real', 'dia', '2018-08-29 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `standar`
--

CREATE TABLE IF NOT EXISTS `standar` (
  `id` int(11) NOT NULL,
  `id_renstra` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `no_dokumen` varchar(200) DEFAULT NULL,
  `nama_dokumen` varchar(200) DEFAULT NULL,
  `file_upload` varchar(200) DEFAULT NULL,
  `no_refisi` varchar(200) DEFAULT NULL,
  `tgl_pblish` date DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `standar_det`
--

CREATE TABLE IF NOT EXISTS `standar_det` (
  `id` int(11) NOT NULL,
  `id_standar` int(11) DEFAULT NULL,
  `nomor` varchar(100) DEFAULT NULL,
  `uraian` varchar(200) DEFAULT NULL,
  `indikaor` int(11) DEFAULT NULL,
  `target_waktu` int(11) DEFAULT NULL,
  `file_upload` varchar(200) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `standar_unitdet`
--

CREATE TABLE IF NOT EXISTS `standar_unitdet` (
  `id` int(11) NOT NULL,
  `id_standar_det` int(11) DEFAULT NULL,
  `id_unit` int(11) DEFAULT NULL,
  `jenis` smallint(1) DEFAULT NULL COMMENT '1=unit,2=prodi,3=direktorat,4=fakultas',
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `statuta`
--

CREATE TABLE IF NOT EXISTS `statuta` (
  `id` int(11) NOT NULL,
  `kode_dokumen` varchar(50) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `nama_dokumen` varchar(200) DEFAULT NULL,
  `file_upload` varchar(200) DEFAULT NULL,
  `no_refisi` varchar(20) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tahun_akademik`
--

CREATE TABLE IF NOT EXISTS `tahun_akademik` (
  `id` int(11) NOT NULL,
  `nm_tahun_akademik` varchar(20) DEFAULT NULL,
  `semester` smallint(1) DEFAULT '1' COMMENT '1=genap,2=ganjil',
  `keterangan` varchar(20) DEFAULT NULL,
  `user_input` varchar(200) DEFAULT NULL,
  `tgl_input` date DEFAULT NULL,
  `user_update` varchar(200) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `status` smallint(1) DEFAULT '1',
  `kd_server` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tahun_akademik`
--

INSERT INTO `tahun_akademik` (`id`, `nm_tahun_akademik`, `semester`, `keterangan`, `user_input`, `tgl_input`, `user_update`, `tgl_update`, `status`, `kd_server`) VALUES
(1, '2016-2017', 1, 'Ganjil', NULL, NULL, NULL, NULL, 1, NULL),
(2, '2017-2018', 1, 'Genap', NULL, NULL, NULL, NULL, 1, NULL),
(3, '2018-2019', 1, 'Ganjil', 'admin', '2018-05-12', 'admin', '2018-07-29', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(6) unsigned NOT NULL,
  `user_c` int(6) DEFAULT NULL,
  `user_u` int(6) DEFAULT NULL,
  `tgl_c` date DEFAULT NULL,
  `tgl_u` date DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `username` varchar(200) NOT NULL,
  `nm_user` varchar(200) DEFAULT NULL,
  `password` varchar(200) NOT NULL,
  `idrool` int(10) NOT NULL,
  `id_level` int(11) DEFAULT NULL,
  `id_rektorat` int(11) DEFAULT NULL,
  `id_direktorat` int(11) DEFAULT NULL,
  `id_unit` int(11) DEFAULT NULL,
  `id_prodi` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_c`, `user_u`, `tgl_c`, `tgl_u`, `status`, `username`, `nm_user`, `password`, `idrool`, `id_level`, `id_rektorat`, `id_direktorat`, `id_unit`, `id_prodi`) VALUES
(1, 0, 3, '2016-05-03', '2017-03-08', 1, 'user', NULL, 'f673fa9f3695f7f985e5a6ba6f72f40d', 1, 3, NULL, NULL, NULL, NULL),
(2, 0, 3, '2016-08-01', '2017-03-01', 1, 'admindua', NULL, 'c58faf53ed72ab132b6e922090e8fd5c', 3, 2, NULL, NULL, NULL, NULL),
(3, 0, 3, '2016-08-01', '2017-02-23', 1, 'admin', NULL, '8dcc28cf85cccf4fc7c19793c909f37f', 3, 1, NULL, NULL, NULL, NULL),
(4, 3, 3, '2017-02-24', '2017-02-27', 1, 'userdua', NULL, '50795beaafd4790b33fdc994ab056320', 3, 8, NULL, NULL, NULL, NULL),
(5, 3, 3, '2018-07-27', '2018-08-09', 1, 'test1', NULL, '733dafb1c672d06b49a4e8de54b58e25', 2, 2, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE IF NOT EXISTS `user_menu` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `tampil` tinyint(1) DEFAULT NULL,
  `simpan` tinyint(1) DEFAULT NULL,
  `ubah` tinyint(1) DEFAULT NULL,
  `hapus` tinyint(1) DEFAULT NULL,
  `approve` tinyint(1) DEFAULT NULL,
  `cetak` tinyint(1) DEFAULT NULL,
  `aktivasi` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1206 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `id_user`, `id_menu`, `tampil`, `simpan`, `ubah`, `hapus`, `approve`, `cetak`, `aktivasi`) VALUES
(389, 1, 11, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(390, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(391, 1, 2, 1, 1, 1, 1, 1, 1, 1),
(392, 1, 5, 1, 1, 1, 1, 1, 1, 1),
(393, 1, 7, 1, 1, 1, 1, 1, 1, 1),
(394, 1, 15, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(395, 1, 17, 1, 1, 1, 1, 1, 1, 1),
(396, 1, 18, 1, 1, 1, 1, 1, 1, 1),
(397, 1, 20, 1, 1, 1, 1, 1, 1, 1),
(398, 2, 11, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(399, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(400, 2, 2, 1, 1, 1, 1, 1, 1, 1),
(401, 2, 5, 1, 1, 1, 1, 1, 1, 1),
(402, 2, 7, 1, 1, 1, 1, 1, 1, 1),
(403, 2, 8, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(404, 2, 9, 1, 1, 1, 1, 1, 1, 1),
(405, 2, 10, 1, 1, 1, 1, 1, 1, 1),
(406, 2, 12, 1, 1, 1, 1, 1, 1, 1),
(407, 2, 13, 1, 1, 1, 1, 1, 1, 1),
(408, 2, 14, 1, 1, 1, 1, 1, 1, 1),
(409, 2, 16, 1, 1, 1, 1, 1, 1, 1),
(410, 2, 15, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(411, 2, 17, 1, 1, 1, 1, 1, 1, 1),
(412, 2, 18, 1, 1, 1, 1, 1, 1, 1),
(413, 2, 20, 1, 1, 1, 1, 1, 1, 1),
(631, 4, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(632, 4, 40, 1, 1, 1, 1, 0, 0, NULL),
(633, 4, 2, 1, 1, 1, 1, 0, 0, NULL),
(634, 4, 5, 1, 1, 1, 1, 0, 0, NULL),
(635, 4, 7, 1, 1, 1, 1, 0, 0, NULL),
(636, 4, 33, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(637, 4, 58, 1, 1, 0, 0, 0, 0, NULL),
(638, 4, 63, 1, 1, 0, 0, 0, 0, NULL),
(639, 4, 59, 1, 1, 0, 0, 0, 0, NULL),
(640, 4, 64, 1, 1, 0, 0, 0, 0, NULL),
(641, 4, 65, 1, 1, 0, 0, 0, 0, NULL),
(642, 4, 66, 1, 1, 0, 0, 0, 0, NULL),
(643, 4, 67, 1, 1, 0, 0, 0, 0, NULL),
(644, 4, 34, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(645, 4, 37, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(646, 4, 57, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(647, 4, 41, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(937, 5, 76, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(938, 5, 34, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(939, 5, 57, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(940, 5, 43, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(941, 5, 44, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(942, 5, 45, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(943, 5, 46, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(944, 5, 47, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(1134, 3, 76, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(1135, 3, 1, 1, 1, 1, 1, 1, 1, NULL),
(1136, 3, 40, 1, 1, 1, 1, 1, 1, NULL),
(1137, 3, 2, 1, 1, 1, 1, 1, 1, NULL),
(1138, 3, 5, 1, 1, 1, 1, 1, 1, NULL),
(1139, 3, 7, 1, 1, 1, 1, 1, 1, NULL),
(1140, 3, 33, 1, 1, 1, 1, 1, 1, NULL),
(1141, 3, 58, 1, 1, 1, 1, 1, 1, NULL),
(1142, 3, 63, 1, 1, 1, 1, 1, 1, NULL),
(1143, 3, 59, 1, 1, 1, 1, 1, 1, NULL),
(1144, 3, 64, 1, 1, 1, 1, 1, 1, NULL),
(1145, 3, 65, 1, 1, 1, 1, 1, 1, NULL),
(1146, 3, 66, 1, 1, 1, 1, 1, 1, NULL),
(1147, 3, 67, 1, 1, 1, 1, 1, 1, NULL),
(1148, 3, 34, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(1149, 3, 35, 1, 1, 1, 1, 1, 1, NULL),
(1150, 3, 73, 1, 1, 1, 1, 1, 1, NULL),
(1151, 3, 36, 1, 1, 1, 1, 1, 1, NULL),
(1152, 3, 68, 1, 1, 1, 1, 1, 1, NULL),
(1153, 3, 74, 1, 1, 1, 1, 1, 1, NULL),
(1154, 3, 69, 1, 1, 1, 1, 1, 1, NULL),
(1155, 3, 70, 1, 1, 1, 1, 1, 1, NULL),
(1156, 3, 71, 1, 1, 1, 1, 1, 1, NULL),
(1157, 3, 37, 1, 1, 1, 1, 1, 1, NULL),
(1158, 3, 38, 1, 1, 1, 1, 1, 1, NULL),
(1159, 3, 103, 1, 1, 1, 1, 1, 1, NULL),
(1160, 3, 39, 1, 1, 1, 1, 1, 1, NULL),
(1161, 3, 72, 1, 1, 1, 1, 1, 1, NULL),
(1162, 3, 57, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(1163, 3, 60, 1, 1, 1, 1, 1, 1, NULL),
(1164, 3, 61, 1, 1, 1, 1, 1, 1, NULL),
(1165, 3, 104, 1, 1, 1, 1, 1, 1, NULL),
(1166, 3, 105, 1, 1, 1, 1, 1, 1, NULL),
(1167, 3, 106, 1, 1, 1, 1, 1, 1, NULL),
(1168, 3, 43, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(1169, 3, 48, 1, 1, 1, 1, 1, 1, NULL),
(1170, 3, 84, 1, 1, 1, 1, 1, 1, NULL),
(1171, 3, 85, 1, 1, 1, 1, 1, 1, NULL),
(1172, 3, 49, 1, 1, 1, 1, 1, 1, NULL),
(1173, 3, 50, 1, 1, 1, 1, 1, 1, NULL),
(1174, 3, 62, 1, 1, 1, 1, 1, 1, NULL),
(1175, 3, 51, 1, 1, 1, 1, 1, 1, NULL),
(1176, 3, 44, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(1177, 3, 90, 1, 1, 1, 1, 1, 1, NULL),
(1178, 3, 52, 1, 1, 1, 1, 1, 1, NULL),
(1179, 3, 91, 1, 1, 1, 1, 1, 1, NULL),
(1180, 3, 92, 1, 1, 1, 1, 1, 1, NULL),
(1181, 3, 98, 1, 1, 1, 1, 1, 1, NULL),
(1182, 3, 99, 1, 1, 1, 1, 1, 1, NULL),
(1183, 3, 100, 1, 1, 1, 1, 1, 1, NULL),
(1184, 3, 101, 1, 1, 1, 1, 1, 1, NULL),
(1185, 3, 102, 1, 1, 1, 1, 1, 1, NULL),
(1186, 3, 53, 1, 1, 1, 1, 1, 1, NULL),
(1187, 3, 54, 1, 1, 1, 1, 1, 1, NULL),
(1188, 3, 55, 1, 1, 1, 1, 1, 1, NULL),
(1189, 3, 45, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(1190, 3, 56, 1, 1, 1, 1, 1, 1, NULL),
(1191, 3, 77, 1, 1, 1, 1, 1, 1, NULL),
(1192, 3, 78, 1, 1, 1, 1, 1, 1, NULL),
(1193, 3, 79, 1, 1, 1, 1, 1, 1, NULL),
(1194, 3, 46, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(1195, 3, 86, 1, 1, 1, 1, 1, 1, NULL),
(1196, 3, 87, 1, 1, 1, 1, 1, 1, NULL),
(1197, 3, 88, 1, 1, 1, 1, 1, 1, NULL),
(1198, 3, 89, 1, 1, 1, 1, 1, 1, NULL),
(1199, 3, 47, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(1200, 3, 80, 1, 1, 1, 1, 1, 1, NULL),
(1201, 3, 81, 1, 1, 1, 1, 1, 1, NULL),
(1202, 3, 82, 1, 1, 1, 1, 1, 1, NULL),
(1203, 3, 83, 1, 1, 1, 1, 1, 1, NULL),
(1204, 3, 107, 1, 1, 1, 1, 1, 1, NULL),
(1205, 3, 108, 1, 1, 1, 1, 1, 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `br_identitas_borang`
--
ALTER TABLE `br_identitas_borang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `br_identitas_borang_dosen`
--
ALTER TABLE `br_identitas_borang_dosen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `br_parameterborang3a`
--
ALTER TABLE `br_parameterborang3a`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `br_standardua`
--
ALTER TABLE `br_standardua`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `br_standardua_kebijakan`
--
ALTER TABLE `br_standardua_kebijakan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `br_standarsatu`
--
ALTER TABLE `br_standarsatu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `br_standarsatu_kebijakan`
--
ALTER TABLE `br_standarsatu_kebijakan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `formulir`
--
ALTER TABLE `formulir`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kebijakan_spmi`
--
ALTER TABLE `kebijakan_spmi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_group`
--
ALTER TABLE `menu_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_group_copy`
--
ALTER TABLE `menu_group_copy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_grup_det`
--
ALTER TABLE `menu_grup_det`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_grup_heder`
--
ALTER TABLE `menu_grup_heder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mkp`
--
ALTER TABLE `mkp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mkp_kegiatan`
--
ALTER TABLE `mkp_kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mkp_target`
--
ALTER TABLE `mkp_target`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mkp_unitdet`
--
ALTER TABLE `mkp_unitdet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_direktorat`
--
ALTER TABLE `ms_direktorat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_fakultas`
--
ALTER TABLE `ms_fakultas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_fakultas_hapus`
--
ALTER TABLE `ms_fakultas_hapus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_jenjang`
--
ALTER TABLE `ms_jenjang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_jenjangprodi`
--
ALTER TABLE `ms_jenjangprodi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_jurusan`
--
ALTER TABLE `ms_jurusan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_jurusan_hapus`
--
ALTER TABLE `ms_jurusan_hapus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_karyawan`
--
ALTER TABLE `ms_karyawan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_level`
--
ALTER TABLE `ms_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_prodi`
--
ALTER TABLE `ms_prodi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_rektorat`
--
ALTER TABLE `ms_rektorat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_unit`
--
ALTER TABLE `ms_unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `renstra`
--
ALTER TABLE `renstra`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `renstra_det`
--
ALTER TABLE `renstra_det`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indexes for table `rol_menu`
--
ALTER TABLE `rol_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sarmut`
--
ALTER TABLE `sarmut`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sarmut_realisasi`
--
ALTER TABLE `sarmut_realisasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sop`
--
ALTER TABLE `sop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_formulir`
--
ALTER TABLE `sp_formulir`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_formulir_audit`
--
ALTER TABLE `sp_formulir_audit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_formulir_audit_doc`
--
ALTER TABLE `sp_formulir_audit_doc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_kebijakan_spmi`
--
ALTER TABLE `sp_kebijakan_spmi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_laporan_audit`
--
ALTER TABLE `sp_laporan_audit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_laporan_audit_bidang`
--
ALTER TABLE `sp_laporan_audit_bidang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_laporan_audit_kts_ob`
--
ALTER TABLE `sp_laporan_audit_kts_ob`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_mkp`
--
ALTER TABLE `sp_mkp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_mkp_kegiatan`
--
ALTER TABLE `sp_mkp_kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_mkp_standar`
--
ALTER TABLE `sp_mkp_standar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_mkp_target`
--
ALTER TABLE `sp_mkp_target`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_mkp_unitdet`
--
ALTER TABLE `sp_mkp_unitdet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_persiapan_formulir`
--
ALTER TABLE `sp_persiapan_formulir`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_persiapan_formulir_doc`
--
ALTER TABLE `sp_persiapan_formulir_doc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_persiapan_pelaksanaan`
--
ALTER TABLE `sp_persiapan_pelaksanaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_planing_audit`
--
ALTER TABLE `sp_planing_audit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_planing_audit_anggota`
--
ALTER TABLE `sp_planing_audit_anggota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_planing_audit_auditie`
--
ALTER TABLE `sp_planing_audit_auditie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_planing_audit_standar`
--
ALTER TABLE `sp_planing_audit_standar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_renstra`
--
ALTER TABLE `sp_renstra`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_renstra_det`
--
ALTER TABLE `sp_renstra_det`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_sarmut`
--
ALTER TABLE `sp_sarmut`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_sarmut_realisasi`
--
ALTER TABLE `sp_sarmut_realisasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_sop`
--
ALTER TABLE `sp_sop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_standar`
--
ALTER TABLE `sp_standar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_standar_det`
--
ALTER TABLE `sp_standar_det`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_standar_unitdet`
--
ALTER TABLE `sp_standar_unitdet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_statuta`
--
ALTER TABLE `sp_statuta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sp_tindak_korektif_hasil`
--
ALTER TABLE `sp_tindak_korektif_hasil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `standar`
--
ALTER TABLE `standar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `standar_det`
--
ALTER TABLE `standar_det`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `standar_unitdet`
--
ALTER TABLE `standar_unitdet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuta`
--
ALTER TABLE `statuta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tahun_akademik`
--
ALTER TABLE `tahun_akademik`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `br_identitas_borang`
--
ALTER TABLE `br_identitas_borang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `br_identitas_borang_dosen`
--
ALTER TABLE `br_identitas_borang_dosen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `br_parameterborang3a`
--
ALTER TABLE `br_parameterborang3a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `br_standardua`
--
ALTER TABLE `br_standardua`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `br_standardua_kebijakan`
--
ALTER TABLE `br_standardua_kebijakan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `br_standarsatu`
--
ALTER TABLE `br_standarsatu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `br_standarsatu_kebijakan`
--
ALTER TABLE `br_standarsatu_kebijakan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `formulir`
--
ALTER TABLE `formulir`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kebijakan_spmi`
--
ALTER TABLE `kebijakan_spmi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT for table `menu_group`
--
ALTER TABLE `menu_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `menu_group_copy`
--
ALTER TABLE `menu_group_copy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `menu_grup_det`
--
ALTER TABLE `menu_grup_det`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `menu_grup_heder`
--
ALTER TABLE `menu_grup_heder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `mkp`
--
ALTER TABLE `mkp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mkp_kegiatan`
--
ALTER TABLE `mkp_kegiatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mkp_target`
--
ALTER TABLE `mkp_target`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mkp_unitdet`
--
ALTER TABLE `mkp_unitdet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_direktorat`
--
ALTER TABLE `ms_direktorat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ms_fakultas`
--
ALTER TABLE `ms_fakultas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_fakultas_hapus`
--
ALTER TABLE `ms_fakultas_hapus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_jenjang`
--
ALTER TABLE `ms_jenjang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ms_jenjangprodi`
--
ALTER TABLE `ms_jenjangprodi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ms_jurusan`
--
ALTER TABLE `ms_jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_jurusan_hapus`
--
ALTER TABLE `ms_jurusan_hapus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_karyawan`
--
ALTER TABLE `ms_karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ms_level`
--
ALTER TABLE `ms_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ms_prodi`
--
ALTER TABLE `ms_prodi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `ms_rektorat`
--
ALTER TABLE `ms_rektorat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ms_unit`
--
ALTER TABLE `ms_unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `renstra`
--
ALTER TABLE `renstra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `renstra_det`
--
ALTER TABLE `renstra_det`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rol`
--
ALTER TABLE `rol`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `rol_menu`
--
ALTER TABLE `rol_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=412;
--
-- AUTO_INCREMENT for table `sarmut`
--
ALTER TABLE `sarmut`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sarmut_realisasi`
--
ALTER TABLE `sarmut_realisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sop`
--
ALTER TABLE `sop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sp_formulir`
--
ALTER TABLE `sp_formulir`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sp_formulir_audit`
--
ALTER TABLE `sp_formulir_audit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `sp_formulir_audit_doc`
--
ALTER TABLE `sp_formulir_audit_doc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `sp_kebijakan_spmi`
--
ALTER TABLE `sp_kebijakan_spmi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sp_laporan_audit`
--
ALTER TABLE `sp_laporan_audit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sp_laporan_audit_bidang`
--
ALTER TABLE `sp_laporan_audit_bidang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sp_laporan_audit_kts_ob`
--
ALTER TABLE `sp_laporan_audit_kts_ob`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `sp_mkp`
--
ALTER TABLE `sp_mkp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sp_mkp_kegiatan`
--
ALTER TABLE `sp_mkp_kegiatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sp_mkp_standar`
--
ALTER TABLE `sp_mkp_standar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sp_mkp_target`
--
ALTER TABLE `sp_mkp_target`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sp_mkp_unitdet`
--
ALTER TABLE `sp_mkp_unitdet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sp_persiapan_formulir`
--
ALTER TABLE `sp_persiapan_formulir`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `sp_persiapan_formulir_doc`
--
ALTER TABLE `sp_persiapan_formulir_doc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `sp_persiapan_pelaksanaan`
--
ALTER TABLE `sp_persiapan_pelaksanaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sp_planing_audit`
--
ALTER TABLE `sp_planing_audit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sp_planing_audit_anggota`
--
ALTER TABLE `sp_planing_audit_anggota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sp_planing_audit_auditie`
--
ALTER TABLE `sp_planing_audit_auditie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sp_planing_audit_standar`
--
ALTER TABLE `sp_planing_audit_standar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sp_renstra`
--
ALTER TABLE `sp_renstra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sp_renstra_det`
--
ALTER TABLE `sp_renstra_det`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sp_sarmut`
--
ALTER TABLE `sp_sarmut`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sp_sarmut_realisasi`
--
ALTER TABLE `sp_sarmut_realisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sp_sop`
--
ALTER TABLE `sp_sop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sp_standar`
--
ALTER TABLE `sp_standar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sp_standar_det`
--
ALTER TABLE `sp_standar_det`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sp_standar_unitdet`
--
ALTER TABLE `sp_standar_unitdet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sp_statuta`
--
ALTER TABLE `sp_statuta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sp_tindak_korektif_hasil`
--
ALTER TABLE `sp_tindak_korektif_hasil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `standar`
--
ALTER TABLE `standar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `standar_det`
--
ALTER TABLE `standar_det`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `standar_unitdet`
--
ALTER TABLE `standar_unitdet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `statuta`
--
ALTER TABLE `statuta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tahun_akademik`
--
ALTER TABLE `tahun_akademik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(6) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1206;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
