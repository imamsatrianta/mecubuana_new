/*
 Navicat Premium Data Transfer

 Source Server         : JEM
 Source Server Type    : MySQL
 Source Server Version : 50621
 Source Host           : localhost:3306
 Source Schema         : spmi_19082018

 Target Server Type    : MySQL
 Target Server Version : 50621
 File Encoding         : 65001

 Date: 27/10/2018 19:48:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for br_identitas_borang
-- ----------------------------
DROP TABLE IF EXISTS `br_identitas_borang`;
CREATE TABLE `br_identitas_borang`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jenjang` int(11) NULL DEFAULT NULL,
  `id_prodi` int(11) NULL DEFAULT NULL,
  `tgl` date NULL DEFAULT NULL,
  `nm_perguruantingi` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_sk_pendirian` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_sk_pendirian` date NULL DEFAULT NULL,
  `pejabat_penandatangan_sk` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bulan_tahun_peneyelengara` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_skijin_op` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_sk_op` date NULL DEFAULT NULL,
  `peringkat_ak_terakhir` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_sk_banpt` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_ps` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_telp_ps` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_fak_ps` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hp_dan_email_ps` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of br_identitas_borang
-- ----------------------------
INSERT INTO `br_identitas_borang` VALUES (2, 2, 6, '2018-08-09', 'd', 'ds', '2018-09-02', 'sd', 'sd', 'sd', '2018-08-01', 'sd', 'sd', 'sd', 'sd', 'sd', 'd', 'admin', '2018-08-09', 'admin', '2018-08-09');

-- ----------------------------
-- Table structure for br_identitas_borang_dosen
-- ----------------------------
DROP TABLE IF EXISTS `br_identitas_borang_dosen`;
CREATE TABLE `br_identitas_borang_dosen`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_identitas` int(11) NULL DEFAULT NULL,
  `id_dosen` int(11) NULL DEFAULT NULL,
  `tgl_pengisian` date NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of br_identitas_borang_dosen
-- ----------------------------
INSERT INTO `br_identitas_borang_dosen` VALUES (2, 2, 2, '2018-08-14', 'admin', '2018-08-09', NULL, NULL);
INSERT INTO `br_identitas_borang_dosen` VALUES (3, 2, 1, '2018-08-15', 'admin', '2018-08-12', NULL, NULL);

-- ----------------------------
-- Table structure for br_parameterborang3a
-- ----------------------------
DROP TABLE IF EXISTS `br_parameterborang3a`;
CREATE TABLE `br_parameterborang3a`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `standar` smallint(2) NULL DEFAULT NULL,
  `poin` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `judul` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `uraian` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `jenis` smallint(1) NULL DEFAULT 1 COMMENT '1=form ambil dari parameter(standar),2=custome',
  `parent_id` int(11) NULL DEFAULT NULL,
  `id_jenjangprodi` int(11) NULL DEFAULT NULL,
  `level` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of br_parameterborang3a
-- ----------------------------
INSERT INTO `br_parameterborang3a` VALUES (1, 1, '1.1.1', 'Visi misi dan tujuan', 'Jelaskan mekanisme penyusunan visi, misi, tujuan dan sasaran program studi, serta pihak-pihak yang dilibatkan', 1, 0, 1, 1, 'admin', '2018-08-09', 'admin', '2018-08-09');
INSERT INTO `br_parameterborang3a` VALUES (2, 1, '1.1.1.a', ' Visi program studi', ' Visi program studi', 1, 1, 1, 1, 'admin', '2018-08-09', 'admin', '2018-08-09');
INSERT INTO `br_parameterborang3a` VALUES (3, 1, '1.1.1.b', 'Misi program studi', 'Misi program studi', 1, 1, 1, 1, 'admin', '2018-08-09', NULL, NULL);
INSERT INTO `br_parameterborang3a` VALUES (4, 1, '1.1.1.c', 'Tujuan program studi', 'Tujuan program studi', 1, 1, 1, 1, 'admin', '2018-08-09', NULL, NULL);
INSERT INTO `br_parameterborang3a` VALUES (5, 1, '1.1.2', 'sasaran', 'Sasaran dan strategi pencapaian', 1, 0, 1, 1, 'admin', '2018-08-09', NULL, NULL);
INSERT INTO `br_parameterborang3a` VALUES (6, 1, '1.2', 'Sosialisasi ', 'Uraikan upaya penyebaran/sosialisasi visi, misi dan tujuan program studi serta pemahaman sivitas akademika (dosen dan mahasiswa) dan tenaga kependidikan.', 1, 0, 1, 1, 'admin', '2018-08-09', NULL, NULL);
INSERT INTO `br_parameterborang3a` VALUES (7, 2, '2.1', 'Sistem Tata Pamong', 'Sistem tata pamong berjalan secara efektif melalui mekanisme yang disepakati bersama, serta dapat memelihara dan mengakomodasi semua unsur, fungsi, dan peran dalam program studi. Tata pamong didukung dengan budaya organisasi yang dicerminkan dengan ada dan tegaknya aturan, tatacara pemilihan pimpinan, etika dosen, etika mahasiswa, etika tenaga kependidikan, sistem penghargaan dan sanksi serta pedoman dan prosedur pelayanan (administrasi, perpustakaan, laboratorium, dan studio). Sistem tata pamong (input, proses, output dan outcome serta lingkungan eksternal yang menjamin terlaksananya tata pamong yang baik) harus diformulasikan, disosialisasikan, dilaksanakan,  dipantau dan dievaluasi dengan peraturan dan prosedur yang jelas. \r\n\r\nUraikan secara ringkas sistem dan pelaksanaan tata pamong di program studi untuk  membangun sistem tata pamong yang kredibel, transparan, akuntabel, bertanggung jawab dan adil.\r\n', 1, 0, 1, 1, 'admin', '2018-08-09', NULL, NULL);
INSERT INTO `br_parameterborang3a` VALUES (8, 2, '2.1', 'Kepemimpinan', 'Kepemimpinan efektif mengarahkan dan mempengaruhi perilaku semua unsur dalam program studi, mengikuti nilai, norma, etika, dan budaya organisasi yang disepakati bersama, serta mampu membuat keputusan yang tepat dan cepat.\r\nKepemimpinan mampu memprediksi masa depan, merumuskan dan mengartikulasi visi yang realistik, kredibel, serta mengkomunikasikan visi ke depan, yang menekankan pada keharmonisan hubungan manusia dan mampu menstimulasi secara intelektual dan arif bagi anggota untuk mewujudkan visi organisasi, serta mampu memberikan arahan, tujuan, peran, dan tugas kepada seluruh unsur dalam perguruan tinggi.  Dalam menjalankan fungsi kepemimpinan dikenal kepemimpinan operasional, kepemimpinan organisasi, dan kepemimpinan publik.  Kepemimpinan operasional berkaitan dengan kemampuan menjabarkan visi, misi ke dalam kegiatan operasional program studi.  Kepemimpinan organisasi berkaitan dengan pemahaman tata kerja antar unit dalam organisasi perguruan tinggi.  Kepemimpinan publik berkaitan dengan kemampuan menjalin kerjasama dan menjadi rujukan bagi publik.\r\n\r\nJelaskan pola kepemimpinan dalam program studi, mencakup informasi tentang kepemimpinan operasional, kepemimpinan organisasi, dan kepemimpinan publik. \r\n', 1, 0, 1, 1, 'admin', '2018-08-09', NULL, NULL);
INSERT INTO `br_parameterborang3a` VALUES (9, 2, '2.3', 'Sistem Pengelolaan', 'Sistem pengelolaan fungsional dan operasional program studi mencakup perencanaan, pengorganisasian, pengembangan staf, pengawasan, pengarahan, representasi, dan penganggaran.\r\n\r\nJelaskan sistem pengelolaan program studi serta dokumen pendukungnya.\r\n\r\n', 1, 0, 1, 1, 'admin', '2018-08-09', NULL, NULL);
INSERT INTO `br_parameterborang3a` VALUES (10, 2, '2.4', 'Penjaminan Mutu', 'Jelaskan penjaminan mutu pada program studi yang mencakup informasi tentang kebijakan, sistem dokumentasi, dan tindak lanjut atas laporan pelaksanaannya', 1, 0, 1, 1, 'admin', '2018-08-09', NULL, NULL);
INSERT INTO `br_parameterborang3a` VALUES (11, 2, '2.5', 'Umpan Balik', 'Apakah program studi telah melakukan kajian tentang proses pembelajaran melalui umpan balik dari dosen, mahasiswa, alumni, dan pengguna lulusan mengenai harapan dan persepsi mereka?  Jika Ya, jelaskan isi umpan balik dan tindak lanjutnya', 2, 0, 1, 1, 'admin', '2018-08-09', NULL, NULL);
INSERT INTO `br_parameterborang3a` VALUES (12, 2, '2.6', 'Keberlanjutan', 'Jelaskan upaya untuk menjamin keberlanjutan (sustainability) program studi ini berikut hasilnya, khususnya dalam hal:\r\na.	Upaya untuk peningkatan animo calon mahasiswa:\r\n\r\nb.	Upaya peningkatan mutu manajemen:\r\n\r\nc.	Upaya untuk peningkatan mutu lulusan:\r\n\r\nd.	Upaya untuk pelaksanaan dan hasil kerjasama kemitraan:\r\n\r\ne.	Upaya dan prestasi memperoleh dana selain dari mahasiswa:', 1, 0, 1, 1, 'admin', '2018-08-09', 'admin', '2018-08-09');

-- ----------------------------
-- Table structure for br_standardua
-- ----------------------------
DROP TABLE IF EXISTS `br_standardua`;
CREATE TABLE `br_standardua`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tahunakademik` int(11) NULL DEFAULT NULL,
  `id_jenjangprodi` int(11) NULL DEFAULT NULL,
  `id_prodi` int(11) NULL DEFAULT NULL,
  `id_parameter` int(11) NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `uraian` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nilai` decimal(5, 2) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of br_standardua
-- ----------------------------
INSERT INTO `br_standardua` VALUES (6, 1, 1, 11, 7, '2018-08-08', '<p>isi prodi data xx</p>\r\n', 2.00, 'admin', '2018-08-12', 'admin', '2018-08-12');

-- ----------------------------
-- Table structure for br_standardua_kebijakan
-- ----------------------------
DROP TABLE IF EXISTS `br_standardua_kebijakan`;
CREATE TABLE `br_standardua_kebijakan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_borangsatu` int(11) NULL DEFAULT NULL,
  `kebijakan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `prosedur` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lampiran` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of br_standardua_kebijakan
-- ----------------------------
INSERT INTO `br_standardua_kebijakan` VALUES (4, 6, 'sdsd xx', 'sds', '', 'admin', '2018-08-12', 'admin', '2018-08-12');

-- ----------------------------
-- Table structure for br_standarsatu
-- ----------------------------
DROP TABLE IF EXISTS `br_standarsatu`;
CREATE TABLE `br_standarsatu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tahunakademik` int(11) NULL DEFAULT NULL,
  `id_jenjangprodi` int(11) NULL DEFAULT NULL,
  `id_prodi` int(11) NULL DEFAULT NULL,
  `id_parameter` int(11) NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `uraian` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nilai` decimal(5, 2) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of br_standarsatu
-- ----------------------------
INSERT INTO `br_standarsatu` VALUES (3, 1, 1, 11, 1, '2018-08-22', '<p>dfdfd fdfd fdfd dfdfd dfd fd</p>\r\n', 4.00, 'admin', '2018-08-11', NULL, NULL);
INSERT INTO `br_standarsatu` VALUES (4, 3, 1, 11, 1, '2018-07-04', '<p>dfdfdf</p>\r\n', 0.00, 'admin', '2018-08-11', NULL, NULL);
INSERT INTO `br_standarsatu` VALUES (5, 1, 1, 11, 2, '2018-08-08', '<p>coba</p>\r\n', 0.00, 'admin', '2018-08-11', 'admin', '2018-08-11');

-- ----------------------------
-- Table structure for br_standarsatu_kebijakan
-- ----------------------------
DROP TABLE IF EXISTS `br_standarsatu_kebijakan`;
CREATE TABLE `br_standarsatu_kebijakan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_borangsatu` int(11) NULL DEFAULT NULL,
  `kebijakan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `prosedur` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lampiran` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of br_standarsatu_kebijakan
-- ----------------------------
INSERT INTO `br_standarsatu_kebijakan` VALUES (2, 3, 'xxxxxxxxx', 'dsds', '41452030072018 FSD Aplikasi SPMI Marcubuana.pdf', NULL, NULL, NULL, NULL);
INSERT INTO `br_standarsatu_kebijakan` VALUES (3, 3, 'ada', '323', '9372861.1. STANDAR KOMPETENSI LULUSAN UMB  (April 2018).pdf', 'admin', '2018-08-11', NULL, NULL);

-- ----------------------------
-- Table structure for formulir
-- ----------------------------
DROP TABLE IF EXISTS `formulir`;
CREATE TABLE `formulir`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sop` int(11) NULL DEFAULT NULL,
  `nm_formulir` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_upload` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for kebijakan_spmi
-- ----------------------------
DROP TABLE IF EXISTS `kebijakan_spmi`;
CREATE TABLE `kebijakan_spmi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_upload` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `menudes` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `url` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `parent` int(9) NOT NULL,
  `menutool` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no` int(9) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 'Setting', '#', 76, 'Setting', 2);
INSERT INTO `menu` VALUES (2, 'Menu ', 'masterdata/menu', 1, 'Menu System', 5);
INSERT INTO `menu` VALUES (5, 'Rool Menu', 'masterdata/rool', 1, 'Master Rool Menu', 7);
INSERT INTO `menu` VALUES (7, 'Master User', 'masterdata/user', 1, 'Master User', 8);
INSERT INTO `menu` VALUES (33, 'Master', '#', 76, 'Master Data', 2);
INSERT INTO `menu` VALUES (34, 'SPMI', '#', 0, 'Master SPMI', 3);
INSERT INTO `menu` VALUES (35, 'Statuta', 'master/statuta', 34, 'Statuta', 1);
INSERT INTO `menu` VALUES (36, 'Renstra', 'master/renstra', 73, 'Renstra', 2);
INSERT INTO `menu` VALUES (37, 'Sarmut', '#', 34, 'Sarmut', 5);
INSERT INTO `menu` VALUES (38, 'Input Sarmut', 'sarmut/inputsarmut', 37, 'Input Sarmut', 1);
INSERT INTO `menu` VALUES (39, 'Realisasi Sarmut', 'sarmut/realisasisarmut', 37, 'Realisasi Sarmut', 2);
INSERT INTO `menu` VALUES (40, 'Grupmenu', 'masterdata/grupmenu', 1, 'Grupmenu', 4);
INSERT INTO `menu` VALUES (43, 'Borang APT', '#', 0, 'Borang APT', 7);
INSERT INTO `menu` VALUES (44, 'Borang D3', '#', 0, 'Borang D3', 8);
INSERT INTO `menu` VALUES (45, 'Borang S1', '#', 0, 'Borang S1', 9);
INSERT INTO `menu` VALUES (46, 'Borang S2', '#', 0, 'Borang S2', 10);
INSERT INTO `menu` VALUES (47, 'Borang S3', '#', 0, 'Borang S3', 11);
INSERT INTO `menu` VALUES (48, 'Borang 3A', '#', 43, 'Borang 3A', 1);
INSERT INTO `menu` VALUES (49, 'Borang 3B', '#', 43, 'Borang 3B', 2);
INSERT INTO `menu` VALUES (50, 'Evaluasi Diri', '#', 43, 'Evaluasi Diri', 3);
INSERT INTO `menu` VALUES (51, 'Laporan ', '#', 43, 'Laporan ', 4);
INSERT INTO `menu` VALUES (52, 'Borang 3A', '#', 44, 'Borang 3A', 3);
INSERT INTO `menu` VALUES (53, 'Borang 3B', '#', 44, 'Borang 3B', 4);
INSERT INTO `menu` VALUES (54, 'Evaluasi Diri', '#', 44, 'Evaluasi Diri', 5);
INSERT INTO `menu` VALUES (55, 'Laporan ', '#', 44, 'Laporan ', 6);
INSERT INTO `menu` VALUES (56, 'Borang 3A', '#', 45, 'Borang 3A', 1);
INSERT INTO `menu` VALUES (57, 'Audit', '#', 0, 'Audit', 4);
INSERT INTO `menu` VALUES (58, 'Tahun Akademik', 'masterdata/tahunakademik', 33, 'Tahun Akademik', 1);
INSERT INTO `menu` VALUES (59, 'Direktorat', 'masterdata/direktorat', 33, 'Direktorat', 2);
INSERT INTO `menu` VALUES (60, 'Planing Audit', 'audit/Planingaudit', 57, 'Planing Audit', 2);
INSERT INTO `menu` VALUES (61, 'Persiapan Formulir', 'audit/Persiapanformulir', 57, 'Persiapan Formulir', 2);
INSERT INTO `menu` VALUES (62, 'Standar 3', 'xx', 43, 'Standar 3', 3);
INSERT INTO `menu` VALUES (63, 'Rektorat', 'masterdata/rektorat', 33, 'Rektorat', 1);
INSERT INTO `menu` VALUES (64, 'Unit', 'masterdata/unit', 33, 'Unit', 3);
INSERT INTO `menu` VALUES (65, 'Karyawan', 'masterdata/karyawan', 33, 'Karyawan', 4);
INSERT INTO `menu` VALUES (66, 'Prodi', 'masterdata/prodi', 33, 'Prodi', 5);
INSERT INTO `menu` VALUES (67, 'Identitas Borang Prodi', 'masterdata/identitasborang', 33, 'Identitas Borang Prodi', 6);
INSERT INTO `menu` VALUES (68, 'Kebijakan SPMI', 'master/kebijakanspmi', 73, 'Kebijakan SPMI', 3);
INSERT INTO `menu` VALUES (69, 'Standar', 'master/standar', 74, 'Standar', 4);
INSERT INTO `menu` VALUES (70, 'SOP', 'master/sop', 74, 'SOP', 6);
INSERT INTO `menu` VALUES (71, 'Formulir', 'master/formulir', 74, 'Formulir', 7);
INSERT INTO `menu` VALUES (72, 'MKP', 'master/mkp', 34, 'MKP', 8);
INSERT INTO `menu` VALUES (73, 'Renstra', '#', 34, 'Renstra', 2);
INSERT INTO `menu` VALUES (74, 'Standar', '#', 34, 'Standar', 3);
INSERT INTO `menu` VALUES (75, 'Laporan Sarmut', '#', 37, 'Sarmut', 4);
INSERT INTO `menu` VALUES (76, 'Master', '#', 0, 'Master', 1);
INSERT INTO `menu` VALUES (77, 'Borang 3B', '#', 45, 'Borang 3B', 2);
INSERT INTO `menu` VALUES (78, 'Evaluasi Diri', '#', 45, 'Evaluasi Diri', 3);
INSERT INTO `menu` VALUES (79, 'Laporan', '#', 45, 'Laporan', 4);
INSERT INTO `menu` VALUES (80, 'Borang 3A', '#', 47, 'Borang 3A', 1);
INSERT INTO `menu` VALUES (81, 'Borang 3B', '#', 47, 'Borang 3B', 2);
INSERT INTO `menu` VALUES (82, 'Evaluasi Diri', '#', 47, 'Evaluasi Diri', 3);
INSERT INTO `menu` VALUES (83, 'Laporan', '#', 47, 'Evaluasi Diri', 4);
INSERT INTO `menu` VALUES (84, 'Standar 1', '#', 48, 'Standar 1', 1);
INSERT INTO `menu` VALUES (85, 'Standar 2', '#', 48, 'Standar 1', 2);
INSERT INTO `menu` VALUES (86, 'Borang 3A', '#', 46, 'Borang 3A', 1);
INSERT INTO `menu` VALUES (87, 'Borang 3B', '#', 46, 'Borang 3B', 2);
INSERT INTO `menu` VALUES (88, 'Evaluasi Diri', '#', 46, 'Evaluasi Diri', 3);
INSERT INTO `menu` VALUES (89, 'Laporan', '#', 46, 'Laporan', 4);
INSERT INTO `menu` VALUES (90, 'Parameter Borang 3A', 'borang/settingborang', 44, 'Parameter Borang 3A', 1);
INSERT INTO `menu` VALUES (91, 'Standar 1', 'borang/standarsatu', 52, 'Standar 1', 2);
INSERT INTO `menu` VALUES (92, 'Standar 2', 'borang/standardua', 52, 'Standar 2', 3);
INSERT INTO `menu` VALUES (93, 'Standar 3', 'borang/standartiga', 48, 'Standar 3', 4);
INSERT INTO `menu` VALUES (94, 'Standar 4', 'borang/standarempat', 48, 'Standar 4', 5);
INSERT INTO `menu` VALUES (95, 'Standar 5', 'borang/standarlima', 48, 'Standar 5', 6);
INSERT INTO `menu` VALUES (96, 'Standar 6', 'borang/standarenam', 48, 'Standar 6', 7);
INSERT INTO `menu` VALUES (97, 'Standar 7', 'borang/standartujuh', 48, 'Standar 7', 8);
INSERT INTO `menu` VALUES (98, 'Standar 3', 'borang/standartiga', 52, 'Standar 3', 3);
INSERT INTO `menu` VALUES (99, 'Standar 4', 'borang/standarempat', 52, 'Standar 4', 4);
INSERT INTO `menu` VALUES (100, 'Standar 5', 'borang/standarlima', 52, 'Standar 5', 5);
INSERT INTO `menu` VALUES (101, 'Standar 6', 'borang/standarenam', 52, 'Standar 6', 6);
INSERT INTO `menu` VALUES (102, 'Standar 7', 'borang/standartujuh', 52, 'Standar 7', 7);
INSERT INTO `menu` VALUES (103, 'Master Audit', '#', 37, 'Master Audit', 1);
INSERT INTO `menu` VALUES (104, 'Surat Tugas Audit', 'audit/Surattugas', 57, 'Surat Tugas Audit', 3);
INSERT INTO `menu` VALUES (105, 'Persiapan Pelaksanaan', 'audit/Persiapanpelaksanaan', 57, 'Persiapan Pelaksanaan', 4);
INSERT INTO `menu` VALUES (106, 'Audit Document', 'audit/pelaksanaan', 57, 'Audit Document', 5);
INSERT INTO `menu` VALUES (107, 'Audit Lapangan', 'audit/laporan', 57, 'Audit Lapangan', 6);
INSERT INTO `menu` VALUES (108, 'Tindak Korektif', 'audit/tindakkorektif', 57, 'Tindak Korektif Hasil Audit', 7);
INSERT INTO `menu` VALUES (109, 'Pemantauan PTK', 'audit/pemantauanptk', 57, 'Pemantauan PTK', 8);

-- ----------------------------
-- Table structure for menu_group
-- ----------------------------
DROP TABLE IF EXISTS `menu_group`;
CREATE TABLE `menu_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NULL DEFAULT 0,
  `id_menugrup` int(11) NULL DEFAULT NULL,
  `nm_grup` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_tahunakademik` int(11) NULL DEFAULT NULL,
  `id_jenjang` int(11) NULL DEFAULT 0,
  `id_direktoorat` int(11) NULL DEFAULT 0,
  `id_unit` int(11) NULL DEFAULT 0,
  `id_fakultas` int(11) NULL DEFAULT 0,
  `id_prodi` int(11) NULL DEFAULT 0,
  `id_jurusan` int(11) NULL DEFAULT 0,
  `id_menu` int(11) NULL DEFAULT NULL,
  `nm_menu` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_group
-- ----------------------------
INSERT INTO `menu_group` VALUES (7, 0, 1, 'Sarmut', 1, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (8, 0, 2, 'Borang', 1, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (9, 7, 7, 'Direktoral IT', 1, 0, 0, 0, 0, 0, 0, NULL, '', NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (10, 7, 2, 'Direktorat Inovasi', 1, 0, 0, 0, 0, 0, 0, NULL, '', NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (11, 9, 2, 'Input Sarmut', 1, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (12, 9, 3, 'Realisasi Sarmut', 1, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (13, 10, 3, 'Input Sarmut', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (14, 10, NULL, 'Realisasi Sarmut', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (15, 0, NULL, 'Unit PBSI', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (16, 8, NULL, 'Unit PASI', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (17, 9, NULL, 'UNIT SATU', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (18, 9, NULL, 'UNIT DUA', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (19, 10, NULL, 'Direktoral IT', 1, 4, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (20, 10, NULL, 'Direktorat Inovasi', 1, 4, 2, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (21, 19, NULL, 'Input Sarmut', 1, 4, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (22, 19, NULL, 'Realisasi Sarmut', 1, 4, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (23, 20, NULL, 'Input Sarmut', 1, 4, 2, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (24, 20, NULL, 'Realisasi Sarmut', 1, 4, 2, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (25, 19, NULL, 'Unit PBSI', 1, 6, 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (26, 19, NULL, 'Unit PASI', 1, 6, 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (27, 20, NULL, 'UNIT SATU', 1, 6, 0, 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (28, 20, NULL, 'UNIT DUA', 1, 6, 0, 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (29, 25, NULL, 'Input Sarmut', 1, 6, 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group` VALUES (30, 25, NULL, 'Realisasi Sarmut', 1, 6, 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for menu_group_copy
-- ----------------------------
DROP TABLE IF EXISTS `menu_group_copy`;
CREATE TABLE `menu_group_copy`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NULL DEFAULT 0,
  `id_menugrup` int(11) NULL DEFAULT NULL,
  `nm_grup` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_tahunakademik` int(11) NULL DEFAULT NULL,
  `id_jenjang` int(11) NULL DEFAULT 0,
  `id_direktoorat` int(11) NULL DEFAULT 0,
  `id_unit` int(11) NULL DEFAULT 0,
  `id_fakultas` int(11) NULL DEFAULT 0,
  `id_prodi` int(11) NULL DEFAULT 0,
  `id_jurusan` int(11) NULL DEFAULT 0,
  `id_menu` int(11) NULL DEFAULT NULL,
  `nm_menu` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_group_copy
-- ----------------------------
INSERT INTO `menu_group_copy` VALUES (7, 0, 1, 'User', 1, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (8, 0, 1, 'Menu', 1, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (9, 0, 1, 'Rool', 1, 0, 0, 0, 0, 0, 0, NULL, '', NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (10, 0, 2, 'Tahun Ajaran', 1, 0, 0, 0, 0, 0, 0, NULL, '', NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (11, 0, 2, 'Borang', 1, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (12, 0, 3, 'Rencana Audit', 1, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (13, 0, 3, 'Pelaksanaan Audit', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (14, 0, NULL, 'Direktoral IT', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (15, 0, NULL, 'Direktorat Inovasi', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (16, 8, NULL, 'Tahun Ajaran', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (17, 9, NULL, 'Rencana Audit', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (18, 9, NULL, 'Pelaksanaan Audit', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (19, 10, NULL, 'Direktoral IT', 1, 4, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (20, 10, NULL, 'Direktorat Inovasi', 1, 4, 2, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (21, 19, NULL, 'Input Sarmut', 1, 4, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (22, 19, NULL, 'Realisasi Sarmut', 1, 4, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (23, 20, NULL, 'Input Sarmut', 1, 4, 2, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (24, 20, NULL, 'Realisasi Sarmut', 1, 4, 2, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (25, 19, NULL, 'Unit PBSI', 1, 6, 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (26, 19, NULL, 'Unit PASI', 1, 6, 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (27, 20, NULL, 'UNIT SATU', 1, 6, 0, 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (28, 20, NULL, 'UNIT DUA', 1, 6, 0, 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (29, 25, NULL, 'Input Sarmut', 1, 6, 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `menu_group_copy` VALUES (30, 25, NULL, 'Realisasi Sarmut', 1, 6, 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for menu_grup_det
-- ----------------------------
DROP TABLE IF EXISTS `menu_grup_det`;
CREATE TABLE `menu_grup_det`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menugroup` int(11) NULL DEFAULT NULL,
  `nm_menu` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_menu` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_grup_det
-- ----------------------------
INSERT INTO `menu_grup_det` VALUES (1, 1, 'Input Sarmut', 38);
INSERT INTO `menu_grup_det` VALUES (2, 1, 'Realisasi Sarmut', 39);
INSERT INTO `menu_grup_det` VALUES (3, 2, 'Input Sarmut', 38);
INSERT INTO `menu_grup_det` VALUES (4, 2, 'Realisasi Sarmut', 39);
INSERT INTO `menu_grup_det` VALUES (5, 3, 'Input Sarmut', 38);
INSERT INTO `menu_grup_det` VALUES (6, 3, 'Realisasi Sarmut', 39);
INSERT INTO `menu_grup_det` VALUES (7, 4, 'Input Sarmut', 38);
INSERT INTO `menu_grup_det` VALUES (8, 4, 'Realisasi Sarmut', 39);
INSERT INTO `menu_grup_det` VALUES (9, 5, 'Master User', 7);
INSERT INTO `menu_grup_det` VALUES (10, 5, 'Menu ', 2);
INSERT INTO `menu_grup_det` VALUES (11, 5, 'Rool Menu', 5);

-- ----------------------------
-- Table structure for menu_grup_heder
-- ----------------------------
DROP TABLE IF EXISTS `menu_grup_heder`;
CREATE TABLE `menu_grup_heder`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_menugrup` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_grup_heder
-- ----------------------------
INSERT INTO `menu_grup_heder` VALUES (1, 'Setting');
INSERT INTO `menu_grup_heder` VALUES (2, 'Master Data');
INSERT INTO `menu_grup_heder` VALUES (3, 'Audit Internal');
INSERT INTO `menu_grup_heder` VALUES (4, 'Sarmut');
INSERT INTO `menu_grup_heder` VALUES (5, 'Borang');

-- ----------------------------
-- Table structure for mkp
-- ----------------------------
DROP TABLE IF EXISTS `mkp`;
CREATE TABLE `mkp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_standar` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for mkp_kegiatan
-- ----------------------------
DROP TABLE IF EXISTS `mkp_kegiatan`;
CREATE TABLE `mkp_kegiatan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mkp_target` int(11) NULL DEFAULT NULL,
  `no` int(11) NULL DEFAULT NULL,
  `kegiatan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `waktu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for mkp_target
-- ----------------------------
DROP TABLE IF EXISTS `mkp_target`;
CREATE TABLE `mkp_target`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mkp` int(11) NULL DEFAULT NULL,
  `uraian_target` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cek` smallint(6) NULL DEFAULT NULL COMMENT '0=target carakter,1=target angka, di target harus diisi',
  `target` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for mkp_unitdet
-- ----------------------------
DROP TABLE IF EXISTS `mkp_unitdet`;
CREATE TABLE `mkp_unitdet`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mkp_kegiatan` int(11) NULL DEFAULT NULL,
  `id_unit` int(11) NULL DEFAULT NULL,
  `jenis` smallint(1) NULL DEFAULT NULL COMMENT '1=unit,2=prodi,3=direktorat,4=fakultas',
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ms_direktorat
-- ----------------------------
DROP TABLE IF EXISTS `ms_direktorat`;
CREATE TABLE `ms_direktorat`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_direktorat` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_rektorat` int(11) NULL DEFAULT NULL,
  `kd_server` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  `flek` smallint(1) UNSIGNED NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ms_direktorat
-- ----------------------------
INSERT INTO `ms_direktorat` VALUES (1, 'Direktorat IT', 1, NULL, NULL, NULL, NULL, NULL, 1, 1);
INSERT INTO `ms_direktorat` VALUES (2, 'Direktorat Inovasi', 1, NULL, NULL, NULL, NULL, NULL, 1, 1);
INSERT INTO `ms_direktorat` VALUES (3, 'Direktorat Pemasaran R1', 1, NULL, NULL, NULL, NULL, NULL, 1, 1);
INSERT INTO `ms_direktorat` VALUES (4, 'Direktorat Keuangan', 1, NULL, NULL, NULL, NULL, NULL, 1, 1);

-- ----------------------------
-- Table structure for ms_fakultas
-- ----------------------------
DROP TABLE IF EXISTS `ms_fakultas`;
CREATE TABLE `ms_fakultas`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_fakultas` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_rektorat` int(11) NULL DEFAULT NULL,
  `id_server` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  `flek` smallint(1) NULL DEFAULT 2,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ms_fakultas
-- ----------------------------
INSERT INTO `ms_fakultas` VALUES (1, 'Fakultas Teknik', 2, NULL, NULL, NULL, NULL, NULL, 1, 2);
INSERT INTO `ms_fakultas` VALUES (2, 'Fakultas Akuntansi', 2, NULL, NULL, NULL, NULL, NULL, 1, 2);

-- ----------------------------
-- Table structure for ms_fakultas_hapus
-- ----------------------------
DROP TABLE IF EXISTS `ms_fakultas_hapus`;
CREATE TABLE `ms_fakultas_hapus`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_fakultas` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_rektorat` int(11) NULL DEFAULT NULL,
  `id_server` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  `flek` smallint(1) NULL DEFAULT 2,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ms_fakultas_hapus
-- ----------------------------
INSERT INTO `ms_fakultas_hapus` VALUES (1, 'Fakultas Teknik', 2, NULL, NULL, NULL, NULL, NULL, 1, 2);
INSERT INTO `ms_fakultas_hapus` VALUES (2, 'Fakultas Akuntansi', 2, NULL, NULL, NULL, NULL, NULL, 1, 2);

-- ----------------------------
-- Table structure for ms_jenjang
-- ----------------------------
DROP TABLE IF EXISTS `ms_jenjang`;
CREATE TABLE `ms_jenjang`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenjang` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ms_jenjang
-- ----------------------------
INSERT INTO `ms_jenjang` VALUES (1, 'Administrator', 'admin', NULL, NULL, NULL, 1);
INSERT INTO `ms_jenjang` VALUES (2, 'Rektor', 'admin', NULL, NULL, NULL, 1);
INSERT INTO `ms_jenjang` VALUES (3, 'Wakil Rektor', 'admin', NULL, NULL, NULL, 1);
INSERT INTO `ms_jenjang` VALUES (4, 'Direktorat', 'admin', NULL, NULL, NULL, 1);
INSERT INTO `ms_jenjang` VALUES (5, 'Fakultas', 'admin', NULL, NULL, NULL, 1);
INSERT INTO `ms_jenjang` VALUES (6, 'Unit', 'admin', NULL, NULL, NULL, 1);
INSERT INTO `ms_jenjang` VALUES (7, 'Prodi', 'admin', NULL, NULL, NULL, 1);
INSERT INTO `ms_jenjang` VALUES (8, 'Jurusan', 'admin', NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for ms_jenjangprodi
-- ----------------------------
DROP TABLE IF EXISTS `ms_jenjangprodi`;
CREATE TABLE `ms_jenjangprodi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_jenjangprodi` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kd_server` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ms_jenjangprodi
-- ----------------------------
INSERT INTO `ms_jenjangprodi` VALUES (1, 'D3', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `ms_jenjangprodi` VALUES (2, 'S1', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `ms_jenjangprodi` VALUES (3, 'S2', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `ms_jenjangprodi` VALUES (4, 'S3', NULL, NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for ms_jurusan
-- ----------------------------
DROP TABLE IF EXISTS `ms_jurusan`;
CREATE TABLE `ms_jurusan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_jurusan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_prodi` int(11) NULL DEFAULT NULL,
  `kd_server` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ms_jurusan
-- ----------------------------
INSERT INTO `ms_jurusan` VALUES (1, 'System Informasi', 1, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `ms_jurusan` VALUES (2, 'Teknik Informatika', 1, NULL, NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for ms_jurusan_hapus
-- ----------------------------
DROP TABLE IF EXISTS `ms_jurusan_hapus`;
CREATE TABLE `ms_jurusan_hapus`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_jurusan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_prodi` int(11) NULL DEFAULT NULL,
  `kd_server` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ms_jurusan_hapus
-- ----------------------------
INSERT INTO `ms_jurusan_hapus` VALUES (1, 'System Informasi', 1, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `ms_jurusan_hapus` VALUES (2, 'Teknik Informatika', 1, NULL, NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for ms_karyawan
-- ----------------------------
DROP TABLE IF EXISTS `ms_karyawan`;
CREATE TABLE `ms_karyawan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nidn` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `nm_karyawan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jenis_jabatan` int(11) NULL DEFAULT 0 COMMENT '1=rektorat,2=direktorat,4=kepala unit,4=staf',
  `id_jabatan` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ms_karyawan
-- ----------------------------
INSERT INTO `ms_karyawan` VALUES (1, '123', 'adam', 1, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ms_karyawan` VALUES (2, '231', 'ali', 2, 2, NULL, NULL, NULL, NULL);
INSERT INTO `ms_karyawan` VALUES (3, '333', 'hasan', 3, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ms_karyawan` VALUES (4, '44', 'malik', 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ms_karyawan` VALUES (5, '123', 'bayu12', 1, 1, 'admin', '2018-08-09', 'admin', '2018-08-09');
INSERT INTO `ms_karyawan` VALUES (6, '0', 'Riyan Apriyanto', 0, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for ms_level
-- ----------------------------
DROP TABLE IF EXISTS `ms_level`;
CREATE TABLE `ms_level`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ms_level
-- ----------------------------
INSERT INTO `ms_level` VALUES (1, 'Administrator', 'admin', NULL, NULL, NULL, 1);
INSERT INTO `ms_level` VALUES (2, 'Rektorat', 'admin', NULL, NULL, NULL, 1);
INSERT INTO `ms_level` VALUES (3, 'Direktorat', 'admin', NULL, NULL, NULL, 1);
INSERT INTO `ms_level` VALUES (4, 'Unit', 'admin', NULL, NULL, NULL, 1);
INSERT INTO `ms_level` VALUES (5, 'Prodi', 'admin', NULL, NULL, NULL, 1);
INSERT INTO `ms_level` VALUES (6, 'PAE', NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for ms_prodi
-- ----------------------------
DROP TABLE IF EXISTS `ms_prodi`;
CREATE TABLE `ms_prodi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_prodi` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_rektorat` int(11) NULL DEFAULT NULL,
  `id_jenjangprodi` int(11) NULL DEFAULT NULL,
  `parent` int(11) NULL DEFAULT 0,
  `kd_server` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  `flek` smallint(1) NULL DEFAULT 2,
  `level` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ms_prodi
-- ----------------------------
INSERT INTO `ms_prodi` VALUES (1, 'PERTANIAN', 3, 2, 0, NULL, NULL, NULL, 'admin', '2018-07-29', 1, 2, 1);
INSERT INTO `ms_prodi` VALUES (2, 'AGRIBISNIS', 3, 2, 1, NULL, NULL, NULL, 'admin', '2018-07-29', 1, 2, 2);
INSERT INTO `ms_prodi` VALUES (3, 'AGRONOMI', 3, 2, 1, NULL, 'admin', '2018-07-21', 'admin', '2018-07-29', 1, 2, 2);
INSERT INTO `ms_prodi` VALUES (4, 'Ilmu Komunikasi', 3, 2, 0, NULL, 'admin', '2018-07-27', 'admin', '2018-07-29', 1, 2, 1);
INSERT INTO `ms_prodi` VALUES (5, 'Penyiaran', 1, 2, 4, NULL, 'admin', '2018-07-27', 'admin', '2018-07-29', 1, 2, 2);
INSERT INTO `ms_prodi` VALUES (6, 'Hubungan Masyarakat', 3, 2, 4, NULL, 'admin', '2018-07-29', NULL, NULL, 1, 2, 2);
INSERT INTO `ms_prodi` VALUES (7, 'Periklanan dan Komunikasi Pemasaran', 3, 2, 4, NULL, 'admin', '2018-07-29', NULL, NULL, 1, 2, 2);
INSERT INTO `ms_prodi` VALUES (8, 'Komunikasi Visual', 3, 2, 4, NULL, 'admin', '2018-07-29', NULL, NULL, 1, 2, 2);
INSERT INTO `ms_prodi` VALUES (9, 'Komunikasi Digital', 3, 2, 4, NULL, 'admin', '2018-07-29', NULL, NULL, 1, 2, 2);
INSERT INTO `ms_prodi` VALUES (10, 'Ekonomi dan Bisnis', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 1, 2, 1);
INSERT INTO `ms_prodi` VALUES (11, 'Manajemen (D3)', NULL, 1, 10, NULL, 'admin', '2018-08-10', NULL, NULL, 1, 2, 2);
INSERT INTO `ms_prodi` VALUES (12, 'Akuntansi (D3)', NULL, 1, 10, NULL, 'admin', '2018-08-10', NULL, NULL, 1, 2, 2);
INSERT INTO `ms_prodi` VALUES (13, 'Desain dan Seni Kreatif', NULL, 2, 0, NULL, 'admin', '2018-08-10', NULL, NULL, 1, 2, 1);
INSERT INTO `ms_prodi` VALUES (14, 'TEKNIK DESAIN DAN INTERIOR (D3)', NULL, 1, 13, NULL, 'admin', '2018-08-10', NULL, NULL, 1, 2, 2);
INSERT INTO `ms_prodi` VALUES (15, 'Desain Produk', NULL, 2, 13, NULL, 'admin', '2018-08-10', NULL, NULL, 1, 2, 2);
INSERT INTO `ms_prodi` VALUES (16, 'Desain Interior', NULL, 2, 13, NULL, 'admin', '2018-08-10', NULL, NULL, 1, 2, 2);
INSERT INTO `ms_prodi` VALUES (17, 'Desain Komunikasi Visual', NULL, 2, 13, NULL, 'admin', '2018-08-10', NULL, NULL, 1, 2, 2);

-- ----------------------------
-- Table structure for ms_rektorat
-- ----------------------------
DROP TABLE IF EXISTS `ms_rektorat`;
CREATE TABLE `ms_rektorat`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_rektorat` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ms_rektorat
-- ----------------------------
INSERT INTO `ms_rektorat` VALUES (1, 'WD Sumberdaya', NULL, NULL, NULL, NULL, 1);
INSERT INTO `ms_rektorat` VALUES (2, 'WD Inovasi dan Kemah', NULL, NULL, 'admin', '2018-07-27', 1);
INSERT INTO `ms_rektorat` VALUES (3, 'WS Kemahasiswaan', 'admin', '2018-07-29', NULL, NULL, 1);

-- ----------------------------
-- Table structure for ms_unit
-- ----------------------------
DROP TABLE IF EXISTS `ms_unit`;
CREATE TABLE `ms_unit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_unit` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_direktorat` int(11) NULL DEFAULT NULL,
  `kd_server` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  `flek` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ms_unit
-- ----------------------------
INSERT INTO `ms_unit` VALUES (1, 'Unit PBSI', 1, NULL, NULL, NULL, NULL, NULL, 1, 1);
INSERT INTO `ms_unit` VALUES (2, 'Unit PASI', 1, NULL, NULL, NULL, NULL, NULL, 1, 1);
INSERT INTO `ms_unit` VALUES (3, 'Unit Satu', 2, NULL, NULL, NULL, NULL, NULL, 1, 1);
INSERT INTO `ms_unit` VALUES (4, 'Unit 2', 2, NULL, NULL, NULL, 'admin', '2018-07-27', 1, 1);

-- ----------------------------
-- Table structure for renstra
-- ----------------------------
DROP TABLE IF EXISTS `renstra`;
CREATE TABLE `renstra`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_dokumen` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_upload` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_awal` date NULL DEFAULT NULL,
  `tgl_akhir` date NULL DEFAULT NULL,
  `tgl_publish` date NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for renstra_det
-- ----------------------------
DROP TABLE IF EXISTS `renstra_det`;
CREATE TABLE `renstra_det`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_renstra` int(11) NULL DEFAULT NULL,
  `nomor` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `uraian` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `target` int(11) NULL DEFAULT NULL,
  `tahun_awal` int(11) NULL DEFAULT NULL,
  `tahun_tengah` int(11) NULL DEFAULT NULL,
  `tahun_akhir` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for rol
-- ----------------------------
DROP TABLE IF EXISTS `rol`;
CREATE TABLE `rol`  (
  `id_rol` int(11) NOT NULL AUTO_INCREMENT,
  `nm_rol` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `user_c` int(11) NULL DEFAULT NULL,
  `tgl_c` date NULL DEFAULT NULL,
  `user_u` int(11) NULL DEFAULT NULL,
  `tgl_u` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_rol`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of rol
-- ----------------------------
INSERT INTO `rol` VALUES (1, 'user', 1, NULL, NULL, 0, '2018-08-09');
INSERT INTO `rol` VALUES (2, 'dua', 1, NULL, NULL, 0, '2016-08-06');
INSERT INTO `rol` VALUES (3, 'admin', 1, 0, '2017-02-21', 0, '2018-08-19');
INSERT INTO `rol` VALUES (4, 'xxx', 1, 0, '2018-07-20', 0, '2018-07-20');
INSERT INTO `rol` VALUES (5, 'xcxcxc', 1, 0, '2018-07-20', 0, '2018-07-20');

-- ----------------------------
-- Table structure for rol_menu
-- ----------------------------
DROP TABLE IF EXISTS `rol_menu`;
CREATE TABLE `rol_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_rol` int(11) NULL DEFAULT NULL,
  `id_menu` int(11) NULL DEFAULT NULL,
  `tampil` tinyint(1) NOT NULL DEFAULT 0,
  `simpan` tinyint(1) NOT NULL DEFAULT 0,
  `ubah` tinyint(1) NOT NULL DEFAULT 0,
  `hapus` tinyint(1) NOT NULL DEFAULT 0,
  `approve` tinyint(1) NOT NULL DEFAULT 0,
  `cetak` tinyint(1) NOT NULL DEFAULT 0,
  `aktivasi` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 413 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of rol_menu
-- ----------------------------
INSERT INTO `rol_menu` VALUES (12, 2, 2, 1, 1, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (13, 2, 4, 1, 1, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (14, 2, 5, 1, 1, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (15, 2, 7, 1, 1, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (101, 4, 1, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (102, 4, 40, 1, 1, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (103, 4, 2, 1, 1, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (104, 4, 5, 1, 1, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (105, 4, 7, 1, 1, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (106, 4, 33, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (107, 4, 34, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (108, 4, 37, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (109, 4, 57, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (110, 4, 41, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (121, 5, 1, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (122, 5, 40, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (123, 5, 2, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (124, 5, 5, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (125, 5, 7, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (126, 5, 33, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (127, 5, 58, 1, 1, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (128, 5, 59, 1, 1, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (129, 5, 34, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (130, 5, 37, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (131, 5, 57, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (132, 5, 41, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (133, 1, 76, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (134, 1, 1, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (135, 1, 33, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (136, 1, 34, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (137, 1, 57, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (138, 1, 43, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (139, 1, 44, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (140, 1, 45, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (141, 1, 46, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (142, 1, 47, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (347, 3, 76, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (348, 3, 1, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (349, 3, 40, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (350, 3, 2, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (351, 3, 5, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (352, 3, 7, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (353, 3, 33, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (354, 3, 58, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (355, 3, 63, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (356, 3, 59, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (357, 3, 64, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (358, 3, 65, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (359, 3, 66, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (360, 3, 67, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (361, 3, 34, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (362, 3, 35, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (363, 3, 73, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (364, 3, 36, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (365, 3, 68, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (366, 3, 74, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (367, 3, 69, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (368, 3, 70, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (369, 3, 71, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (370, 3, 37, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (371, 3, 38, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (372, 3, 103, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (373, 3, 39, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (374, 3, 72, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (375, 3, 57, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (376, 3, 60, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (377, 3, 61, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (378, 3, 104, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (379, 3, 105, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (380, 3, 106, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (381, 3, 43, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (382, 3, 48, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (383, 3, 84, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (384, 3, 85, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (385, 3, 49, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (386, 3, 50, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (387, 3, 62, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (388, 3, 51, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (389, 3, 44, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (390, 3, 90, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (391, 3, 52, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (392, 3, 53, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (393, 3, 54, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (394, 3, 55, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (395, 3, 45, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (396, 3, 56, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (397, 3, 77, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (398, 3, 78, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (399, 3, 79, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (400, 3, 46, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (401, 3, 86, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (402, 3, 87, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (403, 3, 88, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (404, 3, 89, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (405, 3, 47, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `rol_menu` VALUES (406, 3, 80, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (407, 3, 81, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (408, 3, 82, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (409, 3, 83, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (410, 3, 107, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (411, 3, 108, 1, 1, 1, 1, 1, 1, 0);
INSERT INTO `rol_menu` VALUES (412, 3, 109, 1, 1, 1, 1, 1, 1, 0);

-- ----------------------------
-- Table structure for sarmut
-- ----------------------------
DROP TABLE IF EXISTS `sarmut`;
CREATE TABLE `sarmut`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mkp_kegiatan` int(11) NULL DEFAULT NULL,
  `kegiatan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `target_kuantitatif` int(11) NULL DEFAULT NULL,
  `target_kualitatif` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kinerja_tahunlalu` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `angaran` int(11) NULL DEFAULT NULL,
  `id_unit` int(11) NULL DEFAULT NULL,
  `jenis` smallint(6) NULL DEFAULT NULL COMMENT '1=unit,2=prodi,3=direktorat,4=fakultas',
  `tgl_mulai` date NULL DEFAULT NULL,
  `tgl_selesai` date NULL DEFAULT NULL,
  `id_tahunakademik` int(11) NULL DEFAULT NULL,
  `pelaksana` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_rencanaapprove` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_approve` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `presentase_realisasi` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sarmut_realisasi
-- ----------------------------
DROP TABLE IF EXISTS `sarmut_realisasi`;
CREATE TABLE `sarmut_realisasi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sarmut` int(11) NULL DEFAULT NULL,
  `tgl_kegiatan` date NULL DEFAULT NULL,
  `realisasi` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_upload` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ket_proges` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `presentase` int(10) NULL DEFAULT NULL COMMENT 'mengcu ke kolom target kuantitatif',
  `kendala` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tindakan_korelatif` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sop
-- ----------------------------
DROP TABLE IF EXISTS `sop`;
CREATE TABLE `sop`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_sop` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_standar` int(11) NULL DEFAULT NULL,
  `file_upload` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sp_formulir
-- ----------------------------
DROP TABLE IF EXISTS `sp_formulir`;
CREATE TABLE `sp_formulir`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sop` int(11) NULL DEFAULT NULL,
  `nm_formulir` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_upload` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_formulir
-- ----------------------------
INSERT INTO `sp_formulir` VALUES (1, 1, 'Formulir 1', 'dokfolmulir/557708gbr.png', 'admin', '2018-07-29', NULL, NULL, 1);
INSERT INTO `sp_formulir` VALUES (2, 2, 'Formulir 2', 'dokfolmulir/', 'admin', '2018-07-29', NULL, NULL, 1);

-- ----------------------------
-- Table structure for sp_formulir_audit
-- ----------------------------
DROP TABLE IF EXISTS `sp_formulir_audit`;
CREATE TABLE `sp_formulir_audit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ketuatim` int(11) NULL DEFAULT NULL,
  `id_unit` int(11) NULL DEFAULT NULL,
  `id_standar` int(11) NULL DEFAULT NULL,
  `catatan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `rencana` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `periode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tglclosing` date NULL DEFAULT NULL,
  `tgl_input` datetime(0) NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` datetime(0) NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jadwal` date NULL DEFAULT NULL,
  `id_auditie` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 68 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_formulir_audit
-- ----------------------------
INSERT INTO `sp_formulir_audit` VALUES (67, 6, 3, 2, 'wadwad', 'wadwadawdaw', '2017-2018', '2018-10-30', '2018-10-22 00:00:00', 'admin', NULL, NULL, '2018-10-25', 6);

-- ----------------------------
-- Table structure for sp_formulir_audit_doc
-- ----------------------------
DROP TABLE IF EXISTS `sp_formulir_audit_doc`;
CREATE TABLE `sp_formulir_audit_doc`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_formulir_audit` int(11) NULL DEFAULT NULL,
  `id_persiapan_formulir_doc` int(11) NULL DEFAULT NULL,
  `checklist` int(11) NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `masalah` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tindakan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tgl_input` datetime(0) NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` datetime(0) NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 116 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_formulir_audit_doc
-- ----------------------------
INSERT INTO `sp_formulir_audit_doc` VALUES (72, 48, 16, 3, 'dwadwa', 'dwadwadwa', 'dwadwadwadwad', '2018-10-05 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_formulir_audit_doc` VALUES (73, 49, 15, 1, 'wadwadaw', 'dwadwadwa', 'dwadwadwadwa', '2018-10-06 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_formulir_audit_doc` VALUES (78, 51, 16, 0, '', '', '', '2018-10-14 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_formulir_audit_doc` VALUES (80, 52, 15, 3, 'adwdwad', 'awdawdaw', 'dwadwadwa', '2018-10-14 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_formulir_audit_doc` VALUES (81, 50, 16, 1, 'adawdwad', 'wadawdawdaw', 'dwadwadwa', '2018-10-14 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_formulir_audit_doc` VALUES (89, 54, 16, 3, '1', '2', '3', '2018-10-14 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_formulir_audit_doc` VALUES (90, 55, 16, 2, '1', '2', '3', '2018-10-14 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_formulir_audit_doc` VALUES (98, 0, 16, 0, '', '', '', '2018-10-14 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_formulir_audit_doc` VALUES (115, 67, 16, 2, '123', '123', '123', '2018-10-22 00:00:00', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for sp_kebijakan_spmi
-- ----------------------------
DROP TABLE IF EXISTS `sp_kebijakan_spmi`;
CREATE TABLE `sp_kebijakan_spmi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_upload` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_kebijakan_spmi
-- ----------------------------
INSERT INTO `sp_kebijakan_spmi` VALUES (1, 'dokkebijakan/711700gbr.png', 'Kebijakan 2', 'admin', '2018-07-29', NULL, NULL, 1);
INSERT INTO `sp_kebijakan_spmi` VALUES (2, 'dokkebijakan/742401gbr.png', 'Kebijakan 1', 'admin', '2018-07-29', NULL, NULL, 1);

-- ----------------------------
-- Table structure for sp_laporan_audit
-- ----------------------------
DROP TABLE IF EXISTS `sp_laporan_audit`;
CREATE TABLE `sp_laporan_audit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_unit` int(11) NULL DEFAULT NULL,
  `tgl_audit` date NULL DEFAULT NULL,
  `jam` time(0) NOT NULL,
  `periode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kegiatan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tgl_input` datetime(0) NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` datetime(0) NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_mulai` date NULL DEFAULT NULL,
  `id_ketuatim` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_laporan_audit
-- ----------------------------
INSERT INTO `sp_laporan_audit` VALUES (29, 3, '2018-10-24', '22:44:45', '2017-2018', 'awdwadawdwadwa', '2018-10-27 00:00:00', 'admin', NULL, NULL, '2018-10-25', 6);

-- ----------------------------
-- Table structure for sp_laporan_audit_bidang
-- ----------------------------
DROP TABLE IF EXISTS `sp_laporan_audit_bidang`;
CREATE TABLE `sp_laporan_audit_bidang`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_laporan_audit` int(11) NULL DEFAULT NULL,
  `bidang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kelebihan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `peluang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` datetime(0) NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` datetime(0) NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 73 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_laporan_audit_bidang
-- ----------------------------
INSERT INTO `sp_laporan_audit_bidang` VALUES (25, 20, 'awdwadwad', 'wadwa', 'dwadwadwad', '2018-10-14 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_laporan_audit_bidang` VALUES (72, 29, 'awdwadwa', 'dwadwadaw', 'dwaddwa', '2018-10-27 00:00:00', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for sp_laporan_audit_daftar_hadir
-- ----------------------------
DROP TABLE IF EXISTS `sp_laporan_audit_daftar_hadir`;
CREATE TABLE `sp_laporan_audit_daftar_hadir`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_laporan_audit` int(11) NULL DEFAULT NULL,
  `id_unit` int(11) NULL DEFAULT NULL,
  `nm_karyawan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` datetime(6) NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` datetime(6) NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 73 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_laporan_audit_daftar_hadir
-- ----------------------------
INSERT INTO `sp_laporan_audit_daftar_hadir` VALUES (4, 18, 3, 'awdwadawdwa', '2018-10-14 00:00:00.000000', 'admin', NULL, NULL);
INSERT INTO `sp_laporan_audit_daftar_hadir` VALUES (5, 19, 4, 'wadawdwadwa', '2018-10-14 00:00:00.000000', 'admin', NULL, NULL);
INSERT INTO `sp_laporan_audit_daftar_hadir` VALUES (6, 20, 4, 'awdawdwadwadwa', '2018-10-14 00:00:00.000000', 'admin', NULL, NULL);
INSERT INTO `sp_laporan_audit_daftar_hadir` VALUES (72, 29, 3, 'awdwadwadwa', '2018-10-27 00:00:00.000000', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for sp_laporan_audit_kesimpulan
-- ----------------------------
DROP TABLE IF EXISTS `sp_laporan_audit_kesimpulan`;
CREATE TABLE `sp_laporan_audit_kesimpulan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_laporan_audit` int(11) NULL DEFAULT NULL,
  `kesimpulan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `opsi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lainnya` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_laporan_audit_kesimpulan
-- ----------------------------
INSERT INTO `sp_laporan_audit_kesimpulan` VALUES (28, 29, 'wadwadwa', 'dwadwa', 'dwadwdwa', 'admin', '2018-10-27', NULL, NULL);

-- ----------------------------
-- Table structure for sp_laporan_audit_kts_ob
-- ----------------------------
DROP TABLE IF EXISTS `sp_laporan_audit_kts_ob`;
CREATE TABLE `sp_laporan_audit_kts_ob`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_laporan_audit` int(11) NULL DEFAULT NULL,
  `id_standar` int(11) NULL DEFAULT NULL,
  `kts_ob` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pernyataan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` datetime(0) NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` datetime(0) NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 81 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_laporan_audit_kts_ob
-- ----------------------------
INSERT INTO `sp_laporan_audit_kts_ob` VALUES (35, 20, 5, 'wadwadwa', 'wadwadwadwa', '2018-10-14 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_laporan_audit_kts_ob` VALUES (80, 29, 5, '1', 'wadawdwa', '2018-10-27 00:00:00', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for sp_mkp
-- ----------------------------
DROP TABLE IF EXISTS `sp_mkp`;
CREATE TABLE `sp_mkp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tahunakademik` int(11) NULL DEFAULT NULL,
  `nm_mkp` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_mkp
-- ----------------------------
INSERT INTO `sp_mkp` VALUES (3, 1, 'tes', 'admin', '2018-08-28', NULL, NULL, 1);
INSERT INTO `sp_mkp` VALUES (5, 2, 'sdsd', 'admin', '2018-08-28', NULL, NULL, 1);

-- ----------------------------
-- Table structure for sp_mkp_kegiatan
-- ----------------------------
DROP TABLE IF EXISTS `sp_mkp_kegiatan`;
CREATE TABLE `sp_mkp_kegiatan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mkp_target` int(11) NULL DEFAULT NULL,
  `no` int(11) NULL DEFAULT NULL,
  `kegiatan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `waktu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_mkp_kegiatan
-- ----------------------------
INSERT INTO `sp_mkp_kegiatan` VALUES (2, 3, NULL, 'kegiatan 2 edit', 'Sm Ganjil', 'admin', '2018-08-07', NULL, NULL);
INSERT INTO `sp_mkp_kegiatan` VALUES (3, 3, NULL, 'kegiatan 3', 'Sm Ganjil,Sm Genap', 'admin', '2018-08-05', NULL, NULL);

-- ----------------------------
-- Table structure for sp_mkp_standar
-- ----------------------------
DROP TABLE IF EXISTS `sp_mkp_standar`;
CREATE TABLE `sp_mkp_standar`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mkp` int(11) NOT NULL,
  `id_standar` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_mkp_standar
-- ----------------------------
INSERT INTO `sp_mkp_standar` VALUES (1, 1, 2, 'admin', '2018-08-04', NULL, NULL, 1);
INSERT INTO `sp_mkp_standar` VALUES (2, 2, 4, 'admin', '2018-08-04', NULL, NULL, 1);
INSERT INTO `sp_mkp_standar` VALUES (3, 3, 4, 'admin', '2018-08-28', NULL, NULL, 1);
INSERT INTO `sp_mkp_standar` VALUES (4, 6, 4, 'admin', '2018-08-28', NULL, NULL, 1);
INSERT INTO `sp_mkp_standar` VALUES (5, 6, 5, 'admin', '2018-08-28', NULL, NULL, 1);

-- ----------------------------
-- Table structure for sp_mkp_target
-- ----------------------------
DROP TABLE IF EXISTS `sp_mkp_target`;
CREATE TABLE `sp_mkp_target`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mkp` int(11) NULL DEFAULT NULL,
  `uraian_target` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cek` smallint(6) NULL DEFAULT NULL COMMENT '0=target carakter,1=target angka, di target harus diisi',
  `target` int(11) NULL DEFAULT 0,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_mkp_target
-- ----------------------------
INSERT INTO `sp_mkp_target` VALUES (3, 2, 'wwww', 0, 0, 'admin', '2018-08-04', NULL, NULL);

-- ----------------------------
-- Table structure for sp_mkp_unitdet
-- ----------------------------
DROP TABLE IF EXISTS `sp_mkp_unitdet`;
CREATE TABLE `sp_mkp_unitdet`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mkp_kegiatan` int(11) NULL DEFAULT NULL,
  `id_unit` int(11) NULL DEFAULT NULL,
  `jenis` smallint(1) NULL DEFAULT NULL COMMENT '1=unit,2=direktorat',
  `nm_unit` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_mkp_unitdet
-- ----------------------------
INSERT INTO `sp_mkp_unitdet` VALUES (5, 3, 1, 1, NULL, 'admin', '2018-08-05');
INSERT INTO `sp_mkp_unitdet` VALUES (6, 3, 1, 2, NULL, 'admin', '2018-08-05');
INSERT INTO `sp_mkp_unitdet` VALUES (7, 3, 2, 2, NULL, 'admin', '2018-08-05');
INSERT INTO `sp_mkp_unitdet` VALUES (8, 2, 1, 1, NULL, 'admin', '2018-08-07');
INSERT INTO `sp_mkp_unitdet` VALUES (9, 2, 2, 1, NULL, 'admin', '2018-08-07');

-- ----------------------------
-- Table structure for sp_pemantauan
-- ----------------------------
DROP TABLE IF EXISTS `sp_pemantauan`;
CREATE TABLE `sp_pemantauan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_unit` int(11) NULL DEFAULT NULL,
  `id_ketuatim` int(11) NULL DEFAULT NULL,
  `id_auditie` int(11) NULL DEFAULT NULL,
  `id_standar` int(11) NULL DEFAULT NULL,
  `jadwal` date NULL DEFAULT NULL,
  `ktsob` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pernyataan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ptk_no` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kategori_ptk` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `uraian_temuan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `rencana_penyelesaian` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `realisasi_ptk` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pj_ptk` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `periode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` date NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_pemantauan
-- ----------------------------
INSERT INTO `sp_pemantauan` VALUES (3, 1, 1, 3, 2, NULL, 'awdwadaw', 'awdwadawdaw', 'dwadwadawdaw', 'Mayor', 'wadawdawdawdwadwadwa', 'wadwadawdawd', 'awdawdwadwadwa', 'dwadwadawdwad', '2017-2018', 'admin', '2018-10-21', NULL, NULL);
INSERT INTO `sp_pemantauan` VALUES (4, 1, 1, 3, 2, NULL, 'awdwadaw', 'awdwadawdawdaw', 'dwadawdawdwadaw', 'Observasi', 'adawdawdawdwadwadawdwad', 'wadwadwa', 'dawdwadawdawdwa', 'dwadwadawdwad', '2017-2018', 'admin', '2018-10-21', NULL, NULL);
INSERT INTO `sp_pemantauan` VALUES (5, 1, 1, 3, 5, '2018-10-25', 'wadwadwa', 'awdawdawdawd', 'wadwadwadwadwad', 'Observasi', 'dwadawdawdawdwadwa', 'dawdwadawd', 'wadwadwad', 'wadwadwadwadwa', '2017-2018', 'admin', '2018-10-21', NULL, NULL);
INSERT INTO `sp_pemantauan` VALUES (6, 1, 1, 3, 5, '2018-10-25', 'wadwadwa', 'wadwa', 'dwadwadwad', 'Observasi', 'awdwad', 'wadwadwadwaw', 'dwadwa', 'dwadwadwad', '2017-2018', 'admin', '2018-10-22', NULL, NULL);

-- ----------------------------
-- Table structure for sp_persiapan_formulir
-- ----------------------------
DROP TABLE IF EXISTS `sp_persiapan_formulir`;
CREATE TABLE `sp_persiapan_formulir`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_standar` int(11) NULL DEFAULT NULL,
  `materi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tgl_input` datetime(0) NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` datetime(0) NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_persiapan_formulir
-- ----------------------------
INSERT INTO `sp_persiapan_formulir` VALUES (13, 5, 'desa', '2018-08-29 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_persiapan_formulir` VALUES (14, 4, 'Pembelajaran', '2018-09-09 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_persiapan_formulir` VALUES (15, 2, 'Materi Renang', '2018-10-04 00:00:00', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for sp_persiapan_formulir_doc
-- ----------------------------
DROP TABLE IF EXISTS `sp_persiapan_formulir_doc`;
CREATE TABLE `sp_persiapan_formulir_doc`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_persiapan_formulir` int(11) NULL DEFAULT NULL,
  `document` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tgl_input` datetime(0) NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` datetime(0) NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_persiapan_formulir_doc
-- ----------------------------
INSERT INTO `sp_persiapan_formulir_doc` VALUES (13, 13, 'cek', '2018-08-29 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_persiapan_formulir_doc` VALUES (15, 14, 'doc 1', '2018-09-09 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_persiapan_formulir_doc` VALUES (16, 15, 'Materi Renang', '2018-10-04 00:00:00', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for sp_persiapan_pelaksanaan
-- ----------------------------
DROP TABLE IF EXISTS `sp_persiapan_pelaksanaan`;
CREATE TABLE `sp_persiapan_pelaksanaan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal` date NULL DEFAULT NULL,
  `id_planingaudit` int(11) NULL DEFAULT NULL,
  `periode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` datetime(0) NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` datetime(0) NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_persiapan_pelaksanaan
-- ----------------------------
INSERT INTO `sp_persiapan_pelaksanaan` VALUES (34, '2018-10-25', 20, '2017-2018', '2018-10-22 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_persiapan_pelaksanaan` VALUES (35, '2018-10-31', 21, '2017-2018', '2018-10-22 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_persiapan_pelaksanaan` VALUES (36, '2018-10-25', 22, '2017-2018', '2018-10-22 00:00:00', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for sp_planing_audit
-- ----------------------------
DROP TABLE IF EXISTS `sp_planing_audit`;
CREATE TABLE `sp_planing_audit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_unit` int(11) NULL DEFAULT NULL,
  `id_ketuatim` int(11) NULL DEFAULT NULL,
  `periode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tglmulai` date NULL DEFAULT NULL,
  `tglselesai` date NULL DEFAULT NULL,
  `tgl_input` datetime(0) NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` datetime(0) NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_planing_audit
-- ----------------------------
INSERT INTO `sp_planing_audit` VALUES (20, 1, 1, '2017-2018', '2018-10-25', '2018-11-24', '2018-10-27 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit` VALUES (21, 1, 4, '2017-2018', '2018-10-31', '2018-10-17', '2018-10-22 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit` VALUES (22, 3, 6, '2017-2018', '2018-10-25', '2018-10-26', '2018-10-27 00:00:00', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for sp_planing_audit_anggota
-- ----------------------------
DROP TABLE IF EXISTS `sp_planing_audit_anggota`;
CREATE TABLE `sp_planing_audit_anggota`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_planingaudit` int(11) NULL DEFAULT NULL,
  `id_anggota` int(11) NULL DEFAULT NULL,
  `tgl_input` datetime(0) NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` datetime(0) NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 169 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_planing_audit_anggota
-- ----------------------------
INSERT INTO `sp_planing_audit_anggota` VALUES (151, 21, 3, '2018-10-22 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit_anggota` VALUES (157, 22, 4, '2018-10-27 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit_anggota` VALUES (158, 22, 6, '2018-10-27 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit_anggota` VALUES (159, 22, 5, '2018-10-27 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit_anggota` VALUES (160, 22, 2, '2018-10-27 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit_anggota` VALUES (165, 20, 1, '2018-10-27 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit_anggota` VALUES (166, 20, 4, '2018-10-27 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit_anggota` VALUES (167, 20, 6, '2018-10-27 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit_anggota` VALUES (168, 20, 2, '2018-10-27 00:00:00', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for sp_planing_audit_auditie
-- ----------------------------
DROP TABLE IF EXISTS `sp_planing_audit_auditie`;
CREATE TABLE `sp_planing_audit_auditie`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_planingaudit` int(11) NULL DEFAULT NULL,
  `id_auditie` int(11) NULL DEFAULT NULL,
  `tgl_input` datetime(0) NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` datetime(0) NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_planing_audit_auditie
-- ----------------------------
INSERT INTO `sp_planing_audit_auditie` VALUES (94, 21, 3, '2018-10-22 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit_auditie` VALUES (97, 22, 6, '2018-10-27 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit_auditie` VALUES (99, 20, 3, '2018-10-27 00:00:00', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for sp_planing_audit_jadwal
-- ----------------------------
DROP TABLE IF EXISTS `sp_planing_audit_jadwal`;
CREATE TABLE `sp_planing_audit_jadwal`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_planingaudit` int(11) NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `jam_mulai` varchar(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jam_selesai` varchar(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ket_jadwal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` timestamp(6) NULL DEFAULT NULL,
  `tgl_update` timestamp(6) NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 76 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_planing_audit_jadwal
-- ----------------------------
INSERT INTO `sp_planing_audit_jadwal` VALUES (70, 21, '2018-10-24', '1', '2', '3', 'admin', '2018-10-22 00:00:00.000000', NULL, NULL);
INSERT INTO `sp_planing_audit_jadwal` VALUES (73, 22, '2018-10-24', 'awdwa', 'dwadwa', 'dwadwadwad', 'admin', '2018-10-27 00:00:00.000000', NULL, NULL);
INSERT INTO `sp_planing_audit_jadwal` VALUES (75, 20, '2018-10-24', '12', '13', 'JMBT', 'admin', '2018-10-27 00:00:00.000000', NULL, NULL);

-- ----------------------------
-- Table structure for sp_planing_audit_standar
-- ----------------------------
DROP TABLE IF EXISTS `sp_planing_audit_standar`;
CREATE TABLE `sp_planing_audit_standar`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_planingaudit` int(11) NULL DEFAULT NULL,
  `id_standar` int(11) NULL DEFAULT NULL,
  `tgl_input` datetime(0) NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` datetime(0) NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 142 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_planing_audit_standar
-- ----------------------------
INSERT INTO `sp_planing_audit_standar` VALUES (134, 21, 4, '2018-10-22 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit_standar` VALUES (137, 22, 2, '2018-10-27 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit_standar` VALUES (139, 20, 2, '2018-10-27 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit_standar` VALUES (140, 20, 4, '2018-10-27 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit_standar` VALUES (141, 20, 5, '2018-10-27 00:00:00', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for sp_planing_audit_tujuan
-- ----------------------------
DROP TABLE IF EXISTS `sp_planing_audit_tujuan`;
CREATE TABLE `sp_planing_audit_tujuan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_planingaudit` int(11) NULL DEFAULT NULL,
  `tujuan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` datetime(0) NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` datetime(0) NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_planing_audit_tujuan
-- ----------------------------
INSERT INTO `sp_planing_audit_tujuan` VALUES (11, 21, 'JMB', '2018-10-22 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit_tujuan` VALUES (14, 22, 'adawdaw', '2018-10-27 00:00:00', 'admin', NULL, NULL);
INSERT INTO `sp_planing_audit_tujuan` VALUES (16, 20, 'JMB', '2018-10-27 00:00:00', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for sp_renstra
-- ----------------------------
DROP TABLE IF EXISTS `sp_renstra`;
CREATE TABLE `sp_renstra`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_dokumen` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_upload` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_awal` date NULL DEFAULT NULL,
  `tgl_akhir` date NULL DEFAULT NULL,
  `tgl_publish` date NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_renstra
-- ----------------------------
INSERT INTO `sp_renstra` VALUES (1, 'Dokumen Renstra 1', 'dokrenstra/450744gbr.png', '2018-07-03', '2018-07-24', '2018-07-09', 'admin', '2018-07-29', NULL, NULL, 1);
INSERT INTO `sp_renstra` VALUES (3, 'Dokumen Renstra 2', 'dokrenstra/976654Picture1.png', '2018-06-26', '2018-06-27', '2018-06-27', 'admin', '2018-07-29', NULL, NULL, 1);

-- ----------------------------
-- Table structure for sp_renstra_det
-- ----------------------------
DROP TABLE IF EXISTS `sp_renstra_det`;
CREATE TABLE `sp_renstra_det`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_renstra` int(11) NULL DEFAULT NULL,
  `nomor` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `uraian` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `target` int(11) NULL DEFAULT NULL,
  `tahun_awal` int(11) NULL DEFAULT NULL,
  `tahun_tengah` int(11) NULL DEFAULT NULL,
  `tahun_akhir` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_renstra_det
-- ----------------------------
INSERT INTO `sp_renstra_det` VALUES (5, 3, '1', 'test', 20, 2001, 2009, 2019, 'admin', '2018-07-29', NULL, NULL);
INSERT INTO `sp_renstra_det` VALUES (6, 3, '2', 'test', 3, 203, 22, 222, 'admin', '2018-07-29', NULL, NULL);

-- ----------------------------
-- Table structure for sp_sarmut
-- ----------------------------
DROP TABLE IF EXISTS `sp_sarmut`;
CREATE TABLE `sp_sarmut`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mkp_kegiatan` int(11) NULL DEFAULT NULL,
  `kegiatan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `target_kuantitatif` int(11) NULL DEFAULT NULL,
  `target_kualitatif` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kinerja_tahunlalu` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `angaran` int(11) NULL DEFAULT NULL,
  `id_unit` int(11) NULL DEFAULT NULL,
  `jenis` smallint(6) NULL DEFAULT NULL COMMENT '1=unit,2=prodi,3=direktorat,4=fakultas',
  `tgl_mulai` date NULL DEFAULT NULL,
  `tgl_selesai` date NULL DEFAULT NULL,
  `id_tahunakademik` int(11) NULL DEFAULT NULL,
  `pelaksana` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_rencanaapprove` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_approve` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `presentase_realisasi` int(11) NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sp_sarmut_realisasi
-- ----------------------------
DROP TABLE IF EXISTS `sp_sarmut_realisasi`;
CREATE TABLE `sp_sarmut_realisasi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sarmut` int(11) NULL DEFAULT NULL,
  `tgl_kegiatan` date NULL DEFAULT NULL,
  `realisasi` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_upload` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ket_proges` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `presentase` int(10) NULL DEFAULT NULL COMMENT 'mengcu ke kolom target kuantitatif',
  `kendala` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tindakan_korelatif` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sp_sop
-- ----------------------------
DROP TABLE IF EXISTS `sp_sop`;
CREATE TABLE `sp_sop`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_sop` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_standar` int(11) NULL DEFAULT NULL,
  `file_upload` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_sop
-- ----------------------------
INSERT INTO `sp_sop` VALUES (1, 'SOP 1', 2, 'doksop/116394gbr.png', 'admin', '2018-07-29', NULL, NULL, 1);
INSERT INTO `sp_sop` VALUES (2, 'SOP2', 2, 'doksop/', 'admin', '2018-07-29', NULL, NULL, 1);

-- ----------------------------
-- Table structure for sp_standar
-- ----------------------------
DROP TABLE IF EXISTS `sp_standar`;
CREATE TABLE `sp_standar`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_renstra` int(11) NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `no_dokumen` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nm_dokumen` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_upload` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_refisi` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_publish` date NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_standar
-- ----------------------------
INSERT INTO `sp_standar` VALUES (2, 1, 4, '123', 'Dokumen Standar 2', 'dokstandar/689758Picture1.png', '33', '2018-07-31', 'admin', '2018-07-29', NULL, NULL, 1);
INSERT INTO `sp_standar` VALUES (4, 1, 0, '234', 'Dokumen Standar 1', 'dokstandar/322296gbr.png', 'ss', '2018-08-01', 'admin', '2018-07-29', NULL, NULL, 1);
INSERT INTO `sp_standar` VALUES (5, 1, 0, 'd23', 'dokumen standar 5', 'dokstandar/461853gbr.png', '-', '2018-07-31', 'admin', '2018-07-30', NULL, NULL, 1);

-- ----------------------------
-- Table structure for sp_standar_det
-- ----------------------------
DROP TABLE IF EXISTS `sp_standar_det`;
CREATE TABLE `sp_standar_det`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_standar` int(11) NULL DEFAULT NULL,
  `nomor` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `uraian` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `indikaor` int(11) NULL DEFAULT NULL,
  `target_waktu` int(11) NULL DEFAULT NULL,
  `file_upload` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_standar_det
-- ----------------------------
INSERT INTO `sp_standar_det` VALUES (3, 4, '1', '11', 22, 33, 'dokstandar/775726Picture1.png', 'admin', '2018-07-29', NULL, NULL);
INSERT INTO `sp_standar_det` VALUES (4, 5, '1', 'rencana standar 1', 123, 2019, NULL, 'admin', '2018-07-30', NULL, NULL);

-- ----------------------------
-- Table structure for sp_standar_unitdet
-- ----------------------------
DROP TABLE IF EXISTS `sp_standar_unitdet`;
CREATE TABLE `sp_standar_unitdet`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_standar_det` int(11) NULL DEFAULT NULL,
  `id_unit` int(11) NULL DEFAULT NULL,
  `jenis` smallint(1) NULL DEFAULT NULL COMMENT '1=unit,2=prodi,3=direktorat,4=fakultas',
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sp_statuta
-- ----------------------------
DROP TABLE IF EXISTS `sp_statuta`;
CREATE TABLE `sp_statuta`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_dokumen` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT 0,
  `nama_dokumen` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_upload` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_refisi` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_statuta
-- ----------------------------
INSERT INTO `sp_statuta` VALUES (2, 'D1', 0, 'Doukumen 1', 'dokstatuta/307342gbr.png', '123', 'admin', '2018-07-28', NULL, NULL, 1);
INSERT INTO `sp_statuta` VALUES (3, 'D11', 2, 'Dokumen Revisi 1', 'dokstatuta/244842gbr.png', 'R123', 'admin', '2018-07-29', NULL, NULL, 1);

-- ----------------------------
-- Table structure for sp_tindak_korektif
-- ----------------------------
DROP TABLE IF EXISTS `sp_tindak_korektif`;
CREATE TABLE `sp_tindak_korektif`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ketuatim` int(11) NULL DEFAULT NULL,
  `id_auditie` int(11) NULL DEFAULT NULL,
  `id_unit` int(11) NULL DEFAULT NULL,
  `id_standar` int(11) NULL DEFAULT NULL,
  `periode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ktsob` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jadwal` date NULL DEFAULT NULL,
  `ptk_no` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pernyataan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kategori_ptk` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `uraian_temuan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `rencana_temuan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `realisasi_ptk` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pj_ptk` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_input` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `user_update` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sp_tindak_korektif
-- ----------------------------
INSERT INTO `sp_tindak_korektif` VALUES (9, 3, 1, 1, 4, '2017-2018', '59', '2018-10-24', 'awdwadwa', 'wadwdwdwadaw', 'Mayor', 'dwadwad', 'wadwadwawa', 'wadawdwa', 'dwadwadwa', '2018-10-21', 'admin', NULL, NULL);
INSERT INTO `sp_tindak_korektif` VALUES (10, 1, 3, 1, 5, '2017-2018', '35', '2018-10-25', 'wadwadwadwa', 'awdwadwadwada', 'Observasi', 'dawdwa', 'dwadwadwadawdwad', 'awdawda', 'dwad', '2018-10-21', 'admin', NULL, NULL);
INSERT INTO `sp_tindak_korektif` VALUES (11, 4, 3, 1, 5, '2017-2018', '35', '2018-10-31', 'wadwadwadwa', 'awdawdwadwadwa', 'Mayor', 'wadwadwadwa', 'wadwadawdawwa', 'dwadawdwa', 'wadwadwadwa', '2018-10-22', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for standar
-- ----------------------------
DROP TABLE IF EXISTS `standar`;
CREATE TABLE `standar`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_renstra` int(11) NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `no_dokumen` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_dokumen` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_upload` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_refisi` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_pblish` date NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for standar_det
-- ----------------------------
DROP TABLE IF EXISTS `standar_det`;
CREATE TABLE `standar_det`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_standar` int(11) NULL DEFAULT NULL,
  `nomor` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `uraian` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `indikaor` int(11) NULL DEFAULT NULL,
  `target_waktu` int(11) NULL DEFAULT NULL,
  `file_upload` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for standar_unitdet
-- ----------------------------
DROP TABLE IF EXISTS `standar_unitdet`;
CREATE TABLE `standar_unitdet`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_standar_det` int(11) NULL DEFAULT NULL,
  `id_unit` int(11) NULL DEFAULT NULL,
  `jenis` smallint(1) NULL DEFAULT NULL COMMENT '1=unit,2=prodi,3=direktorat,4=fakultas',
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for statuta
-- ----------------------------
DROP TABLE IF EXISTS `statuta`;
CREATE TABLE `statuta`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_dokumen` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `nama_dokumen` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_upload` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_refisi` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tahun_akademik
-- ----------------------------
DROP TABLE IF EXISTS `tahun_akademik`;
CREATE TABLE `tahun_akademik`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_tahun_akademik` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `semester` smallint(1) NULL DEFAULT 1 COMMENT '1=genap,2=ganjil',
  `keterangan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_input` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  `user_update` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_update` date NULL DEFAULT NULL,
  `status` smallint(1) NULL DEFAULT 1,
  `kd_server` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tahun_akademik
-- ----------------------------
INSERT INTO `tahun_akademik` VALUES (1, '2016-2017', 1, 'Ganjil', NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `tahun_akademik` VALUES (2, '2017-2018', 1, 'Genap', NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `tahun_akademik` VALUES (3, '2018-2019', 1, 'Ganjil', 'admin', '2018-05-12', 'admin', '2018-07-29', 1, NULL);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_c` int(6) NULL DEFAULT NULL,
  `user_u` int(6) NULL DEFAULT NULL,
  `tgl_c` date NULL DEFAULT NULL,
  `tgl_u` date NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT 1,
  `username` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nm_user` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `idrool` int(10) NOT NULL,
  `id_level` int(11) NULL DEFAULT NULL,
  `id_rektorat` int(11) NULL DEFAULT NULL,
  `id_direktorat` int(11) NULL DEFAULT NULL,
  `id_unit` int(11) NULL DEFAULT NULL,
  `id_prodi` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 0, 3, '2016-05-03', '2017-03-08', 1, 'user', NULL, 'f673fa9f3695f7f985e5a6ba6f72f40d', 1, 3, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (2, 0, 3, '2016-08-01', '2017-03-01', 1, 'admindua', NULL, 'c58faf53ed72ab132b6e922090e8fd5c', 3, 2, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (3, 0, 3, '2016-08-01', '2017-02-23', 1, 'admin', NULL, '8dcc28cf85cccf4fc7c19793c909f37f', 3, 1, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (4, 3, 3, '2017-02-24', '2017-02-27', 1, 'userdua', NULL, '50795beaafd4790b33fdc994ab056320', 3, 8, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (5, 3, 3, '2018-07-27', '2018-08-09', 1, 'test1', NULL, '733dafb1c672d06b49a4e8de54b58e25', 2, 2, 1, 0, 0, 0);

-- ----------------------------
-- Table structure for user_menu
-- ----------------------------
DROP TABLE IF EXISTS `user_menu`;
CREATE TABLE `user_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NULL DEFAULT NULL,
  `id_menu` int(11) NULL DEFAULT NULL,
  `tampil` tinyint(1) NULL DEFAULT NULL,
  `simpan` tinyint(1) NULL DEFAULT NULL,
  `ubah` tinyint(1) NULL DEFAULT NULL,
  `hapus` tinyint(1) NULL DEFAULT NULL,
  `approve` tinyint(1) NULL DEFAULT NULL,
  `cetak` tinyint(1) NULL DEFAULT NULL,
  `aktivasi` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1207 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_menu
-- ----------------------------
INSERT INTO `user_menu` VALUES (389, 1, 11, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (390, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (391, 1, 2, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (392, 1, 5, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (393, 1, 7, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (394, 1, 15, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (395, 1, 17, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (396, 1, 18, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (397, 1, 20, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (398, 2, 11, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (399, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (400, 2, 2, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (401, 2, 5, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (402, 2, 7, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (403, 2, 8, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (404, 2, 9, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (405, 2, 10, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (406, 2, 12, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (407, 2, 13, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (408, 2, 14, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (409, 2, 16, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (410, 2, 15, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (411, 2, 17, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (412, 2, 18, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (413, 2, 20, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `user_menu` VALUES (631, 4, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (632, 4, 40, 1, 1, 1, 1, 0, 0, NULL);
INSERT INTO `user_menu` VALUES (633, 4, 2, 1, 1, 1, 1, 0, 0, NULL);
INSERT INTO `user_menu` VALUES (634, 4, 5, 1, 1, 1, 1, 0, 0, NULL);
INSERT INTO `user_menu` VALUES (635, 4, 7, 1, 1, 1, 1, 0, 0, NULL);
INSERT INTO `user_menu` VALUES (636, 4, 33, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (637, 4, 58, 1, 1, 0, 0, 0, 0, NULL);
INSERT INTO `user_menu` VALUES (638, 4, 63, 1, 1, 0, 0, 0, 0, NULL);
INSERT INTO `user_menu` VALUES (639, 4, 59, 1, 1, 0, 0, 0, 0, NULL);
INSERT INTO `user_menu` VALUES (640, 4, 64, 1, 1, 0, 0, 0, 0, NULL);
INSERT INTO `user_menu` VALUES (641, 4, 65, 1, 1, 0, 0, 0, 0, NULL);
INSERT INTO `user_menu` VALUES (642, 4, 66, 1, 1, 0, 0, 0, 0, NULL);
INSERT INTO `user_menu` VALUES (643, 4, 67, 1, 1, 0, 0, 0, 0, NULL);
INSERT INTO `user_menu` VALUES (644, 4, 34, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (645, 4, 37, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (646, 4, 57, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (647, 4, 41, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (937, 5, 76, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (938, 5, 34, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (939, 5, 57, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (940, 5, 43, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (941, 5, 44, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (942, 5, 45, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (943, 5, 46, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (944, 5, 47, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (1134, 3, 76, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (1135, 3, 1, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1136, 3, 40, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1137, 3, 2, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1138, 3, 5, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1139, 3, 7, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1140, 3, 33, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1141, 3, 58, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1142, 3, 63, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1143, 3, 59, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1144, 3, 64, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1145, 3, 65, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1146, 3, 66, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1147, 3, 67, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1148, 3, 34, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (1149, 3, 35, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1150, 3, 73, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1151, 3, 36, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1152, 3, 68, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1153, 3, 74, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1154, 3, 69, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1155, 3, 70, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1156, 3, 71, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1157, 3, 37, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1158, 3, 38, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1159, 3, 103, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1160, 3, 39, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1161, 3, 72, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1162, 3, 57, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (1163, 3, 60, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1164, 3, 61, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1165, 3, 104, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1166, 3, 105, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1167, 3, 106, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1168, 3, 43, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (1169, 3, 48, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1170, 3, 84, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1171, 3, 85, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1172, 3, 49, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1173, 3, 50, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1174, 3, 62, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1175, 3, 51, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1176, 3, 44, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (1177, 3, 90, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1178, 3, 52, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1179, 3, 91, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1180, 3, 92, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1181, 3, 98, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1182, 3, 99, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1183, 3, 100, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1184, 3, 101, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1185, 3, 102, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1186, 3, 53, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1187, 3, 54, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1188, 3, 55, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1189, 3, 45, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (1190, 3, 56, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1191, 3, 77, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1192, 3, 78, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1193, 3, 79, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1194, 3, 46, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (1195, 3, 86, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1196, 3, 87, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1197, 3, 88, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1198, 3, 89, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1199, 3, 47, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user_menu` VALUES (1200, 3, 80, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1201, 3, 81, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1202, 3, 82, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1203, 3, 83, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1204, 3, 107, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1205, 3, 108, 1, 1, 1, 1, 1, 1, NULL);
INSERT INTO `user_menu` VALUES (1206, 3, 109, 1, 1, 1, 1, 1, 1, NULL);

SET FOREIGN_KEY_CHECKS = 1;
