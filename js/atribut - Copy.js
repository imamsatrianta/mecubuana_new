

var path = window.location.pathname;
var page = path.split("/").pop();

$(document).ready(function ($) {
var menuaktif='1';//document.getElementById("id_menuaktif").innerHTML;
//alert(menuaktif);
$(".treeview").removeClass('active');
 $("#menu_pertama"+menuaktif).addClass('active');
 
    $('#form')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                },
				'option[]': {
                    validators: {
                        notEmpty: {
                            message: 'Text Harus Diisi'
                        }
                    }
                },
                description: {
                    validators: {
                        notEmpty: {
                            message: 'The description is required'
                        }
                    }
                },
                priority: {
                    validators: {
                        notEmpty: {
                            message: 'The priority is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanData();
		 e.preventDefault();
    });
	
	$('.select2').select2({
		allowClear: true
	});
	
	$aktivasi.prop('disabled', true);
	
	
	
	
	
	// form dua validasi
	  $('#formmenu')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                },
                description: {
                    validators: {
                        notEmpty: {
                            message: 'The description is required'
                        }
                    }
                },
                priority: {
                    validators: {
                        notEmpty: {
                            message: 'The priority is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//	alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		 //alert("ok");
		simpanDatadetail();
		 e.preventDefault();
    });
	
	
		// form approve
	  $('#formapp')
        .bootstrapValidator({
		 excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                },
                description: {
                    validators: {
                        notEmpty: {
                            message: 'The description is required'
                        }
                    }
                },
                priority: {
                    validators: {
                        notEmpty: {
                            message: 'The priority is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
			//alert("h");
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		 //alert("ok");
		simpanDataapprove();
		 e.preventDefault();
    });
	
	
	 $('#formdet')
        .bootstrapValidator({
		// excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required'
                        }
                    }
                },
				'option[]': {
                    validators: {
                        notEmpty: {
                            message: 'Text Harus Diisi'
                        }
                    }
                },
                description: {
                    validators: {
                        notEmpty: {
                            message: 'The description is required'
                        }
                    }
                },
                priority: {
                    validators: {
                        notEmpty: {
                            message: 'The priority is required'
                        }
                    }
                }
            }
        })
        .on('status.field.bv', function(e, data) {
		//alert(data.field);
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e,data){
		// alert("ok");
		simpanDatadet();
		 e.preventDefault();
    });
	
	
 });
 
	
	function simpanDataapprove(){
		 document.getElementById("btnApp").disabled=true;
 	 var data = $('#formapp').serializeArray();
			  $.ajax({
				  type: "POST",
				  url: ""+page+"/simpanDataapprove",
				  data: data,
				  success: function(result) { 
				 // alert(result);
					try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						  keluarLogin();
					}
					
					 tutupFormapp();
					 loadData(1, 15,'desc');
				//	 $("#no_grup_satu").val("");
				  }
			  });
			  return false;
	}

	 function simpanDatadet(){
		  document.getElementById("btnSave").disabled=true;
 	 var data = $('#formdet').serializeArray();
			  $.ajax({
				  type: "POST",
				  url: ""+page+"/simpanData",
				  data: data,
				  success: function(result) { 
				 // alert(result);
					try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						  keluarLogin();
					}
					
					 tutupFormdet();
					 loadData(1, 15,'desc');
					// $("#no_grup_satu").val("");
				  }
			  });
			  return false;
	 }
	 function simpanData(){
	//alert('ok');

	 document.getElementById("btnSave").disabled=true;
 	 var data = $('#form').serializeArray();
			  $.ajax({
				  type: "POST",
				  url: ""+page+"/simpanData",
				  data: data,
				  success: function(result) { 
				 // alert(result);
					try {
					  obj = JSON.parse(result);  
					var pesan=obj['pesan'];
					var status=obj['status'];
				  	Command: toastr[status](pesan);
					  } catch (e) {
						  keluarLogin();
					}
					
					 tutupForm();
					 loadData(1, 15,'desc');
					// $("#no_grup_satu").val("");
				  }
			  });
			  return false;
 }
	function Capitalize(str){  
	return str.replace (/\w\S*/g, 
      function(txt)
      {  return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); } );
	}
	
	function tambahData(){
		var halaman=document.getElementById("judulmenu").innerHTML; 
		//alert("a");
		 
     $('#form')[0].reset(); 
	 $('#form').bootstrapValidator('resetForm', true);
      $('#modal_form').modal('show'); 
	  /* $('#modal_form').modal('show')
              .draggable({ handle: ".modal-header" });
			  */
      $('.modal-title').text('Form Tambah '+halaman+''); 
	  document.getElementById('set').value=0;
	  document.getElementById('btnSave').disabled=false;
	 // $("#id_grup_satu").select2("val","");
		
    }
	
	
	function printHeder(){
		var halaman=document.getElementById("judulmenu").innerHTML; 
		//alert("a");
     $('#formprint')[0].reset(); 
	 $('#formprint').bootstrapValidator('resetForm', true);
      $('#modal_formprint').modal('show'); 
      $('.modal-title').text('Form Print '+halaman+''); 
	  
	  $('.select2').select2({
			allowClear: true
			});
			$('#id_triwulanprint').val('').trigger('change');
	 // $("#id_grup_satu").select2("val","");
		
    }
	
	
	function tutupFormapp(){
		 $('#formprint')[0].reset(); // reset form on modals 
      	$('#modal_formprint').modal('hide'); // show bootstrap modal
	}
	
	function appHeder(){
		var halaman=document.getElementById("judulmenu").innerHTML; 
		//alert("a");
     $('#formapp')[0].reset(); 
	 $('#formapp').bootstrapValidator('resetForm', true);
      $('#modal_formapp').modal('show'); 
      $('.modal-title').text('Form Approve '+halaman+''); 
	  
	  $('.select2').select2({
		allowClear: true
	});
	document.getElementById("btnApp").disabled=false;
	 // $("#id_grup_satu").select2("val",""); 
		
    }
	
	function tutupFormapp(){
		 $('#formapp')[0].reset(); // reset form on modals 
      	$('#modal_formapp').modal('hide'); // show bootstrap modal
	}
   
	function tutupForm(){
		 $('#form')[0].reset(); // reset form on modals 
      	$('#modal_form').modal('hide'); // show bootstrap modal
	}
	
	function tutupFormdet(){
		 $('#formdet')[0].reset(); // reset form on modals 
      	$('#modal_form').modal('hide'); // show bootstrap modal
	}
    function loadData(number, size,order){
            var offset=(number - 1) * size;
            var limit=size;
			
            $.ajax({
                    type: "POST",
                    url: ""+page+"/loaddataTabel?order="+order+"&limit="+limit+"&offset="+offset,
                    dataType:"JSON",
                    success: function(result){
    				 $table.bootstrapTable('load', result);
    			
                    }
                });
        }
		
   
    
    function editForm(row){
		var halaman=document.getElementById("judulmenu").innerHTML; 
        $('#form')[0].reset(); 
      $('#modal_form').modal('show'); 
      $('.modal-title').text('Form Ubah '+halaman+''); 
	  document.getElementById('set').value=1;
	  document.getElementById('btnSave').disabled=false;
	  
		 var buttonedit=document.getElementById('buttonedit').value;
		 
	    for (var name in row) {
		//alert(name);
			 $('#modal_form').find('input[name="' + name + '"]').val(row[name]);
        }
		//alert(row);
		if (buttonedit==1){
			//alert("s");
			editFormtambah(row);
		}
		//$("#idrool").select2("val", 1);
      
		$('#form').bootstrapValidator('resetForm',false);
    }
	
	
	function editFormdua(row){
		var halaman=document.getElementById("judulmenu").innerHTML; 
        $('#formdet')[0].reset(); 
      $('#modal_form').modal('show'); 
      $('.modal-title').text('Form Ubah '+halaman+''); 
	  
	  $(".modal").css({"position":"absolute"});
	  $('html,body').scrollTop(0);

	  document.getElementById('set').value=1;
	  document.getElementById('btnSave').disabled=false;
	  
		 var buttonedit=document.getElementById('buttonedit').value;
		 
	    for (var name in row) {
		//alert(name);
			 $('#modal_form').find('input[name="' + name + '"]').val(row[name]);
        }
		//alert(row);
		if (buttonedit==1){
			//alert("s");
			editFormtambah(row);
		}
		
	//	alert("s");
	//	$('#form').bootstrapValidator('resetForm',false);
		
	}
    
    
   
     var $table = $('#table'),
        $remove = $('#remove'),
		$aktivasi = $('#aktivasi'),
		$aktivasikon = $('#aktivasikon')
    $(function () {
        $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
            $remove.prop('disabled', !$table.bootstrapTable('getSelections').length);
        });
		
        $remove.click(function () {
		var hapus="";
            var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
				hapus=row.id+","+hapus;
            });
			
			hapusArray(hapus);
            $table.bootstrapTable('remove', {
                field: 'id',
                values: ids
            });
            $remove.prop('disabled', true);
        });
		
		
    });
	
	
     $(function () {
        $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
            $aktivasi.prop('disabled', !$table.bootstrapTable('getSelections').length);
        });
		
        $aktivasi.click(function () {
		var aktiv="";
            var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
				aktiv=row.id+","+aktiv;
            });
			
			aktifArray(aktiv);
           /* $table.bootstrapTable('aktivasi', {
                field: 'id',
                values: ids
            });*/
            $aktivasi.prop('disabled', true);
        });
		
		
    });
	
	$(function () {
        $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
            $aktivasikon.prop('disabled', !$table.bootstrapTable('getSelections').length);

        });
		
        $aktivasikon.click(function () {
			var aktiv="";
			aktiv = 'nil=0';
			var i=0;
            var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
				i++;
				aktiv=aktiv+'&id'+i+'='+row.id;
				aktiv=aktiv+'&kondisi'+i+'='+row.kondisi;
				
			
            });
			aktiv=aktiv+'&jumlah='+i;
			//alert(aktiv);
			
			aktifArraykon(aktiv);
            /*$table.bootstrapTable('aktivasikon', {
                field: 'id',
                values: ids
            });
			*/
            $aktivasikon.prop('disabled', true);
        });
		
		
    });
	
	
	
   function statusFormat(value, row, index) {
	   	if(row.status=='0'){
			return ['Tidak Aktiv'];
		}else{
			return [' Aktiv'];
		}
    
    }
   
    window.operateEvents = {
        'click #edit': function (e, value, row, index) {
			if(row.status==0){
				bootbox.alert("Data Tidak Bisa Diubah, Status Data Tidak Aktiv!"); 
			}else{
				editForm(row);
			}
            
         //   alert('You click like action, row: ' + JSON.stringify(row));
        },
		'click #editdua': function (e, value, row, index) {
			if(row.status==0){
				bootbox.alert("Data Tidak Bisa Diubah, Status Data Tidak Aktiv!"); 
			}else{
				editFormdua(row);
			}
            
         //   alert('You click like action, row: ' + JSON.stringify(row));
        },
        'click #remove': function (e, value, row, index) {
            hapusData(row.id);
        },
		'click #aktivasi': function (e, value, row, index) {
            aktivData(row.id);
        },
         'click #menu': function (e, value, row, index) {
            
			if(row.status==0){
				bootbox.alert("Data Tidak Bisa Diubah, Status Data Tidak Aktiv!"); 
			}else{
				menuForm(row);
			}
         //   alert('You click like action, row: ' + JSON.stringify(row));
        },
         'click #appdet': function (e, value, row, index) {
            approveDetail(row.id);
         //   alert('You click like action, row: ' + JSON.stringify(row));
        },
         'click #printdet': function (e, value, row, index) {
            printDetail(row.id);
         //   alert('You click like action, row: ' + JSON.stringify(row));
        },
         'click #aktivasikon': function (e, value, row, index) {
            aktivDatakondisi(row);
        }
    };
      
    function hapusData(id){
		//alert(id);
		bootbox.confirm("Yakin Data akan di Hapus?", function(result) {
			if(result==true){
				 $.ajax({
                    url: ''+page+'/hapusData?id='+id,
                    type: 'POST',
                    success: function (data) {
					//alert(data);
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
					
						
						loadData(1, 15,'desc');
                        
                    }
                })
			}
               
        });
	}
	
	 function aktivData(id){
		bootbox.confirm("Yakin Data akan di Aktivasi?", function(result) {
			if(result==true){
				 $.ajax({
                    url: ''+page+'/aktivData?id='+id,
                    type: 'POST',
                    success: function (data) {
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
						loadData(1, 15,'desc');
                        
                    }
                })
			}
               
        });
	}
	function aktivDatakondisi(row){
		var id=row.id;
		var kondisi=row.kondisi;
		bootbox.confirm("Yakin Data akan di Aktivasi?", function(result) {
			if(result==true){
				 $.ajax({
                    url: ''+page+'/aktivData?id='+id+'&kondisi='+kondisi,
                    type: 'POST',
                    success: function (data) {
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
						loadData(1, 15,'desc');
                        
                    }
                })
			}
               
        });
		
	}
	function approveDetail(id){
		bootbox.confirm("Yakin Data akan di Approve?", function(result) {
			if(result==true){
				 $.ajax({
                    url: ''+page+'/approveDetail?id='+id,
                    type: 'POST',
                    success: function (data) {
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
						loadData(1, 15,'desc');
                        
                    }
                })
			}
               
        });
		
	}
	
	function printDetail(id){
		bootbox.confirm("Yakin Data akan di Approve?", function(result) {
			if(result==true){
				 $.ajax({
                    url: ''+page+'/printDetail?id='+id,
                    type: 'POST',
                    success: function (data) {
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
						loadData(1, 15,'desc');
                        
                    }
                })
			}
               
        });
		
	}
	
	
	function hapusArray(hapus){
		bootbox.confirm("Yakin Data akan di Hapus?", function(result) {
			if(result==true){
				 $.ajax({
                    url: ''+page+'/hapusDataarray?data='+hapus,
                    type: 'POST',
                    success: function (data) {
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
						
						/*obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);*/
						loadData(1, 15,'desc');
                    }
                })
			}
               
        });
	}
	
	function aktifArray(aktiv){
		bootbox.confirm("Yakin Data akan di Aktivasi?", function(result) {
			if(result==true){
				 $.ajax({
                    url: ''+page+'/aktivDataarray?data='+aktiv,
                    type: 'POST',
                    success: function (data) {
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
						/*obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);*/
						loadData(1, 15,'desc');
						$aktivasi.prop('disabled', true);
                    }
                })
			}
               
        });
	}
	
	function aktifArraykon(aktiv){
		bootbox.confirm("Yakin Data akan di Aktivasi?", function(result) {
			if(result==true){
				 $.ajax({
                    url: ''+page+'/aktivDataarraykon?'+aktiv,
                    type: 'POST',
                    success: function (data) {
						try {
						  obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);
						  } catch (e) {
							  keluarLogin();
						}
						/*obj = JSON.parse(data);  
						var pesan=obj['pesan'];
						var status=obj['status'];
						Command: toastr[status](pesan);*/
						loadData(1, 15,'desc');
						$aktivasikon.prop('disabled', true);
                    }
                })
			}
               
        });
	}
	
	
	